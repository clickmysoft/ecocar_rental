<?php


Breadcrumbs::register('admin.auth.user.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('labels.backend.access.users.management'), route('admin.auth.user.index'));
});

Breadcrumbs::register('admin.auth.user.deactivated', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.auth.user.index');
    $breadcrumbs->push(__('menus.backend.access.users.deactivated'), route('admin.auth.user.deactivated'));
});

Breadcrumbs::register('admin.auth.user.deleted', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.auth.user.index');
    $breadcrumbs->push(__('menus.backend.access.users.deleted'), route('admin.auth.user.deleted'));
});

Breadcrumbs::register('admin.auth.user.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.auth.user.index');
    $breadcrumbs->push(__('labels.backend.access.users.create'), route('admin.auth.user.create'));
});

Breadcrumbs::register('admin.auth.user.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.auth.user.index');
    $breadcrumbs->push(__('menus.backend.access.users.view'), route('admin.auth.user.show', $id));
});

Breadcrumbs::register('admin.auth.user.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.auth.user.index');
    $breadcrumbs->push(__('menus.backend.access.users.edit'), route('admin.auth.user.edit', $id));
});

Breadcrumbs::register('admin.auth.user.change-password', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.auth.user.index');
    $breadcrumbs->push(__('menus.backend.access.users.change-password'), route('admin.auth.user.change-password', $id));
});

//
//---- Cars
//
Breadcrumbs::register('admin.car-management', function ($breadcrumbs) {
    $breadcrumbs->push(__('บริหารจัดการรถยนต์'), route('admin.car-management'));
});

Breadcrumbs::register('admin.cars.car.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.car-management');
    $breadcrumbs->push(__('รายการรถยนต์'), route('admin.cars.car.index'));
});

Breadcrumbs::register('admin.cars.car.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.car-management');
    $breadcrumbs->push(__('menus.backend.cars.writing.create'), route('admin.cars.car.create'));
});

Breadcrumbs::register('admin.cars.car.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.cars.car.index');
    $breadcrumbs->push(__('ข้อมูลรถยนต์'), route('admin.cars.car.show', $id));
});

Breadcrumbs::register('admin.cars.car.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.cars.car.index');
    $breadcrumbs->push(__('ปรับปรุงแก้ไขรายละเอียด'), route('admin.cars.car.edit', $id));
});

Breadcrumbs::register('admin.cars.car.destroy', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.cars.car.index');
    $breadcrumbs->push(__('ลบข้อมูล'), route('admin.cars.car.destroy', $id));
});


Breadcrumbs::register('admin.articles.articles.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('แสดงบทความทั้งหมด'), route('admin.articles.articles.index'));
});

Breadcrumbs::register('admin.articles.articles.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('สร้างบทความ'), route('admin.articles.articles.create'));
});


Breadcrumbs::register('admin.articles.articles.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('แก้ไขบทความ'), route('admin.articles.articles.edit', $id));
});



Breadcrumbs::register('admin.articles.articles.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('รายละเอียดบทความ'), route('admin.articles.articles.show', $id));
});


//
//---- Promotion
//
Breadcrumbs::register('admin.promotions.promotions.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('โปรโมชั่นทั้งหมด'), route('admin.promotions.promotions.index'));
});

Breadcrumbs::register('admin.promotions.promotions.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('สร้างโปรโมชั่น'), route('admin.promotions.promotions.create'));
});


Breadcrumbs::register('admin.promotions.promotions.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('แก้ไขโปรโมชั่น'), route('admin.promotions.promotions.edit', $id));
});

Breadcrumbs::register('admin.promotions.promotions.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('รายละเอียดโปรโมชั่น'), route('admin.promotions.promotions.show', $id));
});


//
//---- Booking Car
//


Breadcrumbs::register('admin.booking.car-booking.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('Booking Car'), route('admin.booking.car-booking.index'));
});

Breadcrumbs::register('admin.booking.car-booking.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('Create Booking'), route('admin.booking.car-booking.create'));
});

Breadcrumbs::register('admin.booking.car-booking.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.cars.car.index');
    $breadcrumbs->push(__('ข้อมูลการจอง'), route('admin.booking.car-booking.show', $id));
});

Breadcrumbs::register('admin.booking.car-booking.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.cars.car.index');
    $breadcrumbs->push(__('ปรับปรุงแ้กไขรายละเอียดการจอง'), route('admin.booking.car-booking.edit', $id));
});

Breadcrumbs::register('admin.booking.car-booking.destroy', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.cars.car.index');
    $breadcrumbs->push(__('ลบข้อมูล'), route('admin.booking.car-booking.destroy', $id));
});


Breadcrumbs::register('admin.booking.return-car.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('คืนรถ'), route('admin.booking.return-car.index'));
});

Breadcrumbs::register('admin.booking.return-car.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('บันทึกการคืนรถ'), route('admin.booking.return-car.create'));
});

Breadcrumbs::register('admin.booking.return-car.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.cars.car.index');
    $breadcrumbs->push(__('ข้อมูลรถ'), route('admin.booking.return-car.show', $id));
});

Breadcrumbs::register('admin.booking.return-car.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.cars.car.index');
    $breadcrumbs->push(__('แก้ไขการคืนรถ'), route('admin.booking.return-car.edit', $id));
});


//
//---- Customer
//

Breadcrumbs::register('admin.customers', function ($breadcrumbs) {
    $breadcrumbs->push(__('Customers Management'), route('admin.customer.management.index'));
});

Breadcrumbs::register('admin.customer.management.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.customers');
    $breadcrumbs->push(__('All Customers'), route('admin.customer.management.index'));
});

Breadcrumbs::register('admin.customer.management.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.customers');
    $breadcrumbs->push(__('Create'), route('admin.customer.management.create'));
});

Breadcrumbs::register('admin.customer.management.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.customers');
    $breadcrumbs->push(__('ข้อมูลลูกค้า'), route('admin.customer.management.show', $id));
});

Breadcrumbs::register('admin.customer.management.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.customers');
    $breadcrumbs->push(__('ปรับปรุงแ้กไขรายละเอียดลูกค้า'), route('admin.customer.management.edit', $id));
});

Breadcrumbs::register('admin.customer.management.destroy', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.customers');
    $breadcrumbs->push(__('ลบข้อมูล'), route('admin.customer.management.destroy', $id));
});


//
//---- Prices
//

Breadcrumbs::register('admin.prices', function ($breadcrumbs) {
    $breadcrumbs->push(__('รายการราคาเช่ารถ'), route('admin.prices'));
});

Breadcrumbs::register('admin.prices.car-prices.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.prices');
    $breadcrumbs->push(__('รายการราคาเช่ารถ'), route('admin.prices.car-prices.index'));
});

Breadcrumbs::register('admin.prices.car-prices.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.prices');
    $breadcrumbs->push(__('สร้างรายการราคาเช่ารถ'), route('admin.prices.car-prices.create'));
});

Breadcrumbs::register('admin.prices.car-prices.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.prices');
    $breadcrumbs->push(__('ข้อมูลราคาเช่ารถ'), route('admin.prices.car-prices.show', $id));
});

Breadcrumbs::register('admin.prices.car-prices.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.prices');
    $breadcrumbs->push(__('ปรับปรุงแ้กไขรายละเอียดข้อมูล'), route('admin.prices.car-prices.edit', $id));
});

Breadcrumbs::register('admin.prices.car-prices.destroy', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.prices');
    $breadcrumbs->push(__('ลบข้อมูล'), route('admin.prices.car-prices.destroy', $id));
});



/////////////////////////////////
////// ตั้งค่าเว็บ  ///
/////////////////////////////////
Breadcrumbs::register('admin.config.webconfig.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('เติมคำแบบมีตัวเลือก'), route('admin.config.webconfig.index'));
});
Breadcrumbs::register('admin.config.webconfig.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('สร้างเติมคำแบบมีตัวเลือก'), route('admin.config.webconfig.create'));
});

Breadcrumbs::register('admin.config.webconfig.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('แสดงรายละเอียดคำถาม'), route('admin.config.webconfig.show', $id));
});



/////////////////////////////////
//////  ประเภทเมนู  ///
/////////////////////////////////
Breadcrumbs::register('admin.menu.menu-type.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('ประเภทเมนู'), route('admin.config.webconfig.index'));
});
Breadcrumbs::register('admin.menu.menu-type.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('สร้างประเภทเมนู'), route('admin.config.webconfig.create'));
});

Breadcrumbs::register('admin.menu.menu-type.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('แสดงรายละเอียดประเภทเมนู'), route('admin.config.webconfig.show', $id));
});

Breadcrumbs::register('admin.menu.menu-type.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.prices');
    $breadcrumbs->push(__('ปรับปรุงแ้กไขรายละเอียดข้อมูล'), route('admin.prices.car-prices.edit', $id));
});


/////////////////////////////////
//////  เมนู  ///
/////////////////////////////////
Breadcrumbs::register('admin.menu.menu-items.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('ประเภทเมนู'), route('admin.config.webconfig.index'));
});
Breadcrumbs::register('admin.menu.menu-items.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('สร้างประเภทเมนู'), route('admin.config.webconfig.create'));
});

Breadcrumbs::register('admin.menu.menu-items.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('แสดงรายละเอียดประเภทเมนู'), route('admin.config.webconfig.show', $id));
});

Breadcrumbs::register('admin.menu.menu-items.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.prices');
    $breadcrumbs->push(__('ปรับปรุงแ้กไขรายละเอียดข้อมูล'), route('admin.prices.car-prices.edit', $id));
});


/////////////////////////////////
//////  เงินมัดจำ  ///
/////////////////////////////////
Breadcrumbs::register('admin.deposit.deposit-car.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('รายการเงินมัดจำ'), route('admin.deposit.deposit-car.index'));
});
Breadcrumbs::register('admin.deposit.deposit-car.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('สร้างข้อมูล'), route('admin.deposit.deposit-car.create'));
});

Breadcrumbs::register('admin.deposit.deposit-car.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('แสดงรายละเอียด'), route('admin.deposit.deposit-car.show', $id));
});

Breadcrumbs::register('admin.deposit.deposit-car.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.prices');
    $breadcrumbs->push(__('ปรับปรุงแ้กไขรายละเอียดข้อมูล'), route('admin.deposit.deposit-car.edit', $id));
});










/////////////////////////////////
//////  TAG  ///
/////////////////////////////////
Breadcrumbs::register('admin.tag.tag-list.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('TAG'), route('admin.tag.tag-list.index'));
});
Breadcrumbs::register('admin.tag.tag-list.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('TAG'), route('admin.tag.tag-list.create'));
});

Breadcrumbs::register('admin.tag.tag-list.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('TAG'), route('admin.tag.tag-list.show', $id));
});

Breadcrumbs::register('admin.tag.tag-list.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.prices');
    $breadcrumbs->push(__('TAG'), route('admin.tag.tag-list.edit', $id));
});


/////////////////////////////////
//////  TAG  ///
/////////////////////////////////
Breadcrumbs::register('admin.category.category-list.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('Category'), route('admin.category.category-list.index'));
});
Breadcrumbs::register('admin.category.category-list.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('Category'), route('admin.category.category-list.create'));
});

Breadcrumbs::register('admin.category.category-list.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('Category'), route('admin.category.category-list.show', $id));
});

Breadcrumbs::register('admin.category.category-list.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.prices');
    $breadcrumbs->push(__('Category'), route('admin.category.category-list.edit', $id));
});


