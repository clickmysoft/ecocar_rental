<?php

if($user->total_additional_listings == 0){
	header("Location:useroptions");
	die();
}
?>



<style>
	input[type="text"], .form-control{
		background-color:#fff !important;
	}
	
	.js .inputfile {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
	}
	.inputfile + label {
	    max-width: 80%;
	    font-size: 1.25rem;
	    /* 20px */
	    font-weight: 700;
	    text-overflow: ellipsis;
	    white-space: nowrap;
	    cursor: pointer;
	    display: inline-block;
	    overflow: hidden;
	    padding: 0.625rem 1.25rem;
	    /* 10px 20px */
	}
	
	.no-js .inputfile + label {
	    display: none;
	}
	
	.inputfile:focus + label,
	.inputfile.has-focus + label {
	    outline: 1px dotted #000;
	    outline: -webkit-focus-ring-color auto 5px;
	}
	
	.inputfile + label * {
	    /* pointer-events: none; */
	    /* in case of FastClick lib use */
	}
	
	.inputfile + label svg {
	    width: 1em;
	    height: 1em;
	    vertical-align: middle;
	    fill: currentColor;
	    margin-top: -0.25em;
	    /* 4px */
	    margin-right: 0.25em;
	    /* 4px */
	}
	
	
	
	/* style 4 */
	
	.inputfile-4 + label {
	    color: #d3394c;
	}
	
	.inputfile-4:focus + label,
	.inputfile-4.has-focus + label,
	.inputfile-4 + label:hover {
	    color: #722040;
	}
	
	.inputfile-4 + label figure {
	    width: 100px;
	    height: 100px;
	    border-radius: 50%;
	    background-color: #d3394c;
	    display: block;
	    padding: 20px;
	    margin: 0 auto 10px;
	}
	
	.inputfile-4:focus + label figure,
	.inputfile-4.has-focus + label figure,
	.inputfile-4 + label:hover figure {
	    background-color: #722040;
	}
	
	.inputfile-4 + label svg {
	    width: 100%;
	    height: 100%;
	    fill: #f1e5e6;
	}
	
	div[class^='toggle-editor btn-toolbar pull-right clearfix']{
		display: none;
	}
	iframe#description_ifr{
		height: 185px !important;
	}
	/* style 5 */
	
	.inputfile-5 + label {
	    color: #d3394c;
	}
	
	.inputfile-5:focus + label,
	.inputfile-5.has-focus + label,
	.inputfile-5 + label:hover {
	    color: #722040;
	}
	
	.inputfile-5 + label figure {
	    width: 100px;
	    height: 135px;
	    background-color: #d3394c;
	    display: block;
	    position: relative;
	    padding: 30px;
	    margin: 0 auto 10px;
	}
	
	.inputfile-5:focus + label figure,
	.inputfile-5.has-focus + label figure,
	.inputfile-5 + label:hover figure {
	    background-color: #722040;
	}
	
	.inputfile-5 + label figure::before,
	.inputfile-5 + label figure::after {
	    width: 0;
	    height: 0;
	    content: '';
	    position: absolute;
	    top: 0;
	    right: 0;
	}
	
	.inputfile-5 + label figure::before {
	    border-top: 20px solid #dfc8ca;
	    border-left: 20px solid transparent;
	}
	
	.inputfile-5 + label figure::after {
	    border-bottom: 20px solid #722040;
	    border-right: 20px solid transparent;
	}
	
	.inputfile-5:focus + label figure::after,
	.inputfile-5.has-focus + label figure::after,
	.inputfile-5 + label:hover figure::after {
	    border-bottom-color: #d3394c;
	}
	
	.inputfile-5 + label svg {
	    width: 100%;
	    height: 100%;
	    fill: #f1e5e6;
	}
	
	
	/* style 6 */
	
	.inputfile-6 + label {
	    color: #d3394c;
	}
	
	.inputfile-6 + label {
	    border: 1px solid #d3394c;
	    background-color: #f1e5e6;
	    padding: 0;
	}
	
	.inputfile-6:focus + label,
	.inputfile-6.has-focus + label,
	.inputfile-6 + label:hover {
	    border-color: #722040;
	}
	
	.inputfile-6 + label span,
	.inputfile-6 + label strong {
	    padding: 0.625rem 1.25rem;
	    /* 10px 20px */
	}
	
	.inputfile-6 + label span {
	    width: 200px;
	    min-height: 2em;
	    display: inline-block;
	    text-overflow: ellipsis;
	    white-space: nowrap;
	    overflow: hidden;
	    vertical-align: top;
	}
	
	.inputfile-6 + label strong {
	    height: 100%;
	    color: #f1e5e6;
	    background-color: #d3394c;
	    display: inline-block;
	}
	
	.inputfile-6:focus + label strong,
	.inputfile-6.has-focus + label strong,
	.inputfile-6 + label:hover strong {
	    background-color: #722040;
	}
	
	@media screen and (max-width: 50em) {
		.inputfile-6 + label strong {
			display: block;
		}
	}

	
	.panel-body,.panel{
		 margin-bottom: 0px !important;	
		     background-color: rgb(245, 245, 245) !important;
		         padding-top: 0 !important;
	}
</style>
    


					
    
<div style="border: 1px solid #e3e3e3;-webkit-border-radius: 4px;padding: 19px;">
<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		if (task == 'company.cancel' || task == 'company.aprove' || task == 'company.disaprove' || !validateCmpForm()){
			Joomla.submitform(task, document.getElementById('item-form'));
		}
	}
	window.addEvent('domready', function() {
		SqueezeBox.initialize({});
		SqueezeBox.assign($$('a.modal_jform_created_by'), {
			parse: 'rel'
		});
	});
	function jSelectUser_jform_created_by(id, title) {
		var old_id = document.getElementById("userId").value;
		if (old_id != id) {
			document.getElementById("userId").value = id;
			document.getElementById("userName").value = title;
		}
		SqueezeBox.close();
	}
</script>

<?php $user = JFactory::getUser(); ?>
<?php
// echo "<pre>";
// print_r($this->item);
// echo "</pre>";
if(isset($isProfile) && !$showSteps) { ?>
	<div class="button-row" style="display:none;">
		<button type="button" class="ui-dir-button ui-dir-button-green" onclick="saveCompanyInformation();">
				<span class="ui-button-text"><i class="dir-icon-edit"></i> <?php echo JText::_("LNG_SAVE")?></span>
		</button>
		<button type="button" class="ui-dir-button ui-dir-button-grey" onclick="cancel()">
				<span class="ui-button-text"><i class="dir-icon-remove-sign red"></i> <?php echo JText::_("LNG_CANCEL")?></span>
		</button>
	</div>

	<div class="clear"></div>
<?php
} else if(isset($this->claimDetails) && !isset($isProfile)) { ?>
	<div id="claim-details" class="claim-details">
		<p><?php echo JText::_("LNG_CLAIM_DETAILS_TEXT")?></p>
		<table>
			<tr>
				<th><?php echo JText::_('LNG_FIRST_NAME')?></th>
				<td><?php echo $this->claimDetails->firstName ?></td>
			</tr>
			<tr>
				<th><?php echo JText::_('LNG_LAST_NAME')?></th>
				<td><?php echo $this->claimDetails->lastName ?></td>
			</tr>
			<tr>
				<th><?php echo JText::_('LNG_FUNCTION')?></th>
				<td><?php echo $this->claimDetails->function ?></td>
			</tr>
			<tr>
				<th><?php echo JText::_('LNG_PHONE')?></th>
				<td><?php echo $this->claimDetails->phone ?></td>
			</tr>
			<tr>
				<th><?php echo JText::_('LNG_EMAIL_ADDRESS')?></th>
				<td><?php echo $this->claimDetails->email ?></td>
			</tr>
		</table>
		<p><?php echo JText::_("LNG_USER_DETAILS_TXT")?></p>
		<?php $claimUser = JFactory::getUser($this->item->userId); ?>
		<table>
			<tr>
				<th><?php echo JText::_('LNG_FIRST_NAME')?></th>
				<td><?php echo $claimUser->name ?></td>
			</tr>
			<tr>
				<th><?php echo JText::_('LNG_USERNAME')?></th>
				<td><?php echo $claimUser->username ?></td>
			</tr>
			<tr>
				<th><?php echo JText::_('LNG_EMAIL')?></th>
				<td><?php echo $claimUser->email ?></td>
			</tr>
		</table>
	</div>
<?php
} ?>

<?php
if($showSteps) { ?>
	<div id="process-container" class="process-container">
		<div class="process-steps-wrapper">
			<div id="process-steps-container" class="process-steps-container">
				<div class="main-process-step completed" >
					<div class="process-step-number">1</div>
					<?php echo JText::_("LNG_CHOOSE_PACKAGE")?>
				</div>
				<div class="main-process-step completed">
					<div class="process-step-number">2</div>
					<?php echo JText::_("LNG_BASIC_INFO")?>
				</div>
				<div class="main-process-step">
					<div class="process-step-number">3</div>
					<?php echo JText::_("LNG_LISTING_INFO")?>
				</div>
			</div>
			<div class="meter">
				<span style="width: 70%"></span>
			</div>
		</div>
		<div class="clear"></div>
	</div>
<?php
} ?>

<div class="category-form-container">
	<div class="clr mandatory" style="display:none;">
		<p><?php echo JText::_("LNG_REQUIRED_INFO")?></p>
	</div>
	<?php
	if ($showSteps) { ?>
		<div id="process-steps" class="process-steps" style="display:none">
			<div id="step1" class="process-step">
				<div class="step">
					<div class="step-number">1</div>
				</div>
				<?php echo JText::_("LNG_STEP_1")?>
			</div>
			<div id="step2" class="process-step">
				<div class="step">
					<div class="step-number">2</div>
				</div>
				<?php echo JText::_("LNG_STEP_2")?>
			</div>
			<div id="step3" class="process-step">
				<div class="step">
					<div class="step-number">3</div>
				</div>
				<?php echo JText::_("LNG_STEP_3")?>
			</div>
			<div id="step4" class="process-step">
				<div class="step">
					<div class="step-number">4</div>
				</div>
				<?php echo JText::_("LNG_STEP_4")?>
			</div>
			<?php
			if(((!$enablePackages || (!empty($this->item->package->features) && in_array(SOCIAL_NETWORKS,$this->item->package->features)))
				|| (!$enablePackages || (!empty($this->item->package->features) && in_array(VIDEOS,$this->item->package->features)))
				|| (!$enablePackages || (!empty($this->item->package->features) && in_array(IMAGE_UPLOAD,$this->item->package->features))))){ ?>
				<div id="step5" class="process-step">
					<div class="step">
						<div class="step-number">5</div>
					</div>
					<?php echo JText::_("LNG_STEP_5")?>
				</div>
			<?php
			} ?>
			<div id="steps-info" class="steps-info">
				<div id="active-step" class="step active-step">
					<div class="step-number" id="active-step-number">1</div>
				</div>
				<div class="step-divider">/</div>
				<div id="step" class="step">
					<div id="max-tabs" class="step-number">5</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div id="process-tabs"  class="process-tabs">
			<div id="tab1" class="process-tab">
				<?php echo JText::_("LNG_TAB_1")?>
			</div>
			<div id="tab2" class="process-tab">
				<?php echo JText::_("LNG_TAB_2")?>
			</div>
			<div id="tab3" class="process-tab">
				<?php echo JText::_("LNG_TAB_3")?>
			</div>
			<div id="tab4" class="process-tab">
				<?php echo JText::_("LNG_TAB_4")?>
			</div>
			<?php  if(((!$enablePackages || (!empty($this->item->package->features) && in_array(SOCIAL_NETWORKS,$this->item->package->features)))
			    || (!$enablePackages || (!empty($this->item->package->features) && in_array(VIDEOS,$this->item->package->features)))
				||(!$enablePackages || (!empty($this->item->package->features) && in_array(IMAGE_UPLOAD,$this->item->package->features))))){ ?>

				<div id="tab5" class="process-tab">
					<?php echo JText::_("LNG_TAB_5")?>
				</div>
			<?php
			} ?>
		</div>
	<?php } ?>

	<?php
		$db04 = JFactory::getDBO();
		$query04 = ' SELECT * FROM #__jbusinessdirectory_packages';
		$db04->setQuery($query04); 
		$DBorders04= $db04->loadObjectList();


		
	
	?>

	<div class="edit-container">
		<form  action="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="item-form" class="form-horizontal">
			<?php
			if($enablePackages && !$showSteps) { ?>
				<div class="panel panel-default fie_wid">
						<div class="panel-heading" style="font-size: 14px;line-height: 22px;display:none;"><strong><h3>Package</h3></strong>
				
							<div class="package_content panel-body">
								<select name="filter_package" class="inputbox input-medium" style="   width: 100%;" onchange="this.form.submit()">
									<?php
											foreach($DBorders04 as $key => $value) {
									?>
												<option value="<?=$value->id?>" <?=$this->state->get('company.packageId')==$value->id?'selected':''?>><?=$value->name?></option>
									<?php
											}
									?>	

									<?php //echo JHtml::_('select.options', $this->packageOptions, 'value', 'text', $this->state->get('company.packageId'));?>
								</select>
								<p>
								<?php echo JText::_('LNG_CURRENT_PACKAGE')?>: <?php echo $this->item->package->name ?><br/>
									<?php
									if(isset($this->item->paidPackage)){ ?>
										<?php echo JText::_('LNG_STATUS')?>: <?php echo !$this->item->paidPackage->expired ? JText::_("LNG_VALID"): JText::_("LNG_EXPIRED") ?>
										<br/>
										<?php echo JText::_('LNG_START_DATE')?>: <?php echo JBusinessUtil::getDateGeneralFormat($this->item->paidPackage->start_date) ?> <br/>
										<?php echo JText::_('LNG_EXPIRATION_DATE')?>: <?php echo JBusinessUtil::getDateGeneralFormat($this->item->paidPackage->expirationDate) ?><a href="javascript:extendPeriod()"> <?php echo JText::_("LNG_EXTEND_PERIOD")?></a>
									<?php
									} else {?>
										<?php echo JText::_('LNG_STATUS')?>: <?php echo $this->item->package->price == 0? JText::_("LNG_FREE"):JText::_("LNG_NOT_PAID") ?>
									<?php
									} ?>
								</p>
								<?php
								if(!isset($this->item->paidPackage) && isset($this->item->lastActivePackage)) { ?>
									<div class="package-info">
										<?php echo JText::_('LNG_LAST_PAID_PACKAGE')?>: <?php echo $this->item->lastActivePackage->name ?><br/>
										<?php echo JText::_('LNG_STATUS')?>: <?php echo !$this->item->lastActivePackage->expired ? JText::_("LNG_VALID"): JText::_("LNG_EXPIRED") ?><br/>
										<?php echo JText::_('LNG_START_DATE')?>: <?php echo JBusinessUtil::getDateGeneralFormat($this->item->lastActivePackage->start_date) ?> <br/>
										<?php echo JText::_('LNG_EXPIRATION_DATE')?>: <?php echo JBusinessUtil::getDateGeneralFormat($this->item->lastActivePackage->expirationDate) ?> <a href="javascript:extendPeriod()"> <?php echo JText::_("LNG_EXTEND_PERIOD")?></a>
									</div>
								<?php
								} ?>
							</div>
			


			<?php
			} else {
				$packageId = JRequest::getVar("filter_package");
				if(empty($packageId) && !empty($this->item->package)){
					$packageId = $this->item->package->id;
				}
			?>
				<input type="hidden" name="filter_package" id="filter_package" value="<?php echo $packageId ?>"/>
			<?php
			} ?>

		</div>

		<br><br>

			<div id="edit-tab1" class="edit-tab">
				<div class="panel panel-default fie_wid">
					<div class="panel-heading">
						<strong>
							<h3>
								<i class="fa fa-user-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;
									<?php echo "Your Account"//JText::_('LNG_COMPANY_DETAILS');?>
							</h3>
						</strong>
					</div>

					<div class="package_content panel-body">
						<div class="box-info">
										<!-- /.box-header -->
										<!-- form start -->
										<?php
											$db3 = JFactory::getDBO();
											$query3 = ' SELECT * FROM #__jbusinessdirectory_companies 
												WHERE #__jbusinessdirectory_companies.userId= "'.$user->id.'"';
											$db3->setQuery($query3); 
											$DBorders3= $db3->loadObjectList();

										?>
										<div class="form-horizontal">
											<div class="box-body">
												<div class="form-box" style="margin-left: 44px !important;">
												<div class="detail_box">
														<div  class="form-detail req"></div>
														<label for="name">Notification Email</label>
														<input type="text"class="input_txt validate[required] text-input" value="<?php echo $user->email ?>" onchange="checkCompanyName('<?php echo $this->item->name ?>',this.value)" maxlength="100">
														<div class="clear"></div>
														<p class="small">Please enter an email address you would like associated with this profile.</p>
													</div>

													<div class="detail_box">
														<div  class="form-detail req"></div>
														<label for="name"><?php echo JText::_('LNG_COMPANY_NAME')?> </label>
														<input type="text"	name="name" id="name" class="input_txt validate[required] text-input" value="<?=$this->item->name==null?$user->name_company_input:$this->item->name ?>" onchange="checkCompanyName('<?php echo $this->item->name ?>',this.value)" maxlength="100">
														<div class="clear"></div>
														<span class="error_msg" id="company_exists_msg" style="display: none;"><?php echo JText::_('LNG_COMPANY_NAME_ALREADY_EXISTS')?></span>
														<span class="" id="claim_company_exists_msg" style="display: none;"><?php echo JText::_('LNG_CLAIM_COMPANY_EXISTS')?> <a id="claim-link" href=""><?php echo JText::_("LNG_HERE")?></a></span>
														<span class="error_msg" id="frmCompanyName_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
													</div>
													<div class="detail_box" style="display:none;">
														<label for="name"><?php echo JText::_('LNG_ALIAS')?> </label>
														<input type="text"	name="alias" id="alias"  placeholder="<?php echo JText::_('LNG_AUTO_GENERATE_FROM_NAME')?>" class="input_txt text-input" value="<?php echo $this->item->alias ?>">
														<div class="clear"></div>
													</div>

													<?php
													if($attributeConfig["company_type"]!=ATTRIBUTE_NOT_SHOW) { ?>
														<div class="detail_box" style="display:none;">
															<?php if($attributeConfig["company_type"] == ATTRIBUTE_MANDATORY){?>
																<div  class="form-detail req"></div>
															<?php }?>
															<label for="companyTypes"><?php echo JText::_('LNG_COMPANY_TYPE')?> </label>
															<select class="input_sel <?php echo $attributeConfig["company_type"] == ATTRIBUTE_MANDATORY?"validate[required]":""?> select" name="typeId" id="companyTypes">
																<option  value=""> </option>
																<?php
																foreach( $this->item->types as $type ) { ?>
																	<option <?php echo $type->name=='Company'? "selected" : ""?> value='<?php echo $type->id?>'><?php echo $type->name ?></option>
																<?php
																} ?>
															</select>
															<div class="clear"></div>
															<span class="error_msg" id="frmCompanyType_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
														</div>
													<?php
													} ?>




													<?php
				if($attributeConfig["logo"]!=ATTRIBUTE_NOT_SHOW){?>
					<?php
					if(!$enablePackages || isset($this->item->package->features) && in_array(SHOW_COMPANY_LOGO,$this->item->package->features)) { ?>

							<label for="company_logo"><?php echo JText::_('LNG_ADD_LOGO')?> </label>

							<div class="box-info" >

											<!-- /.box-header -->
											<!-- form start -->
									<div class="form-horizontal">
									    <div class="box-body">
									        <div class="form-box" style="margin-left: 0px !important;">
									            <div class="form-upload-elem">
									                <div class="form-upload">
									                   	<small class="small_note"><?php echo JText::_('LNG_ADD_LOGO_TEXT');?></small>
									                    <p class="hint">
									                        <?php echo JText::_( 'LNG_LOGO_MAX_SIZE');?>
									                    </p>
									                    <input type="hidden" name="logoLocation" id="imageLocation" value="<?php echo $this->item->logoLocation?>">
									                    <input type="hidden" id="MAX_FILE_SIZE" value="2097152" name="MAX_FILE_SIZE">
									                    
									                    
									                    <input type="file" name="uploadLogo" id="imageUploader" size="50" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple / style=" visibility: hidden;display: none; ">
														<label for="imageUploader"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
															<path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <?php echo JText::_('LNG_PLEASE_CHOOSE_A_FILE'); ?></strong></label>
									                    
									                    
									                    <div class="clear"></div>
									                   
									                 
									                </div>
									                
									                <div class="info">
									                    <?php if($attributeConfig[ "logo"]==ATTRIBUTE_OPTIONAL){ ?>
									                    	
									                    <div class="info-box">
									                        <?php echo JText::_( 'LNG_ADD_LOGO_CONTINUE');?>
									                    </div>
									                    
									                    <?php } ?>
									                </div>
									                
									            </div>
									            
									            <div class="picture-preview" id="picture-preview">
									                <?php if(!empty($this->item->logoLocation)){
									                	
														 echo "<img src='".JURI::root().PICTURES_PATH.$this->item->logoLocation."' />"; 
											
													?>
													
														   <a href="javascript:removeLogo()">
									                        	<?php echo JText::_( "LNG_REMOVE_LOGO")?>
									                    	</a>
													<?php
													
													} ?>
												
									            </div>
									            	
									           		
									            <div class="clear"></div>
									            <span class="error_msg" id="frmCompanyName_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
									        </div>
									    </div>
									</div>
									
								<br>
							</div>
						
					<?php
					} ?>
				<?php
				} ?>

				<?php
													if($attributeConfig["short_description"]!=ATTRIBUTE_NOT_SHOW) { ?>
														<div class="detail_box">
															<?php if($attributeConfig["short_description"] == ATTRIBUTE_MANDATORY){?>
																<div  class="form-detail req"></div>
															<?php }?>
															<label for="description_id">Awards</label>
															<?php
															if($this->appSettings->enable_multilingual) {
																echo JHtml::_('tabs.start', 'tab_groupsd_id', $options);
																foreach( $this->languages  as $k=>$lng ){
																	echo JHtml::_('tabs.panel', $lng, 'tab'.$k );
																	$langContent = isset($this->translations[$lng."_short"])?$this->translations[$lng."_short"]:"";
																	if($lng==JFactory::getLanguage()->getDefault() && empty($langContent)) {
																		$langContent = $this->item->short_description;
																	}
																	echo "<textarea id='short_description'.$lng' name='short_description_$lng' class='input_txt' cols='75' rows='5' maxLength='".COMPANY_SHORT_DESCRIPTIION_MAX_LENGHT."'>$langContent</textarea>";
																	echo "<div class='clear'></div>";
																}
																echo JHtml::_('tabs.end');
															} else { ?>
																<textarea name="short_description" id="short_description" class=" input_txt <?php echo $attributeConfig["short_description"] == ATTRIBUTE_MANDATORY?"validate[required]":""?> text-input"  cols="75" rows="5"  maxLength="<?php echo COMPANY_SHORT_DESCRIPTIION_MAX_LENGHT?>" onkeyup="calculateLenghtShort();"><?php echo $this->item->short_description ?></textarea>
																<div class="clear"></div>
																<span class="error_msg" id="frmDescription_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
																<div class="description-counter" style="display:none;">
																	<input type="hidden" name="descriptionMaxLenghtShort" id="descriptionMaxLenghtShort" value="<?php echo COMPANY_SHORT_DESCRIPTIION_MAX_LENGHT?>" />
																	<label for="decriptionCounterShort">(Max. <?php echo COMPANY_SHORT_DESCRIPTIION_MAX_LENGHT?> <?php JText::_('LNG_CHARACTRES')?>).</label>
																	<?php echo JText::_('LNG_REMAINING')?>&nbsp;&nbsp;<input type="text" value="0" id="descriptionCounterShort" name="descriptionCounterShort">
																</div>
															<?php
															} ?>
														</div>
													<?php
													} ?>

													


													<?php
													if($attributeConfig["comercial_name"]!=ATTRIBUTE_NOT_SHOW){?>
														<div class="detail_box">
															<?php if($attributeConfig["comercial_name"] == ATTRIBUTE_MANDATORY){?>
																<div  class="form-detail req"></div>
															<?php }?>
															<label for="comercialName"><?php echo JText::_('LNG_COMPANY_COMERCIAL_NAME')?> </label>
															<input type="text"
																name="comercialName" id="comercialName" class="input_txt <?php echo $attributeConfig["comercial_name"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->comercialName ?>">
															<div class="clear"></div>
															<span class="error_msg" id="frmCompanyComercialName_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
														</div>
													<?php
													} ?>
													<?php
													if($attributeConfig["tax_code"]!=ATTRIBUTE_NOT_SHOW){ ?>
														<div class="detail_box" style="display:none">
															<?php if($attributeConfig["tax_code"] == ATTRIBUTE_MANDATORY){?>
																<div  class="form-detail req"></div>
															<?php }?>
															<label for="taxCode"><?php echo JText::_('LNG_TAX_CODE')?> </label>
															<input type="text" name="taxCode" id="taxCode" class="input_txt <?php echo $attributeConfig["tax_code"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->taxCode ?>">
															<div class="clear"></div>
															<span class="error_msg" id="frmTaxCode_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
														</div>
													<?php
													} ?>
													<?php
													if($attributeConfig["registration_code"]!=ATTRIBUTE_NOT_SHOW) { ?>
														<div class="detail_box">
															<?php if($attributeConfig["registration_code"] == ATTRIBUTE_MANDATORY){?>
																<div  class="form-detail req"></div>
															<?php }?>
															<label for="registrationCode"><?php echo JText::_('LNG_REGISTRATION_CODE')?> </label>
															<input type="text"
																name="registrationCode" id="registrationCode" class="input_txt <?php echo $attributeConfig["registration_code"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" 	value="<?php echo $this->item->registrationCode ?>">
															<div class="clear"></div>
															<span class="error_msg" id="frmRegistrationCode_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
														</div>
													<?php
													} ?>
													
													
													<?php
													if($attributeConfig["slogan"]!=ATTRIBUTE_NOT_SHOW) { ?>
														<div class="detail_box">
															<?php if($attributeConfig["slogan"] == ATTRIBUTE_MANDATORY){?>
																<div  class="form-detail req"></div>
															<?php }?>
															<label for="slogan"><?php echo JText::_("LNG_COMPANY_SLOGAN")?> &nbsp;&nbsp;&nbsp;</label>
															<p class="small"><?php echo JText::_("LNG_COMPANY_SLOGAN_INFO")?></p>
															<?php
															if($this->appSettings->enable_multilingual) {
																echo JHtml::_('tabs.start', 'tab_groupsd_id', $options);
																foreach($this->languages  as $k=>$lng ) {
																	echo JHtml::_('tabs.panel', $lng, 'tab'.$k );
																	$langContent = isset($this->translationsSlogan[$lng])?$this->translationsSlogan[$lng]:"";
																	if($lng==JFactory::getLanguage()->getDefault() && empty($langContent)){
																		$langContent = $this->item->slogan;
																	}
																	echo "<textarea id='slogan'.$lng' name='slogan_$lng' class='input_txt' cols='75' rows='5' maxLength='".COMPANY_SLOGAN_MAX_LENGHT."'>$langContent</textarea>";
																	echo "<div class='clear'></div>";
																}
																echo JHtml::_('tabs.end');
															} else { ?>
																<textarea name="slogan" id="slogan" class="input_txt text-input <?php echo $attributeConfig["slogan"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>"  cols="75" rows="5"  maxLength="<?php echo COMPANY_SLOGAN_MAX_LENGHT?>"><?php echo $this->item->slogan ?></textarea>
															<?php
															} ?>
														</div>
													<?php
													} ?>
													
													
													<?php
													if($attributeConfig["keywords"]!=ATTRIBUTE_NOT_SHOW) { ?>
														<div class="detail_box" style="display:none;">
															<?php if($attributeConfig["keywords"] == ATTRIBUTE_MANDATORY){?>
																<div  class="form-detail req"></div>
															<?php }?>
															<label for="keywords"><?php echo JText::_('LNG_KEYWORDS')?> </label>
															<p class="small"><?php echo JText::_('LNG_COMPANY_KEYWORD_INFO')?></p>
															<input	type="text" name="keywords" class="input_txt <?php echo $attributeConfig["keywords"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" id="keywords" value="<?php echo $this->item->keywords ?>"  maxLength="75">
														<div class="clear"></div>
												</div>
											<?php
											} ?>
										</div>
									</div>
									<!-- /.box-body -->
								</div>
							</div>
								<!-- box-info -->
					</div>
					<!-- panel-body -->
	</div>


<?php
	if($this->appSettings->enable_attachments == 0) { ?>
	<div class="panel panel-default fie_wid">
	  <div class="panel-heading">
	  		<strong><h3><i class="fa fa-file-archive-o" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('LNG_ATTACHMENTS');?></strong></h3> 
	  		<small><?php echo JText::_('LNG_ATTACHMENTS_INFORMATION_TEXT');?>.</small>
	  </div>
	  <div class="panel-body">
				
					<fieldset class="boxed ">
						<input type='button' name='btn_removefile_at' id='btn_removefile_at' value='x' style='display:none'>
						<input type='hidden' name='crt_pos_a' id='crt_pos_a' value=''>
						<input type='hidden' name='crt_path_a' id='crt_path_a' value=''>
						
						<TABLE class="admintable" align='center' id='table_company_attachments' name='table_company_attachments' >
										<?php
										if(!empty($this->item->attachments))
											foreach( $this->item->attachments as $attachment ) { ?>
												<TR>
											
													<TD align='left'>
														<input type="text" name='attachment_name[]' id='attachment_name'   value="<?php echo $attachment->name ?>" /><br/>
														<?php echo basename($attachment->path)?>
													</TD>
													<td align='center'>
														<img class='btn_attachment_delete'
															src='<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/del_options.gif"?>'
															onclick="
																if(!confirm('<?php echo JText::_('LNG_CONFIRM_DELETE_ATTACHMENT',true)?>'))
																	return;
																var row = jQuery(this).parents('tr:first');
																var row_idx = row.prevAll().length;
																jQuery('#crt_pos_a').val(row_idx);
																jQuery('#crt_path_a').val('<?php echo $attachment->path?>');
																jQuery('#btn_removefile_at').click();"/>
													</td>
													<td align='center'>
														<input type='hidden' value='<?php echo $attachment->status?>' name='attachment_status[]' id='attachment_status'>
														<input type='hidden' value='<?php echo $attachment->path?>' name='attachment_path[]' id='attachment_path'>
														<img class='btn_attachment_status'
															src='<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/".($attachment->status ? 'checked' : 'unchecked').".gif"?>'
															onclick="
																var form = document.adminForm;
																var v_status = null;
																var pos = jQuery(this).closest('tr')[0].sectionRowIndex;

																if( form.elements['attachment_status[]'].length == null ) {
																	v_status  = form.elements['attachment_status[]'];
																}
																else {
																	v_status  = form.elements['attachment_status[]'][pos];
																}
																if( v_status.value == '1') {
																	jQuery(this).attr('src', '<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/unchecked.gif"?>');
																	v_status.value ='0';
																}
																else {
																	jQuery(this).attr('src', '<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/checked.gif"?>');
																	v_status.value ='1';
																}" />
													</td>
													<td align="center">
														<span class="span_up" onclick='var row = jQuery(this).parents("tr:first");  row.insertBefore(row.prev());'>
															<img src="<?php echo JURI::root()?>administrator/components/<?php echo JBusinessUtil::getComponentName()?>/assets/img/up-icon.png">
														</span>
														<span class="span_down" onclick='var row = jQuery(this).parents("tr:first"); row.insertAfter(row.next());'>
															<img src="<?php echo JURI::root()?>administrator/components/<?php echo JBusinessUtil::getComponentName()?>/assets/img/down-icon.png">
														</span>
													</td>
												</TR>			
						</TABLE>
								
								
								
								
						<TABLE class='picture-table' align='left' border='0'>
							
							<TR>
								
										<input type="file" name="uploadAttachment" id="multiFileUploader" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple / style=" visibility: hidden;display: none; ">
										<label for="multiFileUploader"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <?php echo JText::_('LNG_PLEASE_CHOOSE_A_FILE'); ?></strong></label>
								
								
							</TR>
						</TABLE>
					</fieldset>
				<?php
				} ?>
			</div>


	</div>
	<?php
		} 
	?>


	<div id="edit-tab2" class="edit-tab">
				<?php if($attributeConfig["category"]!=ATTRIBUTE_NOT_SHOW){?>
					<div class="panel panel-default fie_wid">
							<div class="panel-heading" style="display:none;"> <h2> <i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('LNG_COMPANY_CATEGORIES');?></h2>
							<small class="small_note" style="display:none;"><?php echo JText::_('LNG_SELECT_CATEGORY');?></small> </div>
							<div class="package_content panel-body">

							<div class="box-info" style="padding: 0 15px">

											<?php
													$db = JFactory::getDBO();
													$query = ' SELECT `id`,`parent_id`,`level`,`name`,`published` FROM #__jbusinessdirectory_categories where published=1 ';
													$db->setQuery($query); 
													$categories= $db->loadObjectList();
													//echo "<pre>";
													//print_r($DBorders);
													//echo "</pre>";
											?>
											<!-- /.box-header -->
											<!-- form start -->
											<div class="form-horizontal">
												<div class="box-body">
													<div class="form-box" style="margin-left: 28px !important;">
														<div class="detail_box">
															<?php if($attributeConfig["category"] == ATTRIBUTE_MANDATORY){?>
																<div  class="form-detail req"></div>
															<?php }?>
														
															<label for="category">Industry</label>
																<div class="form-detail req"></div>
																<select name="maincategory" id="categories" class="validate[required] inputbox input-medium" required>
																	<option value=""><?php echo JText::_('LNG_SELECT_CAT');?></option>
																	<?php
																		foreach ($categories as $key => $value) {
																			if($value->level == 1){
																	?>
																				<option  <?=$value->id==$this->item->maincategory?'selected':''?> value="<?=$value->id?>"><?=$value->name?></option>
																	<?php
																			}
																		}
																	?>
																</select>
															<div class="clear"></div>
														</div>

														<div class="detail_box">
															<label for="category">Category</label>
															
															<div class="form-detail req"></div>
																<select name="selectedSubcategories[]" id="selectedSubcategories" class="validate[required] inputbox input-medium" multiple="multiple" required>
																	<?php
																		if($this->item->id != null){
																			foreach ($this->categoryOptions as $key => $value) {
																				if($value->value == $this->item->selCats[0]){
																	?>
																					<option value="<?=$value->value?>"><?=$value->text?></option>
																	<?php
																				}
																			}
																		}
																	?>
																	<?php //echo JHtml::_('select.options', $this->categoryOptions, 'value', 'text', $this->item->selCats);?>
																</select>
															<div class="clear"></div>
														</div>


														<div class="detail_box" style="display:none;">
															<?php if($attributeConfig["category"] == ATTRIBUTE_MANDATORY){?>
																<div  class="form-detail req"></div>
															<?php }?>
															<label for="subcat_main_id"><?php echo JText::_('LNG_MAIN_SUBCATEGORY');?></label>
															<div class="clear"></div>
															<select class="input_sel select <?php echo $attributeConfig["category"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" name="mainSubcategory" id="mainSubcategory">
															<?php foreach( $this->item->selectedCategories as $selectedCategory){?>
																		<option value="<?php echo $selectedCategory->id ?>" <?php echo $selectedCategory->id == $this->item->mainSubcategory ? "selected":"" ; ?>><?php echo $selectedCategory->name ?></option>
																<?php } ?>
															</select>
															<div class="clear"></div>
															<span class="error_msg" id="frmMainSubcategory_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
														</div>
													</div>
												</div>
											</div>
							</div>
						</div>
						<!-- panel-body -->
					<?php }?>
				</div>






				<?php
				if($this->appSettings->limit_cities == 1) { ?>
					<div class="panel panel-default fie_wid">
						  <div class="panel-heading">
								<strong><h3> <i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('LNG_ACTIVITY_CITIES');?></h3></strong>
								<small class="small_note"><?php echo JText::_('LNG_ACTIVITY_CITIES_INFO');?>.</small>
							</div>
							<div class="package_content panel-body">

							<div class="box-info" style="padding: 15px;">
							        <!-- /.box-header -->
							        <!-- form start -->
							<div class="form-horizontal">
							<div class="box-body">
							  <div class="form-box" style="margin-left: 28px !important;">
							<div class="form-box">
								<div class="detail_box">
									<label for="activity_cities"><?php echo JText::_('LNG_SELECT_ACTIVITY_CITY')?></label>
									<select multiple="multiple" id="activity_cities"  class="input_sel select" name="activity_cities[]">
										<option  value=""> </option>
										<?php
										foreach( $this->item->cities as $city ) {
											$selected = false;
											foreach($this->item->activityCities as $acity) {
												if($acity->city_id == $city->id)
													$selected = true;
											} ?>
											<option <?php echo $selected ? "selected" : ""?> value='<?php echo $city->id ?>'>
												<?php echo $city->name ?>
											</option>
										<?php
										} ?>
									</select>
									<a href="javascript:checkAllActivityCities()" class="select-all"><?php echo JText::_('LNG_CHECK_ALL',true)?></a>
									<a href="javascript:uncheckAllActivityCities()" class="deselect-all"><?php echo JText::_('LNG_UNCHECK_ALL',true)?></a>
									<div class="clear"></div>
								</div>
							</div></div></div></div></div></div>
						</div>
				<?php
				} ?>
			</div>

			
				<div class="panel panel-default fie_wid " style="display:none;">
					<div class="panel-heading fieldset-business_hours">
					<label for="business_hours"><i class="icon-plus-circle"></i><?php echo JText::_('LNG_OPENING_HOURS');?> 
					<small class="small_note">(<?php echo JText::_("LNG_OPTIONAL")?>)</small></label>
						<div class="field">
							<table>
								<tr>
									<th width="50%">&nbsp;</th>
									<th align="left"><?php echo JText::_("LNG_OPEN")?></th>
									<th align="left"><?php echo JText::_("LNG_CLOSE")?></th>
								</tr>

								<?php $dayNames = array(JText::_("MONDAY"),JText::_("TUESDAY"),JText::_("WEDNESDAY"),JText::_("THURSDAY"),JText::_("FRIDAY"),JText::_("SATURDAY"),JText::_("SUNDAY")); ?>
								<?php
								foreach($dayNames as $index=>$day){?>
									<tr>
										<td align="left"><?php echo $day?></td>
										<td align="left" class="business-hour"><input type="text" class="timepicker" name="business_hours[]" value="<?php echo !empty($this->item->business_hours)?$this->item->business_hours[$index*2]:"" ?>" class="regular-text"/></td>
										<td align="left" class="business-hour"><input type="text" class="timepicker" name="business_hours[]" value="<?php echo !empty($this->item->business_hours)?$this->item->business_hours[$index*2+1]:"" ?>" class="regular-text"/></td>
									</tr>
								<?php
								} ?>
							</table>
						</div>
					</div>
				</div>



				
				<?php
				if(!empty($this->item->customFields) && $this->item->containsCustomFields){?>
					    
					            <div class="package_content box-body">
					             
 										<div class="panel-heading" style='display: none;'>
				                        	<h3><i class="fa fa-address-card-o" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('LNG_ADDITIONAL_INFO');?></h3> <small class="small_note"><?php echo JText::_('LNG_ADDITIONAL_INFO_TEXT');?></small>
				                    	</div>

				                    <div class="panel-body">
				                        
				                    	<div class="box-info" style="padding: 0 15px 15px 15px;">

											<!-- /.box-header -->
											<!-- form start -->
											<div class="form-horizontal">
												<div class="box-body">
													<div class="form-box" style="margin-left: 28px !important;">
														<div id="custo_tanawat_kanrai">
														<div class="detail_box" style="    top: 9px;
    margin-bottom: 23px;">
															<div class="form-detail req"></div>
															<label for="fname">First Name </label>
															<input type="text" class="input_txt  text-input" value="<?=$user->name?>">
															<div class="clear"></div>
														</div>
														<div class="detail_box" >
															<label for="lname">Last Name </label>
															<input type="text"  class="input_txt text-input" value="<?=$user->lastname?>">
															<div class="clear"></div>
														</div>
													</div>
														<div class="detail_box"  id="custom_feide">

				                        <?php $packageFeatures=! empty($this->item->package->features)?$this->item->package->features:null; $renderedContent = AttributeService::renderAttributes($this->item->customFields, $enablePackages, $packageFeatures); echo $renderedContent; ?>
				                        </div></div></div></div>
				                    </div>
	 							
					            </div>
					   
				<?php
				} ?>
			</div>



			
			<div id="edit-tab3" class="edit-tab">
			<div class="panel panel-default fie_wid">
				<div class="panel-heading" style="display:none;">
					<h3>  <i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp;<stong><?php echo JText::_('LNG_COMPANY_LOCATION');?></stong></h3>
					<small class="small_note"><?php echo JText::_('LNG_COMPANY_LOCATION_TXT');?></small>&nbsp;&nbsp;&nbsp;
					<small class="small_note"><?php echo JText::_("LNG_ADDRESS_SUGESTION")?></small>
				</div>

				<div class="package_content panel-body">

				<div class="box-info" style="padding: 15px;">

								<!-- /.box-header -->
								<!-- form start -->
			<div class="form-horizontal">
				<div class="box-body">

					<div class="form-box" style="margin-left: 28px !important;" id="puk_ku_tong">
						
						<?php if($attributeConfig["address"]!=ATTRIBUTE_NOT_SHOW){?>
						<div class="detail_box">
							<?php if($attributeConfig["address"] == ATTRIBUTE_MANDATORY){?>
							<div  class="form-detail req req_input_section_address"></div>
						<?php }?>
							<label for="address_id"><?php echo JText::_('LNG_ADDRESS')?></label>
							<input type="text" name="address" id="route" placeholder="&#61447;" class="input_section_address txt_wat_address1 input_address_geo input_txt <?php echo $attributeConfig["address"] == ATTRIBUTE_MANDATORY?"validate[required]":""?> text-input" value="<?php echo $this->item->address==''?$user->address:$this->item->address ?>">
							<div class="clear"></div>
							<span class="error_msg" id="frmAddress_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
						</div>
						<?php } ?>

						<?php if($attributeConfig["country"]!=ATTRIBUTE_NOT_SHOW){?>

						<div class="detail_box" style="display:none;">
							<?php if($attributeConfig["country"] == ATTRIBUTE_MANDATORY){?>
								<div  class="form-detail req"></div>
							<?php }?>
							<label for="countryId"><?php echo JText::_('LNG_COUNTRY')?> </label>
							<div class="clear"></div>
							<select class="input_sel <?php echo $attributeConfig["country"] == ATTRIBUTE_MANDATORY?"validate[required]":""?> select" name="countryId" id="country" >
									<option value=''></option>
									<?php
										foreach($this->item->countries as $country ){
									?>
										<option <?php echo $country->country_name=='Australia'? "selected" : ""?>
											value='<?php echo $country->id?>'
										><?php echo $country->country_name ?></option>
									<?php
										}
									?>
							</select>
							<div class="clear"></div>
							<span class="error_msg" id="frmCountry_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
						</div>
						<?php } ?>

						<?php if($attributeConfig["city"]!=ATTRIBUTE_NOT_SHOW){?>

						<div class="detail_box">
							<?php if($attributeConfig["city"] == ATTRIBUTE_MANDATORY){?>
								<div  class="form-detail req req_input_section_address"></div>
							<?php }?>
							<label for="city_id"><?php echo JText::_('LNG_CITY')?> </label>
							<input class="input_section_address txt_wat_address2 input_address_geo input_txt <?php echo $attributeConfig["city"] == ATTRIBUTE_MANDATORY?"validate[required]":""?> text-input" type="text" name="city" id="locality" value="<?php echo $this->item->city==''?$user->city:$this->item->city ?>">
							<div class="clear"></div>
							<span class="error_msg" id="frmCity_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
						</div>
						<?php } ?>

						<?php if($attributeConfig["region"]!=ATTRIBUTE_NOT_SHOW){?>

						<div class="detail_box" id="districtContainer">
							<?php if($attributeConfig["region"] == ATTRIBUTE_MANDATORY){?>
								<div  class="form-detail req req_input_section_address"></div>
							<?php }?>
							<label for="district_id"><?php echo JText::_('LNG_COUNTY')?> </label>
							<input class="input_section_address txt_wat_address3 input_address_geo input_txt <?php echo $attributeConfig["region"] == ATTRIBUTE_MANDATORY?"validate[required]":""?> text-input" type="text" name="county" id="administrative_area_level_1" value="<?php echo $this->item->county==''?$user->state:$this->item->county ?>" />
							<div class="clear"></div>
							<span class="error_msg" id="frmDistrict_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
						</div>
						<?php } ?>

						<?php if($attributeConfig["postal_code"]!=ATTRIBUTE_NOT_SHOW){?>

						<div class="detail_box" id="districtContainer">
							<?php if($attributeConfig["postal_code"] == ATTRIBUTE_MANDATORY){?>
								<div  class="form-detail req req_input_section_address"></div>
							<?php }?>
							<label for="district_id"><?php echo JText::_('LNG_POSTAL_CODE')?> </label>
							<input class="input_section_address txt_wat_address4 input_address_geo input_sel input_txt  <?php echo $attributeConfig["postal_code"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" type="text" name="postalCode" id="postal_code" value="<?php echo $this->item->postalCode==''?$user->postcode:$this->item->postalCode ?>" />
							<div class="clear"></div>
						</div>
						<?php } ?>


						<div class="detail_box" style="display: none;">

							<label for="longitude"><?php echo JText::_('LNG_PUBLISH_ONLY_CITY')?> </label>
							<input class="" type="checkbox" name="publish_only_city" id="publish_only_city" value="1" <?php echo $this->item->publish_only_city?"checked":"" ?>>
							<div class="clear"></div>
						</div>

						<?php if(!$enablePackages || isset($this->item->package->features) && in_array(GOOGLE_MAP,$this->item->package->features)){ ?>

						<?php if($attributeConfig["map"]!=ATTRIBUTE_NOT_SHOW){?>

						
						 <button style="display:none;" type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo" id="btn_collapse">Validate Address</button>
						 <span class="error_msg" id="message_not_found" style="display:none;padding-left:0px;font-size: 14px;    font-weight: 600;">Address not found.</span>
						 <br><br>


						  <div id="demo" class="collapse" style="padding-left: 36px;width: 93%;">

							<br>
							<label for="address_id"><?php echo JText::_('LNG_ADDRESS')?></label>
							<div class="detail_box">

								<input type="text" id="autocomplete" placeholder="<?php echo JText::_("LNG_ENTER_ADDRESS") ?>"  value="65 Bell St, South Townsville, QLD, 4810, Australia"></input>
								<div class="clear"></div>
							</div>

							<div class="detail_box" >
								<?php if($attributeConfig["map"] == ATTRIBUTE_MANDATORY){?>
									<div  class="form-detail req"></div>
								<?php }?>
								<label for="latitude"><?php echo JText::_('LNG_LATITUDE')?> </label>
								<p class="small"><?php echo JText::_('LNG_MAP_INFO')?></p>
								<input class="input_txt <?php echo $attributeConfig["map"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" type="text" name="latitude" id="latitude" value="<?php echo $this->item->latitude ?>">
								<div class="clear"></div>
							</div>

							<div class="detail_box">
								<?php if($attributeConfig["map"] == ATTRIBUTE_MANDATORY){?>
									<div  class="form-detail req"></div>
								<?php }?>
								<label for="longitude"><?php echo JText::_('LNG_LONGITUDE')?> </label>
								<p class="small"><?php echo JText::_('LNG_MAP_INFO')?></p>
								<input class="input_txt <?php echo $attributeConfig["map"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" type="text" name="longitude" id="longitude" value="<?php echo $this->item->longitude ?>">
								<div class="clear"></div>
							</div>
					
							<div id="map-container">
								<div id="company_map">
								</div>
							</div>
							<br>
							</div>
						<?php }?>
						<?php } ?>
					</div>	</div>	</div>	</div>	</div>

				</div>


				<?php
				if($this->appSettings->show_secondary_locations == 1 && !$showSteps && !empty($this->item->id)) { ?>
					<div class="panel panel-default fie_wid">
							<div class="panel-heading">
								<strong><h3> <?php echo JText::_('LNG_COMPANY_SECONDARY_LOCATIONS');?></h3></strong>
								<small class="small_note"> <?php echo JText::_('LNG_COMPANY_SECONDARY_LOCATIONS_TXT');?>.</small>
							</div>
							<div class="package_content panel-body">

							<div class="box-info" style="padding: 15px;">

											<!-- /.box-header -->
											<!-- form start -->
							<div class="form-horizontal">
							<div class="box-body">
								<div class="form-box" style="margin-left: 44px !important;">
						<div class="form-box" id="company-locations">
						<?php foreach ( $this->item->locations as $location){ ?>
							<div class="detail_box" id="location-box-<?php echo $location->id?>">
								<div id="location-<?php echo $location->id?>"><?php echo $location->name." - ".$location->street_number.", ".$location->address.", ".$location->city.", ".$location->county.", ".$location->country?></div>
								<a href="javascript:editLocation(<?php echo $location->id ?>)"><?php echo JText::_("LNG_EDIT") ?></a> | <a href="javascript:deleteLocation(<?php echo $location->id ?>)"><?php echo JText::_("LNG_DELETE") ?></a>
							</div>
						<?php } ?>
						</div>
						<div>
							<a href="javascript:editLocation(0)"><?php echo JText::_("LNG_ADD_NEW_LOCATION") ?></a>
						</div></div></div></div></div></div>
					</div>
				<?php
				} ?>
			</div>


			<div id="edit-tab4" class="edit-tab">
				<div class="panel panel-default fie_wid">
					<div class="panel-heading" style="display:none;">
							<strong><h3> <i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp; <?php echo JText::_('LNG_COMPANY_CONTACT_INFORMATION');?></h3></strong>
							<small class="small_note"> <?php echo JText::_('LNG_COMPANY_CONTACT_INFORMATION_TEXT');?>.</small>
					</div>
					<div class="package_content panel-body">

					<div class="box-info" style="padding: 15px;">

				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-box" style="margin-left: 28px !important;">

						<?php
						if($attributeConfig["mobile_phone"]!=ATTRIBUTE_NOT_SHOW) { ?>
							<div class="detail_box">
								<?php if($attributeConfig["mobile_phone"] == ATTRIBUTE_MANDATORY){?>
									<div  class="form-detail req"></div>
								<?php }?>
								<label for="phone"><?php echo "Mobile"//JText::_('LNG_MOBILE_PHONE')?></label>
								<input type="text"	name="mobile" id="mobile" class="input_txt <?php echo $attributeConfig["mobile_phone"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->mobile ?>">
								<div class="clear"></div>
							</div>
						<?php
						} ?>

					

							<?php
						if($attributeConfig["email"]!=ATTRIBUTE_NOT_SHOW) { ?>
							<div class="detail_box">
								<?php if($attributeConfig["email"] == ATTRIBUTE_MANDATORY){?>
									<div  class="form-detail req"></div>
								<?php }?>
								<label for="email"><?php echo JText::_('LNG_EMAIL')?></label>
								<input type="text" name="email" id="email" class="input_txt <?php echo $attributeConfig["email"] == ATTRIBUTE_MANDATORY?"validate[required,custom[email]]":""?> text-input" value="<?php echo $this->item->email==null?$user->email:$this->item->email ?>">
								<div class="description">e.g. office@site.com</div>
								<div class="clear"></div>
								<span class="error_msg" id="frmEmail_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
							</div>
						<?php
						} ?>

						<div class="detail_box" style="top: 9px;margin-bottom: 23px;">
															<label for="email_public">Email (Public) </label>
															<input type="text" class="input_txt  text-input" value="">
															<div class="clear"></div>
						</div>

						<?php
						if($attributeConfig["phone"]!=ATTRIBUTE_NOT_SHOW) { ?>
							<div class="detail_box">
								<?php if($attributeConfig["phone"] == ATTRIBUTE_MANDATORY){?>
									<div  class="form-detail req"></div>
								<?php }?>
								<label for="phone"><?php echo JText::_('LNG_TELEPHONE')?></label>
								<input type="text"	name="phone" id="phone" class="input_txt <?php echo $attributeConfig["phone"] == ATTRIBUTE_MANDATORY?"validate[required]":""?> text-input"
									value="<?php echo $this->item->phone==null?$user->phone:$this->item->phone ?>">
								<div class="clear"></div>
							</div>
						<?php
						} ?>
						
					
						<?php
						if($attributeConfig["fax"]!=ATTRIBUTE_NOT_SHOW) { ?>
							<div class="detail_box">
								<?php if($attributeConfig["fax"] == ATTRIBUTE_MANDATORY){?>
									<div  class="form-detail req"></div>
								<?php }?>
								<label for="fax"><?php echo JText::_('LNG_FAX')?></label>
								<input type="text" name="fax" id="fax" class="input_txt <?php echo $attributeConfig["fax"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->fax ?>">
								<div class="clear"></div>
							</div>
						<?php
						} ?>

							<?php
													if($attributeConfig["website"]!=ATTRIBUTE_NOT_SHOW) { ?>
														<?php
														if(!$enablePackages || isset($this->item->package->features) && in_array(WEBSITE_ADDRESS,$this->item->package->features)) { ?>
															<div class="detail_box">
																<?php if($attributeConfig["website"] == ATTRIBUTE_MANDATORY){?>
																	<div  class="form-detail req"></div>
																<?php }?>
																<label for="website"><?php echo JText::_('LNG_WEBSITE')?> </label>
																<input type="text" name="website" id="website" value="<?php echo $this->item->website ?>"	class="input_txt <?php echo $attributeConfig["website"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>">
																<div class="clear"></div>
															</div>
														<?php
														} ?>
													<?php
													} ?>

					</div>	</div>	</div>	</div>	</div>
				</div>




				<?php
				if($attributeConfig["contact_person"]!=ATTRIBUTE_NOT_SHOW) { ?>

					<div class="panel panel-default fie_wid" style="display:none;">
						<div class="panel-heading">
								<strong><h3> <i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp; <?php echo JText::_('LNG_COMPANY_CONTACT_PERSON_INFORMATION');?></strong></h3>
								<small class="small_note"> <?php echo JText::_('LNG_COMPANY_CONTACT_PERSON_INFORMATION_TEXT');?>.</small>
						</div>
						<div class="package_content panel-body">

						<div class="box-info" style="padding: 15px;">

					<div class="form-horizontal">
						<div class="box-body">
							<div class="form-box" style="margin-left: 44px !important;">

						<div class="form-box">
							<div class="detail_box">
								<?php if($attributeConfig["contact_person"] == ATTRIBUTE_MANDATORY){?>
									<div  class="form-detail req"></div>
								<?php }?>
								<label for="contact_name"><?php echo JText::_('LNG_NAME')?></label>
								<input type="text" name="contact_name" id="contact_name" class="input_txt <?php echo $attributeConfig["contact_person"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->contact->contact_name ?>">
								<div class="clear"></div>
							</div>
							<div class="detail_box">
								<label for="contact_email"><?php echo JText::_('LNG_EMAIL')?></label>
								<input type="text" name="contact_email" id="contact_email" class="input_txt text-input"
									value="<?php echo $this->item->contact->contact_email ?>">
									<div class="description">e.g. office@site.com</div>
								<div class="clear"></div>
							</div>
							<div class="detail_box">
								<label for="contact_phone"><?php echo JText::_('LNG_TELEPHONE')?></label>
								<input type="text" name="contact_phone" id="contact_phone" class="input_txt text-input"
									value="<?php echo $this->item->contact->contact_phone ?>">
								<div class="clear"></div>
							</div>
							<div class="detail_box">
								<label for="contact_fax"><?php echo JText::_('LNG_FAX')?></label>
								<input type="text" name="contact_fax" id="contact_fax" class="input_txt" value="<?php echo $this->item->contact->contact_fax ?>">
								<div class="clear"></div>
							</div>
						</div></div></div></div></div></div>
					</div>



				<?php
				} ?>
			</div>




			<?php
			if(!$showSteps || ((!$enablePackages || (isset($this->item->package->features) && in_array(SOCIAL_NETWORKS,$this->item->package->features)))
			    || (!$enablePackages || (isset($this->item->package->features) && in_array(VIDEOS,$this->item->package->features)))
				||(!$enablePackages || (isset($this->item->package->features) && in_array(IMAGE_UPLOAD,$this->item->package->features))))){ ?>
				<div id="edit-tab5"  class="edit-tab" style="display:none;">
					<div class="panel panel-default fie_wid">
						<div class="panel-heading"><h3> <i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp;&nbsp; <?php echo JText::_('LNG_COMPANY_PICTURES');?></h3>
							<small class="small_note"><?php echo JText::_('LNG_COMPANY_PICTURE_INFORMATION_TEXT');?>.</small>
						</div>
					<div class="panel-body">
					<?php
					if($attributeConfig["pictures"]!=ATTRIBUTE_NOT_SHOW) {
						if(!$enablePackages || isset($this->item->package->features) && in_array(IMAGE_UPLOAD,$this->item->package->features)) { ?>
							<fieldset class="boxed ">



								<TABLE class="admintable" align='center'  id='table_company_pictures' name='table_company_pictures'>
												<?php
												foreach( $this->item->pictures as $picture ) { ?>
													<TR>
															<TD align='left'>
																<textarea cols='50' rows='2' name='company_picture_info[]'  style="width: 100% !important;" id='company_picture_info'><?php echo $picture['company_picture_info']?></textarea>
															</TD>
															<td align='center' id='wat'>
																
																<input type='hidden' value='<?php echo $picture['company_picture_enable']?>' name='company_picture_enable[]' id='company_picture_enable'>
																<input type='hidden' value='<?php echo $picture['company_picture_path']?>' name='company_picture_path[]' id='company_picture_path'>
															</td>
															<td align='center'>
																<img class='btn_picture_delete'
																	src='<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/del_options.gif"?>'
																	onclick="
																		if(!confirm('<?php echo JText::_('LNG_CONFIRM_DELETE_PICTURE',true)?>'))
																			return;
																		var row = jQuery(this).parents('tr:first');
																		var row_idx = row.prevAll().length;
																		jQuery('#crt_pos').val(row_idx);
																		jQuery('#crt_path').val('<?php echo $picture['company_picture_path']?>');
																		jQuery('#btn_removefile').click();"/>
															</td>
															<td align='center'>
																<img class='btn_picture_status'
																	src='<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/".($picture['company_picture_enable'] ? 'checked' : 'unchecked').".gif"?>'
																	onclick="
																		var form = document.adminForm;
																		var v_status = null;
																		var pos = jQuery(this).closest('tr')[0].sectionRowIndex;

																		if( form.elements['company_picture_enable[]'].length == null ) {
																			v_status  = form.elements['company_picture_enable[]'];
																		}
																		else {
																			v_status  = form.elements['company_picture_enable[]'][pos];
																		}
																		if( v_status.value == '1') {
																			jQuery(this).attr('src', '<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/unchecked.gif"?>');
																			v_status.value ='0';
																		}
																		else {
																			jQuery(this).attr('src', '<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/checked.gif"?>');
																			v_status.value ='1';
																		}"/>
															</td>
															<td>
																<span class="span_up" onclick='var row = jQuery(this).parents("tr:first");  row.insertBefore(row.prev());'>
																	<img src="<?php echo JURI::root()?>administrator/components/<?php echo JBusinessUtil::getComponentName()?>/assets/img/up-icon.png">
																</span>
																<span class="span_down"onclick='var row = jQuery(this).parents("tr:first"); row.insertAfter(row.next());'>
																	<img src="<?php echo JURI::root()?>administrator/components/<?php echo JBusinessUtil::getComponentName()?>/assets/img/down-icon.png">
																</span>
															</td>
													</TR>
												<?php
												} ?>
								</TABLE>
											
											
								<input type='button' name='btn_removefile' id='btn_removefile' value='x' style='display:none'>
								<input type='hidden' name='crt_pos' id='crt_pos' value=''>
								<input type='hidden' name='crt_path' id='crt_path' value=''>
								<input type='hidden' name='images_included' value="1">
								<TABLE class='picture-table' align='left' border='0'>
								
									<TR id="add-pictures">
										
											
											
											<input type="file" name="uploadfile" id="multiImageUploader"  class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple / style=" visibility: hidden;display: none; ">
											<label for="multiImageUploader"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
											<path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <?php echo JText::_('LNG_PLEASE_CHOOSE_A_FILE'); ?></strong></label>
								
								
								
									</TR>
								</TABLE>
							</fieldset>
						<?php
						} ?>
					<?php
					} ?>
				</div></div>



					<?php
					if($attributeConfig["video"]!=ATTRIBUTE_NOT_SHOW) {
						if(!$enablePackages || isset($this->item->package->features) && in_array(VIDEOS,$this->item->package->features)) { ?>
						<div class="panel panel-default fie_wid">
							  <div class="panel-heading">
										<strong><h3> <?php echo JText::_('LNG_COMPANY_VIDEOS');?></strong></h3>
										<small class="small_note"> <?php echo JText::_('LNG_COMPANY_VIDEO_INFORMATION_TEXT');?>.</small>
								</div>
								<input type="hidden" name="videos-included" value="1"/>


								<div class="package_content panel-body">

								<div class="box-info" style="padding: 15px;">

												<!-- /.box-header -->
												<!-- form start -->
							<div class="form-horizontal">
								<div class="box-body">
									<div class="form-box" style="margin-left: 44px !important;">
								<div class="form-box">
									<div id="video-container">
										<?php
										if(count($this->item->videos) == 0){?>
											<div class="detail_box" id="detailBox0">
												<label for="video1"><?php echo JText::_('LNG_VIDEO')?></label>
												<textarea name="videos[]" id="0" class="input_txt" cols="75" rows="2"></textarea>
												<img height="12px" align="left" width="12px"
													src="<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/del_icon.png"?>" alt="Delete video" onclick="removeRow('detailBox0')" style="cursor: pointer; margin: 3px;">
												<div class="clear"></div>
											</div>
										<?php
										} ?>

										<?php $index = 0;
										if(count($this->item->videos)>0)
											foreach($this->item->videos as $video) { ?>
												<div class="detail_box" id="detailBox<?php echo $index ?>">
													<?php if($attributeConfig["video"] == ATTRIBUTE_MANDATORY){?>
														<div  class="form-detail req"></div>
													<?php }?>
													<label for="<?php echo $video->id?>"><?php echo JText::_('LNG_VIDEO')?></label>
													<textarea name="videos[]" id="<?php echo $video->id?>" class="input_txt" cols="75" rows="2"><?php echo $video->url ?></textarea>
													<img height="12px" align="left" width="12px" src="<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/del_icon.png"?>" alt="Delete video" onclick="removeRow('detailBox<?php echo $index++; ?>')" style="cursor: pointer; margin: 3px;">
													<div class="clear"></div>
												</div>
											<?php
											} ?>
									</div>
									<a id="add-video" href="javascript:void(0);" onclick="addVideo()"><?php echo JText::_('LNG_ADD_VIDEO')?></a>
								</div></div></div></div></div></div>
							</div>
						<?php
						} ?>
					<?php
					}?>



					<?php
					if($attributeConfig["social_networks"]!=ATTRIBUTE_NOT_SHOW) {
						if(!$enablePackages || isset($this->item->package->features) && in_array(SOCIAL_NETWORKS,$this->item->package->features)) { ?>

							<div class="panel panel-default fie_wid">
								<div class="panel-heading">
										<strong><h3><i class="fa fa-weixin" aria-hidden="true"></i>&nbsp;&nbsp; <?php echo JText::_('LNG_SOCIAL_NETWORKS');?>	</strong></h3>
										<small class="small_note"><?php echo JText::_('LNG_SOCIAL_NETWORKS_TEXT');?></small>
								</div>
								<div class="package_content panel-body">

								<div class="box-info" style="padding: 15px;">

												<!-- /.box-header -->
												<!-- form start -->
							<div class="form-horizontal">
								<div class="box-body">
									<div class="form-box" style="margin-left: 44px !important;">
								<div class="form-box">
									<div class="detail_box">
										<?php if($attributeConfig["social_networks"] == ATTRIBUTE_MANDATORY){?>
											<div  class="form-detail req"></div>
										<?php }?>
										
										<label for="facebook"><i class="fa fa-facebook-official" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('LNG_FACEBOOK')?></label>
										<input type="text" name="facebook" id="facebook" class="input_txt <?php echo $attributeConfig["social_networks"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->facebook ?>">
										<div class="clear"></div>
									</div>
									<div class="detail_box">
										<?php if($attributeConfig["social_networks"] == ATTRIBUTE_MANDATORY){?>
											<div  class="form-detail req"></div>
										<?php }?>
										<label for="twitter"><i class="fa fa-twitter" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('LNG_TWITTER')?></label>
										<input type="text" name="twitter" id="twitter" class="input_txt <?php echo $attributeConfig["social_networks"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->twitter ?>">
										<div class="clear"></div>
									</div>
									<div class="detail_box">
										<?php if($attributeConfig["social_networks"] == ATTRIBUTE_MANDATORY){?>
											<div  class="form-detail req"></div>
										<?php }?>
										<label for="googlep"><i class="fa fa-google-plus-official" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('LNG_GOOGLE_PLUS')?></label>
										<input type="text" name="googlep" id="googlep" class="input_txt <?php echo $attributeConfig["social_networks"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->googlep ?>">
										<div class="clear"></div>
									</div>
									<div class="detail_box">
										<?php if($attributeConfig["social_networks"] == ATTRIBUTE_MANDATORY){?>
											<div  class="form-detail req"></div>
										<?php }?>
										<label for="linkedin"><i class="fa fa-linkedin-square" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('LNG_LINKEDIN')?></label>
										<input type="text" name="linkedin" id="linkedin" class="input_txt <?php echo $attributeConfig["social_networks"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->linkedin?>">
										<div class="clear"></div>
									</div>
									<div class="detail_box">
										<?php if($attributeConfig["social_networks"] == ATTRIBUTE_MANDATORY){?>
											<div  class="form-detail req"></div>
										<?php }?>
										<label for="linkedin"><i class="fa fa-skype" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('LNG_SKYPE_ID')?></label>
										<input type="text" name="skype" id="skype" class="input_txt <?php echo $attributeConfig["social_networks"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->skype?>">
										<div class="clear"></div>
									</div>
									<div class="detail_box">
										<?php if($attributeConfig["social_networks"] == ATTRIBUTE_MANDATORY){?>
											<div  class="form-detail req"></div>
										<?php }?>
										<label for="youtube"><i class="fa fa-youtube-play" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('LNG_YOUTUBE')?></label>
										<input type="text" name="youtube" id="youtube" class="input_txt <?php echo $attributeConfig["social_networks"] == ATTRIBUTE_MANDATORY?"validate[required]":""?>" value="<?php echo $this->item->youtube?>">
										<div class="clear"></div>
									</div>
								</div> </div></div></div></div></div>
							</div>

						<?php
						} ?>
					<?php
					} ?>
				</div>
			<?php
			} ?>
			<?php
			if(!isset($isProfile)) {
				if(!isset($this->item->userId)) {
					$this->item->userId = 0;
				}
				$companyOwner = JFactory::getUser($this->item->userId); ?>

				<div class="panel panel-default fie_wid">
					<div class="panel-heading">
								<strong><h3> <?php echo JText::_('LNG_COMPANY_USER');?></h3></strong>
								<small>User information</small>
					</div>

						<div class="package_content panel-body">

						<div class="box-info" style="padding: 15px;">

										<!-- /.box-header -->
										<!-- form start -->
						<div class="form-horizontal">
						<div class="box-body">
							<div class="form-box" style="margin-left: 44px !important;">
							<div class="detail_box">
								<label for="userId"><?php echo JText::_('LNG_USERID')?></label>
								<input id="userName" class="input-medium" type="text" disabled="disabled" value="<?php echo $companyOwner->name ?>">
								<?php
								if(JBusinessUtil::isJoomla3()) { ?>
									<a class="btn btn-primary modal_jform_created_by" rel="{handler: 'iframe', size: {x: 800, y: 500}}" href="index.php?option=com_users&view=users&layout=modal&tmpl=component&field=jform_created_by" title="Select User">
										<i class="dir-icon-user"></i>
									</a>
								<?php
								} else { ?>
									<div class="button2-left">
										<div class="blank">
											<a class="btn btn-primary modal_jform_created_by" rel="{handler: 'iframe', size: {x: 800, y: 500}}" href="index.php?option=com_users&view=users&layout=modal&tmpl=component&field=jform_created_by" title="Select User">
												<?php echo JText::_('JLIB_FORM_CHANGE_USER')?>
											</a>
										</div>
									</div>
								<?php
								} ?>
								<input type="hidden" id="userId" name="userId" value="<?php echo $this->item->userId ?>" />
								<div class="clear"></div>
								<span class="error_msg" id="frmCompanyUser_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
							</div>
						</div></div></div></div></div>
					</div>

			<?php
			} ?>

			<div class="package_content panel-body">

					<div class="box-info" style="padding: 15px;">

				<div class="form-horizontal">
					<div class="box-body">
						<div class="form-box" style="    margin-left: 28px !important;margin-top: -30px;">

													<div class="detail_box">
			<?php
			if(isset($isProfile)) {
				if($this->item->id == 0) { ?>
					<div class="term_conditions" id="term_conditions">
						<input class="validate[required]" type='checkbox' id='accept_policies' name='accept_policies'>
						&nbsp;

						<a style="color:red;line-height:21px;" id="demo01" href="#animatedModal"><?php echo JText::_('LNG_AGREE_WITH_TERMS')?></a></div>
					
				<?php
				} ?>
				<?php
				if(!$showSteps) { ?>
					<div class="button-row">
						<button type="button" class="ui-dir-button ui-dir-button-green" onclick="saveCompanyInformation();">
							<span class="ui-button-text"><i class="dir-icon-edit"></i> <?php echo JText::_("LNG_SAVE")?></span>
						</button>
						<button type="button" class="ui-dir-button ui-dir-button-grey" onclick="cancel()">
								<span class="ui-button-text"><i class="dir-icon-remove-sign red"></i> <?php echo JText::_("LNG_CANCEL")?></span>
						</button>
					</div>
				<?php
				} ?>

			<?php
			} ?>
			<?php
			if($showSteps) { ?>
				<div class="button-row">
					<button id="prev-btn" type="button" class="ui-dir-button" onclick="previousTab();">
						<span class="ui-button-text"><?php echo JText::_("LNG_PREVIOUS")?></span>
					</button>
					<button id="next-btn" type="button" class="ui-dir-button ui-dir-button-green right" onclick="nextTab()">
						<span class="ui-button-text"><?php echo JText::_("LNG_NEXT")?></span>
					</button>
					<button id="save-btn" type="button" class="ui-dir-button ui-dir-button-green right" onclick="saveCompanyInformation()">
						<span class="ui-button-text"><?php echo JText::_("LNG_SAVE")?></span>
					</button>
				</div>
			<?php
			} ?>

			</div></div></div></div></div></div>
			<script  type="text/javascript">
				function saveCompanyInformation() {
					if(validateCmpForm())
						return false;
					jQuery("#task").val('managecompany.save');
					jQuery("#selectedSubcategories").each(function(){
						jQuery("#selectedSubcategories option").attr("selected","selected");
					});
					var form = document.adminForm;
					form.submit();
				}
				function cancel() {
					jQuery("#task").val('managecompany.cancel');
					var form = document.adminForm;
					form.submit();
				}
				function validateCmpForm() {
					var isError = jQuery("#item-form").validationEngine('validate');
					return !isError;
				}
			</script>
			<input type="hidden" name="option" value="<?php echo JBusinessUtil::getComponentName()?>" />
			<input type="hidden" name="task" id="task" value="" />
			<input type="hidden" name="task_state" value="<?php echo $this->item->id==null?'save':'edit' ?>" />
			<input type="hidden" name="id" value="<?php echo $this->item->id ?>" />
			<input type="hidden" name="company-exists" id="company-exists" value="" />
			<input type="hidden" name="company-deleted" id="company-deleted" value="" />

			<?php
			if(isset($isProfile)) { ?>
				<input type="hidden" id="userId" name="userId" value="<?php echo $this->item->userId? $this->item->userId : $user->id ?>" />
				<input type="hidden" name="view" id="view" value="managecompany" />
			<?php
			} else { ?>
				<input type="hidden" name="view" id="view" value="company" />
			<?php
			}?>
			<?php echo JHTML::_( 'form.token' ); ?>
		</form>
	</div>
</div>
<div class="clear"></div>
<div id="location-dialog"  style="display:none">
	<div id="dialog-container">
		<div class="titleBar">
			<span class="dialogTitle" id="dialogTitle"> </span>
			<span  title="Cancel"  class="dialogCloseButton" onClick="jQuery.unblockUI();">
				<span title="Cancel" class="closeText">x</span>
			</span>
		</div>
		<div class="dialogContent">
			<a name="locationD"></a>
			<h3 class="title"><?php echo JText::_("LNG_COMPANY_LOCATION")?> </h3>
			<iframe id="location-frame" height="500" width="700" src=""></iframe>
		</div>
	</div>
</div>
<div id="conditions" class="terms-conditions" style="display:none">
	<div id="dialog-container">
		<div class="titleBar">
			<span class="dialogTitle" id="dialogTitle"></span>
			<span  title="Cancel"  class="dialogCloseButton" onClick="jQuery.unblockUI();">
				<span title="Cancel" class="closeText">x</span>
			</span>
		</div>
		<div class="dialogContent">
			<a id="termsc" name="termsc"></a>
			<h3 class="title"> <?php echo JText::_('LNG_TERMS_AND_CONDITIONS');?></h3>
			<div class="dialogContentBody" id="dialogContentBody">
				<?php echo $this->appSettings->terms_conditions ?>
			</div>
		</div>
	</div>
</div>

<?php include JPATH_COMPONENT_SITE.'/assets/uploader.php'; ?>

<script>
	var maxPictures = <?php echo isset($this->item->package)?$this->item->package->max_pictures:$this->appSettings->max_pictures ?>;
	var maxVideos = <?php echo isset($this->item->package)?$this->item->package->max_videos :$this->appSettings->max_video ?>;
	var maxCategories = <?php echo isset($this->item->package)?$this->item->package->max_categories :$this->appSettings->max_categories ?>;

	jQuery(document).ready(function() {

		//jQuery("#item-form").validationEngine('attach');
		jQuery("#tab1").validationEngine('attach');

		jQuery(".fieldset-business_hours").click(function(){
			jQuery(this).toggleClass("open");
		});

		jQuery(".fieldset-business_hours > .field").click(function(event) {
			event.stopPropagation();
		});

		jQuery('.timepicker').timepicker({ 'timeFormat': '<?php echo $this->appSettings->time_format?>', 'minTime': '6:00am', });

		checkNumberOfVideos();
		checkNumberOfPictures();

		var categoriesSelectList = null;

		categoriesSelectList = jQuery("select#selectedSubcategories").selectList({
			sort: true,
			classPrefix: 'categories_ids',
			instance: true,
			onAdd: function (select, value, text) {
				if( jQuery("select#selectedSubcategories option:selected").length >= maxCategories-1 ) {
					jQuery("select.categories_ids-select").hide();
				}
				var option = jQuery('<option/>');
				option.attr({ 'value': value }).text(text);
				jQuery('#mainSubcategory').append(option);
			},
			onRemove: function (select, value, text) {
				if( jQuery("select#selectedSubcategories option:selected").length <= maxCategories ) {
					jQuery("select.categories_ids-select").show();
				}
				var option = jQuery('#selectedSubcategories option:selected');
				jQuery('#mainSubcategory option[value="'+value+'"]').remove();
			}
		});
		if( jQuery("select#selectedSubcategories option:selected").length >= maxCategories ) {
			jQuery("select.categories_ids-select").hide();
		}

		initializeAutocomplete();
		<?php if ($showSteps) {?>
			showTab(1);
		<?php } ?>
	});

	function addVideo() {
		var count = jQuery("#video-container").children().length+1;
		id=0;
		var outerDiv = document.createElement('div');
		outerDiv.setAttribute('class',		'detail_box');
		outerDiv.setAttribute('id',		'detailBox'+count);

		var newLabel = document.createElement('label');
		newLabel.setAttribute("for",		id);
		newLabel.innerHTML="<?php echo JText::_('LNG_VIDEO',true)?>";

		var newInput = document.createElement('textarea');
		newInput.setAttribute('name',		'videos[]');
		newInput.setAttribute('id',			id);
		newInput.setAttribute('class',		'input_txt');

		var img_del	= document.createElement('img');
		img_del.setAttribute('src', "<?php echo JURI::root() ."administrator/components/".JBusinessUtil::getComponentName()."/assets/img/del_icon.png"?>");
		img_del.setAttribute('alt', 'Delete option');
		img_del.setAttribute('height', '12px');
		img_del.setAttribute('width', '12px');
		img_del.setAttribute('align', 'left');
		img_del.setAttribute('onclick', 'removeRow("detailBox'+count+'")');
		img_del.setAttribute('style', "cursor: pointer; margin:3px;");

		var clearDiv = document.createElement('div');
		clearDiv.setAttribute('class',		'clear');

		outerDiv.appendChild(newLabel);
		outerDiv.appendChild(newInput);
		outerDiv.appendChild(img_del);
		outerDiv.appendChild(clearDiv);

		var facilityContainer =jQuery("#video-container");
		facilityContainer.append(outerDiv);

		checkNumberOfVideos();
	}

	function removeRow(id) {
		jQuery('#'+id).remove();
		checkNumberOfVideos();
	}

	function checkNumberOfVideos() {
		var nrVideos = jQuery('textarea[name*="videos[]"]').length;
		if(nrVideos < maxVideos) {
			jQuery("#add-video").show();
		}
		else {
			jQuery("#add-video").hide();
		}
	}

	function extendPeriod() {
		<?php  if(!empty($isProfile)) { ?>
			jQuery("#task").val("managecompany.extendPeriod");
		<?php } else { ?>
			jQuery("#task").val("company.extendPeriod");
		<?php } ?>
		jQuery("#item-form").submit();
	}

	var placeSearch, autocomplete;
	var component_form = {
		'street_number': 'short_name',
		'route': 'long_name',
		'locality': 'long_name',
		'administrative_area_level_1': 'long_name',
		'country': 'long_name',
		'postal_code': 'short_name'
	};


	function initializeAutocomplete() {
		//console.log("load");
		
		autocomplete = new google.maps.places.Autocomplete(document.getElementById('autocomplete'), { types: [ 'geocode' ] });
		//console.log(autocomplete);
		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			fillInAddress();
		});

	}

	function fillInAddress() {
		var place = autocomplete.getPlace();
		//console.log(place);
		for (var component in component_form) {
			var obj = document.getElementById(component);
			if(typeof maybeObject != "undefined") {
				document.getElementById(component).value = "";
				document.getElementById(component).disabled = false;
			}
		}
		for (var j = 0; j < place.address_components.length; j++) {
			var att = place.address_components[j].types[0];
			if (component_form[att]) {
				var val = place.address_components[j][component_form[att]];
				jQuery("#"+att).val(val);
				if(att=='country') {
					jQuery('#country option').filter(function () {
						return jQuery(this).text() === val;
					}).attr('selected', true);
				}

			}
		}

		if(typeof map != "undefined") {

			if (place.geometry.viewport) {
				map.fitBounds(place.geometry.viewport);
			} else {
				
				map.setCenter(place.geometry.location);
				map.setZoom(17);
				addMarker(place.geometry.location);
			}
		}
	}

	function geolocate() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var geolocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
				autocomplete.setBounds(new google.maps.LatLngBounds(geolocation, geolocation));
			});
		}
	}

	var map;
	var markers = [];

	function initialize() {
		<?php
		$latitude = isset($this->item->latitude) && strlen($this->item->latitude)>0?$this->item->latitude:"0";
		$longitude = isset($this->item->longitude) && strlen($this->item->longitude)>0?$this->item->longitude:"0";
		?>
		var companyLocation = new google.maps.LatLng(<?php echo $latitude ?>, <?php echo $longitude ?>);
		var mapOptions = {
			zoom: <?php echo !(isset($this->item->latitude) && strlen($this->item->latitude))?1:15?>,
			center: companyLocation,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var mapdiv = document.getElementById("company_map");
		mapdiv.style.width = '97%';
		mapdiv.style.height = '400px';
		map = new google.maps.Map(mapdiv,  mapOptions);
		var latitude = '<?php echo  $this->item->latitude ?>';
		var longitude = '<?php echo  $this->item->longitude ?>';
		if(latitude && longitude)
			addMarker(new google.maps.LatLng(latitude, longitude ));
		google.maps.event.addListener(map, 'click', function(event) {
			deleteOverlays();
			addMarker(event.latLng);
		});
	}

	// Add a marker to the map and push to the array.
	function addMarker(location) {
		document.getElementById("latitude").value = location.lat();
		document.getElementById("longitude").value = location.lng();
		marker = new google.maps.Marker({
			position: location,
			map: map
		});
		markers.push(marker);
	}

	// Sets the map on all markers in the array.
	function setAllMap(map) {
		for (var i = 0; i < markers.length; i++) {
			markers[i].setMap(map);
		}
	}

	// Removes the overlays from the map, but keeps them in the array.
	function clearOverlays() {
		setAllMap(null);
	}

	// Shows any overlays currently in the array.
	function showOverlays() {
		setAllMap(map);
	}

	// Deletes all markers in the array by removing references to them.
	function deleteOverlays() {
		clearOverlays();
		markers = [];
	}

	function loadScript() {
		<?php if($attributeConfig["map"] != ATTRIBUTE_NOT_SHOW){?>
			initialize();
		<?php } ?>
	}

	<?php if(!$enablePackages || isset($this->item->package->features) && in_array(GOOGLE_MAP,$this->item->package->features)){ ?>
		window.onload = loadScript;
	<?php } ?>

	var activityCitiesList = null;
	jQuery(document).ready(function() {
		activityCitiesList = jQuery("select#activity_cities").selectList({
			sort: true,
			classPrefix: 'cities_ids',
			instance: true
		});
	});

	function checkAllActivityCities() {
		uncheckAllActivityCities();
		jQuery(".cities_ids-select option").each(function() {
			if(jQuery(this).val()!="") {
				activityCitiesList.add(jQuery(this));
			}
		});
		jQuery("#activity_cities option").each(function() {
			jQuery(this).attr("selected","selected");
		});
	}

	function uncheckAllActivityCities() {
		jQuery("#activity_cities option").each(function() {
			jQuery(this).removeAttr("selected");
		});
		activityCitiesList.remove();
	}

	function editLocation(locationId) {
		var baseUrl = "<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&view=company&tmpl=component&layout=locations&id='.$this->item->id,false); ?>";
		<?php if(isset($isProfile)) { ?>
			baseUrl = "<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&view=managecompany&tmpl=component&layout=locations&id='.$this->item->id,false); ?>";
		<?php } ?>
		baseUrl = baseUrl + "&locationId=" + locationId;
		jQuery("#location-frame").attr("src",baseUrl);
		jQuery.blockUI({ message: jQuery('#location-dialog'),  css: {width: 'auto', top: '10%', left:"0", position:"absolute"}});
		jQuery('.blockUI.blockMsg').center();
		jQuery('.blockOverlay').attr('title','Click to unblock').click(jQuery.unblockUI);
		location.href = "#locationD";
	}
	function deleteLocation(locationId) {
		if(!confirm("<?php echo JText::_("LNG_DELETE_LOCATION_CONF") ?>")) {
			return;
		}
		var baseUrl = "<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&task=company.deleteLocation',false); ?>";
		<?php  if(isset($isProfile)) { ?>
			baseUrl = "<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&task=managecompany.deleteLocation',false); ?>";
		<?php } ?>

		var postData="&locationId="+locationId;
		jQuery.post(baseUrl, postData, processDeleteLocationResult);
	}

	function processDeleteLocationResult(responce) {
		var xml = responce;
		jQuery(xml).find('answer').each(function() {
			if( jQuery(this).attr('error') == '1' )
				jQuery("#location-box-"+jQuery(this).attr('locationId')).remove();
			else {
				jQuery.blockUI({
					message: '<h3><?php echo JText::_("LNG_LOCATION_DELETE_FAILED",true)?></h3>'
				});
				setTimeout(jQuery.unblockUI, 2000);
			}
		});
	}

	function updateLocation(id, name, streetNumber, address, city, county, country) {
		if (jQuery("#location-"+id).length > 0) {
			jQuery("#location-"+id).html(name +" - "+ streetNumber+", "+address+", "+city+", "+county+", "+country);
		}
		else {
			var locationContainer = '<div id="location-box-'+id+'" class="detail_box">';
			locationContainer += '<div id="location-"'+id+'">'+name +" - "+streetNumber+", "+address+", "+city+", "+county+" ,"+country+'</div>';
			locationContainer += '</div>';
			jQuery("#company-locations").append(locationContainer);
		}
	}

	function closeLocationDialog() {
		jQuery.unblockUI();
	}

	function showTerms() {
		jQuery.blockUI({ message: jQuery('#conditions'),  css: {width: 'auto', top: '10%', left:"0",position:"absolute"}});
		jQuery('.blockUI.blockMsg').center();
		jQuery('.blockOverlay').attr('title','Click to unblock').click(jQuery.unblockUI);
		jQuery(document).scrollTop( jQuery("#termsc").offset().top );
	}

	var currentTab =1;
	var maxTabs = 5;
	var tabMapInitialized = 0;
	<?php
	if(!((!$enablePackages || isset($this->item->package->features) && in_array(SOCIAL_NETWORKS,$this->item->package->features))
		|| (!$enablePackages || isset($this->item->package->features) && in_array(VIDEOS,$this->item->package->features))
		|| (!$enablePackages || isset($this->item->package->features) && in_array(IMAGE_UPLOAD,$this->item->package->features)))) {
		echo "maxTabs = 4;";
	} ?>

	jQuery("#max-tabs").html(maxTabs);

	function showTab(tab) {
		jQuery(".edit-tab").each(function(){
			jQuery(this).hide();
		});
		jQuery(".process-step").each(function(){
			jQuery(this).hide();
			jQuery(this).removeClass("active");

		});
		jQuery(".process-tab").each(function(){
			jQuery(this).removeClass("active");
		});
		if(tab==1) {
			jQuery("#prev-btn").hide();
		}
		else {
			jQuery("#prev-btn").show();
		}
		if(tab==maxTabs) {
			jQuery("#next-btn").hide();
			jQuery("#save-btn").show();
			jQuery("#term_conditions").show();
		}
		else {
			jQuery("#next-btn").show();
			jQuery("#save-btn").hide();
			jQuery("#term_conditions").hide();
		}
		jQuery("#edit-tab"+tab).show();
		jQuery("#step"+tab).show();
		jQuery(window).scrollTop(10);
		jQuery("#step"+tab).addClass("active");
		jQuery("#tab"+tab).addClass("active");
		jQuery("#active-step-number").html(tab);
		if(tab==3 && tabMapInitialized==0) {
			initialize();
			tabMapInitialized = 1;
		}
	}

	function nextTab() {
		var isOK = jQuery("#item-form").validationEngine('validate');
		if(isOK) {
			if(currentTab < maxTabs)
				currentTab ++;
			showTab(currentTab);
		}
	}

	function previousTab() {
		if(currentTab >1)
			currentTab --;
		showTab(currentTab);
	}





		


function showResult(result) {
	var x = jQuery("input#latitude").val(result.geometry.location.lat());
	var y = jQuery("input#longitude").val(result.geometry.location.lng());
	//console.log(result.geometry.location.lat());
	var latlng = new google.maps.LatLng(x,y);
	var myOptions = {
    zoom: 5,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.TERRAIN,
    mapTypeControlOptions: {

        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
    },
    scaleControl: true,
    streetViewControl: false
	}
    var map = new google.maps.Map(jQuery("#company_map")[0],myOptions);

	map.setCenter(new google.maps.LatLng(x,y));
		


	//map.setCenter(new google.maps.LatLng(x,y));
}

function getLatitudeLongitude(callback, address,thisComponent) {
	
    // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
    address = address || 'Ferrol, Galicia, Spain';
    // Initialize the Geocoder
    geocoder = new google.maps.Geocoder();
 	var succe = '';
    if (geocoder) {
        geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                callback(results[0]);
                //console.log(results);
                jQuery("span#message_not_found").show();
               	jQuery("span#message_not_found").text("Address confirmed");
               	jQuery("span#message_not_found").css('color','green');
               	//thisComponent.css({"border-color": "green","border-width":"1px","border-style":"solid"});
               	jQuery(thisComponent).attr('style','border: 1px solid green !important');
               	jQuery("input.input_address_geo").attr('style','border: 1px solid green !important');
               	jQuery("button.ui-dir-button.ui-dir-button-green").prop('disabled', false).css("background","red");
               
            }else{
        		if(jQuery("select#attribute_12 option:selected").text() != "Services all areas"){
					jQuery("span#message_not_found").show();
	    			jQuery("span#message_not_found").text("Address not found.");
	    			jQuery("span#message_not_found").css('color','red');
	    			jQuery(thisComponent).attr('style','border: 1px solid red !important');
	    			jQuery("button.ui-dir-button.ui-dir-button-green").prop('disabled', true).css("background","gray");
	    			jQuery("input#latitude").val('');
	    			jQuery("input#longitude").val('');
				}else{
					jQuery("span#message_not_found").hide();
				}
    		}
        });
    }
}

var address = jQuery(".txt_wat_address1").val();
var city = jQuery(".txt_wat_address2").val();
var state = jQuery(".txt_wat_address3").val();
var postcode = jQuery(".txt_wat_address4").val();

jQuery(".txt_wat_address1").keyup(function(){
        address = jQuery(this).val();
        var new_address = address+'.,'+city+','+state+','+postcode;
		getLatitudeLongitude(showResult,new_address,'.txt_wat_address1');


});

jQuery(".txt_wat_address2").keyup(function(){
       	city = jQuery(this).val();
        //console.log(city);
         var new_address = address+'.,'+city+','+state+','+postcode;
		getLatitudeLongitude(showResult,new_address,'.txt_wat_address2');
});

jQuery(".txt_wat_address3").keyup(function(){
        state = jQuery(this).val();
        //console.log(state);
        var new_address = address+'.,'+city+','+state+','+postcode;
		getLatitudeLongitude(showResult,new_address,'.txt_wat_address3');
});

jQuery(".txt_wat_address4").keyup(function(){
        postcode = jQuery(this).val();
        //console.log(postcode);
        var new_address = address+'.,'+city+','+state+','+postcode;
		getLatitudeLongitude(showResult,new_address,'.txt_wat_address4');
});

jQuery("button#btn_collapse").click(function(){
    if (jQuery("div#demo").hasClass("collapse") ) {
    	jQuery("div#demo").css({"border-color": "#c9ced5","border-width":"1px","border-style":"solid"});
	}else{
		jQuery("div#demo").css({"border-color": "#c9ced5","border-width":"1px","border-style":"solid"});
	}

});

//var xxx = jQuery("input#autocomplete").val();
var new_address = address+'.,'+city+','+state+','+postcode;
getLatitudeLongitude(showResult,new_address);

</script>
</div>

<style>
	.category-form-container{
		float: none !important;
	}
	.panel{
		box-shadow: none !important;
	}
	.panel-heading{
		border-top-left-radius: 8px !important;
	    border-top-right-radius: 8px !important;
	    padding: 10px 15px !important;
	    border-bottom: none !important;
	    color: #333333 !important;
	    background-color: #f5f5f5 !important; 
	    border-color: #dddddd !important;
	}
	.panel-body {
	    min-height: none !important;
	    padding:  none !important;
	    margin-top:  0px !important;
	    margin-bottom:  none !important;
	    background-color: #f5f5f5;
	    border:  none !important;
	        border-bottom-left-radius: 8px !important;
    border-bottom-right-radius: 8px !important;
	}
	h3{
		font-weight: bold;
		    font-size: 18px !important;
	}
	.description{
		display:none;
	}
	.detail_box input[type="text"] {
    	width: 97% !important;
	}
	.category-form-container h2{
		font-size: 18px !important;
	}
	.page-header h2{
	    margin-top: 0px;
}
#sidebar{
	padding: 18px;
}
</style>


<style type="text/css">
	
table {
  text-align: left;
  line-height: 40px;
  border-collapse: separate;
  border-spacing: 0;
 
  width: 100%;
      background-color: #fff;
  border-radius: .25rem;
  margin-bottom: 20px;
}

thead tr:first-child {
  background: #ee3928;
  color: #fff;
  border: none;
}

th:first-child, td:first-child { padding: 0 0px 0 20px; }

thead tr:last-child th { border-bottom: 3px solid #ddd; }



tbody tr:last-child td { border: none; }
tbody td { border-bottom: 1px solid #ddd;}

td:last-child {
  text-align: left;
  padding-right: 10px;
}


.admintable input[type="text"], .form-control {
	width: 96% !important;
    display: table-cell;
    vertical-align: inherit;
    margin-top: 7%;
}

fieldset.boxed{
	background: none !important;
    border: none !important;
    padding: 0% 0% !important;
}

td:last-child{
	    padding-right: 0px !important;
}



.inputfile-6 + label strong{
	float:right;
}
.inputfile + label{
	max-width: 100% !important;
}

.category-form-container label{
	line-height: 26px !important;
}

#picture-preview img{
	    width: 200px !important;
}


#table_company_pictures td:nth-child(2){
    text-align: center;
    width: 21%;
    padding: 20px;
}
#table_company_pictures td:nth-child(2) img{
       display: inline-block;
    max-width: 100%;
    height: auto;
    padding: 4px;
    line-height: 1.42857143;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}

#table_company_pictures td:nth-child(2) span{
    display:none;
}


#table_company_pictures #company_picture_info{
	    width: 98% !important;
    height: 62%;
    padding: 5px;
}

.fieldset-business_hours table input[type="text"]{
	    width: 86% !important;
    margin-top: 12px !important;
}

.detail_box input[type="text"] {
 
    border-radius: 6px;
}
#descriptionCounterShort{
	width: 8% !important;
}

.detail_box textarea, .form-item  textarea{
	    width: 98.5% !important;
}
</style>

<script type="text/javascript">
jQuery(document).ready(function() {
  //jQuery("#companyTypes").select2();
  //jQuery("#country").select2();
});
</script>

<style>
	.select2-search--dropdown .select2-search__field{
		    padding: 14px !important;
	}


.detail_box {
    clear: both;
    float: none;
    margin-bottom: 10px;
    position: relative;
    display: grid;
}



.wrapper input[type="text"] {
    position: relative; 
}

input { font-family: 'FontAwesome'; } /* This is for the placeholder */

.wrapper:before {
    font-family: 'FontAwesome';
    color:red;
    position: relative;
    left: -5px;
    content: "\f007";
}

</style>


<script>
jQuery(document).ready(function(){
	setTimeout(function(){ 
		var conceptName = jQuery('#attribute_12').find(":selected").text();

		//console.log(conceptName);
		if(conceptName != 'Select'){
			jQuery("div#custom_feide > div:nth-child(4)").css({"opacity":0.4,"cursor":"not-allowed"});
		    jQuery("div#custom_feide > div:nth-child(4) input[name='attribute_11[]']").attr("disabled", true);
		    jQuery("div#custom_feide > div:nth-child(4) input[name='attribute_11[]']").prop('checked', false);
		}
  	 },500);

   jQuery("div[class^='form-group rsform-block rsform-block-captcha']  div > div:nth-child(3) > div").addClass('col-lg-8').css('text-align','left');

	jQuery("#demo01").animatedModal();
	jQuery("#demo01").click(function (e) {
		jQuery("#animatedModal").css('display','block');
	});



	setTimeout(function(){

	var xxx = jQuery("#custo_tanawat_kanrai").remove().clone();
	jQuery("div#custom_feide > div:nth-child(2)").append(xxx);

	var xxx2 = jQuery("div#custom_feide > div.detail_box:nth-child(3)").remove().clone();
	jQuery("div#puk_ku_tong").append(xxx2);

	var xxx3 = jQuery("div#custom_feide > div.detail_box:nth-child(3)").remove().clone();
	jQuery("div#puk_ku_tong").append(xxx3);


  	jQuery("input#attribute_11").on("click",function(e){
  	   var total = jQuery('input[name="attribute_11[]"]:checked').length;
  	  
  	   if(total == 0){
  	   	   	jQuery("select#attribute_12").css({"opacity":1,"cursor":"allowed"});
		   	jQuery("select#attribute_12").attr("disabled", false);
		   	jQuery("select#attribute_12 option:selected").prop("selected", false);
		   	jQuery("select#attribute_12").addClass("validate[required]");
		   	jQuery(".attribute_12formError.parentFormitem-form.formError").css({"display":'block'});
	   		jQuery("label#details-lbl[for='attribute_12']").prev().css("display", "block" );
	   		jQuery("input.input_section_address").addClass("validate[required]");
	   		jQuery(".req_input_section_address").show();
  	   }else{
  	   		jQuery("select#attribute_12").css({"opacity":0.4,"cursor":"not-allowed"});
	   		jQuery("select#attribute_12").attr("disabled", true);
	   		jQuery("select#attribute_12 option:selected").prop("selected", false);
	   		jQuery("select#attribute_12").removeClass("validate[required]");
	   		jQuery(".attribute_12formError.parentFormitem-form.formError").css({"display":'none'});
	   		jQuery("label#details-lbl[for='attribute_12']").prev().css("display", "none" );
	   		jQuery("input.input_section_address").removeClass("validate[required]");
			jQuery(".req_input_section_address").hide();
  	   }
	  
	});

	jQuery("select#attribute_12").change(function(e){

		if(jQuery("select#attribute_12 option:selected").text() != "Services all areas" && jQuery("select#attribute_12 option:selected").text() != "Select" ){
				jQuery("input.input_section_address").addClass("validate[required]");
				jQuery(".req_input_section_address").show();
		}else{
				jQuery("input.input_section_address").removeClass("validate[required]");
				jQuery(".req_input_section_address").hide();
			  	jQuery("input.input_address_geo").attr('style','border: 1px solid green !important');
               	jQuery("button.ui-dir-button.ui-dir-button-green").prop('disabled', false).css("background","red");
               	jQuery("span#message_not_found").show();
		}

		if( jQuery("select#attribute_12 option:selected").text() == "Select"){
				jQuery("input.input_section_address").addClass("validate[required]");
				jQuery(".req_input_section_address").show();
		}

		if(jQuery(this).val() != null){
			jQuery("div#puk_ku_tong > div:last-child").css({"opacity":0.4,"cursor":"not-allowed"});
		    jQuery("div#puk_ku_tong > div:last-child input[name='attribute_11[]']").attr("disabled", true);
		    jQuery("div#puk_ku_tong > div:last-child input[name='attribute_11[]']").prop('checked', false);
		    jQuery("div#puk_ku_tong > div:last-child input[name='attribute_11[]']").addClass("validate[required]");
	   		jQuery(".attribute_11formError.parentFormitem-form.formError").css({"display":'none'});
	   		jQuery("label#details-lbl[for='attribute_11']").prev().css("display", "none" );
		}
		 
		if(jQuery(this).val() == null || jQuery(this).val() == ''){
			jQuery("div#puk_ku_tong > div:last-child").css({"opacity":1,"cursor":"allowed"});
		    jQuery("div#puk_ku_tong > div:last-child input[name='attribute_11[]']").removeAttr("disabled");
		    jQuery("div#puk_ku_tong > div:last-child input[name='attribute_11[]']").prop('checked', false);
		    jQuery("div#puk_ku_tong > div:last-child input[name='attribute_11[]']").removeClass("validate[required]");
		    jQuery(".attribute_11formError.parentFormitem-form.formError").css({"display":'block'});
	   		jQuery("label#details-lbl[for='attribute_11']").prev().css("display", "block" );
		}
	});


	  jQuery('button.ui-dir-button.ui-dir-button-green').on('click', function() {
               var $this = jQuery(this);
               $this.attr('data-loading-text',"<i class='fa fa-spinner fa-spin'></i>&nbsp;Processing");
               $this.button('loading');

               setTimeout(function() {
	       			$this.button('reset');
	   			},3000);
            });
	   
	  	jQuery("#categories").change(function(){
		
		var html= "<option value='0'>Select a Category</option>";
		var arrayFromPHP = <?php echo json_encode($categories); ?>;
		//console.log(arrayFromPHP);
		var this_category = jQuery(this).val();
		//console.log(this_category);
		jQuery.each(arrayFromPHP, function (i, elem) {
			if(elem.parent_id == this_category){
				html+="<option value="+elem.id+">"+elem.name+"</option>";
			}
		});

		jQuery('select.inputbox.input-medium.categories_ids-select').append(html);

    	
	});

  }, 2000);





});







</script>
