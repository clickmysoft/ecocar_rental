<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', 'HomeController@index')->name('index');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/document', 'HomeController@document')->name('document');
Route::get('/service', 'HomeController@service')->name('service');
Route::get('/ceo', 'HomeController@ceo')->name('ceo');
Route::get('/csr', 'HomeController@csr')->name('csr');
Route::get('/questions', 'HomeController@questions')->name('questions');
Route::get('/branch', 'HomeController@branch')->name('branch');
Route::get('/contact', 'ContactController@index')->name('contact');
Route::resource('/promotion','PromotionController');
Route::resource('/blog','ArticleController');
Route::resource('/booking','BookingController');

Route::get('/pattaya', 'HomeController@pattaya')->name('branch.pattaya');
Route::get('/downtown', 'HomeController@downtown')->name('branch.downtown');
Route::get('/onnut', 'HomeController@onnut')->name('branch.onnut');
Route::get('/bangwa', 'HomeController@bangwa')->name('branch.bangwa');
Route::get('/ladprao', 'HomeController@ladprao')->name('branch.ladprao');
Route::get('/nonthaburi', 'HomeController@nonthaburi')->name('branch.nonthaburi');
Route::get('/samrong', 'HomeController@samrong')->name('branch.samrong');
Route::get('/ubonratchathani', 'HomeController@ubonratchathani')->name('branch.ubonratchathani');
Route::get('/chiangmai', 'HomeController@chiangmai')->name('branch.chiangmai');

Route::resource('/vehicles','Booking\VehiclesController');
Route::post('/vehicles/find','Booking\VehiclesController@find')->name('vehicles.find');

Route::resource('/extra-option','Booking\ExtraOptionController');
Route::resource('/customer-detail','Booking\CustomerDetailController');
Route::post('/customer-detail/customer','Booking\CustomerDetailController@save_form_customer')->name('customer.detail');
Route::resource('/payment','Booking\PaymentController');
Route::get('/payment-pay-later','Booking\PaymentController@pay_later')->name('payment.pay_later');
Route::get('/result-booking','Booking\PaymentController@result_payment')->name('result-booking');
Route::resource('/thanachartbank','Payment\ThanachartBankController');

Route::post('contact/send', 'HomeController@send')->name('contact.send');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        /*
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');

        /*
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');
    });
});
   