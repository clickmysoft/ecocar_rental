<?php

/**
 * Global Routes
 * Routes that are used between both frontend and backend.
 */

// Switch between the included languages
Route::get('lang/{lang}', 'LanguageController@swap');

/*
 * Frontend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
    include_route_files(__DIR__.'/frontend/');
});

/*
 * Backend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    /*
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     * These routes can not be hit if the password is expired
     */
    include_route_files(__DIR__.'/backend/');
});


Route::get('ajaxdata/getdata/cars', 'Backend\Cars\CarController@getdata')->name('ajaxdata.getdata.cars');
Route::get('ajaxdata/getdata/customers', 'Backend\Customers\CustomerController@data_processing')->name('ajaxdata.getdata.customers');
Route::get('ajaxdata/getdata/prices/cars', 'Backend\Prices\CarPricesController@data_processing')->name('ajaxdata.getdata.prices.cars');

Route::get('ajaxdata/getdata/article/promotion', 'Backend\Promotions\PromotionController@data_processing')->name('ajaxdata.getdata.article.promotion');
Route::get('ajaxdata/getdata/article/blog', 'Backend\Articles\ArticleController@data_processing')->name('ajaxdata.getdata.article.blog');
Route::get('ajaxdata/getdata/users','Backend\Auth\User\UserController@data_processing')->name('ajaxdata.getdata.users');

Route::get('ajaxdata/getdata/booking-car','Backend\Booking\CarBookingController@data_processing')->name('ajaxdata.getdata.booking.car');
Route::get('ajaxdata/getdata/return-car','Backend\Booking\ReturnCarController@data_processing')->name('ajaxdata.getdata.return.car');

Route::get('ajaxdata/getdata/deposit-car','Backend\Booking\DepositController@data_processing')->name('ajaxdata.getdata.deposit.cars');

Route::get('ajaxdata/getdata/category','Backend\Category\CategoryController@data_processing')->name('ajaxdata.getdata.category');
Route::get('ajaxdata/getdata/list','Backend\Tag\TagController@data_processing')->name('ajaxdata.getdata.list');



Route::get('paypal/ec-checkout', 'PayPalController@getExpressCheckout');
Route::get('paypal/ec-checkout-success', 'PayPalController@getExpressCheckoutSuccess');
Route::get('paypal/adaptive-pay', 'PayPalController@getAdaptivePay');
Route::post('paypal/notify', 'PayPalController@notify');