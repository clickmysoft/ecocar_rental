<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', 'DashboardController@index')->name('dashboard');
Route::get('car-management','Cars\CarController@index')->name('car-management');

// Route::get('exam/user/sale','DashboardController@index')->name('exam.user.sale.index');



/*
 * Exam CRUD
 */
Route::group([
    'prefix'     => 'cars',
    'as'         => 'cars.',
    'namespace'  => 'Cars',
], function () {

	Route::resource('car','CarController');
    Route::get('car-branch-code/{id}','CarController@get_branch_code')->name('car-branch-code');
    Route::group(['prefix' => 'get','as' => 'generation'], function () {
        Route::post('get_generation','CarController@get_generation');
    });

    Route::group(['prefix' => 'get','as' => 'nickname'], function () {
        Route::post('get_nickname','CarController@get_nickname');
    });
   
    Route::group(['prefix' => 'get','as' => 'variant'], function () {
        Route::post('get_variant','CarController@get_variant');
    });

    Route::group(['prefix' => 'get','as' => 'type-insurance'], function () {
        Route::post('type-insurance','CarController@get_type_insurance');
    });


    
    
});

/*
 * Article
 */
Route::group([
    'prefix'     => 'articles',
    'as'         => 'articles.',
    'namespace'  => 'Articles',
], function () {

    Route::resource('articles','ArticleController');
    Route::post('editor-upload-file','ArticleController@upload_file')->name('editor.upload.file');
    
});


/*
 * Promotion
 */
Route::group([
    'prefix'     => 'promotions',
    'as'         => 'promotions.',
    'namespace'  => 'Promotions',
], function () {

    Route::resource('promotions','PromotionController');
    
});


/*
 * Booking
 */
Route::group([
    'prefix'     => 'booking',
    'as'         => 'booking.',
    'namespace'  => 'Booking',
], function () {
    Route::resource('car-booking','CarBookingController');
    Route::resource('return-car','ReturnCarController');
});

/*
 * Customer
 */
Route::get('customers','Customers\CustomerController@index')->name('customers');
Route::group([
    'prefix'     => 'customer',
    'as'         => 'customer.',
    'namespace'  => 'Customers',
], function () {
    Route::resource('management','CustomerController');
});



/*
 * Customer
 */
Route::get('prices','Prices\CarPricesController@index')->name('prices');
Route::group([
    'prefix'     => 'prices',
    'as'         => 'prices.',
    'namespace'  => 'Prices',
], function () {
    Route::resource('car-prices','CarPricesController');
});



/*
 * WebconfigController
 */
Route::group([
    'prefix'     => 'config',
    'as'         => 'config.',
    'namespace'  => 'Webconfig',
], function () {
        Route::resource('webconfig','WebconfigController');
        
});



/*
 * Menu
 */
Route::group([
    'prefix'     => 'menu',
    'as'         => 'menu.',
    'namespace'  => 'Menu',
], function () {

    Route::resource('menu-type','MenuTypeController');
    Route::resource('menu-items','MenuController');
    
});


/*
 * Menu
 */
Route::group([
    'prefix'     => 'booking',
    'as'         => 'deposit.',
    'namespace'  => 'Booking',
], function () {

    Route::resource('deposit-car','DepositController');
    
});



/*
 * Menu
 */
Route::group([
    'prefix'     => 'articles/category',
    'as'         => 'category.',
    'namespace'  => 'Category',
], function () {

    Route::resource('category-list','CategoryController');
    
});


/*
 * Menu
 */
Route::group([
    'prefix'     => 'articles/tag',
    'as'         => 'tag.',
    'namespace'  => 'Tag',
], function () {

    Route::resource('tag-list','TagController');
    
});


