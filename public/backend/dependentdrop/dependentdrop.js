$(document).ready(function(){
	    $(".DependentDropdown").change(function(e) {
	    	var DependentDropdownRoute 	= $(this).attr("data-select_url");
    		var DependentDropdownName 	= $(this).attr("data-traget_select_name");
    		var DependentDropdownToken 	= $(this).attr("data-select_token");
    		
    		if((DependentDropdownRoute&&DependentDropdownName)&&DependentDropdownToken!=null){

		    	$('select[name='+DependentDropdownName+']').addClass('clickmycom-loading');
		    	$('select[name='+DependentDropdownName+']').html('<option>Loading ...</option>');

		        e.preventDefault();
	                $.ajaxSetup({
	                    headers: {
	                        'X-CSRF-TOKEN': DependentDropdownToken
	                    }
	            	});

	            	$.ajax({
	                    type: 'POST',
	                    url: DependentDropdownRoute,
	                    data: {this_SelectValue: e.target.value},
	                    dataType: "json",
	                    
	                    success: function (data) {
	                       if(data.length>0){
	                       		$('select[name='+DependentDropdownName+']').removeClass("clickmycom-loading");
		                       	$('select[name='+DependentDropdownName+']').empty();
		                       	$('select[name='+DependentDropdownName+']').html('<option>Select Data</option>');
		                       	$.each(data,function(index,data){
		                       		var option_data = '<option value='+data.id+'>'+data.name+'</option>';
		                       		$('select[name='+DependentDropdownName+']').append(option_data);
		                       	});
	                       }else{
	                       		$('select[name='+DependentDropdownName+']').removeClass("clickmycom-loading");
	                       		$('select[name='+DependentDropdownName+']').html('<option>Empty Data</option>');
	                       }
	                    },
	                    error: function (data) {
		                       	$('select[name='+DependentDropdownName+']').empty();
		                       	$('select[name='+DependentDropdownName+']').html('<option>Error</option>');
	                    }
	                }); //end ajax send
	    	} // end if
	    }); //end  $(".DependentDropdown").change
}); // end ready jquery
