<?php

namespace App\Http\Controllers;

use App\Models\Paypal\Invoice;
use App\Models\Paypal\Item;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Srmklive\PayPal\Services\AdaptivePayments;
use Srmklive\PayPal\Services\ExpressCheckout;

use App\Models\ExtraOption\ExtraOptionDB;
use App\Models\Cars\Description\Cars;
use App\Models\OutSideArea\OutSideAreaDB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Frontend\Booking\SendBooking;
use App\Models\Booking\CarBooking;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
class PayPalController extends Controller
{
      use AuthenticatesUsers;
    /**
     * @var ExpressCheckout
     */
    protected $provider;

    public function __construct()
    {
        $this->provider = new ExpressCheckout();
    }

    public function getIndex(Request $request)
    {
        $response = [];
        if (session()->has('code')) {
            $response['code'] = session()->get('code');
            session()->forget('code');
        }

        if (session()->has('message')) {
            $response['message'] = session()->get('message');
            session()->forget('message');
        }

        return view('welcome', compact('response'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getExpressCheckout(Request $request)
    {

        $recurring = ($request->get('mode') === 'recurring') ? true : false;
        
        $cart = $this->getCheckoutData($recurring);

        try {
            $response = $this->provider->setExpressCheckout($cart, $recurring);

            return redirect($response['paypal_link']);

        } catch (\Exception $e) {
            $invoice = $this->createInvoice($cart, 'Invalid');

            session()->put(['code' => 'danger', 'message' => "Error processing PayPal payment for Order $invoice->id!"]);
        }

    }

    /**
     * Process payment on PayPal.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getExpressCheckoutSuccess(Request $request)
    {

        // print_r($session_get = session()->get('check_booking'));
        // exit;
        $recurring = ($request->get('mode') === 'recurring') ? true : false;
        $token = $request->get('token');
        $PayerID = $request->get('PayerID');

        $cart = $this->getCheckoutData($recurring);

        // Verify Express Checkout Token
        $response = $this->provider->getExpressCheckoutDetails($token);
                
        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
            if ($recurring === true) {
                $response = $this->provider->createMonthlySubscription($response['TOKEN'], 9.99, $cart['subscription_desc']);
                if (!empty($response['PROFILESTATUS']) && in_array($response['PROFILESTATUS'], ['ActiveProfile', 'PendingProfile'])) {
                    $status = 'Processed';
                } else {
                    $status = 'Invalid';
                }
            } else {
                // Perform transaction on PayPal
                $payment_status = $this->provider->doExpressCheckoutPayment($cart, $token, $PayerID);

                // echo "<pre>";
                // print_r($payment_status);
                // echo "</pre>";
                // exit;
                $status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'];
            }

            $invoice = $this->createInvoice($cart, $status);

            if ($invoice->paid == 1) {
                session()->put(['code' => 'success', 'message' => "Order $invoice->id has been paid successfully!"]);
                
                if(env('SMS_SEND_ORDER') == true){
                    include(app_path() . '/Http/Controllers/sms.php');
                }

                if(env('EMAIL_SEND_ORDER') == true){
                    Mail::send('frontend.mail.booking-invoice', ['data' => $cart], function($message) {
                        $message->to(session()->get('check_booking')['email'], 'Tutorials Point');
                         $message->cc('info@ecocar.co.th','Info Ecocar');
                        $message->subject('Test mail Booking');
                        $message->from(config('mail.from.address'), config('mail.from.name'));
                    });
                }


            } else {
                session()->put(['code' => 'danger', 'message' => "Error processing PayPal payment for Order $invoice->id!"]);
            }
            session()->forget('check_booking');
            // return redirect('/');
            

            //  $session_get = session()->get('result-booking');
            // return view('frontend.booking.result_payment')->with([
            //     'data' => $session_get
            // ]);

            return redirect('result-booking');
        }
    }

    public function getAdaptivePay()
    {
        $this->provider = new AdaptivePayments();

        $data = [
            'receivers'  => [
                [
                    'email'   => 'johndoe@example.com',
                    'amount'  => 10,
                    'primary' => true,
                ],
                [
                    'email'   => 'janedoe@example.com',
                    'amount'  => 5,
                    'primary' => false,
                ],
            ],
            'payer'      => 'EACHRECEIVER', // (Optional) Describes who pays PayPal fees. Allowed values are: 'SENDER', 'PRIMARYRECEIVER', 'EACHRECEIVER' (Default), 'SECONDARYONLY'
            'return_url' => url('payment/success'),
            'cancel_url' => url('payment/cancel'),
        ];

        $response = $this->provider->createPayRequest($data);
        dd($response);
    }

    /**
     * Parse PayPal IPN.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function notify(Request $request)
    {
        if (!($this->provider instanceof ExpressCheckout)) {
            $this->provider = new ExpressCheckout();
        }

        $request->merge(['cmd' => '_notify-validate']);
        $post = $request->all();

        $response = (string) $this->provider->verifyIPN($post);

        $logFile = 'ipn_log_'.Carbon::now()->format('Ymd_His').'.txt';
        Storage::disk('local')->put($logFile, $response);
    }

    /**
     * Set cart data for processing payment on PayPal.
     *
     * @param bool $recurring
     *
     * @return array
     */
    protected function getCheckoutData($recurring = false)
    {

         $session_get = session()->get('check_booking');
         $data = $session_get;

        if($data['pick_up_code'] != $data['drop_off_code']){
           $dropoff_outside = OutSideAreaDB::where('area_code',$data['drop_off_code'])->pluck('price')->first();
           $data['dropoff_outside'] = $dropoff_outside;
        }else{
            $dropoff_outside = 0;
            $data['dropoff_outside'] = $dropoff_outside;
        }
           
        $car = Cars::where('id',$session_get['this_car'])->first();

        $start_date=date_create(session()->get('check_booking')['pick_up_date']);
        $end_date=date_create(session()->get('check_booking')['drop_off_date']);
        $diff=date_diff($start_date,$end_date);
        $diff_day = $diff->format("%a");
       

        $order_id = Invoice::all()->count() + 1;

            if ($recurring === true) {
                $data['items'] = [
                    [
                        'name'  => 'Monthly Subscription '.config('paypal.invoice_prefix').' #'.$order_id,
                        'price' => 0,
                        'qty'   => 1,
                    ],
                ];

                $data['return_url'] = url('/paypal/ec-checkout-success?mode=recurring');
                $data['subscription_desc'] = 'Monthly Subscription '.config('paypal.invoice_prefix').' #'.$order_id;
            } else {

                $price_car_engine_cc = 0;
                if($data['car_engine_cc'] <= 1200){
                    $price_car_engine_cc = ENV('PRICE_CC_CARS_1200');
                }

                if($data['car_engine_cc'] >= 1200 && $data['car_engine_cc'] <= 1500){
                    $price_car_engine_cc = ENV('PRICE_CC_CARS_1500');
                }

                if($data['car_engine_cc'] >= 1800){
                    $price_car_engine_cc = ENV('PRICE_CC_CARS_1800');
                }
          
                $data['items'] = [
                    [
                        'name'  => strtoupper($car->car_brand).' '.strtoupper($car->car_generation),
                        'price' => $car->price*$diff_day,
                        'qty'   => 1,
                    ],
                    [
                        'name'  => 'Dropoff Outside',
                        'price' => $dropoff_outside,
                        'qty'   => 1,
                    ],
                    [
                        'name'  => 'Deposit',
                        'price' => $price_car_engine_cc,
                        'qty'   => 1,
                    ],
                ];

            if($diff_day == 0){
                $diff_day=1;
            }

            if(!empty(session()->get('check_booking')['extra_option'])){
               $data['query_option'] = ExtraOptionDB::whereIn('id',session()->get('check_booking')['extra_option'])->get()->toArray();
            

                foreach ($data['query_option'] as $key_query_option => $value_query_option) {

                    if(!empty($session_get['extra_option_qty'][$value_query_option['id']])){
                         $data_qty =  $session_get['extra_option_qty'][$value_query_option['id']];
                    }else{
                        $data_qty = 1;
                    }

                    if($value_query_option['extra_option_code'] != 'AA112209' && $value_query_option['extra_option_code'] != 'AA313233'){
                        $data['items'][] = array(
                                    'name'      =>  $value_query_option['extra_option_name'],
                                    'price'     =>  $value_query_option['extra_option_price']*$diff_day,
                                    'qty'       =>  $data_qty
                                );
                    }else{
                        $data['items'][] = array(
                                    'name'      =>  $value_query_option['extra_option_name'],
                                    'price'     =>  $value_query_option['extra_option_price'],
                                    'qty'       =>  $data_qty
                                );
                    }
                    
                }
            }

            $data['return_url'] = url('/paypal/ec-checkout-success');
        }


        // Mail::send(new SendBooking($data));

        $data['total_day'] = $diff_day;
        $data['invoice_id'] = config('paypal.invoice_prefix').'_'.$order_id;
        $data['invoice_description'] = "รายการที่ Invoice #$order_id ";
        $data['cancel_url'] = url('/');

        $total = 0;
        foreach ($data['items'] as $item) {
            $total += $item['price'] * $item['qty'];
        }

        if(!empty(ENV('DISCOUNT_FOR_PAY_NOW'))){
            $discount_for_pay_now = ENV('DISCOUNT_FOR_PAY_NOW');
        }else{
             $discount_for_pay_now = 0;
        }

        $data['total'] = $total-$discount_for_pay_now;


        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";

        // exit;

       

        return $data;
    }

    /**
     * Create invoice.
     *
     * @param array  $cart
     * @param string $status
     *
     * @return \App\Invoice
     */
    protected function createInvoice($cart, $status)
    {   

        $session_get = session()->get('check_booking');

        $session_get['items'] = $cart['items'];
        $session_get['total_day'] = $cart['total_day'];
        
    
        $user_login = null;
        $user_register ='no';
        if(!empty(auth()->user())){
            $user_login = auth()->user()->toArray()['id'];
            $user_register='yes';
        }
 
        $invoice = new Invoice();
        $invoice->title = $cart['invoice_description'];
        $invoice->price = $cart['total'];

        if (!strcasecmp($status, 'Completed') || !strcasecmp($status, 'Processed')) {
            $invoice->paid = 1;
        } else {
            $invoice->paid = 0;
        }
        $invoice->save();

        $order_id = Invoice::all()->count();

            CarBooking::create([
            'car_booking_code'       => str_random(10),
            'fk_car_id'              => $cart['this_car'],
            'fk_invoice'             => $order_id,
            'pick_up'                => $cart['pick_up_code'],
            'drop_off'               => $cart['drop_off_code'],
            'pick_up_date'           => $cart['pick_up_date'],
            'pick_up_time'           => $cart['pick_up_time_text'],
            'drop_off_date'          => $cart['drop_off_date'],
            'drop_off_time'          => $cart['drop_off_time_text'],
            'car_option'             => json_encode($session_get['extra_option']),
            'extra_option_qty'       => json_encode($session_get['extra_option_qty']),
            'comment_remark'         => $cart['comment_remark'],
            'customer_first_name'    => $cart['first_name'],
            'customer_last_name'     => $cart['last_name'],
            'customer_email'         => $cart['email'],
            'customer_phone'         => $cart['phone'],
            'customer_flight_info'   => $cart['travel_flight_info'],
            'customer_flight_number' => $cart['travel_flight_number'],
            'start_date'             => $cart['pick_up_date'].' '.$cart['pick_up_time_text'],
            'end_date'               => $cart['drop_off_date'].' '.$cart['drop_off_time_text'],
            'data_sesstion'          => json_encode($session_get),
            'type_pay'               => 'paypal',
            'deposit'                => 'pay',
            'fk_staff_approve'       => null,
            'fk_user_booking'        => $user_login,
            'user_register'          => $user_register
                
        ]);
        
        $cart['status'] = "Order $order_id has been paid successfully!";
        $cart['status_pay'] = 1;

        if(!empty(ENV('DISCOUNT_FOR_PAY_NOW'))){
            $discount_for_pay_now = ENV('DISCOUNT_FOR_PAY_NOW');
        }else{
             $discount_for_pay_now = 0;
        }
        
        $cart['total_price'] = $cart['total']-$discount_for_pay_now;

        session()->put('result-booking',$cart);
        //session()->forget('check_booking');

        // collect($cart['items'])->each(function ($product) use ($invoice) {
        //     $item = new Item();
        //     $item->invoice_id = $invoice->id;
        //     $item->item_name = $product['name'];
        //     $item->item_price = $product['price'];
        //     $item->item_qty = $product['qty'];

        //     $item->save();
        // });

        return $invoice;
    }


}
