<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\Article\ArticleModel;
use Carbon\Carbon;
use App\Models\Auth\User;
use App\Models\Customers\Customers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;
/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    use AuthenticatesUsers;


    public function index()
    {
        $get_user =auth()->user()->email;

        $data['user'] = DB::table('customers','users')
        ->select(
                 'customers.*',
                 'users.*',
                 'customers.id as customers_id'
                )
        ->join('users','users.email','=','customers.email_customer')
        ->where('customers.email_customer',$get_user)
        ->first();


        $booking  =  DB::table('car_booking','cars','invoices')
                    ->select('car_booking.*',
                            'car_booking.id as car_booking_id',
                             'cars.*',
                             'cars.price as car_price',
                             'invoices.*',
                             'invoices.id as invoices_id',
                             'invoices.created_at as invoice_created_at'
                            )
                    ->join('cars','cars.id','=', 'car_booking.fk_car_id')
                    ->join('invoices','invoices.id', '=', 'car_booking.fk_invoice')
                    ->where('car_booking.fk_user_booking','=',auth()->user()->id)
                    ->get()->toArray();
      
       

        return view('frontend.user.dashboard')->with([
            'data' => $data,
            'booking'   => $booking

        ]);


    }
}
