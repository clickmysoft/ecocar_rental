<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\Article\ArticleModel;
use Carbon\Carbon;
/**
 * Class AccountController.
 */
class AccountController extends Controller
{
    
    public function __construct()
    {
        $this->article = ArticleModel::paginate(5);
    }


    public function index()
    {

    	 return view('frontend.user.account')->with([
            'article'   => $this->article,
        ]);

    }
}
