<?php

namespace App\Http\Controllers\Frontend\Booking;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ExtraOption\ExtraOptionDB;
use App\Models\Cars\Description\Cars;
use App\Models\OutSideArea\OutSideAreaDB;
use App\Http\Requests\Frontend\Booking\CustomerRequest;
use Carbon\Carbon;

class CustomerDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     


          $session_get = session()->get('check_booking');

        

        if(empty(session()->get('check_booking')['type_pay'])){
            return redirect('/');
        }else{
            return view('frontend.booking.customer_detail')->with([
                    'data' => session()->get('check_booking')
            ]);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();


        if(empty(session()->get('check_booking')['type_pay'])){
            return redirect('/');
        }else{
            $session_get = session()->get('check_booking');
            $data['pick_up_code']  = $session_get['pick_up_code'];
            $data['pick_up_date']  = $session_get['pick_up_date'];
            $data['pick_up_time']  = $session_get['pick_up_time'];
            $data['pick_up_text']  = $session_get['pick_up_text'];
            $data['pick_up_date_text'] = $session_get['pick_up_date_text'];
            $data['pick_up_time_text']  = $session_get['pick_up_time_text'];
            $data['drop_off_code']  = $session_get['drop_off_code'];
            $data['drop_off_date']  = $session_get['drop_off_date'];
            $data['drop_off_time']  = $session_get['drop_off_time'];
            $data['drop_off_text']  = $session_get['drop_off_text'];
            $data['drop_off_date_text'] = $session_get['drop_off_date_text'];
            $data['drop_off_time_text']  = $session_get['drop_off_time_text'];
            $data['type_pay']  = $session_get['type_pay'];
            $data['this_car']  = $session_get['this_car'];

            $data['extra_option']  = @$input['extra_option'];
            $data['extra_option_qty']  = @$input['extra_option_qty'];
            $data['comment_remark']  = @$input['comment_remark'];

            session()->put('check_booking',$data);
        }

        return view('frontend.booking.customer_detail')->with([
                'data' => session()->get('check_booking')
        ]);


    }


    public function save_form_customer(CustomerRequest $request){
         $input = $request->all();



         if(empty(session()->get('check_booking')['type_pay'])){
            return redirect('/');
        }else{
            $session_get = session()->get('check_booking');
            $data['pick_up_code']  = $session_get['pick_up_code'];
            $data['pick_up_date']  = $session_get['pick_up_date'];
            $data['pick_up_time']  = $session_get['pick_up_time'];
            $data['pick_up_text']  = $session_get['pick_up_text'];
            $data['pick_up_date_text'] = $session_get['pick_up_date_text'];
            $data['pick_up_time_text']  = $session_get['pick_up_time_text'];
            $data['drop_off_code']  = $session_get['drop_off_code'];
            $data['drop_off_date']  = $session_get['drop_off_date'];
            $data['drop_off_time']  = $session_get['drop_off_time'];
            $data['drop_off_text']  = $session_get['drop_off_text'];
            $data['drop_off_date_text'] = $session_get['drop_off_date_text'];
            $data['drop_off_time_text']  = $session_get['drop_off_time_text'];
            $data['type_pay']  = $session_get['type_pay'];
            $data['this_car']  = $session_get['this_car'];

            $data['extra_option']  = $session_get['extra_option'];
            $data['extra_option_qty']  = $session_get['extra_option_qty'];
            $data['comment_remark']  = $session_get['comment_remark'];

            $data['first_name']  = $input['first_name'];
            $data['last_name']  = $input['last_name'];
            $data['email']  = $input['email'];
            $data['phone']  = $input['phone'];
            $data['travel_flight_info']  = $input['travel_flight_info'];
            $data['travel_flight_number']  = $input['travel_flight_number'];


            $data['pick_up_date_thai'] = $this->DateThai($session_get['pick_up_date']);
            $data['drop_off_date_thai'] = $this->DateThai($session_get['drop_off_date']);
            $data['dropoff_outside'] = 0;

            if($data['pick_up_code'] != $data['drop_off_code']){
               $dropoff_outside = OutSideAreaDB::where('area_code',$data['drop_off_code'])->pluck('price')->first();
               $data['dropoff_outside'] = $dropoff_outside;
            }else{
                $dropoff_outside = 0;
                $data['dropoff_outside'] = $dropoff_outside;
            }

            if(!empty(session()->get('check_booking')['extra_option'])){
               $data['query_option'] = ExtraOptionDB::whereIn('id',session()->get('check_booking')['extra_option'])->get()->toArray();
            }

            $car_piece = Cars::where('id',$session_get['this_car'])->first();



            $date = Carbon::parse($session_get['pick_up_date']);
            $now = Carbon::parse($session_get['drop_off_date']);

            $diff_day = $date->diffInDays($now);

            $data['car_engine_cc'] = $car_piece->car_engine_cc;


            $price_car_engine_cc = 0;
            if($car_piece->car_engine_cc <= 1200){
                $price_car_engine_cc = ENV('PRICE_CC_CARS_1200');
            }

            if($car_piece->car_engine_cc >= 1200 && $car_piece->car_engine_cc <= 1500){
                $price_car_engine_cc = ENV('PRICE_CC_CARS_1500');
            }

            if($car_piece->car_engine_cc >= 1800){
                $price_car_engine_cc = ENV('PRICE_CC_CARS_1800');
            }

            $data['price_car_engine_cc'] = $price_car_engine_cc;


            $price_of_book_car = 0;
            if($data['type_pay'] == 'pay_later'){
                $price_of_book_car = ENV('PRICE_OF_PRE_BOOK_CAR');
                 $discount_for_pay_now = 0;
            }else{
                 $discount_for_pay_now = ENV('DISCOUNT_FOR_PAY_NOW');
            }
            
            // echo "<pre>";
            // print_r($data);
            // echo "</pre>";
            // exit;

            session()->put('check_booking',$data);


            return view('frontend.booking.payment')->with([
                'data' => session()->get('check_booking'),
                'car_piece' => ($car_piece->price*$diff_day)-$discount_for_pay_now,
                'data_car'  => $car_piece,
                'dropoff_outside' => $dropoff_outside,
                'car_engine_cc' => $car_piece->car_engine_cc,
                'price_car_engine_cc' => $price_car_engine_cc,
                'type_pay'  => $data['type_pay'],
                'price_of_book_car' => $price_of_book_car,
                 'discount_for_pay_now' => $discount_for_pay_now,
            ]);


        }

    
    }


     public function DateThai($strDate){
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
