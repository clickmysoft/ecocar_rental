<?php

namespace App\Http\Controllers\Frontend\Booking;
    
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cars\Description\Cars;
use App\Models\Booking\CarBooking;
use Illuminate\Support\Facades\DB;
use App\Models\Article\ArticleModel;
use App\Http\Requests\Frontend\Booking\CalendarRequest;

class VehiclesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(empty(session()->get('check_booking')['pick_up_date'])){
            return redirect('/');
        }else{
            $start_date = str_replace("/","-",session()->get('check_booking')['pick_up_date'])." ".session()->get('check_booking')['pick_up_time_text'].':00';
            $end_date = str_replace("/","-",session()->get('check_booking')['drop_off_date'])." ".session()->get('check_booking')['drop_off_time_text'].':00';

            //$data['cars'] = Cars::orderByRaw("RAND()")->paginate(5);

            $data['this_car_booking'] = DB::table('cars','car_booking')
            ->select(
                     'cars.*',
                     'car_booking.*',
                     'cars.id as cars_id'
                    )
            ->join('car_booking','car_booking.fk_car_id','=','cars.id')
            ->whereBetween('start_date',[$start_date,$end_date])
            ->Where('start_date', '>=', $start_date)
            ->orwhere('start_date', '<=',$end_date)
             ->Where('end_date', '>=', $start_date)
            ->orwhere('end_date', '<=',$end_date)
            // ->where('drop_off_time', '>=', $start_date)
            // ->orwhere('drop_off_time', '<=',$end_date)
            ->Where('pick_up_date', '<=', $start_date)
            ->Where('pick_up_date', '>=', $end_date)
            ->get()->toArray();

            // echo "<pre>";
            // print_r($data['this_car_booking']);
            // echo "</pre>";
            // exit;
            
            //  echo "<pre>";
            // print_r(session()->get('check_booking'));
            // echo "</pre>";
            // exit;


            $array_id_car = array();
            $car_id = array(); 
            foreach ($data['this_car_booking'] as $key => $value) {
                 $car_id[]=$value->cars_id;
            }

            //$data['cars'] = Cars::whereNotIn('id',$car_id)->orderByRaw("RAND()")->paginate(5);


            $data['cars'] = DB::table('cars')
            ->select(
                     'cars.*',
                     'cars.id as cars_id'
                    )
            ->where('branch_code',session()->get('check_booking')['pick_up_code'])
            ->where('cars.status','=','enabled')
            ->whereNotIn('id',$car_id)->get()->toArray();
            
            // echo "<pre>";
            // print_r($data['cars']);
            // echo "</pre>";
            // exit;    

            $car_array = array();
            foreach ($data['cars'] as $key_car => $value_car) {
                $car_array[$value_car->car_generation][$value_car->branch_car_code] = $value_car;
            }

            
            
            
            $result_car = array();
          
            foreach (array_keys($car_array) as $key_car_array => $value_car_array) {
                $result_car[] = array_rand($car_array[$value_car_array],1);
            }

            // // $oop = Cars::whereNotIn('branch_car_code',['bangwa34,bangwa08,bangwa43,bangwa50'])->orderByRaw("RAND()")->paginate(5);

            // $now = date('Y-m-d H:i:s');

            $booking_car = DB::table('cars')
            ->select(
                     'cars.*',
                     'cars.id as cars_id'
                    )
            ->whereIn('branch_car_code',$result_car)->orderByRaw("RAND()")->paginate(5);

            return view('frontend.booking.vehicles')->with([
                'cars' => $booking_car,
                'data' => session()->get('check_booking'),
                'discount_for_pay_now' => ENV('DISCOUNT_FOR_PAY_NOW')
            ]);
        }
    }




    public function find(CalendarRequest $request){

            $input = $request->all();

      
            $x = str_replace('box-branch-','',$input['pickup']);
            
            $data['pick_up_code']  = str_replace('box-branch-','',$input['pickup']);
            $data['pick_up_date']  = $input['pick_up_date_submit'];
            $data['pick_up_time']  = strtotime($input['pick_up_time']);
            $data['pick_up_text']  = $input['pick-up-point'];
            $data['pick_up_date_text'] = $input['pick_up_date'];
            $data['pick_up_time_text']  = $input['pick_up_time'];

                
            $data['drop_off_code']  = str_replace('box-branch-','',$input['dropoff']);
            $data['drop_off_date']  = $input['drop_off_date_submit'];
            $data['drop_off_time']  = strtotime($input['drop_off_time']);
            $data['drop_off_text']  = $input['drop-car-point'];
            $data['drop_off_date_text'] = $input['drop_off_date'];
            $data['drop_off_time_text']  = $input['drop_off_time'];


            session()->put('check_booking',$data);

           return redirect()->route('frontend.vehicles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
