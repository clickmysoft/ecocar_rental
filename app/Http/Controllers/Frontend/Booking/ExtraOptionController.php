<?php

namespace App\Http\Controllers\Frontend\Booking;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cars\Description\Cars;
use App\Models\Booking\CarBooking;
use Illuminate\Support\Facades\DB;
use App\Models\Article\ArticleModel;
use App\Http\Requests\Frontend\Booking\CalendarRequest;
use App\Models\ExtraOption\ExtraOptionDB;
class ExtraOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(empty(session()->get('check_booking')['type_pay'])){
            return redirect('/');
        }else{
            $extra_option = ExtraOptionDB::where('status','enabled')->get()->toArray();
            return view('frontend.booking.extra_option')->with([
                    'extra_option' => $extra_option,
                    'data' => session()->get('check_booking')
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        if(empty(session()->get('check_booking')['pick_up_date'])){
            return redirect('/');
        }else{
            $session_get = session()->get('check_booking');
            $data['pick_up_code']  = $session_get['pick_up_code'];
            $data['pick_up_date']  = $session_get['pick_up_date'];
            $data['pick_up_time']  = $session_get['pick_up_time'];
            $data['pick_up_text']  = $session_get['pick_up_text'];
            $data['pick_up_date_text'] = $session_get['pick_up_date_text'];
            $data['pick_up_time_text']  = $session_get['pick_up_time_text'];
            $data['drop_off_code']  = $session_get['drop_off_code'];
            $data['drop_off_date']  = $session_get['drop_off_date'];
            $data['drop_off_time']  = $session_get['drop_off_time'];
            $data['drop_off_text']  = $session_get['drop_off_text'];
            $data['drop_off_date_text'] = $session_get['drop_off_date_text'];
            $data['drop_off_time_text']  = $session_get['drop_off_time_text'];
            $data['type_pay']  = $input['type_pay'];
            $data['this_car']  = $input['car_id'];
            session()->put('check_booking',$data);
        }

        $extra_option = ExtraOptionDB::where('status','enabled')->get()->toArray();


    
        return view('frontend.booking.extra_option')->with([
                'extra_option' => $extra_option,
                'data' => session()->get('check_booking')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
