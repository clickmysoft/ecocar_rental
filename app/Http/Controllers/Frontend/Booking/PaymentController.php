<?php

namespace App\Http\Controllers\Frontend\Booking;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cars\Description\Cars;
use App\Models\Booking\CarBooking;
use Illuminate\Support\Facades\DB;
use App\Models\Article\ArticleModel;
use App\Models\ExtraOption\ExtraOptionDB;
use App\Models\Paypal\Invoice;
use Carbon\Carbon;
use App\Models\OutSideArea\OutSideAreaDB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
class PaymentController extends Controller
{
        use AuthenticatesUsers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

          return view('frontend.booking.payment')->with([
            'article' => '',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pay_later(){

        if(empty(session()->get('check_booking')['type_pay'] == 'pay_later')){
            return redirect('/');
        }else{
            //echo "<pre>";
            $session_get = session()->get('check_booking');
            // print_r($session_get);
            // echo "</pre>";


            // exit;
            $data['car_booking_code']      = str_random(10);
            $data['fk_car_id'] = $session_get['this_car'];
            $data['pick_up'] = $session_get['pick_up_code'];
            $data['pick_up_text'] = $session_get['pick_up_text'];
            $data['pick_up_date'] = $session_get['pick_up_date'];
            $data['pick_up_time'] = $session_get['pick_up_time_text'];

            $data['type_pay'] = $session_get['type_pay'];

            $data['drop_off'] = $session_get['drop_off_code'];
            $data['drop_off_text'] = $session_get['drop_off_text'];
            $data['drop_off_date'] = $session_get['drop_off_date'];
            $data['drop_off_time'] = $session_get['drop_off_time_text'];


            $data['car_option'] = $session_get['extra_option_qty'];
            $data['comment_remark'] = $session_get['comment_remark'];

            $data['customer_first_name'] = $session_get['first_name'];
            $data['customer_last_name'] = $session_get['last_name'];
            $data['customer_email'] = $session_get['email'];
            $data['customer_phone'] = $session_get['phone'];
            $data['customer_flight_info'] = $session_get['travel_flight_info'];
            $data['customer_flight_number'] = $session_get['travel_flight_number'];


            $data['start_date'] = $session_get['pick_up_date'].' '.$session_get['pick_up_time_text'];
            $data['end_date'] = $session_get['drop_off_date'].' '.$session_get['drop_off_time_text'];
           

            $order_id = Invoice::all()->count() + 1;
            $data['title'] = "รายการ Invoice #$order_id ";


             if($session_get['pick_up_code'] != $session_get['drop_off_code']){
               $dropoff_outside = OutSideAreaDB::where('area_code',$session_get['drop_off_code'])->pluck('price')->first();
               $session_get['dropoff_outside'] = $dropoff_outside;
            }else{
                $dropoff_outside = 0;
                $session_get['dropoff_outside'] = $dropoff_outside;
            }

            $date = Carbon::parse($data['start_date']);
            $now = Carbon::parse($data['end_date']);
            $diff_day = $date->diffInDays($now);

            $car = Cars::where('id',$session_get['this_car'])->first();




            $price_car_engine_cc = 0;
            if($car->car_engine_cc <= 1200){
                $price_car_engine_cc = ENV('PRICE_CC_CARS_1200');
            }

            if($car->car_engine_cc >= 1200 && $car->car_engine_cc <= 1500){
                $price_car_engine_cc = ENV('PRICE_CC_CARS_1500');
            }

            if($car->car_engine_cc >= 1800){
                $price_car_engine_cc = ENV('PRICE_CC_CARS_1800');
            }


            $data['items'] = [
                [
                    'name'  => strtoupper($car->car_brand).' '.strtoupper($car->car_generation),
                    'price' => $car->price*$diff_day,
                    'qty'   => 1,
                ],
                [
                    'name'  => 'Dropoff Outside',
                    'price' => $dropoff_outside,
                    'qty'   => 1,
                ],
                [
                    'name'  => 'Deposit',
                    'price' => $price_car_engine_cc,
                    'qty'   => 1,
                ],
                [
                    'name'  => 'Price Pre Book Car',
                    'price' => ENV('PRICE_OF_PRE_BOOK_CAR'),
                    'qty'   => 1,
                ],
            ];


            if(!empty(session()->get('check_booking')['extra_option'])){
               $data['query_option'] = ExtraOptionDB::whereIn('id',session()->get('check_booking')['extra_option'])->get()->toArray();
            

                foreach ($data['query_option'] as $key_query_option => $value_query_option) {

                    if(!empty($session_get['extra_option_qty'][$value_query_option['id']])){
                         $data_qty =  $session_get['extra_option_qty'][$value_query_option['id']];
                    }else{
                        $data_qty = 1;
                    }

                    //Delivery Service Car  && Service Return A Car
                    if($value_query_option['extra_option_code'] != 'AA112209' && $value_query_option['extra_option_code'] != 'AA313233'){
                        $data['items'][] = array(
                                    'name'      =>  $value_query_option['extra_option_name'],
                                    'price'     =>  $value_query_option['extra_option_price']*$diff_day,
                                    'qty'       =>  $data_qty
                                );
                    }else{

                        $data['items'][] = array(
                                    'name'      =>  $value_query_option['extra_option_name'],
                                    'price'     =>  $value_query_option['extra_option_price'],
                                    'qty'       =>  $data_qty
                                );
                    }
                    
                }
            }




            $data['total_day'] = $diff_day;
            $total = 0;
            foreach ($data['items'] as $item) {
                $total += $item['price'] * $item['qty'];
            }

            $session_get['total_day'] =  $data['total_day'];
            $session_get['items'] = $data['items'];

            $data['data_sesstion'] = json_encode($session_get);


            $user_login = null;
            $user_register ='no';
            if(!empty(auth()->user())){
                $user_login = auth()->user()->toArray()['id'];
                $user_register='yes';
            }
           
            
            $invoice = new Invoice();
            $invoice->title = $data['title'];
            $invoice->price = $total;
            $invoice->paid = 0;
            $invoice->save();


       
            CarBooking::create([
                 'car_booking_code'      => str_random(10),
                'fk_car_id'             => $data['fk_car_id'],
                'fk_invoice'            => $order_id,
                'pick_up'               => $data['pick_up'],
                'drop_off'              => $data['drop_off'],
                'pick_up_date'          => $data['pick_up_date'],
                'pick_up_time'          => $data['pick_up_time'],
                'drop_off_date'         => $data['drop_off_date'],
                'drop_off_time'         => $data['drop_off_time'],
                'car_option'            => json_encode($session_get['extra_option']),
                'extra_option_qty'      => json_encode($session_get['extra_option_qty']),
                'comment_remark'        => $data['comment_remark'],
                'customer_first_name'   => $data['customer_first_name'],
                'customer_last_name'    => $data['customer_last_name'],
                'customer_email'        => $data['customer_email'],
                'customer_phone'        => $data['customer_phone'],
                'customer_flight_info'  => $data['customer_flight_info'],
                'customer_flight_number' => $data['customer_flight_number'],
                'start_date'             => $data['start_date'],
                'end_date'               => $data['end_date'],
                'data_sesstion'          => $data['data_sesstion'],
                'type_pay'               => $data['type_pay'],
                'deposit'                => 'not_pay',
                'fk_staff_approve'       => null,
                'fk_user_booking'        => $user_login,
                'user_register'          => $user_register
                
            ]);

            $data['status'] = "Order $order_id has been paid successfully!";
            $data['status_pay'] = 0;
            $data['total_price'] = $total;

            session()->put(['code' => 'success', 'message' => "Order $order_id has been paid successfully!"]);

            Cars::where('id',$session_get['this_car'])
                        ->update([
                                    'status'      => 'booking'
                        ]);

            if(env('SMS_SEND_ORDER') == true){
                include(app_path() . '/Http/Controllers/sms.php');
            }

            if(env('EMAIL_SEND_ORDER') == true){
                
                $data_mail['first_name'] = $data['customer_first_name'];
                $data_mail['last_name'] = $data['customer_last_name'];
                $data_mail['invoice_id'] = $order_id;
                $data_mail['items'] = $data['items'];
                $data_mail['pick_up_date'] = $data['pick_up_date'];
                $data_mail['pick_up_time_text'] = $session_get['pick_up_time_text'];

                $data_mail['drop_off_date'] = $data['drop_off_date'];
                $data_mail['drop_off_time_text'] = $session_get['drop_off_time_text'];
                $data_mail['total'] = $total;
                $data_mail['dataset'] = $data;
                
                Mail::send('frontend.mail.booking-pay-later-invoice', ['data' => $data_mail], function($message) {
                    $message->to(session()->get('check_booking')['email'], 'Tutorials Point');
                    $message->cc('info@ecocar.co.th','Info Ecocar');
                    
                    $message->subject('Test mail Booking');
                    $message->from(config('mail.from.address'), config('mail.from.name'));
                });
            }
            
        }

        // return view('frontend.booking.result_payment')->with([
        //     'data' => $data
        // ]);

      
        // retrieving data

        session()->put('result-booking',$data);
        
        return redirect('result-booking');

    }
    
    public function result_payment(){
        $session_get = session()->get('result-booking');
        $response['code'] = session()->get('code');
        $response['message'] = session()->get('message');
   
        // echo "<pre>";
        // print_r($session_get);
        // echo "</pre>";
        // exit;
        return view('frontend.booking.result_payment')->with([
            'data' => $session_get,
            'response' => $response

        ]);
    }

}
