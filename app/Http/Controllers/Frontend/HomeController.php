<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Article\ArticleModel;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{

    public function __construct()
    {
        $this->article = ArticleModel::paginate(5);
    }

    public function index()
    {
        session()->forget('check_booking');
        session()->forget('result-booking');
        $response = [];
        if (session()->has('code')) {
            $response['code'] = session()->get('code');
            session()->forget('code');
        }

        if (session()->has('message')) {
            $response['message'] = session()->get('message');
            session()->forget('message');
        }
        
        return view('frontend.index')->with([
            'article' => $this->article,
            'response' => $response
        ]);
    }

    public function document(){
      
        return view('frontend.document')->with([
            'article' => $this->article,
        ]);

    }

    public function about(){

        return view('frontend.about')->with([
            'article' => $this->article,
        ]);
    }

    public function service(){

        return view('frontend.service')->with([
            'article' => $this->article,
        ]);
    }

    public function ceo(){
        return view('frontend.ceo')->with([
            'article' => $this->article,
        ]);
    }

    public function csr(){
        return view('frontend.csr')->with([
            'article' => $this->article,
        ]);
    }

    public function questions(){
        return view('frontend.questions')->with([
            'article' => $this->article,
        ]);
    }

    public function branch(){

        return view('frontend.branch')->with([
            'article' => $this->article,
        ]);
    }

    public function pattaya(){

        return view('frontend.branch.pattaya')->with([
            'article' => $this->article,
        ]);
    }

    public function downtown(){

        return view('frontend.branch.downtown')->with([
            'article' => $this->article,
        ]);
    }

    public function onnut(){

        return view('frontend.branch.onnut')->with([
            'article' => $this->article,
        ]);
    }

    public function bangwa(){

        return view('frontend.branch.bangwa')->with([
            'article' => $this->article,
        ]);
    }

    public function ladprao(){

        return view('frontend.branch.ladprao')->with([
            'article' => $this->article,
        ]);
    }

    public function nonthaburi(){

        return view('frontend.branch.nonthaburi')->with([
            'article' => $this->article,
        ]);
    }

    public function samrong(){

        return view('frontend.branch.samrong')->with([
            'article' => $this->article,
        ]);
    }

    public function ubonratchathani(){

        return view('frontend.branch.ubonratchathani')->with([
            'article' => $this->article,
        ]);
    }

    public function chiangmai(){

        return view('frontend.branch.chiangmai')->with([
            'article' => $this->article,
        ]);
    }

}

