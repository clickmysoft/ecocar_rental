<?php

namespace App\Http\Controllers\Frontend\Auth;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Customers\Customers;
use App\Http\Requests\Frontend\Auth\CustomerRequest;
use JsValidator;
use DataTables;
use Carbon\Carbon;
use App\Models\Auth\User;






class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRequest $request, $id)
    {
         $input = $request->all();
         
        $data['prefix_name_customer'] = trim($input['prefix_name_customer']);
        $data['first_name_customer'] = trim($input['first_name_customer']);
        $data['last_name_customer'] = trim($input['last_name_customer']);
        $data['nick_name_customer'] = trim($input['nick_name_customer']);
        $data['email_customer'] = trim($input['email_customer']);
        $data['phone_customer'] = trim($input['phone_customer']);
        $data['line_id_customer'] = trim($input['line_id_customer']);
        $data['id_card_customer'] = trim($input['id_card_customer']);
        $data['personal_card_address'] = trim($input['personal_card_address']);
        $data['present_address'] = trim($input['present_address']);
        $data['company_name'] = trim($input['company_name']);
        $data['position'] = trim($input['position']);
        $data['tel_office'] = trim($input['tel_office']);
        $data['explain_use_car'] = trim($input['explain_use_car']);
        $data['province_use_car'] = trim($input['province_use_car']);
        $data['prefix_name_beneficiary'] = trim($input['prefix_name_beneficiary']);
        $data['first_name_beneficiary'] = trim($input['first_name_beneficiary']);
        $data['last_name_beneficiary'] = trim($input['last_name_beneficiary']);
        $data['relation_beneficiary'] = trim($input['relation_beneficiary']);
        $data['phone_beneficiary'] = trim($input['phone_beneficiary']);
        $data['personal_card_img'] = $request->file('personal_card_img');
        $data['driver_license_card_img'] = $request->file('driver_license_card_img');
        $data['credit_card_img'] = $request->file('credit_card_img');
        $data['image_profile'] = $request->file('image_profile');
        
     
         /*
        |--------------------------------------------------------------------------
        | ทำการตรวจสอบประเภทไฟล์ที่รับเข้ามา เพื่อใช้ในการตั้งประเภทไฟล์
        |--------------------------------------------------------------------------
        */
        if(!empty($request->file('personal_card_img'))){
            $file = $request->file('personal_card_img');
            $path = base_path().'/public/customers/'.$data['id_card_customer'].'/';
            $filename = $file->getClientOriginalName();
            $type_img = array('.png', '.jpg', '.gif','.jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = 'personal_id';
            $data['personal_card_img'] = $data['id_card_customer'].'/'.$filename.$type_name;
            
            if(isset($file)){
                $file->move($path,$filename.$type_name);
            }
        }else{
             $data['personal_card_img'] = $input['personal_card_img_old'];
        }


        if(!empty($request->file('driver_license_card_img'))){
            $file = $request->file('driver_license_card_img');
            $path = base_path().'/public/customers/'.$data['id_card_customer'].'/';
            $filename = $file->getClientOriginalName();
            $type_img = array('.png', '.jpg', '.gif','.jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = 'driver_license_card';
            $data['driver_license_card_img'] = $data['id_card_customer'].'/'.$filename.$type_name;
            
            if(isset($file)){
                $file->move($path,$filename.$type_name);
            }
        }else{
            $data['driver_license_card_img'] = $input['driver_license_card_img_old'];
        }

        if(!empty($request->file('credit_card_img'))){
            $file = $request->file('credit_card_img');
            $path = base_path().'/public/customers/'.$data['id_card_customer'].'/';
            $filename = $file->getClientOriginalName();
            $type_img = array('.png', '.jpg', '.gif','.jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = 'credit_card';
            $data['credit_card_img'] = $data['id_card_customer'].'/'.$filename.$type_name;

            if(isset($file)){
                $file->move($path,$filename.$type_name);
            }
        }else{
            $data['credit_card_img'] = $input['credit_card_img_old'];
        }
        
        if(!empty($request->file('image_profile'))){
            $file = $request->file('image_profile');
            $path = base_path().'/public/customers/'.$data['id_card_customer'].'/';
            $filename = $file->getClientOriginalName();

            $type_img = array('.png', '.jpg', '.gif','.jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = 'image_profile';
            $data['image_profile'] = $data['id_card_customer'].'/'.$filename.$type_name;

            if(isset($file)){
                $file->move($path,$filename.$type_name);
            }
        }else{
            $data['image_profile'] = $input['image_profile_old'];
        }



        Customers::where('username',$id)->update($data);
        
        if(empty($data['image_profile'])){
            $data['image_profile'] = 'avatars/dufult/avatar12_big.png';
        }
        User::where('username',$id)->update([
            'first_name'    => $input['first_name_customer'],
            'last_name'     => $input['last_name_customer'],
            'phone'         => $input['phone_customer'],
            'nickname'      => $input['nick_name_customer'],
            'company_name'  => $input['company_name'],
            'position_name' => $input['position'],
            'user_image'    => 'customers/'.$data['image_profile']
        ]);
        
        return redirect()->back();
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
