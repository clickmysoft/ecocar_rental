<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article\ArticleModel;
use Carbon\Carbon;

use App\Models\Category\CategoryModel;
use App\Models\Tag\TagModel;

use App\Models\Article\CategoryPostModel;
use App\Models\Article\TagPostModel;
use Illuminate\Support\Facades\DB;
use App\Models\Auth\User;

class ArticleController extends Controller
{
    
    public function __construct()
    {
        $this->article = ArticleModel::paginate(5);
    }

    public function index()
    {
        
        $data = ArticleModel::orderBy('id','desc')->paginate(5);

        return view('frontend.article.index')->with([
            'data'      => $data,
        ]);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = $this->article ;
        $data = ArticleModel::find($id);
        if(empty($data)){
            abort(404);
        }

        $data['created_at_humans']  = $data['created_at']->diffForHumans();
        $data['created_at_time']    = $this->DateThai($data['created_at']);
        $data['category']           = CategoryModel::where('status','enabled')->get();
        $data['fk_create_by']       = $this->getUser($data['fk_create_by']);


        $x2 = CategoryPostModel::where('post_id',$id)->get()->pluck('category_id')->toArray();
        $x3 = TagPostModel::where('post_id',$id)->get()->pluck('tag_id')->toArray();

        $data['category_post']  =  DB::table('category')
                    ->select('category.*')
                    ->whereIn('category.id',$x2)
                    ->get()->toArray();

        $data['post_tag']  =  DB::table('tags')
                    ->select('tags.*')
                    ->whereIn('tags.id',$x3)
                    ->get()->toArray();

         //$data['post_tag']  =   TagModelL::whereIn('id',$x3)->get()->toArray();
            
        //               echo "<pre>";
        // print_r($data['post_tag']);
        // echo "</pre>";
        // // exit;
                    
        
        //$data['category_post']      = CategoryPostModel::where('post_id',$data['id'])->get();


        // foreach ($data['category'] as $key => $value) {
        //         echo $value->id;
        //         $xx = CategoryPostModel::select(DB::raw('count(*) as count','category_id'))->where('category_id', '=', 2)->get()->toArray();
        //         echo "<pre>";
        //         print_r($xx);
        //         echo "</pre>";
        // }
        
        // echo "<pre>";
        // print_r($data['category']);
        // echo "</pre>";
        // exit;

        return view('frontend.article.show',compact('data','article'));

      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function DateThai($strDate){
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }

    public function getUser($id){
        $data = User::find($id);
        return $data['first_name'].' '.$data['last_name'];
    }

}
