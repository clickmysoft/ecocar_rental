<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article\ArticleModel;
use App\Models\Promotion\PromotionModel;

class PromotionController extends Controller
{
    

    public function __construct()
    {
        $this->article = ArticleModel::paginate(5);
    }


    public function index()
    {
        $data['promotion'] =  PromotionModel::paginate(6);

        foreach ($data['promotion'] as $key => $value) {

                $start_date = $this->DateThai($value['start_date']);
                $end_date   = $this->DateThai($value['end_date']);

                $start = explode("00:00 น.",$start_date);
                $end = explode("00:00 น.",$end_date);

                if($start[1] == null || $start[1] == ''){
                    $value['start_date'] = $start[0];
                }
                
                if($end[1] == null || $end[1] == ''){
                    $value['end_date'] = $end[0];
                }

        }

        return view('frontend.promotion.index')->with([
            'article'   =>  $this->article,
            'data'      =>  $data,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = $this->article ;
        $data = PromotionModel::find($id);
        if(empty($data)){
            abort(404);
        }

        $data['created_at_humans']  = $data['created_at']->diffForHumans();
        $data['created_at_time']    = $this->DateThai($data['created_at']);
     
        return view('frontend.promotion.show',compact('data','article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function DateThai($strDate){
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];

        if($strDay<=9){
            $strDay = "<small style='color:#fff;'>0</small>".$strDay;
        }
        return '<i class="fa fa-calendar"></i> '."$strDay $strMonthThai $strYear ".$strHour.':'.$strMinute.' น.';
    }

}
