<?php

namespace App\Http\Controllers\Backend\Customers;


use App\Http\Requests;
use Request;    //ใส่ use Request ที่อยู่ใน aliases แทน  use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customers\Customers;
use App\Http\Requests\Backend\Customers\CustomerRequest;
use JsValidator;
use DataTables;
use Carbon\Carbon;
use App\Models\Auth\User;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$query = Customers::where('active',1)->orderBy('id', 'desc')->get();
        // return view('backend.customer.index',compact('query'));
         return view('backend.customer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $rule = new CustomerRequest();
        $validator = JsValidator::make($rule->rules());
        
        return view('backend.customer.create')->with([
            'validator' => $validator,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        
        $input = $request->all();
        $data =$input;

        $data['prefix_name_customer'] = trim($input['prefix_name_customer']);
        $data['first_name_customer'] = trim($input['first_name_customer']);
        $data['last_name_customer'] = trim($input['last_name_customer']);
        $data['nick_name_customer'] = trim($input['nick_name_customer']);
        $data['email_customer'] = trim($input['email_customer']);
        $data['phone_customer'] = trim($input['phone_customer']);
        $data['line_id_customer'] = trim($input['line_id_customer']);
        $data['id_card_customer'] = trim($input['id_card_customer']);
        $data['personal_card_address'] = trim($input['personal_card_address']);
        $data['present_address'] = trim($input['present_address']);
        $data['company_name'] = trim($input['company_name']);
        $data['position'] = trim($input['position']);
        $data['experience'] = trim($input['experience']);
        $data['tel_office'] = trim($input['tel_office']);
        $data['explain_use_car'] = trim($input['explain_use_car']);
        $data['province_use_car'] = trim($input['province_use_car']);
        $data['prefix_name_beneficiary'] = trim($input['prefix_name_beneficiary']);
        $data['first_name_beneficiary'] = trim($input['first_name_beneficiary']);
        $data['last_name_beneficiary'] = trim($input['last_name_beneficiary']);
        $data['relation_beneficiary'] = trim($input['relation_beneficiary']);
        $data['phone_beneficiary'] = trim($input['phone_beneficiary']);
        $data['personal_card_img'] = $request->file('personal_card_img');
        $data['driver_license_card_img'] = $request->file('driver_license_card_img');
        $data['credit_card_img'] = $request->file('credit_card_img');
        $data['image_profile'] = $request->file('image_profile');
        $data['customers_description'] = trim($input['customers_description']);
        $data['active'] = trim($input['active']);
        $data['username'] = $data['username'];
        $data['password'] = bcrypt($data['password']);
      

        /*
        |--------------------------------------------------------------------------
        | ทำการตรวจสอบประเภทไฟล์ที่รับเข้ามา เพื่อใช้ในการตั้งประเภทไฟล์
        |--------------------------------------------------------------------------
        */
        if(!empty($request->file('personal_card_img'))){
            $file = $request->file('personal_card_img');
            $path = base_path().'/public/customers/'.$data['id_card_customer'].'/';
            $filename = $file->getClientOriginalName();
            $type_img = array('.png', '.jpg', '.gif','.jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = 'personal_id';
            $data['personal_card_img'] = $data['id_card_customer'].'/'.$filename.$type_name;
            
            if(isset($file)){
                $file->move($path,$filename.$type_name);
            }
        }


        if(!empty($request->file('driver_license_card_img'))){
            $file = $request->file('driver_license_card_img');
            $path = base_path().'/public/customers/'.$data['id_card_customer'].'/';
            $filename = $file->getClientOriginalName();
            $type_img = array('.png', '.jpg', '.gif','.jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = 'driver_license_card';
            $data['driver_license_card_img'] = $data['id_card_customer'].'/'.$filename.$type_name;
            
            if(isset($file)){
                $file->move($path,$filename.$type_name);
            }
        }

        if(!empty($request->file('credit_card_img'))){
            $file = $request->file('credit_card_img');
            $path = base_path().'/public/customers/'.$data['id_card_customer'].'/';
            $filename = $file->getClientOriginalName();
            $type_img = array('.png', '.jpg', '.gif','.jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = 'credit_card';
            $data['credit_card_img'] = $data['id_card_customer'].'/'.$filename.$type_name;

            if(isset($file)){
                $file->move($path,$filename.$type_name);
            }
        }
        
        if(!empty($request->file('image_profile'))){
            $file = $request->file('image_profile');
            $path = base_path().'/public/customers/'.$data['id_card_customer'].'/';
            $filename = $file->getClientOriginalName();

            $type_img = array('.png', '.jpg', '.gif','.jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = 'image_profile';
            $data['image_profile'] = $data['id_card_customer'].'/'.$filename.$type_name;

            if(isset($file)){
                $file->move($path,$filename.$type_name);
            }
        }

        if(Customers::create($data)){

            User::create([
                'employee_id' => $data['id_card_customer'],
                'first_name' => $data['first_name_customer'],
                'last_name' => $data['last_name_customer'],
                'email' => $data['email_customer'],
                'nickname' => $data['nick_name_customer'],
                'company_name' => $data['company_name'],
                'position_name' => $data['position'],
                'username' => $data['username'],
                'password' => $data['password'],
                'phone' => $data['phone_customer'],
                'active' => 1,
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => 1,
                'type_register' => 'customer'


             ]);


            $request->session()->flash('message', 'jSuccess');
            return redirect()->back();
        }

    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $customer = Customers::find($id);
        if(empty($customer)){
            abort(404);
        }
            
        return view('backend.customer.show',compact('customer'));
        // echo "<pre>";
        // print_r($customer);
        // echo "</pre>";
        

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Customers::findOrFail($id);
        echo $data->delete();
    }


    public function data_processing(){
        // $query = Customers::orderBy('created_at', 'desc')->get();
        
        return  DataTables::of(Customers::where('type_register','customer'))
                ->editColumn('created_at', function ($query) {
                    return $query->created_at->diffForHumans();
                })

                ->editColumn('status', function ($query) {
                    $color = '';
                    if($query->status == 'enabled'){
                        $color = 'green';
                    }

                    if($query->status == 'closed'){
                        $color = 'red';
                    }

                    if($query->status == 'booking'){
                        $color = '#5f5798';
                    }

                   return '<label style="color:'.$color.'">'.strtoupper($query->status).'</label>';
                })

                ->addColumn('full_name', function ($query) {
                    return $query->prefix_name_customer.$query->first_name_customer." ".$query->last_name_customer;
                })

                ->addColumn('action', function ($query) {
                    return '
                            <a href="'.route("admin.customer.management.show",$query->id).'" class="btn btn-info" style="border-radius: 50px;background-color: #48beb3;padding: 4px 8px; margin-bottom: 0 !important;">
                                <i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="View" data-original-title="View"></i>
                            </a>

                            <a href="'.route("admin.cars.car.edit",$query->id).'" class="btn btn-primary" style="border-radius: 50px;background-color: #cc1156;    border-color: transparent;padding: 4px 8px;margin-bottom: 0 !important; ">
                                <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit" data-original-title="Edit"></i>
                            </a>

                            <button  onclick="destroy('.$query->id.')" id="this-data-'.$query->id.'" 
                                style="border-radius: 50px;background-color: #0068b2; border-color: transparent;padding: 4px 9px;"
                                class="btn btn-primary deleteproductModal"
                                data-toggle="modal" 
                                data-destroy_token="'.csrf_token().'"
                                data-destroy_name="'.$query->prefix_name_customer.$query->first_name_customer." ".$query->last_name_customer.'"
                                data-destroy_route="'.url('admin/customer/management/'.$query->id).'"
                                data-destroy_redirect="'.route("admin.customer.management.index").'"
                                >
                                <i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="Delete" data-original-title="Delete"></i>
                            </button>
                    ';
                })

                ->rawColumns(['action','status'])
                ->toJson();

    }
}
