<?php

namespace App\Http\Controllers\Backend\Cars;
use App\Http\Requests;
use Request;    //ใส่ use Request ที่อยู่ใน aliases แทน  use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cars\Description\Cars;
use App\Http\Requests\Backend\Cars\CarRequest;
use JsValidator;
use DataTables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Carbon\Carbon;
class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

           
     

        //$query = Cars::orderBy('branch_car_code', 'desc')->get();
        
        //$query = Cars::select('id','branch_car_code');
        return view('backend.cars.index');
        //return DataTables::of($query)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rule = new CarRequest();
        $validator = JsValidator::make($rule->rules());
        
        return view('backend.cars.create')->with([
            'validator' => $validator,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarRequest $request)
    {
        // $product_type = new Cars();
        // $product_type->company_name = "naruto";
        // print_r($product_type);
            $input = $request->all();
            $data['company_name'] =trim($input['company_name']);
            $data['branch_name'] =trim($input['branch_name']);
            $data['license_plate'] =trim($input['license_plate']);
            $data['license_province'] = trim($input['license_province']);
            $data['color']  = trim($input['color']);
            $data['body_number'] =trim($input['body_number']);
            $data['engine_number'] =trim($input['engine_number']);
            $data['branch_car_code'] =trim($input['branch_car_code']);
            $data['car_brand'] =trim($input['car_brand']);
            $data['car_generation'] =trim($input['car_generation']);
            $data['car_nickname'] =trim($input['car_nickname']);
            $data['car_generation_detail'] =trim($input['car_generation_detail']);
            $data['car_engine_cc'] =trim($input['car_engine_cc']);
            $data['car_type']   = trim($input['car_type']);
            $data['car_human_qty']  = trim($input['car_human_qty']);
            $data['car_baggage_qty'] = trim($input['car_baggage_qty']);
            $data['car_type_fule'] =json_encode(@$input['car_type_fule']);
            $data['car_type_gear'] =trim(@$input['car_type_gear']);
            $data['car_pressure_front'] =trim($input['car_pressure_front']);
            $data['car_pressure_back'] =trim($input['car_pressure_back']);
            $data['car_mile'] =str_replace(',', '',trim($input['car_mile']));
            $data['car_door_qty'] =trim($input['car_door_qty']);
            $data['car_image'] = $request->file('car_image');
            $data['car_description'] =trim($input['car_description']);
            $data['car_option'] =json_encode($input['car_option']);
            $data['car_company_insurance'] =trim($input['car_company_insurance']);
            $data['car_type_insurance'] =trim($input['car_type_insurance']);
            $data['car_date_register'] =date('Y-m-d', strtotime(trim($input['car_date_register'])));
            $data['car_enddate_insurance'] =date('Y-m-d', strtotime(trim($input['car_enddate_insurance'])));
            $data['car_enddate_act'] =date('Y-m-d', strtotime(trim($input['car_enddate_act'])));
            $data['car_enddate_tax'] =date('Y-m-d', strtotime(trim($input['car_enddate_tax'])));
            $data['status'] =trim($input['status']);



            switch ($data['branch_name']) {
                case 'Downtown':
                     $data['branch_code'] = 'uoSDDYle';
                    break;

                 case 'Onnut':
                     $data['branch_code'] = 'izLH906r';
                    break;

                 case 'Bangwa':
                     $data['branch_code'] = 'uzIEA3or';
                    break;

                 case 'Ladprao':
                     $data['branch_code'] = 'SAHbAkUY';
                    break;

                 case 'Nonthaburi':
                     $data['branch_code'] = 'Z07e1ySa';
                    break;

                 case 'Samrong':
                     $data['branch_code'] = 'hfNms3YD';
                    break;

                 case 'UbonAirport':
                     $data['branch_code'] = 'E4Rd5kYV';
                    break;

                 case 'ChiangmaiAirport':
                     $data['branch_code'] = 'tzRyDFfT';
                    break;

                 case 'Pattaya':
                     $data['branch_code'] = 'zrYbZkkK';
                    break;

                 case 'DonmueangAirport':
                     $data['branch_code'] = 'TifEfmFR';
                    break;

                case 'SuvarnabhumiAirport':
                     $data['branch_code'] = 'ML05GTQO';
                    break;

                case 'HatyaiAirport':
                     $data['branch_code'] = 'EbRREe9k';
                    break;
              
            }

            /*
            |--------------------------------------------------------------------------
            | ทำการตรวจสอบประเภทไฟล์ที่รับเข้ามา เพื่อใช้ในการตั้งประเภทไฟล์
            |--------------------------------------------------------------------------
            */
            if(!empty($request->file('car_image'))){
                $file = $request->file('car_image');
                $path = base_path().'/public/cars/';
                $filename = $file->getClientOriginalName();

                $type_img = array('.png', '.jpg', '.gif','jpeg');
                $index = array();
                foreach ($type_img as  $value) {
                    if(strpos($filename,$value)){
                        array_push($index,strpos($filename,$value));
                    }
                }
                $type_name = substr($filename,@$index[0]); //.jpg
                $filename = trim($input['branch_car_code']);
                $data['car_image'] = $filename.$type_name;
            }

            if(Cars::create($data)){
                if(isset($file)){
                    $file->move($path,$filename.$type_name);
                }
                $request->session()->flash('message', 'jSuccess');
                return redirect()->back();
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $set_body_page['data-page'] = "shopping_cart";
        $set_body_page['class'] = "shopping-cart-page";

        $car = Cars::find($id);
        if(empty($car)){
            abort(404);
        }

        $car['car_brand_new'] =  strtoupper($car['car_brand']);
        $car['car_generation_new'] =  strtoupper($car['car_generation']);
        $car['car_nickname_new'] =  config('cars.'.app()->getLocale().'.nickname.'.$car['car_brand'].'.'.$car['car_generation'].'.'.$car['car_nickname'])[0];
        $car['car_generation_detail_new'] =  strtoupper(config('cars.'.app()->getLocale().'.variant.'.$car['car_brand'].'.'.$car['car_generation'].'.'.$car['car_nickname'].'.'.$car['car_generation_detail'])[0]);
        $car['car_type_gear_new'] = config('cars.gear.'.$car['car_type_gear'])[0];

        $car['car_type_insurance_new'] = config('cars.'.app()->getLocale().'.'.$car['car_company_insurance'].'.'.'type2')[0];
        $car['car_company_insurance_new'] = config('cars.'.app()->getLocale().'.insurance.'.$car['car_company_insurance'])[0];
        $car['car_date_register_new'] = $this->DateThai($car['car_date_register_new']);
      

        $car['exp_car_date_register'] = $this->count_exp($car['car_date_register']);
        $car['exp_car_enddate_insurance'] = $this->count_exp($car['car_enddate_insurance']);
        $car['exp_car_enddate_act'] = $this->count_exp($car['car_enddate_act']);
        $car['exp_car_enddate_tax'] = $this->count_exp($car['car_enddate_tax']);


        return view('backend.cars.show',compact('car','set_body_page'));
    }


    public function count_exp($date){

            $now = Carbon::now();
            $end = Carbon::parse($date);
            $exp_date = $end->diffInDays($now);
            return $exp_date;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        session()->put('key_edit_post',$id);

        $data = Cars::find($id);
        if(empty($data)){
            abort(404);
        }
        $rule = new CarRequest();
        $validator = JsValidator::make($rule->rules());
        return view('backend.cars.edit',compact('data'))->with([
            'validator' => $validator,
        ]) ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarRequest $request, $id)
    {
        $input = $request->all();

        // echo "<pre>";
        // print_r($input);
        // echo "</pre>";
        // exit;


        $input = $request->all();
            $data['company_name'] =trim($input['company_name']);
            $data['branch_name'] =trim($input['branch_name']);
            $data['license_plate'] =trim($input['license_plate']);
            $data['license_province'] = trim($input['license_province']);
            $data['color']  = trim($input['color']);
            $data['body_number'] =trim($input['body_number']);
            $data['engine_number'] =trim($input['engine_number']);
            $data['branch_car_code'] =trim($input['branch_car_code']);
            $data['car_brand'] =trim($input['car_brand']);
            $data['car_generation'] =trim($input['car_generation']);
            $data['car_nickname'] =trim($input['car_nickname']);
            $data['car_generation_detail'] =trim($input['car_generation_detail']);
            $data['car_engine_cc'] =trim($input['car_engine_cc']);
            $data['car_type']   = trim($input['car_type']);
            $data['car_human_qty']  = trim($input['car_human_qty']);
            $data['car_baggage_qty'] = trim($input['car_baggage_qty']);
            $data['car_type_fule'] =json_encode(@$input['car_type_fule']);
            $data['car_type_gear'] =trim(@$input['car_type_gear']);
            $data['car_pressure_front'] =trim($input['car_pressure_front']);
            $data['car_pressure_back'] =trim($input['car_pressure_back']);
            $data['car_mile'] =str_replace(',', '',trim($input['car_mile']));
            $data['car_door_qty'] =trim($input['car_door_qty']);
            $data['car_image'] = $request->file('car_image');
            $data['car_description'] =trim($input['car_description']);
            $data['car_option'] =json_encode($input['car_option']);
            $data['car_company_insurance'] =trim($input['car_company_insurance']);
            $data['car_type_insurance'] =trim($input['car_type_insurance']);
            $data['car_date_register'] =date('Y-m-d', strtotime(trim($input['car_date_register'])));
            $data['car_enddate_insurance'] =date('Y-m-d', strtotime(trim($input['car_enddate_insurance'])));
            $data['car_enddate_act'] =date('Y-m-d', strtotime(trim($input['car_enddate_act'])));
            $data['car_enddate_tax'] =date('Y-m-d', strtotime(trim($input['car_enddate_tax'])));
            $data['status'] =trim($input['status']);



            switch ($data['branch_name']) {
                case 'Downtown':
                     $data['branch_code'] = 'uoSDDYle';
                    break;

                 case 'Onnut':
                     $data['branch_code'] = 'izLH906r';
                    break;

                 case 'Bangwa':
                     $data['branch_code'] = 'uzIEA3or';
                    break;

                 case 'Ladprao':
                     $data['branch_code'] = 'SAHbAkUY';
                    break;

                 case 'Nonthaburi':
                     $data['branch_code'] = 'Z07e1ySa';
                    break;

                 case 'Samrong':
                     $data['branch_code'] = 'hfNms3YD';
                    break;

                 case 'UbonAirport':
                     $data['branch_code'] = 'E4Rd5kYV';
                    break;

                 case 'ChiangmaiAirport':
                     $data['branch_code'] = 'tzRyDFfT';
                    break;

                 case 'Pattaya':
                     $data['branch_code'] = 'zrYbZkkK';
                    break;

                 case 'DonmueangAirport':
                     $data['branch_code'] = 'TifEfmFR';
                    break;

                case 'SuvarnabhumiAirport':
                     $data['branch_code'] = 'ML05GTQO';
                    break;

                case 'HatyaiAirport':
                     $data['branch_code'] = 'EbRREe9k';
                    break;
              
            }

            /*
            |--------------------------------------------------------------------------
            | ทำการตรวจสอบประเภทไฟล์ที่รับเข้ามา เพื่อใช้ในการตั้งประเภทไฟล์
            |--------------------------------------------------------------------------
            */
            if(!empty($request->file('car_image'))){
                $file = $request->file('car_image');
                $path = base_path().'/public/cars/';
                $filename = $file->getClientOriginalName();

                $type_img = array('.png', '.jpg', '.gif','jpeg');
                $index = array();
                foreach ($type_img as  $value) {
                    if(strpos($filename,$value)){
                        array_push($index,strpos($filename,$value));
                    }
                }
                $type_name = substr($filename,@$index[0]); //.jpg
                $filename = trim($input['branch_car_code']);
                $data['car_image'] = $filename.$type_name;
            }

            if(empty($data['car_image'])){
                $data['car_image'] = $input['old_image'];
            }
        // echo "<pre>";
        // print_r($input);
        // echo "</pre>";
        // exit;        


             Cars::where('id',$id)->update($data);
              if(isset($file)){
                    $file->move($path,$filename.$type_name);
                }
         $request->session()->flash('message', 'jSuccess');
            return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $car = Cars::findOrFail($id);
            // if(!empty($car->car_image)){
            //     $image_path = public_path().'/cars/'.$car->car_image;
            //     if(file_exists($image_path)){
            //         unlink($image_path);
            //     }
            // }
            echo $car->delete();
    }

    public function get_generation()
    {
        if(isset($_POST['brand'])){
            foreach (config('cars.'.app()->getLocale().'.brands') as $key2 => $value2) {
                if($key2 == $_POST['brand']){
                    foreach ($value2 as $key => $value) {
                        echo "<option ";
                        echo " data-brand=".strtolower($key2)." value=".$key.">";
                        echo $value[0];
                        echo "</option>";
                    }
                }
            }
        }
    }


    public function get_nickname()
    {
        if(isset($_POST['generation']) && isset($_POST['brand'])){
            if(!empty(config('cars.'.app()->getLocale().'.nickname.'.$_POST['brand'].'.'.$_POST['generation']))){
                foreach (config('cars.'.app()->getLocale().'.nickname.'.$_POST['brand'].'.'.$_POST['generation']) as $key => $value) {
                    echo "<option  ";
                    echo " data-brand=".$_POST['brand']." data-nick=".$_POST['generation']."  value=".$key.">";
                    echo $value[0];
                    echo "</option>";
                }
            }else{
                echo '';
            }
        }
    }

    public function get_variant()
    {
     
            if(!empty(config('cars.'.app()->getLocale().'.variant.'.$_POST['brand'].'.'.$_POST['nickname'].'.'.$_POST['variant']))){
                foreach (config('cars.'.app()->getLocale().'.variant.'.$_POST['brand'].'.'.$_POST['nickname'].'.'.$_POST['variant']) as $key => $value) {
                    echo "<option data-brand=".$_POST['brand']." data-nick=".$_POST['nickname']."  data-variant=".$_POST['variant']."  value=".$key.">";
                    echo $value[0];
                    echo "</option>";
                }
            }else{
                echo '';
            }
      
    }

    public function get_type_insurance()
    {
        if(isset($_POST['insurance'])){
            foreach (config('insurance.'.app()->getLocale().'.'.$_POST['insurance']) as $key => $value) {
                        $img = asset("backend/img/various/clipboard-list.svg");
                        echo "<option  ";
                        echo " data-img-src='".$img."' value=".$key.">";
                        echo $value[0];
                        echo "</option>";
            }
        }else{
            echo '';
        }

    }

    public function get_branch_code($branch_code){
        $new_branch_code = array();
       
        $query = Cars::select('branch_car_code','branch_name','license_plate')
        ->orWhere('branch_name',$branch_code)
        ->orderBy('branch_car_code','desc')
        ->limit(1)
        ->get();

        


        foreach ($query as $title) {

            $string = $title->branch_car_code;
            $parts = preg_split("/(,?\s+)|((?<=[a-z])(?=\d))|((?<=\d)(?=[a-z]))/i", $string);
            $num = (int)$parts[1];
            $newcode = '';
            if($num<9){
               $newcode =  "0".++$parts[1];
            }else{
               $newcode = ++$parts[1];
            }

            $newarray = explode("-",$title->branch_car_code);
            $new_branch_code['new_branch_code'] = $branch_code.$newcode;
            $new_branch_code['max_branch_code'] = $newcode;
        }

        if(empty($new_branch_code)){
            $new_branch_code['new_branch_code'] = $branch_code.'01';
            $new_branch_code['max_branch_code'] = '01';
           
        }
        
        return response()->json($new_branch_code);

        
       
    }


    /**
    * เปลี่ยนวันให้เป็นภาษาคน.
    */
    public  function dateForHumans($query){
        if($query!='1970-01-01'){
                        $created = new Carbon($query);
                        $now = Carbon::now();
                        $difference = $now;
                        $difference = ($created->diff($now)->days < 1) ? 'วันนี้' : $created->diffForHumans($now);
        }else{
             $difference = "-";
        }

        return $difference;
    }

    public function DateThai($strDate){
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }

    /**
    * เรียกใช้ข้อมูล.
    */

    public function getdata(){
        //$query = Cars::orderBy('branch_car_code', 'desc')->get();

        return  DataTables::of(Cars::query())
                ->editColumn('created_at', function ($query) {
                    return $query->created_at->diffForHumans();
                })

                ->editColumn('car_date_register', function ($query) {
                   return $this->DateThai($query->car_date_register);
                })

                 ->editColumn('car_mile', function ($query) {
                   return number_format($query->car_mile,0);
                })

                ->editColumn('car_enddate_insurance', function ($query) {
                   return $this->dateForHumans($query->car_enddate_insurance);
                })

                ->editColumn('car_enddate_act', function ($query) {
                   return $this->dateForHumans($query->car_enddate_act);
                })


                ->editColumn('car_enddate_tax', function ($query) {
                   return $this->dateForHumans($query->car_enddate_tax);
                })

                 ->editColumn('car_generation', function($query){
                   return config('cars.'.app()->getLocale().'.brands.'.$query->car_brand.'.'.$query->car_generation)[0];
                })

                ->editColumn('status', function ($query) {
                    $color = '';
                    if($query->status == 'enabled'){
                        $color = 'green';
                    }

                    if($query->status == 'closed'){
                        $color = 'red';
                    }

                    if($query->status == 'booking'){
                        $color = '#5f5798';
                    }


                   return '<label style="color:'.$color.'">'.strtoupper($query->status).'</label>';
                })

             

                ->addColumn('action', function ($query) {
                    return '
                            <a href="'.route("admin.cars.car.show",$query->id).'" class="btn btn-info" style="border-radius: 50px;background-color: #48beb3;padding: 4px 8px; margin-bottom: 0 !important;">
                                <i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="View" data-original-title="View"></i>
                            </a>

                            <a href="'.route("admin.cars.car.edit",$query->id).'" class="btn btn-primary" style="border-radius: 50px;background-color: #cc1156;    border-color: transparent;padding: 4px 8px;margin-bottom: 0 !important; ">
                                <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit" data-original-title="Edit"></i>
                            </a>

                             <button  onclick="destroy('.$query->id.')" id="this-data-'.$query->id.'" 
                                style="border-radius: 50px;background-color: #0068b2; border-color: transparent;padding: 4px 9px;"
                                class="btn btn-primary deleteproductModal"
                                data-toggle="modal"
                                data-destroy_token="'.csrf_token().'"
                                data-destroy_name="'.$query->license_plate.'"
                                data-destroy_route="'.url('admin/cars/car/'.$query->id).'"
                                data-destroy_redirect="'.route("admin.cars.car.index").'"
                                >
                                <i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="Delete" data-original-title="Delete"></i>
                            </button>
                    ';
                })

                ->rawColumns(['action','status'])
                ->toJson(true);
    }



}
