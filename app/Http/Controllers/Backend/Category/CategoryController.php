<?php

namespace App\Http\Controllers\Backend\Category;

use App\Http\Requests;
use Request;    //ใส่ use Request ที่อยู่ใน aliases แทน  use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category\CategoryModel;
use App\Http\Requests\Backend\Category\CategoryRequest;
use JsValidator;
use DataTables;
use Carbon\Carbon;
use App\Models\Auth\User;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $input = $request->all();
        $input['fk_create_by']  = auth()->user()->toArray()['id'];
        
        if($input['status'] == 'on'){
            $input['status'] = 'enabled';
        }else{
             $input['status'] = 'closed';
        }

        // echo "<pre>";
        // print_r($input);
        // echo "</pre>";
        // exit;
        if(CategoryModel::create($input)){
            $request->session()->flash('message','jSuccess');
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = CategoryModel::find($id)->toArray();
        if(empty($data)){
            abort(404);
        }

        $rule = new CategoryRequest();
        $validator = JsValidator::make($rule->rules());
        
        return view('backend.category.edit',compact('data'))->with([
            'validator' => $validator,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $input = $request->all();
       

        if(empty($input['status'])){
                 $input['status'] = 'closed';
        }else{
            $input['status'] = 'enabled';
        }   
       
     

       
        if(CategoryModel::where('id',$id)->update([
                'category_name'     =>  $input['category_name'],
                'status'            =>  $input['status']
            ])
        ){
            $request->session()->flash('message','jSuccess');
            return redirect()->back();



        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function data_processing(){
        $str ="";
        return  DataTables::of(CategoryModel::query())
                
            ->editColumn('updated_at', function ($query) {
                return $query->updated_at->diffForHumans();
            })
             
             ->editColumn('category_name', function ($query) {
                    return strip_tags(str_limit($query->category_name, 50));
                })

            ->editColumn('status', function ($query) {
                $color = '';
                if($query->status == 'enabled'){
                    $color = 'green';
                }

                if($query->status == 'closed'){
                    $color = 'red';
                }

               return '<label style="color:'.$color.'">'.strtoupper($query->status).'</label>';
            })
                
            ->addColumn('action', function ($query) {
                    return '
                            <a href="'.route("admin.category.category-list.show",$query->id).'" class="btn btn-info" style="border-radius: 50px;background-color: #48beb3;padding: 4px 8px; margin-bottom: 0 !important;">
                                <i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="View" data-original-title="View"></i>
                            </a>

                            <a href="'.route("admin.category.category-list.edit",$query->id).'" class="btn btn-primary" style="border-radius: 50px;background-color: #cc1156;    border-color: transparent;padding: 4px 8px;margin-bottom: 0 !important; ">
                                <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit" data-original-title="Edit"></i>
                            </a>

                             <button  onclick="destroy('.$query->id.')" id="this-data-'.$query->id.'" 
                                style="border-radius: 50px;background-color: #0068b2; border-color: transparent;padding: 4px 9px;"
                                class="btn btn-primary deleteproductModal"
                                data-toggle="modal"
                                data-destroy_token="'.csrf_token().'"
                                data-destroy_name="'.$query->license_plate.'"
                                data-destroy_route="'.url('admin/category/category-list/{'.$query->id).'"
                                data-destroy_redirect="'.route("admin.category.category-list.index").'"
                                >
                                <i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="Delete" data-original-title="Delete"></i>
                            </button>
                    ';
            })

            ->rawColumns(['action','status','fk_create_by'])
            ->toJson(true);
    }

    public function getUser($id){
        $data = User::find($id);
        return $data['first_name'].' '.$data['last_name'];
    }


}
