<?php

namespace App\Http\Controllers\Backend\Prices;

use App\Http\Requests;
use Request;    //ใส่ use Request ที่อยู่ใน aliases แทน  use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JsValidator;
use DataTables;
use Carbon\Carbon;
use App\Http\Requests\Backend\Prices\CarPricesReques;
use App\Models\Prices\CarPrices;
use Validator;
use App\Models\Cars\Description\Cars;
class CarPricesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('backend.prices.car_prices.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $rule = new CarPricesReques();
        $validator = JsValidator::make($rule->rules());
        
        return view('backend.prices.car_prices.create')->with([
            'validator' => $validator,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarPricesReques $request)
    {

        $input = $request->all();

        if($input['status_event'] == 'open'){

            $validator = Validator::make($request->all(), [
                'date_price_start'  => 'required',
                'date_price_end'    => 'required',
                'time_price_start'  => 'required',
                'time_price_end'    => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('admin/prices/car-prices/create')
                            ->withErrors($validator)
                            ->withInput();
            }else{

                $date_price_start = date('Y-m-d', strtotime($input['date_price_start']));
                $time_price_start = date('H:i:s', strtotime($input['time_price_start']));

                $date_price_end = date('Y-m-d', strtotime($input['date_price_end']));
                $time_price_end = date('H:i:s', strtotime($input['time_price_end']));

                $input['start_date'] = $date_price_start.' '.$time_price_start; 
                $input['end_date'] = $date_price_end.' '.$time_price_end; 

            }
        }

        if($input['branch_name'] == 'all'){
            $input['priority'] = 'medium';
        }

        $car =  Cars::select('id','branch_car_code')->where('branch_name',$input['branch_name'])->where('car_brand',$input['car_brand'])->where('car_generation',$input['car_generation'])->get()->toArray();

        $vat = 1.07;
        foreach ($car as $key_car => $value_car) {
           
             Cars::select('id','price')
                        ->where('id',$value_car['id'])
                        ->update([
                                    'price'      => (trim($input['car_prices'])*$vat)
                                ]);
        }

        if(CarPrices::create($input)){
            $request->session()->flash('message', 'jSuccess');
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = CarPrices::find($id);
        if(empty($data)){
            abort(404);
        }

        if($data['type_rental_prices']  == 'per_day'){
            $data['type_rental_prices_new'] = 'รายวัน';
        }

        if($data['type_rental_prices'] == 'per_month'){
            $data['type_rental_prices_new'] = 'รายเดือน';
        }

        if($data['type_rental_prices'] == 'per_year'){
            $data['type_rental_prices_new'] = 'รายปี';
        }

        $now = Carbon::now();
        $end = Carbon::parse($data['start_date']);
        $length = $end->diffInDays($now);

        $totalDuration = $end->diffInSeconds($now);

        
        $data['total_date'] = gmdate('H:i:s', $totalDuration);


        if($now->timestamp > $end->timestamp){
            $data['exp_date_value'] = 'close';
            $data['exp_time_value'] = 'close';
            $data['exp_date'] = 'หมดเวลา';
            $data['exp_time'] = 'ผ่านมาแล้ว '.$data['total_date'].' นาที';
            
        }else{
            $data['exp_date_value'] = 'open';
            $data['exp_time_value'] = 'open';
            $data['exp_time'] = 'เหลือเวลาอีก '.$data['total_date'].' นาที';
        }

        $data['branch_name_new'] = config('company.'.app()->getLocale().'.belong.'.$data['branch_name'])[0];
        $data['car_brand_new'] = strtoupper($data['car_brand']);
        $data['car_generation_new'] = config('cars.'.app()->getLocale().'.brands.'.$data['car_brand'].'.'.$data['car_generation'])[0];
        $data['start_date_new'] =  $this->DateThai($data['start_date']);
        $data['end_date_new']   =  $this->DateThai($data['end_date']);
        $data['status_event_new'] = strtoupper($data['status_event']);
        $data['car_prices_new'] = number_format($data['car_prices'],0);
        $data['status_new'] = strtoupper($data['status']);
        $data['created_at_new'] = $data['created_at']->diffForHumans();
        $data['priority'] = strtoupper($data['priority']);
        return view('backend.prices.car_prices.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = CarPrices::find($id)->toArray();

        
        $rule = new CarPricesReques();
        $validator = JsValidator::make($rule->rules());
        

        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // exit;

        return view('backend.prices.car_prices.edit')->with([
            'validator' => $validator,
            'data'  => $data
        ]);

      


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarPricesReques $request, $id)
    {
        $input = $request->all();


         CarPrices::where('id',$id)->update([
            'company_name'                  => $input['company_name'],
            'branch_name'                   => $input['branch_name'],
            'car_brand'                     =>  $input['car_brand'],
            'car_generation'                =>  $input['car_generation'], 
            'car_prices'                    =>  $input['car_prices'], 
            'status'                        =>  $input['status'],
            'remarks'                       =>  $input['remarks']
        ]);

        if($input['branch_name'] == 'all'){
            $input['priority'] = 'medium';
        }

        $car =  Cars::select('id','branch_car_code')->where('branch_name',$input['branch_name'])->where('car_brand',$input['car_brand'])->where('car_generation',$input['car_generation'])->get()->toArray();

        $vat = 1.07;
        foreach ($car as $key_car => $value_car) {
           
             Cars::select('id','price')
                        ->where('id',$value_car['id'])
                        ->update([
                                    'price'      => (trim($input['car_prices'])*$vat)
                                ]);
        }

         $request->session()->flash('message', 'jSuccess');
            return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function data_processing(){
        // $query = Customers::orderBy('created_at', 'desc')->get();

        return  DataTables::of(CarPrices::query())

                ->editColumn('car_brand', function ($query) {
                    return strtoupper($query->car_brand);
                })

                ->editColumn('car_prices', function ($query) {
                    return "<label style='color: #159077;font-weight: 400;'>".number_format($query->car_prices,0)."</label>";
                })

                ->editColumn('car_generation', function ($query) {
                    return config('cars.'.app()->getLocale().'.brands.'.$query->car_brand.'.'.$query->car_generation)[0];
                })

                ->addColumn('date_time', function ($query) {

                    if($query->start_date != null){

                        $now = Carbon::now();
                        $end = Carbon::parse($query->end_date);

                        if($now->timestamp > $end->timestamp){
                            return '
                               <label style="font-size:12px;background-color: white;color: black;border: 1px solid #c75757;padding: 3px;border-radius: 4px;text-decoration: line-through;">'.$this->DateThai($query->start_date).'</label>&nbsp;
                               <i style="font-size:8px;" class="fa fa-minus"></i>&nbsp;
                               <label style="font-size:12px;background-color: white;color: black;border: 1px solid #c75757;padding: 6px 5px 5px 5px;border-radius: 4px;text-decoration: line-through;"'.$this->DateThai($query->end_date).'</label>
                            ';
                        }else{
                            return '
                               <label style="font-size:12px;background-color: white;color: black;border: 1px solid #159077;padding: 3px;border-radius: 4px;">'.$this->DateThai($query->start_date).'</label>&nbsp;
                               <i style="font-size:8px;" class="fa fa-minus"></i>&nbsp;
                               <label style="font-size:12px;background-color: white;color: black;border: 1px solid #159077;padding: 6px 5px 5px 5px;border-radius: 4px;"'.$this->DateThai($query->end_date).'</label>
                            ';
                        }
                        
                    }else{
                        return '<label style="font-size:12px;">ไม่กำหนดระยะเวลา</label>';
                    }
                    
                   
                })

                ->editColumn('branch_name', function ($query) {
                    return config('company.'.app()->getLocale().'.belong.'.$query->branch_name)[0];
                })

                ->editColumn('type_rental_prices', function ($query) {
                    if($query->type_rental_prices == 'per_day'){
                        return 'รายวัน';
                    }

                    if($query->type_rental_prices == 'per_month'){
                        return 'รายเดือน';
                    }

                    if($query->type_rental_prices == 'per_year'){
                        return 'รายปี';
                    }
                })
             

                ->editColumn('status', function ($query) {
                    $color = '';
                    if($query->status == 'enabled'){
                        $color = 'green';
                    }

                    if($query->status == 'closed'){
                        $color = 'red';
                    }

                   return '<label style="color:'.$color.'">'.strtoupper($query->status).'</label>';
                })

                ->addColumn('action', function ($query) {
                    return '
                            <a href="'.route("admin.prices.car-prices.show",$query->id).'" class="btn btn-info" style="border-radius: 50px;background-color: #48beb3;padding: 4px 8px; margin-bottom: 0 !important;">
                                <i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="View" data-original-title="View"></i>
                            </a>

                            <a href="'.route("admin.prices.car-prices.edit",$query->id).'" class="btn btn-primary" style="border-radius: 50px;background-color: #cc1156;    border-color: transparent;padding: 4px 8px;margin-bottom: 0 !important; ">
                                <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit" data-original-title="Edit"></i>
                            </a>
                            
                            <button  onclick="destroy('.$query->id.')" id="this-data-'.$query->id.'" 
                                style="border-radius: 50px;background-color: #0068b2; border-color: transparent;padding: 4px 9px;"
                                class="btn btn-primary deleteproductModal"
                                data-toggle="modal" 
                                data-destroy_token="'.csrf_token().'"
                                data-destroy_name="'.$query->prefix_name_customer.$query->first_name_customer." ".$query->last_name_customer.'"
                                data-destroy_route="'.url('admin/prices/car-prices/'.$query->id).'"
                                data-destroy_redirect="'.route("admin.prices.car-prices.index").'"
                                >
                                <i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="Delete" data-original-title="Delete"></i>
                            </button>
                    ';
                })

                ->rawColumns(['action','status','date_time','car_prices']) //บอกมันว่าให้อ่านHTMLไม่งั้นมันจะไม่อ่านHTMLอ่านเป็นTEXT
                ->toJson();

    }


       /**
    * เปลี่ยนวันให้เป็นภาษาคน.
    */

     public function DateThai($strDate){
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];

        if($strDay<=9){
            $strDay = "<small style='color:#fff;'>0</small>".$strDay;
        }
        return '<i class="fa fa-calendar"></i> '."$strDay $strMonthThai $strYear ".$strHour.':'.$strMinute.' น.';
    }

  


}
