<?php

namespace App\Http\Controllers\Backend\Promotions;

use App\Http\Requests;
use Request;    //ใส่ use Request ที่อยู่ใน aliases แทน  use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JsValidator;
use DataTables;
use Carbon\Carbon;
use App\Models\Promotion\PromotionModel;
use App\Http\Requests\Backend\Promotion\PromotionRequest;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        return view('backend.promotions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $rule = new PromotionRequest();
        $validator = JsValidator::make($rule->rules());
        
        return view('backend.promotions.create')->with([
            'validator' => $validator,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PromotionRequest $request)
    {
        $input = $request->all();
        $data = $input;

        if(!empty($request->file('image_thumbnail'))){
            $file_1 = $request->file('image_thumbnail');
            $path = base_path().'/public/promotions/';
            $filename = $file_1->getClientOriginalName();

            $type_img = array('.png', '.jpg', '.gif','jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = str_random(6).'_thumbnail';
            $data['image_thumbnail'] = $filename.$type_name;
            if(isset($file_1)){
                $file_1->move($path,$filename.$type_name);
            }
        }

        if(!empty($request->file('image_full'))){
            $file_2 = $request->file('image_full');
            $path = base_path().'/public/promotions/';
            $filename = $file_2->getClientOriginalName();

            $type_img = array('.png', '.jpg', '.gif','jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = str_random(6).'_full';
            $data['image_full'] = $filename.$type_name;
            if(isset($file_2)){
                $file_2->move($path,$filename.$type_name);
            }
        }

        $data['start_date'] = date('Y-m-d', strtotime(trim($input['start_date'])));
        $data['end_date'] = date('Y-m-d', strtotime(trim($input['end_date'])));

        if(PromotionModel::create($data)){
            $request->session()->flash('message', 'jSuccess');
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = PromotionModel::find($id);
        if(empty($data)){
            abort(404);
        }
            
        return view('backend.promotions.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //session()->put('key_edit_post',$id);
        $data = PromotionModel::find($id)->toArray();
        if(empty($data)){
            abort(404);
        }
        $rule = new PromotionRequest();
        $validator = JsValidator::make($rule->rules());
        
        return view('backend.promotions.edit',compact('data'))->with([
            'validator' => $validator,
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PromotionRequest $request, $id)
    {
        $input = $request->all();
        $data = $input;
       

        if(!empty($request->file('image_thumbnail'))){
            $file_1 = $request->file('image_thumbnail');
            $path = base_path().'/public/promotions/';
            $filename = $file_1->getClientOriginalName();

            $type_img = array('.png', '.jpg', '.gif','jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = str_random(6).'_thumbnail';
            $data['image_thumbnail'] = $filename.$type_name;
            if(isset($file_1)){
                $file_1->move($path,$filename.$type_name);
            }
        }

        if(!empty($request->file('image_full'))){
            $file_2 = $request->file('image_full');
            $path = base_path().'/public/promotions/';
            $filename = $file_2->getClientOriginalName();

            $type_img = array('.png', '.jpg', '.gif','jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = str_random(6).'_full';
            $data['image_full'] = $filename.$type_name;
            if(isset($file_2)){
                $file_2->move($path,$filename.$type_name);
            }
        }

        $data['start_date'] = date('Y-m-d', strtotime(trim($input['start_date'])));
        $data['end_date'] = date('Y-m-d', strtotime(trim($input['end_date'])));

        if(PromotionModel::where('id',$id)->update([
                'title'                 =>  $data['title'],
                'description'           =>  $data['description'],
                'promotion_code'        =>  $data['promotion_code'],
                'promotion_discount'    =>  $data['promotion_discount'],
                'discount_type'         =>  $data['discount_type'],
                'start_date'            =>  $data['start_date'],
                'end_date'              =>  $data['end_date'],
                'mata_title'            =>  $data['mata_title'],
                'mata_description'      =>  $data['mata_description'],
                'mata_keyword'          =>  $data['mata_keyword'],
                'status'                =>  $data['status'],
                'image_thumbnail'       =>  $data['image_thumbnail'],
                'image_full'            =>  $data['image_full']
            ])
        ){
            $request->session()->flash('message', 'jSuccess');
            return redirect()->back();

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = PromotionModel::findOrFail($id);
        echo $data->delete();
    }


     public function data_processing(){
        // $query = Customers::orderBy('created_at', 'desc')->get();

        return  DataTables::of(PromotionModel::query())
                ->editColumn('created_at', function ($query) {
                    return $query->created_at->diffForHumans();
                })

                ->editColumn('status', function ($query) {
                    $color = '';
                    if($query->status == 'enabled'){
                        $color = 'green';
                    }

                    if($query->status == 'closed'){
                        $color = 'red';
                    }

                    if($query->status == 'booking'){
                        $color = '#5f5798';
                    }

                   return '<label style="color:'.$color.'">'.strtoupper($query->status).'</label>';
                })

                ->addColumn('action', function ($query) {
                    return '
                            <a href="'.route("admin.promotions.promotions.show",$query->id).'" class="btn btn-info" style="border-radius: 50px;background-color: #48beb3;padding: 4px 8px; margin-bottom: 0 !important;">
                                <i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="View" data-original-title="View"></i>
                            </a>

                            <a href="'.route("admin.promotions.promotions.edit",$query->id).'" class="btn btn-primary" style="border-radius: 50px;background-color: #cc1156;    border-color: transparent;padding: 4px 8px;margin-bottom: 0 !important; ">
                                <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit" data-original-title="Edit"></i>
                            </a>

                            <button  onclick="destroy('.$query->id.')" id="this-data-'.$query->id.'" 
                                style="border-radius: 50px;background-color: #0068b2; border-color: transparent;padding: 4px 9px;"
                                class="btn btn-primary deleteproductModal"
                                data-toggle="modal" 
                                data-destroy_token="'.csrf_token().'"
                                data-destroy_name="'.$query->title.'"
                                data-destroy_route="'.url('admin/promotions/promotions/'.$query->id).'"
                                data-destroy_redirect="'.route("admin.promotions.promotions.index").'"
                                >
                                <i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="Delete" data-original-title="Delete"></i>
                            </button>
                    ';
                })

                ->rawColumns(['action','status'])
                ->toJson();

    }

}
