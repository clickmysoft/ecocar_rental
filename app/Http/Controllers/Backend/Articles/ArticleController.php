<?php
namespace App\Http\Controllers\Backend\Articles;

header("X-XSS-Protection: 0");
use App\Http\Requests;
use Request;    //ใส่ use Request ที่อยู่ใน aliases แทน  use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JsValidator;
use DataTables;
use Carbon\Carbon;
use App\Models\Article\ArticleModel;
use App\Http\Requests\Backend\Article\ArticleRequest;
use App\Models\Category\CategoryModel;
use App\Models\Tag\TagModel;
use App\Http\Requests\Backend\Category\CategoryRequest;
use App\Http\Requests\Backend\FroalaWysiwygEditor\EditorUploadRequest;

use App\Models\Article\CategoryPostModel;
use App\Models\Article\TagPostModel;


class ArticleController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.articles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rule = new ArticleRequest();
        $validator = JsValidator::make($rule->rules());
        $data['type_category'] = CategoryModel::get();
        $data['tags'] = TagModel::get();
        $gen_code = preg_replace('/[0-9]+/', '',strtolower(stripslashes(str_random(13))));
        $data['code']           = $gen_code;
        return view('backend.articles.create')->with([
            'validator' => $validator,
            'data'      => $data
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {

            $input = $request->all();


            $data =   $input;
            $data['fk_create_by']  = auth()->user()->toArray()['id'];
            
            if(!empty($request->file('image_thumbnail'))){
                $file = $request->file('image_thumbnail');
                //$path = base_path().'/public/article/';
                $path = base_path().'/public/frontend/article/'.$input['code'].'/files/';
                $filename = $file->getClientOriginalName();

                $type_img = array('.png', '.jpg', '.gif','jpeg');
                $index = array();
                foreach ($type_img as  $value) {
                    if(strpos($filename,$value)){
                        array_push($index,strpos($filename,$value));
                    }
                }
                $type_name = substr($filename,@$index[0]); //.jpg
                $filename = str_random(6).'_thumbnail';
                $data['image_thumbnail'] = $filename.$type_name;
                
                if(isset($file)){
                    $file->move($path,$filename.$type_name);
                }
            }

            if(!empty($request->file('image_full'))){
                $file = $request->file('image_full');
                $path = base_path().'/public/frontend/article/'.$input['code'].'/files/';
                $filename = $file->getClientOriginalName();

                $type_img = array('.png', '.jpg', '.gif','jpeg');
                $index = array();
                foreach ($type_img as  $value) {
                    if(strpos($filename,$value)){
                        array_push($index,strpos($filename,$value));
                    }
                }
                $type_name = substr($filename,@$index[0]); //.jpg
                $filename = str_random(6).'_full';
                $data['image_full'] = $filename.$type_name;
                if(isset($file)){
                    $file->move($path,$filename.$type_name);
                }
            }

           

            if($post = ArticleModel::create($data)){
                

                foreach ($input['type_category'] as $key => $value) {
                    $category_post = new CategoryPostModel();
                    $category_post->category_id = $value;
                    $category_post->post_id = $post->id;
                    $category_post->save();
                }

                foreach ($input['tags'] as $key => $value) {
                    $tags = new TagPostModel();
                    $tags->tag_id = $value;
                    $tags->post_id = $post->id;
                    $tags->save();
                }

                $request->session()->flash('message', 'jSuccess');
                return redirect()->back();
            }

            // print_r($input);
            // echo "</pre>";
            // exit;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ArticleModel::find($id);
        if(empty($data)){
            abort(404);
        }
            
        return view('backend.articles.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        session()->flash('key_edit_post', $id);
        $data = ArticleModel::find($id);
        if(empty($data)){
            abort(404);
        }

        $rule = new ArticleRequest();
        $validator = JsValidator::make($rule->rules());
        $data['type_category'] = CategoryModel::get();
        $data['tags'] = TagModel::get();
        $data['category'] = CategoryPostModel::where('post_id',$id)->get()->toArray();
        $data['tag'] = TagPostModel::where('post_id',$id)->get()->toArray();

        

        return view('backend.articles.edit')->with([
            'validator' => $validator,
            'data'      => $data
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, $id)
    {
        $input = $request->all();
        

        $input['fk_create_by']  = auth()->user()->toArray()['id'];

        $image_thumbnail = $input['old_image_thumbnail'];
        if(!empty($request->file('image_thumbnail'))){
                $file = $request->file('image_thumbnail');
                //$path = base_path().'/public/article/';
                $path = base_path().'/public/frontend/article/'.$input['code'].'/files/';
                $filename = $file->getClientOriginalName();

                $type_img = array('.png', '.jpg', '.gif','jpeg');
                $index = array();
                foreach ($type_img as  $value) {
                    if(strpos($filename,$value)){
                        array_push($index,strpos($filename,$value));
                    }
                }
                $type_name = substr($filename,@$index[0]); //.jpg
                $filename = str_random(6).'_thumbnail';
                $data['image_thumbnail'] = $filename.$type_name;
                $image_thumbnail = $filename.$type_name;
                if(isset($file)){
                    $file->move($path,$filename.$type_name);
                }


        }

        ArticleModel::where('id',$id)->update([
            'title'                         => $input['title'],
            'image_thumbnail'               =>  $image_thumbnail,
            'description'                   =>  $input['description'], 
            'fk_create_by'                  =>  auth()->user()->toArray()['id'],
            'mata_title'                    =>  $input['mata_title'], 
            'mata_description'              =>  $input['mata_description'], 
            'mata_keyword'                  =>  $input['mata_keyword'], 
            'status'                        =>  $input['status']
        ]);
                
        CategoryPostModel::where('post_id',$id)->delete();
        TagPostModel::where('post_id',$id)->delete();

        foreach ($input['type_category'] as $key => $value) {
            $category_post = new CategoryPostModel();
            $category_post->category_id = $value;
            $category_post->post_id = $id;
            $category_post->save();
        }

        foreach ($input['tags'] as $key22 => $value22) {
            $tags = new TagPostModel();
            $tags->tag_id = $value22;
            $tags->post_id = $id;
            $tags->save();
        }

        $request->session()->flash('message', 'jSuccess');
        return redirect()->back();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = ArticleModel::findOrFail($id);
        echo $data->delete();
    }


    public function data_processing(){

        return  DataTables::of(ArticleModel::query())
                ->editColumn('created_at', function ($query) {
                    return $query->created_at->diffForHumans();
                })

                ->editColumn('status', function ($query) {
                    $color = '';
                    if($query->status == 'enabled'){
                        $color = 'green';
                    }

                    if($query->status == 'closed'){
                        $color = 'red';
                    }

                    if($query->status == 'booking'){
                        $color = '#5f5798';
                    }

                   return '<label style="color:'.$color.'">'.strtoupper($query->status).'</label>';
                })


                ->editColumn('title', function ($query) {
                   return str_limit($query->title,50);
                })

                ->editColumn('description', function ($query) {
                   return strip_tags(str_limit($query->description,60));
                })


                ->addColumn('action', function ($query) {
                    return '
                            <a href="'.route("admin.articles.articles.show",$query->id).'" class="btn btn-info" style="border-radius: 50px;background-color: #48beb3;padding: 4px 8px; margin-bottom: 0 !important;">
                                <i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="View" data-original-title="View"></i>
                            </a>

                            <a href="'.route("admin.articles.articles.edit",$query->id).'" class="btn btn-primary" style="border-radius: 50px;background-color: #cc1156;    border-color: transparent;padding: 4px 8px;margin-bottom: 0 !important; ">
                                <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit" data-original-title="Edit"></i>
                            </a>

                            <button  onclick="destroy('.$query->id.')" id="this-data-'.$query->id.'" 
                                style="border-radius: 50px;background-color: #0068b2; border-color: transparent;padding: 4px 9px;"
                                class="btn btn-primary deleteproductModal"
                                data-toggle="modal" 
                                data-destroy_token="'.csrf_token().'"
                                data-destroy_name="'.$query->title.'"
                                data-destroy_route="'.url('admin/articles/articles/'.$query->id).'"
                                data-destroy_redirect="'.route("admin.articles.articles.index").'"
                                >
                                <i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="Delete" data-original-title="Delete"></i>
                            </button>
                    ';
                })

                ->rawColumns(['action','status'])
                ->toJson();

    }

    public function upload_file(EditorUploadRequest $request){
        $input   = $request->all();

        // echo "<pre>";
        // print_r($input);
        // echo "</pre>";
        // exit;
        $file_a = $request->file('file_param');
        $filename_a = $file_a->getClientOriginalName();

        $path_a = base_path().'/public/frontend/article/'.$input['code'].'/editor/files/';
        $filename_a = $file_a->getClientOriginalName();
        $file_a->move($path_a,$filename_a);

        $success['link'] = '/frontend/article/'.$input['code'].'/editor/files/'.$filename_a;
        

        return stripslashes(response()->json($success)->content());

        //return response()->json($success)->content();

        //return stripslashes(response()->json($success)->content());
    }

}
