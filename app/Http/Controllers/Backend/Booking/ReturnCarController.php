<?php

namespace App\Http\Controllers\Backend\Booking;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Booking\CarBooking;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DataTables;
use App\Models\Auth\User;
use App\Models\Booking\ReturnCarModel;
use App\Models\Cars\Description\Cars;
class ReturnCarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $query = DB::table('car_booking','cars','invoices')
        //             ->select('car_booking.*',
        //                      'cars.*',
        //                      'invoices.*',
        //                      'invoices.price as invoice_price',
        //                      'invoices.created_at as invoice_created_at'
        //                     )
        //             ->join('cars','cars.id','=', 'car_booking.fk_car_id')
        //             ->join('invoices','invoices.id', '=', 'car_booking.fk_invoice')
        //             ->get();

        // echo "<pre>";
        // print_r($query);
        // echo "</pre>";
        // exit;
        return view('backend.booking.return_car.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $data  =  DB::table('car_booking','cars','invoices')
                    ->select('car_booking.*',
                             'car_booking.id as car_booking_id',
                             'cars.*',
                             'invoices.*',
                             'invoices.price as invoice_price',
                             'invoices.created_at as invoice_created_at'
                            )
                    ->join('cars','cars.id','=', 'car_booking.fk_car_id')
                    ->join('invoices','invoices.id', '=', 'car_booking.fk_invoice')
                    ->where('car_booking.id','=',$id)
                    ->first();

        $staff = User::select('first_name','last_name','id')->get();
      


        return view('backend.booking.return_car.edit')->with([
            'data' => $data,
            'staff' => $staff
        ]);
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        CarBooking::where('id',$data['car_booking'])->update([
                            'return_date'           => $data['return_date'],
                            'return_time'           => $data['return_time'],
                            'return_remark'         => $data['return_remark'],
                            'fk_staff_receive'      => $data['fk_staff_receive'],
                            'status_return_car'     => 'return',
                        ]);


        $count_booking = CarBooking::where('fk_car_id',$data['car_id'])->where('status_return_car','booking')->count();
        if($count_booking == 0){
            Cars::where('id',$data['car_id'])->update(['status' => 'enabled']);
        }
        
         return redirect('admin/booking/return-car');
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function data_processing(){

        $query  =  DB::table('car_booking','cars','invoices')
                    ->select('car_booking.*',
                             'cars.*',
                             'invoices.*',
                             'invoices.price as invoice_price',
                             'invoices.created_at as invoice_created_at'
                            )
                    ->join('cars','cars.id','=', 'car_booking.fk_car_id')
                    ->join('invoices','invoices.id', '=', 'car_booking.fk_invoice')
                    ->get();


        return  DataTables::of($query)
                
                ->editColumn('status_return_car', function ($query) {
                    $color = '';
                    $status_return_car = '';
                    if($query->status_return_car == 'return'){
                        $color = 'green';
                        $status_return_car = 'คืนแล้ว';
                    }

                    if($query->status_return_car == 'booking'){
                        $color = 'purple';
                        $status_return_car = 'ยังไม่คืน';
                    }

                   return '<label style="color:'.$color.'">'.$status_return_car.'</label>';
                })
                
                ->addColumn('action', function ($query) {
                    return '
                            <a data-balloon="บันทึกการคืน" data-balloon-pos="up" href="'.route("admin.booking.return-car.edit",$query->id).'" class="btn btn-primary" style="border-radius: 50px;background-color: #cc1156; border-color: transparent;padding: 4px 8px;margin-bottom: 0 !important; ">
                                <i class="fa fa-pencil"></i>
                            </a>
                    ';
                })
                ->rawColumns(['action','status_return_car'])
                ->toJson(true);
    }


}
