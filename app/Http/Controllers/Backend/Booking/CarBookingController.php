<?php

namespace App\Http\Controllers\Backend\Booking;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Booking\CarBooking;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DataTables;
use Lang;
use App\Models\ExtraOption\ExtraOptionDB;
use App\Models\Cars\Description\Cars;
use App\Models\Customers\Customers;
use App\Models\Auth\User;
use App\Models\Paypal\Invoice;
class CarBookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        // $customer = Customers::find(1);
        // return view('backend.booking.carbooking.show',compact('customer'));




        return view('backend.booking.carbooking.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $set_body_page['data-page'] = "shopping_cart";
        $set_body_page['class'] = "shopping-cart-page";
        $set_body_page['id'] = "wrapper";
        $set_body_page['onload'] = "init()";
        
        return view('backend.booking.carbooking.create',compact('set_body_page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $input = $request->all();

       





       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data  =  DB::table('car_booking','cars','invoices')
                    ->select('car_booking.*',
                             'cars.*',
                             'cars.price as car_price',
                             'invoices.*',
                             'invoices.price as invoice_price',
                             'invoices.created_at as invoice_created_at'
                            )
                    ->join('cars','cars.id','=', 'car_booking.fk_car_id')
                    ->join('invoices','invoices.id', '=', 'car_booking.fk_invoice')
                    ->where('car_booking.id','=',$id)
                    ->first();


        if(empty($data)){
            abort(404);
        }

        $branch = array();
        foreach (Lang::get('branch') as $key => $value) {
          $branch[$value['code']] = $value;
        }

        $data->id = $id;
        $data->pick_up = $branch[$data->pick_up]['name'];
        $data->drop_off = $branch[$data->drop_off]['name'];
        $data->pick_up_date =  $this->DateOnly($data->pick_up_date);
        $data->drop_off_date =  $this->DateOnly($data->drop_off_date);
        $data->branch_code = $branch[$data->branch_code]['name'];
        $data->car_enddate_act = $this->DateOnly($data->car_enddate_act);
        $data->car_enddate_insurance = $this->DateOnly($data->car_enddate_insurance);
        $data->invoice_created_at = $this->DateThai($data->invoice_created_at);
        $data->data_sesstion = json_decode($data->data_sesstion);

        // $data['total_day'] = $diff_day;
        // $total = 0;
        // foreach ($data['items'] as $item) {
        //     $total += $item['price'] * $item['qty'];
        // }

        return view('backend.booking.carbooking.show',compact('data'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $booking  =  DB::table('car_booking','cars','invoices')
                    ->select('car_booking.*',
                              'car_booking.company_name as car_booking_company_name',
                             'cars.*',
                             'cars.price as car_price',
                             'invoices.*',
                             'invoices.id as invoices_id',
                             'invoices.price as invoice_price',
                             'invoices.created_at as invoice_created_at'
                            )
                    ->join('cars','cars.id','=', 'car_booking.fk_car_id')
                    ->join('invoices','invoices.id', '=', 'car_booking.fk_invoice')
                    ->where('car_booking.id','=',$id)
                    ->first();

     

        if(empty($booking)){
            abort(404);
        }

        $staff = '';
        if(!empty($booking->fk_staff_approve_deposit)){
            $staff_query = User::select('first_name','last_name','id')->where('id','=',$booking->fk_staff_approve_deposit)->first();
            $staff = $staff_query->first_name." ".$staff_query->last_name;
        }

        
        $branch = array();
        foreach (Lang::get('branch') as $key => $value) {
          $branch[$value['code']] = $value;
        }
        
        $booking->pick_up = $branch[$booking->pick_up]['name'];
        $booking->drop_off = $branch[$booking->drop_off]['name'];
        $booking->data_sesstion = json_decode($booking->data_sesstion,true);
        $booking->pick_up_date =  $this->DateOnly($booking->pick_up_date);
        $booking->drop_off_date =  $this->DateOnly($booking->drop_off_date);

        $customer = null;
        if($booking->fk_user_booking != null && $booking->user_register != 'no'){
            $customer = Customers::where('id','=',$booking->fk_user_booking)->first();
        }

        $data['car'] = Cars::select('license_plate','id','branch_car_code','car_brand','car_generation','car_type','price')->get();

        // echo "<pre>";
        // print_r($customer);
        // echo "</pre>";
        
        return view('backend.booking.carbooking.edit')->with([
            'booking'   =>  $booking,
            'data'      =>  $data,
            'customer'  => $customer,
            'staff'     => $staff
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
        $data = $request->all();

      

         /*
        |--------------------------------------------------------------------------
        | ทำการตรวจสอบประเภทไฟล์ที่รับเข้ามา เพื่อใช้ในการตั้งประเภทไฟล์
        |--------------------------------------------------------------------------
        */
        if(!empty($request->file('personal_card_img'))){
            $file = $request->file('personal_card_img');
            $path = base_path().'/public/customers/'.$data['car_booking_code'].'/';
            $filename = $file->getClientOriginalName();
            $type_img = array('.png', '.jpg', '.gif','.jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = 'personal_id';
            $data['personal_card_img'] = $data['car_booking_code'].'/'.$filename.$type_name;
            
            if(isset($file)){
                $file->move($path,$filename.$type_name);
                 CarBooking::where('id',$data['id'])
                        ->update([
                                'personal_card_img'             => @$data['personal_card_img']
                            ]);
            }
        }


        if(!empty($request->file('driver_license_card_img'))){
            $file = $request->file('driver_license_card_img');
            $path = base_path().'/public/customers/'.$data['car_booking_code'].'/';
            $filename = $file->getClientOriginalName();
            $type_img = array('.png', '.jpg', '.gif','.jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = 'driver_license_card';
            $data['driver_license_card_img'] = $data['car_booking_code'].'/'.$filename.$type_name;
            
            if(isset($file)){
                $file->move($path,$filename.$type_name);
                    CarBooking::where('id',$data['id'])
                        ->update([
                                'driver_license_card_img'             => @$data['driver_license_card_img']
                            ]);
            }
        }

        if(!empty($request->file('credit_card_img'))){
            $file = $request->file('credit_card_img');
            $path = base_path().'/public/customers/'.$data['car_booking_code'].'/';
            $filename = $file->getClientOriginalName();
            $type_img = array('.png', '.jpg', '.gif','.jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = 'credit_card';
            $data['credit_card_img'] = $data['car_booking_code'].'/'.$filename.$type_name;

            if(isset($file)){
                $file->move($path,$filename.$type_name);
            }

            CarBooking::where('id',$data['id'])
                        ->update([
                                'credit_card_img'             => @$data['credit_card_img']
                        ]);
        }

        if(!empty($request->file('image_paid'))){
            $file = $request->file('image_paid');
            $path = base_path().'/public/booking/paid/'.$data['car_booking_code'].'/';
            $filename = $file->getClientOriginalName();
            $type_img = array('.png', '.jpg', '.gif','.jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = 'credit_card';
            $data['image_paid'] = $data['car_booking_code'].'/'.$filename.$type_name;

            if(isset($file)){
                $file->move($path,$filename.$type_name);
            }

            CarBooking::where('id',$data['id'])
                        ->update([
                                'image_paid'             => @$data['image_paid']
                        ]);

            Invoice::where('id',$data['invoices_id'])->update([
            'image_paid'  => $data['image_paid']
            ]);

        }


        $this_car = Cars::where('branch_car_code',$data['branch_car_code'])->first();



         CarBooking::where('id',$data['id'])
                        ->update([
                                'fk_car_id'                     => $this_car->id,
                                'fk_staff_approve_booking'      => auth()->user()->toArray()['id'],
                                'status_receive_car'            => 'yes',
                                'personal_card_address'         => $data['personal_card_address'],
                                'present_address'               => $data['present_address'],
                                'company_name'                   => $data['company_name'],
                                'position'                       => $data['position'],
                                'tel_office'                     => $data['tel_office'],
                                'id_card_customer'               => $data['id_card_customer'],
                                'first_name_beneficiary'         => $data['first_name_beneficiary'],
                                'last_name_beneficiary'          => $data['last_name_beneficiary'],
                                'relation_beneficiary'           => $data['relation_beneficiary'],
                                'phone_beneficiary'              => $data['phone_beneficiary'],

                            ]);

        $status_pay = 0;
        if($data['paid'] == 'pay'){
            $status_pay = 1;
        }else{
            $status_pay = 0;
        }

        Invoice::where('id',$data['invoices_id'])->update([
            'paid'  => $status_pay
            ]);

        $request->session()->flash('message', 'jSuccess');
        return redirect()->back();
     


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




    public function data_processing(){

    

        $query  =  DB::table('car_booking','cars','invoices')
                    ->select('car_booking.*',
                             'car_booking.id as car_booking_id',
                             'cars.*',
                             'invoices.*',
                             'invoices.price as invoice_price',
                             'invoices.created_at as invoice_created_at'
                            )
                    ->join('cars','cars.id','=', 'car_booking.fk_car_id')
                    ->join('invoices','invoices.id', '=', 'car_booking.fk_invoice')
                    ->get();

        

        return  DataTables::of($query)
                
                ->editColumn('title', function ($query) {
                     $string = $query->title;
                    $x = str_replace("รายการที่ ","",$string);
                    $y = str_replace("รายการ ","",$x);
                    return  $y;
                })


                

                ->editColumn('created_at', function ($query) {
                    $sss= "<label title'asf'>".Carbon::parse($query->created_at)->diffForHumans()."</label>";
                    return  $sss;
                })

                ->editColumn('name', function ($query) {
                    return $query->customer_first_name.' '.$query->customer_last_name;
                })


                ->editColumn('pick_up', function ($query) {
                    $branch = array();
                    foreach (Lang::get('branch') as $key => $value) {
                        $branch[$value['code']] = $value;
                    }
                    return $branch[$query->pick_up]['name'];
                })

                ->editColumn('drop_off', function ($query) {
                    $branch = array();
                    foreach (Lang::get('branch') as $key => $value) {
                        $branch[$value['code']] = $value;
                    }
                    return $branch[$query->drop_off]['name'];
                })

                 ->editColumn('paid', function ($query) {
                    $color = '';
                    $status ='';
                    if($query->paid == 1){
                        $color = 'green';
                        $status = 'ชำระเงินแล้ว';
                    }

                    if($query->paid == 0){
                        $color = 'red';
                        $status = 'ยังไม่ได้ชำระเงิน';
                    }


                   return '<label style="color:'.$color.'">'.$status.'</label>';

                })


                  ->editColumn('status_receive_car', function ($query) {
                    $color = '';
                    $status ='';
                    if($query->status_receive_car == 'yes'){
                        $color = 'green';
                        $status = 'รับรถแล้ว';
                    }

                    if($query->status_receive_car == 'no'){
                        $color = 'red';
                        $status = 'ยังไม่ได้รับรถ';
                    }


                   return '<label style="color:'.$color.'">'.$status.'</label>';

                })


                 


                ->editColumn('pick_up_date', function ($query) {
                    return $this->DateThai($query->start_date);
                })

                ->editColumn('drop_off_date', function ($query) {
                    return $this->DateThai($query->end_date);
                })



                ->editColumn('status', function ($query) {
                    $color = '';
                    if($query->status == 'enabled'){
                        $color = 'green';
                    }

                    if($query->status == 'closed'){
                        $color = 'red';
                    }

                    if($query->status == 'booking'){
                        $color = 'purple';
                    }

                   return '<label style="color:'.$color.'">'.strtoupper($query->status).'</label>';
                })
                
                ->addColumn('action', function ($query) {
                    return '
                            <a href="'.route("admin.booking.car-booking.show",$query->car_booking_id).'" class="btn btn-info" style="border-radius: 50px;background-color: #48beb3;padding: 4px 8px;">
                                <i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="View" data-original-title="View"></i>
                            </a>

                            <a title="รับรถ" href="'.route("admin.booking.car-booking.edit",$query->car_booking_id).'" class="btn btn-primary" style="border-radius: 50px;background-color: #cc1156; border-color: transparent;padding: 4px 8px;">
                                 <i class="fa fa-automobile" data-toggle="tooltip" data-placement="top" data-original-title="การรับรถ"></i>
                            </a>
                    ';
                })
                ->rawColumns(['action','status','pick_up','drop_off','pick_up_date','drop_off_date','paid','created_at','status_receive_car'])
                ->toJson(true);
    }


    public function DateThai($strDate){
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];

        return ''."$strDay $strMonthThai $strYear ".$strHour.':'.$strMinute.'';
    }


    public function DateOnly($strDate){
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];

       
        return "$strDay $strMonthThai $strYear";
    }


}
