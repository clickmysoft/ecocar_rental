<?php

namespace App\Http\Controllers\Backend\Booking;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Booking\CarBooking;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DataTables;
use Lang;
use App\Models\ExtraOption\ExtraOptionDB;
use App\Models\Cars\Description\Cars;
use App\Models\Customers\Customers;
use App\Models\Auth\User;





class DepositController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('backend.deposit.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.deposit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        echo "<pre>";
        print_r($input);
        echo "</pre>";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $booking  =  DB::table('car_booking','cars','invoices')
                    ->select('car_booking.*',
                             'car_booking.id as car_booking_id',
                             'cars.*',
                             'cars.price as car_price',
                             'invoices.*',
                             'invoices.price as invoice_price',
                             'invoices.created_at as invoice_created_at'
                            )
                    ->join('cars','cars.id','=', 'car_booking.fk_car_id')
                    ->join('invoices','invoices.id', '=', 'car_booking.fk_invoice')
                    ->where('car_booking.id','=',$id)
                    ->first();

     

        if(empty($booking)){
            abort(404);
        }

        // echo "<pre>";
        // print_r($customer);
        // echo "</pre>";
        
        return view('backend.deposit.create')->with([
            'booking'   =>  $booking
        ]);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

       

         /*
        |--------------------------------------------------------------------------
        | ทำการตรวจสอบประเภทไฟล์ที่รับเข้ามา เพื่อใช้ในการตั้งประเภทไฟล์
        |--------------------------------------------------------------------------
        */
        if(!empty($request->file('deposit_image'))){
            $file = $request->file('deposit_image');
            $path = base_path().'/public/booking/deposit/'.$data['car_booking_code'].'/';
            $filename = $file->getClientOriginalName();
            $type_img = array('.png', '.jpg', '.gif','.jpeg');
            $index = array();
            foreach ($type_img as  $value) {
                if(strpos($filename,$value)){
                    array_push($index,strpos($filename,$value));
                }
            }
            $type_name = substr($filename,@$index[0]); //.jpg
            $filename = 'deposit_payment';
            $data['deposit_image'] = $data['car_booking_code'].'/'.$filename.$type_name;
            
           
        }

         CarBooking::where('car_booking_code',$data['car_booking_code'])
                        ->update([
                                'deposit'                       => 'pay',
                                'fk_staff_approve_deposit'      => auth()->user()->toArray()['id'],
                                'deposit_amount'                => $data['deposit_amount'],
                                'deposit_pay_date'              => $data['deposit_pay_date'],
                                'deposit_pay_time'              => $data['deposit_pay_time'],
                                'deposit_remark'               => @$data['deposit_remark']
                            ]);
        if(isset($file)){
                $file->move($path,$filename.$type_name);
                CarBooking::where('car_booking_code',$data['car_booking_code'])
                        ->update([
                                'deposit_image'                 => $data['deposit_image']
                            ]);
        }

        $request->session()->flash('message', 'jSuccess');
        return redirect('admin/booking/deposit-car');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function data_processing(){

    
        $query  =  DB::table('car_booking','cars','invoices')
                    ->select('car_booking.*',
                             'car_booking.id as car_booking_id',
                             'cars.*',
                             'invoices.*',
                             'invoices.price as invoice_price',
                             'invoices.created_at as invoice_created_at'
                            )
                    ->join('cars','cars.id','=', 'car_booking.fk_car_id')
                    ->join('invoices','invoices.id', '=', 'car_booking.fk_invoice')
                    ->get();

        

        return  DataTables::of($query)
                
                ->editColumn('created_at', function ($query) {
                    $sss= "<label title'asf'>".Carbon::parse($query->created_at)->diffForHumans()."</label>";
                    return  $sss;
                })

                ->editColumn('name', function ($query) {
                    return $query->customer_first_name.' '.$query->customer_last_name;
                })


                ->editColumn('pick_up', function ($query) {
                    $branch = array();
                    foreach (Lang::get('branch') as $key => $value) {
                        $branch[$value['code']] = $value;
                    }
                    return $branch[$query->pick_up]['name'];
                })

                ->editColumn('drop_off', function ($query) {
                    $branch = array();
                    foreach (Lang::get('branch') as $key => $value) {
                        $branch[$value['code']] = $value;
                    }
                    return $branch[$query->drop_off]['name'];
                })

                 ->editColumn('paid', function ($query) {
                    $color = '';
                    $status ='';
                    if($query->paid == 1){
                        $color = 'green';
                        $status = 'ชำระเงินแล้ว';
                    }

                    if($query->paid == 0){
                        $color = 'red';
                        $status = 'ยังไม่ได้ชำระเงิน';
                    }


                   return '<label style="color:'.$color.'">'.$status.'</label>';

                })

                 ->editColumn('deposit', function ($query) {
                    $color = '';
                    $status ='';
                    if($query->deposit == 'pay'){
                        $color = 'green';
                        $status = 'ชำระเงินแล้ว';
                    }else{
                        $color = 'red';
                        $status = 'ยังไม่ได้ชำระเงิน';
                    }


                   return '<label style="color:'.$color.'">'.$status.'</label>';

                })



                  ->editColumn('status_receive_car', function ($query) {
                    $color = '';
                    $status ='';
                    if($query->status_receive_car == 'yes'){
                        $color = 'green';
                        $status = 'รับรถแล้ว';
                    }

                    if($query->status_receive_car == 'no'){
                        $color = 'red';
                        $status = 'ยังไม่ได้รับรถ';
                    }


                   return '<label style="color:'.$color.'">'.$status.'</label>';

                })


                 


                ->editColumn('pick_up_date', function ($query) {
                    return $this->DateThai($query->start_date);
                })

                ->editColumn('drop_off_date', function ($query) {
                    return $this->DateThai($query->end_date);
                })



                ->editColumn('status', function ($query) {
                    $color = '';
                    if($query->status == 'enabled'){
                        $color = 'green';
                    }

                    if($query->status == 'closed'){
                        $color = 'red';
                    }

                    if($query->status == 'booking'){
                        $color = 'purple';
                    }

                   return '<label style="color:'.$color.'">'.strtoupper($query->status).'</label>';
                })
                
                ->addColumn('action', function ($query) {
                    return '
                            <a href="'.route("admin.booking.car-booking.show",$query->car_booking_id).'" class="btn btn-info" style="border-radius: 50px;background-color: #48beb3;padding: 4px 8px;">
                                <i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="View" data-original-title="View"></i>
                            </a>

                            <a data-balloon="บันทึกการคืน" data-balloon-pos="up" href="'.route("admin.deposit.deposit-car.edit",$query->car_booking_id).'" class="btn btn-primary" style="border-radius: 50px;background-color: #cc1156; border-color: transparent;padding: 4px 8px;">
                                <i class="fa fa-pencil"></i>
                            </a>
                    ';
                })
                ->rawColumns(['action','status','pick_up','drop_off','pick_up_date','drop_off_date','paid','created_at','status_receive_car','deposit'])
                ->toJson(true);
    }


    public function DateThai($strDate){
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];

        return ''."$strDay $strMonthThai $strYear ".$strHour.':'.$strMinute.'';
    }


    public function DateOnly($strDate){
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];

       
        return "$strDay $strMonthThai $strYear";
    }

}
