<?php

namespace App\Http\Controllers\Backend\Auth\User;

use App\Models\Auth\User;
use App\Http\Controllers\Controller;
use App\Events\Backend\Auth\User\UserDeleted;
use App\Repositories\Backend\Auth\RoleRepository;
use App\Repositories\Backend\Auth\UserRepository;
use App\Repositories\Backend\Auth\PermissionRepository;
use App\Http\Requests\Backend\Auth\User\StoreUserRequest;
use App\Http\Requests\Backend\Auth\User\ManageUserRequest;
use App\Http\Requests\Backend\Auth\User\UpdateUserRequest;
use Request; //ใส่ use Request ที่อยู่ใน aliases แทน  use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use JsValidator;
use DataTables;
/**
 * Class UserController.
 */
class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * UserController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageUserRequest $request)
    {
        return view('backend.auth.user.index');
    }

    /**
     * @param ManageUserRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     *
     * @return mixed
     */
    public function create(ManageUserRequest $request, RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        return view('backend.auth.user.create')
            ->withRoles($roleRepository->with('permissions')->get(['id', 'name']))
            ->withPermissions($permissionRepository->get(['id', 'name']));
    }

    /**
     * @param StoreUserRequest $request
     *
     * @return mixed
     */
    public function store(StoreUserRequest $request)
    {

         $input = $request->all();
        /*
            |--------------------------------------------------------------------------
            | ทำการตรวจสอบประเภทไฟล์ที่รับเข้ามา เพื่อใช้ในการตั้งประเภทไฟล์
            |--------------------------------------------------------------------------
            */
            if(!empty($request->file('user_image'))){
                $file = $request->file('user_image');
                $path = base_path().'/public/avatars/image_profile';
                $filename = $file->getClientOriginalName();

                $type_img = array('.png', '.jpg', '.gif','jpeg');
                $index = array();
                foreach ($type_img as  $value) {
                    if(strpos($filename,$value)){
                        array_push($index,strpos($filename,$value));
                    }
                }

                $type_name = substr($filename,@$index[0]); //.jpg
                $filename = trim($_POST['employee_id']);
                // $_POST['user_image'] = '/avatars/'.$filename.$type_name;
                $input['user_image'] = '/avatars/image_profile/'.$filename.$type_name;
        }
    
        // echo "<pre>";
        // print_r($input);
        // echo "</pre>";
        // exit;
        $success = $this->userRepository->create($input);

        if($success){
            if(isset($file)){
                $file->move($path,$filename.$type_name);
            }
        }

        return redirect()->route('admin.auth.user.index')->withFlashSuccess(__('alerts.backend.users.created'));
    }

    /**
     * @param User              $user
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function show(User $user, ManageUserRequest $request)
    {
      //  $user = DB::table('model_has_roles')->where('model_id',$user->id)->first();


          $query = DB::table('model_has_roles','roles')
            ->select('model_has_roles.*',
                        'roles.*'
                    )
            ->where('model_id',$user->id)
            ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->get();
      
        $role = $query->toArray();

        $user['permissions_name'] = strtoupper(@$role[0]->name);

        $user['company_name']= config('company.'.app()->getLocale().'.name.'.$user['company_name'])[0];
        $user['position_name']= config('company.'.app()->getLocale().'.position.'.$user['position_name'])[0];
        $user['start_work_new'] =  $this->DateThai($user['start_work']);
        $user['long_work'] = $this->get_long_time($user['start_work']);

        
        return view('backend.auth.user.show')
            ->withUser($user);
    }

    /**
     * @param User                 $user
     * @param ManageUserRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     *
     * @return mixed
     */
    public function edit(User $user, ManageUserRequest $request, RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        return view('backend.auth.user.edit')
            ->withUser($user)
            ->withRoles($roleRepository->get())
            ->withUserRoles($user->roles->pluck('name')->all())
            ->withPermissions($permissionRepository->get(['id', 'name']))
            ->withUserPermissions($user->permissions->pluck('name')->all());
    }

    /**
     * @param User              $user
     * @param UpdateUserRequest $request
     *
     * @return mixed
     */
    public function update(User $user, UpdateUserRequest $request)
    {
        $this->userRepository->update($user, $request->only(
            'first_name',
            'last_name',
            'username',
            'email',
            'timezone',
            'roles',
            'permissions'
        ));

        return redirect()->route('admin.auth.user.index')->withFlashSuccess(__('alerts.backend.users.updated'));
    }

    /**
     * @param User              $user
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function destroy(User $user, ManageUserRequest $request)
    {
   
        $this->userRepository->deleteById($user->id);

        event(new UserDeleted($user));

        return redirect()->route('admin.auth.user.deleted')->withFlashSuccess(__('alerts.backend.users.deleted'));
    }


    /**
    * เปลี่ยนวันให้เป็นภาษาคน.
    */
    public  function dateForHumans($query){
        if($query!='1970-01-01'){
                        $created = new Carbon($query);
                        $now = Carbon::now();
                        $difference = $now;
                        $difference = ($created->diff($now)->days < 1) ? 'วันนี้' : $created->diffForHumans($now);
        }else{
             $difference = "-";
        }

        return $difference;
    }

    public function DateThai($strDate){
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }


    public function get_long_time($strDate){
        
        $string = '';

        $strYear = date("Y",strtotime($strDate));
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));


        $Born = Carbon::create($strYear,$strMonth,$strDay);

        $year = $Born->diff(Carbon::now())->format('%y');
        $month = $Born->diff(Carbon::now())->format('%m');
        $day = $Born->diff(Carbon::now())->format('%d');

        if($year != 0){
            $string= $year." ปี, ".$month." เดือน ".$day." วัน";
        }elseif ($month != 0) {
             $string= $month." เดือน ".$day." วัน";
        }else{
             $string= $day." วัน";
        }

        return $string;

    }



    public function data_processing(){

        return  DataTables::of(User::where('active',1)->where('type_register','employee'))
                
                ->editColumn('updated_at', function ($query) {
                    return $query->updated_at->diffForHumans();
                })

                ->editColumn('company_name', function ($query) {
                    return config('company.'.app()->getLocale().'.name.'.$query->company_name)[0];
                })

                ->editColumn('position_name', function ($query) {
                     return config('company.'.app()->getLocale().'.position.'.$query->position_name)[0];
                })

                ->addColumn('user_permisstion', function ($query) {
                     $query_permisstion = DB::table('model_has_roles','roles')
                    ->select('model_has_roles.*',
                                'roles.*'
                            )
                    ->where('model_id',$query->id)
                    ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->first();

                    if(isset($query_permisstion->name)){
                        return ucfirst($query_permisstion->name);
                    }else{
                        return 'ไม่พบข้อมูล';
                    }

                })
                
                ->addColumn('action', function ($query) {
                    $query_permisstion = DB::table('model_has_roles','roles')
                    ->select('model_has_roles.*',
                                'roles.*'
                            )
                    ->where('model_id',$query->id)
                    ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->first();
                    return '
                        <div class="btn-group btn-group-sm" role="group" aria-label="User Actions">
                            <a href="'.route('admin.auth.user.show',$query->id).'" class="btn btn-info"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="" data-original-title="แสดง"></i></a>
                            <a href="'.url('admin/auth/user/'.$query->id.'/edit').'" class="btn btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="แก้ไข"></i></a>

                            <a class="btn btn-effect btn-danger" onclick="tanawat_popup('.$query->id.')">More</a>
                                <div style="z-index:999;" class="md-modal md-effect-2" id="modal-'.$query->id.'">
                                     <div class="md-content md-content-red">
                                        <h3 style="font-size: 22px;color: #fff;">Select Event</h3>
                                            <div>
                                                <ul class="dropdown-menu-list withScroll _mCS_1 mCustomScrollbar _mCS_2" data-height="220" style="height: 220px;"><div class="mCustomScrollBox mCS-dark-2" id="mCSB_2" style="position:relative; height:100%; overflow:hidden; max-width:100%;"><div class="mCSB_container mCS_no_scrollbar" style="position:relative; top:0;">

                                                    <li class="naruto" style="display:none">
                                                        <i class="fa fa-star p-r-10 f-18 c-orange"></i>
                                                        <span class="dropdown-time"></span>
                                                    </li>

                                                    <li class="naruto" style="display:block">
                                                        <i class="fa fa-heart p-r-10 f-18 c-red"></i>
                                                        <span class="dropdown-time"><a href="'.url('admin/auth/user/'.$query->id.'/login-as').'" class="dropdown-item">เข้าสู่ระบบเสมือนเป็น '.$query->first_name.' '.$query->last_name.'</a> </span>
                                                    </li>
                                                    <li class="naruto" style="display:block">
                                                        <i class="fa fa-file-text p-r-10 f-18"></i>
                                                        <span class="dropdown-time"><a href="'.route('admin.auth.user.change-password',$query->id).'" class="dropdown-item">เปลี่ยนรหัสผ่าน</a> </span>
                                                    </li>
                                                    <li class="naruto" style="display:block">
                                                        <i class="fa fa-lock p-r-10 f-18"></i>
                                                        <span class="dropdown-time"><a href="'.url('admin/auth/user/'.$query->id.'/mark/0').'" class="dropdown-item">พักการใช้งาน</a> </span>
                                                    </li>                                                                                                       
                                                    <li class="naruto" style="display:none">
                                                        <i class="fa fa-bell p-r-10 f-18 c-orange"></i>
                                                        <span class="dropdown-time"> </span>
                                                    </li>
                                                    <li class="naruto" style="display:block">
                                                        <i class="fa fa-trash-o p-r-10 f-18"></i>
                                                        <span class="dropdown-time"> <a data-method="delete" data-trans-button-cancel="ยกเลิก" data-trans-button-confirm="ลบ" data-trans-title="คุณแน่ใจหรือ?" class="dropdown-item" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">ลบ
                                                            <form action="'.url('admin/auth/user/'.$query->id).'" method="POST" name="delete_item" style="display:none">
                                                                <input type="hidden" name="_method" value="delete">
                                                                <input type="hidden" name="_token" value="'.csrf_token().'">
                                                            </form>
                                                        </a> </span>
                                                    </li>
                                            </div>
                                                <div class="mCSB_scrollTools" style="position: absolute; display: none;"><div class="mCSB_draggerContainer"><div class="mCSB_dragger" style="position: absolute; top: 0px;" oncontextmenu="return false;"><div class="mCSB_dragger_bar" style="position:relative;"></div></div><div class="mCSB_draggerRail"></div></div></div></div></ul>
                                                <button class="btn  btn-modal btn-default">Close</button>
                                            </div>
                                        </div>
                                </div>
                            <div class="md-overlay"></div>
                        </div>
                    ';

                })

                ->rawColumns(['action'])
                ->toJson(true);
    }



}
