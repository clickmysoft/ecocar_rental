<?php

namespace App\Http\Controllers\Backend\Tag;

use App\Http\Requests;
use Request;    //ใส่ use Request ที่อยู่ใน aliases แทน  use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tag\TagModel;
use App\Http\Requests\Backend\Tag\TagRequest;
use JsValidator;
use DataTables;
use Carbon\Carbon;
use App\Models\Auth\User;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.tag.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('backend.tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request)
    {
        $input = $request->all();
        $input['fk_create_by']  = auth()->user()->toArray()['id'];
        
        if($input['status'] == 'on'){
            $input['status'] = 'enabled';
        }else{
             $input['status'] = 'closed';
        }

      
        if(TagModel::create($input)){
            $request->session()->flash('message','jSuccess');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = TagModel::find($id)->toArray();
        if(empty($data)){
            abort(404);
        }

        $rule = new TagRequest();
        $validator = JsValidator::make($rule->rules());

        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // exit;
        
        return view('backend.tag.edit',compact('data'))->with([
            'validator' => $validator,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TagRequest $request, $id)
    {
        $input = $request->all();
        
        if(empty($input['status'])){
                 $input['status'] = 'closed';
        }else{
            $input['status'] = 'enabled';
        }

        if(TagModel::where('id',$id)->update([
                'tag_name'     =>  $input['tag_name'],
                'status'       =>  $input['status']
            ])
        ){
            $request->session()->flash('message','jSuccess');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function data_processing(){
        $str ="";
        return  DataTables::of(TagModel::query())
                
            ->editColumn('updated_at', function ($query) {
                return $query->updated_at->diffForHumans();
            })
             
             ->editColumn('tag_name', function ($query) {
                    return strip_tags(str_limit($query->tag_name, 50));
                })

             ->editColumn('fk_create_by', function ($query) {
                   return $this->getUser($query->fk_create_by);
                })

            ->editColumn('status', function ($query) {
                $color = '';
                if($query->status == 'enabled'){
                    $color = 'green';
                }

                if($query->status == 'closed'){
                    $color = 'red';
                }

               return '<label style="color:'.$color.'">'.strtoupper($query->status).'</label>';
            })
                
            ->addColumn('action', function ($query) {
                    return '
                            <a href="'.route("admin.tag.tag-list.show",$query->id).'" class="btn btn-info" style="border-radius: 50px;background-color: #48beb3;padding: 4px 8px; margin-bottom: 0 !important;">
                                <i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="View" data-original-title="View"></i>
                            </a>

                            <a href="'.route("admin.tag.tag-list.edit",$query->id).'" class="btn btn-primary" style="border-radius: 50px;background-color: #cc1156;    border-color: transparent;padding: 4px 8px;margin-bottom: 0 !important; ">
                                <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit" data-original-title="Edit"></i>
                            </a>

                             <button  onclick="destroy('.$query->id.')" id="this-data-'.$query->id.'" 
                                style="border-radius: 50px;background-color: #0068b2; border-color: transparent;padding: 4px 9px;"
                                class="btn btn-primary deleteproductModal"
                                data-toggle="modal"
                                data-destroy_token="'.csrf_token().'"
                                data-destroy_name="'.$query->license_plate.'"
                                data-destroy_route="'.url('admin/articles/tag/tag-list/{'.$query->id).'"
                                data-destroy_redirect="'.route("admin.tag.tag-list.index").'"
                                >
                                <i class="fa fa-trash-o" data-toggle="tooltip" data-placement="top" title="Delete" data-original-title="Delete"></i>
                            </button>
                    ';
            })

            ->rawColumns(['action','status','fk_create_by'])
            ->toJson(true);
    }

    public function getUser($id){
        $data = User::find($id);
        return $data['first_name'].' '.$data['last_name'];
    }


}
