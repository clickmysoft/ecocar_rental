<?php

namespace App\Http\Requests\Backend\Cars;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            {
                  return [
                    'car_mile'          => 'required',
                    'car_brand'         => 'required',
                    'car_door_qty'      => 'required',
                    'body_number'       => 'required|unique:cars,body_number,'.session()->get('key_edit_post'),
                    'license_plate'     => 'required|unique:cars,license_plate,'.session()->get('key_edit_post'),
                    'engine_number'     => 'required|unique:cars,engine_number,'.session()->get('key_edit_post'),
                    'branch_car_code'   => 'required|unique:cars,branch_car_code,'.session()->get('key_edit_post'),
                ];
            }
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'car_mile'          => 'required',
                    'car_brand'         => 'required',
                    'car_door_qty'      => 'required',
                    'body_number'       => 'required|unique:cars,body_number',
                    'license_plate'     => 'required|unique:cars,license_plate',
                    'engine_number'     => 'required|unique:cars,engine_number',
                    'branch_car_code'   => 'required|unique:cars,branch_car_code',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {

                return [
                    'car_mile'          => 'required',
                    'car_brand'         => 'required',
                    'car_door_qty'      => 'required',
                    'body_number'       => 'required|unique:cars,body_number,'.session()->get('key_edit_post'),
                    'license_plate'     => 'required|unique:cars,license_plate,'.session()->get('key_edit_post'),
                    'engine_number'     => 'required|unique:cars,engine_number,'.session()->get('key_edit_post'),
                    'branch_car_code'   => 'required|unique:cars,branch_car_code,'.session()->get('key_edit_post'),
                ];
                
            }
            default:break;
        }


    }

    

}
