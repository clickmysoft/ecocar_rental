<?php

namespace App\Http\Requests\Backend\Auth\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateUserRequest.
 */
class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id'   => 'required',
            'first_name'    => 'required|max:191',
            'last_name'     => 'required|max:191',
            'phone'         => 'max:10',
            'email'         => 'required|email|max:191',
            'nickname'      => 'max:191',
            'company_name'  => 'max:255',
            'position_name' => 'max:255',
            'belong'        => 'max:255',
            'username'      => 'required',
            'timezone'      => 'required|max:191',
            'roles'         => 'required|array',
        ];
    }
}
