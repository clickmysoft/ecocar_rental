<?php

namespace App\Http\Requests\Backend\Auth\User;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreUserRequest.
 */
class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        return [
            'employee_id'   => 'required|unique:users,employee_id',
            'first_name'    => 'required|max:191',
            'last_name'     => 'required|max:191',
            'phone'         => 'max:10',
            'email'         => ['required','email', 'max:191', Rule::unique('users')],
            'nickname'      => 'max:191',
            'company_name'  => 'max:255',
            'position_name' => 'max:255',
            'belong'        => 'max:255',
            'user_image'    => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            'username'      => 'required|unique:users,username',
            'timezone'      => 'required|max:191',
            'password'      => 'required|min:6|confirmed',
            'roles'         => 'required|array',
        ];
    }
}
