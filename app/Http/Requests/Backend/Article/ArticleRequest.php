<?php

namespace App\Http\Requests\Backend\Article;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            {
                return [
                    'title'                  => 'required',
                    'image_thumbnail'        => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'description'            => 'required',
                    'mata_title'             => 'required',
                    'mata_description'       => 'required',
                    'mata_keyword'           => 'required',
                    'status'                 => 'required',
                ];
            }
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'title'                  => 'required',
                    'image_thumbnail'        => 'required',
                    'description'            => 'required',
                    'mata_title'             => 'required',
                    'mata_description'       => 'required',
                    'mata_keyword'           => 'required',
                    'status'                 => 'required',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'title'                  => 'required',
                    'description'            => 'required',
                    'mata_title'             => 'required',
                    'mata_description'       => 'required',
                    'mata_keyword'           => 'required',
                    'status'                 => 'required',
                    'image_thumbnail'        => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ];
            }
            default:break;
        }





    }
}
