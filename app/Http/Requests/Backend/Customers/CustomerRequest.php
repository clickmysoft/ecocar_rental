<?php

namespace App\Http\Requests\Backend\Customers;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prefix_name_customer'      => 'required',
            'first_name_customer'       => 'required',
            'last_name_customer'        => 'required',
            'email_customer'            => 'email|unique:customers,email_customer',
            'phone_customer'            => 'required|regex:/[0-9]{10}/|numeric|unique:customers,phone_customer',
            'personal_card_address'     => 'required',
            'line_id_customer'          => 'unique:customers,line_id_customer',
            'id_card_customer'          => 'required|min:13|numeric|unique:customers,id_card_customer',
            'present_address'           => 'required',
            'prefix_name_beneficiary'   => 'required',
            'first_name_beneficiary'    => 'required',
            'last_name_beneficiary'     => 'required',
            'relation_beneficiary'      => 'required',
            'phone_beneficiary'         => 'required|numeric|unique:customers,phone_beneficiary',
            'username'                  => 'required|unique:users,username',
            'password'                  => 'required|min:6|confirmed',
        ];
    }
}
