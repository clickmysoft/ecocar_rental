<?php

namespace App\Http\Requests\Backend\Promotion;

use Illuminate\Foundation\Http\FormRequest;

class PromotionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        

        switch ($this->method()) {
            case 'GET':
            {
                return [
                    
                    'title'                 => 'required',
                    'description'           => 'required',
                    // 'promotion_code'        => 'required|unique:promotion,promotion_code,'.session()->get('key_edit_post'),
                    'promotion_discount'    => 'required',
                    'discount_type'         => 'required',
                    'start_date'            => 'required',
                    'end_date'              => 'required',
                    'status'                => 'required',
                ];
            }
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'image_thumbnail'       => 'required',
                    'image_full'            => 'required',
                    'title'                 => 'required',
                    'description'           => 'required',
                    'promotion_code'        => 'required|unique:promotion,promotion_code',
                    'promotion_discount'    => 'required',
                    'discount_type'         => 'required',
                    'start_date'            => 'required',
                    'end_date'              => 'required',
                    'status'                => 'required',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {

                return [
                   
                    'title'                 => 'required',
                    'description'           => 'required',
                    //  
                    'promotion_discount'    => 'required',
                    'discount_type'         => 'required',
                    'start_date'            => 'required',
                    'end_date'              => 'required',
                    'status'                => 'required',
                ];
                
            }
            default:break;
        }
    }
}
