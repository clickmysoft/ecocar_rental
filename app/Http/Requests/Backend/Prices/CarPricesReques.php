<?php

namespace App\Http\Requests\Backend\Prices;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CarPricesReques extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name'          => 'required',
            'branch_name'           => 'required',
            'car_brand'             => 'required',
            'car_generation'        => 'required',
            'status_event'          => 'required',
            // 'type_rental_prices'    => [
            //                                 'required',
            //                                 Rule::unique('car_prices')->where(function($query){
            //                                     $company_name = $this->request->get('company_name');
            //                                     $branch_name = $this->request->get('branch_name');
            //                                     $car_brand = $this->request->get('car_brand');
            //                                     $car_generation = $this->request->get('car_generation');

            //                                     $status_event = $this->request->get('status_event');

            //                                     $date_price_start = date('Y-m-d', strtotime($this->request->get('date_price_start')));
            //                                     $time_price_start = date('H:i:s', strtotime($this->request->get('time_price_start')));

            //                                     $date_price_end = date('Y-m-d', strtotime($this->request->get('date_price_end')));
            //                                     $time_price_end = date('H:i:s', strtotime($this->request->get('time_price_end')));


            //                                     $start_date = $date_price_start.' '.$time_price_start;
            //                                     $end_date = $date_price_end.' '.$time_price_end; 

            //                                     if($status_event == 'close'){
            //                                         $query->where('company_name',$company_name)->where('branch_name',$branch_name)->where('car_brand',$car_brand)->where('car_generation',$car_generation);
            //                                     }

            //                                     if($status_event == 'open'){
            //                                         $query->where('company_name',$company_name)->where('branch_name',$branch_name)->where('car_brand',$car_brand)->where('car_generation',$car_generation)->where('status_event','open')->where('start_date','>=',$start_date)->where('end_date','<=',$end_date);
            //                                     }

            //                                 }),
            //                             ],
            'car_prices'            => 'required',
        ];


    }


    public function messages()
    {
     return [
          'type_rental_prices.unique' => 'ไม่สามารถใช้ข้อมูลนี้ได้ พบข้อมูลนี้อยู่ในระบบแล้ว'
     ];
    }

}
