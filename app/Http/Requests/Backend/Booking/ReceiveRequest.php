<?php

namespace App\Http\Requests\Backend\Booking;

use Illuminate\Foundation\Http\FormRequest;

class ReceiveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_first_name'           => 'required',
            'customer_last_name'            => 'required',
            'customer_phone'                => 'required',
            'customer_email'                => 'required',
            'personal_card_address'         => 'required',
            'present_address'               => 'required',
            'id_card_customer'              => 'required',
            'first_name_beneficiary'        => 'required',
            'last_name_beneficiary'         => 'required',
            'relation_beneficiary'          => 'required',
            'phone_beneficiary'             => 'required',
            'branch_car_code'               => 'required',
            'license_plate'                 => 'required',
            'fk_staff_deposit'              => 'required',
        ];
    }
}
