<?php

namespace App\Http\Requests\Frontend\Auth;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        switch ($this->method()) {
            case 'POST':
            {
                return [
                    'email_customer'            => 'email|unique:customers,email_customer',
                    'phone_customer'            => 'required|regex:/[0-9]{10}/|numeric|unique:customers,phone_customer',
                    'line_id_customer'          => 'unique:customers,line_id_customer',
                    'id_card_customer'          => 'required|min:13|numeric|unique:customers,id_card_customer',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'email_customer'            => 'email|unique:customers,email_customer,'.$this->get('username').',username',
                    'phone_customer'            => 'required|regex:/[0-9]{10}/|numeric|unique:customers,phone_customer,'.$this->get('username').',username',
                    'line_id_customer'          => 'unique:customers,line_id_customer,'.$this->get('username').',username',
                    'id_card_customer'          => 'required|min:13|numeric|unique:customers,id_card_customer,'.$this->get('username').',username',
                ];

            }
            default:break;
        }

       
    }
}
