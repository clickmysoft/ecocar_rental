<?php

namespace App\Http\Requests\Frontend\Booking;

use Illuminate\Foundation\Http\FormRequest;

class CalendarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'pick-up-point' => 'required',
            'drop-car-point' => 'required',
            'pick_up_date' => 'required',
            'pick_up_time' => 'required',
            'drop_off_date' => 'required',
            'drop_off_time' => 'required',
        ];
    }

    public function messages()
    {
     return [
          'pick-up-point.required' => 'สถานที่รับรถ จำเป็นต้องระบุ',
          'drop-car-point.required' => 'สถานที่คืนรถ จำเป็นต้องระบุ',
          'pick_up_date.required' => 'วันที่รับรถ จำเป็นต้องระบุ',
          'pick_up_time.required' => 'เวลารับรถ จำเป็นต้องระบุ',
          'drop_off_date.required' => 'วันส่งคืนรถ จำเป็นต้องระบุ',
          'drop_off_time.required' => 'เวลาส่งคืนรถ จำเป็นต้องระบุ'
     ];
    }

}
