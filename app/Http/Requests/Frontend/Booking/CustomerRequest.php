<?php

namespace App\Http\Requests\Frontend\Booking;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
         return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ];
    }

    public function messages()
    {
     return [
          'first_name.required' => 'ชื่อ จำเป็นต้องระบุ',
          'last_name.required' => 'นามสกุล จำเป็นต้องระบุ',
          'email.required' => 'อีเมล จำเป็นต้องระบุ',
          'phone.required' => 'เบอร์โทรศัพท์ จำเป็นต้องระบุ'
     ];
    }

}
