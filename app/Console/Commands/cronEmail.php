<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Booking\CarBooking;
use Lang;
class cronEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:Email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email ever minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


         $query  =  DB::table('car_booking','cars','invoices')
                    ->select('car_booking.*',
                             'car_booking.id as car_booking_id',
                             'cars.car_brand',
                             'cars.car_generation',
                             'cars.car_nickname',
                             'cars.branch_code',
                             'cars.branch_name',
                             'invoices.*',
                             'invoices.price as invoice_price',
                             'invoices.created_at as invoice_created_at'
                            )
                    ->join('cars','cars.id','=', 'car_booking.fk_car_id')
                    ->join('invoices','invoices.id', '=', 'car_booking.fk_invoice')
                    ->where('car_booking.send_message_before_one_hour','false')
                    ->where('car_booking.deposit','pay')
                    ->get()->toArray();

        $branch = array();
        foreach (Lang::get('branch') as $key => $value) {
          $branch[$value['code']] = $value;
        }


        foreach ($query as $key => $data) {
             $data->pick_up = $branch[$data->pick_up]['name'];
            $data->drop_off = $branch[$data->drop_off]['name'];
            $data->pick_up_date =  $this->DateOnly($data->pick_up_date);
            $data->drop_off_date =  $this->DateOnly($data->drop_off_date);
            $data->branch_code = $branch[$data->branch_code]['name'];
            $data->invoice_created_at = $this->DateThai($data->invoice_created_at);
            $data->data_sesstion = json_decode($data->data_sesstion);
        }
        
     
         $date = new Carbon('now');
        foreach ($query as $key => $value) {


            if($date->diffInHours($value->start_date) == 0){

                $emails = [$value->customer_email];
                Mail::send('frontend.mail.test', ['data' => $value], function($message) use ($emails){    
                  $message->to($emails,'Clickmycom');
                  $message->cc('info@ecocar.co.th','Info Ecocar');
                            $message->subject('Test mail Booking');
                            $message->from(config('mail.from.address'), config('mail.from.name'));  
                });

                if(env('SMS_SEND_ORDER') == true){
                        $phone = $value->customer_phone;
                        $customer_first_name = $value->customer_first_name;
                        $customer_last_name = $value->customer_last_name;
                        $date = $value->pick_up_date;
                        $time = $value->pick_up_time;
                        include(app_path() . '/Http/Controllers/sms_before_1_hr.php');
                    
                }
                
                
                CarBooking::where('car_booking_code',$value->car_booking_code)
                        ->update([
                                'send_message_before_one_hour'             => 'true'
                            ]);
             }


        }
        

        
    }
    
    public function DateThai($strDate){
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];

        return ''."$strDay $strMonthThai $strYear ".$strHour.':'.$strMinute.'';
    }


    public function DateOnly($strDate){
        $strYear = date("Y",strtotime($strDate))+543;
        $strMonth= date("n",strtotime($strDate));
        $strDay= date("j",strtotime($strDate));
        $strHour= date("H",strtotime($strDate));
        $strMinute= date("i",strtotime($strDate));
        $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthThai=$strMonthCut[$strMonth];

       
        return "$strDay $strMonthThai $strYear";
    }
    
}
