<?php

namespace App\Models\ExtraOption;

use Illuminate\Database\Eloquent\Model;

class ExtraOptionDB extends Model
{
    protected 	$table = 'extra_option';
	protected 	$primaryKey = 'id';
	protected  	$fillable = [
					'extra_option_icon',
					'extra_option_name',
					'extra_option_qty',
					'extra_option_price',
					'extra_option_per',
					'extra_option_code',
					'status'
				];
}
