<?php

namespace App\Models\Auth\Traits\Attribute;

/**
 * Trait UserAttribute.
 */
trait UserAttribute
{
    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<span class='badge badge-success'>".__('labels.general.active').'</span>';
        }

        return "<span class='badge badge-danger'>".__('labels.general.inactive').'</span>';
    }

    /**
     * @return string
     */
    public function getConfirmedLabelAttribute()
    {
        if ($this->isConfirmed()) {
            if ($this->id != 1 && $this->id != auth()->id()) {
                return '<a href="'.route('admin.auth.user.unconfirm',
                        $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.backend.access.users.unconfirm').'" name="confirm_item"><span class="badge badge-success" style="cursor:pointer">'.__('labels.general.yes').'</span></a>';
            } else {
                return '<span class="badge badge-success">'.__('labels.general.yes').'</span>';
            }
        }

        return '<a href="'.route('admin.auth.user.confirm', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.backend.access.users.confirm').'" name="confirm_item"><span class="badge badge-danger" style="cursor:pointer">'.__('labels.general.no').'</span></a>';
    }

    /**
     * @return string
     */
    public function getRolesLabelAttribute()
    {
        $roles = $this->getRoleNames()->toArray();

        if (count($roles)) {
            return implode(', ', array_map(function ($item) {
                return ucwords($item);
            }, $roles));
        }

        return 'N/A';
    }

    /**
     * @return string
     */
    public function getPermissionsLabelAttribute()
    {
        $permissions = $this->getDirectPermissions()->toArray();

        if (count($permissions)) {
            return implode(', ', array_map(function ($item) {
                return ucwords($item['name']);
            }, $permissions));
        }

        return 'N/A';
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->last_name
            ? $this->first_name.' '.$this->last_name
            : $this->first_name;
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->full_name;
    }

    /**
     * @return mixed
     */
    public function getPictureAttribute()
    {
        return $this->getPicture();
    }

    /**
     * @return string
     */
    public function getSocialButtonsAttribute()
    {
        $accounts = [];

        foreach ($this->providers as $social) {
            $accounts[] = '<a href="'.route('admin.auth.user.social.unlink',
                    [$this, $social]).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.backend.access.users.unlink').'" data-method="delete"><i class="fa fa-'.$social->provider.'"></i></a>';
        }

        return count($accounts) ? implode(' ', $accounts) : 'None';
    }

    /**
     * @return string
     */
    public function getLoginAsButtonAttribute()
    {
        /*
         * If the admin is currently NOT spoofing a user
         */
        if (! session()->has('admin_user_id') || ! session()->has('temp_user_id')) {
            //Won't break, but don't let them "Login As" themselves
            if ($this->id != auth()->id()) {
                return '<a href="'.route('admin.auth.user.login-as',
                        $this).'" class="dropdown-item">'.__('buttons.backend.access.users.login_as', ['user' => $this->full_name]).'</a> ';
            }
        }

        return '';
    }

    /**
     * @return string
     */
    public function getClearSessionButtonAttribute()
    {
        if ($this->id != auth()->id() && config('session.driver') == 'database') {
            return '<a href="'.route('admin.auth.user.clear-session', $this).'"
                 data-trans-button-cancel="'.__('buttons.general.cancel').'"
                 data-trans-button-confirm="'.__('buttons.general.continue').'"
                 data-trans-title="'.__('strings.backend.general.are_you_sure').'"
                 class="dropdown-item" name="confirm_item">'.__('buttons.backend.access.users.clear_session').'</a> ';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
        return '<a href="'.route('admin.auth.user.show', $this).'" class="btn btn-info"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'"></i></a>';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="'.route('admin.auth.user.edit', $this).'" class="btn btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'"></i></a>';
    }

    /**
     * @return string
     */
    public function getChangePasswordButtonAttribute()
    {
        return '<a href="'.route('admin.auth.user.change-password', $this).'" class="dropdown-item">'.__('buttons.backend.access.users.change_password').'</a> ';
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        if ($this->id != auth()->id()) {
            switch ($this->active) {
                case 0:
                    return '<a href="'.route('admin.auth.user.mark', [
                            $this,
                            1,
                        ]).'" class="dropdown-item">'.__('buttons.backend.access.users.activate').'</a> ';
                // No break

                case 1:
                    return '<a href="'.route('admin.auth.user.mark', [
                            $this,
                            0,
                        ]).'" class="dropdown-item">'.__('buttons.backend.access.users.deactivate').'</a> ';
                // No break

                default:
                    return '';
                // No break
            }
        }

        return '';
    }

    /**
     * @return string
     */
    public function getConfirmedButtonAttribute()
    {
        if (! $this->isConfirmed() && ! config('access.users.requires_approval')) {
            return '<a href="'.route('admin.auth.user.account.confirm.resend', $this).'" class="dropdown-item">'.__('buttons.backend.access.users.resend_email').'</a> ';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        if ($this->id != auth()->id() && $this->id != 1) {
            return '<a href="'.route('admin.auth.user.destroy', $this).'"
                 data-method="delete"
                 data-trans-button-cancel="'.__('buttons.general.cancel').'"
                 data-trans-button-confirm="'.__('buttons.general.crud.delete').'"
                 data-trans-title="'.__('strings.backend.general.are_you_sure').'"
                 class="dropdown-item">'.__('buttons.general.crud.delete').'</a> ';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDeletePermanentlyButtonAttribute()
    {
        return '<a href="'.route('admin.auth.user.delete-permanently', $this).'" name="confirm_item" class="btn btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="'.__('buttons.backend.access.users.delete_permanently').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getRestoreButtonAttribute()
    {
        return '<a href="'.route('admin.auth.user.restore', $this).'" name="confirm_item" class="btn btn-info"><i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title="'.__('buttons.backend.access.users.restore_user').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {

        if ($this->trashed()) {
            return '
                <div class="btn-group btn-group-sm" role="group" aria-label="User Actions">
                  '.$this->restore_button.'
                  '.$this->delete_permanently_button.'
                </div>';
        }
        $showdisplay_clear_session_button = 'none';
        $showdisplay_login_as_button = 'none';
        $showdisplay_change_password_button = 'none';
        $showdisplay_status_button = 'none';
        $showdisplay_confirmed_button = 'none';
        $showdisplay_delete_button = 'none';

        if(!empty($this->clear_session_button)){
             $showdisplay_clear_session_button = 'block';
        }

        if(!empty($this->login_as_button)){
             $showdisplay_login_as_button = 'block';
        }

        if(!empty($this->change_password_button)){
             $showdisplay_change_password_button = 'block';
        }

        if(!empty($this->status_button)){
             $showdisplay_status_button = 'block';
        }
        
        if(!empty($this->confirmed_button)){
             $showdisplay_confirmed_button = 'block';
        }

        if(!empty($this->delete_button)){
             $showdisplay_delete_button = 'block';
        }

        return '
        <div class="btn-group btn-group-sm" role="group" aria-label="User Actions">
          '.$this->show_button.'
          '.$this->edit_button.'
            <button class=" btn-effect  btn-secondary dropdown-toggle" data-modal="modal-'.$this->id.'">More</button>
            <div class="md-modal md-effect-2" id="modal-'.$this->id.'" style="z-index: 999;">
                <div class="md-content md-content-red">
                    <h3 style="font-size: 22px;color: #fff;">Select Event</h3>
                    <div>
                    <ul class="dropdown-menu-list withScroll mCustomScrollbar _mCS_1" data-height="220" style="height: 220px;">
                            <div class="mCustomScrollBox mCS-dark-2" id="mCSB_1" style="position:relative; height:100%; overflow:hidden; max-width:100%;">
                                <div class="mCSB_container mCS_no_scrollbar" style="position:relative; top:0;">
                                    <li class="naruto" style="display:'.$showdisplay_clear_session_button.'">
                                            <i class="fa fa-star p-r-10 f-18 c-orange"></i>
                                            <span class="dropdown-time">'.$this->clear_session_button.'</span>
                                    </li>
                                                                        
                                    <li class="naruto" style="display:'.$showdisplay_login_as_button.'">
                                            <i class="fa fa-heart p-r-10 f-18 c-red"></i>
                                           <span class="dropdown-time">'.$this->login_as_button.'</span>
                                    </li>
                                    <li class="naruto" style="display:'.$showdisplay_change_password_button.'">
                                            <i class="fa fa-file-text p-r-10 f-18"></i>
                                           <span class="dropdown-time">'.$this->change_password_button.'</span>
                                    </li>
                                    <li class="naruto" style="display:'.$showdisplay_status_button.'">
                                            <i class="fa fa-lock p-r-10 f-18"></i>
                                            <span class="dropdown-time">'.$this->status_button.'</span>
                                    </li>                                                                                                       
                                    <li class="naruto" style="display:'.$showdisplay_confirmed_button.'">
                                            <i class="fa fa-bell p-r-10 f-18 c-orange"></i>
                                            <span class="dropdown-time"> '.$this->confirmed_button.'</span>
                                       
                                    </li>
                                    <li class="naruto" style="display:'.$showdisplay_delete_button.'">
                                            <i class="fa fa-trash-o p-r-10 f-18"></i>
                                            <span class="dropdown-time"> '.$this->delete_button.'</span>
                                    </li>
                                </div>
                                
                                <div class="mCSB_scrollTools" style="position: absolute; display: none; opacity: 0;">
                                    <div class="mCSB_draggerContainer">
                                        <div class="mCSB_dragger" style="position: absolute; top: 0px; height: 110px;" oncontextmenu="return false;">
                                            <div class="mCSB_dragger_bar" style="position: relative; line-height: 110px;"></div>
                                        </div>
                                        <div class="mCSB_draggerRail"></div>
                                    </div>
                                </div>
                            </div>
                    </ul>
                    <button class="btn  btn-modal btn-default">Close</button>
                </div>
            </div>
        </div>
            <div class="md-overlay" style="z-index:998"></div>
        </div>';
    }
}
