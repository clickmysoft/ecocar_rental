<?php

namespace App\Models\Promotion;

use Illuminate\Database\Eloquent\Model;

class PromotionModel extends Model
{
    protected 	$table = 'promotion';
	protected 	$primaryKey = 'id';
	protected  	$fillable = [
					'image_thumbnail',
					'image_full',
					'title',
					'description',
					'promotion_code',
					'promotion_discount',
					'discount_type',
					'start_date',
					'end_date',
					'mata_title',
					'mata_description',
					'mata_keyword',
					'status'
				];


}
