<?php

namespace App\Models\Menu;

use Illuminate\Database\Eloquent\Model;

class MenuTypesModel extends Model
{
    protected 	$table = 'menu_types';
	protected 	$primaryKey = 'id';
	protected  	$fillable = [
					'menu_type',
					'title',
					'description',
					'menu_sortable',
					'status',
					
		           	
				];
}
