<?php

namespace App\Models\Menu;

use Illuminate\Database\Eloquent\Model;

class MenuModel extends Model
{
    protected 	$table = 'menu';
	protected 	$primaryKey = 'id';
	protected  	$fillable = [
					'fk_menu_types',
					'title',
					'alias',
					'type',
					'status',
					'parent_id',
					'level',
					'fk_component_id',
					'access',
					'img',
					'template_style_id',
					'menu_sortable',
					'fk_user_create',
					
				];
}
