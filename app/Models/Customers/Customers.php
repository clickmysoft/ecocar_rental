<?php

namespace App\Models\Customers;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Customers extends Model
{
    protected 	$table = 'customers';
	protected 	$primaryKey = 'id';
	protected  	$fillable = [
					'prefix_name_customer',
					'first_name_customer',
					'last_name_customer',
					'nick_name_customer',
					'email_customer',
					'phone_customer',
					'line_id_customer',
					'id_card_customer',
					'personal_card_address',
					'present_address',
					'company_name',
					'position',
					'experience',
					'tel_office',
					'explain_use_car',
					'province_use_car',
					'prefix_name_beneficiary',
					'first_name_beneficiary',
					'last_name_beneficiary',
					'relation_beneficiary',
					'phone_beneficiary',
					'personal_card_img',
					'driver_license_card_img',
					'credit_card_img',
					'image_profile',
					'customers_description',
					'status',
					'username',
					'password'
				];
}