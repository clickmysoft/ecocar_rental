<?php

namespace App\Models\Booking;

use Illuminate\Database\Eloquent\Model;

class DepositModel extends Model
{
    protected 	$table = 'deposit';
	protected 	$primaryKey = 'id';
	protected  	$fillable = [
					'fk_booking_id',
					'amount',
					'pay_date',
					'image_deposit',
					'remark',
					'fk_user_create',
					'deposit'
				];
}
