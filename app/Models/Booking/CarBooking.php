<?php

namespace App\Models\Booking;

use Illuminate\Database\Eloquent\Model;

class CarBooking extends Model
{
    protected 	$table = 'car_booking';
	protected 	$primaryKey = 'id';
	protected  	$fillable = [
					'car_booking_code',
					'fk_car_id',
					'fk_invoice',
					'pick_up',
					'drop_off',
					'pick_up_date',
					'pick_up_time',
					'drop_off_date',
		           	'drop_off_time',
		           	'car_id',
		           	'car_option',
		           	'customer_first_name',
		           	'customer_last_name',
		           	'customer_email',
		           	'customer_phone',
		           	'customer_flight_info',
		           	'customer_flight_number',
		           	'total_car_price',
		           	'total_option_price',
		           	'total_total_price',
		           	'start_date',
		           	'end_date',
		           	'type_pay',
		           	'data_sesstion',
		           	'deposit',
		           	'fk_staff_approve_deposit',
		           	'deposit_image',
		           	'deposit_amount',
		           	'deposit_pay_date',
		           	'deposit_pay_time',
		           	'deposit_remark',
		           	'fk_user_booking',
		           	'user_register',
		           	'fk_staff_approve_booking',
		           	'driver_license_card_img',
		           	'credit_card_img',
		           	'image_profile',
		           	'status_receive_car',
		           	'personal_card_id',
		           	'tel_office',
		           	'personal_card_address',
		           	'present_address',
		           	'company_name',
		           	'position',
		           	'tel_office',
		           	'id_card_customer',
		           	'first_name_beneficiary',
		           	'last_name_beneficiary',
		           	'relation_beneficiary',
		           	'phone_beneficiary',
		           	'return_date',
		           	'return_time',
		           	'return_remark',
		           	'fk_staff_receive',
		           	'return_image',
		           	'status_return_car',
		           	'image_paid',
		           	'send_message_before_one_hour',
		           	'comment_remark'
				];
}
