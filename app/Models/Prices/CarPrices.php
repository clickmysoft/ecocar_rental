<?php

namespace App\Models\Prices;

use Illuminate\Database\Eloquent\Model;

class CarPrices extends Model
{
    protected 	$table = 'car_prices';
	protected 	$primaryKey = 'id';
	protected  	$fillable = [
					'car_prices_id',
					'car_prices_code',
					'company_name',
					'branch_name',
					'car_brand',
					'car_generation',
					'type_rental_prices',
					'car_prices',
					'status_event',
					'start_date',
					'end_date',
		           	'status',
		           	'priority',
		           	'remarks'
				];
}
