<?php

namespace App\Models\OutSideArea;

use Illuminate\Database\Eloquent\Model;

class OutSideAreaDB extends Model
{
    protected 	$table = 'dropoff_outside';
	protected 	$primaryKey = 'id';
	protected  	$fillable = [
					'name_area',
					'area_code',
					'price',
					'in_area'
				];
}