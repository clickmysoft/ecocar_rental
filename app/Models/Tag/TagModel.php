<?php

namespace App\Models\Tag;

use Illuminate\Database\Eloquent\Model;

class TagModel extends Model
{
    protected 	$table = 'tags';
	protected 	$primaryKey = 'id';
	protected  	$fillable = [
					'tag_name',
					'fk_create_by',
					'status',
				];
}
