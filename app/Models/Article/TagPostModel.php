<?php

namespace App\Models\Article;

use Illuminate\Database\Eloquent\Model;

class TagPostModel extends Model
{
        protected 	$table = 'post_tag';
	protected  	$fillable = [
					'post_id',
					'tag_id'
				];
}
