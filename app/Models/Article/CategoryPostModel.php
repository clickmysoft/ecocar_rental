<?php

namespace App\Models\Article;

use Illuminate\Database\Eloquent\Model;

class CategoryPostModel extends Model
{
    protected 	$table = 'category_post';
	protected  	$fillable = [
					'category_id',
					'post_id'
				];
}
