<?php

namespace App\Models\Article;

use Illuminate\Database\Eloquent\Model;

class ArticleModel extends Model
{

	protected 	$table = 'article';
	protected 	$primaryKey = 'id';
	protected  	$fillable = [
					'title',
					'image_thumbnail',
					'image_full',
					'description',
					'mata_title',
					'mata_description',
					'mata_keyword',
					'code',
					'status',
					'fk_create_by'
				];
}
