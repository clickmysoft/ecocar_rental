<?php

namespace App\Models\Cars\Description;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Cars extends Model
{
    protected 	$table = 'cars';
	protected 	$primaryKey = 'id';
	protected  	$fillable = [
					'license_plate',
					'license_province',
					'body_number',
					'engine_number',
					'branch_car_code',
		           	'company_name',
		           	'branch_name',
		           	'branch_code',
		           	'color',
		           	'car_brand',
		           	'car_generation',
		           	'car_nickname',
		           	'car_generation_detail',
		           	'car_engine_cc',
		           	'car_type',
		           	'car_human_qty',
		           	'car_baggage_qty',
		           	'car_type_fule',
		           	'car_type_gear',
		           	'car_pressure_front',
		           	'car_pressure_back',
		           	'car_mile',
		           	'car_door_qty',
		           	'car_image',
		           	'car_description',
		           	'car_option',
		           	'car_company_insurance',
		           	'car_type_insurance',
		           	'car_date_register',
		           	'car_enddate_insurance',
		           	'car_enddate_act',
		           	'car_enddate_tax',
		           	'price',
		           	'status'
				];
}
