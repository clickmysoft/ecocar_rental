<?php

namespace App\Models\Paypal;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

	public function items()
    {
    	return $this->hasMany('App\Models\Paypal\Item');
    }
     
}