<?php

namespace App\Models\Paypal;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

	public function invoices()
    {
    	return $this->belongsTo('App\Models\Paypal\Invoice');

    }
    
}
