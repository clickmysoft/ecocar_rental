<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    protected 	$table = 'category';
	protected 	$primaryKey = 'id';
	protected  	$fillable = [
					'category_name',
					'status',
				];
}
