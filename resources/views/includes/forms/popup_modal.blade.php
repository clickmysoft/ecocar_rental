<script type="text/javascript">
        
    function tanawat_popup($id) {

            var overlay = document.querySelector( '.md-overlay' );

            [].slice.call( document.querySelectorAll( ".btn-effect" ) ).forEach( function( el, i ) {

                var modal = document.querySelector( '#modal-' + $id ),
                    close = modal.querySelector( '.btn-modal' );
                

                function removeModal( hasPerspective ) {
                    classie.remove( modal, 'md-show' );

                    if( hasPerspective ) {
                        classie.remove( document.documentElement, 'md-perspective' );
                    }
                }

                function removeModalHandler() {
                    removeModal( classie.has( el, 'md-setperspective' ) ); 
                }

                classie.add( modal, 'md-show' );
                overlay.removeEventListener( 'click', removeModalHandler );
                overlay.addEventListener( 'click', removeModalHandler );

                if( classie.has( el, 'md-setperspective' ) ) {
                    setTimeout( function() {
                        classie.add( document.documentElement, 'md-perspective' );
                    }, 25 );
                }
            

                close.addEventListener( 'click', function( ev ) {
                    ev.stopPropagation();
                    removeModalHandler();
                });

            } );


    }
</script>