<script>
      function destroy($id) {
                var this_data = document.getElementById("this-data-"+$id);
                var DestroyToken = this_data.getAttribute("data-destroy_token");
                var DestroyName = this_data.getAttribute("data-destroy_name");
                var DestroyRoute = this_data.getAttribute("data-destroy_route");
                var DestroyRedirect = this_data.getAttribute("data-destroy_redirect");
                DestroyData(DestroyToken, DestroyName, DestroyRoute,DestroyRedirect);
              
                function DestroyData(DestroyToken, DestroyName, DestroyRoute,DestroyRedirect)
                {
                    swal({
                      title: "{{ Lang::get('alerts.swal_title_delete') }}",
                      text: "{{ Lang::get('alerts.swal_text_delete_sec1') }} \"" + DestroyName + "\" {{ Lang::get('alerts.swal_text_delete_sec2') }}" +
                      "{{ Lang::get('alerts.swal_text_delete_sec3') }}",
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: "#ec6c62",
                      cancelButtonColor: '#d33',
                      confirmButtonText: "{{ Lang::get('alerts.Button_Delete') }} " + DestroyName,
                      cancelButtonText: "{{ Lang::get('alerts.Button_Cancle') }} ",
                      confirmButtonClass: 'btn btn-success',
                      cancelButtonClass: 'btn btn-danger',
                      buttonsStyling: false,
                      reverseButtons: true
                    }).then((result) => {
                      if (result.value) {
                            $.ajax({
                                type: "DELETE",
                                url: DestroyRoute,
                                headers: { 'X-CSRF-TOKEN' : DestroyToken }
                            }).done(function(data){
                                console.log(data);
                                if(data == 1){
                                    swal("{{ Lang::get('alerts.swal_text_success') }}",DestroyName + " {{ Lang::get('alerts.swal_text_success_del') }}", "success");
                                }
                                // setTimeout('redirect("'+DestroyRedirect+'")',1000);

                                var datatable = $('#DataTable').DataTable();    
                                //datatable.clear().draw();
                                //datatable.rows.add(NewlyCreatedData); // Add new data
                                datatable.columns.adjust().draw(); // Redraw the DataTable

                            }).error(function(data){
                                console.log(data);
                                swal("{{ Lang::get('alerts.swal_text_unsuccess') }}",DestroyName + " {{ Lang::get('alerts.swal_text_unseccess_del') }}", "error");
                            });
                      }
                    })
                }
      }
</script>