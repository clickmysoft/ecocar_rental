<select class="form-control chosen-img" name="company_name" style="height:40px;">

	<?php
  		foreach (config('company.'.app()->getLocale().'.name') as $key => $value) {
  			if($value != null){
  				$img = asset("backend/img/cars/brands/company.png");
  				  echo "<option ";
            echo (old('company_name')==$key?'selected':'');
            echo " data-img-src='".$img."' value=".$key.">";
		        echo $value[0];
		        echo "</option>";
  			}
  		}
  	?>

</select>