
@foreach (array_keys(config('locale.languages')) as $lang)
    @if ($lang != app()->getLocale())
      
        <a href="#" class="dropdown-item" onclick="location.href='{{ '/lang/'.$lang }}'">
         
          @if($lang == 'th')
            <img src="{{ asset("frontend/img/flags/64/th.png")}}" alt="user avatar" >
             {{ __('menus.language-picker.langs.'.$lang) }}
          @endif

          @if($lang == 'en')
            <img src="{{ asset("backend/img/lang/uk.png")}}" alt="user avatar" >
            ENG
          @endif
         
      </a>
   
    @endif
@endforeach
