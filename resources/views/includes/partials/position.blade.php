
<select class="form-control" name="position_name" style="height:40px;">
  <option>{{ __('validation.attributes.backend.company.position_selectbox') }}</option>
  	<?php
  		foreach (config('company.'.app()->getLocale().'.position') as $key => $value) {
  			if($value != null){
  				  echo "<option ";
            echo (old('position_name')==$key?'selected':'');
            echo " value=".$key.">";
		        echo $value[0];
		        echo "</option>";
  			}
  		}
  	?>
</select>