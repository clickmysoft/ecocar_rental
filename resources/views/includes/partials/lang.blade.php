
@foreach (array_keys(config('locale.languages')) as $lang)
    @if ($lang != app()->getLocale())
   
      <a href="{{ '/lang/'.$lang }}" class="dropdown-item">
      	 <li>
      	 	@if($lang == 'th')
      	 		<img src="{{ asset("backend/img/lang/th.png")}}" alt="user avatar" width="30" class="p-r-5">
      	 	@endif

      	 	@if($lang == 'en')
      	 		<img src="{{ asset("backend/img/lang/uk.png")}}" alt="user avatar" width="30" class="p-r-5">
      	 	@endif

      		{{ __('menus.language-picker.langs.'.$lang) }}
      	 </li>
      </a>
    @endif
@endforeach
