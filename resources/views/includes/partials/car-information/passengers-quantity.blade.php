<select class="form-control chosen-img" name="car_human_qty" data-live-search="true" style="height:40px;">
<option data-img-src="{{ asset("backend/img/cars/type/people.png")}}" value="">เลือกจำนวนผู้โดยสาร</option>
	<?php
  		foreach (config('cars.'.app()->getLocale().'.passengers_quantity') as $key => $value) {
  			if($value != null){
  				$img = asset("backend/img/cars/type/people.png");
  				  echo "<option ";
            echo (old('car_human_qty')==$key?'selected':'');
            echo " data-img-src='".$img."' value=".$key.">";
		        echo $value[0];
		        echo "</option>";
  			}
  		}
  	?>
</select>