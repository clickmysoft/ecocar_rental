<select class="form-control chosen-img" name="car_baggage_qty" data-live-search="true" style="height:40px;">

<option data-img-src="{{ asset("backend/img/cars/type/bag.png")}}" value="">เลือกจำนวนสัมภาระ</option>
	<?php
  		foreach (config('cars.'.app()->getLocale().'.baggage_quantity') as $key => $value) {
  			if($value != null){
  				  $img = asset("backend/img/cars/type/bag.png");
  				  echo "<option ";
            echo (old('car_baggage_qty')==$key?'selected':'');
            echo " data-img-src='".$img."' value=".$key.">";
		        echo $value[0];
		        echo "</option>";

  			}
  		}
  	?>
</select>