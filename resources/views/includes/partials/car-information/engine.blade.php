<select class="form-control" name="brand_name" data-live-search="true" style="height:40px;">
 <option value="">{{ __('validation.attributes.backend.company.company_selectbox') }}</option>
	<?php
  		foreach (config('cars.'.app()->getLocale().'.engine') as $key => $value) {
  			if($value != null){
  				echo "<option ";
  				echo (old('brand_name')==$key?'selected':'');
  				echo " value=".$key.">";
		        echo $value[0];
		        echo "</option>";
  			}
  		}
  	?>
</select>