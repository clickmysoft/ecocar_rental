<select class="form-control chosen-img" name="car_type" data-live-search="true" style="height:40px;">
<option data-img-src="{{ asset("backend/img/cars/type/car.png")}}" value="">เลือกประเภทรถยนต์</option>
	<?php
  		foreach (config('cars.'.app()->getLocale().'.type') as $key => $value) {
  			$img = asset("backend/img/cars/type/".config('cars.type_img.'.$key)[0]."");
  			if($value != null){
  				  echo "<option ";
            echo (old('car_type')==$key?'selected':'');
            echo " data-img-src='".$img."' value=".$key.">";
		        echo $value[0];
		        echo "</option>";
  			}
  		}
  	?>
</select>