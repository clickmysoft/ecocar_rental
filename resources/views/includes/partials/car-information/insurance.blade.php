<select class="form-control chosen-img" name="car_company_insurance" data-live-search="true" style="height:40px;">

<option data-img-src="{{ asset("backend/img/cars/type/bag.png")}}" value="">เลือกบริษัทประกันภัย</option>
  <?php
      foreach (config('insurance.'.app()->getLocale().'.name') as $key => $value) {
        if($value != null){
          $img = asset("backend/img/cars/type/bag.png");
            echo "<option ";
            echo (old('car_company_insurance')==$key?'selected':'');
            echo " data-img-src='".$img."' value=".$key.">";
            echo $value[0];
            echo "</option>";
        }
      }
    ?>
</select>