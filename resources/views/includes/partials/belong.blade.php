
<select class="form-control chosen-img" name="branch_name" style="height:40px;">
<option value="" data-img-src="{{asset("backend/img/cars/brands/belong.png")}}">{{ __('validation.attributes.backend.company.belong_selectbox') }}</option>
  	<?php
  		foreach (config('company.'.app()->getLocale().'.belong') as $key => $value) {
  			if($value != null){
  				  $img = asset("backend/img/cars/brands/belong.png");
            echo "<option ";
            echo (old('branch_name')==$value[0]?'selected':'');
            echo " data-belogn-code='".$key."'   data-img-src='".$img."' value=".$key.">";
		        echo $value[0];
		        echo "</option>";
  			}
  		}
  	?>
</select>