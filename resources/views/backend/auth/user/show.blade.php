@extends('backend.layouts.app')

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

@section('content')


<style type="text/css">
 .about_me_id{
        color: #000;
    }

    button.edit-but {
    padding: 8px 20px;
    background: #000;
    color: #fff;
    font-size: 14px;
}

/*.col-md-12 {
   
    background-color: #fff;
    padding-top: 80px;
    padding-bottom: 40px;
*/
}
  p.suvarnabhumi {
    color: #fff;
    text-align: right;
    font-size: 20px;
}

    p.em-id {
    color: #fff;
    text-align: right;
    font-size: 20px;
}



.col-md-7.profile2 {
    margin-top: 0px;
    background-color: #fff;
    padding-top: 0px;
}

.col-md-4.profile3 {
    padding-left: 50px;
}

p.head-profile {

    font-size: 18px;
    font-weight: 400;


}

h3.profile {
    margin: 20px;
}

.p-20 {
    margin-left: 40px;
    margin-right: 25px;
    display: none;
}


.profil-sidebar-element {
    margin-left: 40px;
    margin-right: 25px;
}


p.head-profile2 {

    font-size: 18px;


}

.btn-blue{
    color: #fff;
    background-color: #000;
}


.text-profile{
    color: #000
}

.col-md-3.a1 {


        text-align: right;

}

.col-md-7.aa {
    
    color: #000;
    padding-top: 0px !important;
    padding-bottom: 10px;
}

.col-md-4.profile4 {
    text-align: right;
}

button.btn.btn-success {
    background-color: #5ac9dc;
    border-color: #5ac9dc;
    border-radius: 4px;
}
button#none {
    background: #dc4848;
    border-color: #dc4848;
}

.glyphicon-barcode:before {
    font-size: 20px;
        padding-left: 25px;
}

.glyphicon-home:before {
    font-size: 20px;
    padding-left: 35px;
}



.col-md-12.but {
    margin-top: 0px;
}

h1.company {
    color: #138815;
}

.glyphicon {
    width: 100%;
    font-size: 24px;
    padding: 0;
}


.card {
    background-color: #fff;
}
.glyphicon-user:before {
    font-size: 20px;
    padding-left: 40px;
}


.glyphicon-barcode:before {
    font-size: 20px;
    padding-left: 65px;
}

.glyphicon-flag:before {
    font-size: 20px;
    padding-left: 65px;
}

.col-md-7.aa {
    padding-left: 80px;
}


.fa-item.col-md-12.col-sm-12.bbb {
    font-size: 17px;
}

.fa-item.col-md-12.col-sm-12.aaa {
    font-size: 17px;
}

h2.name {
    padding-left: 15px;
}


h5.position {
    margin-bottom: 20px;
    padding-left: 15px;
}

.col-md-3.profil-sidebar .img-responsive {
    width: 70%;
    margin-bottom: 30px;
}

.list-group-item {
    border: 0px
}


p.font {
    font-size: 15px;
    font-weight: 600;
}

p.font2 {
    font-size: 15px;

}

/*big do for 1366px screen  STILL NOT Yet Responsive*/
 .ks-cover {
    background-color: #ccc;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 250px;
}
.ks-item {
    margin-right: 60px;
    text-align: center;
}
.ks-amount {
    color: #25628f;
    font-size: 28px;
    font-weight: 600;
}
.ks-text {
    font-size: 13px;
}

.ks-widget-payment-simple-amount-item {
    min-height: 124px;
    padding: 30px 20px;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    -webkit-flex-direction: row;
    -ms-flex-direction: row;
    flex-direction: row;
    border-color: #e5e5e5;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    -webkit-flex-wrap: wrap;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
}

.card {
    -webkit-border-radius: 2px;
     border-radius: 5px;
    border: 1px solid #dadada;
}
.payment-simple-amount-item-icon-block {
    background-color: #70a84e;
}

.payment-simple-amount-item-icon-block {
    width: 64px;
    height: 64px;
    -webkit-border-radius: 32px;
    border-radius: 32px;
    display: -webkit-inline-box;
    display: -webkit-inline-flex;
    display: -ms-inline-flexbox;
    display: inline-flex;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    margin-right: 20px;
}
.ks-icon {
    position: relative;
    left: 1px;
    color: #fff;
    font-size: 32px;
}
.ks-icon-edit {
    position: relative;
    left: 1px;
    color: #3379b7;
    font-size: 20px;
}

 .ks-progress-type {
    color: #70a84e;
    font-weight: 600;
    font-size: 12px;
}
.row+.row {
    margin-top: 25px;
}

.ks-card-widget {
    padding: 0;
    -webkit-border-radius: 3px;
    border-radius: 5px;
}
.ks-widget-payment-budget .ks-thumbnail {
    display: block;
}
.embed-responsive {
    position: relative;
    display: block;
    width: 100%;
    padding: 0;
    overflow: hidden;
}
.ks-card-widget .card-header {
    margin: 0;
    background: #fff;
    border-bottom: none;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    -webkit-box-pack: justify;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    padding: 26px 30px 10px 30px;
    font-size: 18px;
    color: #333;
    font-weight: 400;
}
.ks-widget-payment-budget .ks-card-widget-datetime {
padding-left: 35px;
    padding-right: 34px;
   /* padding-bottom: 20px;*/
}

.ks-card-widget .ks-card-widget-datetime {
    margin-top: 20px;
    /*margin-bottom: 20px;*/
    font-size: 12px;
    line-height: 1.67;
    color: #858585;
}
.ks-widget-payment-budget .card-block {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    -webkit-box-pack: justify;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between;
}

.ks-card-widget .card-block {
    padding: 0px 20px 0px 20px;
}
.ks-dashed{
    border-top: 1px dashed #e4d2d2;
}
.card-block {
    padding: 16px 20px;
    font-size: 13px;
}

.card-block-edit {
    padding: 8px 30px 30px 30px;
}

div.row.card-block{
    display: none;
}

.ks-user {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    -webkit-box-pack: justify;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between;
    /*padding-right: 8px;*/
    padding-bottom: 82px;
    margin-top: -51px;
    margin-left: 0px;
}
.amount {
    color: #25628f;
    font-size: 28px;
    font-weight: 600;
}
.circular--square {
    border: 5px solid #FFF;
    border-radius: 70px;
    box-shadow: 0 0 2px #888;
  /*border-radius: 50%;*/
}
/*The style rule above is the shorthand for:*/
.circular--square {
  border-top-left-radius: 50% 50%;
  border-top-right-radius: 50% 50%;
  border-bottom-right-radius: 50% 50%;
  border-bottom-left-radius: 50% 50%;
}
.ks-payment-budget-amount-block{
  width: 50%
}
.card-header-edit {
    font-family: 'Open Sans', verdana, arial;
    font-weight: 300;
    border-radius: 5px;
    margin: 0;
    background: #fff;
    border-bottom: none;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    -webkit-box-pack: justify;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    padding: 17px 30px 10px 25px;
    font-size: 25px;
    color: #333;
    font-weight: 400;
}
.dropdown .dropdown-menu {
    font-size: 12px;
    padding: 0;
    border: solid 1px #c4cbe1;
    margin: 4px 0;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    overflow: hidden;
}

.show>.dropdown-menu {
    display: block;
}
.dropdown-menu-right {
    right: 0;
    left: auto;
}

.dropdown-menu {
    position: absolute;
    top: 100%;
    left: 0;
    z-index: 1000;
    display: none;
    float: left;
    min-width: 10rem;
    padding: .5rem 0;
    margin: .125rem 0 0;
    font-size: 1rem;
    color: #292b2c;
    text-align: left;
    list-style: none;
    background-color: #fff;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,.15);
    border-radius: .25rem;
}
.dropdown .dropdown-menu .dropdown-item {
    color: #474747;
    padding: 10px 15px;
    margin: 0;
    border-bottom: 1px solid #c4cbe1;
}

.dropdown-item {
    display: block;
    width: 100%;
    padding: 3px 1.5rem;
    clear: both;
    font-weight: 400;
    color: #292b2c;
    text-align: inherit;
    white-space: nowrap;
    background: 0 0;
    border: 0;
    }

.ks-widget-icon-item{
    /*min-height: 72px;*/
    padding: 7.7px 5px;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: left;
    -ms-flex-pack: center;
    -ms-flex-direction: row;
    -ms-flex-wrap: wrap;
}
.amount-item-icon-block {
    background: #3379b7;
    width: 35px;
    height: 35px;
    -webkit-border-radius: 32px;
    border-radius: 32px;
    display: -webkit-inline-box;
    display: -webkit-inline-flex;
    display: -ms-inline-flexbox;
    display: inline-flex;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    margin-right: 20px;
}
.ks-icon-info {
    position: relative;
    /*left: 1px;*/
    color: #fff;
    font-size: 19px;
}
.icon-amount{
    color: #25628f;
    font-size: 17px;
    font-weight: 400;
}
.ks-amount-header-box{
    color: #25628f;
    font-size: 18px;
    font-weight: 600;

}
.company-p{
    margin-bottom: 14px;
    font-weight: 600;
    font-size: 25px;
    color: #72aa50;
}
.ks-control-link {
    font-size: 12px;
    color: #858585;
}
 .tasks-statuses-progress-table {
    width: 100%;
    margin: 0;
    margin-top: 25px;
}
.ks-widget-tasks-statuses-progress .tasks-statuses-progress-table td:first-child {
    padding-left: 0;
}

.ks-widget-tasks-statuses-progress .tasks-statuses-progress-table .ks-progress-status {
    font-weight: 700;
}
.ks-widget-tasks-statuses-progress .tasks-statuses-progress-table td {
    font-size: 12px;
    border: none;
    padding-top: 9px;
    padding-bottom: 9px;
        width: 30%;
}
.tasks-total-statuses-progress {
    /* display: -webkit-box; */
    /* display: -webkit-flex; */
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    /* -webkit-box-pack: justify; */
    /* -webkit-justify-content: space-between; */
    -ms-flex-pack: justify;
    /* justify-content: space-between; */
    font-size: 12px;
    color: #858585;
    /* -webkit-box-align: center; */
    /* -webkit-align-items: center; */
    -ms-flex-align: center;
    align-items: center;
    margin-top: 24px;
}
.ks-widget-tasks-statuses-progress .tasks-total-statuses-progress .progress {
    min-width: 180px;
    margin: 0 20px;
}

.progress.ks-progress-xs {
    height: 6px;
}
.progress {
    width: 100%;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    height: 16px;
    background-color: rgba(57,81,155,.1);
    font-size: 12px;
}
.progress {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    overflow: hidden;
    font-size: .75rem;
    line-height: 1rem;
    text-align: center;
    background-color: #eceeef;
    border-radius: .25rem;
}
.badge-cranberry {
    background-color: #df538b;
    border: solid 1px #df538b;
    color: #fff;
}
.ks-text-right {
    text-align: right;
}
.badge-crusta {
    background-color: #e79716;
    border: solid 1px #e79716;
    color: #fff;
}
.badge-mantis {
    background-color: #81c159;
    border: solid 1px #81c159;
    color: #fff;
}
.badge {
    display: inline-block;
    padding: 7px 6px 6px 6px;
    font-size: 10px;
    font-weight: 500;
    line-height: 7px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    text-transform: uppercase;
    font-family: Montserrat,sans-serif;
}
.badge-default {
       background-color: #eaeaea !important;
    border: 1px solid #eff1fc;
    color: #333;
}
.ks-text-bold{
    font-weight: 500;
    font-size: 14px;
    font-family: Montserrat,sans-serif;
}
.ks-widget-payment-table-invoicing .ks-payment-table-invoicing td:last-child {
    padding-right: 0;
    text-align: right;
}
.ks-widget-payment-table-invoicing .ks-payment-table-invoicing td {
    padding-bottom: 8px;
    font-size: 14px;
}
.ks-text-light {
    color: #858585!important;
    font-family: Montserrat,sans-serif;
}
.ks-widget-simple-weather-only.ks-cold {
    border-radius: 5px;
    border: 1px solid #dadada;
    background: url(/backend/img/avatars/chat.svg) no-repeat top right;
    background-color: #fff;
    }
.ks-widget-simple-weather-only.ks-position  {
    border-radius: 5px;
    border: 1px solid #dadada;
    background: url(/backend/img/avatars/bag.svg) no-repeat top right;
    background-color: #fff;
    }
.ks-widget-simple-weather-only.ks-phone  {
    border-radius: 5px;
    border: 1px solid #dadada;
    background: url(/backend/img/avatars/phone.svg) no-repeat top right;
    background-color: #fff;
    }
.ks-widget-simple-weather-only.ks-email {
    border-radius: 5px;
    border: 1px solid #dadada;
    background: url(/backend/img/avatars/mail.svg) no-repeat top right;
    background-color: #fff;
    }
.ks-widget-simple-weather-only {
    border: none;
    /*text-align: right;*/
        padding: 11px 0px 10px 15px;
        margin-bottom: 8px;
}
.ks-widget-simple-weather-only .ks-icon {
    font-size: 26px;
}
.wi {
    display: inline-block;
    font-style: normal;
    font-weight: 400;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
.ks-widget-simple-weather-only-body {
    text-align: left;
    margin-top: 30px;
}
.ks-widget-simple-weather-only-block-amount {
    font-size: 30px;
    font-weight: 600;
    line-height: 36px;
    margin-bottom: 10px;
}
.ks-widget-simple-weather-only-location {
    font-size: 14px;
}
.ks-edit{
    font-weight: 600;
    padding-bottom: 0px;
    /* margin-bottom: -10px;*/
    /* line-height: 0px; */
    font-size: 17px;
    color: #25628f;
}

 </style>

<div class="panel panel-default">

    <div class="panel-heading " style="height: 55px; color: #fff !important;background:#607d8b;">
        <h3 class="panel-title" style="float:left;"><i class="fa fa-database"></i>&nbsp;ข้อมูลผู้ใช้งาน</h3>
    </div>
    <div class="panel-body">
                       <!--start row-->
                        <div class="row">
                            <!--start row-lg-5-->
                            <div class="col-lg-5">
                                <div class="card ks-card-widget ks-widget-payment-budget">

                                         <img src="{{ asset('backend/img/avatars/banner-2.jpg') }}" class="" width="100%">
                                    
                                    <div class="row ks-user">
                                      
                                           <div class="col-md-2"><img src="{{ asset($logged_in_user->user_image)}}" class="ks-avatar circular--square" width="120" height="120"></div>

                                           <div class="col-md-10 ks-card-widget-datetime">&nbsp;&nbsp;<span class="badge badge-secondary badge-mantis" style="margin-bottom: 5px;">
                                           <?=$user->permissions_name==null?"ผู้ใช้งาน":$user->permissions_name?></span><p style="padding-top: 8px;"><span class="ks-amount" style="font-size: 22px;">&nbsp;&nbsp;{{$user->first_name}} {{$user->last_name}} </span></p><b>ตำแหน่ง : <?=$user->permissions_name==null?"ผู้ใช้งาน":$user->permissions_name?></b></div>
                                    
                                    </div>  
                                        <div class="ks-dashed"></div> 
                                    <div class="row card-block">
                                        <div class="col-md-4" style="text-align: center;padding: 15px 0px 0px 20px;">
                                                <svg 
                                                     width="55px" height="55px" viewBox="0 149.829 595.279 542.233">
                                                <path fill="#25628f" d="M455.447,277.656c0,18.034-13.129,29.001-22.716,37.05c-3.631,3.048-10.389,8.611-10.37,10.688
                                                    c0.065,8.15-6.488,14.659-14.64,14.659c-0.041,0-0.082,0-0.123,0c-8.095,0-14.69-6.309-14.756-14.418
                                                    c-0.13-15.984,11.535-25.556,20.908-33.426c7.355-6.176,12.179-10.521,12.179-14.499c0-8.05-6.55-14.6-14.601-14.6
                                                    c-8.054,0-14.604,6.55-14.604,14.6c0,8.152-6.607,14.759-14.759,14.759c-8.152,0-14.76-6.607-14.76-14.759
                                                    c0-24.326,19.792-44.118,44.12-44.118C435.655,233.592,455.447,253.329,455.447,277.656z M408.006,353.678
                                                    c-8.151,0-14.433,6.607-14.433,14.759v0.113c0,8.152,6.282,14.703,14.433,14.703s14.759-6.665,14.759-14.816
                                                    S416.158,353.678,408.006,353.678z M110.516,456.488c-8.152,0-14.76,6.606-14.76,14.759s6.607,14.759,14.76,14.759h1.229
                                                    c8.152,0,14.759-6.606,14.759-14.759s-6.606-14.759-14.759-14.759H110.516z M161.81,456.488c-8.152,0-14.759,6.606-14.759,14.759
                                                    s6.606,14.759,14.759,14.759h1.229c8.151,0,14.759-6.606,14.759-14.759s-6.607-14.759-14.759-14.759H161.81z M595.28,220.755
                                                    v166.123c0,39.333-31.538,71.25-70.871,71.25H333.006c-3.742,0-6.824-0.021-9.401-0.075c-2.701-0.056-5.742-0.077-6.928,0.054
                                                    c-1.234,0.82-4.262,3.593-7.179,6.281c-1.156,1.064-2.465,2.235-3.877,3.523l-54.152,49.357c-4.319,3.941-10.71,4.961-16.06,2.603
                                                    c-5.35-2.359-9.104-7.656-9.104-13.503V349.895H71.696c-23.06,0-42.178,18.896-42.178,41.954v166.122
                                                    c0,23.059,19.12,41.188,42.178,41.188h213.8c3.682,0,7.125,1.691,9.844,4.174l44.118,40.676V503.733
                                                    c0-8.152,6.607-14.759,14.759-14.759c8.152,0,14.759,6.606,14.759,14.759V677.46c0,5.847-3.345,11.064-8.696,13.424
                                                    c-1.913,0.845-3.836,1.178-5.848,1.178c-3.614,0-7.18-1.484-9.952-4.016l-64.705-59.371H71.696C32.362,628.675,0,597.304,0,557.97
                                                    V391.849c0-39.333,32.361-71.472,71.695-71.472h154.609v-99.622c0-39.334,32.609-70.927,71.943-70.927H524.41
                                                    C563.741,149.829,595.28,181.421,595.28,220.755z M565.762,220.755c0-23.059-18.296-41.409-41.354-41.409h-226.16
                                                    c-23.059,0-42.426,18.35-42.426,41.409v114.519v137.642l29.674-26.816c1.375-1.255,2.914-2.402,4.038-3.439
                                                    c13.55-12.485,17.017-14.481,34.691-14.117c2.41,0.051,5.282,0.068,8.78,0.068h191.402c23.058,0,41.354-18.675,41.354-41.731
                                                    V220.755z"/>
                                                </svg>

                                        </div>
                                        <div class="col-md-8">
                                            <p class="ks-amount-header-box">Biography</p>
                                            <p>Donec sit amet urna vitae sem semper aliquet at at diam. Pellentesque eu</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end row-lg-5 -->
                            <!-- start row-lg-4 -->
                            <div class="col-lg-4">
                                <div class="card ks-card-widget ks-widget-payment-card-rate-details">
                                    <div class="card-header-edit">
                                       <span class="ks-amount-header-box">ข้อมูลสำคัญ</span>
                                    </div>

                                   
                                    <div class="card-block">

                                        <div class="company-p">
                                            บริษัท: {{$user->company_name}}
                                        </div>
                                        <!--hr--><div class="ks-dashed"></div> 

                                                <div class="ks-widget-icon-item">
                                                        <div class="amount-item-icon-block">
                                                            <span class="fa fa-comment ks-icon-info" data-toggle="tooltip" title="Employee ID"></span>
                                                        </div>

                                                        <div class="payment-simple-amount-item-body">
                                                                    <div class="payment-simple-amount-item-description">
                                                                         <span class="ks-progress-type">Employee ID</span>
                                                                    </div>
                                                                    <div class="payment-simple-amount-item-amount">
                                                                         <span class="icon-amount">{{$user->employee_id}}</span>
                                                                    </div>
                                                        </div>
                                                </div>

                                        <div class="ks-dashed"></div> 

                                                <div class="ks-widget-icon-item">
                                                        <div class="amount-item-icon-block">
                                                            <span class="fa fa-shield ks-icon-info" data-toggle="tooltip" title="Employee ID"></span>
                                                        </div>

                                                        <div class="payment-simple-amount-item-body">
                                                                <div class="payment-simple-amount-item-description">
                                                                     <span class="ks-progress-type">Belong</span>
                                                                </div>
                                                                <div class="payment-simple-amount-item-amount">
                                                                     <span class="icon-amount">{{$user->belong}}</span>
                                                                </div>
                                                        </div>
                                                </div>

                                                   <div class="ks-dashed"></div> 
                                                <div class="ks-widget-icon-item">
                                                        <div class="amount-item-icon-block">
                                                            <span class="fa fa-shield ks-icon-info" data-toggle="tooltip" title="Employee ID"></span>
                                                        </div>

                                                        <div class="payment-simple-amount-item-body">
                                                                <div class="payment-simple-amount-item-description">
                                                                     <span class="ks-progress-type">วันเริ่มงาน</span>
                                                                </div>
                                                                <div class="payment-simple-amount-item-amount">
                                                                     <span class="icon-amount">{{$user->start_work}}</span>
                                                                </div>
                                                        </div>
                                                </div>
                                                 <div class="ks-dashed"></div>
                                                <div class="ks-widget-icon-item">
                                                        <div class="amount-item-icon-block">
                                                            <span class="fas fa-briefcase ks-icon-info"  data-toggle="tooltip" title="User ID"></span>
                                                        </div>

                                                        <div class="payment-simple-amount-item-body">
                                                                    <div class="payment-simple-amount-item-description">
                                                                         <span class="ks-progress-type">หมายเลขอ้างอิง</span>
                                                                    </div>
                                                                    <div class="payment-simple-amount-item-amount">
                                                                         <span class="icon-amount">{{$user->id}}</span>
                                                                    </div>
                                                  
                                                        </div>
                                                </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end row-lg-4 -->
                            <div class="col-lg-3">


                                     <div class="ks-widget-simple-weather-only ks-cold">
                                        <span class="ks-progress-type">Nickname</span >
                                            <br>
                                        <p class="ks-edit">{{$user->nickname}}</p>
                                        
                                        
                                     </div>
                                     <div class="ks-widget-simple-weather-only ks-position ">
                                        <span class="ks-progress-type">Position</span >
                                            <br>
                                        <p class="ks-edit">{{$user->position_name}}</p>
                                         
                                        
                                     </div>
                                     <div class="ks-widget-simple-weather-only ks-phone ">
                                         <span class="ks-progress-type">Phone</span >
                                             <br>
                                        <p class="ks-edit">{{$user->phone}}</p>
                                        
                                       
                                     </div>
                                     <div class="ks-widget-simple-weather-only ks-email">
                                        <span class="ks-progress-type">Email-Address</span >
                                             <br>
                                        <p class="ks-edit">{{$user->email}}</p>
                                        
                                        
                                     </div>
                                    
                                </div>
                          

                        </div>
                        <!--End row-->

                       
    </div>
</div>

@endsection