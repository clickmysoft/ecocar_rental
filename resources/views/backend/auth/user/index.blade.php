@extends ('backend.layouts.app')
@section ('title', __('labels.backend.access.users.management'))
    
@section('begin_css')

<link rel='stylesheet' type='text/css' href='https://datatables.yajrabox.com/css/datatables.bootstrap.css'/>
<link href="{{ asset("plugins/modal/css/component.css")}}" rel="stylesheet">

@endsection

@section('content')
    
        <div class="panel panel-default">
                        <div class="panel-heading" style="    background-color: #9E9E9E !important;    color: #fff !important;">
                            <h3 class="panel-title" style="float:left;"><i class="fa fa-database"></i>&nbsp;@yield('title', app_name())</h3>
                                @include('backend.auth.user.includes.header-buttons')
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <form method="POST" id="search-form" class="form-inline" role="form" style="display:none;">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" name="first_name" id="name" placeholder="search name">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control" name="email" id="email" placeholder="search email">
                                    </div>
                                    <button type="submit" class="btn btn-primary">Search</button>
                                </form>

                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red filter-right">
                                   
                                        <table class="table table-striped table-hover " id="DataTable">
                                          <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>{{ __('labels.backend.access.users.table.first_name') }}</th>
                                                <th>{{ __('labels.backend.access.users.table.last_name') }}</th>
                                                <th>สิทธิ์การใช้งาน</th>
                                                <th>{{ __('labels.backend.access.users.table.last_updated') }}</th>
                                                <th>{{ __('labels.general.actions') }}</th>
                                            </tr>
                                            </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
        </div>


        <!-- END MAIN CONTENT -->
     

@endsection


@section('begin_javascript')

    <script src="{{ asset("plugins/iziModal-master/js/iziModal.min.js")}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>


    <script src="{{ asset("plugins/modal/js/classie.js")}}"></script>
    <script src="{{ asset("plugins/modal/js/modalEffects.js")}}"></script>

    <script>
        jQuery(document).ready(function(){
            jQuery('#modal').iziModal('open');

            //////// DataTable ///////
                var table = $('#DataTable').DataTable({
                    "lengthMenu": [[10,25,50, -1], [10,25,50, "All"]],
                    "processing": true,
                    "serverSide": true,
                    "bAutoWidth": false,
                    "ajax":{
                        url  : "{{ route('ajaxdata.getdata.users') }}",
                        data : function (d){
                             d.name = $('input[name=first_name]').val();
                             d.email = $('input[name=email]').val();
                        }
                    },
                    "order": [[ 0, "desc" ]],
                    "bSearchable":true,
                    "bFilter": true,
                    "language": {
                        "lengthMenu": "แสดงผล _MENU_ ข้อมูลต่อหน้า",
                        "zeroRecords": "Nothing found - sorry",
                         "info": "ลำดับหน้าที่ _PAGE_ จาก _PAGES_ หน้า - จำนวนรายการทั้งหมด _TOTAL_ รายการ",
                        "infoEmpty": "No records available",
                        "infoFiltered": "( ค้นหาข้อมูลจาก _MAX_ รายการ )",
                        "search": "<i class='fa fa-search'></i> ค้นหาชื่อ - สกุล:&nbsp;",
                        "paginate": {
                            "first":      "หน้าแรก",
                            "last":       "หน้าสุดท้าย",
                            "next":       "ถัดไป",
                            "previous":   "ก่อนหน้า"
                        },
                    },

                    "columns":[
                            {data: 'id'},
                            {data: 'first_name'},
                            {data: 'last_name'},
                            {data: 'user_permisstion'},
                            {data: 'updated_at'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                    ],
                }); // End DataTable //
         
                $('#search-form').on('submit', function(e) {
                    table.draw();
                    e.preventDefault();
                });


        });
    </script>

    <!--START IMPORT EVENT FORM-->
    @include('includes.forms.popup_modal')
    <!--END IMPORT EVENT FORM-->
    
@endsection


