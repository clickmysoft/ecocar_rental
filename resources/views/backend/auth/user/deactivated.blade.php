@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.users.management') . ' | ' . __('labels.backend.access.users.deactivated'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('begin_css')
<link href="{{ asset("plugins/modal/css/component.css")}}" rel="stylesheet">
<link href="{{ asset("backend/css/style.css")}}" rel="stylesheet">
<link href="{{ asset("backend/css/colors/color-green.css")}}" rel="stylesheet" >
<link rel="stylesheet" href="{{ asset("plugins/datatables/dataTables.css")}}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset("plugins/datatables/dataTables.tableTools.css")}}" rel="stylesheet">
<link rel='stylesheet' type='text/css' href='https://datatables.yajrabox.com/css/datatables.bootstrap.css'/>
@endsection

@section('content')

<style type="text/css">
    table#DataTables_Table_0> tbody tr td {
        padding-left: 18px;
    }
    input[type=search]{
        margin-left: 10px;
    }
    
    @media (max-width: 706px) {

        div#DataTables_Table_0_length{
        padding-top: 20px;
    }
        div.dataTables_filter input{
            width: 100%;
        }
        input[type=search]{
        margin-left: 0;
        }
    }
    
</style>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    <!-- {{ __('labels.backend.access.users.management') }}
                    <small class="text-muted">{{ __('labels.backend.access.users.deactivated') }}</small> -->
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row">
                <div class="col-md-12">



            <div class="panel panel-default">
                <div class="panel-heading " style="height: 55px; color: #fff !important;background:#607d8b;">
                    <h3 class="panel-title" style="float:left;"><i class="fa fa-database"></i>&nbsp;{{ __('labels.backend.access.users.management') }} {{ __('labels.backend.access.users.deactivated') }}</h3>
                    </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red filter-right">
                                    <table class="table table-striped table-hover table-dynamic" id="DataTable">
                                        <thead>
                                        <tr>
                                            <th>{{ __('labels.backend.access.users.table.last_name') }}</th>
                                            <th>{{ __('labels.backend.access.users.table.first_name') }}</th>
                                            <th>{{ __('labels.backend.access.users.table.email') }}</th>
                                            <th>{{ __('labels.backend.access.users.table.confirmed') }}</th>
                                            <th>{{ __('labels.backend.access.users.table.roles') }}</th>
                                            <th>{{ __('labels.backend.access.users.table.other_permissions') }}</th>
                                            <th>{{ __('labels.backend.access.users.table.social') }}</th>
                                            <th>{{ __('labels.backend.access.users.table.last_updated') }}</th>
                                            <th>{{ __('labels.general.actions') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if ($users->count())
                                            @foreach ($users as $user)
                                                <tr>
                                                    <td>{{ $user->last_name }}</td>
                                                    <td>{{ $user->first_name }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>{!! $user->confirmed_label !!}</td>
                                                    <td>{!! $user->roles_label !!}</td>
                                                    <td>{!! $user->permissions_label !!}</td>
                                                    <td>{!! $user->social_buttons !!}</td>
                                                    <td>{{ $user->updated_at->diffForHumans() }}</td>
                                                    <td>{!! $user->action_buttons !!}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
            </div><!--col-->
        </div><!--row-->
        </div>
        </div>
        </div>
        <div class="row" style="display:none;">
            <div class="col-7">
                <div class="float-left" style="padding-left: 20px;">
                    {!! $users->total() !!} {{ trans_choice('labels.backend.access.users.table.total', $users->total()) }}
                </div>
            </div><!--col-->
            <div class="col-5">
                <div class="float-right">
                    {!! $users->render() !!}
                </div>
            </div><!--col-->           
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection


@section('begin_javascript')
    <script src="{{ asset("plugins/modal/js/classie.js")}}"></script>
    <script src="{{ asset("plugins/modal/js/modalEffects.js")}}"></script>
    <script src="{{ asset("plugins/bootstrap-switch/bootstrap-switch.js")}}"></script>
    <script src="{{ asset("plugins/bootstrap-progressbar/bootstrap-progressbar.js")}}"></script>
    <script src="{{ asset("plugins/datatables/dynamic/jquery.dataTables.min.js")}}"></script>
    <script src="{{ asset("plugins/datatables/dataTables.bootstrap.js")}}"></script>
    <script src="{{ asset("plugins/datatables/dataTables.tableTools.js")}}"></script>
    <script src="{{ asset("plugins/datatables/table.editable.js")}}"></script>
    <script src="{{ asset("plugins/modal/js/classie.js")}}"></script>
    <script src="{{ asset("plugins/modal/js/modalEffects.js")}}"></script>
    <script src="{{ asset("plugins/iziModal-master/js/iziModal.min.js")}}"></script>

    <script>
        jQuery(document).ready(function(){
            jQuery('#modal').iziModal('open');
            $('#DataTable').DataTable();
           
        });
    </script>
@endsection

