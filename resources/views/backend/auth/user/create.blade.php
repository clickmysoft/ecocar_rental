@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.users.management') . ' | ' . __('labels.backend.access.users.create'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')

<!-- BEGIN PAGE LEVEL STYLE -->
<link href="{{ asset("plugins/datetimepicker/jquery.datetimepicker.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.date.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.time.css")}}" rel="stylesheet">

<!-- END PAGE LEVEL STYLE -->




    <form method="POST" action="{{ route('admin.auth.user.store') }}" enctype="multipart/form-data" class="form-horizontal">   
    {{ csrf_field() }}
        <div class="row">
                <div class="col-md-12">


                      <div class="panel panel-default">
                          <div class="panel-heading " style="height: 55px; color: #fff !important;background:#00a788;">
                              <h3 class="panel-title">{{ __('labels.backend.access.users.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.users.create') }}</small></h3>
                          </div>

                          
                        <div class="panel-body">

                            <div class="row">

                                  <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="panel bd-t-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">
                                                    <strong>User</strong> Information</h3>
                                            </div>

                                            <div class="form-group">
                                              <label class="control-label col-sm-2" for="email">{{ __('validation.attributes.backend.access.users.companyname') }}:</label>
                                              <div class="col-sm-10">
                                                  @include('includes.partials.company')
                                              </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">
                                                    {{ __('validation.attributes.backend.access.users.position_name') }}:
                                                </label>
                                              <div class="col-sm-10">        
                                                    @include('includes.partials.position')  
                                              </div>
                                            </div>


                                            <div class="form-group" >
                                                <label class="control-label col-sm-2" for="pwd">
                                                   {{ __('validation.attributes.backend.access.users.belong') }}:
                                                </label>
                                              <div class="col-sm-10">          
                                                  
                                                    <select class="form-control chosen-img" name="belong" style="height:40px;">
                                                    <option value="" data-img-src="{{asset("backend/img/cars/brands/belong.png")}}">{{ __('validation.attributes.backend.company.belong_selectbox') }}</option>
                                                        <?php
                                                          foreach (config('company.'.app()->getLocale().'.belong') as $key => $value) {
                                                            if($value != null){
                                                                $img = asset("backend/img/cars/brands/belong.png");
                                                                echo "<option ";
                                                                echo (old('branch_name')==$value[0]?'selected':'');
                                                                echo " data-belogn-code='".$key."'   data-img-src='".$img."' value=".$key.">";
                                                                echo $value[0];
                                                                echo "</option>";
                                                            }
                                                          }
                                                        ?>
                                                    </select>
                                              </div>
                                            </div>


                                            <div class="form-group">
                                              <label class="control-label col-sm-2" > {{ __('validation.attributes.backend.access.users.employee_id') }}:</label>
                                              <div class="col-sm-10">          
                                                    {{ html()->text('employee_id')
                                                            ->class($errors->has('employee_id')==true?'form-control parsley-error':''." form-control")
                                                            ->attribute('maxlength', 191)
                                                            ->required()
                                                            ->autofocus() 
                                                    }}

                                                    @if ($errors->has('employee_id'))
                                                    <span class="help-block parsley-error">
                                                        <strong>{{ $errors->first('employee_id') }}</strong>
                                                    </span>
                                                    @endif
                                              </div>
                                            </div>


                                            <div class="form-group">
                                              <label class="control-label col-sm-2" for="pwd">{{ __('validation.attributes.backend.access.users.first_name') }}:</label>
                                              <div class="col-sm-10">          
                                                    {{ html()->text('first_name')
                                                            ->class('form-control')
                                                            ->attribute('maxlength', 191)
                                                            ->required()
                                                            
                                                    }}
                                              </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">
                                                    {{ __('validation.attributes.backend.access.users.last_name') }}:
                                                </label>
                                              <div class="col-sm-10">          
                                                  {{ html()->text('last_name')
                                                            ->class('form-control')
                                                            ->attribute('maxlength', 191)
                                                            ->required() }}
                                              </div>
                                            </div>



                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">
                                                {{ __('validation.attributes.backend.access.users.phone') }}:
                                                </label>
                                              <div class="col-sm-10">          
                                                  {{ html()->text('phone')
                                                            ->class('form-control')
                                                            ->attribute('maxlength', 191)
                                                            ->required() }}
                                              </div>
                                            </div>



                                            <div class="form-group">
                                              <label class="control-label col-sm-2" for="pwd">{{ __('validation.attributes.backend.access.users.email') }}:</label>
                                                <div class="col-sm-10">          
                                                        {{ html()->email('email')
                                                                ->class($errors->has('email')==true?'form-control parsley-error':''." form-control")
                                                                ->attribute('maxlength', 191)
                                                                ->required() }}

                                                        @if ($errors->has('email'))
                                                        <span class="help-block parsley-error">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">
                                                     {{ __('validation.attributes.backend.access.users.nickname') }}:
                                                </label>
                                              <div class="col-sm-10">          
                                                  {{ html()->text('nickname')
                                                            ->class('form-control')
                                                            ->attribute('maxlength', 191)
                                                            ->required() }}
                                              </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">
                                                     วันเร่ิมงาน :
                                                </label>
                                              <div class="col-sm-2">          
                                                  {{ html()->date('nickname')
                                                            ->class('form-control')
                                                            ->style('line-height: 20px;')
                                                            ->attribute('maxlength', 191)
                                                            ->required() }}
                                              </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">
                                                     {{ __('validation.attributes.backend.access.users.upload_img') }}:
                                                </label>
                                              <div class="col-sm-10"> 

                                                    {!! Form::file('user_image',
                                                        ['class'=> $errors->has('email')==true?'form-control parsley-error':''." form-control"])
                                                    !!}


                                                    @if ($errors->has('user_image'))
                                                    <span class="help-block parsley-error">
                                                        <strong>{{ $errors->first('user_image') }}</strong>
                                                    </span>
                                                    @endif

                                              </div>
                                            </div>

                                          </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="panel bd-t-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">
                                                    <strong>Account</strong> config</h3>
                                            </div>


                                            <div class="form-group">
                                              
                                                <label class="control-label col-sm-2" for="pwd">
                                                    {{ __('validation.attributes.backend.access.users.username') }}:
                                                </label>
                                              <div class="col-sm-10"> 

                                                    <div class="input-group transparent">
                                                        <span class="input-group-addon bg-blue">
                                                          <i class="fa fa-user"></i>
                                                        </span>
                                                         {{ html()->text('username')
                                                            ->placeholder("Enter Username")
                                                            ->class($errors->has('username')==true?'form-control parsley-error':''." form-control")
                                                            ->required() 
                                                            }}
                                                    </div>

                                                    @if ($errors->has('username'))
                                                    <span class="help-block parsley-error">
                                                        <strong>{{ $errors->first('username') }}</strong>
                                                    </span>
                                                    @endif
                                              </div>
                                            </div>


                                            <div class="form-group">
                                              <label class="control-label col-sm-2" for="pwd">{{ __('validation.attributes.backend.access.users.password') }}:</label>
                                              <div class="col-sm-10">

                                                    <div class="input-group transparent">
                                                        <span class="input-group-addon bg-red">
                                                          <i class="fa fa-lock"></i>
                                                        </span>
                                                            {{ html()->password('password')
                                                            ->placeholder("Enter Password")
                                                            ->class('form-control')
                                                            ->required() }}
                                                    </div>

                                                    @if ($errors->has('password'))
                                                    <span class="help-block parsley-error">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                    @endif

                                              </div>
                                            </div>

                                            <div class="form-group">
                                              <label class="control-label col-sm-2" for="pwd">{{ __('validation.attributes.backend.access.users.password_confirmation') }}:</label>
                                              <div class="col-sm-10">
                                                    <div class="input-group transparent">
                                                        <span class="input-group-addon bg-red">
                                                          <i class="fa fa-lock"></i>
                                                        </span>
                                                            {{ html()->password('password_confirmation')
                                                            ->placeholder("Enter Password Confirmation")
                                                            ->class('form-control')
                                                            ->required() }}
                                                    </div>

                                                    @if ($errors->has('password_confirmation'))
                                                    <span class="help-block parsley-error">
                                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                    </span>
                                                    @endif
                                              </div>
                                            </div>


                                            <div class="form-group" style="display:none;">
                                              <label class="control-label col-sm-2" for="pwd">{{ __('validation.attributes.backend.access.users.timezone') }}:</label>
                                              <div class="col-sm-10">          
                                                      <select name="timezone" id="timezone" class="form-control" required="required" style="height:40px;">
                                                        @foreach (timezone_identifiers_list() as $timezone)
                                                            <option value="{{ $timezone }}" {{ $timezone == 'Asia/Bangkok' ? 'selected' : '' }} {{ $timezone == old('timezone') ? ' selected' : '' }}>
                                                                {{ $timezone }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group" style="display:none;">
                                    <div class="col-sm-12">
                                        <div class="panel bd-t-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">
                                                    <strong>Confirm</strong> Account</h3>
                                            </div>

                                            <div class="form-group">
                                              <label class="control-label col-sm-2" for="pwd">
                                                {{ __('validation.attributes.backend.access.users.active') }}: 
                                              </label>
                                              <div class="col-sm-10" style="padding: 10px 5px;">          
                                                   {{ html()->checkbox('active', true, '1') }}
                                              </div>
                                            </div>

                                            <div class="form-group">
                                              <label class="control-label col-sm-2" for="pwd">
                                                {{ __('validation.attributes.backend.access.users.confirmed') }}: 
                                              </label>
                                              <div class="col-sm-10" style="padding: 10px 5px;">          
                                                    {{ html()->checkbox('confirmed', true, '1')->class('switch-input') }}
                                              </div>
                                            </div>

                                            @if (! config('access.users.requires_approval'))
                                            <div class="form-group">
                                              <label class="control-label col-sm-2" for="pwd">
                                                {{ __('validation.attributes.backend.access.users.send_confirmation_email') }}:
                                                <br>
                                                <small>{{ __('strings.backend.access.users.if_confirmed_off') }}</small> 
                                              </label>

                                              <div class="col-sm-10" style="padding: 18px 5px;">          
                                                    {{ html()->checkbox('confirmation_email', true, '1')->class('switch-input') }}
                                              </div>
                                            </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>


                                  <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="panel bd-t-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">
                                                    <strong>Abilities</strong></h3>
                                            </div>

                                             <div class="col-md-12" >
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>{{ __('labels.backend.access.users.table.roles') }}</th>
                                                                <th style="display:none;">{{ __('labels.backend.access.users.table.permissions') }}</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>
                                                                    @if ($roles->count())
                                                                        @foreach($roles as $role)
                                                                            <div class="card">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-sm-2" for="pwd">
                                                                                        {{ html()->label(ucwords($role->name))->for('role-'.$role->id) }}: 
                                                                                    </label>
                                                                                    <div class="col-sm-3" >          
                                                                                            {{ html()->label(
                                                                                                html()->checkbox('roles[]', old('roles') && in_array($role->name, old('roles')) ? true : false, $role->name)
                                                                                                      ->id('role-'.$role->id)
                                                                                                . '<span class="switch-label"></span><span class="switch-handle"></span>')
                                                                                            ->for('role-'.$role->id) }}
                                                                                            <div  class="col-sm-8">
                                                                                            @if ($role->id != 1)
                                                                                                @if ($role->permissions->count())
                                                                                                    @foreach ($role->permissions as $permission)
                                                                                                        <i class="fa fa-dot-circle-o"></i> {{ ucwords($permission->name) }}
                                                                                                    @endforeach
                                                                                                @else
                                                                                                    {{ __('labels.general.none') }}
                                                                                                @endif
                                                                                            @else
                                                                                                {{ __('labels.backend.access.users.all_permissions') }}
                                                                                            @endif
                                                                                            </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div><!--card-->
                                                                        @endforeach
                                                                    @endif
                                                                </td>
                                                                <td style="display:none;">
                                                                    @if ($permissions->count())
                                                                        @foreach($permissions as $permission)
                                                                                <div class="form-group">
                                                                                      <label class="control-label col-sm-2" for="pwd">
                                                                                        {{ __('validation.attributes.backend.access.users.active') }}: 
                                                                                      </label>
                                                                                      <div class="col-sm-10" style="padding: 10px 5px;">          
                                                                                           {{ html()->checkbox('active', true, '1') }}
                                                                                      </div>
                                                                                    </div>
                                                                                {{ html()->label(
                                                                                        html()->checkbox('permissions[]', old('permissions') && in_array($permission->name, old('permissions')) ? true : false, $permission->name)
                                                                                              ->id('permission-'.$permission->id)
                                                                                        . '<span class="switch-label"></span><span class="switch-handle"></span>')
                                                                                    ->for('permission-'.$permission->id) }}
                                                                                {{ html()->label(ucwords($permission->name))->for('permission-'.$permission->id) }}
                                                                        @endforeach
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div><!--col-->
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12"> 
                                        <ul class="list-inline" style="float:right;">
                                            <li><button type="submit" class="btn btn-success "><i class="fa fa-save"></i>&nbsp;{{__('buttons.general.crud.create')}}</button></li>
                                            <li><a class="btn btn-warning prev-step" href="{{ route('admin.auth.user.index') }}"><i class="fa fa-times"></i>&nbsp;{{__('buttons.general.cancel')}}</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
        </div>

    {{ html()->form()->close() }}
@endsection