@extends('backend.layouts.app')

@section('begin_css')
<link href="{{ asset("plugins/modal/css/component.css")}}" rel="stylesheet">
    <link href="{{ asset("backend/css/style.css")}}" rel="stylesheet">
    <link href="{{ asset("backend/css/colors/color-green.css")}}" rel="stylesheet" >
    <link rel="stylesheet" href="{{ asset("plugins/datatables/dataTables.css")}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset("plugins/datatables/dataTables.tableTools.css")}}" rel="stylesheet">
@endsection

@section('content')
  <div class="panel panel-default">
                        <div class="panel-heading" style="color: #fff !important;color: #fff !important;background: #60868b;padding: 10px 15px;">
                            <h3 class="panel-title" style="float:left;padding: 10px 0 !important;
    margin: 0 !important;"><i class="fa fa-database"></i>{{ __('labels.backend.access.users.management') }} <small class="text-muted">{{ __('labels.backend.access.users.deleted') }}</small></h3>
     <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                                   
                            </div>
                            
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red filter-right">
                                    <table class="table table-striped table-hover table-dynamic" >
                                            <thead>
                                                <tr>
                                                    <th class="col-md-1" text-align: center;">{{ __('labels.backend.access.users.table.last_name') }}</</th>
                                                    <th class="col-md-3"  style="padding-left: 0;">{{ __('labels.backend.access.users.table.first_name') }}</th>
                                                    <th class="col-md-2" style="padding-left: 0; text-align: center;">{{ __('labels.backend.access.users.table.email') }}</th>
                                                    <th class="col-md-2" style="padding-left: 0; text-align: center;">{{ __('labels.backend.access.users.table.confirmed') }}</th>
                                                    <th class="col-md-1" style="padding-left: 0;text-align:center;" >{{ __('labels.backend.access.users.table.roles') }}</th>
                                                    <th class="col-md-1" style="padding-left: 0;text-align:center;">{{ __('labels.backend.access.users.table.other_permissions') }}</th>
                                                 
                                                    <th class="col-md-1">{{ __('labels.backend.access.users.table.last_updated') }}</th>
                                                    <th class="col-md-1">{{ __('labels.general.actions') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody> 
                                                    @if ($users->count())
                                                        @foreach ($users as $user)
                                                            <tr>
                                                                <td>{{ $user->last_name }}</td>
                                                                <td>{{ $user->first_name }}</td>
                                                                <td>{{ $user->email }}</td>
                                                                <td>{!! $user->confirmed_label !!}</td>
                                                                <td>{!! $user->roles_label !!}</td>
                                                                <td>{!! $user->permissions_label !!}</td>
                                                                <td>{{ $user->updated_at->diffForHumans() }}</td>
                                                                <td>{!! $user->action_buttons !!}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
       
          <div class="row" style="display:none;">
            <div class="col-7">
                <div class="float-left" style="padding-left: 15px;">
                    {!! $users->total() !!} {{ trans_choice('labels.backend.access.users.table.total', $users->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $users->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
@endsection



@section('begin_javascript')
    <script src="{{ asset("plugins/modal/js/classie.js")}}"></script>
    <script src="{{ asset("plugins/modal/js/modalEffects.js")}}"></script>
    <script src="{{ asset("plugins/bootstrap-switch/bootstrap-switch.js")}}"></script>
    <script src="{{ asset("plugins/bootstrap-progressbar/bootstrap-progressbar.js")}}"></script>
    <script src="{{ asset("plugins/datatables/dynamic/jquery.dataTables.min.js")}}"></script>
    <script src="{{ asset("plugins/datatables/dataTables.bootstrap.js")}}"></script>
    <script src="{{ asset("plugins/datatables/dataTables.tableTools.js")}}"></script>
    <script src="{{ asset("plugins/datatables/table.editable.js")}}"></script>
    <script src="{{ asset("plugins/modal/js/classie.js")}}"></script>
    <script src="{{ asset("plugins/modal/js/modalEffects.js")}}"></script>
    <script src="{{ asset("plugins/iziModal-master/js/iziModal.min.js")}}"></script>

    <script>
        jQuery(document).ready(function(){
            jQuery('#modal').iziModal('open');

            /****  Tables Dynamic  ****/
                if ($('.table-dynamic').length && $.fn.dataTable) {
                    $('.table-dynamic').each(function () {
                        var opt = {};
                        // Tools: export to Excel, CSV, PDF & Print
                        if ($(this).hasClass('table-tools')) {
                            opt.sDom = "<'row'<'col-md-6'f><'col-md-6'T>r>t<'row'<'col-md-6'i><'spcol-md-6an6'p>>",
                            opt.oTableTools = {
                                "sSwfPath": "assets/plugins/datatables/swf/copy_csv_xls_pdf.swf",
                                "aButtons": ["csv", "xls", "pdf", "print"]
                            };
                        }
                        if ($(this).hasClass('no-header')) {
                            opt.bFilter = false;
                            opt.bLengthChange = false;
                        }
                        if ($(this).hasClass('no-footer')) {
                            opt.bInfo = false;
                            opt.bPaginate = false;
                        }
                        var oTable = $(this).dataTable(opt);
                        oTable.fnDraw();
                    });
                }
        });
    </script>
@endsection