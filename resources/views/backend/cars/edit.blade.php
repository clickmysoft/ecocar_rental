@extends ('backend.layouts.app')

@section ('title', __('สร้างรายละเอียด ข้อมูลรถยนต์'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('begin_css')
		<link href="{{ asset("plugins/datetimepicker/jquery.datetimepicker.css")}}" rel="stylesheet">
		<link href="{{ asset("plugins/pickadate/themes/default.css")}}" rel="stylesheet">
		<link href="{{ asset("plugins/pickadate/themes/default.date.css")}}" rel="stylesheet">
		<link href="{{ asset("plugins/pickadate/themes/default.time.css")}}" rel="stylesheet">
		<link href="{{ asset("backend/dependentdrop/dependentdrop.css")}}" rel="stylesheet">
		<link rel="stylesheet" href="{{ asset("plugins/chosen/chosen.css")}}" />
		<link rel="stylesheet" href="{{ asset("plugins/chosen/chosenImage/chosenImage.css")}}"/>
		<link rel="stylesheet" href="{{ asset("plugins/wizard/wizard.css")}}" />
		<link rel="stylesheet" href="{{ asset("plugins/jquery-steps/jquery.steps.css")}}" />

		<link href="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />

		<style type="text/css">
		    .chosen-single{
		        line-height: 32px !important;
		        height: 32px !important;
		    }

		    div.payan-chana ul.chosen-choices{
		            height: 33px !important;
		    }
		    div#select_number ul.chosen-results li{
		         background-size: 0px !important;
		    }

		    div#brand_cars ul.chosen-results li:first-child{
		        background-size: 0px !important;
		        background-position-x: 12px;
		    }
		    div#brand_cars ul.chosen-results li{
		        background-size: 27px !important;
		        text-indent: 7px;
		        padding-top: 6px;
		        -webkit-backface-visibility: hidden;
		        -webkit-transform: translateZ(0) scale(1.0, 1.0);
		    }
		   
		    div#car_type ul.chosen-results li{
		        background-size: 40px !important;
		        text-indent: 26px;
		    }
		    div#car_type ul.chosen-results li:first-child{
		        background-size: 0px !important;
		        background-position-x: 12px;
		    }

		    div#total_human ul.chosen-results li:first-child{
		        background-size: 0px !important;
		    }

		    div#total_bag ul.chosen-results li:first-child{
		        background-size: 0px !important;
		    }
		    div.fule_type > div.ui-checkbox{
		            margin-top: 0;
		    }
		    div.fule_type > div.ui-checkbox label{
		           font-size: 14px !important;
		    }


		.registration-ui {
		 /*   background: linear-gradient(to bottom, #f8d038 0%,#f5ca2e 100%);*/
		   /* padding: .25em 1em .25em 1.75em;*/
		   /*padding:0 .7em 1.14em .75em;
		    font-weight: bold;
		    font-size: 2em;
		    border-radius: 5px;
		    border: 1px solid #000;
		    box-shadow: 1px 1px 1px #ddd;
		    position: relative;
		    font-family: helvetica, ariel, sans-serif;*/
		    padding: 12.4px .7em 0.7em .75em;
		    font-weight: bold;
		    font-size: 2em;
		    border-radius: 5px;
		    border: 1px solid #000;
		    box-shadow: 1px 1px 1px #ddd;
		    position: relative;
		    font-family: helvetica, ariel, sans-serif;
		}

		.registration-ui:before {
		    /*content: 'ECOCAR';
		    display: block;
		    width: 30px;
		    height: 100%;
		    background: #063298;
		    position: absolute;
		    top: 0;
		    border-radius: 5px 0 0 5px;
		    color: #f8d038;
		    font-size: .5em;
		    line-height: 85px;
		    padding-left: 5px;*/
		    content: 'ECOCAR';
		    display: block;
		    text-align: center;
		    width: 100%;
		    height: 18px;
		    background: #159077;
		    position: absolute;
		    top: 46.1px;
		    border-radius: 5px 0 0 5px;
		    color: #ffffff;
		    font-size: .4em;
		    line-height: 19px;
		    left: 0px;
		    border-bottom-right-radius: 4px;
		    border-top-left-radius: 0;
		    border-bottom-left-radius: 4px;
		}

		.registration-ui:after {
		    content: '';
		    display: block;
		    position: absolute;
		    top: 7px;
		    left: 5px;
		    width: 20px;
		    height: 20px;
		    border-radius: 30px;
		   /* border: 1px dashed #f8d038;*/
		}


</style>
@endsection
@section('content')

 <?php
  	// echo "<pre>";
   //  print_r($data);
   //  echo "</pre>";
  ?>

<div class="panel panel-default">
    <div class="panel-heading " style="height: 55px; color: #fff !important;background:#00a788;">
        <h3 class="panel-title" style="float:left;"><i class="fa fa-database"></i>&nbsp;แก้ไขข้อมูลรถยนต์</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <form method="POST" action="{{ route('admin.cars.car.update',$data['id']) }}" enctype="multipart/form-data" class="form-horizontal" id="my-form">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="panel bd-t-red">
                            
                            
                             @if ($errors->any())
                                <div class="alert alert-danger" style="width:100%;">
                                  <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                  </ul>
                                </div>
                              @endif
                            
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="fa fa-gear"></i>&nbsp;<strong>แก้ไขรายละเอียด&nbsp;</strong>ข้อมูลรถยนต์</h3>
                            </div>
                            <div class="form-group ">
                                    <label class="control-label col-sm-2" for="email">ยี่ห้อ:</label>
                                    <div class="col-sm-10"  id="brand_cars">
                                       <select class="form-control " name="car_brand"  tabindex="1" style="height:40px;text-transform: uppercase;" >
											<option value="" data-img-src="{{ asset("backend/img/various/clipboard-list.svg")}}">เลือกยี่ห้อรถ</option>
												<?php
											  		foreach (config('cars.'.app()->getLocale().'.brands') as $key => $value) {
											  			if($value != null){
											  				$img = asset("backend/img/cars/brands/".config('cars.brands_img.'.$key)[0]."");
											  				  echo "<option ";
											            echo ($data['car_brand']==$key?'selected':'');
											            echo " data-img-src='".$img."' value=".$key.">";
													        echo strtoupper($key);
													        echo "</option>";
											  			}
											  		}
											  	?>
										</select>

                                        @if($errors->has('car_brand'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('car_brand') }} </strong>
                                            </span>
                                        @endif
                                    </div>
                            </div>
                            <div class="form-group">
                                    <label class="control-label col-sm-2" for="email">รุ่น:</label>
                                    <div class="col-sm-10">
                                        <select id="set_generation" name="car_generation"  tabindex="2" class="form-control " style="height:40px;" >
                                          <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg")}}">เลือกรุ่น</option>
                                        </select>
                                    </div>
                            </div>
                            <div class="form-group">
                                    <label class="control-label col-sm-2" for="email">โฉมรถยนต์:</label>
                                    <div class="col-sm-10">
                                        <select id="nickname" name="car_nickname"   tabindex="3" class="form-control " style="height:40px;" >
                                          <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg")}}">เลือกโฉมรถยนต์</option>
                                        </select>
                                    </div>
                            </div>
                            <div class="form-group">
                                    <label class="control-label col-sm-2" for="email">รายละเอียดรุ่น:</label>
                                    <div class="col-sm-10">
                                        <select id="variant" name="car_generation_detail"  tabindex="4" class="form-control " style="height:40px;" >
                                          <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg")}}">เลือกรายละเอียดรุ่น</option>
                                        </select>
                                    </div>
                            </div>
                            <div class="form-group">
                                    <label class="control-label col-sm-2" for="email">เครื่องยนต์ CC:</label>
                                    <div class="col-sm-10">
                                        <select name="car_engine_cc"  tabindex="5" class="form-control chosen-select" style="height:40px;" >
                                            <option data-img-src="{{ asset("backend/img/cars/gear/engine.png")}}">เลือกเครื่องยนต์ CC</option>
                                            <?php
                                                for ($i=1200; $i <= 5000 ; $i+=100) {
                                                    $img = asset("backend/img/cars/gear/engine.png");
                                                    
                                                    echo "<option ";
                                                    echo ($data['car_engine_cc']==$i?'selected':'');
                                                    echo " data-img-src='".$img."' value=".$i.">";
                                                    echo ''.$i.' CC';
                                                    echo "</option>";
                                                }
                                            ?> 
                                        </select>
                                    </div>
                            </div>
                            <div class="form-group" >
                                    <label class="control-label col-sm-2"  for="email">ประเภทรถยนต์:</label>
                                    <div class="col-sm-10" id="car_type" >
                                        <select class="form-control chosen-img" name="car_type" data-live-search="true" style="height:40px;">
											<option data-img-src="{{ asset("backend/img/cars/type/car.png")}}" value="">เลือกประเภทรถยนต์</option>
												<?php
											  		foreach (config('cars.'.app()->getLocale().'.type') as $key => $value) {
											  			$img = asset("backend/img/cars/type/".config('cars.type_img.'.$key)[0]."");
											  			if($value != null){
											  				  echo "<option ";
											            echo ($data['car_type']==$key?'selected':'');
											            echo " data-img-src='".$img."' value=".$key.">";
													        echo $value[0];
													        echo "</option>";
											  			}
											  		}
											  	?>
										</select>
                                    </div>
                            </div>               
                            <div class="form-group">
                                    <label class="control-label col-sm-2" for="email">จำนวนผู้โดยสาร:</label>
                                    <div class="col-sm-10" id="car_human_qty">
                                        <select class="form-control chosen-img" name="car_human_qty" data-live-search="true" style="height:40px;">
											<option data-img-src="{{ asset("backend/img/cars/type/people.png")}}" value="">เลือกจำนวนผู้โดยสาร</option>
												<?php
											  		foreach (config('cars.'.app()->getLocale().'.passengers_quantity') as $key => $value) {
											  			if($value != null){
											  				$img = asset("backend/img/cars/type/people.png");
											  				  echo "<option ";
											            echo ($data['car_human_qty']==$key?'selected':'');
											            echo " data-img-src='".$img."' value=".$key.">";
													        echo $value[0];
													        echo "</option>";
											  			}
											  		}
											  	?>
										</select>
                                    </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="email">จำนวนสัมภาระ:</label>
                                <div class="col-sm-10" id="car_baggage_qty">
                                    <select class="form-control chosen-img" name="car_baggage_qty" data-live-search="true" style="height:40px;">
										<option data-img-src="{{ asset("backend/img/cars/type/bag.png")}}" value="">เลือกจำนวนสัมภาระ</option>
											<?php
										  		foreach (config('cars.'.app()->getLocale().'.baggage_quantity') as $key => $value) {
										  			if($value != null){
										  				  $img = asset("backend/img/cars/type/bag.png");
										  				  echo "<option ";
											            echo ($data['car_baggage_qty']==$key?'selected':'');
											            echo " data-img-src='".$img."' value=".$key.">";
												        echo $value[0];
												        echo "</option>";

										  			}
										  		}
										  	?>
									</select>
                                </div>
                            </div>
                            <?php
                                $fule = json_decode($data['car_type_fule'],true);
                            ?>
                            <div class="form-group">
                                <label class="control-label col-md-2" for="email" style="padding-top: 23px;">ประเภทของน้ำมันที่ใช้ในเครื่องยนต์:</label>
                                <div class="col-md-5 fule_type" style="padding-left: 6px;">
                                    <label >
                                        <input <?=(@in_array("gasohol_95",$fule)==true?'checked':'')?> type="checkbox" name="car_type_fule[]" data-parsley-multiple="my_input_group" value="gasohol_95" >
                                        <img src="{{ asset("backend/img/cars/fule/so95.png")}}" style="max-width:110px;">&nbsp;|Gasohol 95
                                    </label>
                                    <label>
                                        <input <?=(@in_array("gasohol_91",$fule)==true?'checked':'')?> type="checkbox" name="car_type_fule[]" data-parsley-multiple="my_input_group" value="gasohol_91" >
                                        <img src="{{ asset("backend/img/cars/fule/so91.png")}}" style="max-width:110px;">&nbsp;|Gasohol 91
                                    </label>
                                    <label>
                                        <input <?=(@in_array("e20",$fule)==true?'checked':'')?> type="checkbox" name="car_type_fule[]" data-parsley-multiple="my_input_group" value="e20">
                                        <img src="{{ asset("backend/img/cars/fule/e20.png")}}" style="max-width:110px;">&nbsp;|Gasohol E20
                                    </label>
                                    <label>
                                        <input <?=(@in_array("e85",$fule)==true?'checked':'')?> type="checkbox" name="car_type_fule[]" data-parsley-multiple="my_input_group" value="e85">
                                        <img src="{{ asset("backend/img/cars/fule/e85.png")}}" style="max-width:110px;">&nbsp;|Gasohol E85
                                    </label>
                                </div>
                                <div class="col-md-5 fule_type" style="padding-left: 6px;">
                                    <label>
                                        <input <?=(@in_array("diesel",$fule)==true?'checked':'')?> type="checkbox" name="car_type_fule[]" data-parsley-multiple="my_input_group" value="diesel">
                                        <img src="{{ asset("backend/img/cars/fule/diesel.png")}}" style="max-width:110px;">&nbsp;|Diesel
                                    </label>
                                    <label>
                                        <input <?=(@in_array("gasoline_95",$fule)==true?'checked':'')?> type="checkbox" name="car_type_fule[]" data-parsley-multiple="my_input_group" value="gasoline_95" >
                                        <img src="{{ asset("backend/img/cars/fule/gasoline_95.png")}}" style="max-width:110px;">&nbsp;|Gasoline 95
                                    </label>
                                    <label>
                                        <input <?=(@in_array("lpg",$fule)==true?'checked':'')?> type="checkbox" name="car_type_fule[]" data-parsley-multiple="my_input_group" value="lpg">
                                        <img src="{{ asset("backend/img/cars/fule/lpg.png")}}" style="max-width:110px;">&nbsp;|LPG
                                    </label>
                                    <label>
                                        <input <?=(@in_array("ngv",$fule)==true?'checked':'')?> type="checkbox" name="car_type_fule[]" data-parsley-multiple="my_input_group" value="ngv">
                                        <img src="{{ asset("backend/img/cars/fule/ngv.png")}}" style="max-width:110px;">&nbsp;|NGV
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2" for="email" style="    padding-top: 30px;">ประเภทระบบเกียร์:</label>
                                <div class="col-md-3 fule_type" style="padding-left: 6px;">
                                    <label style="font-size:14px;">
                                        <input type="radio" <?=$data['car_type_gear']=='automatic_gear'?'checked':''?> name="car_type_gear" data-parsley-multiple="my_input_group" value="automatic_gear" >
                                        <img src="{{ asset("backend/img/cars/gear/automatic.png")}}" style="max-width:46px;">&nbsp;Automatic Gear
                                    </label>
                                </div>
                                <div class="col-md-3 fule_type" style="padding-left: 6px;">
                                    <label style="font-size:14px;">
                                        <input type="radio" <?=$data['car_type_gear']=='manual_gear'?'checked':''?> name="car_type_gear" data-parsley-multiple="my_input_group" value="manual_gear">
                                        <img src="{{ asset("backend/img/cars/gear/manul.png")}}" style="max-width:46px;">&nbsp;Manual Gear
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                    <label class="control-label col-sm-2" for="email">ความดันลมยางมาตรฐาน:</label>
                                    <div class="col-sm-3" id="total_human">
                                         <select class="form-control chosen-img" name="car_pressure_front" style="height:40px;">
                                            <option data-img-src="{{ asset("backend/img/cars/gear/tire.png")}}" value="">ความดันลมยาง ล้อหน้า</option>
                                            <?php
                                                for ($i=25; $i < 50 ; $i++) {
                                                    $img = asset("backend/img/cars/gear/tire.png");
                                                    echo "<option ";
                                                    echo ($data['car_pressure_front']==$i?'selected':'');
                                                    echo " data-img-src='".$img."' value=".$i.">";
                                                    echo 'ล้อหน้าความดันลม '.$i.' ปอนด์/ตารางนิ้ว';
                                                    echo "</option>";
                                                }
                                            ?> 
                                          </select>
                                    </div>
                                     <div class="col-sm-3" id="total_human">
                                        <select class="form-control chosen-img" name="car_pressure_back" style="height:40px;">
                                            <option data-img-src="{{ asset("backend/img/cars/gear/tire.png")}}" value="">ความดันลมยาง ล้อหลัง</option>
                                            <?php
                                                for ($i=25; $i < 50 ; $i++) {
                                                    $img = asset("backend/img/cars/gear/tire.png");
                                                    echo "<option ";
                                                    echo ($data['car_pressure_back']==$i?'selected':'');
                                                    echo " data-img-src='".$img."' value=".$i.">";
                                                    echo 'ล้อหลังความดันลม '.$i.' ปอนด์/ตารางนิ้ว';
                                                    echo "</option>";
                                                }
                                            ?>
                                          </select>
                                    </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label col-sm-2" for="email">เลขไมล์รถ:</label>
                                <div class="col-sm-6">
                                    <div class="input-group ">
                                    <span class="input-group-addon">
                                      <i class="fa fa-dashboard"></i>
                                    </span>
                                    <input  class="form-control number" type="text" name="car_mile" placeholder="Enter your name" value="<?=$data['car_mile']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="email">จำนวนประตู:</label>
                                <div class="col-sm-6">
                                    <div class="input-group ">
                                    <span class="input-group-addon">
                                      <i class="fa fa-taxi"></i>
                                    </span>
                                    <input  class="form-control number"  name="car_door_qty" placeholder="Enter your name" maxlength="1" value="<?=$data['car_door_qty']?>">
                                </div>
                                </div>
                            </div>

                            <div class="form-group ">
                              <label class="col-md-2 control-label" for="password">รูปภาพตัวอย่าง</label>
                              <div class="col-md-10"> 

                                        <div class="avatar-upload">
                                            <div class="avatar-preview">
	                                            <?php 
	                                            	if ($data['car_image'] != null) {
	                                            ?>
	                                            		<img id="imagePreview" src="{{ asset('cars/'.$data['car_image']) }}" style="width: 20%;">
	                                            <?php
	                                            	} else {
	                                            ?>
	                                            		<img id="imagePreview" src="{{ asset('https://via.placeholder.com/250x150/'.$data['car_image']) }}" style="width: 20%;">
	                                            <?php
	                                            	}	
	                                            ?>
                                            </div>
                                        </div>

                              </div>
                          </div>
                         
                          <div class="form-group " id="image_banner" >
                              <label class="col-md-2 control-label" for="password">อัพโหลดรูปภาพ</label>
                              <div class="col-md-10"> 
                                     <div class="avatar-edit">
                                      <input type="file" name="car_image" class="form-control" id="imageUpload" accept=".png, .jpg, .jpeg">
                                   </div>
                              </div><!--col-->
                          </div><!--form-group-->

                        </div>
                         <div class=" bd-t-red">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="fa fa-gears"></i> <strong>กำหนดรายการ</strong>อุปกรณ์เสริมของรถยนต์</h3>
                            </div>
                            <?php
                                $car_option = json_decode($data['car_option'],true);
                            ?>
                            <div class="form-group">
                                <label class="control-label col-md-2" for="email" style="padding-top: 23px;">รายการอุปกรณ์เสริมของรถยนต์:</label>
                                <div class="col-md-3 fule_type" style="padding-left: 6px;">
                                    <label >
                                        <input <?=(@in_array("satellite_navigation",$car_option)==true?'checked':'')?> type="checkbox" name="car_option[]" data-parsley-multiple="my_input_group" value="satellite_navigation" >
                                        <img src="{{ asset("backend/img/cars/extra_option/satellite.png")}}" style="max-width:45px;">&nbsp;|&nbsp;Satellite Navigation
                                    </label>
                                    <label>
                                        <input <?=(@in_array("power_door",$car_option)==true?'checked':'')?> type="checkbox" name="car_option[]" data-parsley-multiple="my_input_group" value="power_door" >
                                        <img src="{{ asset("backend/img/cars/extra_option/door.png")}}" style="max-width:45px;">&nbsp;|&nbsp;Power Door
                                    </label>
                                    <label>
                                        <input <?=(@in_array("tilt_steering_wheel",$car_option)==true?'checked':'')?> type="checkbox" name="car_option[]" data-parsley-multiple="my_input_group" value="tilt_steering_wheel" >
                                        <img src="{{ asset("backend/img/cars/extra_option/control.png")}}" style="max-width:45px;">&nbsp;|&nbsp;Tilt Steering Wheel
                                    </label>
                                    <label>
                                        <input <?=(@in_array("fm_radio_and_stereo_cd",$car_option)==true?'checked':'')?> type="checkbox" name="car_option[]" data-parsley-multiple="my_input_group" value="fm_radio_and_stereo_cd">
                                        <img src="{{ asset("backend/img/cars/extra_option/radio.png")}}" style="max-width:45px;">&nbsp;|&nbsp;FM Radio,Stereo CD
                                    </label>
                                </div>
                                <div class="col-md-3 fule_type" style="padding-left: 6px;">
                                    <label>
                                        <input <?=(@in_array("touch_screen_media_player",$car_option)==true?'checked':'')?> type="checkbox" name="car_option[]" data-parsley-multiple="my_input_group" value="touch_screen_media_player">
                                        <img src="{{ asset("backend/img/cars/extra_option/tablet.png")}}" style="max-width:50px;">&nbsp;|&nbsp;Touch screen media player
                                    </label>
                                    <label>
                                        <input <?=(@in_array("camera",$car_option)==true?'checked':'')?> type="checkbox" name="car_option[]" data-parsley-multiple="my_input_group" value="camera">
                                        <img src="{{ asset("backend/img/cars/extra_option/security-camera.png")}}" style="max-width:50px;">&nbsp;|&nbsp;Camera
                                    </label>
                                     <label>
                                        <input <?=(@in_array("sensor",$car_option)==true?'checked':'')?>  type="checkbox" name="car_option[]" data-parsley-multiple="my_input_group" value="sensor">
                                        <img src="{{ asset("backend/img/cars/extra_option/senser.png")}}" style="max-width:50px;">&nbsp;|&nbsp;Sensor
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class=" bd-t-red">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="fa fa-gears"></i> <strong>กำหนดประกันภัย</strong>ของรถยนต์</h3>
                            </div>
                             <div class="form-group">
                                <label class="control-label col-sm-2" for="email">บริษัทประกันภัย:</label>
                               <div class="col-sm-10">
                                        <select class="form-control chosen-img" name="car_company_insurance" data-live-search="true" style="height:40px;">

											<option data-img-src="{{ asset("backend/img/cars/type/bag.png")}}" value="">เลือกบริษัทประกันภัย</option>
											  <?php
											      foreach (config('insurance.'.app()->getLocale().'.name') as $key => $value) {
											        if($value != null){
											          $img = asset("backend/img/cars/type/bag.png");
											            echo "<option ";
											            echo ($data['car_company_insurance']==$key?'selected':'');
											            echo " data-img-src='".$img."' value=".$key.">";
											            echo $value[0];
											            echo "</option>";
											        }
											      }
											    ?>
											</select> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd">ประเภทประกันภัย:</label>
                                <div class="col-sm-10">
                                  <select name="car_type_insurance" id="car_type_insurance" class="form-control " style="height:40px;" >
                                          <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg")}}">เลือกประเภทประกันภัย</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd">
                                  วันที่จดทะเบียน:
                                </label>
                                <div class="col-sm-10" >
                                  <div class="input-group ">
                                        <span class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                        </span>
                                        <input id="datepicker" data-format="dd-mm-yyyy" class=" form-control"  name="car_date_register" type="text" value="<?=$data['car_date_register']?>">
                                    </div>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd">
                                  วันหมดอายุประกันภัย:
                                </label>
                                <div class="col-sm-10">
                                  <div class="input-group ">
                                        <span class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                        </span>
                                        <input id="datepicker2" data-format="dd-mm-yyyy" class=" form-control" name="car_enddate_insurance" type="text" value="<?=$data['car_enddate_insurance']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd">
                                  วันหมดอายุ พรบ:
                                </label>
                                <div class="col-sm-10">
                                  <div class="input-group ">
                                        <span class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                        </span>
                                        <input id="datepicker3" data-format="dd-mm-yyyy" class=" form-control" name="car_enddate_act"  type="text" value="<?=$data['car_enddate_act']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd">
                                  วันหมดอายุภาษีรถยนต์:
                                </label>
                                <div class="col-sm-10">
                                  <div class="input-group ">
                                        <span class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                        </span>
                                        <input id="datepicker4" data-format="dd-mm-yyyy" class=" form-control"  name="car_enddate_tax" type="text" value="<?=$data['car_enddate_tax']?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" bd-t-red">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="fa fa-gears"></i> <strong>กำหนดข้อมูลสำคัญ</strong>ของรถยนต์</h3>
                            </div>
                             <div class="form-group">
                                <label class="control-label col-sm-2" for="email">{{ __('validation.attributes.backend.access.users.companyname') }}:</label>
                                <div class="col-sm-10">
                                  @include('includes.partials.company')
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd">
                                    {{ __('validation.attributes.backend.access.users.belong') }}:
                                </label>
                                <div class="col-sm-10">          
                                		<select class="form-control chosen-img" name="branch_name" style="height:40px;">
											<option value="" data-img-src="{{asset("backend/img/cars/brands/belong.png")}}">{{ __('validation.attributes.backend.company.belong_selectbox') }}</option>
											  	<?php
											  		foreach (config('company.'.app()->getLocale().'.belong') as $key => $value) {
											  			if($value != null){
											  				  $img = asset("backend/img/cars/brands/belong.png");
											            echo "<option ";
											            echo ($data['branch_name']==$key?'selected':'');
											            echo " data-belogn-code='".$key."'   data-img-src='".$img."' value=".$key.">";
													        echo $value[0];
													        echo "</option>";
											  			}
											  		}
											  	?>
										</select> 
                                </div>
                            </div>
                            <div class="form-group">
                                  <label class="    control-label col-sm-2" for="pwd">
                                      ทะเบียนรถยนต์:
                                  </label>
                                <div class="col-sm-2" id="select_number">        
                                      <input type="text" id="license_plate" name="license_plate" value="<?=$data['license_plate']?>">
                                </div>
                                
                                <div class="col-sm-3">        
                                      <select class="form-control chosen-img"  name="license_province" id="car_licen_4_province" style="height:40px;" data-placeholder="เลือกจังหวัด" >
                                         <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg")}}">เลือกจังหวัด</option>
                                        <?php
                                            $arr_province =array("กระบี่","กรุงเทพมหานคร","กาญจนบุรี","กาฬสินธุ์","กำแพงเพชร","ขอนแก่น","จันทบุรี","ฉะเชิงเทรา" ,"ชลบุรี","ชัยนาท","ชัยภูมิ","ชุมพร","เชียงราย","เชียงใหม่","ตรัง","ตราด","ตาก","นครนายก","นครปฐม","นครพนม","นครราชสีมา" ,"นครศรีธรรมราช","นครสวรรค์","นนทบุรี","นราธิวาส","น่าน","บุรีรัมย์","ปทุมธานี","ประจวบคีรีขันธ์","ปราจีนบุรี","ปัตตานี" ,"พะเยา","พังงา","พัทลุง","พิจิตร","พิษณุโลก","เพชรบุรี","เพชรบูรณ์","แพร่","ภูเก็ต","มหาสารคาม","มุกดาหาร","แม่ฮ่องสอน" ,"ยโสธร","ยะลา","ร้อยเอ็ด","ระนอง","ระยอง","ราชบุรี","ลพบุรี","ลำปาง","ลำพูน","เลย","ศรีสะเกษ","สกลนคร","สงขลา" ,"สตูล","สมุทรปราการ","สมุทรสงคราม","สมุทรสาคร","สระแก้ว","สระบุรี","สิงห์บุรี","สุโขทัย","สุพรรณบุรี","สุราษฎร์ธานี" ,"สุรินทร์","หนองคาย","หนองบัวลำภู","อยุธยา","อ่างทอง","อำนาจเจริญ","อุดรธานี","อุตรดิตถ์","อุทัยธานี","อุบลราชธานี");
                                            
                                                foreach ($arr_province as &$alphabet) {
                                                    $img = '';
                                                    echo "<option ";
                                                    echo ($data['license_province']==$alphabet?'selected':'');
                                                    echo " data-img-src='".$img."' value=".$alphabet.">";
                                                    echo $alphabet;
                                                    echo "</option>";
                                                }
                                        ?>
                                      </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2" style="padding: 0 15px 29px 15px;">
                                     <span class="registration-ui">
                                        <label style="line-height: 0px;text-align: center;">
                                        	<small id="car_licen_1_number"></small>
                                            <small id="car_licen_4_province" style="    font-size: 12px;display: block;text-align: center;padding-top: 20px;">xxxxxxxxxxxxx</small>
                                        </label>
                                     </span>
                                </div>
                            </div>
                            <div class="form-group">
                                  <label class="control-label col-sm-2" for="pwd">
                                      สีรถยนต์:
                                  </label>
                                <div class="col-sm-10">
                                        <select class="form-control chosen-img" name="color" style="height:40px;">
                                        <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg") }}" <?=$data['color']=='black'?'selected':'';?> value="black">สีดำ</option>
                                        <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg") }}" <?=$data['color']=='gray'?'selected':'';?> value="gray">สีเทา</option>
                                        <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg") }}" <?=$data['color']=='white'?'selected':'';?> value="white">สีขาว</option>
                                        <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg") }}" <?=$data['color']=='blue'?'selected':'';?> value="blue">สีฟ้า</option>
                                        <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg") }}" <?=$data['color']=='silver'?'selected':'';?> value="silver">สีเงิน</option>
                                        <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg") }}" <?=$data['color']=='red'?'selected':'';?> value="red">สีแดง</option>
                                        <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg") }}" <?=$data['color']=='green'?'selected':'';?> value="green">สีเขียว</option>
                                        <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg") }}" <?=$data['color']=='orange'?'selected':'';?> value="orange">สีส้ม</option>
                                        <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg") }}" <?=$data['color']=='bourne'?'selected':'';?> value="bourne">สีบรอนซ์</option>
                                        <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg") }}" <?=$data['color']=='yellow'?'selected':'';?> value="yellow">สีเหลือง</option>
                                        <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg") }}" <?=$data['color']=='violet'?'selected':'';?> value="violet">สีม่วง</option>
                                        <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg") }}" <?=$data['color']=='brown'?'selected':'';?> value="brown">สีน้ำตาล</option>
                                      </select>
                                </div>
                            </div>
                            <div class="form-group">
                                  <label class="control-label col-sm-2" for="pwd">
                                      เลขเครื่องยนต์:
                                  </label>
                                <div class="col-sm-10">
                                    <div class="input-group ">
                                        <span class="input-group-addon" style="padding: 10px;">
                                          <i class="fa fa-automobile"></i>
                                        </span>
                                        <input class=" form-control" name="engine_number" value="<?=$data['engine_number'];?>" type="text" required style="text-transform: uppercase;"> 
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                  <label class="control-label col-sm-2" for="pwd">
                                      เลขตัวถัง:
                                  </label>
                                <div class="col-sm-10">
                                    <div class="input-group ">
                                        <span class="input-group-addon" style="padding: 10px;">
                                          <i class="fa fa-automobile"></i>
                                        </span>
                                        <input class=" form-control" name="body_number" type="text" required style="text-transform: uppercase;" value="<?=$data['body_number'];?>"> 
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                  <label class="control-label col-sm-2" for="pwd">
                                      รหัสรถยนต์ประจำสาขา:
                                  </label>
                                <div class="col-sm-10">
                                    <div class="input-group ">
                                        <span class="input-group-addon" style="padding: 10px;">
                                          <i class="fa fa-automobile"></i>
                                        </span>
                                        <input class=" form-control" name="branch_car_code_cc" type="text" required style="text-transform: uppercase;" value="<?=$data['branch_car_code'];?>"> 
                                        <input class=" form-control" name="branch_car_code" type="hidden" required style="text-transform: uppercase;" value="<?=$data['branch_car_code'];?>"> 
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                  <label class="control-label col-sm-2" for="pwd">
                                      สถานะการทำงาน:
                                  </label>
                                <div class="col-sm-10">        
                                      <select class="form-control chosen-img" name="status" style="height:40px;">
                                        <option <?=$data['status']=='enabled'?'selected':'';?> data-img-src="{{ asset("backend/img/checked.svg")}}"  value="enabled">เปิดการใช้งาน</option>
                                        <option <?=$data['status']=='closed'?'selected':'';?> data-img-src="{{ asset("backend/img/multiply.svg")}}" value="closed">ปิดการใช้งาน</option>
                                        <option <?=$data['status']=='booking'?'selected':'';?> data-img-src="{{ asset("backend/img/multiply.svg")}}" value="booking">อยู่ระหว่างการเช่า</option>
                                      </select>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label col-sm-2" for="email">หมายเหตุ:</label>
                                <div class="col-sm-10">
                                    <textarea name="car_description" class="textarea_question" rows="10" cols="80"><?=$data['car_description'];?></textarea>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <ul class="list-inline" style="float:right;">
                            <li><button type="submit" class="btn btn-success "><i class="fa fa-save"></i>&nbsp;บันทึก</button></li>
                            <li><a class="btn btn-warning prev-step" href="{{ route('admin.auth.user.index') }}"><i class="fa fa-times"></i>&nbsp;ยกเลิก</a></li>
                        </ul>
                    </div>
                </div>

                <input type="hidden" name="old_image" value="{{ $data['car_image']}}">
               
            </form>
        </div>
    </div>
</div>




@endsection
<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript') 
<script src="{{ asset("plugins/ckeditor/ckeditor.js")}}"></script>
<script src="{{ asset("plugins/bootstrap-datepicker/bootstrap-datepicker.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.date.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.time.js")}}"></script>
<script src="{{ asset("plugins/bootstrap-switch/bootstrap-switch.js")}}"></script>
<script src="{{ asset("plugins/bootstrap-progressbar/bootstrap-progressbar.js")}}"></script>

<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script>

<script src="{{ asset("plugins/chosen/chosen.jquery.js")}}"></script>
<script src="{{ asset("plugins/chosen/chosenImage/chosenImage.jquery.js")}}"></script>
<!-- Laravel Javascript Validation -->
<script type="text/javascript" src="{{ asset("vendor/jsvalidation/js/jsvalidation.js")}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\Backend\Cars\CarRequest','#my-form'); !!}

<script>
        CKEDITOR.replace('car_description',{
           toolbar: [
              { name: 'tools', items: [ 'Maximize'] },
              { name: 'insert', items: ['Format' ] },
              { name: 'document', items: [ '-', 'Undo', 'Redo' ] },
              { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
              { name: 'insert', items: ['Table', 'HorizontalRule', 'SpecialChar' ] },
              { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] },
              { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
              { name: 'colors', groups: [ 'colors' ] },
            ]
        });
</script>

<script type="text/javascript">
$(document).ready(function () {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap'
    });
});

$(document).ready(function () {
    $('#datepicker2').datepicker({
      uiLibrary: 'bootstrap'
    });
});

$(document).ready(function () {
    $('#datepicker3').datepicker({
      uiLibrary: 'bootstrap'
    });
});

$(document).ready(function () {
    $('#datepicker4').datepicker({
      uiLibrary: 'bootstrap'
    });
});

	function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
          jQuery('#imagePreview').attr('src',e.target.result);
          $('#imagePreview').hide();
          $('#imagePreview').fadeIn(650);
      }
      reader.readAsDataURL(input.files[0]);
  }
}
$("#imageUpload").change(function() {
    readURL(this);
});

    jQuery(document).ready(function(){

   	jQuery("input#license_plate").keyup(function(){
        var text = jQuery("input#license_plate").val();
		jQuery("small#car_licen_1_number").text(text)
       
    });

   	var text = jQuery("input#license_plate").val();
	jQuery("small#car_licen_1_number").text(text)

        var car1 = "";
        var car2 = "";
        var car3 = "";

        jQuery(".chosen-img,.chosen-select").chosenImage();

        jQuery('body').on('focus', '.chosen-container-single input', function(){
            if (!jQuery(this).closest('.chosen-container').hasClass('chosen-container-active')){
                jQuery(this).closest('.chosen-container').trigger('mousedown');
                //or use this instead
                //$('#select').trigger('liszt:open');
            }    
        });
        jQuery('input.number').keyup(function(event) {
          // skip for arrow keys
          if(event.which >= 37 && event.which <= 40) return;

          // format number
          jQuery(this).val(function(index, value) {
            return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            ;
          });
        });
        $.ajaxSetup({
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
        });
        //ยี่ห่อเปลี่ยน
        jQuery("select[name='car_brand']").change(function(){

            var brand_name = jQuery(this).val();

            jQuery("select#set_generation").empty().append('<option >กำลังโหลด...</option>');
            jQuery("select#set_generation").trigger("chosen:updated");
            
            jQuery.post("{{ route('admin.cars.generation') }}", {brand: brand_name}, function(result){
                if(result){
                        setTimeout(
                            function(){
                                    jQuery("select#set_generation option:first-child").text('เลือกรุ่น');
                                    jQuery("select#set_generation").append(result);
                                    jQuery("select#set_generation").trigger("chosen:updated");
                            },300
                        );
                }else{
                    jQuery("select#set_generation").empty().append('<option >ไม่พบอะไรกับเขาหร๊อก...</option>');
                    jQuery("select#set_generation").trigger("chosen:updated");
                }

            });
        });

        //รุ่นเปลียน
        jQuery("select#set_generation").change(function(){
            var generation = jQuery(this).val();
            var brand = jQuery(this).find(':selected').attr('data-brand');

            jQuery("select#nickname").empty().append('<option >กำลังโหลด...</option>');
            jQuery("select#nickname").trigger("chosen:updated");
            
            jQuery.post("{{ route('admin.cars.nickname') }}", {generation: generation,brand:brand}, function(result){
                if(result != ''){
                        setTimeout(
                            function(){
                                    jQuery("select#nickname option:first-child").text('เลือกรุ่น');
                                    jQuery("select#nickname").append(result);
                                    jQuery("select#nickname").trigger("chosen:updated");
                            },300
                        );
                }else{
                    jQuery("select#nickname").empty().append('<option >ไม่พบอะไรกับเขาหร๊อก...</option>');
                    jQuery("select#nickname").trigger("chosen:updated");
                }

            });
        });


        //โฉมรถยนต์เปลี่ยน
        jQuery("select#nickname").change(function(){

            var nickname = jQuery(this).find(':selected').attr('data-nick');
            var brand = jQuery(this).find(':selected').attr('data-brand');
            var variant = jQuery(this).find(':selected').attr('value');

            jQuery("select[name='car_generation_detail']").empty().append('<option >กำลังโหลด...</option>');
             jQuery("select[name='car_generation_detail']").trigger("chosen:updated");
            
            // alert(brand+' '+nickname+" "+variant);
            jQuery.post("{{ route('admin.cars.variant') }}", {brand: brand,nickname: nickname,variant: variant}, function(result){
                if(result != ''){
                        setTimeout(
                            function(){
                                    jQuery("select[name='car_generation_detail'] option:first-child").text('เลือกรายละเอียดรุ่น');
                                    jQuery("select[name='car_generation_detail']").append(result);
                                    jQuery("select[name='car_generation_detail']").trigger("chosen:updated");
                            },300
                        );
                }else{
                    jQuery("select[name='car_generation_detail']").empty().append('<option >ไม่พบอะไรกับเขาหร๊อก...</option>');
                    jQuery("select[name='car_generation_detail']").trigger("chosen:updated");
                }

            });
        });



         //บริษัทประกันภัยเปลี่ยน
        jQuery("select[name='car_company_insurance']").change(function(){

            var insurance = jQuery(this).val();

            jQuery("select[name='car_type_insurance']").empty().append('<option >กำลังโหลด...</option>');
            jQuery("select[name='car_type_insurance']").trigger("chosen:updated");
          
            jQuery.post("{{ route('admin.cars.type-insurance') }}", {insurance: insurance}, function(result){
             
                if(result != ''){
                        setTimeout(
                            function(){
                                    jQuery("select[name='car_type_insurance'] option:first-child").text('เลือกประเภทประกันภัย');
                                    jQuery("select[name='car_type_insurance']").append(result);
                                    jQuery("select[name='car_type_insurance']").trigger("chosen:updated");
                            },300
                        );
                }else{
                    jQuery("select[name='car_type_insurance']").empty().append('<option >ไม่พบอะไรกับเขาหร๊อก...</option>');
                    jQuery("select[name='car_type_insurance']").trigger("chosen:updated");
                }

            });
        });

		var insurance = jQuery("select[name='car_company_insurance']").val();

            jQuery("select[name='car_type_insurance']").empty().append('<option >กำลังโหลด...</option>');
            jQuery("select[name='car_type_insurance']").trigger("chosen:updated");
          
            jQuery.post("{{ route('admin.cars.type-insurance') }}", {insurance: insurance}, function(result){
             
                if(result != ''){
                        setTimeout(
                            function(){
                                    jQuery("select[name='car_type_insurance'] option:first-child").text('เลือกประเภทประกันภัย');
                                    jQuery("select[name='car_type_insurance']").append(result);
                                    jQuery("select[name='car_type_insurance']").trigger("chosen:updated");

                                     jQuery("select#car_type_insurance").val('{{ $data->car_type_insurance }}');
                            },300
                        );
                }else{
                    jQuery("select[name='car_type_insurance']").empty().append('<option >ไม่พบอะไรกับเขาหร๊อก...</option>');
                    jQuery("select[name='car_type_insurance']").trigger("chosen:updated");
                }

           });


        //ป้ายทะเบียนรถ
        jQuery("select[name='car_licen_1_number']").change(function(){
            car1 = jQuery(this).val();
            jQuery("small#car_licen_1_number").text(jQuery(this).val());
            jQuery("input[name='license_plate']").val(car1+car2+car3);
        }); 
        jQuery("select[name='car_licen_2_text']").change(function(){
            var text123 =  jQuery("select[name='car_licen_2_text'] + div > ul.chosen-choices li span").text();
            car2 = text123;
            jQuery("small#car_licen_2_text").text(text123.replace(/,\s?/g, ""));
            jQuery("input[name='license_plate']").val(car1+car2+car3);
        }); 

        jQuery("select[name='car_licen_2_text'] + div > ul.chosen-choices li a.search-choice-close").on("click",function(){
            var text123 =  jQuery("select[name='car_licen_2_text'] + div > ul.chosen-choices li span").text();
            car2 = text123;
            jQuery("small#car_licen_2_text").text(text123.replace(/,\s?/g, ""));
            jQuery("input[name='license_plate']").val(car1+car2+car3);
        }); 
        //ทำให้มันคลิกซ้ำได้
        jQuery("select[name='car_licen_2_text'] + div").on("click",function(){
           jQuery("select[name='car_licen_2_text'] + div > div.chosen-drop > ul.chosen-results >li").each(function(){
                 jQuery("select[name='car_licen_2_text'] + div > div.chosen-drop > ul.chosen-results >li").removeClass('result-selected').addClass('active-result');
            });
        });
        jQuery("select[name='car_licen_3_number']").change(function(){
            var text456 =  jQuery("select[name='car_licen_3_number'] + div > ul.chosen-choices li span").text();
            car3= text456;
            jQuery("small#car_licen_3_number").text(text456.replace(/,\s?/g, ""));
            jQuery("input[name='license_plate']").val(car1+car2+car3);
        });

        jQuery("select[name='car_licen_3_number'] + div").on("click",function(){
           jQuery("select[name='car_licen_3_number'] + div > div.chosen-drop > ul.chosen-results >li").each(function(){
                 jQuery("select[name='car_licen_3_number'] + div > div.chosen-drop > ul.chosen-results >li").removeClass('result-selected').addClass('active-result');
            });
        });

        jQuery("select[name='car_licen_4_province']").change(function(){
            jQuery("small#car_licen_4_province").text(jQuery(this).val());

            jQuery("input[name='license_province']").val(jQuery(this).val());
        });

        jQuery("small#car_licen_4_province").text(jQuery("select[name='car_licen_4_province']").val());

        jQuery("small#car_licen_4_province").text(jQuery("select[name='license_province']").val());

        jQuery('form button[type="submit"]').on('click', function() {
                var $this = jQuery(this);
                $this.attr('data-loading-text',"<i class='fa fa-spinner fa-spin'></i>&nbsp;กำลังประมวลผล");
                $this.button('loading');

                setTimeout(function(){
                    var total_error = jQuery('form').find('.has-error').length;
                    if(total_error>0){
                      $this.button('reset');
                      jError(
                        '<i class="fa fa-info-circle" style="padding-right:8px"></i>พบข้อผิดพลาด!', {
                            HorizontalPosition: 'right',
                            VerticalPosition: 'top',
                            ShowOverlay: false,
                            TimeShown: 3000,
                            MinWidth:400
                        });
                    }

                },500);
        });
        jQuery("select[name='gen_car_id']").change(function(e){
            e.preventDefault();
             console.log('Change');
                dish_id = $(this).val();
                $.get("/admin/cars/car-branch-code/" + dish_id,function (data) {
                   // console.log('รอแว๊บกำลังประมวลผล');
                   jQuery("span#code_branch + input").val("รอแว๊บกำลังประมวลผล");
                }).done(function (data) {
                        console.log(data);
                        setTimeout(function(){
                             jQuery("span#code_branch + input").val(data.max_branch_code);
                            jQuery("input[name='branch_car_code']").val(data.new_branch_code);
                        },800);
                  
                });
            
            jQuery("span#code_branch").text(jQuery(this).val()+" - ");
        });

        jQuery("input[name='branch_car_code_cc']").keyup(function(e){
            var branch_name = jQuery("select[name='gen_car_id']").val();

            jQuery("input[name='branch_car_code']").val(branch_name+'-'+jQuery(this).val());
        });

        loadbody();
    });// End tag jQuery





	function loadbody(){
			var brand_name = jQuery("select[name='car_brand']").val();

            jQuery("select#set_generation").empty().append('<option >กำลังโหลด...</option>');
            jQuery("select#set_generation").trigger("chosen:updated");
            
            jQuery.post("{{ route('admin.cars.generation') }}", {brand: brand_name}, function(result){
                if(result){
                        setTimeout(
                            function(){
                                    jQuery("select#set_generation option:first-child").text('เลือกรุ่น');
                                    jQuery("select#set_generation").append(result);
                                    jQuery("select#set_generation").trigger("chosen:updated");

                                    jQuery("select#set_generation").val('{{ $data->car_generation }}');
                                    setTimeout(function(){ load_a(); }, 500);
                            },300
                        );
                }else{
                    jQuery("select#set_generation").empty().append('<option >ไม่พบอะไรกับเขาหร๊อก...</option>');
                    jQuery("select#set_generation").trigger("chosen:updated");
                }
            });
	}


	function load_a(){
		 var generation = jQuery("select#set_generation").val();
            var brand = jQuery("select#set_generation").find(':selected').attr('data-brand');

            jQuery("select#nickname").empty().append('<option >กำลังโหลด...</option>');
            jQuery("select#nickname").trigger("chosen:updated");
            
            jQuery.post("{{ route('admin.cars.nickname') }}", {generation: generation,brand:brand}, function(result){
                if(result != ''){
                        setTimeout(
                            function(){
                                    jQuery("select#nickname option:first-child").text('เลือกรุ่น');
                                    jQuery("select#nickname").append(result);
                                    jQuery("select#nickname").trigger("chosen:updated");

                                    jQuery("select#nickname").val('{{ $data->car_nickname }}');


                                     setTimeout(function(){ load_b(); }, 500);

                            },300
                        );
                }else{
                    jQuery("select#nickname").empty().append('<option >ไม่พบอะไรกับเขาหร๊อก...</option>');
                    jQuery("select#nickname").trigger("chosen:updated");
                }

            });
	}



	function load_b(){
		  var nickname = jQuery("select#nickname").find(':selected').attr('data-nick');
            var brand = jQuery("select#nickname").find(':selected').attr('data-brand');
            var variant = jQuery("select#nickname").find(':selected').attr('value');

            jQuery("select[name='car_generation_detail']").empty().append('<option >กำลังโหลด...</option>');
             jQuery("select[name='car_generation_detail']").trigger("chosen:updated");
            
            // alert(brand+' '+nickname+" "+variant);
            jQuery.post("{{ route('admin.cars.variant') }}", {brand: brand,nickname: nickname,variant: variant}, function(result){
                if(result != ''){
                        setTimeout(
                            function(){
                                    jQuery("select[name='car_generation_detail'] option:first-child").text('เลือกรายละเอียดรุ่น');
                                    jQuery("select[name='car_generation_detail']").append(result);
                                    jQuery("select[name='car_generation_detail']").trigger("chosen:updated");

                                     jQuery("select#variant").val('{{ $data->car_generation_detail }}');

                            },300
                        );
                }else{
                    jQuery("select[name='car_generation_detail']").empty().append('<option >ไม่พบอะไรกับเขาหร๊อก...</option>');
                    jQuery("select[name='car_generation_detail']").trigger("chosen:updated");
                }

            });
	}
</script>

@if(session()->has('message'))

    <script type="text/javascript">
        <?php
            $type = session()->get('message');
            $message = __('alerts.backend.database.'.$type.'');
        ?>

        <?=$type?>(
                '<i class="fa fa-check-square-o" style="color:#fff;padding-right:8px"></i><?=$message?>', {
                    HorizontalPosition: 'right',
                    VerticalPosition: 'top',
                    ShowOverlay: false,
                    TimeShown: 5000,
                    MinWidth:400
        });
    </script>
@endif

@endsection
