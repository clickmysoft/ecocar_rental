@extends('backend.layouts.app')

@section ('title', __('รายการรถยนต์'))

@section('content')

<link rel='stylesheet' type='text/css' href='https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css'/>
  <div class="panel panel-default">
                        <div class="panel-heading" style="color: #fff !important;color: #fff !important;background: #60868b;padding: 10px 15px;">
                            <h3 class="panel-title" style="float:left;padding: 10px 0 !important; margin: 0 !important;"><i class="fa fa-database"></i>&nbsp;ตารางแสดงรายการรถยนต์</h3>
                            <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                                    <a href="/admin/cars/car/create" class="btn btn-success ml-1" data-toggle="tooltip" title="" 
                            style="float: right;" data-original-title="Create New"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;สร้างข้อมูลรถยนต์</a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red filter-right">
                                     <table id="DataTable" class="table table-striped table-hover table-dynamic">
                                            <thead>
                                                <tr>
                                                    <th class="col-md-1" style="text-align: center;">code</th>
                                                    <th class="col-md-1" style="padding-left: 0;">ทะเบียน</th>
                                                    <th class="col-md-1">รุ่นรถ</th>
                                                    <th class="col-md-1">เลขไมค์</th>
                                                    <th class="col-md-1" style="font-size: 12px;">วันที่จดทะเบียน</th>
                                                    <th class="col-md-1" style="font-size: 12px;">วันหมดอายุประกันภัย</th>
                                                    <th class="col-md-1" style="font-size: 12px;">วันหมดอายุ พรบ</th>
                                                    <th class="col-md-1" style="font-size: 12px;">วันหมดอายุภาษีรถยนต์</th>
                                                    <th class="col-md-1" style="padding-left: 0;text-align:center;">สร้างเมื่อ</th>
                                                    <th class="col-md-1" style="padding-left: 0;text-align:center;">สถานะ</th>
                                                    <th class="col-md-1" style="padding-left: 0; text-align: center;">{{ __('labels.general.actions') }}</th>
                                                </tr>
                                            </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
        <div class="row">
            <div class="col-5">
                <div class="float-right">
                </div>
            </div><!--col-->
        </div><!--row-->
@endsection

@section('begin_javascript')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <!--START CODING JS-->
    <script>
        $(document).ready(function () {
            //////// DataTable //////
            $('#DataTable').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('ajaxdata.getdata.cars') }}",
                "order": [[ 8, "desc" ]],
                "language": {
                    "lengthMenu": "แสดงผล _MENU_ ข้อมูลต่อหน้า",
                    "zeroRecords": "Nothing found - sorry",
                    "info": "ลำดับหน้าที่ _PAGE_ จาก _PAGES_ หน้า - จำนวนรายการทั้งหมด _TOTAL_ รายการ",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "<i class='fa fa-search'></i> ค้นหาข้อมูล:",
                    "paginate": {
                        "first":      "หน้าแรก",
                        "last":       "หน้าสุดท้าย",
                        "next":       "ถัดไป",
                        "previous":   "ก่อนหน้า"
                    },
                },
                "columns":[
                        {data: 'branch_car_code'},
                        {data: 'license_plate'},
                        {data: 'car_generation'},
                        {data: 'car_mile'},
                        {data: 'car_date_register'},
                        {data: 'car_enddate_insurance'},
                        {data: 'car_enddate_act'},
                        {data: 'car_enddate_tax'},
                        {data: 'created_at'},
                        {data: 'status'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            }); ///////// End DataTable ///////

        }); //End jQuery
    </script>
     <!--END CODING JS-->
    <!--START IMPORT EVENT FORM-->
    @include('includes.forms.ajax_delete')
    <!--END IMPORT EVENT FORM-->
@endsection