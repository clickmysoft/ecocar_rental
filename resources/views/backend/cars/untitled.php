@extends ('backend.layouts.app')

@section ('title', __('รายละเอียดข้อมูลราคา'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('begin_css')
<style type="text/css">
.page-profil h4{
    width: auto;
}
    .card{
        -webkit-border-radius: 2px;
        border-radius: 2px;
        border: 1px solid #dee0e1;
    }
    .card{
        -webkit-border-radius: 2px;
        border-radius: 2px;
        border: solid 1px #dee0e1;
        margin: 0;
    }
    .ks-panel, .panel{
        -webkit-border-radius: 2px;
        border-radius: 2px;
        border: solid 1px #dee0e1;
        background: #fff;
    }
    .card{
        position: relative;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -webkit-flex-direction: column;
       -ms-flex-direction: column;
        flex-direction: column;
        background-color: #fff;
        border: 1px solid rgba(0,0,0,.125);
        border-radius: .25rem;
    }
    .card-header:first-child{
        -webkit-border-radius: 0;
        border-radius: 0;
    }
    .card>.card-header{
        line-height: 15px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        -js-display: flex;
        display: flex;
        -webkit-box-pack: justify;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
    }
    .ks-panel .card-header, .ks-panel .panel-heading, .panel .card-header, .panel .panel-heading{
        padding: 16px 20px;
        font-weight: 500;
        background: 0 0;
        border-bottom: solid 1px #dee0e1;
        margin: 0;
       -webkit-border-top-left-radius: 2px;
        border-top-left-radius: 2px;
       -webkit-border-top-right-radius: 2px;
        border-top-right-radius: 2px;
        font-size: 14px;
    }
    .card-header:first-child{
        border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
    }
    .ks-panel .card-header+.card-block, .ks-panel .card-header+.panel-body, .ks-panel .panel-heading+.card-block, .ks-panel .panel-heading+.panel-body, .panel .card-header+.card-block, .panel .card-header+.panel-body, .panel .panel-heading+.card-block, .panel .panel-heading+.panel-body
    {
        -webkit-border-top-left-radius: 0;
        border-top-left-radius: 0;
        -webkit-border-top-right-radius: 0;
        border-top-right-radius: 0;
    }
    .ks-panel .card-block, .ks-panel .panel-body, .panel .card-block, .panel .panel-body{
        -webkit-border-top-left-radius: 2px;
        border-top-left-radius: 2px;
       -webkit-border-top-right-radius: 2px;
        border-top-right-radius: 2px;
    }
    .card-block{
        padding: 16px 20px;
        font-size: 14px;
    }
    .card-block{
        -webkit-box-flex: 1;
        -webkit-flex: 1 1 auto;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        padding: 1.25rem;
    }
    
    .ks-widget-payment-simple-amount-item{
            min-height: 124px;
    padding: 30px 20px;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    -webkit-flex-direction: row;
    -ms-flex-direction: row;
    flex-direction: row;
    border-color: #e5e5e5;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    -webkit-flex-wrap: wrap;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    }
    .ks-widget-payment-simple-amount-item.ks-green .payment-simple-amount-item-icon-block{
            background-color: #67af42bf;
    }
    .ks-widget-payment-simple-amount-item .payment-simple-amount-item-icon-block{
            width: 64px;
    height: 64px;
    -webkit-border-radius: 32px;
    border-radius: 32px;
    display: -webkit-inline-box;
    display: -webkit-inline-flex;
    display: -ms-inline-flexbox;
    display: inline-flex;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    margin-right: 20px;
    }
</style>
@endsection

@section('content')
  <div id="main-content" class="page-profil">
            <div class="row">
                <!-- BEGIN PROFIL SIDEBAR -->
                <div class="col-md-3 profil-sidebar m-t-20">
                    <img src='{{ asset("cars/toyota/vios/image-01.jpg")}}' class="img-responsive" alt="profil" > 

                    <div class="box-body box-profile">
                        <h3 class="profile-username text-center" style="color: yellowgreen;">บริษัท {{ $data->company_name }} </h3>
                            <h4 class="text-muted text-center">สาขา {{ $data->branch_name_new }}</h4>
            </div>



                </div>
                <!-- END PROFIL SIDEBAR -->
                <!-- BEGIN PROFIL CONTENT -->
                <div class="col-md-9 profil-content m-t-20">
                    <div class="row">
                         <div class="">
                        <div class="profil-sidebar-element">


                             <div class="col-lg-4">
                            <div class="card ks-widget-payment-simple-amount-item ks-green" style="justify-content: left;">
                                    <div class="payment-simple-amount-item-icon-block">
                                        <span class="ks-icon-combo-chart ks-icon"><i class="fa fa-car fa-2x" aria-hidden="true" style="
    color: #fff;
"></i></span>
                                    </div>
                                    <div class="payment-simple-amount-item-body">
                                        <div class="payment-simple-amount-item-amount">
                                            <h4 class="ks-amount">ยี่ห้อ </h4>
                                            <span class="ks-amount-icon ks-icon-circled-up-right"></span>
                                        </div>
                                        <div class="payment-simple-amount-item-description" style="font-size: 14px;color: #0090DA;">
                                            {{ $data->car_brand_new }}
                                        </div>
                                    </div>
                                </div>
                        </div>


                         <div class="col-lg-4">
                            <div class="card ks-widget-payment-simple-amount-item ks-green" style="justify-content: left;">
                                    <div class="payment-simple-amount-item-icon-block">
                                        <span class="ks-icon-combo-chart ks-icon"><i class="fa fa-tasks fa-2x" aria-hidden="true" style="
    color: #fff;
"></i></span>
                                    </div>
                                    <div class="payment-simple-amount-item-body">
                                        <div class="payment-simple-amount-item-amount">
                                            <h4 class="ks-amount">รุ่น </h4>
                                            <span class="ks-amount-icon ks-icon-circled-up-right"></span>
                                        </div>
                                        <div class="payment-simple-amount-item-description" style="font-size: 14px;color: #0090DA;">
                                            {{ $data->car_generation_new }}
                                        </div>
                                    </div>
                                </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="card ks-widget-payment-simple-amount-item ks-green" style="justify-content: left;">
                                    <div class="payment-simple-amount-item-icon-block">
                                        <span class="ks-icon-combo-chart ks-icon"><i class="fa fa-calendar fa-2x" aria-hidden="true" style="
    color: #fff;
"></i></span>
                                    </div>
                                    <div class="payment-simple-amount-item-body">
                                        <div class="payment-simple-amount-item-amount">
                                            <h4 class="ks-amount">กำหนดวัน - เวลา </h4>
                                            <span class="ks-amount-icon ks-icon-circled-up-right"></span>
                                        </div>
                                        <div class="payment-simple-amount-item-description" style="font-size: 14px;color: #0090DA;">
                                            <?php
                                if($data->status_event =='close'){
                            ?>
                                ปิด
                            <?php       
                                }else{
                            ?>
                                เปิด
                            <?php       
                                }
                            ?> 
                                        </div>
                                    </div>
                                </div>
                        </div>
                          <div class="col-lg-4">
                            <div class="card ks-widget-payment-simple-amount-item ks-green" style="justify-content: left;">
                                    <div class="payment-simple-amount-item-icon-block">
                                        <span class="ks-icon-combo-chart ks-icon"><i class="fa fa-tasks fa-2x" aria-hidden="true" style="
    color: #fff;
"></i></span>
                                    </div>
                                    <div class="payment-simple-amount-item-body">
                                        <div class="payment-simple-amount-item-amount">
                                            <h4 class="ks-amount">ประเภทการเช่า </h4>
                                            <span class="ks-amount-icon ks-icon-circled-up-right"></span>
                                        </div>
                                        <div class="payment-simple-amount-item-description" style="font-size: 14px;color: #0090DA;">
                                            {{ $data->type_rental_prices_new }} 
                                        </div>
                                    </div>
                                </div>
                        </div>
                          <div class="col-lg-4">
                            <div class="card ks-widget-payment-simple-amount-item ks-green" style="justify-content: left;">
                                    <div class="payment-simple-amount-item-icon-block">
                                        <span class="ks-icon-combo-chart ks-icon"><i class="fa fa-money fa-2x" aria-hidden="true" style="
    color: #fff;
"></i></span>
                                    </div>
                                    <div class="payment-simple-amount-item-body">
                                        <div class="payment-simple-amount-item-amount">
                                            <h4 class="ks-amount">ราคาเช่ารถ </h4>
                                            <span class="ks-amount-icon ks-icon-circled-up-right"></span>
                                        </div>
                                        <div class="payment-simple-amount-item-description" style="font-size: 14px;color: #0090DA;">
                                          
                                        </div>
                                    </div>
                                </div>
                        </div>

                          <div class="col-lg-4">
                            <div class="card ks-widget-payment-simple-amount-item ks-green" style="justify-content: left;">
                                    <div class="payment-simple-amount-item-icon-block">
                                        <span class="ks-icon-combo-chart ks-icon"><i class="fa fa-building-o fa-2x" aria-hidden="true" style="
    color: #fff;
"></i></span>
                                    </div>
                                    <div class="payment-simple-amount-item-body">
                                        <div class="payment-simple-amount-item-amount">
                                            <h4 class="ks-amount">สถานะ </h4>
                                            <span class="ks-amount-icon ks-icon-circled-up-right"></span>
                                        </div>
                                        <div class="payment-simple-amount-item-description" style="font-size: 14px;color: #0090DA;">
                                            {{ $data->status_new }}
                                        </div>
                                    </div>
                                </div>
                        </div>
                        </div>
                      <!-- 
                        <div class="profil-sidebar-element">
                            <h4>SOCIAL SHARING</h4>
                            <div class="social-btn-small"><a href="#" class="zocial icon facebook m-0">Sign in with Facebook</a>
                            </div>
                            <div class="social-btn-small"><a href="#" class="zocial icon twitter m-0">Sign in with Twitter</a>
                            </div>
                            <div class="social-btn-small"><a href="#" class="zocial icon googleplus m-0">Sign in with Google+</a>
                            </div>
                        </div> -->
                    </div>
                        <div class="col-md-12 profil-presentation p-t-20">
                                  <div class="card ks-panel">
                                    <h4 class="card-header" style="font-size: 18px;">
                                        หมายเหตุ:
                                    </h4>
                                    <div class="card-block">
                                        <div class="ks-text-block">
                                            <p class="ks-text"><?=$data->remarks?></p>
                                        </div>
                                        
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="row profil-groups">
                        <div class="col-md-6">
                            <div class="row border-gray">
                                <div class="col-xs-4">
                                    <div class="row">
                                        
                                    </div>
                                    <div class="row">
                                        
                                    </div>
                                </div>
                                <div class="col-xs-8 p-0 profil-group">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 border-bottom p-t-20"></div>
                    </div>
                </div>
                <!-- END PROFIL CONTENT -->
            </div>
        </div>

@endsection

<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript') 
<link rel="stylesheet" href="{{ asset("frontend/css/icomoon.css")}}">
<script src="{{ asset("plugins/ckeditor/ckeditor.js")}}"></script>
<script src="{{ asset("plugins/datetimepicker/jquery.datetimepicker.js")}}"></script>
<script src="{{ asset("plugins/bootstrap-datepicker/bootstrap-datepicker.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.date.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.time.js")}}"></script>
<script src="{{ asset("plugins/bootstrap-switch/bootstrap-switch.js")}}"></script>
<script src="{{ asset("plugins/bootstrap-progressbar/bootstrap-progressbar.js")}}"></script>
<script src="{{ asset("plugins/chosen/chosen.jquery.js")}}"></script>
<script src="{{ asset("plugins/chosen/chosenImage/chosenImage.jquery.js")}}"></script>


<script src="{{ asset("plugins/modal/js/classie.js")}}"></script>
<script src="{{ asset("plugins/modal/js/modalEffects.js")}}"></script>

<script src="{{ asset("plugins/bootstrap-wizard/jquery.bootstrap.wizard.js")}}"></script>
<script src="{{ asset("js/backend/js/ecommerce.js")}}"></script>
   <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function(){
       
$('#table_sreach_client').dataTable( {
              "lengthMenu": [ 5, 25, 50, 75, 100 ]
            });
$('#example').dataTable( {
              "lengthMenu": [ 5, 25, 50, 75, 100 ]
            });
    });
function myFunction() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "block") { 
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}
function showCar() {
    var v = document.getElementById("blockCar");
    if (v.style.display === "block") { 
        v.style.display = "none";
    } else {
        v.style.display = "block";
    }
}
</script>


@endsection