@extends('backend.layouts.app')

@section ('title', __('ตั้งค่าเว็บ'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection



@section('content')
 <div class="panel panel-default">
    <div class="panel-heading " style="height: 55px; color: #fff !important;background:#607d8b;" >
        <h3 class="panel-title" style="float:left;"><i class="fa fa-database"></i>&nbsp;</h3>
    </div>

    <div class="panel-body" style="padding: 15px;">
        <div class="row">
             <div class="col-md-12">
                 @if ($errors->any())
                  <div class="alert alert-danger" style="width:100%;">
                    <ul>
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
            </div>
            <!-- START CONTAINER -->
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Menus</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <br>
                                    <form id="form2" class="form-horizontal" data-parsley-validate>


                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Title</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control"  data-parsley-minlength="6">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Menu Type</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" data-parsley-minlength="6">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Description</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" data-parsley-minlength="6">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">status</label>
                                                <div class="col-sm-10">
                                                    <input type="checkbox" checked data-on-color="btn btn-success" data-off-color="danger" class="switch">
                                                </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="disabledInput" class="col-sm-2 control-label">Link</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" id="disabledInput" type="text" placeholder="Disabled input here..." disabled>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-8 col-sm-offset-4">
                                            <div class="pull-right">
                                                <button type="submit" class="btn btn-success" ><i class="fa fa-edit" style="color:white"></i> Save</button>
                                                <button type="reset" class="btn btn-default">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- END CONTAINER FLUID -->

                <br>
                <div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection


<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript')
<script type="text/javascript">
   
   
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
@endsection

@section('begin_css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.css" rel="stylesheet">
<style type="text/css">


</style>
@endsection