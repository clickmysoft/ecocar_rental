    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#sidebar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a id="menu-medium" class="sidebar-toggle tooltips">
                    <i class="fa fa-outdent"></i>
                </a>
                <a class="navbar-brand" href="#"><?php  print_r(Config::get('app.name')); ?></a>
            </div>
            <div class="navbar-center">@yield('title', app_name())</div>
            <div class="navbar-collapse collapse">
                <!-- BEGIN TOP NAVIGATION MENU -->
                <ul class="nav navbar-nav pull-right header-menu">
                    <!-- BEGIN LANG DROPDOWN -->
                    <li class="dropdown" id="user-header" >
                        <a href="#" class="dropdown-toggle c-white" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

                                    @if(strtoupper(app()->getLocale()) == 'TH')
                                        <img src="{{ asset("/backend/img/lang/th.png")}}" width="30" class="p-r-5">
                                    @endif

                                    @if(strtoupper(app()->getLocale()) == 'EN')
                                        <img src="{{ asset("/backend/img/lang/uk.png")}}" width="30" class="p-r-5">
                                    @endif

                                    <span class="username">{{ __('menus.language-picker.language') }} ({{ strtoupper(app()->getLocale()) }})</span>
                                    <i class="fa fa-angle-down p-r-10"></i>
                        </a>
                        
                        <ul class="dropdown-menu">
                            <li>
                                <ul class="dropdown-menu-list withScroll">
                                        @include('includes.partials.lang')
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <!-- END LANG DROPDOWN -->

                    <!-- BEGIN USER DROPDOWN -->
                    <li class="dropdown" id="user-header">
                        <a href="#" class="dropdown-toggle c-white" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img src="{{ asset($logged_in_user->user_image)}}" alt="user avatar" width="30" class="p-r-5">
                            <span class="username">{{ $logged_in_user->full_name }}</span>
                            <i class="fa fa-angle-down p-r-10"></i>
                        </a>
                        <ul class="dropdown-menu">

                            <li>
                                <a href="{{ route('frontend.auth.logout') }}">
                                     <i class="fa fa-power-off"></i> {{ __('navs.general.logout') }}
                                </a>
                            </li>
                           <!--  <li class="dropdown-footer clearfix">
                            <a href="javascript:;" class="toggle_fullscreen" title="Fullscreen">
                                <i class="glyph-icon flaticon-fullscreen3"></i>
                            </a>
                            <a href="lockscreen.html" title="Lock Screen">
                                <i class="glyph-icon flaticon-padlock23"></i>
                            </a>
                            <a href="{{ route('frontend.auth.logout') }}" title="{{ __('navs.general.logout') }}">
                                <i class="fa fa-power-off"></i>
                            </a> -->
                        </li>
                        </ul>
                    </li>
                    <!-- END USER DROPDOWN -->
              

                    

                </ul>
                <!-- END TOP NAVIGATION MENU -->
            </div>
        </div>
    </nav>
    <!-- END TOP MENU