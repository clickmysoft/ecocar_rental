<!-- BEGIN MAIN SIDEBAR -->
<style type="text/css">

</style>
<nav id="sidebar">
    <div id="main-menu">
        <ul class="sidebar-nav">
            <li>
                <div class="sidebar-profile clearfix" >
                    <a href="#" class="pull-left" style="padding: 0px;">
                        <figure class="profile-picture">
                            <img src="{{ asset($logged_in_user->user_image)}} " class="img-circle" alt="User Name">
                        </figure>
                    </a>
                    <h6 id="txt_welcome">{{ __('labels.welcome') }}<br><br>{{ $logged_in_user->full_name }}</h6>
                </div>
            </li>
            
            <li>
                <a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i><span class="sidebar-text">แดชบอร์ด</span></a>
            </li>
            @if ($logged_in_user->isAdmin())
            <li class="{{ active_class(Active::checkUriPattern('admin/auth*'), 'active current hasSub') }}">
                <a href="#">
                    <i class="fa fa-user"></i><span class="sidebar-text">{{ __('menus.backend.access.title') }}</span>
                        <span class="fa arrow"></span>
                        @if ($pending_approval > 0)
                            <span class="label label-danger pull-right w-300" style="margin-right: 8px;">{{ $pending_approval }}</span>
                        @endif
                </a>

                <ul class="submenu collapse">
                  
                            <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/auth/user'), 'active current hasSub') }}">
                                <a href="{{ route('admin.auth.user.index') }}" class="{{ active_class(Active::checkUriPattern('admin/auth/user'), 'active current hasSub') }}">
                                    <span class="sidebar-text">{{ __('menus.backend.access.users.all') }}</span>
                                </a>
                            </li>
                            <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/auth/user/create'), 'active current hasSub') }}">
                                <a href="{{ route('admin.auth.user.create') }}" class="{{ active_class(Active::checkUriPattern('admin/auth/user/create'), 'active current hasSub') }}">
                                    <span class="sidebar-text" >{{__('menus.backend.access.users.create')}}</span>
                                </a>
                            </li>
                            <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/auth/user/deactivated'), 'active current hasSub') }}">
                                <a href="{{ route('admin.auth.user.deactivated') }}" class="{{ active_class(Active::checkUriPattern('admin/auth/user/deactivated'), 'active current hasSub') }}"><span class="sidebar-text">{{ __('menus.backend.access.users.deactivated') }}</span></a>
                            </li>
                            <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/auth/user/deleted'), 'active current hasSub') }}" >
                                <a href="{{ route('admin.auth.user.deleted') }}" class="{{ active_class(Active::checkUriPattern('admin/auth/user/deleted'), 'active current hasSub') }}"><span class="sidebar-text">{{ __('menus.backend.access.users.deleted') }}</span></a>
                            </li>
                     

                    <?php
                        if(1==5){
                            //เมนูจัดการบทบาท
                    ?>
                              <li>
                                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/role*')) }}" href="{{ route('admin.auth.role.index') }}">
                                     <i class="fa fa-code"></i><span class="sidebar-text">{{ __('labels.backend.access.roles.management') }}</span>
                                </a>
                            </li>
                    <?php
                        }
                    ?>
                  


                </ul>
            </li>
            @endif

            <li class="{{ active_class(Active::checkUriPattern('admin/booking/*'), 'active current hasSub') }}">
                <a href="#"><i class="fa fa-book"></i><span class="sidebar-text">Booking</span><span class="fa arrow"></span></a>
                <ul class="submenu collapse">

                    <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/booking/car-booking'),'active current hasSub') }}">
                        <a href="{{ route('admin.booking.car-booking.index') }}" class="{{ active_class(Active::checkUriPattern('admin/booking/car-booking'), 'active current hasSub') }}"><span class="sidebar-text">รายการ Booking</span></a>
                    </li>


                    <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/booking/return-car'),'active current hasSub') }}">
                        <a href="{{ route('admin.booking.return-car.index') }}" class="{{ active_class(Active::checkUriPattern('admin/booking/return-car'), 'active current hasSub') }}"><span class="sidebar-text">คืนรถ</span></a>
                    </li>

                    <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/booking/deposit-car'),'active current hasSub') }}">
                        <a href="{{ route('admin.deposit.deposit-car.index') }}" class="{{ active_class(Active::checkUriPattern('admin/booking/deposit-car'), 'active current hasSub') }}"><span class="sidebar-text">ค่าจองรถ</span></a>
                    </li>

                </ul>
            </li>


            <li class="{{ active_class(Active::checkUriPattern('admin/customer/*'), 'active current hasSub') }}">
                <a href="{{ route('admin.car-management') }}"><i class="fa fa-user"></i><span class="sidebar-text">บริหารจัดการข้อมูลลูกค้า</span><span class="fa arrow"></span></a>
                <ul class="submenu collapse">
                   <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/customer/management'), 'active current hasSub') }}" >
                                <a href="{{ route('admin.customer.management.index') }}" class="{{ active_class(Active::checkUriPattern('admin/customer/management'), 'active current hasSub') }}"><span class="sidebar-text">รายการลูกค้า</span></a>
                            </li>

                            <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/customer/management/create'), 'active current hasSub') }}">
                                <a href="{{ route('admin.customer.management.create') }}" class="{{ active_class(Active::checkUriPattern('admin/customer/management/create'), 'active current hasSub') }}">
                                    <span class="sidebar-text">สร้างข้อมูล</span>
                                </a>
                            </li>
                </ul>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('admin/cars/*'), 'active current hasSub') }}">
                <a href="{{ route('admin.car-management') }}"><i class="fa fa-car"></i><span class="sidebar-text">บริหารจัดการรถยนต์</span><span class="fa arrow"></span></a>
                <ul class="submenu collapse">
                            <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/cars/car'), 'active current hasSub') }}" >
                                <a href="{{ route('admin.cars.car.index') }}" class="{{ active_class(Active::checkUriPattern('admin/cars/car'), 'active current hasSub') }} "><span class="sidebar-text">รายการรถยนต์</span></a>
                            </li>

                            <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/cars/car/create'), 'active current hasSub') }}">
                                <a href="{{ route('admin.cars.car.create') }}" class="{{ active_class(Active::checkUriPattern('admin/cars/car/create'), 'active current hasSub') }}">
                                    <span class="sidebar-text">สร้างข้อมูล</span>
                                </a>
                            </li>
                </ul>
            </li>

            @if ($logged_in_user->isAdmin())
                <li class="{{ active_class(Active::checkUriPattern('admin/prices/*'), 'active current hasSub') }}">
                        <a href="{{ route('admin.car-management') }}">
                            <i class="fa fa-money" aria-hidden="true"></i>
                            <span class="sidebar-text">กำหนดราคาการเช่า</span>
                            <span class="fa arrow"></span>
                        </a>
                
                        <ul class="submenu collapse">
                            <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/prices/car-prices'), 'active current hasSub') }}" >
                                <a href="{{ route('admin.prices.car-prices.index') }}" class="{{ active_class(Active::checkUriPattern('admin/prices/car-prices'), 'active current hasSub') }}"><span class="sidebar-text">รายการราคา</span></a>
                            </li>

                            <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/prices/car-prices/create'), 'active current hasSub') }}">
                                <a href="{{ route('admin.prices.car-prices.create') }}" 
                                class="{{ active_class(Active::checkUriPattern('admin/prices/car-prices/create'), 'active current hasSub') }}">
                                    <span class="sidebar-text">สร้างข้อมูล</span>
                                </a>
                            </li>
                        </ul>
                </li>
            @endif

          
            <li class="{{ active_class(Active::checkUriPattern('admin/articles/*'), 'active current hasSub') }}">
                <a href="#"><i class="fa fa-edit"></i><span class="sidebar-text">บริหารจัดการบทความ</span><span class="fa arrow"></span></a>
                <ul class="submenu collapse">
                    <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/articles/category/category-list*'),'active current hasSub') }}">
                        <a href="{{ route('admin.category.category-list.index') }}" class="{{ active_class(Active::checkUriPattern('admin/articles/category/category-list*'),'active current hasSub') }}"><span class="sidebar-text">หมวดหมู่บทความ</span></a>
                    </li>

                    <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/articles/tag/tag-list*'),'active current hasSub') }}">
                        <a href="{{ route('admin.tag.tag-list.index') }}" class="{{ active_class(Active::checkUriPattern('admin/articles/tag/tag-list*'),'active current hasSub') }}"><span class="sidebar-text">Tag บทความ</span></a>
                    </li>

                    <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/articles/articles*'),'active current hasSub') }}">
                        <a href="{{ route('admin.articles.articles.index') }}" class="{{ active_class(Active::checkUriPattern('admin/articles/articles*'),'active current hasSub') }}"><span class="sidebar-text">รายการบทความ</span></a>
                    </li>
                    
                </ul>
            </li>


            <li class="{{ active_class(Active::checkUriPattern('admin/promotions/*'), 'active current hasSub') }}" >
                <a href="#"><i class="fa fa-edit"></i><span class="sidebar-text">บริหารจัดการโปรโมชั่น</span><span class="fa arrow"></span></a>
                <ul class="submenu collapse">
                    <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/promotions/promotions'),'active current hasSub') }}">
                        <a href="{{ route('admin.promotions.promotions.index') }}" class="{{ active_class(Active::checkUriPattern('admin/promotions/promotions'),'active current hasSub') }}"><span class="sidebar-text">รายการโปรโมชั่น</span></a>
                    </li>
                    <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/promotions/promotions/create'),'active current hasSub') }}">
                        <a href="{{ route('admin.promotions.promotions.create') }}" class="{{ active_class(Active::checkUriPattern('admin/promotions/promotions/create'),'active current hasSub') }}"><span class="sidebar-text">สร้างโปรโมช่น</span></a>
                    </li>
                </ul>
            </li>


            <li class="{{ active_class(Active::checkUriPattern('admin/menu/*'), 'active current hasSub') }}" style="display:none;">
                <a href="#"><i class="fa fa-edit"></i><span class="sidebar-text">เมนู</span><span class="fa arrow"></span></a>
                <ul class="submenu collapse">
                    <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/menu/menu-type'),'active current hasSub') }}">
                        <a href="{{ route('admin.menu.menu-type.index') }}" class="{{ active_class(Active::checkUriPattern('admin/menu/menu-type'),'active current hasSub') }}"><span class="sidebar-text">ประเภทเมนู</span></a>
                    </li>
                    <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/menu/menu-items'),'active current hasSub') }}">
                        <a href="{{ route('admin.menu.menu-items.index') }}" class="{{ active_class(Active::checkUriPattern('admin/menu/menu-items'),'active current hasSub') }}"><span class="sidebar-text">สร้างเมนู</span></a>
                    </li>
                </ul>
            </li>



            <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'active current hasSub') }}">
                <a href="#"><i class="fa fa-edit"></i><span class="sidebar-text">{{ __('menus.backend.log-viewer.main') }}</span><span class="fa arrow"></span></a>
                <ul class="submenu collapse">
                    <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/log-viewer'),'active current hasSub') }}">
                        <a href="{{ route('log-viewer::dashboard') }}"" class="{{ active_class(Active::checkUriPattern('admin/promotions/promotions'),'active current hasSub') }}"><span class="sidebar-text">{{ __('menus.backend.log-viewer.dashboard') }}</span></a>
                    </li>
                    <li style="text-indent: 8px;" class="{{ active_class(Active::checkUriPattern('admin/log-viewer/logs*'),'active current hasSub') }}">
                        <a href="{{ route('log-viewer::logs.list') }}" class="{{ active_class(Active::checkUriPattern('admin/log-viewer/logs*'),'active current hasSub') }}"><span class="sidebar-text">{{ __('menus.backend.log-viewer.logs') }}</span></a>
                    </li>
                </ul>
            </li>

           

            <li>
                <a href="{{ route('frontend.index') }}" target="_blank"><i class="glyph-icon flaticon-frontend"></i><span class="sidebar-text">หน้าเว็บเพจ</span></a>
            </li>

           
            @if ($logged_in_user->isAdmin())
                <li class="m-b-245">  
                    <a href="{{ route('admin.config.webconfig.create') }}" ><i class="glyph-icon flaticon-padlock23"></i><span class="sidebar-text">ตั้งค่าเว็บไซต์</span></a>
                </li>
            @endif


            
          

        </ul>
    </div>
    <div class="footer-widget">

       
        <div class="sidebar-footer clearfix">
            <a class="pull-left" href="{!! route('frontend.auth.logout') !!}" data-rel="tooltip" data-placement="top" data-original-title="Settings"><i class="glyph-icon flaticon-settings21"></i></a>
            <a class="pull-left toggle_fullscreen" href="#" data-rel="tooltip" data-placement="top" data-original-title="Fullscreen"><i class="glyph-icon flaticon-fullscreen3"></i></a>
            <a class="pull-left" href="{!! route('frontend.auth.logout') !!}" data-rel="tooltip" data-placement="top" data-original-title="Lockscreen"><i class="glyph-icon flaticon-padlock23"></i></a>

            <a class="pull-left" data-rel="tooltip" data-placement="top" data-original-title="Logout" href="{!! route('frontend.auth.logout') !!}" title="{{ trans('navs.general.logout') }}">
                <i class="fa fa-power-off"></i>
            </a>

        </div>
    </div>
</nav>