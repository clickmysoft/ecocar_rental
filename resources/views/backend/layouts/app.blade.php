<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js sidebar-large"><!--<![endif]-->

@langrtl
    <html lang="{{ app()->getLocale() }}" dir="rtl">
@else
    <html lang="{{ app()->getLocale() }}">
@endlangrtl
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'Clickmycom')">
    <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
    @yield('meta')

    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
    @stack('before-styles')

    <!-- BEGIN MANDATORY STYLE -->
    <link href="{{ asset("css/backend/css/icons/icons.min.css")}}" rel="stylesheet">
    <link href="{{ asset("css/backend/css/bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{ asset("css/backend/css/plugins.min.css")}}" rel="stylesheet">
    <link href="{{ asset("css/backend/css/style.css")}}" rel="stylesheet">

    <link href="{{ asset("css/backend/css/colors/color-green.css")}}" rel="stylesheet" >
     <link rel="stylesheet" href="{{ asset("plugins/jnotify/jNotify.jquery.css")}}">

      <!-- select2 -->
    <link href="{{ asset("plugins/select2/select2.css")}}" rel="stylesheet">
    <!-- END  MANDATORY STYLE -->

    <!-- BEGIN PAGE LEVEL STYLE -->
    @yield('begin_css')
    <!-- BEGIN PAGE LEVEL STYLE -->
    <script src="{{ asset("plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js")}}"></script>
    <!-- Scripts -->
    @stack('before-scripts')
    {!! script(mix('js/backend.js')) !!}
    @stack('after-scripts')


    <style type="text/css">
        button#imageManager-1,button#imageManager-2,button#imageManager-3,button#imageManager-4,button#imageManager-5,button#imageManager-6,button#imageManager-7,button#imageManager-8,button#imageManager-9,button#imageManager-10,button#imageManager-11,button#imageManager-12,button#imageManager-13,button#imageManager-14,button#imageManager-15,button#imageManager-16,button#imageManager-17,button#imageManager-18,button#imageManager-19,button#imageManager-20 {
      display: none;
    }
    
    </style>
</head>

<?php  
    $class = '';
    $data_page = '';
    if(empty($set_body_page)){
        $class = config('backend.body_classes');
        $data_page = "blank_page";
    }else{
        $class = $set_body_page['class'];
        $data_page = $set_body_page['data-page'];
    }

?>
<body class="{{ $class }}"  data-page="{{ $data_page }}">

    @include('backend.includes.header')
    @include('backend.includes.sidebar')

    <!-- BEGIN WRAPPER -->
    <div id="wrapper">
 
        <div class="content-header">
            @yield('page-header')
        </div><!--content-header-->
       <!--  @include('includes.partials.messages') -->

        <!-- BEGIN MAIN CONTENT -->
          @include('includes.partials.logged-in-as')
           {!! Breadcrumbs::render() !!}
        <div id="main-content" >
          
               
                    @yield('content')
             
        </div>
        <!-- END MAIN CONTENT -->
       
    </div>
    <!-- END WRAPPER -->
    @include('backend.includes.footer')

    <!-- BEGIN MANDATORY SCRIPTS -->
    <script src="{{ asset("plugins/jquery-1.11.js")}}"></script>
    <script src="{{ asset("plugins/jquery-migrate-1.2.1.js")}}"></script>
    <script src="{{ asset("plugins/jquery-ui/jquery-ui-1.10.4.min.js")}}"></script>
    <script src="{{ asset("plugins/jquery-mobile/jquery.mobile-1.4.2.js")}}"></script>
    <script src="{{ asset("plugins/bootstrap/bootstrap.min.js")}}"></script>
    <script src="{{ asset("plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js")}}"></script>
    <script src="{{ asset("plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js")}}"></script>
    <script src="{{ asset("plugins/mmenu/js/jquery.mmenu.min.all.js")}}"></script>
    <script src="{{ asset("plugins/nprogress/nprogress.js")}}"></script>
    <script src="{{ asset("plugins/charts-sparkline/sparkline.min.js")}}"></script>
    <script src="{{ asset("plugins/breakpoints/breakpoints.js")}}"></script>
    <script src="{{ asset("plugins/numerator/jquery-numerator.js")}}"></script>
    <script src="{{ asset("plugins/jquery.cookie.min.js")}}" type="text/javascript"></script>
    <script src="{{ asset("plugins/jnotify/jNotify.jquery.min.js")}}"></script>
    <!-- select2 -->
<script src="{{ asset("plugins/select2/select2.min.js")}}"></script>
    <!-- END MANDATORY SCRIPTS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
   
    @yield('begin_javascript')
    <!-- END  PAGE LEVEL SCRIPTS -->
    
     <script src="{{ asset("js/backend/js/application.js")}}"></script>

<style type="text/css">
    
    button.swal2-cancel {
        margin-right: 5px;
    }
    button.swal2-confirm {
        margin-left: 5px;
    }

    div#main-content div[class='panel panel-default']{
        margin-bottom: 0px !important;
    }
</style>

<script type="text/javascript">
    jQuery(document).ready(function(){
        $('.select2').select2();
            jQuery("button[type='submit']").on('click', function() {
                var $this = jQuery(this);
                $this.attr('data-loading-text',"<i class='fa fa-spinner fa-spin'></i>&nbsp;กำลังประมวลผล");
                $this.button('loading');

                setTimeout(function(){
                    $this.button('reset');
                },2000);
            });
    });// End tag jQuery
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();  
        jQuery("a[class^='sidebar-toggle tooltips']").click(function(){
            jQuery("h6#txt_welcome").hide();
            jQuery("ul[class^='submenu collapse in']").removeClass('in');
            if(jQuery(this).attr('id')== 'menu-large'){
                jQuery("h6#txt_welcome").show();
                jQuery(".profile-picture").show();
                jQuery("ul.sidebar-nav > li:first").show();

            }else{
                jQuery(".profile-picture").hide();
                jQuery("ul.sidebar-nav > li:first").hide();
            }
        });
    });
</script>

</body>
</html>
