@extends('backend.layouts.app')

@section ('title', __('ตั้งค่าเว็บ'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection



@section('content')
 <div class="panel panel-default">
    <div class="panel-heading " style="height: 55px; color: #fff !important;background:#607d8b;" >
        <h3 class="panel-title" style="float:left;"><i class="fa fa-database"></i>&nbsp;</h3>
    </div>

    <div class="panel-body" style="padding: 15px;">
        <div class="row">
             <div class="col-md-12">
                 @if ($errors->any())
                  <div class="alert alert-danger" style="width:100%;">
                    <ul>
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
            </div>
            <!-- START CONTAINER -->
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">add&nbsp;<strong>Menus</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <br>
                                        <form id="form2" class="form-horizontal" data-parsley-validate>


                                            <div class="form-group">
                                                <label class="col-sm-2 col-md-3 control-label">Title:</label>
                                                <div class="col-sm-10 col-md-9">
                                                    <input type="text" name="title" class="form-control"  data-parsley-minlength="6">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-md-3 control-label">Alias:</label>
                                                <div class="col-sm-10 col-md-9">
                                                    <input type="text" name="alias" class="form-control"  data-parsley-minlength="6" placeholder="Auto-generate from title" autocomplete="off" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-md-3 control-label">Target Window:</label>
                                                <div class="col-sm-10 col-md-9">
                                                    <input type="text" name="template_style_id" class="form-control"  data-parsley-minlength="6" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-md-3 control-label">Status:</label>
                                                    <div class="col-sm-10 col-md-9">
                                                        <input type="checkbox" name="status" checked data-on-color="btn btn-success" data-off-color="danger" class="switch">
                                                    </div>
                                            </div>
                                        </form>
                                </div>
                                <div class="col-md-6">
                                    <br>
                                        <form id="form2" class="form-horizontal" data-parsley-validate>
                                            <!-- <div class="form-group">
                                                <label for="disabledInput" class="col-sm-2 col-md-4 control-label">Link:</label>
                                                <div class="col-sm-10 col-md-8">
                                                    <input class="form-control" id="disabledInput" type="text" placeholder="Disabled input here..." disabled>
                                                </div>
                                            </div>
                                            <fieldset disabled> -->
                                            <div class="form-group">
                                                <label class="col-sm-2 col-md-4 control-label">Menu Type:</label>
                                                <div class="col-sm-10 col-md-8">
                                                    <select class="form-control" name="type">
                                                        <option value="">-Selete Menu Type-</option>
                                                        <option value="Mustard">Mustard</option>
                                                        <option value="Ketchup">Ketchup</option>
                                                        <option value="Relish">Relish</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-md-4 control-label">Parent Item:</label>
                                                <div class="col-sm-10 col-md-8">
                                                    <select class="form-control" name="parent_id">
                                                        <option value="">-Selete Parent Item-</option>
                                                        <option value="Mustard">Mustard</option>
                                                        <option value="Ketchup">Ketchup</option>
                                                        <option value="Relish">Relish</option>
                                                    </select>
                                                </div>
                                            </div> 
                                            <div class="form-group">
                                                <label class="col-sm-2 col-md-4 control-label">Secondary Item:</label>
                                                <div class="col-sm-10 col-md-8">
                                                    <select class="form-control" name="parent_id">
                                                        <option value="">-Selete Secondary Item-</option>
                                                        <option value="Mustard">Mustard</option>
                                                        <option value="Ketchup">Ketchup</option>
                                                        <option value="Relish">Relish</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group">
                                                <label class="col-sm-2 col-md-4 control-label">Image:</label>
                                                <div class="col-sm-10 col-md-8">
                                                    <input type="text" name="img" class="form-control" data-parsley-minlength="6">
                                                </div>
                                            </div> -->
                                            <div class="form-group">
                                                <label class="col-sm-2 col-md-4 control-label">Note:</label>
                                                <div class="col-sm-10 col-md-8">
                                                    <textarea class="form-control" rows="1" id="comment"></textarea>
                                                </div>
                                            </div>
                                        </form>
                                            
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-8 col-sm-offset-4">
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-success" ><i class="fa fa-edit" style="color:white"></i> Save</button>
                                            <button type="reset" class="btn btn-default">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>           
                        </div>
                    </div>
                </div>
                    
                <!-- END CONTAINER FLUID -->
            </div>
        </div>
    </div>
@endsection


<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript')
<script type="text/javascript">
   
   
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

@endsection

@section('begin_css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css">
<style type="text/css">


</style>
@endsection