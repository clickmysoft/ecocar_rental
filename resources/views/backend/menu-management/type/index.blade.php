@extends ('backend.layouts.app')

@section ('title', __('รายการลูกค้าทั้งหมด'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('begin_css')

<link rel='stylesheet' type='text/css' href='https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css'/>
<style type="text/css">
    input[type="search"] {
    -webkit-appearance: textfield;
}
</style>
@endsection

@section('content')
  <div class="panel panel-default">
                        <div class="panel-heading" style="color: #fff !important;color: #fff !important;background: #60868b;padding: 10px 15px;">
                            <h3 class="panel-title" style="float:left;padding: 10px 0 !important; margin: 0 !important;"><i class="fa fa-database"></i>&nbsp;ตารางแสดงรายการประเภทเมนู</h3>
                            <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                                    <a href="{{ route('admin.menu.menu-type.create') }}" class="btn btn-success ml-1" data-toggle="tooltip" title="" style="    float: right;" data-original-title="Create New"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;สร้างประเภทเมนู</a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red filter-right">
                                                    
                                    <table id="DataTable" class="table table-striped table-hover table-dynamic" >
                                            <thead>
                                                <tr>
                                                    <th class="col-md-1" style="padding-left: 0;">ลำดับ</th>
                                                    <th class="col-md-1" style="padding-left: 0;text-align:center;">ชื่อเมนู</th>
                                                    <th class="col-md-2" style="padding-left: 0;text-align:center;">ประเภทเมนู</th>
                                                    <th class="col-md-3" style="padding-left: 0;text-align:center;">คำอธิบาย</th>
                                                    <th class="col-md-2" style="padding-left: 0;text-align:center;">menu sortable</th>
                                                    <th class="col-md-1" style="padding-left: 0;text-align:center;">วันที่ สร้างเมนู</th>
                                                    <th class="col-md-1" style="padding-left: 0;text-align:center;">สถานะ</th>
                                                    <th class="col-md-1" style="padding-left: 0;text-align:center;">{{ __('labels.general.actions') }}</th>
                                                </tr>
                                            </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
        <div class="row">
            <div class="col-5">
                <div class="float-right">
                </div>
            </div><!--col-->
        </div><!--row-->
@endsection
<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
   
    <!--END CODING JS-->


    <!--START IMPORT EVENT FORM-->
    @include('includes.forms.ajax_delete')
    <!--END IMPORT EVENT FORM-->

@endsection
