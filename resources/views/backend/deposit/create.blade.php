@extends ('backend.layouts.app')

@section ('title', __('สร้างรายละเอียด เงินมัดจำ'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection


@section('content')

<?php
// echo "<pre>";
// print_r($booking);
// echo "</pre>";

?>

<div class="panel panel-default">
    <div class="panel-heading " style="height: 55px; color: #fff !important;background:#00a788;">
        <h3 class="panel-title" style="float:left;"><i class="fa fa-database"></i>&nbsp;บันทึกรับค่าจองรถ</h3>
    </div>

    <div class="panel-body">
        <div class="row">

            <form method="POST" action="{{ route('admin.deposit.deposit-car.update',$booking->id) }}" enctype="multipart/form-data" class="form-horizontal" id="my-form">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-group">
                        <div class="panel bd-t-red">
                            @if ($errors->any())
                                <div class="alert alert-danger" style="width:100%;">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="fa fa-gear"></i>&nbsp;<strong>สร้างรายละเอียด&nbsp;</strong>บันทึกรับเงินมัดจำ</h3>
                            </div>

                            <div class="row column-seperation">
                                    <div class="col-md-6">
                                        <table class="w3-table-all">
                                            <tr>
                                                <td>ข้อมูลลูกค้า</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="detail">ชื่อ-สกุล:</td>
                                                <td><?php print_r($booking->customer_first_name); ?>&nbsp;<?php print_r($booking->customer_last_name); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="detail">อีเมลล์:</td>
                                                <td><?php print_r($booking->customer_email); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="detail">โทรศัพท์:</td>
                                                <td><?php print_r($booking->customer_phone); ?></td>
                                            </tr>
                                        </table>
                                        <div  style="display:none;">
                                            <div class="form-group">
                                                <div style="text-align: -webkit-right;" class="col-sm-2" for="email"><p>ชื่อบริษัท:</p></div>
                                                <div class="col-sm-10"><p><?php print_r($booking->company_name); ?></p></div>
                                            </div>
                                            <div class="form-group">
                                                <div style="text-align: -webkit-right;" class="col-sm-2" for="email"><p>หน่วยงานสังกัด:</p></div>
                                                <div class="col-sm-10"><p><?php print_r($booking->branch_name); ?></p></div>
                                            </div>
                                            <div class="form-group">
                                                <div style="text-align: -webkit-right;" class="col-sm-2" for="email"><p>ชื่อ-สกุล:</p></div>
                                                <div class="col-sm-10"><p><?php print_r($booking->customer_first_name); ?>&nbsp;<?php print_r($booking->customer_last_name); ?></p></div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <div style="text-align: -webkit-right;" class="col-sm-2" for="email"><p>อีเมลล์:</p></div>
                                                <div class="col-sm-10"><p><?php print_r($booking->customer_email); ?></p></div>
                                            </div>
                                            <div class="form-group">
                                                <div style="text-align: -webkit-right;" class="col-sm-2" for="email"><p>โทรศัพท์:</p></div>
                                                <div class="col-sm-10"><p><?php print_r($booking->customer_phone); ?></p></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <table class="w3-table-all">
                                            <tr>
                                                <td>ข้อมูลการเช่า</td>
                                                <td></td>
                                              </tr>
                                              <tr>
                                                <td class="detail">Invoice:</td>
                                                <td><?php print_r($booking->title); ?></td>
                                              </tr>
                                            <tr>
                                                <td class="detail">ชื่อบริษัท:</td>
                                                <td><?php print_r($booking->company_name); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="detail">สาขา:</td>
                                                <td><?php print_r($booking->branch_name); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="detail">ยี่ห้อ:</td>
                                                <td><?php print_r(strtoupper($booking->car_brand)); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="detail">รุ่น:</td>
                                                <td><?php print_r(strtoupper($booking->car_generation)); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="detail">รหัสรถประจำสาขา:</td>
                                                <td><?php print_r($booking->branch_car_code); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="detail">ทะเบียนรถ:</td>
                                                <td><?php print_r($booking->license_plate); ?>&nbsp;<?php print_r($booking->license_province); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="detail">จำนวนเงิน:</td>
                                                <td><?php print_r(number_format($booking->invoice_price)); ?>&nbsp;บาท</td>
                                            </tr>
                                        </table> 

                                        <div style="display:none;">  
                                            <div class="form-group">
                                                    <div style="text-align: -webkit-right;" class="col-sm-2" for="email"><p>รายการ Invoice:</p></div>
                                                    <div class="col-sm-10"><p><?php print_r($booking->title); ?><p></div>
                                            </div>
                                            <div class="form-group">
                                                    <div style="text-align: -webkit-right;" class="col-sm-2" for="email"><p>ยี่ห้อ:</p></div>
                                                    <div class="col-sm-10"><p><?php print_r($booking->car_brand); ?></p></div>
                                            </div>
                                            <div class="form-group">
                                                    <div style="text-align: -webkit-right;" class="col-sm-2" for="email"><p>รุ่น:</p></div>
                                                    <div class="col-sm-10"><p><?php print_r($booking->car_generation); ?></p></div>
                                            </div>
                                            <div class="form-group">
                                                    <div style="text-align: -webkit-right;" class="col-sm-2" for="email"><p>ทะเบียนรถ:</p></div>
                                                    <div class="col-sm-10"><p><?php print_r($booking->license_plate); ?>&nbsp;<?php print_r($booking->license_province); ?></p></div>
                                            </div>
                                            <div class="form-group">
                                                    <div style="text-align: -webkit-right;" class="col-sm-2" for="email"><p>จำนวนเงิน:</p></div>
                                                    <div class="col-sm-10"><p><?php print_r(number_format($booking->invoice_price)); ?>&nbsp;บาท<p></div>
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            <br>

                            


                            
                             <div style="display:none;"> class="form-group <?=$errors->has('type_rental_prices')!=null?'has-error':''?>">
                                <label class="control-label col-sm-2" for="email">กำหนดวัน - เวลา:</label>
                                <div class="col-sm-5">
                                        <select  name="status_event" class="form-control " style="height: 36.5px;">
                                          <option value="" <?=old('status_event')==''?' selected':''?>>เลือกสถานะ ปิด/เปิด</option>
                                            <option value="close" selected <?=old('status_event')=='close'?' selected':''?>>ปิด</option>
                                          <option value="close" <?=old('status_event')=='close'?' selected':''?>>ปิด</option>
                                          <option value="open" <?=old('status_event')=='open'?' selected':''?>>เปิด</option>
                                        </select>

                                        @if($errors->has('status_event'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('status_event') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            </div>

                            <div class="status_open"  style="<?=old('status_event')=='open'?'display:block;':'display:none;'?>">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" >
                                      วันเริ่มใช้งาน:
                                    </label>
                                    <div class="col-sm-10" >
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </span>
                                            <input class=" form-control pickadate set_event"  name="date_price_start" type="text" value="<?=old('price_start_date')?>">
                                        </div>

                                        @if($errors->has('date_price_start'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('date_price_start') }} </strong>
                                            </span>
                                        @endif

                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="control-label col-sm-2" >
                                      เวลาเริ่มใช้งาน:
                                    </label>
                                    <div class="col-sm-10" >
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-clock-o"></i>
                                            </span>
                                            <input class="pickatime form-control set_event" type="text" name="time_price_start" value="<?=old('time_price_start')?>">
                                        </div>

                                        @if($errors->has('time_price_start'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('time_price_start') }} </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                
                                <div class="form-group">
                                    <label class="control-label col-sm-2" >
                                      วันสิ้นสุดการใช้งาน:
                                    </label>
                                    <div class="col-sm-10" >
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </span>
                                            <input class=" form-control pickadate set_event"  name="date_price_end" type="text" value="<?=old('date_price_end')?>">
                                        </div>

                                        @if($errors->has('date_price_end'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('date_price_end') }} </strong>
                                            </span>
                                        @endif

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2" >
                                      เวลาสิ้นสุดใช้งาน:
                                    </label>
                                    <div class="col-sm-10" >
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-clock-o"></i>
                                            </span>
                                            <input class="pickatime form-control set_event" type="text" name="time_price_end" value="<?=old('time_price_end')?>">
                                        </div>

                                        @if($errors->has('time_price_end'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('time_price_end') }} </strong>
                                            </span>
                                        @endif

                                    </div>
                                </div>
                            </div>

                            
                            <!-- <div class="form-group" style="display:none;">
                                <label class="control-label col-sm-2" for="email">เงินมัดจำ:</label>ราคา
                                <div class="col-sm-10">
                                    <select class="form-control" name="amount" style="height:40px;">
                                        <option value="">เลือกจำนวนเงินมัดจำ</option>
                                        <option value="">500</option>
                                        <option value="">1,000</option>
                                    </select>
                                </div>
                                <div class="col-sm-5">
                                    <div class="input-group ">
                                        <span class="input-group-addon">
                                          <i class="fa fa-dashboard"></i>
                                        </span>
                                        <input  class="form-control" type="number" name="car_prices" placeholder="ระบุราคาเช่า">
                                         <span class="input-group-addon txt_type_rental">
                                          บาท
                                        </span>
                                    </div>

                                    @if($errors->has('car_prices'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('car_prices') }} </strong>
                                            </span>
                                    @endif

                                </div>
                            </div> -->

                            
                            <!-- <div class="form-group">
                                    <label class="control-label col-sm-2" for="pwd">
                                            สถานะการทำงาน:
                                    </label>
                                    <div class="col-sm-5">        
                                          <select class="form-control chosen-img" name="status" style="height:40px;">
                                            <option value="enabled">เปิดการใช้งาน</option>
                                            <option value="closed">ปิดการใช้งาน</option>
                                          </select>

                                        @if($errors->has('status'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('status') }} </strong>
                                                </span>
                                        @endif

                                    </div>
                            </div> -->
                            
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="email">การโอนเงิน:</label><!-- ราคา -->
                                <div class="col-sm-10">
                                    <select class="form-control" name="deposit_amount" style="height:40px;">
                                        <option value="500">จ่ายเงินจอง ({{ENV('PRICE_OF_PRE_BOOK_CAR')}} บาท)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                    <label class="control-label col-sm-2" for="pwd">
                                        อัพโหลดภาพหลักฐานการจ่ายเงิน:
                                    </label>
                                    <div class="col-sm-10">        
                                    <?php
                                            if($booking->deposit_image != null && file_exists(public_path('booking/deposit/'.$booking->deposit_image))){
                                            ?>
                                              <img style="max-width: 350px;" src="<?=asset('booking/deposit/'.$booking->deposit_image)?>">
                                            <?php
                                              }
                                            ?>
                                        <div class="input-group">

                                            <span class="input-group-btn">

                                                <span class="btn btn-default btn-file">
                                                    Browse… <input type="file" id="imgInp" name="deposit_image">
                                                </span>
                                            </span>
                                            <input type="text" class="form-control" readonly>
                                        </div>
                                        <img id='img-upload'/>
                                          
                                    </div>
                            </div>

                             <div class="form-group">
                                    <label class="control-label col-sm-2" for="pwd">วันที่ชำระ:</label>
                                    <div class="col-sm-5">        
                                        <!-- <input type="date" class="form-control" name="deposit_pay_date" value="<?=$booking->deposit_pay_date?>"> -->
                                        <div class="wrap-input100 content-pic">
                                            <input class=" form-control pickadate"  name="deposit_pay_date" type="text" value="<?=$booking->deposit_pay_date?>">
                                        </div>
                                    </div>
                            </div>

                            <div class="form-group">
                                    <label class="control-label col-sm-2" for="pwd">เวลาที่ชำระ:</label>
                                    <div class="col-sm-5">        
                                        <!-- <input type="time" class="form-control" name="deposit_pay_time" value="<?=$booking->deposit_pay_time?>"> -->
                                        <div class="wrap-input100 content-pic">
                                            <div class="form-group label-floating is-empty" style="margin-bottom: 0px;">
                                                <input type="text" id="time" name="deposit_pay_time" class="form-control" data-dtp="dtp_3fsbI" value="<?=$booking->deposit_pay_time?>">
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="email">หมายเหตุ:</label>
                                <div class="col-sm-10">
                                    <textarea name="deposit_remark" class="form-control textarea_question" rows="10" cols="80"><?=$booking->deposit_remark?></textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-12">
                                    <ul class="list-inline" style="float:right;">
                                        <li><button type="submit" class="btn btn-success "><i class="fa fa-save"></i>&nbsp;บันทึก</button></li>
                                        <li><a class="btn btn-warning prev-step" href="{{ route('admin.auth.user.index') }}"><i class="fa fa-times"></i>&nbsp;ยกเลิก</a></li>
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                </div>

                <input type="hidden" name="car_booking_code" value="{{$booking->car_booking_code}}">
            </form>
        </div>
    </div>
</div>


@endsection


<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript') 
    <script src="{{ asset("plugins/ckeditor/ckeditor.js")}}"></script>

    <script src="{{ asset("plugins/bootstrap-datepicker/bootstrap-datepicker.js")}}"></script>
    <script src="{{ asset("plugins/pickadate/picker.js")}}"></script>
    <script src="{{ asset("plugins/pickadate/picker.date.js")}}"></script>
    <script src="{{ asset("plugins/pickadate/picker.time.js")}}"></script>
    <script src="{{ asset("plugins/bootstrap-progressbar/bootstrap-progressbar.js")}}"></script>
    <script src="{{ asset("vendorto/daterangepicker/moment.min.js")}}"></script>
    <script src="{{ asset("material-datetime/bootstrap-material-datetimepicker.js")}}"></script>

    <script src="{{ asset("plugins/chosen/chosen.jquery.js")}}"></script>
    <script src="{{ asset("plugins/chosen/chosenImage/chosenImage.jquery.js")}}"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset("vendor/jsvalidation/js/jsvalidation.js")}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\Backend\Prices\CarPricesReques','#my-form'); !!}

    <script type="text/javascript">
   
        $(document).ready(function(){
            $('#time').bootstrapMaterialDatePicker({ 
                date: false,
                format: 'HH:mm',
                themes: 'dark-mint-gradient'
            });
        });
    </script>

    <script>
        $(document).ready( function() {
            $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
            });

            $('.btn-file :file').on('fileselect', function(event, label) {
                
                var input = $(this).parents('.input-group').find(':text'),
                    log = label;
                
                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }
            
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    
                    reader.onload = function (e) {
                        $('#img-upload').attr('src', e.target.result);
                    }
                    
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#imgInp").change(function(){
                readURL(this);
            });     
        });
    </script>

    <script>
            CKEDITOR.replace('deposit_remark',{
               toolbar: [
                  { name: 'tools', items: [ 'Maximize'] },
                  { name: 'insert', items: ['Format' ] },
                  { name: 'document', items: [ '-', 'Undo', 'Redo' ] },
                  { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
                  { name: 'insert', items: ['Table', 'HorizontalRule', 'SpecialChar' ] },
                  { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] },
                  { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
                  { name: 'colors', groups: [ 'colors' ] },
                ]
            });
    </script>

    <script type="text/javascript">
        jQuery(document).ready(function(){

            $.ajaxSetup({
                    headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
            });

            //ยี่ห่อเปลี่ยน
            jQuery("select[name='car_brand']").change(function(){
                var brand_name = jQuery(this).val();

                jQuery("select#set_generation").empty().append('<option >กำลังโหลด...</option>');
                jQuery("select#set_generation").trigger("chosen:updated");
                
                jQuery.post("{{ route('admin.cars.generation') }}", {brand: brand_name}, function(result){
                    if(result){
                            setTimeout(
                                function(){
                                        jQuery("select#set_generation option:first-child").text('เลือกรุ่น');
                                        jQuery("select#set_generation").append(result);
                                        jQuery("select#set_generation").trigger("chosen:updated");
                                },300
                            );
                    }else{
                        jQuery("select#set_generation").empty().append('<option >ไม่พบอะไรกับเขาหร๊อก...</option>');
                        jQuery("select#set_generation").trigger("chosen:updated");
                    }

                });
            });


            jQuery("select[name='type_rental_prices']").change(function(){

                var data = jQuery("select[name='type_rental_prices']").val();
                var txt = '';
                if(data == 'per_day'){
                    txt = '/ต่อวัน';
                }
                if(data == 'per_month'){
                    txt = '/ต่อเดือน';
                }

                if(data == 'per_year'){
                    txt = '/ต่อปี';
                }
                jQuery("span.txt_type_rental").text('บาท '+txt);
            });

            jQuery("select[name='status_event']").change(function(){

                var status = jQuery(this).val();
                if(status == 'close'){
                    jQuery("div[class^='status_open']").slideUp();
                    jQuery("input.set_event").val('');
                }
                if(status == 'open'){
                    jQuery("div[class^='status_open']").slideDown();
                }
            });


        });// End tag jQuery

    </script>

    @if(session()->has('message'))

        <script type="text/javascript">
            <?php
                $type = session()->get('message');
                $message = __('alerts.backend.database.'.$type.'');
            ?>

            <?=$type?>(
                    '<i class="fa fa-check-square-o" style="color:#fff;padding-right:8px"></i><?=$message?>', {
                        HorizontalPosition: 'right',
                        VerticalPosition: 'top',
                        ShowOverlay: false,
                        TimeShown: 5000,
                        MinWidth:400
            });
        </script>
    @endif

@endsection


@section('begin_css')
    <link href="{{ asset("plugins/datetimepicker/jquery.datetimepicker.css")}}" rel="stylesheet">
    <link href="{{ asset("plugins/pickadate/themes/default.css")}}" rel="stylesheet">
    <link href="{{ asset("plugins/pickadate/themes/default.date.css")}}" rel="stylesheet">
    <link href="{{ asset("plugins/pickadate/themes/default.time.css")}}" rel="stylesheet">
    <link href="{{ asset("backend/dependentdrop/dependentdrop.css")}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset("plugins/chosen/chosen.css")}}" />
    <link rel="stylesheet" href="{{ asset("plugins/chosen/chosenImage/chosenImage.css")}}"/>
    <link rel="stylesheet" href="{{ asset("plugins/jquery-steps/jquery.steps.css")}}" />

<link rel="stylesheet" href="{{ asset("material-datetime/bootstrap-material-datetimepicker.css")}}" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <style type="text/css">
        td.detail{
            text-align: -webkit-right;
            width: 30%;
        }
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }

        #img-upload{
            width: 50%;
        }

        .chosen-single{
            line-height: 32px !important;
            height: 32px !important;
        }

        div.payan-chana ul.chosen-choices{
                height: 33px !important;
        }

        div#select_number ul.chosen-results li{
             background-size: 0px !important;
        }

        div#brand_cars ul.chosen-results li:first-child{
            background-size: 0px !important;
            background-position-x: 12px;
        }

        div#brand_cars ul.chosen-results li{
            background-size: 27px !important;
            text-indent: 7px;
            padding-top: 6px;
            -webkit-backface-visibility: hidden;
            -webkit-transform: translateZ(0) scale(1.0, 1.0);
        }
       
        div#car_type ul.chosen-results li{
            background-size: 40px !important;
            text-indent: 26px;
        }
        div#car_type ul.chosen-results li:first-child{
            background-size: 0px !important;
            background-position-x: 12px;
        }

        div#total_human ul.chosen-results li:first-child{
            background-size: 0px !important;
        }

        div#total_bag ul.chosen-results li:first-child{
            background-size: 0px !important;
        }
        div.fule_type > div.ui-checkbox{
                margin-top: 0;
        }
        div.fule_type > div.ui-checkbox label{
               font-size: 14px !important;
        }


        .registration-ui {
         /*   background: linear-gradient(to bottom, #f8d038 0%,#f5ca2e 100%);*/
           /* padding: .25em 1em .25em 1.75em;*/
           /*padding:0 .7em 1.14em .75em;
            font-weight: bold;
            font-size: 2em;
            border-radius: 5px;
            border: 1px solid #000;
            box-shadow: 1px 1px 1px #ddd;
            position: relative;
            font-family: helvetica, ariel, sans-serif;*/
            padding: 12.4px .7em 0.7em .75em;
            font-weight: bold;
            font-size: 2em;
            border-radius: 5px;
            border: 1px solid #000;
            box-shadow: 1px 1px 1px #ddd;
            position: relative;
            font-family: helvetica, ariel, sans-serif;
        }

        .registration-ui:before {
            /*content: 'ECOCAR';
            display: block;
            width: 30px;
            height: 100%;
            background: #063298;
            position: absolute;
            top: 0;
            border-radius: 5px 0 0 5px;
            color: #f8d038;
            font-size: .5em;
            line-height: 85px;
            padding-left: 5px;*/
            content: 'ECOCAR';
            display: block;
            text-align: center;
            width: 100%;
            height: 18px;
            background: #159077;
            position: absolute;
            top: 46.1px;
            border-radius: 5px 0 0 5px;
            color: #ffffff;
            font-size: .4em;
            line-height: 19px;
            left: 0px;
            border-bottom-right-radius: 4px;
            border-top-left-radius: 0;
            border-bottom-left-radius: 4px;
        }

        .registration-ui:after {
            content: '';
            display: block;
            position: absolute;
            top: 7px;
            left: 5px;
            width: 20px;
            height: 20px;
            border-radius: 30px;
           /* border: 1px dashed #f8d038;*/
        }

        .panel-body {
           padding-bottom: 0;
        }
    </style>
@endsection
