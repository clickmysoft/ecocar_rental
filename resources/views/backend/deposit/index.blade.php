@extends ('backend.layouts.app')

@section ('title', __('รายการลูกค้าทั้งหมด'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('begin_css')

<link rel='stylesheet' type='text/css' href='https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css'/>

<style type="text/css">
    table#DataTable > tbody >  tr > td{
         text-align: center;
         vertical-align: initial;
    }

    table#DataTable > tbody >  tr > td:nth-child(2){
          text-align: left;
    }

</style>
@endsection

@section('content')
  <div class="panel panel-default">
                        <div class="panel-heading" style="color: #fff !important;color: #fff !important;background: #60868b;padding: 10px 15px;">
                            <h3 class="panel-title" style="float:left;padding: 10px 0 !important; margin: 0 !important;"><i class="fa fa-database"></i>&nbsp;ตารางแสดงรายการเงินมัดจำ</h3>
                            <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                                    
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red filter-right">
                                    <table id="DataTable" class="table table-striped table-hover table-dynamic" >
                                            <thead>
                                                <tr>
                                                                        <th class="col-md-2" style="text-align: center;">ชื่อ - สกุล</th>
                                                                        <th class="col-md-1">โทรศัพท์</th>
                                                                        <th class="col-md-1" style="text-align: center;">อีเมล</th>
                                                                        <th class="col-md-1" style="font-size: 12px;">สถานที่รับรถ</th>
                                                                        <th class="col-md-1" style="font-size: 12px;">สถานที่คืนรถ</th>
                                                                        <th class="col-md-1" style="font-size: 12px;">วันที่รับรถ</th>
                                                                        <th class="col-md-1" style="font-size: 12px;">วันที่คืนรถ</th>
                                                                        <th class="col-md-1" style="font-size: 12px;">ชำระเงินจอง</th>
                                                                        <th class="col-md-1" style="padding-left: 0; text-align: center;">{{ __('labels.general.actions') }}</th>
                                                                    </tr>
                                            </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
        <div class="row">
            <div class="col-5">
                <div class="float-right">
                </div>
            </div><!--col-->
        </div><!--row-->
@endsection
<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    
    <!--START CODING JS-->
    <script>
            $(document).ready(function () {

                //////// DataTable ///////
                var table = $('#DataTable').DataTable({
                    "lengthMenu": [[10,25,50, -1], [10,25,50, "All"]],
                    "processing": true,
                    "serverSide": true,
                    "ajax": "{{ route('ajaxdata.getdata.deposit.cars') }}",
                    "order": [[ 0, "desc" ]],
                    "language": {
                        "lengthMenu": "แสดงผล _MENU_ ข้อมูลต่อหน้า",
                        "zeroRecords": "Nothing found - sorry",
                         "info": "ลำดับหน้าที่ _PAGE_ จาก _PAGES_ หน้า - จำนวนรายการทั้งหมด _TOTAL_ รายการ",
                        "infoEmpty": "No records available",
                        "infoFiltered": "( ค้นหาข้อมูลจาก _MAX_ รายการ )",
                        "search": "<i class='fa fa-search'></i> ค้นหาข้อมูล:",
                        "paginate": {
                            "first":      "หน้าแรก",
                            "last":       "หน้าสุดท้าย",
                            "next":       "ถัดไป",
                            "previous":   "ก่อนหน้า"
                        },
                    },
                    "columns":[
                            {data: 'name'},
                            {data: 'customer_phone'},
                            {data: 'customer_email'},
                            {data: 'pick_up'},
                            {data: 'drop_off'},
                            {data: 'pick_up_date'},
                            {data: 'drop_off_date'},
                            {data: 'deposit'},
                            {data: 'action'}
                    ],
                    
                }); // End DataTable //
                
            }); //End jQuery
    </script>
    <!--END CODING JS-->

    <!--START IMPORT EVENT FORM-->
    @include('includes.forms.ajax_delete')
    <!--END IMPORT EVENT FORM-->

@endsection
