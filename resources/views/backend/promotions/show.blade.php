@extends('backend.layouts.app')

@section('content')
<div class="panel panel-default">
	<!-- Panel Heading -->
	<div class="panel-heading" style="    background-color: #00a788 !important;    color: #fff !important;">
		รายละเอียดโปรโมชั่น
	</div>
	<!-- Panel body -->
	<div class="panel-body">


		<div class="course_header">

			<div id="item-header" role="complementary">

				<div class="row">
					<div class="col-md-12">

						<div id="item-header-content">



							<h1>{{ $data->title }}</h1>


						</div>
						<!-- #item-header-content -->

					</div>

				</div>

				<div class="row">
					<div class="col-md-9">

						<div id="item-header-avatar">
							<img width="100%" height="100%" src="<?=asset('promotions/'.$data->image_full.'') ?>" class="attachment-full size-full wp-post-image"></a>                        
						</div>
						<!-- #item-header-avatar -->
					</div>

					<div class="col-md-3">
						<div class="widget pricing" id="course-pricing">
							<div href="#" class="course_button full button code"><span class="course_price">รหัสโปโมชั่น</span>{{ $data->promotion_code}}</div>
							<div class="course_details">
								<ul>
									<li class="course_time"><i class="fa fa-ticket"></i>ส่วนลด<br><span class="code_detail">{{ $data->promotion_discount}} {{ $data->discount_type}}</span></li>

									<li class="course_badge"><i class="fa fa-calendar-check-o" style="color:#309434;"></i> วันเริ่มโปรโมชั่น<br><span class="code_detail">{{ $data->start_date}} </span></li>
									<li class="course_certificate"><i class="fa fa-calendar-times-o" style="color:red;"></i> วันสิ้นสุดโปรโมชั่น<br><span class="code_detail">{{ $data->end_date}}</span></li>

								</ul>
							</div>
							<div class="course_sharing"></div>
						</div>
						<div id="item-admins">

							<h5>สถานะ</h5>
							<div class="instructor_course status-border">

								<p>
									<svg width="27px" height="27px" viewBox="0 0 77 77">
										<?php
										if($data->active == 'enabled'){
											?>
											<path fill="#6AC259" d="M38.5,0C17.238,0,0,17.237,0,38.5S17.238,77,38.5,77C59.764,77,77,59.763,77,38.5S59.764,0,38.5,0z
											M31.438,58.276L14.485,41.325l5.65-5.65l11.302,11.302l25.427-25.428l5.65,5.65L31.438,58.276z"/>
											<span class="status-text">เปิดใช้งาน</span>
											<?php
										}else{
											?>
											<ellipse fill="#E21B1B" cx="38.5" cy="38.5" rx="38.5" ry="38.475"/>
											<g>
												<rect x="34.292" y="17.016" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 92.9483 38.4932)" fill="#FFFFFF" width="8.42" height="42.961"/>
												<rect x="17.019" y="34.284" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 92.9426 38.4903)" fill="#FFFFFF" width="42.962" height="8.421"/>
											</g>
											<span class="status-text">ปิดใช้งาน</span>
											<?php
										}
										?>
									</svg></p>

								</div>
							</div>
							<!-- #item-actions -->
						</div>
					</div>

				</div>
				<!-- #item-header -->

			</div>



			<div class="row contentpromo">
				<div class="col-md-12">
					<div id="item-body">
						<div class="course_description" id="course-home">

							<div class="small_desc">
								<?=$data->description ?>
							</div>
						</div>    
					</div>
				</div>
			</div>

		</div>
	</div>
	@endsection



	<!-- ใส่ CSS -->
	@section('begin_css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
	.status-text{
		position: absolute;
		margin: 5px 0px 0px 15px;
	}
	.status-border{
		width: 100%;
		background: #fff;
		border-radius: 4px;
		padding: 10px 10px 0px 10px;
		border: 1px #d2d2d2 solid;
		text-align: left;
	}

	.course_details li i {
		float: right;
		font-size: 20px;
		line-height: 1;
		margin-right: 10px;
		font-style: normal;
	}
	.block.courseitem.course3 .block_content .course_price {
		font-size: 16px;
		margin: 10px 0;
		color: #70c989;
		font-weight: 600;
		display: inline-block;
		width: 100%;
	}
	.block.courseitem.course3 .block_content .block_title {
		padding: 0 20px 0 0;
		margin: 0;
		font-size: 16px;
		line-height: 1.2;
		border: none;
		min-height: 64px;
	}
	.block.courseitem .block_content .block_title {
		font-weight: 600;
		padding: 15px 0;
		border-bottom: 1px solid rgba(0,0,0,.08);
	}
	.block h4.block_title {
		margin: 10px 0 0;
		font-size: 18px;
		font-weight: 600;
	}
	.block.courseitem.course3 .block_content .course_instructor img {
		border-radius: 50%;
		width: 65px;
		border: 2px solid #FFF;
	}
	.block .block_media img {
		border-radius: 2px;
		width: 100%;
	}
	.block.courseitem.course3 .block_content {
		padding: 15px 15px 10px;
		position: relative;
	}

	.block.courseitem .block_content {
		display: inline-block;
		background: #FFF;
		width: 100%;
		padding: 0 15px 15px;
		border: 1px solid rgba(0,0,0,.08);
		border-bottom-left-radius: 2px;
		border-bottom-right-radius: 2px;
	}
	.block .block_content {
		position: relative;
		line-height: 1.4;
	}
	.block.courseitem.course3 .block_content .course_instructor {
		position: absolute;
		right: 0;
		top: -60px;
		border-radius: 50%;
	}

	.block.courseitem .course_instructor {
		float: left;
	}
	.course_instructor {
		margin: 0;
		font-size: 13px;
		text-transform: uppercase;
		font-weight: 600;
	}
	.block.courseitem.course3 {
		border: 1px solid #EEE;
		border-radius: 2px;
	}

	.block.courseitem {
		margin-bottom: 30px;
	}
	.heading {
		margin: 20px 0;
		clear: both;
		font-size: 16px;
		padding: 8px 0;
		font-weight: 600;
		text-transform: uppercase;
		border-bottom: 3px solid rgba(0,0,0,.08);
	}
	.contentpromo{
		margin-top: 30px;
	}
	.students_undertaking {
		border: 1px solid rgba(0,0,0,.08)!important;
		background: #FFF;
		display: block;
		padding: 30px;
		margin: 0 0 30px;
		text-align: center;
		border: none;
		border-radius: 4px;
		width: 100%;
	}
	.course_instructor {

		padding: 15px;
		margin: 0;
		font-size: 13px;
		text-transform: uppercase;
		font-weight: 600;
	}
	#item-admins {
		margin: 0;
		padding-bottom: 20px;
	}
	#item-admins .item-avatar img {
		width: 40px;
		height: 40px;
		border-radius: 50%;
	}
	#item-admins .item-avatar {
		float: left;
		margin-top: 0px;
		margin-right: 20px;
		margin-bottom: 0px;
		margin-left: 0px;
		border-radius: 50%;
	}
	.instructor_course {
		padding-left: 15px;
	}
	.course_excerpt {
		margin: 15px 0 30px;
		font-size: 14px;
	}
	.course_header {
		background-color: #FAFAFA;
		padding: 30px;
	}
	.widget.pricing {
		background: #FFF;
		padding: 15px;
		border: 1px solid rgba(0,0,0,.08);
		color: #444444;
	}
	.course_button.button {
		padding: 1em .1em!important;
		background-color: #fff;
		width: 100%;
		border: 2px solid;
		text-align: center;
		border-color: #57CA85;
	}
	.button:hover {
		background: #57CA85;
		color: #FFF;
		background-image: none;
	}
	.course_button>span {
		display: block;;
		font-weight: 400;
	}
	.widget .course_details>ul>li.course_price>strong {
		padding-top: 8px;
		color: #70c989;
		display: inline-block;
	}
	.single-course.c5 .course_header #item-header .widget.pricing .course_details li {
		line-height: 2;
	}

	.widget .course_details>ul>li.course_price {
		min-height: 40px;
		font-size: 16px;
		padding: 0;
		text-transform: uppercase;
	}
	.widget .course_details li {
		margin-top: 7px;
		padding: 6px 0;
		position: relative;
		font-size: 15px;
		color: #909090;
	}
	.widget ul li {
		width: 100%;
		padding: 8px 0;
		display: inline-block;
		border-bottom: 1px dotted rgba(0,0,0,.08);
		font-size: 14px;
	}
	.course_details li {
		font-size: 12px;
		font-weight: 600;
		text-transform: uppercase;
		border-bottom: 1px solid #ddd;
	}
	.single-course.c5 .course_header #item-header .widget.pricing .course_details li {
		line-height: 2;
	}

	.course_time {
		margin-top: 30px;
		font-family: sans-serif;
		text-transform: uppercase;
	}
	.button {
		padding: 10px 24px;
		display: inline-block;
		margin: 10px 10px 10px 0;
		background-color: #78C8Ce;
		border: none;
		border-radius: 2px;
		letter-spacing: .1em;
		position: relative;
		-webkit-transition: all .3s;
		transition: all .3s;
		z-index: 1;
	}
	.code{
		font-size: 30px;
		font-weight: 800;
	}
	.code>span {
		font-size: 13px;
		font-weight: 600;
	}
	.col-md-3 .widget+.widget {
		clear: both;
		display: inline-block;
		width: 100%;
	}

	.widget+.widget {
		margin-top: 30px;
	}

	.ol, .ul {
		margin: 0;
		padding: 0;
		list-style: none;
	}
	.postsmall .post_thumb a {
		padding: 0;
		border: 2px solid #232b2d;
		display: inline-block;
		max-width: 100%;
	}
	.certificate, .certificate p, .postsmall .post_title {
		margin: 0;
	}
	.post_thumb{

		margin-bottom: 21px;
	}
	.code_detail{
		color: #70c989;
		font-size: 24px;
	}
	</style>
	@endsection


	<!-- ใส่ Javascript -->
	@section('begin_javascript')


	@endsection