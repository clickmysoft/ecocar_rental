@extends ('backend.layouts.app')

@section ('title', __('รายการโปรโมชั่น'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('begin_css')

<link rel='stylesheet' type='text/css' href='https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css'/>
@endsection

@section('content')
<div class="panel panel-default">
    <!-- Panel Heading -->
    <div class="panel-heading" style="color: #fff !important;color: #fff !important;background: #60868b;padding: 10px 15px;">
        <h3 class="panel-title" style="float:left;padding: 10px 0 !important; margin: 0 !important;"><i class="fa fa-database"></i>&nbsp;ตารางแสดงรายการโปรโมชั่น</h3>
        <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
            <a href="<?=route('admin.promotions.promotions.create')?>" class="btn btn-success ml-1" data-toggle="tooltip" title="" style="    float: right;" data-original-title="Create New"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;สร้างโปรโมชั่น</a>
        </div>
    </div>
    <!-- Panel body -->
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red filter-right">
                <table id="DataTable" class="table table-striped table-hover table-dynamic">
                    <thead>
                        <tr>
                            <th class="col-md-1">ลำดับ</th>
                            <th class="col-md-2" style="text-align: center;">หัวข้อ</th>
                            <th class="col-md-1">โปรโมชั่นcode</th>
                            <th class="col-md-1">ส่วนลด</th>
                            <th class="col-md-1" style="font-size: 12px;">อัตตราส่วนลด</th>
                            <th class="col-md-1" style="font-size: 12px;">วันเริ่มโปรโมชั่น</th>
                            <th class="col-md-1" style="font-size: 12px;">วันสิ้นสุดโปรโมชั่น</th>
                            <th class="col-md-1" style="padding-left: 0;text-align:center;">สถานะ</th>
                            <th class="col-md-1" style="padding-left: 0;text-align:center;">สร้างเมื่อ</th>
                            <th class="col-md-2" style="padding-left: 0;text-align: center;">{{ __('labels.general.actions') }}</th>
                        </tr>
                    </thead>
            </table>

        </div>


    </div>
</div>
</div>
@endsection


<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    
    <!--START CODING JS-->
    <script>
            $(document).ready(function () {

                //////// DataTable ///////
                var table = $('#DataTable').DataTable({
                    "lengthMenu": [[10,25,50, -1], [10,25,50, "All"]],
                    "processing": true,
                    "serverSide": true,
                    "ajax": "{{ route('ajaxdata.getdata.article.promotion') }}",
                    "order": [[ 0, "desc" ]],
                    "language": {
                        "lengthMenu": "แสดงผล _MENU_ ข้อมูลต่อหน้า",
                        "zeroRecords": "Nothing found - sorry",
                         "info": "ลำดับหน้าที่ _PAGE_ จาก _PAGES_ หน้า - จำนวนรายการทั้งหมด _TOTAL_ รายการ",
                        "infoEmpty": "No records available",
                        "infoFiltered": "( ค้นหาข้อมูลจาก _MAX_ รายการ )",
                        "search": "<i class='fa fa-search'></i> ค้นหาข้อมูล:",
                        "paginate": {
                            "first":      "หน้าแรก",
                            "last":       "หน้าสุดท้าย",
                            "next":       "ถัดไป",
                            "previous":   "ก่อนหน้า"
                        },
                    },
                    "columns":[
                            {data: 'id'},
                            {data: 'title'},
                            {data: 'promotion_code'},
                            {data: 'promotion_discount'},
                            {data: 'discount_type'},
                            {data: 'start_date'},
                            {data: 'end_date'},
                            {data: 'created_at'},
                            {data: 'status'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                    ],
                    
                }); // End DataTable //
                
            }); //End jQuery
    </script>
    <!--END CODING JS-->

    <!--START IMPORT EVENT FORM-->
    @include('includes.forms.ajax_delete')
    <!--END IMPORT EVENT FORM-->

@endsection