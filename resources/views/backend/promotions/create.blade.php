@extends('backend.layouts.app')


@section('content')
<div class="panel panel-default">
  <!-- Panel Heading -->
<div class="panel-heading " style="height: 55px; color: #fff !important;background:#607d8b;">
        <h3 class="panel-title" style="float:left;"><i class="fa fa-database"></i>&nbsp;สร้างโปโมชั่น</h3>
    </div>
  <!-- Panel body -->
  <div class="panel-body">
    <div class="row">
     
        <form method="POST" action="{{ route('admin.promotions.promotions.store') }}" enctype="multipart/form-data" class="form-horizontal" id="my-form">
          {{ csrf_field() }}
            <div class="form-group">
                <div class="panel bd-t-red">
                            @if ($errors->any())
                                <div class="alert alert-danger" style="width:100%;">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                     <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="fa fa-gear"></i>&nbsp;<strong>สร้างรายละเอียด&nbsp;</strong>โปรโมชั่น</h3>
                     </div>

                    <div class="form-group"> 
                      <label class="col-md-2 col-form-label text-right" for="exampleFormControlFile1" style="display: inherit; ">เลือกภาพโปรโมชั่น (ขนาดย่อ 360X270)</label>
                      <div class="col-md-4" >
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="fa fa-file-image-o"></i>
                          </span>
                          <input class=" form-control" name="image_thumbnail" type="file">
                        </div>
                      </div>
                    </div>


                    <div class="form-group"> 
                      <label class="col-md-2 col-form-label text-right" for="exampleFormControlFile1" style="display: inherit; ">เลือกภาพโปรโมชั่น (ขนาดขนาดเต็ม)</label>
                      <div class="col-md-4" >
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="fa fa-file-image-o"></i>
                          </span>
                          <input class=" form-control" name="image_full" type="file">
                        </div>
                      </div>
                    </div>

                    <div class="form-group" >
                      <label for="title" class="col-md-2 col-form-label text-right">หัวข้อ</label>
                      <div class="col-md-10" >
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="fa fa-font" aria-hidden="true"></i>
                          </span>
                          <input type="text" class="form-control" name="title" id="" value="" placeholder="" />
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="short-description" class="col-md-2 col-form-label text-right">คำอธิบายอย่างสั้น</label>
                      <div class="col-md-10" >
                        <textarea name="description" id="txtMessage" class="ckeditor" cols="45" rows="5" name="short_des_pro"></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="short-description" class="col-md-2 col-form-label text-right">โปรโมชั่นcode</label>
                      <div class="col-md-10">
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="fa fa-sort-amount-asc" aria-hidden="true"></i>
                          </span>
                          <input type="text" class="form-control" id="" value="" placeholder="" name="promotion_code" />
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="short-description" class="col-md-2 col-form-label text-right">ส่วนลด</label>
                      <div class="col-md-10">
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="fa fa-minus" aria-hidden="true"></i>
                          </span>
                          <input type="text" class="form-control" id="" value="" placeholder="" name="promotion_discount"/>
                        </div>
                      </div>
                    </div>   
                    <div class="form-group">
                      <label for="short-description" class="col-md-2 col-form-label text-right rate-discount">อัตตราส่วนลด</label> 
                      <div class="col-md-4">

                        <select class="form-control chosen-img"  style="height:40px;" name="discount_type">
                         <option value="bath" data-img-src="{{ asset("backend/img/bank-icon.svg")}}">บาท</option>
                         <option value="percen" data-img-src="{{ asset("backend/img/percent2-icon.png")}}">เปอร์เซ็น</option>11
                       </select>
                     </div>
                   </div>

                   <div class="form-group">
                    <label for="short-description" class="col-md-2 col-form-label text-right">วันเริ่มโปรโมชั่น</label>
                    <div class="input-group">
                      <div class="col-md-10">

                       <div class="input-group">
                        <span class="input-group-addon">
                         <i class="fa fa-calendar"></i>
                       </span>
                       <input class=" form-control pickadate" name="start_date" type="text" value="<?=old('start_date')?>" style="width: 200px;">
                     </div>

                   </div>
                 </div> 
               </div>


               <div class="form-group">
                <label for="short-description" class="col-md-2 col-form-label text-right">วันสิ้นสุดโปรโมชั่น</label>
                <div class="input-group">
                  <div class="col-md-10">

                    <div class="input-group">
                      <span class="input-group-addon">
                       <i class="fa fa-calendar"></i>
                     </span>
                     <input class=" form-control pickadate" name="end_date" type="text" value="<?=old('end_date')?>" style="width: 200px;">
                   </div>


                 </div>
               </div> 
             </div>

             <div class="bd-t-red">
              <div class="panel-heading">
                <h3 class="panel-title">
                  <i class="fa fa-gears"></i>Optimize SEO</h3>
                </div>
                <div class="form-group" >
                  <label for="title" class="col-md-2 col-form-label text-right">Meta title</label>
                  <div class="col-md-10" >
                    <div class="input-group">
                      <span class="input-group-addon">
                        <i class="fa fa-font" aria-hidden="true"></i>
                      </span>
                      <input type="text" class="form-control" id="" value="" placeholder="" name="mata_title" />
                    </div>
                  </div>
                </div>

                <div class="form-group" >
                  <label for="title" class="col-md-2 col-form-label text-right">Meta Description</label>
                  <div class="col-md-10" >
                    <div class="input-group" style="width:100%;">
                       <textarea name="mata_description" class="mata_description" rows="10" cols="80"><?=old('mata_description');?></textarea>
                    </div>
                  </div>
                </div>

                <div class="form-group" >
                  <label for="title" class="col-md-2 col-form-label text-right">Meta Keywords</label>
                  <div class="col-md-10" >
                    <div class="input-group" style="width: 100%;"> 
                       <textarea name="mata_keyword" class="mata_keyword" rows="10" cols="80"><?=old('mata_keyword');?></textarea>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-sm-2 text-right" for="pwd">
                    สถานะการทำงาน:
                  </label>
                  <div class="col-sm-10">        
                    <select class="form-control chosen-img" name="status" style="height:50px;">
                      <option <?=old('status')=='enabled'?'selected':'';?> data-img-src="{{ asset("backend/img/checked.svg")}}" value="enabled">เปิดการใช้งาน</option>
                      <option <?=old('status')=='closed'?'selected':'';?> data-img-src="{{ asset("backend/img/multiply.svg")}}" value="closed">ปิดการใช้งาน</option>
                    </select>
                  </div>
                </div>

              </div>   

              <div class="form-group">
                <div class="col-sm-12">
                  <ul class="list-inline" style="float:right;">
                    <li><button type="submit" class="btn btn-success "><i class="fa fa-save"></i>&nbsp;บันทึก</button></li>
                    <li><a class="btn btn-warning prev-step" href="{{ route('admin.auth.user.index') }}"><i class="fa fa-times"></i>&nbsp;ยกเลิก</a></li>
                  </ul>
                </div>
              </div>    


                </div>
             </div>
  </form>

</div>
</div>
</div>
@endsection


@section('begin_css')
<!-- BEGIN PAGE LEVEL STYLE PROMOTION -->
<link href="{{ asset("plugins/datetimepicker/jquery.datetimepicker.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.date.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.time.css")}}" rel="stylesheet">
<link href="{{ asset("backend/dependentdrop/dependentdrop.css")}}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset("plugins/chosen/chosen.css")}}" />
<link rel="stylesheet" href="{{ asset("plugins/chosen/chosenImage/chosenImage.css")}}"/>
<link rel="stylesheet" href="{{ asset("plugins/wizard/wizard.css")}}" />
<link rel="stylesheet" href="{{ asset("plugins/jquery-steps/jquery.steps.css")}}" />

<style type="text/css">

  .chosen-search {
    display: none;
  }
  .chosen-single {
    height: 36px !important;
    padding-top: 5px !important;
  }
</style>

@endsection


@section('begin_javascript')
<script src="{{ asset("plugins/ckeditor/ckeditor.js")}}"></script>
<script src="{{ asset("plugins/bootstrap-datepicker/bootstrap-datepicker.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.date.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.time.js")}}"></script>
<script src="{{ asset("plugins/bootstrap-switch/bootstrap-switch.js")}}"></script>
<script src="{{ asset("plugins/bootstrap-progressbar/bootstrap-progressbar.js")}}"></script>


<script src="{{ asset("plugins/chosen/chosen.jquery.js")}}"></script>
<script src="{{ asset("plugins/chosen/chosenImage/chosenImage.jquery.js")}}"></script>

<script type="text/javascript" src="{{ asset("vendor/jsvalidation/js/jsvalidation.js")}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\Backend\Promotion\PromotionRequest','#my-form'); !!}


<script type="text/javascript">


  jQuery(document).ready(function(){
    jQuery(".chosen-img,.chosen-select").chosenImage();

    jQuery("#file-1").change(function(){
      readURL(this);
    });
  });

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#blah').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

</script>


@if(session()->has('message'))

    <script type="text/javascript">
        <?php
            $type = session()->get('message');
            $message = __('alerts.backend.database.'.$type.'');
        ?>

        <?=$type?>(
                '<i class="fa fa-check-square-o" style="color:#fff;padding-right:8px"></i><?=$message?>', {
                    HorizontalPosition: 'right',
                    VerticalPosition: 'top',
                    ShowOverlay: false,
                    TimeShown: 5000,
                    MinWidth:400
        });
    </script>
@endif

@endsection