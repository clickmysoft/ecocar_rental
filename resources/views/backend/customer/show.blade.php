@extends ('backend.layouts.app')

@section ('title', __('รายการลูกค้าทั้งหมด'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('begin_css')
  <link rel='stylesheet' href="{{ asset("plugins/dhtmlxScheduler/dhtmlxscheduler.css")}}" rel="stylesheet">
  <link rel='stylesheet' href="{{ asset("plugins/dhtmlxScheduler/styles.css")}}" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">


<link rel='stylesheet' type='text/css' href='https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css'/>


<style>
.ks-cover {
background-color: #ccc;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 160px
}
.ks-user {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    -webkit-box-pack: justify;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between;
    padding-left: 30px;
    padding-right: 30px;
    margin-top: -50px;
}
.profil{
    padding-top: 13px;
    font-size: 30px;
    font-weight: bold;
}
.ks-actions {
    margin-top: 15px;
  }  

 .ks-statistics {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    margin-top: 21px;
}
.ks-item {
    text-align: center;
}
.ks-amount {
    color: #25628f;
    font-size: 28px;
    font-weight: 600;
}
.ks-text {
    font-size: 13px;
}


.panel {
    border: solid 1px #dee0e1;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    border: solid 1px #dee0e1;
    background: #fff;
}
.card {
    border-radius: 2px;
    border: solid 1px #dee0e1;
}
.space-top{
padding-top: 20px;
    margin-top: 23px;
    border-top: 1px #e8e8e8 dashed;
}
.status-border{
        width: 80%;
      background: #f9f9f9;
    border-radius: 4px;
    padding: 10px 10px 0px 10px;
    border: 1px #d2d2d2 solid;
   text-align: left;
  }

.box-insurance{
    text-align: left;
    margin-bottom: 8px;
    width: 90%;
    border-radius: 6px;
    padding: 8px 10px 13px 10px;
    background: #f9f9f9;
    border: 1px #e6e6e6 solid;
  }
.box-insuranced :hover{
    margin-bottom: 8px;
    width: 90%;
    border-radius: 6px;
    padding: 8px 10px 13px 10px;
    background: #f9f9f9;
    border: 2px #dfdfdf solid;
  }

[class^="icon"] {
  width: 22px;
  height: 22px;
  position: relative;
  top: 5px;
  margin-right: 10px;
  fill: #168f76;
}

.icon-style {
    padding-left: 2px;
    padding-right: 10px;
    position: relative;
    top: 3px;
    font-size: 1.8em;
}

.icon-style-check{
    padding-left: 2px;
    padding-right: 10px;
    position: relative;
    top: 3px;
    font-size: 1.8em;
    color: #6AC259;
}

.btn:hover{
    background: #137863;
    color: #fff;
    border: 1px #9E9E9E solid;
}
.headpargraph{
    font-weight: 800;
    color: #3379b7;
}
.headpargraph span{
        font-weight: 400;
        color: #000;
}
.number{
    margin-top: -10px;
    font-size: 21px;
    font-weight: 600;
    letter-spacing: 1px;
}
.status-text{
    position: absolute;
    margin: 5px 0px 0px 15px;
}

@media only screen and (max-width: 800px) {
    
    /* Force table to not be like tables anymore */
    #no-more-tables table, 
    #no-more-tables thead, 
    #no-more-tables tbody, 
    #no-more-tables th, 
    #no-more-tables td, 
    #no-more-tables tr { 
        display: block; 
    }
 
    /* Hide table headers (but not display: none;, for accessibility) */
    #no-more-tables thead tr { 
        position: absolute;
        top: -9999px;
        left: -9999px;
    }
 
    #no-more-tables tr { border: 1px solid #ccc; }
 
    #no-more-tables td { 
        /* Behave  like a "row" */
        border: none;
        border-bottom: 1px solid #eee; 
        position: relative;
        padding-left: 50%; 
        white-space: normal;
        text-align:left;
    }
 
    #no-more-tables td:before { 
        /* Now like a table header */
        position: absolute;
        /* Top/left values mimic padding */
        top: 6px;
        left: 6px;
        width: 45%; 
        padding-right: 10px; 
        white-space: nowrap;
        text-align:left;
        font-weight: bold;
    }
 
    /*
    Label the data
    */
    #no-more-tables td:before { content: attr(data-title); }

}

.panel-body{
  padding: 7px !important;
}
</style>
@endsection

@section('content')


<div class="panel panel-default">
    <div class="panel-heading" style="color: #fff !important;color: #fff !important;background: #60868b;padding: 10px 15px;">
        <h3 class="panel-title" style="float:left;padding: 10px 0 !important; margin: 0 !important;"><i class="fa fa-database"></i>&nbsp;ข้อมูลลูกค้า</h3>
        <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups"></div>
    </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red filter-right">
                    <!-- BEGIN PROFIL SIDEBAR -->
                   
                    <!-- First Section --> 
                    <div class="ks-cover" style='background-image: url("{{ asset('backend/img/avatars/banner-2.jpg') }}")'></div>


                     <div class="row">
                          <div class="col-md-3 ks-user">
                             <div class="row ks-info">
                                 <div class="col-md-12">
                                    <?php
                                      if(!empty($customer->image_profile)){
                                         echo "<img src='".asset("customers/".$customer->image_profile)."' alt='รูปโปรไฟล์' width='167' height='167' >";
                                      }else{
                                         echo "<img src='".asset("customers/default/image_profile.png")."' alt='รูปโปรไฟล์' width='167' height='167'>";
                                      }
                                    ?>
                                </div>
                             </div>
                          </div>  
                     
                          <div class="col-md-5">

                            <div class="profil">{{ $customer->prefix_name_customer }}{{ $customer->first_name_customer }} {{ $customer->last_name_customer }}</div>  
                            <p class="headpargraph">บริษัท / ที่ทำงาน : <span>{{ $customer->company_name }}</span></p>
                          </div>  
                          
                          <div class="col-md-4 ks-statistics" style="display:none;">
                            <div class="row">
                              <div class="ks-item col-md-6" style="padding-left: 10px;text-align: left;">
                                  <div class="ks-amount">102 <small>&nbsp;ครั้ง</small></div>
                                  <div class="ks-text">จำนวนครั้งที่เข้าใช้บริการ</div>
                              </div>
                              <div class="ks-item col-md-6" >
                                  <div class="ks-amount">13/05/2561</div>
                                  <div class="ks-text">วันที่ใช้บริการที่ผ่านมา</div>
                              </div>
                            </div> 
                          </div> 
                    </div>
                    <!-- End First Section --> 
                    <!-- Second Section --> 
                    <div class="col-md-12 space-top">
                         <div class="row">
                            <div class="col-md-3">
                                <p class="headpargraph">ชื่อเล่น: <span>{{ $customer->nick_name_customer}}</span></p>
                                <p class="headpargraph">อีเมลล์: <span>{{ $customer->email_customer}}</span></p>
                                <p class="headpargraph">เบอร์โทรศัพท์มือถือ: <span>{{ $customer->phone_customer}}</span></p>
                                <p class="headpargraph">Line ID: <span>{{ $customer->line_id_customer}}</span></p>
                            </div>
                            <div class="col-md-5">
                                <p class="headpargraph">หมายเลขบัตรประชาชน:</p>
                                <p class="number">{{ $customer->id_card_customer}}</p>
                                <p class="headpargraph">ที่อยู่ตามบัตรประชาชน:</p>
                                <?=$customer->personal_card_address?>
                            </div>
                            <div class="col-md-4">
                                <p class="headpargraph">ที่อยู่ปัจจุบัน:</p>   
                              <?=$customer->present_address?>
                            </div>
                         </div> 
                    </div>
                    <!-- End Second Section --> 
                    <!-- Third Section --> 
                    <div class="col-md-12 space-top">
                         <div class="row">
                            <div class="col-md-3">
                                <p class="headpargraph">ชื่อบริษัทหรือชื่อที่ทำงาน:<span>
                              
                                <?php
                                $company = config('company.'.app()->getLocale().'.name.'.$customer->company_name)[0];
                                if($company != null){
                                    echo $company;
                                }else{
                                    echo $customer->company_name;
                                }
                                ?>
                                </p>
                                
                                <p class="headpargraph">ตำแหน่งงาน: <span>
                                
                                <?php
                                $position = config('company.'.app()->getLocale().'.position.'.$customer->position)[0];
                                
                                if($position != null){
                                    echo $position;
                                }else{
                                    echo $customer->position;
                                }
                                
                                
                                ?>
                                     
                                     
                                     
                                </p>
                                <p class="headpargraph">เบอร์โทรที่ทำงาน: <span> {{ $customer->tel_office}}</p>
                            </div>
                            <div class="col-md-5">
                                <p class="headpargraph">จุดประสงค์การใช้งานรถยนต์:</p> 
                                <?= $customer->explain_use_car?>
                            </div>
                            <div class="col-md-4">
                                <p class="headpargraph">จังหวัดที่ใช้รถ:</p>
                                <p class="number">{{ $customer->province_use_car}}</p>
                            </div> 
                         </div>
                    </div>
                    <!-- End Third Section --> 
                    <!--  Fourth  Section --> 
                    <div class="col-md-12 space-top">
                         <div class="row">
                            <div class="col-md-3">
                                <div class="headpargraph">ผู้รับผลประโยชน์ จากประกันอุบัติเหตุ<br>หรือบุคคลที่สามารถติดต่อได้ยามฉุกเฉิน<br>(เน้นเป็นพ่อแม่หรือญาติสนิท)</div>
                               <br>
                                <p class="headpargraph">ชื่อ:  <span>{{ $customer->prefix_name_beneficiary}}{{ $customer->first_name_beneficiary}} {{ $customer->last_name_beneficiary}} </span> </p>
                                <p class="headpargraph">ความสำพันธ์: <span>{{ $customer->relation_beneficiary}}</span></p>
                                <p class="headpargraph">เบอร์โทร:  <span>{{ $customer->phone_beneficiary}}</span></p>
                            </div>
                            <div class="col-md-2">
                                <div class="headpargraph">เอกสารที่ต้องใช้สำหรับเช่ารถ </div>
                                <ul style="margin-top: 20px;">
                                        <li class="btn box-insurance" data-toggle="modal" data-target="#modal-large">
                                        <i class="fas fa-file-alt icon-style"></i>
                                                  บัตรประชาชน 
                                        </li>
                                        <li class="btn box-insurance" data-toggle="modal" data-target="#modal-large-2">
                                        <i class="fas fa-file-alt icon-style"></i>
                                                  ใบขับขี่ 
                                        </li>
                                         <li class="btn box-insurance" data-toggle="modal" data-target="#modal-large-3">
                                        <i class="fas fa-file-alt icon-style"></i>
                                                  บัตรเคดิต 
                                        </li>
                                </ul>
                            </div>


                            <div class="col-md-3">
                                <div class="headpargraph"> กำหนดรายละเอียดเพิ่มเติม</div>
                                <p class="headpargraph">รายละเอียดเพิ่มเติม:</p>
                                <?= $customer->customers_description?>
                            </div>

                            <div class="col-md-4">
                              
                                        <p class="headpargraph">สถานะ</p>
                                        <div class="col-md-12  status-border">
                                                      <p>
                                                        <svg width="27px" height="27px" viewBox="0 0 77 77">
                                                      <?php
                                                        if($customer->status == 'enabled'){
                                                      ?>
                                                          <path fill="#6AC259" d="M38.5,0C17.238,0,0,17.237,0,38.5S17.238,77,38.5,77C59.764,77,77,59.763,77,38.5S59.764,0,38.5,0z
                                                          M31.438,58.276L14.485,41.325l5.65-5.65l11.302,11.302l25.427-25.428l5.65,5.65L31.438,58.276z"/>
                                                          <span class="status-text">เปิดใช้งาน</span>
                                                      <?php
                                                        }else{
                                                        ?>
                                                          <ellipse fill="#E21B1B" cx="38.5" cy="38.5" rx="38.5" ry="38.475"/>
                                                          <g>
                                                          <rect x="34.292" y="17.016" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 92.9483 38.4932)" fill="#FFFFFF" width="8.42" height="42.961"/>
                                                          <rect x="17.019" y="34.284" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 92.9426 38.4903)" fill="#FFFFFF" width="42.962" height="8.421"/>
                                                          </g>
                                                          <span class="status-text">ปิดใช้งาน</span>
                                                      <?php
                                                        }
                                                      ?>
                                                    </svg></p>
                                        </div>
                              </div>
                         </div>
                    </div>
                    <!-- End Fourth Section --> 
                    <!--  Fifth  Section --> 
                  
                    <!-- End Fifth  Section --> 
                    <!-- END PROFIL CONTENT -->
                </div>
            </div>
        </div>
</div>
<!--Pop up บัตรประชาชน่ -->
            <div class="modal fade" id="modal-large" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><strong>บัตรประชาชน</strong></h4>
                        </div>
                         <div class="modal-body" style="text-align: center;">
                            <center>
                            <?php
                              if(!empty($customer->personal_card_img)){
                                echo "<img style='max-width: 100%;' src='".asset("customers/".$customer->personal_card_img)."' alt='ใบขับขี่' >";
                              }else{
                                echo "<img style='max-width: 100%;' src='".asset("customers/default/personal_id.png")."' alt='ใบขับขี่'>";
                              }
                            ?>
                            </center>
                           
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                        </div>
                    </div>
                </div>
            </div>
<!--Pop up -->
<!--Pop up ใบขับขี -->
            <div class="modal fade" id="modal-large-2" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><strong>ใบขับขี</strong></h4>
                        </div>
                         <div class="modal-body" style="text-align: center;">
                            
                            <?php
                              if(!empty($customer->driver_license_card_img)){
                                echo "<img style='max-width: 100%;' src='".asset("customers/".$customer->driver_license_card_img)."' alt='ใบขับขี่' >";
                              }else{
                                echo "<img style='max-width: 100%;' src='".asset("customers/default/driver_license_card.png")."' alt='ใบขับขี่'>";
                              }
                            ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                          
                        </div>
                    </div>
                </div>
            </div>
<!--Pop up -->
<!--Pop up บัตรเคดิต  -->
            <div class="modal fade" id="modal-large-3" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><strong>บัตรเคดิต</strong></h4>
                        </div>

                        <div class="modal-body" style="text-align: center;">                    
                             <?php
                              if(!empty($customer->credit_card_img)){
                                echo "<img style='max-width: 100%;' src='".asset("customers/".$customer->credit_card_img)."' alt='บัตรเคดิต' >";
                              }else{
                                echo "<img style='max-width: 100%;' src='".asset("customers/default/credit_card.png")."' alt='บัตรเคดิต'>";
                              }
                            ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                        </div>
                    </div>
                </div>
            </div>
<!--Pop up -->
@endsection

<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript') 
  <script src="{{ asset("plugins/dhtmlxScheduler/dhtmlxscheduler.js")}}"></script>
  <script src="{{ asset("plugins/dhtmlxScheduler/ext/dhtmlxscheduler_limit.js")}}"></script>
  <script src="{{ asset("plugins/dhtmlxScheduler/ext/dhtmlxscheduler_collision.js")}}"></script>
  <script src="{{ asset("plugins/dhtmlxScheduler/ext/dhtmlxscheduler_timeline.js")}}"></script>
  <script src="{{ asset("plugins/dhtmlxScheduler/ext/dhtmlxscheduler_editors.js")}}"></script>
  <script src="{{ asset("plugins/dhtmlxScheduler/ext/dhtmlxscheduler_minical.js")}}"></script>
  <script src="{{ asset("plugins/dhtmlxScheduler/ext/dhtmlxscheduler_tooltip.js")}}"></script>
  <script src="{{ asset("js/backend/dhtmlxScheduler/mock_backend.js")}}"></script>
  <script src="{{ asset("js/backend/dhtmlxScheduler/scripts.js")}}"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    
   <!--START CODING JS-->
    <script>
        $(document).ready(function () {
            //////// DataTable ///////
                var table = $('#DataTable').DataTable({
                    "lengthMenu": [[10,25,50, -1], [10,25,50, "All"]],
                    "order": [[ 0, "desc" ]],
                    "language": {
                        "lengthMenu": "แสดงผล _MENU_ ข้อมูลต่อหน้า",
                        "zeroRecords": "Nothing found - sorry",
                         "info": "ลำดับหน้าที่ _PAGE_ จาก _PAGES_ หน้า - จำนวนรายการทั้งหมด _TOTAL_ รายการ",
                        "infoEmpty": "No records available",
                        "infoFiltered": "( ค้นหาข้อมูลจาก _MAX_ รายการ )",
                        "search": "<i class='fa fa-search'></i> ค้นหาข้อมูล:",
                        "paginate": {
                            "first":      "หน้าแรก",
                            "last":       "หน้าสุดท้าย",
                            "next":       "ถัดไป",
                            "previous":   "ก่อนหน้า"
                        },
                    }
                }); // End DataTable //

        }); //End jQuery
    </script>
    <!--END CODING JS-->

@endsection


