@extends('backend.layouts.app')

@section ('title', __('สร้าง Tags'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

<?php
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // exit;
?>


@section('content')
 <div class="panel panel-default">
    <div class="panel-heading " style="height: 55px; color: #fff !important;background:#607d8b;" >
        <h3 class="panel-title" style="float:left;"><i class="fa fa-database"></i>&nbsp;สร้าง Tags</h3>
    </div>

    <div class="panel-body" style="padding: 15px;">
        <div class="row">
             <div class="col-md-12">
                 @if ($errors->any())
                  <div class="alert alert-danger" style="width:100%;">
                    <ul>
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
            </div>
            <!-- START CONTAINER -->
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>รายละเอียด</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                     <form method="POST" action="{{ route('admin.tag.tag-list.update',$data['id']) }}" enctype="multipart/form-data" class="form-horizontal" id="my-form">
                                        {{ csrf_field() }}
                                        {{ method_field('PATCH') }}
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">ชื่อ Tag</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="tag_name" class="form-control" value="<?=$data['tag_name'] ?>">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">สถานะ </label>
                                                <div class="col-sm-10">
                                                    <input type="checkbox" name="status" <?=$data['status']=='enabled'?'checked':'';?> data-on-color="btn btn-success" data-off-color="danger" class="switch">
                                                </div>
                                        </div>
                                        
                                        <div class="col-sm-8 col-sm-offset-4">
                                            <div class="pull-right">
                                                <button type="submit" class="btn btn-success" ><i class="fa fa-edit" style="color:white"></i> บันทึก</button>
                                                <button type="reset" class="btn btn-default">ยกเลิก</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- END CONTAINER FLUID -->
            </div>
        </div>
    </div>
@endsection


<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript')
<script type="text/javascript">
   
   
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>


@if(session()->has('message'))
  <script type="text/javascript">
      <?php
          $type = session()->get('message');
          $message = __('alerts.backend.database.'.$type.'');
      ?>

      <?=$type?>(
              '<i class="fa fa-check-square-o" style="color:#fff;padding-right:8px"></i><?=$message?>', {
                  HorizontalPosition: 'right',
                  VerticalPosition: 'top',
                  ShowOverlay: false,
                  TimeShown: 5000,
                  MinWidth:700
      });
  </script>
@endif

@endsection

@section('begin_css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.css" rel="stylesheet">
<style type="text/css">


</style>
@endsection