@extends ('backend.layouts.app')

@section ('title', __('รายการหมวดหมู่ทั้งหมด'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('begin_css')

<link rel='stylesheet' type='text/css' href='https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css'/>
<style type="text/css">
    input[type="search"] {
    -webkit-appearance: textfield;
}
</style>
@endsection

@section('content')
  <div class="panel panel-default">
                        <div class="panel-heading" style="color: #fff !important;color: #fff !important;background: #60868b;padding: 10px 15px;">
                            <h3 class="panel-title" style="float:left;padding: 10px 0 !important; margin: 0 !important;"><i class="fa fa-database"></i>&nbsp;ตารางแสดงรายการTagบทความ</h3>
                            <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                                    <a href="{{ route('admin.tag.tag-list.create') }}" class="btn btn-success ml-1" data-toggle="tooltip" title="" style="    float: right;" data-original-title="Create New"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;สร้างTagบทความ</a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red filter-right">
                                                    
                                    <table id="DataTable" class="table table-striped table-hover table-dynamic" >
                                            <thead>
                                                <tr>
                                                    <th class="col-md-1" style="padding-left: 0;">ลำดับ</th>
                                                    <th class="col-md-5" style="padding-left: 0;">ชื่อTagบทความ</th>
                                                    <th class="col-md-2" style="padding-left: 0;text-align:center;">ผู้สร้าง</th>
                                                    <th class="col-md-1" style="padding-left: 0;text-align:center;">วันที่ สร้าง</th>
                                                    <th class="col-md-1" style="padding-left: 0;text-align:center;">สถานะ</th>
                                                    <th class="col-md-3" style="padding-left: 0;text-align:center;">{{ __('labels.general.actions') }}</th>
                                                </tr>
                                            </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
        <div class="row">
            <div class="col-5">
                <div class="float-right">
                </div>
            </div><!--col-->
        </div><!--row-->
@endsection
<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <!--END CODING JS-->

    <script src="{{ asset("plugins/iziModal-master/js/iziModal.min.js")}}"></script>
    <script type="text/javascript" src="{{ asset("plugins/datatables/jquery.dataTables.min.js")}}"></script>
    <script src="{{ asset("plugins/datatables/dataTables.bootstrap.js")}}"></script>
    <script src="{{ asset("plugins/modal/js/classie.js")}}"></script>
    <script src="{{ asset("plugins/modal/js/modalEffects.js")}}"></script>

    <script>
        jQuery(document).ready(function(){

                //////// DataTable ///////
                var table = $('#DataTable').DataTable({
                    "lengthMenu": [[10,25,50, -1], [10,25,50, "All"]],
                    "processing": true,
                    "serverSide": true,
                    "ajax": "{{ route('ajaxdata.getdata.list') }}",
                    "order": [[ 0, "desc" ]],
                    "language": {
                        "lengthMenu": "แสดงผล _MENU_ ข้อมูลต่อหน้า",
                        "zeroRecords": "Nothing found - sorry",
                         "info": "ลำดับหน้าที่ _PAGE_ จาก _PAGES_ หน้า - จำนวนรายการทั้งหมด _TOTAL_ รายการ",
                        "infoEmpty": "No records available",
                        "infoFiltered": "( ค้นหาข้อมูลจาก _MAX_ รายการ )",
                        "search": "<i class='fa fa-search'></i> ค้นหาแบบทดสอบ:",
                        "paginate": {
                            "first":      "หน้าแรก",
                            "last":       "หน้าสุดท้าย",
                            "next":       "ถัดไป",
                            "previous":   "ก่อนหน้า"
                        },
                    },
                    "columns":[
                            {data: 'id'},
                            {data: 'tag_name'},
                            {data: 'fk_create_by'},
                            {data: 'updated_at'},
                            {data: 'status'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                    ],
                    
                }); // End DataTable //

        });
    </script>

      <!--START IMPORT EVENT FORM-->
    @include('includes.forms.ajax_delete')
    <!--END IMPORT EVENT FORM-->

@endsection
