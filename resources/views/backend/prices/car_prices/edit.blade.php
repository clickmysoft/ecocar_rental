@extends ('backend.layouts.app')

@section ('title', __('สร้างรายละเอียด ข้อมูลราคาเช่า'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('begin_css')
<link href="{{ asset("plugins/datetimepicker/jquery.datetimepicker.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.date.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.time.css")}}" rel="stylesheet">
<link href="{{ asset("backend/dependentdrop/dependentdrop.css")}}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset("plugins/chosen/chosen.css")}}" />
<link rel="stylesheet" href="{{ asset("plugins/chosen/chosenImage/chosenImage.css")}}"/>
<link rel="stylesheet" href="{{ asset("plugins/jquery-steps/jquery.steps.css")}}" />
<style type="text/css">
    .chosen-single{
        line-height: 32px !important;
        height: 32px !important;
    }

    div.payan-chana ul.chosen-choices{
            height: 33px !important;
    }

    div#select_number ul.chosen-results li{
         background-size: 0px !important;
    }

    div#brand_cars ul.chosen-results li:first-child{
        background-size: 0px !important;
        background-position-x: 12px;
    }

    div#brand_cars ul.chosen-results li{
        background-size: 27px !important;
        text-indent: 7px;
        padding-top: 6px;
        -webkit-backface-visibility: hidden;
        -webkit-transform: translateZ(0) scale(1.0, 1.0);
    }
   
    div#car_type ul.chosen-results li{
        background-size: 40px !important;
        text-indent: 26px;
    }
    div#car_type ul.chosen-results li:first-child{
        background-size: 0px !important;
        background-position-x: 12px;
    }

    div#total_human ul.chosen-results li:first-child{
        background-size: 0px !important;
    }

    div#total_bag ul.chosen-results li:first-child{
        background-size: 0px !important;
    }
    div.fule_type > div.ui-checkbox{
            margin-top: 0;
    }
    div.fule_type > div.ui-checkbox label{
           font-size: 14px !important;
    }


.registration-ui {
 /*   background: linear-gradient(to bottom, #f8d038 0%,#f5ca2e 100%);*/
   /* padding: .25em 1em .25em 1.75em;*/
   /*padding:0 .7em 1.14em .75em;
    font-weight: bold;
    font-size: 2em;
    border-radius: 5px;
    border: 1px solid #000;
    box-shadow: 1px 1px 1px #ddd;
    position: relative;
    font-family: helvetica, ariel, sans-serif;*/
    padding: 12.4px .7em 0.7em .75em;
    font-weight: bold;
    font-size: 2em;
    border-radius: 5px;
    border: 1px solid #000;
    box-shadow: 1px 1px 1px #ddd;
    position: relative;
    font-family: helvetica, ariel, sans-serif;
}

.registration-ui:before {
    /*content: 'ECOCAR';
    display: block;
    width: 30px;
    height: 100%;
    background: #063298;
    position: absolute;
    top: 0;
    border-radius: 5px 0 0 5px;
    color: #f8d038;
    font-size: .5em;
    line-height: 85px;
    padding-left: 5px;*/
    content: 'ECOCAR';
    display: block;
    text-align: center;
    width: 100%;
    height: 18px;
    background: #159077;
    position: absolute;
    top: 46.1px;
    border-radius: 5px 0 0 5px;
    color: #ffffff;
    font-size: .4em;
    line-height: 19px;
    left: 0px;
    border-bottom-right-radius: 4px;
    border-top-left-radius: 0;
    border-bottom-left-radius: 4px;
}

.registration-ui:after {
    content: '';
    display: block;
    position: absolute;
    top: 7px;
    left: 5px;
    width: 20px;
    height: 20px;
    border-radius: 30px;
   /* border: 1px dashed #f8d038;*/
}

.panel-body {
   padding-bottom: 0;
}

</style>
@endsection

@section('content')

<div class="panel panel-default">
    <div class="panel-heading " style="height: 55px; color: #fff !important;background:#00a788;">
        <h3 class="panel-title" style="float:left;"><i class="fa fa-database"></i>&nbsp;สร้างข้อมูลรถยนต์</h3>
    </div>

    <div class="panel-body">
        <div class="row">

            <form method="POST" action="{{ route('admin.prices.car-prices.update',$data['id']) }}" class="form-horizontal" id="my-form">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-group">
                        <div class="panel bd-t-red">
                            @if ($errors->any())
                                <div class="alert alert-danger" style="width:100%;">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="fa fa-gear"></i>&nbsp;<strong>สร้างรายละเอียด&nbsp;</strong>ข้อมูลราคาเช่า</h3>
                            </div>

                            <div class="form-group <?=$errors->has('type_rental_prices')!=null?'has-warning':''?>">
                                <label class="control-label col-sm-2" for="email">{{ __('validation.attributes.backend.access.users.companyname') }}:</label>
                                <div class="col-sm-10">
                                  @include('includes.partials.company')
                                </div>
                            </div>
                            
                            <div class="form-group <?=$errors->has('type_rental_prices')!=null?'has-warning':''?>">
                                <label class="control-label col-sm-2" for="pwd">
                                    {{ __('validation.attributes.backend.access.users.belong') }}:
                                </label>
                                <div class="col-sm-10">          
                                      <select class="form-control chosen-img" name="branch_name" style="height:40px;">
                                        <option value="" data-img-src="{{asset("backend/img/cars/brands/belong.png")}}">{{ __('validation.attributes.backend.company.belong_selectbox') }}</option>
                                            <?php
                                                foreach (config('company.'.app()->getLocale().'.belong') as $key => $value) {
                                                    if($value != null){
                                                          $img = asset("backend/img/cars/brands/belong.png");
                                                        echo "<option ";
                                                        echo ($data['branch_name']==$key?'selected':'');
                                                        echo " data-belogn-code='".$key."'   data-img-src='".$img."' value=".$key.">";
                                                        echo $value[0];
                                                        echo "</option>";
                                                    }
                                                }
                                            ?>
                                        </select>

                                </div>
                            </div>

                            <div class="form-group <?=$errors->has('type_rental_prices')!=null?'has-warning':''?>">
                                    <label class="control-label col-sm-2" for="email">ยี่ห้อ:</label>
                                    <div class="col-sm-10"  id="brand_cars">
                                       
                                                <select class="form-control chosen-img" name="car_brand"  tabindex="1" style="height:40px;text-transform: uppercase;" >
                                                 <option value="">เลือกยี่ห้อรถ</option>
                                                    <?php
                                                        foreach (config('cars.'.app()->getLocale().'.brands') as $key => $value) { 
                                                            if($value != null){
                                                                $img = asset("backend/img/cars/brands/".config('cars.brands_img.'.$key)[0]."");
                                                                    echo "<option ";
                                                                    echo ($data['car_brand']==$key?'selected':'');
                                                                    echo " value=".$key.">";
                                                                    echo strtoupper($key);
                                                                    echo "</option>";
                                                            }
                                                        }
                                                    ?>
                                                </select>
  
                                        @if($errors->has('car_brand'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('car_brand') }} </strong>
                                            </span>
                                        @endif
                                    </div>
                            </div>
                            <div class="form-group <?=$errors->has('type_rental_prices')!=null?'has-warning':''?>">
                                    <label class="control-label col-sm-2" for="email">รุ่น:</label>
                                    <div class="col-sm-10">
                                        <select id="set_generation" name="car_generation"  class="form-control " style="height:40px;" >
                                          <option value="" data-img-src="{{ asset("backend/img/various/clipboard-list.svg")}}">เลือกรุ่น</option>
                                                

                                        </select>

                                        @if($errors->has('car_generation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('car_generation') }} </strong>
                                            </span>
                                        @endif

                                    </div>
                            </div>
                            
                             <div style="display:none;"> class="form-group <?=$errors->has('type_rental_prices')!=null?'has-error':''?>">
                                <label class="control-label col-sm-2" for="email">กำหนดวัน - เวลา:</label>
                                <div class="col-sm-5">
                                        <select  name="status_event" class="form-control " style="height: 36.5px;">
                                          <option value="" <?=old('status_event')==''?' selected':''?>>เลือกสถานะ ปิด/เปิด</option>
                                            <option value="close" selected <?=old('status_event')=='close'?' selected':''?>>ปิด</option>
                                          <option value="close" <?=old('status_event')=='close'?' selected':''?>>ปิด</option>
                                          <option value="open" <?=old('status_event')=='open'?' selected':''?>>เปิด</option>
                                        </select>

                                        @if($errors->has('status_event'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('status_event') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            </div>

                            <div class="status_open"  style="<?=old('status_event')=='open'?'display:block;':'display:none;'?>">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" >
                                      วันเริ่มใช้งาน:
                                    </label>
                                    <div class="col-sm-10" >
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </span>
                                            <input class=" form-control pickadate set_event"  name="date_price_start" type="text" value="<?=old('price_start_date')?>">
                                        </div>

                                        @if($errors->has('date_price_start'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('date_price_start') }} </strong>
                                            </span>
                                        @endif

                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="control-label col-sm-2" >
                                      เวลาเริ่มใช้งาน:
                                    </label>
                                    <div class="col-sm-10" >
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-clock-o"></i>
                                            </span>
                                            <input class="pickatime form-control set_event" type="text" name="time_price_start" value="<?=old('time_price_start')?>">
                                        </div>

                                        @if($errors->has('time_price_start'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('time_price_start') }} </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                
                                <div class="form-group">
                                    <label class="control-label col-sm-2" >
                                      วันสิ้นสุดการใช้งาน:
                                    </label>
                                    <div class="col-sm-10" >
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </span>
                                            <input class=" form-control pickadate set_event"  name="date_price_end" type="text" value="<?=old('date_price_end')?>">
                                        </div>

                                        @if($errors->has('date_price_end'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('date_price_end') }} </strong>
                                            </span>
                                        @endif

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2" >
                                      เวลาสิ้นสุดใช้งาน:
                                    </label>
                                    <div class="col-sm-10" >
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-clock-o"></i>
                                            </span>
                                            <input class="pickatime form-control set_event" type="text" name="time_price_end" value="<?=old('time_price_end')?>">
                                        </div>

                                        @if($errors->has('time_price_end'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('time_price_end') }} </strong>
                                            </span>
                                        @endif

                                    </div>
                                </div>
                            </div>


                            <!--<div class="form-group <?=$errors->has('type_rental_prices')!=null?'has-error':''?>">-->
                            <!--    <label class="control-label col-sm-2" for="email">ประเภทการเช่า:</label>-->
                            <!--    <div class="col-sm-5">-->
                            <!--            <select  name="type_rental_prices" class="form-control " style="    height: 36.5px;">-->
                            <!--              <option value="" <?=old('type_rental_prices')==''?' selected':''?>>เลือกประเภทการเช่า</option>-->
                            <!--              <option value="per_day" <?=old('type_rental_prices')=='per_day'?' selected':''?>>รายวัน</option>-->
                                          <!--<option value="per_day" <?=old('type_rental_prices')=='per_day'?' selected':''?>>รายวัน</option>-->
                                          <!--<option value="per_month" <?=old('type_rental_prices')=='per_month'?' selected':''?>>รายเดือน</option>-->
                                          <!--<option value="per_year" <?=old('type_rental_prices')=='per_year'?' selected':''?>>รายปี</option>-->
                            <!--            </select>-->

                            <!--            @if($errors->has('type_rental_prices'))-->
                            <!--                <span class="help-block">-->
                            <!--                    <strong>{{ $errors->first('type_rental_prices') }}</strong>-->
                            <!--                </span>-->
                            <!--            @endif-->
                            <!--    </div>-->
                            <!--</div>-->

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="email">ราคาเช่ารถ:</label>
                                <div class="col-sm-5">
                                    <div class="input-group ">
                                        <span class="input-group-addon">
                                          <i class="fa fa-dashboard"></i>
                                        </span>
                                        <input  class="form-control" type="number" name="car_prices" placeholder="ระบุราคาเช่า" value="<?=$data['car_prices']?>">
                                         <span class="input-group-addon txt_type_rental">
                                          บาท / วัน
                                        </span>
                                    </div>

                                    @if($errors->has('car_prices'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('car_prices') }} </strong>
                                            </span>
                                    @endif

                                </div>

                            </div>

                           


                            <div class="form-group">
                                    <label class="control-label col-sm-2" for="pwd">
                                          สถานะการทำงาน:
                                    </label>
                                    <div class="col-sm-10">        
                                          <select class="form-control chosen-img" name="status" style="height:40px;">
                                            <option value="" >เลือกสถานะ</option>
                                            <option value="enabled" <?=$data['status']=='enabled'?' selected':''?>>เปิดการใช้งาน</option>
                                            <option value="closed" <?=$data['status']=='closed'?' selected':''?>>ปิดการใช้งาน</option>
                                          </select>

                                        @if($errors->has('status'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('status') }} </strong>
                                                </span>
                                        @endif

                                    </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="email">หมายเหตุ:</label>
                                <div class="col-sm-10">
                                    <textarea name="remarks" class="textarea_question" rows="10" cols="80"><?=$data['remarks']?></textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-12">
                                    <ul class="list-inline" style="float:right;">
                                        <li><button type="submit" class="btn btn-success "><i class="fa fa-save"></i>&nbsp;บันทึก</button></li>
                                        <li><a class="btn btn-warning prev-step" href="{{ route('admin.auth.user.index') }}"><i class="fa fa-times"></i>&nbsp;ยกเลิก</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                </div>

                <input type="hidden" name="start_date">
                <input type="hidden" name="end_date">
            </form>
        </div>
    </div>
</div>


@endsection


<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript') 
<script src="{{ asset("plugins/ckeditor/ckeditor.js")}}"></script>

<script src="{{ asset("plugins/bootstrap-datepicker/bootstrap-datepicker.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.date.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.time.js")}}"></script>
<script src="{{ asset("plugins/bootstrap-progressbar/bootstrap-progressbar.js")}}"></script>


<script src="{{ asset("plugins/chosen/chosen.jquery.js")}}"></script>
<script src="{{ asset("plugins/chosen/chosenImage/chosenImage.jquery.js")}}"></script>
<!-- Laravel Javascript Validation -->
<script type="text/javascript" src="{{ asset("vendor/jsvalidation/js/jsvalidation.js")}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\Backend\Prices\CarPricesReques','#my-form'); !!}

<script>
        CKEDITOR.replace('remarks',{
           toolbar: [
              { name: 'tools', items: [ 'Maximize'] },
              { name: 'insert', items: ['Format' ] },
              { name: 'document', items: [ '-', 'Undo', 'Redo' ] },
              { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
              { name: 'insert', items: ['Table', 'HorizontalRule', 'SpecialChar' ] },
              { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] },
              { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
              { name: 'colors', groups: [ 'colors' ] },
            ]
        });
</script>

<script type="text/javascript">
    jQuery(document).ready(function(){

        $.ajaxSetup({
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
        });

        //ยี่ห่อเปลี่ยน
        jQuery("select[name='car_brand']").change(function(){
            var brand_name = jQuery(this).val();

            jQuery("select#set_generation").empty().append('<option >กำลังโหลด...</option>');
            jQuery("select#set_generation").trigger("chosen:updated");
            
            jQuery.post("{{ route('admin.cars.generation') }}", {brand: brand_name}, function(result){
                if(result){
                        setTimeout(
                            function(){
                                    jQuery("select#set_generation option:first-child").text('เลือกรุ่น');
                                    jQuery("select#set_generation").append(result);
                                    jQuery("select#set_generation").trigger("chosen:updated");
                            },300
                        );
                }else{
                    jQuery("select#set_generation").empty().append('<option >ไม่พบอะไรกับเขาหร๊อก...</option>');
                    jQuery("select#set_generation").trigger("chosen:updated");
                }

            });
        });


        jQuery("select[name='type_rental_prices']").change(function(){

            var data = jQuery("select[name='type_rental_prices']").val();
            var txt = '';
            if(data == 'per_day'){
                txt = '/ต่อวัน';
            }
            if(data == 'per_month'){
                txt = '/ต่อเดือน';
            }

            if(data == 'per_year'){
                txt = '/ต่อปี';
            }
            jQuery("span.txt_type_rental").text('บาท '+txt);
        });

        jQuery("select[name='status_event']").change(function(){

            var status = jQuery(this).val();
            if(status == 'close'){
                jQuery("div[class^='status_open']").slideUp();
                jQuery("input.set_event").val('');
            }
            if(status == 'open'){
                jQuery("div[class^='status_open']").slideDown();
            }
        });


        var brand_name = jQuery("select[name='car_brand']").val();

            jQuery.post("{{ route('admin.cars.generation') }}", {brand: brand_name}, function(result){
                if(result){
                        setTimeout(
                            function(){
                                    jQuery("select#set_generation option:first-child").text('เลือกรุ่น');
                                    jQuery("select#set_generation").append(result);
                                    jQuery("select#set_generation").trigger("chosen:updated");

                                    jQuery('select#set_generation').val('{{ $data['car_generation'] }}');
                            },300
                        );
                }else{
                    jQuery("select#set_generation").empty().append('<option >ไม่พบอะไรกับเขาหร๊อก...</option>');
                    jQuery("select#set_generation").trigger("chosen:updated");
                }

            });

            


    });// End tag jQuery

</script>

@if(session()->has('message'))

    <script type="text/javascript">
        <?php
            $type = session()->get('message');
            $message = __('alerts.backend.database.'.$type.'');
        ?>

        <?=$type?>(
                '<i class="fa fa-check-square-o" style="color:#fff;padding-right:8px"></i><?=$message?>', {
                    HorizontalPosition: 'right',
                    VerticalPosition: 'top',
                    ShowOverlay: false,
                    TimeShown: 5000,
                    MinWidth:400
        });
    </script>
@endif

@endsection
