@extends ('backend.layouts.app')

@section ('title', __('รายละเอียดข้อมูลราคา'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('begin_css')
<style type="text/css">
[class^="icon"] {
  width: 22px;
  height: 22px;
  position: relative;
  top: 5px;
  margin-right: 10px;
  fill: #168f76;
}
[class^="icon0"] {
  width: 62px;
  height: 62px;
  position: relative;
  top: 17px;
  margin-right: 5px;
  fill: #00a950;
}
/* Match any class name that starts with icon */
[class^="icon1"] {
  width: 33px;
  height: 33px;
  position: relative;
  top: 10px;
  margin-right: 5px;
  fill: #00a950;
}
/* Match any class name that starts with icon */
[class^="icon2"] {
  width: 33px;
  height: 33px;
  position: relative;
  top: 14px;
  margin-right: 10px;
  fill: #9c9595;
}
[class^="icon3"] {
  width: 35px;
  height: 35px;
  position: relative;
  top: 13px;
  margin-right: 10px;
  fill: #00a94f;
}
[class^="icon-insure"] {
  width: 80px;
  height: 80px;
  position: relative;
  top: -5px;
  margin-right: 4px;
}
.status-eco.ks-office {
  border-radius: 5px;
  border: 1px solid #0cab54;
  background: url(/backend/img/avatars/office.svg) no-repeat top right;
  background-color: #f9f9f9;
}
.status-eco.ks-belong {
  border-radius: 5px;
  border: 1px solid #0cab54;
  background: url(/backend/img/avatars/belong.svg) no-repeat top right;
  background-color: #f9f9f9;
}

.status-eco.ks-license {
  border-radius: 5px;
  border: 1px solid #0cab54;
  background: url(/backend/img/avatars/license.svg) no-repeat top right;
  background-color: #f9f9f9;
}

.status-eco.ks-color {
  border-radius: 5px;
  border: 1px solid #0cab54;
  background: url(/backend/img/avatars/color.svg) no-repeat top right;
  background-color: #f9f9f9;
}

.status-eco.ks-nu {
  border-radius: 5px;
  border: 1px solid #0cab54;
  background: url(/backend/img/avatars/nu.svg) no-repeat top right;
  background-color: #f9f9f9;
}

.status-eco.ks-no {
  border-radius: 5px;
  border: 1px solid #0cab54;
  background: url(/backend/img/avatars/no.svg) no-repeat top right;
  background-color: #f9f9f9;
}

.status-eco.ks-mam {
  border-radius: 5px;
  border: 1px solid #0cab54;
  background: url(/backend/img/avatars/mam.svg) no-repeat top right;
  background-color: #f9f9f9;
}

.status-border{
  margin-top: 5px;
  width: 80%;
  background: #f9f9f9;
  border-radius: 4px;
  padding: 10px 10px 0px 10px;
  border: 1px #d2d2d2 solid;
}
.status-eco{
  margin: 5px -8px;
  background: #f9f9f9;
  border-radius: 4px;
  padding: 10px 10px 0px 10px;
  border: 1px #0cab54 solid;
}
.panel-title {
  width: 100% !important;
}

.alert{
  width: 100% !important;
  margin-left: 5px! important;
}

.box-insurance{
  margin-bottom: 8px;
  width: 90%;
  border-radius: 6px;
  padding: 8px 10px 13px 10px;
  background: #f9f9f9;
  border: 1px #e6e6e6 solid;
}
.class-miles{
  font-weight: 700;

}
 /* .blink {
  animation: blink-animation 1s steps(5, start) infinite;
  -webkit-animation: blink-animation 1s steps(5, start) infinite;
}
@keyframes blink-animation {
  to {
    visibility: hidden;
  }
}
@-webkit-keyframes blink-animation {
  to {
    visibility: hidden;
  }
  }*/
  .paragrahp-icon{
    margin-top: 23px;
    font-size: 16px;
    font-weight: 600;
  }
  .space-top-table{
    padding-top: 10px;
    margin-top: 16px;
    border-top: 1px #e8e8e8 dashed;
  }
  .status-text{
    position: absolute;
    margin: 15px 0px 0px 15px;
  }
  #blockCar{
    display: none;
  }
  #myDIV{
   display: none;
 }
 div#table_sreach_client_filter{
  width: 100%;
}
div#example_paginate{
  text-align: right;
}
.shopping-cart-page #main-content .row span:not(.filter-option) {
  padding: 6px 12px;
}
a#steps-uid-0-t-0{
  border: 50px;
}
input.customshow_search{
  height: 46px;
}
select.form-control.chosen-img.custom-select {
  height: 46px;
}
.wizard-inline > .steps > ul > li {
  width: 33.333%;
}
.wizard-inline > .content{
  min-height: 45em;
  background-color: transparent;
}
.input-group.informstep{
  display: inline-flex;
  width: 100% !important;
}
select.form-control.input-sm{
  height: 40px; 
}
div#example_info{
  color: #000;
  font-weight: 800;
}
li.paginate_button a {
  color: #000;
}
li#example_next a{
  color: #000;
}
tr.odd{
  background-color: #dcdcdc45 !important;
}
div#example_filter {
  width: 100%;
  text-align: right;
}
.booking-item-features > li > i {
  height: 35px;
  width: 35px;
  text-align: center;
  line-height: 35px;
  -webkit-transition: 0.3s;
  -moz-transition: 0.3s;
  -o-transition: 0.3s;
  -ms-transition: 0.3s;
  transition: 0.3s;
  font-size: 23px;
  display: block;
  -webkit-border-radius: 5px;
  border-radius: 5px;
  border: 1px solid #ccc;
  color: #686868;
}
.booking-item-car-img img{
  width:  150px  !important ;
}
.ui-checkbox{
  display: inline-block;
  padding-left: 20px;
  margin-top: 15px;
}


.page-profil h4{
    width: auto;
}
    .card{
        -webkit-border-radius: 2px;
        border-radius: 2px;
        border: 1px solid #dee0e1;
    }
    .card{
        -webkit-border-radius: 2px;
        border-radius: 2px;
        border: solid 1px #dee0e1;
        margin: 0;
    }
    .ks-panel, .panel{
        -webkit-border-radius: 2px;
        border-radius: 2px;
        border: solid 1px #dee0e1;
        background: #fff;
    }
    .card{
        position: relative;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -webkit-flex-direction: column;
       -ms-flex-direction: column;
        flex-direction: column;
        background-color: #fff;
        border: 1px solid rgba(0,0,0,.125);
        border-radius: .25rem;
    }
    .card-header:first-child{
        -webkit-border-radius: 0;
        border-radius: 0;
    }
    .card>.card-header{
        line-height: 15px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        -js-display: flex;
        display: flex;
        -webkit-box-pack: justify;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
    }
    .ks-panel .card-header, .ks-panel .panel-heading, .panel .card-header, .panel .panel-heading{
        padding: 16px 20px;
        font-weight: 500;
        background: 0 0;
        border-bottom: solid 1px #dee0e1;
        margin: 0;
       -webkit-border-top-left-radius: 2px;
        border-top-left-radius: 2px;
       -webkit-border-top-right-radius: 2px;
        border-top-right-radius: 2px;
        font-size: 14px;
    }
    .card-header:first-child{
        border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
    }
    .ks-panel .card-header+.card-block, .ks-panel .card-header+.panel-body, .ks-panel .panel-heading+.card-block, .ks-panel .panel-heading+.panel-body, .panel .card-header+.card-block, .panel .card-header+.panel-body, .panel .panel-heading+.card-block, .panel .panel-heading+.panel-body
    {
        -webkit-border-top-left-radius: 0;
        border-top-left-radius: 0;
        -webkit-border-top-right-radius: 0;
        border-top-right-radius: 0;
    }
    .ks-panel .card-block, .ks-panel .panel-body, .panel .card-block, .panel .panel-body{
        -webkit-border-top-left-radius: 2px;
        border-top-left-radius: 2px;
       -webkit-border-top-right-radius: 2px;
        border-top-right-radius: 2px;
    }
    .card-block{
        padding: 16px 20px;
        font-size: 14px;
    }
    .card-block{
        -webkit-box-flex: 1;
        -webkit-flex: 1 1 auto;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        padding: 1.25rem;
    }
    
    .ks-widget-payment-simple-amount-item{
            min-height: 124px;
    padding: 30px 20px;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    -webkit-flex-direction: row;
    -ms-flex-direction: row;
    flex-direction: row;
    border-color: #e5e5e5;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    -webkit-flex-wrap: wrap;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    }
    .ks-widget-payment-simple-amount-item.ks-green .payment-simple-amount-item-icon-block{
            background-color: #338c7c;
    }
    .ks-widget-payment-simple-amount-item .payment-simple-amount-item-icon-block{
            width: 64px;
    height: 64px;
    -webkit-border-radius: 32px;
    border-radius: 32px;
    display: -webkit-inline-box;
    display: -webkit-inline-flex;
    display: -ms-inline-flexbox;
    display: inline-flex;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    margin-right: 20px;
    }
</style>
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading " style="height: 55px; color: #fff !important;background:#607d8b;">
        <h3 class="panel-title" style="float:left;"><i class="fa fa-database"></i>&nbsp;สร้างข้อมูลราคาเช่ารถยนต์</h3>
    </div>

    <div class="panel-body">
        <div class="row">

         <div id="main-content" class="page-profil">
            <div class="row">
                <!-- BEGIN PROFIL SIDEBAR -->
                <div class="col-md-3 profil-sidebar m-t-20">
                    <img src='{{ asset("cars/toyota/vios/image-01.jpg")}}' class="img-responsive" alt="profil" > 
                    <div class="box-body box-profile">
                        <h3 class="profile-username text-center" style="color: #3a9787;text-transform: uppercase;">บริษัท ไทยเร้นท์อีโก้คาร์ จำกัด  </h3>
                            <h4 class="text-muted text-center">สาขา {{ $data->branch_name_new }}</h4>
            </div>
                </div>
                <!-- END PROFIL SIDEBAR -->
                <!-- BEGIN PROFIL CONTENT -->
                <div class="col-md-9 profil-content m-t-20">
                    <div class="row">
                         <div class="">
                        <div class="profil-sidebar-element">
                             <div class="col-lg-4">
                            <div class="card ks-widget-payment-simple-amount-item ks-green" style="justify-content: left;">
                                    <div class="payment-simple-amount-item-icon-block">
                                        <span class="ks-icon-combo-chart ks-icon"><i class="fa fa-car fa-2x" aria-hidden="true" style="
    color: #fff;
"></i></span>
                                    </div>
                                    <div class="payment-simple-amount-item-body">
                                        <div class="payment-simple-amount-item-amount">
                                            <h4 class="ks-amount">ยี่ห้อ </h4>
                                            <span class="ks-amount-icon ks-icon-circled-up-right"></span>
                                        </div>
                                        <div class="payment-simple-amount-item-description" style="font-size: 14px;color: #0090DA;">
                                            {{ $data->car_brand_new }}
                                        </div>
                                    </div>
                                </div>
                        </div>

                         <div class="col-lg-4">
                            <div class="card ks-widget-payment-simple-amount-item ks-green" style="justify-content: left;">
                                    <div class="payment-simple-amount-item-icon-block">
                                        <span class="ks-icon-combo-chart ks-icon"><i class="fa fa-list-alt fa-2x" aria-hidden="true" style="
    color: #fff;
"></i></span>
                                    </div>
                                    <div class="payment-simple-amount-item-body">
                                        <div class="payment-simple-amount-item-amount">
                                            <h4 class="ks-amount">รุ่น </h4>
                                            <span class="ks-amount-icon ks-icon-circled-up-right"></span>
                                        </div>
                                        <div class="payment-simple-amount-item-description" style="font-size: 14px;color: #0090DA;">
                                            {{ $data->car_generation_new }}
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card ks-widget-payment-simple-amount-item ks-green" style="justify-content: left;">
                                    <div class="payment-simple-amount-item-icon-block">
                                        <span class="ks-icon-combo-chart ks-icon"><i class="fa fa-calendar fa-2x" aria-hidden="true" style="
    color: #fff;
"></i></span>
                                    </div>
                                    <div class="payment-simple-amount-item-body">
                                        <div class="payment-simple-amount-item-amount">
                                            <h4 class="ks-amount">กำหนดวัน - เวลา </h4>
                                            <span class="ks-amount-icon ks-icon-circled-up-right"></span>
                                        </div>
                                    <div class="payment-simple-amount-item-description" style="font-size: 14px;color: #0090DA;font-weight: 400;">
                                            <?php
                                if($data->status_event =='close'){
                            ?>
                             ไม่กำหนดระยะเวลา
                            <?php       
                                }else{
                            ?>
                             <span>เริ่ม <?=$data->start_date_new ?></span><br>
                             <span>สิ้นสุด <?=$data->end_date_new ?></span><br>
                             <span><?=$data->exp_time ?></span><br>
                             <span style="color: red;"><?=$data->exp_date ?></span>
                            <?php       
                                }
                            ?> 
                                        </div>
                                    </div>
                                </div>
                        </div>
                          <div class="col-lg-4">
                            <div class="card ks-widget-payment-simple-amount-item ks-green" style="justify-content: left;">
                                    <div class="payment-simple-amount-item-icon-block">
                                        <span class="ks-icon-combo-chart ks-icon"><i class="fa fa-tasks fa-2x" aria-hidden="true" style="color: #fff;"></i></span>
                                    </div>
                                    <div class="payment-simple-amount-item-body">
                                        <div class="payment-simple-amount-item-amount">
                                            <h4 class="ks-amount">ประเภทการเช่า </h4>
                                            <span class="ks-amount-icon ks-icon-circled-up-right"></span>
                                        </div>
                                        <div class="payment-simple-amount-item-description" style="font-size: 14px;color: #0090DA;">
                                            {{ $data->type_rental_prices_new }}
                                        </div>
                                    </div>
                                </div>
                        </div>
                          <div class="col-lg-4">
                            <div class="card ks-widget-payment-simple-amount-item ks-green" style="justify-content: left;">
                                    <div class="payment-simple-amount-item-icon-block">
                                        <span class="ks-icon-combo-chart ks-icon"><i class="fa fa-money fa-2x" aria-hidden="true" style="
    color: #fff;
"></i></span>
                                    </div>
                                    <div class="payment-simple-amount-item-body">
                                        <div class="payment-simple-amount-item-amount">
                                            <h4 class="ks-amount">ราคาเช่ารถ </h4>
                                            <span class="ks-amount-icon ks-icon-circled-up-right"></span>
                                        </div>
                                        <div class="payment-simple-amount-item-description" style="font-size: 14px;color: #0090DA;">
                                            {{ $data->car_prices_new }} บาท
                                        </div>
                                    </div>
                                </div>
                        </div>
                          <div class="col-lg-4">
                            <div class="card ks-widget-payment-simple-amount-item ks-green" style="justify-content: left;">
                                    <div class="payment-simple-amount-item-icon-block">
                                        <span class="ks-icon-combo-chart ks-icon">
                                                 <?php
                                                        if($data->status_new =='CLOSED'){
                                                    ?>
                                                      <i class="fa fa-times fa-2x" aria-hidden="true" style="color: #fff;"></i>
                                                    <?php       
                                                        }else{
                                                    ?>
                                                    <i class="fa fa-check fa-2x" aria-hidden="true" style="color: #fff;"></i>
                                                    <?php       
                                                        }
                                                    ?> 
                                        </span>
                                    </div>
                                    <div class="payment-simple-amount-item-body">
                                        <div class="payment-simple-amount-item-amount">
                                            <h4 class="ks-amount">สถานะ </h4>
                                            <span class="ks-amount-icon ks-icon-circled-up-right"></span>
                                        </div>
                                        <div class="payment-simple-amount-item-description" style="font-size: 14px;color: #0090DA;">
                                            
                                             <?php 
                                                if ($data->status_new =='CLOSED') {
                                                   ?>
                                                   <sapn style="color: red">CLOSED</sapn> 
                                                   <?php
                                                    }
                                                    else{
                                                    ?>
                                                     ENABLED

                                                    <?php
                                                    }
                                             ?>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        </div>
                      <!-- 
                        <div class="profil-sidebar-element">
                            <h4>SOCIAL SHARING</h4>
                            <div class="social-btn-small"><a href="#" class="zocial icon facebook m-0">Sign in with Facebook</a>
                            </div>
                            <div class="social-btn-small"><a href="#" class="zocial icon twitter m-0">Sign in with Twitter</a>
                            </div>
                            <div class="social-btn-small"><a href="#" class="zocial icon googleplus m-0">Sign in with Google+</a>
                            </div>
                        </div> -->
                    </div>
                        <div class="col-md-12 profil-presentation p-t-20">
                                  <div class="card ks-panel">
                                    <h4 class="card-header" style="font-size: 18px;">
                                        หมายเหตุ:
                                    </h4>
                                    <div class="card-block">
                                        <div class="ks-text-block">
                                            <p class="ks-text"><?=$data->remarks?></p>
                                        </div>
                                        
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <!-- END PROFIL CONTENT -->
            </div>
        </div>           
        </div>
    </div>
</div>

<!-- <?php
  echo "<pre>";
  print_r(json_decode($data));
  echo "</pre>";
?> -->

@endsection

<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript') 
<link rel="stylesheet" href="{{ asset("frontend/css/icomoon.css")}}">
<script src="{{ asset("plugins/ckeditor/ckeditor.js")}}"></script>
<script src="{{ asset("plugins/datetimepicker/jquery.datetimepicker.js")}}"></script>
<script src="{{ asset("plugins/bootstrap-datepicker/bootstrap-datepicker.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.date.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.time.js")}}"></script>
<script src="{{ asset("plugins/bootstrap-switch/bootstrap-switch.js")}}"></script>
<script src="{{ asset("plugins/bootstrap-progressbar/bootstrap-progressbar.js")}}"></script>
<script src="{{ asset("plugins/chosen/chosen.jquery.js")}}"></script>
<script src="{{ asset("plugins/chosen/chosenImage/chosenImage.jquery.js")}}"></script>


<script src="{{ asset("plugins/modal/js/classie.js")}}"></script>
<script src="{{ asset("plugins/modal/js/modalEffects.js")}}"></script>

<script src="{{ asset("plugins/bootstrap-wizard/jquery.bootstrap.wizard.js")}}"></script>
<script src="{{ asset("js/backend/js/ecommerce.js")}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function(){
       
$('#table_sreach_client').dataTable( {
              "lengthMenu": [ 5, 25, 50, 75, 100 ]
            });
$('#example').dataTable( {
              "lengthMenu": [ 5, 25, 50, 75, 100 ]
            });
    });
function myFunction() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "block") { 
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}
function showCar() {
    var v = document.getElementById("blockCar");
    if (v.style.display === "block") { 
        v.style.display = "none";
    } else {
        v.style.display = "block";
    }
}
</script>


@endsection