@extends('backend.layouts.app')
@section ('title', __('สร้าง Booking'))
@section('breadcrumb-links')
@include('backend.auth.user.includes.breadcrumb-links')
@endsection
@section('begin_css')
  <link href="{{ asset("backend/theme/theme-2/assets/plugins/font-awesome/css/font-awesome.css")}}" rel="stylesheet" type="text/css" />
  <link class="main-stylesheet" href="{{ asset("backend/theme/theme-2/pages/css/pages.css")}}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Pacifico|Courgette" rel="stylesheet">



<link href="{{ asset("plugins/datetimepicker/jquery.datetimepicker.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.date.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.time.css")}}" rel="stylesheet">


<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<style>
body {
    font-family: 'Open Sans', verdana, arial;
}
    .nav.nav-tabs > li > a{
        background: transparent;
        box-shadow: none;
        border-color: transparent;
    }

    .card-custom {
        box-shadow: none!important;s
    background: #FFF;
    float: left;
    position: relative;
    border-radius: 5px;
    /* border-bottom-right-radius: 2px; */
    /* border-bottom-left-radius: 2px; */
    margin-bottom: 20px;
    border: 1px solid #e6e6e6;
    padding: 0px 0px 0px 0px!important;
}
   .small {
    line-height: 18px;
    font-size: 85%;
}
   .container-fluid {
    padding-left: 0px!important;
  }
  .btn-animated > span {
    height: 4%!important;
  }

.dataTables_info {
    margin: 0!important;
}

.dataTables_wrapper .dataTables_info {
    padding: 0px 0px 0px 0px!important;
    width: 100%;
}
.pagination {
    margin: 0!important;
}
select.input-sm {
    line-height: 11px!important;
    }
.h5class{
    font-size: 16px;
    line-height: 25.88px;
}

.no-margin {
    margin: -2px -2px 13px -14px !important;
}
.btn-group, .btn-group-vertical {
    position: relative;
    display: inline-block;
    vertical-align: middle;
}
.shopping-cart-page #main-content .row span:not(.filter-option) {
     padding: 0px 8px 1px 0px !important;
}
.b-rad-lg {
    border-radius: 7px;
    -webkit-border-radius: 7px;
    -moz-border-radius: 7px;
}

.padding-30 {
    padding: 30px !important;
}
.bg-master-light {
    background-color: #e6e6e6;
}
.pull-left {
    float: left !important;
}

.no-margin {
    margin: 0px !important;
}
.list-inline {
    padding-left: 0;
    margin-left: -5px;
    list-style: none;
}
.m-t-25 {
    margin-top: 25px;
}

.form-group-default {
    background-color: #fff;
    position: relative;
    border: 1px solid rgba(0, 0, 0, 0.07);
    border-radius: 2px;
    padding-top: 7px;
    padding-left: 12px;
    padding-right: 12px;
    padding-bottom: 4px;
    overflow: hidden;
    -webkit-transition: background-color 0.2s ease;
    transition: background-color 0.2s ease;
}

.shopping-cart-page #main-content .col-md-7 .row {
    box-shadow: none!important;
  }

  /* enable absolute positioning */
.inner-addon { 
    position: relative; 
}
/* style icon */
.inner-addon .fa {
  position: absolute;
  padding: 10px;
  pointer-events: none;
}
/* align icon */
.left-addon .glyphicon  { left:  0px;}
.right-addon .glyphicon { right: 0px;}

/* add padding  */
.left-addon input  { padding-left:  30px; }
.right-addon input { padding-right: 30px; }
.checkbox-rounded [type="checkbox"][class*='filled-in']+label:after {
    border-radius: 50%;
}

.card-edit {
    background-color: #fff;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}
#exTab2 {

  padding : 0px 0px;
}
.card-block {
    padding: 16px 20px;
    font-size: 13px;
    -webkit-box-flex: 1;
    -webkit-flex: 1 1 auto;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1.25rem;
}
.form>.row {
    margin-bottom: 20px;
}
.form-group.row .form-control-label {
    padding: .375rem .925rem;
    font-size: 13px;
    color: #333;
}
.row+.row {
    margin-top: 30px;
}
.btn-primary {
    background-color: #25628f;
    border: solid 1px #25628f;
}
.btnx {
    position: relative;
    font-size: 12px;
    font-weight: 500;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    padding: 10px 25px;
    font-family: Montserrat,sans-serif;
    color: #fff;
    height: 38px;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-transition: none;
    transition: none;
        width: 100%;
}
.ks-btn-file {
    position: relative;
    overflow: hidden;
}
.custom-control {
    min-height: 16px;
    position: relative;
    display: -webkit-inline-box;
    display: -webkit-inline-flex;
    display: -ms-inline-flexbox;
    display: inline-flex;
    min-height: 1.5rem;
    padding-left: 1.5rem;
    margin-right: 1rem;
    cursor: pointer;
}
.custom-control-input {
    position: absolute;
    z-index: -1;
    opacity: 0;
}

[type=checkbox], [type=radio] {
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    padding: 0;
}

.btn>.ks-icon+.ks-text {
    padding-left: 21px;
    padding-right: 0!important;
}
.ks-btn-file>input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    opacity: 0;
    cursor: inherit;
    display: block;
}
.cs {
 font-family: 'Courgette', cursive;
  font-size:3em;
  color:#555;
    text-align:center;
  margin-top:10%;
}

#exTab2 li.active > a {
  color:#9c4444
  border-radius: 6px 6px 0px 0px;
  background-color: #e4e4e4;
}
.panel.panel-transparent {
    background: transparent;
    -webkit-box-shadow: none;
    box-shadow: none;
}

.panel {
    -webkit-box-shadow: none;
    box-shadow: none;
    border-radius: 1px;
    -webkit-border-radius: 1px;
    -moz-border-radius: 1px;
    -webkit-transition: all 0.2s ease;
    transition: all 0.2s ease;
    position: relative;
}
.panel {
    margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
}
.padding-40{
  margin-left: 20px;
}
.headersummary{
    padding: .375rem .925rem;
    font-size: 13px;
    color: #333;
    font-family: 'Montserrat';
    font-size: 18px;
    text-transform: uppercase;
    font-weight: 600;
}
.thankyou-order-received {
    border: 3px dashed #0fa257;
    color: #000000;
    width: 100%;
    text-align: center;
    font-size: 22px;
    padding: 3%;
    margin-bottom: 30px;
    line-height: 1.4;
}

element.style {
}
.woocommerce-thankyou-order-details, .wc-bacs-bank-details {
    padding: 0;
    list-style: none;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    -ms-flex-direction: row;
    flex-direction: row;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-bottom: 10px;
}
.woocommerce-thankyou-order-details li, .wc-bacs-bank-details li {
    -webkit-box-flex: 1;
    -ms-flex: 1 0 auto;
    flex: 1 0 auto;
    padding-left: 20px;
    padding-right: 20px;
    margin-bottom: 20px;
    border-right: 1px solid rgba(129, 129, 129, 0.2);
    text-align: center;
}

.woocommerce-thankyou-order-details strong, .wc-bacs-bank-details strong {
    display: block;
    margin-top: 10px;
    color: #000000;
}
.dataTables_filter{

  float: right;
}
.dataTables_wrapper .dataTables_paginate ul > li.active > a{
  color: #fff;
}
.form-control{
   border: 1px solid rgba(23, 23, 23, 0.44);
}

#main-content {
      background-color: #DFE5E9 !important;
}
button#Previous-btn-booking:hover {
    background-color: #fafafa !important;
        padding: 5px 20px 5px 20px ;
   
}
button#btn-next-booking {
  background-color: #6d5cae;
}
button#btn-next-booking:hover {
  background-color: #8a7dbe !important; 
}
label.custom-control.custom-checkbox.ui-btn.ui-corner-all.ui-btn-inherit.ui-btn-icon-left.ui-checkbox-off {
      background: transparent !important;
}
.ui-page-theme-a .ui-btn:active{
  background: transparent !important;
}
.ui-page-theme-a .ui-btn:hover {
  background: transparent !important;
}
.dataTables_wrapper .dataTables_paginate ul > li {
  font-size: 14px;
}
#exTab2 li.active > a{
    background-color: #0c9086;
    color: #fff !important;
}
.nav.nav-tabs > li.active > a {
    background-color: white;
    color: #121212;
    border-top: 3px solid #28D094;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    color: #555;
    border-left: 1px solid #ececec;
    border-right: 1px solid #ececec;
    color: #28D094 !important;
}
.nav-tabs-linetriangle > li.active > a:hover, .nav-tabs-linetriangle > li.active > a:focus, .nav-tabs-linetriangle > li.active > a:active {
    border-color: transparent;
    background-color: transparent;
    border-top: 3px solid #28D094;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    color: #555;
    border-left: 1px solid #ececec;
    border-right: 1px solid #ececec;
}
.form-group label:not(.error) {
  font-family:  Verdana,sans-serif ;
}
select.form-control.chosen-img{
  padding-left: 20px;
 
}
i.fa.fa-credit-card.fa-stack-2x{
  font-size: 1.9em;
}
.nav-tabs-linetriangle{
  border-bottom: 1px solid #12d292;
}
i.fa.fa-car.fa-stack-2x {
  font-size: 1.8em;
}
.picker__holder{
  height: 600px;
}
input[readonly]{
  color: #10753f !important;
}
button#Previous-btn-booking{
  margin-right: 5px;
  padding: 6px 20px 7px 20px;
}
button#btn-finish{
  padding: 5px 20px 5px 20px;
  font-size: 14px;
}
.dataTables_wrapper .dataTables_paginate ul > li > a {
  opacity: 0.8;
}
</style>
@endsection
@section('content')
  <div class="panel panel-default">
                        <div class="panel-heading" style="color: #fff !important;color: #fff !important;background: #60868b;padding: 10px 15px;">
                            <h3 class="panel-title" style="float:left;padding: 10px 0 !important; margin: 0 !important;font-size: 18px;"><i class="fa fa-database"></i>&nbsp;ตารางแสดงรายการรถยนต์</h3>
                            <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid">
            <div id="rootwizard" class="m-t-50">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs nav-tabs-linetriangle nav-tabs-separator nav-stack-sm">
                <li class="active">
                  <a data-toggle="tab" href="#tab1"><span class="fa-stack fa-lg" style="padding-right: 30px;"><i class="fa fa-info fa-stack-2x"></i></span> <span style="font-size: 14px;">ข้อมูลลูกค้า</span></a>
                </li>
                <li class="">
                  <a data-toggle="tab" href="#tab2"><span class="fa-stack fa-lg" style="padding-right: 30px;"><i class="fa fa-calendar fa-stack-2x"></i></span> <span style="font-size: 14px;">วันเวลา / สถานที่</span></a>
                </li>
                <li class="">
                  <a data-toggle="tab" href="#tab3"><span class="fa-stack fa-lg" style="padding-right: 30px;"><i class="fa fa-car fa-stack-2x"></i></span><span style="font-size: 14px;">รถยนต์</span></a>
                </li>
                <li class="">
                  <a data-toggle="tab" href="#tab4"><span class="fa-stack fa-lg" style="padding-right: 30px;"><i class="fa fa-pencil-square-o fa-stack-2x"></i></span> <span style="font-size: 14px;">อุปกรณ์เสริม</span></a>
                </li>
                <li class="">
                 <a data-toggle="tab" href="#tab5"><span class="fa-stack fa-lg" style="padding-right: 50px;"><i class="fa fa-credit-card fa-stack-2x"></i></span> <span style="font-size: 14px;">ข้อมูลชําระเงิน</span></a>
                </li>
                <li class="">
                  <a data-toggle="tab" href="#tab6"><span class="fa-stack fa-lg" style="padding-right: 30px;"> </i><i class="fa fa-check fa-stack-2x"></i></span> <span style="font-size: 14px;">สรุป</span></a>
                </li>
              </ul>
              <!-- Tab panes -->
              <div class="tab-content">
                <!--tab1-->
                <div class="tab-pane padding-20 active slide-left w3-animate-right" id="tab1">
                  <div class="row row-same-height">
                    <div class="col-md-5 b-r b-dashed b-grey sm-b-b" style="border-right: 2px solid #15ac6de0">
                      <div class="padding-30">
                        <div class="md-12" style="padding-bottom: 30px;">
                          <p style="font-weight: 800;">เลขบัตรประชาชน</p>
                            <div class="active-pink-3 active-pink-4 mb-4">
                                <input class="form-control" type="text" placeholder="Search" aria-label="Search">
                           </div>
                        </div>
                        <div class="md-12" style="padding-bottom: 30px;">
                          <p style="font-weight: 800;">หมายเลขโทรศัพท์</p>
                            <div class="active-pink-3 active-pink-4 mb-4">
                                <input class="form-control" type="text" placeholder="Search" aria-label="Search">
                           </div>
                        </div>
                        <div class="md-12" style="padding-bottom: 30px;">
                        <button type="button" class="btn btn-default"> <span class="fa fa-search"></span>ค้นหา</button>
                        </div>
                        <div class="md-12" style="padding-bottom: 30px;">
                        <p>Discover goods you'll love from brands that inspire. The easiest way to open your own online store. Discover amazing stuff or open your own store for free!</p>
                        <p class="small hint-text">Below is a sample page for your cart , Created using pages design UI Elementes</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="padding-30">
                        <table id="DataTable" class="table table-striped table-hover table-dynamic" >
                                            <thead>
                                                <tr>
                                                    <th class="" style="padding-left: 0; color: #000">ชื่อ - สกุล</th>
                                                    <th class="" style="padding-left: 0;text-align:center; color: #000;">หมายเลขบัตรประชาชน</th>
                                                    <th class="" style="padding-left: 0;text-align:center; color: #000;">หมายเลขโทรศัพท์</th>
                                                    <th class="" style="text-align: center;color: #000;">{{ __('labels.general.actions') }}</th>
                                                </tr>
                                            </thead>
                        </table>
                        <div class="row">
                          <div class="col-lg-12 col-md-12">
                            <p class="no-margin">Donate now and give clean, safe water to those in need. </p>
                            <p class="small hint-text">
                              100% of your donation goes to the field, and you can track the progress of every dollar spent. <a href="#">Click Here</a>
                            </p>
                          </div>
                        </div>                      
                      </div>
                    </div>
                  </div>
                </div>
                <!--tab2-->
                <div class="tab-pane slide-left padding-20 w3-animate-right" id="tab2" style="min-height: 400px;">
                  <div class="row row-same-height">
                    <div class="col-md-12" style="">
                      <div class="padding-30">
                        <div class="row" style="margin-bottom: 30px;">
                          <div class="col-md-6">
                            <label for=""><strong>Pick Up Location</strong></label>
                            <br>
                             <div class="inner-addon left-addon">
                                <i class="fa fa-map-marker" ></i>      
                                <select class="form-control chosen-img" name="branch_name" style="height: 40px;">
                                    <option value="">เลือกสถานที่รับรถ</option>
                                    <option data-belogn-code="DonMueang"  value="DonMueang">ดอนเมือง</option>
                                    <option data-belogn-code="OnNut" value="OnNut">อ่อนนุช</option>
                                    <option data-belogn-code="BangWa" value="BangWa">บางหว้า</option>
                                    <option data-belogn-code="LatPhrao" value="LatPhrao">ลาดพร้าว</option>
                                    <option data-belogn-code="Nonthaburi" value="Nonthaburi">นนทบุรี</option>
                                    <option data-belogn-code="Samrong" value="Samrong">สำโรง</option>
                                    <option data-belogn-code="UbonAirport" value="UbonAirport">อุบล/สนามบินอุบล</option>
                                    <option data-belogn-code="ChiangMaiAirport" value="ChiangMaiAirport">เชียงใหม่/สนามบินเชียงใหม่</option>
                                    <option data-belogn-code="Pattaya" value="Pattaya">พัทยา</option>
                                    <option data-belogn-code="DonMueangAirport" value="DonMueangAirport">สนามบินดอนเมือง</option>
                                    <option data-belogn-code="SuvarnabhumiAirport" value="SuvarnabhumiAirport">สนามบินสุวรรณภูมิ</option>
                                    <option data-belogn-code="all" value="all">ทุกสาขาทั้งหมด</option>
                                </select>
                              </div>
                          </div>
                          <div class="col-md-3">
                            <label for=""><strong>Pick-up Date</strong></label>
                             <div class="inner-addon left-addon">
                                <i class="fa fa-calendar"></i>      
                               <input class=" form-control pickadate"  name="car_date_register" type="text" placeholder="เลือกวันที่รับรถ" style="height: 40px;">
                              </div>
                          </div>
                          <div class="col-md-3">
                            <label for=""> <strong>Pick-up Time</strong></label>
                             <div class="inner-addon left-addon">
                                <i class="fa fa-clock-o"></i>      
                                 <input class="pickatime form-control set_event" type="text" name="time_price_start" placeholder="เลือกเวลารับรถ" style="height: 40px;">
                              </div>
                          </div>
                        </div>
                        <div class="row" style="margin-bottom: 30px;">
                          <div class="col-md-6">
                            <label for=""><strong>Drop Off Location</strong></label>
                            <br>
                             <div class="inner-addon left-addon">
                                <i class="fa fa-map-marker"></i>      
                                  <select class="form-control chosen-img" name="branch_name" style="height: 40px;">
                                    <option value="">เลือกสถานที่คืนรถ</option>
                                    <option data-belogn-code="DonMueang"  value="DonMueang">ดอนเมือง</option>
                                    <option data-belogn-code="OnNut" value="OnNut">อ่อนนุช</option>
                                    <option data-belogn-code="BangWa" value="BangWa">บางหว้า</option>
                                    <option data-belogn-code="LatPhrao" value="LatPhrao">ลาดพร้าว</option>
                                    <option data-belogn-code="Nonthaburi" value="Nonthaburi">นนทบุรี</option>
                                    <option data-belogn-code="Samrong" value="Samrong">สำโรง</option>
                                    <option data-belogn-code="UbonAirport" value="UbonAirport">อุบล/สนามบินอุบล</option>
                                    <option data-belogn-code="ChiangMaiAirport" value="ChiangMaiAirport">เชียงใหม่/สนามบินเชียงใหม่</option>
                                    <option data-belogn-code="Pattaya" value="Pattaya">พัทยา</option>
                                    <option data-belogn-code="DonMueangAirport" value="DonMueangAirport">สนามบินดอนเมือง</option>
                                    <option data-belogn-code="SuvarnabhumiAirport" value="SuvarnabhumiAirport">สนามบินสุวรรณภูมิ</option>
                                    <option data-belogn-code="all" value="all">ทุกสาขาทั้งหมด</option>
                                </select>
                              </div>
                          </div>
                          <div class="col-md-3">
                            <label for=""><strong>Drop-off Date</strong></label>
                             <div class="inner-addon left-addon">
                                <i class="fa fa-calendar"></i>      
                                <input class=" form-control pickadate"   placeholder="เลือกวันที่คืนรถ" name="car_date_register" type="text" value="<?=old('car_date_register')?>" style="height: 40px;">
                              </div>
                          </div>
                          <div class="col-md-3">
                            <label for=""><strong>Drop-off Time</strong></label>
                             <div class="inner-addon left-addon">
                                <i class="fa fa-clock-o"></i>      
                                 <input class="pickatime form-control set_event"  placeholder="เลือกเวลาคืนรถ"  type="text" name="time_price_start" style="height: 40px;"> 
                              </div>
                          </div>
                        </div>
                      </div> 
                    </div>
                  </div>
                </div>
                <!--tab3-->
                <div class="tab-pane slide-left padding-20 w3-animate-right" id="tab3">
                  <div class="row row-same-height">
                    <div class="col-md-12">
                      <div class="padding-30">
                        <table id="DataTable-Cars" class="table table-striped table-hover table-dynamic" style="width: 100%;">
                                   <thead>
                                                <tr>
                                                    <th class="col-md-1" style="text-align: center;">code</th>
                                                    <th class="col-md-1" style="padding-left: 0;color: #000;">ทะเบียน</th>
                                                    <th class="col-md-1" style="color: #000;">รุ่นรถ</th>
                                                    <th class="col-md-1" style="color: #000;">เลขไมค์</th>
                                                    <th class="col-md-1" style="font-size: 12px;color: #000;">วันที่จดทะเบียน</th>
                                                    <th class="col-md-1" style="font-size: 12px;color: #000;">วันหมดอายุประกันภัย</th>
                                                    <th class="col-md-1" style="font-size: 12px;color: #000;">วันหมดอายุ พรบ</th>
                                                    <th class="col-md-1" style="font-size: 12px;color: #000;">วันหมดอายุภาษีรถยนต์</th>
                                                    <th class="col-md-1" style="padding-left: 0;text-align:center;color: #000;">สถานะ</th>
                                                    <th class="col-md-1" style="padding-left: 0; text-align: center;color: #000;">{{ __('labels.general.actions') }}</th>
                                                </tr>
                                   </thead>         
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <!--tab4-->
                <div class="tab-pane slide-left padding-20 w3-animate-right" id="tab4">
                  <div class="row row-same-height">
                    <div class="col-md-7 b-r b-dashed b-grey ">
                                                      <div class="form-group row">
                                                            <span class="headersummary">EXTRA OPTIONS</span>
                                                            <div class="col-sm-12">
                                                                <label class="custom-control custom-checkbox" style="width: 100%;">
                                                                  <div class="row" style="width: 100%;">
                                                                    <div class="col-md-8">
                                                                       <input type="checkbox" class="custom-control-input">
                                                                       <span class="custom-control-indicator"></span>
                                                                        <img src="{{ asset("backend/img/cars/extra_option/icon-extra-05.png")}}" style="max-width:41px;">
                                                                      <span class="custom-control-description" style="font-size: 12px;font-weight: 300;">GPS</span>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                      <span class="padding-40" style="font-size: 12px;font-weight: 300;">200 THB / DAY</span>
                                                                    </div>
                                                                  </div>
                                                                </label>
                                                                <label class="custom-control custom-checkbox" style="width: 100%;">
                                                                  <div class="row" style="width: 100%;">
                                                                    <div class="col-md-8">
                                                                       <input type="checkbox" class="custom-control-input">
                                                                       <span class="custom-control-indicator"></span>
                                                                        <img src="{{ asset("backend/img/cars/extra_option/icon-extra-04.png")}}" style="max-width:41px;">
                                                                      <span class="custom-control-description">
                                                                            <select  style="font-size: 12px;font-weight: 300;">
                                                                                <option>Baby Seat 1</option>
                                                                                <option>Baby Seat 2</option>
                                                                                <option>Baby Seat 3</option>
                                                                            </select>
                                                                      </span>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                      <span class="padding-40" style="font-size: 12px;font-weight: 300;">100 THB / DAY</span>
                                                                    </div>
                                                                  </div>
                                                                </label>
                                                                <label class="custom-control custom-checkbox" style="width: 100%;">
                                                                  <div class="row" style="width: 100%;">
                                                                    <div class="col-md-8">
                                                                       <input type="checkbox" class="custom-control-input">
                                                                       <span class="custom-control-indicator"></span>
                                                                        <img src="{{ asset("backend/img/cars/extra_option/icon-extra-03.png")}}" style="max-width:41px;">
                                                                      <span class="custom-control-description" style="font-size: 12px;font-weight: 300;">SLDW (Super Loss Damage Waiver)</span>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                      <span class="padding-40" style="font-size: 12px;font-weight: 300;">200 THB / DAY</span>
                                                                    </div>
                                                                  </div>
                                                                </label>
                                                                <label class="custom-control custom-checkbox" style="width: 100%;">
                                                                  <div class="row" style="width: 100%;">
                                                                    <div class="col-md-8">
                                                                       <input type="checkbox" class="custom-control-input">
                                                                       <span class="custom-control-indicator"></span>
                                                                        <img src="{{ asset("backend/img/cars/extra_option/icon-extra-06.png")}}" style="max-width:41px;">
                                                                      <span class="custom-control-description" style="font-size: 12px;font-weight: 300;">Delivery Service Car</span>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                      <span class="padding-40" style="font-size: 12px;font-weight: 300;">535 THB</span>
                                                                    </div>
                                                                  </div>
                                                                </label>
                                                                <label class="custom-control custom-checkbox" style="width: 100%;">
                                                                  <div class="row" style="width: 100%;">
                                                                    <div class="col-md-8">
                                                                       <input type="checkbox" class="custom-control-input">
                                                                       <span class="custom-control-indicator"></span>
                                                                        <img src="{{ asset("backend/img/cars/extra_option/icon-extra-01.png")}}" style="max-width:41px;">
                                                                      <span class="custom-control-description" style="font-size: 12px;font-weight: 300;">Service Return A Car</span>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                      <span class="padding-40" style="font-size: 12px;font-weight: 300;">535 THB</span>
                                                                    </div>
                                                                  </div>
                                                                </label>
                                                            </div>
                                                        </div>                          
                    </div>
                    <div class="col-md-5">
                         <span class="headersummary">CAR RENTAL SUMMARY</span>                       
                        <table class="table table-condensed">
                          <tr>
                            <td class=" col-md-4">
                              <span class="m-l-4 font-montserrat fs-18 all-caps" style="font-size: 16px;font-weight: 800;">Car price (Vat excluded)</span>
                            </td>
                            <td class=" col-md-4 text-right">
                              <span style="font-size: 16px;">1,200.00 THB</span>
                            </td>
                          </tr>
                          <tr>
                            <td class=" col-md-4">
                              <span class="m-l-4 font-montserrat fs-18 all-caps" style="font-size: 16px;font-weight: 800;">Option Price</span>
                            </td>
                            <td class=" col-md-4 text-right">
                              <span style="font-size: 16px;">0.00 THB</span>
                            </td>
                          </tr>
                          <tr>
                            <td class=" col-md-4">
                              <span class="m-l-4 font-montserrat fs-18 all-caps" style="font-size: 16px;font-weight: 800;">VAT 7%</span>
                            </td>
                            <td class=" col-md-4 text-right">
                              <span style="font-size: 16px;">84.00 THB</span>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" class=" col-md-3 text-right">
                              <h4 class="text-primary no-margin font-montserrat">1,284.00 THB</h4>
                            </td>
                          </tr>
                        </table>
                    </div>
                  </div>
                </div>
                <!--tab5-->
                <div class="tab-pane slide-left padding-20 w3-animate-right" id="tab5">
                  <div class="row row-same-height">
                        <div class="col-md-7">
                      <div class="">
                          <div id="exTab2" class=""> 
                            <ul class="nav nav-tabs">
                                  <li class="active">
                                    <a  href="#1" data-toggle="tab" style="margin-left: 30px !important;">โอนเงินทางธนาคาร</a>
                                  </li>
                                  <li><a href="#2" data-toggle="tab" >ชำระเงินผ่าน PayPal</a> 
                                  </li>
                                </ul>
                                  <div class="tab-content ">
                                    <div class="tab-pane active" id="1">
                                      <div class="col-lg-12 text-center">
                                            <div class="card-edit">
                                                <div class="card-block">
                                                    <form>
                                                        <div class="form-group row">
                                                            <label for="default-input" class="col-sm-2 form-control-label">ชื่อ - นามสกุล</label>
                                                            <div class="col-sm-10 text-left">
                                                                <input type="text" class="form-control" id="default-input" placeholder="นาย อีโค คาร์">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="default-input" class="col-sm-2 form-control-label">เบอร์ติดต่อ</label>
                                                            <div class="col-sm-10 text-left">
                                                                <input type="text" class="form-control" id="default-input" placeholder="089 999 9999">
                                                            </div>
                                                        </div>
                                                        <!--
                                                        <div class="form-group row">
                                                            <label for="disabled-input" class="col-sm-4 form-control-label">Disabled input</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control" disabled id="disabled-input" placeholder="Disabled input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="readonly-input" class="col-sm-4 form-control-label">Readonly input</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control" readonly id="readonly-input" placeholder="Readonly input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="active-input" class="col-sm-4 form-control-label">Active input</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control active" id="active-input" placeholder="Active">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-4 form-control-label">Static</label>
                                                            <div class="col-sm-8">
                                                                <p class="form-control-static">email@example.com</p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="default-input-rounded" class="col-sm-4 form-control-label">Default input</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control ks-rounded" id="default-input-rounded" placeholder="Default rounded input">
                                                            </div>
                                                        </div>
                                                      -->
                                                        <div class="form-group row">
                                                            <label for="default-input-rounded" class="col-sm-2 form-control-label">รายการสั่งซื้อ</label>
                                                            <div class="col-sm-10 text-left">
                                                                <select class="form-control">
                                                                    <option>เลขที่ 166 - จำนวนเงิน 330 บาท</option>
                                                                    <option>เลขที่ 167 - จำนวนเงิน 530 บาท</option> 
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="default-input" class="col-sm-2 form-control-label">ยอดเงินที่โอน</label>
                                                            <div class="col-sm-10 text-left">
                                                                <input type="text" class="form-control" id="default-input" placeholder="1000">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 form-control-label p-t-0">ธนาคารที่โอน</label>
                                                            <div class="col-sm-10 text-left">
                                                                <label class="custom-control custom-radio">
                                                                    <input id="radio1" name="radio" type="radio" class="custom-control-input">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <img src="{{ asset("backend/img/cars/bank/bank-04.png")}}" style="max-height: 40px;">
                                                                      <span class="custom-control-description" style="margin: auto 0; padding-left: 5px !important;">ธนาคารกรุงเทพ</span>
                                                                </label>
                                                                <label class="custom-control custom-radio">
                                                                    <input id="radio1" name="radio" type="radio" class="custom-control-input" checked>
                                                                    <span class="custom-control-indicator"></span>
                                                                    <img src="{{ asset("backend/img/cars/bank/bank-06.png")}}" style="max-height: 40px;">
                                                                      <span class="custom-control-description" style="margin: auto 0; padding-left: 5px !important;"> ธนาคารกสิกรไทย</span>
                                                                </label>
                                                                <label class="custom-control custom-radio">
                                                                    <input id="radio1" name="radio" type="radio" class="custom-control-input">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <img src="{{ asset("backend/img/cars/bank/bank-05.png")}}" style="max-height: 40px;">
                                                                      <span class="custom-control-description" style="margin: auto 0; padding-left: 5px !important;">ธนาคารกรุงไทย</span>
                                                                </label>
                                                                 <label class="custom-control custom-radio">
                                                                    <input id="radio1" name="radio" type="radio" class="custom-control-input">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <img src="{{ asset("backend/img/cars/bank/bank-01.png")}}" style="max-height: 40px;">
                                                                      <span class="custom-control-description" style="margin: auto 0; padding-left: 5px !important;"> ธนาคารทหารไทย</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="default-input" class="col-sm-2 form-control-label">วัน-เวลา ที่โอน</label>
                                                            <div class="col-sm-5">
                                                                 <div class='input-group date' id='datepicker'>
                                                                      <input type='text' class="form-control" /> 
                                                                      <span class="input-group-addon">
                                                                          <span class="fa fa-calendar" style="margin-left: 15px;"></span>
                                                                      </span>
                                                                  </div>
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <div class='input-group date' id='timepicker'>
                                                                      <input type='text' class="form-control" />
                                                                      <span class="input-group-addon">
                                                                          <span class="fa fa-clock-o" style="margin-left: 15px;"></span>
                                                                      </span>
                                                                  </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="default-input-rounded" class="col-sm-2 form-control-label">File Input</label>
                                                            <div class="col-sm-10">
                                                                <button class="btnx btn-primary btn-sm ks-btn-file" style="margin-left: 0 !important;">
                                                                    <span class="fa fa-cloud-upload ks-icon"></span>
                                                                    <span class="ks-text">Choose file</span>
                                                                    <input type="file">
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="default-input-rounded" class="col-sm-2 form-control-label">รายละเอียดเพิ่มเติม</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="กรอกรายละเอียด"></textarea>
                                                            </div>
                                                        </div>
                                                        <!--
                                                        <div class="form-group row">
                                                            <label class="col-sm-4 form-control-label p-t-0">Checkboxes</label>
                                                            <div class="col-sm-8">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Custom checkbox</span>
                                                                </label>

                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" checked>
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Custom checkbox</span>
                                                                </label>

                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" disabled>
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Custom checkbox</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                      -->
                                                    </form>
                                                </div>
                                            </div>
                                    </div>
                                    </div>
                                    <div class="tab-pane text-center" id="2">
                                      <span class="headersummary">Online payment processing paypal</span>
                              <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/checkout-logo-large.png" alt="Check out with PayPal" /> </div>              
                                  </div>
                              </div>
                      </div>
                    </div>
                    <div class="col-md-5 b-r b-dashed b-grey ">
                      <span class="headersummary">CAR RENTAL SUMMARY</span>
                        <table class="table table-condensed">
                          <tr>
                            <td class=" col-md-4">
                              <span class="m-l-4 font-montserrat fs-18 all-caps" style="font-size: 16px;font-weight: 800;">Car price (Vat excluded)</span>
                            </td>
                            <td class=" col-md-4 text-right">
                              <span style="font-size: 16px;">1,200.00 THB</span>
                            </td>
                          </tr>
                          <tr>
                            <td class=" col-md-4">
                              <span class="m-l-4 font-montserrat fs-18 all-caps" style="font-size: 16px;font-weight: 800;">Option Price</span>
                            </td>
                            <td class=" col-md-4 text-right">
                              <span style="font-size: 16px;">0.00 THB</span>
                            </td>
                          </tr>
                          <tr>
                            <td class=" col-md-4">
                              <span class="m-l-4 font-montserrat fs-18 all-caps" style="font-size: 16px;font-weight: 800;">VAT 7%</span>
                            </td>
                            <td class=" col-md-4 text-right">
                              <span style="font-size: 16px;">84.00 THB</span>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" class=" col-md-3 text-right">
                              <h4 class="text-primary no-margin font-montserrat" >1,284.00 THB</h4>
                            </td>
                          </tr>
                        </table>
                    </div>         
                  </div>
                </div>
                <!--tab6-->
                <div class="tab-pane slide-left padding-20 w3-animate-right" id="tab6">
                  <div class="md-12">
                    <p class="thankyou-order-received" style="color: #000000;
    font-weight: 600;">Thank you. Your order has been received.</p>
                  </div>
                  <div class="md-12">
                    <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">
                      <li class="woocommerce-order-overview__order order" style="border-left: 2px solid #0fa257;"> 
                        Order number:         <strong>3397</strong>
                      </li>
                      <li class="woocommerce-order-overview__date date" style="border-left: 2px solid #0fa257;">
                        Date:         <strong>May 18, 2018</strong>
                      </li>
                      <li class="woocommerce-order-overview__total total" style="border-left: 2px solid #0fa257;">
                        Total:          <strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>111.00</span></strong>
                      </li>
                                <li class="woocommerce-order-overview__payment-method method" style="border-right: 2px solid #0fa257; border-left: 2px solid #0fa257;">
                          Payment method:<strong>Cash on PayPal</strong>
                                </li>
                    </ul>
                  </div>
                  <div class="md-12" style="padding: 30px;">
                  </div>
                  <div class="md-12">
                     <span class="headersummary">CAR RENTAL SUMMARY ORDERS DETAIL</span>
                        <table class="table table-condensed">
                          <tr>
                            <td class=" col-md-4">
                              <span class="m-l-4 font-montserrat fs-18 all-caps" style="font-size: 16px; font-weight: 800;">Car price (Vat excluded)</span> 
                            </td>
                            <td class=" col-md-4 text-right">
                              <span style="font-size: 16px;">1,200.00 THB</span>
                            </td>
                          </tr>
                          <tr>
                            <td class=" col-md-4">
                              <span class="m-l-4 font-montserrat fs-18 all-caps" style="font-size: 16px; font-weight: 800;">Option Price</span>
                            </td>
                            <td class=" col-md-4 text-right">
                              <span style="font-size: 16px;">0.00 THB</span>
                            </td>
                          </tr>
                          <tr>
                            <td class=" col-md-4">
                              <span class="m-l-4 font-montserrat fs-18 all-caps" style="font-size: 16px; font-weight: 800;">VAT 7%</span>
                            </td>
                            <td class=" col-md-4 text-right">
                              <span style="font-size: 16px;">84.00 THB</span>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" class=" col-md-3 text-right">
                              <h4 class="text-primary no-margin font-montserrat" >1,284.00 THB</h4>
                            </td>
                          </tr>
                        </table>
                  </div>
                </div>
                <div class="padding-20 bg-white">
                  <ul class="pager wizard">
                    <li class="next">
                      <button id="btn-next-booking" class="btn btn-primary btn-cons btn-animated from-left fa fa-truck pull-right" type="button" style="background-color: #6d5cae;
    border: solid 1px #6d5cae;">
                        <span>Next</span>
                      </button>
                    </li>
                    <li class="next finish hidden">
                      <button class="btn btn-primary btn-cons btn-animated from-left fa fa-cog pull-right" id="btn-finish" type="button" style="background-color: #6d5cae;
    border: solid 1px #6d5cae;">
                        <span>Finish</span>
                      </button>
                    </li>
                    <li class="previous first hidden">
                      <button class="btn btn-default btn-cons btn-animated from-left fa fa-cog pull-right" type="button" style="background-color: #6d5cae; 
    border: solid 1px #6d5cae; color: #fff;">
                        <span>First</span>
                      </button>
                    </li>
                    <li class="previous">
                      <button id="Previous-btn-booking" class="btn btn-default btn-cons pull-right" type="button">
                        <span>Previous</span>
                      </button>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
      </div>
      </div>
      </div>
@endsection
<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript')

  <script src="{{ asset("backend/theme/theme-2/assets/plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js")}}" type="text/javascript"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/js/form_wizard.js")}}" type="text/javascript"></script>
    <!--  <script src="{{ asset("backend/theme/theme-2/assets/js/scripts.js")}}" type="text/javascript"></script> -->
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/jquery-actual/jquery.actual.min.js")}}"></script>
    <script src="{{ asset("plugins/bootstrap-datepicker/bootstrap-datepicker.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.date.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.time.js")}}"></script>


  
<!-- 
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/jquery/jquery-1.11.1.min.js")}}" type="text/javascript"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/modernizr.custom.js")}}" type="text/javascript"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/jquery-ui/jquery-ui.min.js")}}" type="text/javascript"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/jquery/jquery-easy.js")}}" type="text/javascript"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/jquery-unveil/jquery.unveil.min.js")}}" type="text/javascript"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/jquery-bez/jquery.bez.min.js")}}"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/jquery-ios-list/jquery.ioslist.min.js")}}" type="text/javascript"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js")}}"></script>
    <script type="text/javascript" src="{{ asset("backend/theme/theme-2/assets/plugins/bootstrap-select2/select2.min.js")}}"></script>
     <script type="text/javascript" src="{{ asset("backend/theme/theme-2/assets/plugins/classie/classie.js")}}"></script>

    <script src="{{ asset("backend/theme/theme-2/assets/plugins/switchery/js/switchery.min.js")}}" type="text/javascript"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}"></script>
    <script type="text/javascript" src="{{ asset("backend/theme/theme-2/assets/plugins/jquery-autonumeric/autoNumeric.js")}}"></script>
    <script type="text/javascript" src="{{ asset("backend/theme/theme-2/assets/plugins/dropzone/dropzone.min.js")}}"></script>
    <script type="text/javascript" src="{{ asset("backend/theme/theme-2/assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js")}}"></script>
    <script type="text/javascript" src="{{ asset("backend/theme/theme-2/assets/plugins/jquery-inputmask/jquery.inputmask.min.js")}}"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js")}}" type="text/javascript"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/jquery-validation/js/jquery.validate.min.js")}}" type="text/javascript"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js")}}" type="text/javascript"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/summernote/js/summernote.min.js")}}" type="text/javascript"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/moment/moment.min.js")}}"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js")}}"></script> -->
    
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="backend/theme/theme-2/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <!--START CODING JS-->
    <script>
            $(document).ready(function () {
                //////// DataTable ///////
                var table = $('#DataTable').DataTable({
                    "lengthMenu": [[1,25,50, -1], [1,25,50, "All"]],
                    "processing": true,
                    "serverSide": true,
                    "ajax": "{{ route('ajaxdata.getdata.customers') }}",
                    "order": [[ 0, "desc" ]],
                    "language": {
                        "lengthMenu": "แสดงผล _MENU_ ข้อมูลต่อหน้า",
                        "zeroRecords": "Nothing found - sorry",
                         "info": "ลำดับหน้าที่ _PAGE_ จาก _PAGES_ หน้า - จำนวนรายการทั้งหมด _TOTAL_ รายการ",
                        "infoEmpty": "No records available",
                        "infoFiltered": "( ค้นหาข้อมูลจาก _MAX_ รายการ )",
                        "search": "<i class='fa fa-search'></i> ค้นหาข้อมูล:",
                        "paginate": {
                            "first":      "หน้าแรก",
                            "last":       "หน้าสุดท้าย",
                            "next":       "ถัดไป",
                            "previous":   "ก่อนหน้า"
                        },
                    },
                    "columns":[
                            {data: 'full_name'},
                            {data: 'id_card_customer'},
                            {data: 'phone_customer'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                    ],
                    
                }); // End DataTable //
                //////// DataTable //////
            $('#DataTable-Cars').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('ajaxdata.getdata.cars') }}",
                "order": [[ 0, "desc" ]],
                "language": {
                    "lengthMenu": "แสดงผล _MENU_ ข้อมูลต่อหน้า",
                    "zeroRecords": "Nothing found - sorry",
                    "info": "ลำดับหน้าที่ _PAGE_ จาก _PAGES_ หน้า - จำนวนรายการทั้งหมด _TOTAL_ รายการ",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "<i class='fa fa-search'></i> ค้นหาข้อมูล:",
                    "paginate": {
                        "first":      "หน้าแรก",
                        "last":       "หน้าสุดท้าย",
                        "next":       "ถัดไป",
                        "previous":   "ก่อนหน้า"
                    },
                },
                "columns":[
                        {data: 'branch_car_code'},
                        {data: 'license_plate'},
                        {data: 'car_generation'},
                        {data: 'car_mile'},
                        {data: 'car_date_register'},
                        {data: 'car_enddate_insurance'},
                        {data: 'car_enddate_act'},
                        {data: 'car_enddate_tax'},
                        {data: 'status'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            }); ///////// End DataTable ///////
            }); //End jQuery
    </script>
    <!--END CODING JS-->



@if(session()->has('message'))

    <script type="text/javascript">
        <?php
            $type = session()->get('message');
            $message = __('alerts.backend.database.'.$type.'');
        ?>

        <?=$type?>(
                '<i class="fa fa-check-square-o" style="color:#fff;padding-right:8px"></i><?=$message?>', {
                    HorizontalPosition: 'right',
                    VerticalPosition: 'top',
                    ShowOverlay: false,
                    TimeShown: 5000,
                    MinWidth:400
        });
    </script>
@endif

@endsection
