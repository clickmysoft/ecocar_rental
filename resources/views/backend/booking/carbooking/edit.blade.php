@extends ('backend.layouts.app')

@section ('title', __('สร้างรายละเอียด ข้อมูลรถยนต์'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection



@section('content')

<?php
    // echo "<pre>";
    // print_r($booking);
    // echo "</pre>";
?>

<div class="panel panel-default">
    <div class="panel-heading " style="height: 55px; color: #fff !important;background:slategray;">
        <h3 class="panel-title" style="float:left;"><i class="fa fa-database"></i>&nbsp;บันทึก ลูกค้ารับรถ</h3>
    </div>

    <div class="panel-body">
        <div class="row">
            <form method="POST" action="{{ route('admin.booking.car-booking.update',$booking->id) }}" enctype="multipart/form-data" class="form-horizontal" id="my-form">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="panel bd-t-red">    

                            @if ($errors->any())
                                <div class="alert alert-danger" style="width:100%;">
                                  <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                  </ul>
                                </div>
                            @endif

                        <div class=" bd-t-red">

                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="fa fa-gears"></i> <strong>รายละเอียด</strong>ลูกค้า</h3>
                            </div>

                            <div class="form-group ">
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">ชื่อ:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="10" type="text" name="customer_first_name" readonly  value="<?=$booking->customer_first_name?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">นามสกุล:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="10" type="text" name="customer_last_name" readonly  value="<?=$booking->customer_last_name?>">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group ">
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">เบอร์โทรศัพท์มือถือ:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="10" type="text" name="customer_phone" readonly  value="<?=$booking->customer_phone?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">E-mail:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="10" type="text" name="customer_email" readonly  value="<?=$booking->customer_email?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group ">
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">ที่อยู่ตามบัตรประชาชน:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <textarea class="form-control " maxlength="10"  name="personal_card_address" ><?php
                                                if(!empty(strip_tags(@$customer->personal_card_address))){
                                                    echo strip_tags(@$customer->personal_card_address);
                                                }else{
                                                    echo $booking->personal_card_address;
                                                }
                                            ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">ที่อยู่ปัจจุบัน:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <textarea class="form-control " maxlength="10"  name="present_address" ><?php
                                                if(!empty(strip_tags(@$customer->present_address))){
                                                    echo strip_tags(@$customer->present_address);
                                                }else{
                                                    echo $booking->present_address;
                                                }
                                            ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group ">
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">ชื่อบริษัทที่ทำงาน:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="10" type="text" name="company_name" value="<?php
                                                if(!empty($customer->company_name)){
                                                    echo $customer->company_name;
                                                }else{
                                                    echo $booking->car_booking_company_name;
                                                }

                                            ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">ตำแหน่งงาน:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="10" type="text" name="position"   value="<?php
                                                if(!empty($customer->position)){
                                                    echo $customer->position;
                                                }else{
                                                    echo $booking->position;
                                                }

                                            ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group ">
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">เบอร์โทรที่ทำงาน:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="10" type="text" name="tel_office"  value="<?php
                                                if(!empty($customer->tel_office)){
                                                    echo $customer->tel_office;
                                                }else{
                                                    echo $booking->tel_office;
                                                }

                                            ?>">
                                        </div>
                                    </div>
                                </div>

                                  <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">หมายเลขบัตรประชาชน:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="13" type="text" name="id_card_customer" value="<?php
                                                if(!empty($customer->id_card_customer)){
                                                    echo $customer->id_card_customer;
                                                }else{
                                                    echo $booking->id_card_customer;
                                                }

                                            ?>">
                                        </div>
                                    </div>
                                </div>

                            </div>


                             <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="fa fa-gears"></i> <strong>รายละเอียด</strong>ผู้รับผลประโยชน์จากประกันอุบัติเหตุหรือบุคคลที่สามารถติดต่อได้ยากฉุกเฉิน</h3>
                            </div>

                            <div class="form-group ">
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">ชื่อ:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="10" type="text" name="first_name_beneficiary"   value="<?php
                                                if(!empty($customer->first_name_beneficiary)){
                                                    echo $customer->first_name_beneficiary;
                                                }else{
                                                    echo $booking->first_name_beneficiary;
                                                }

                                            ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">นามสกุล:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="10" type="text" name="last_name_beneficiary"   value="<?php
                                                if(!empty($customer->last_name_beneficiary)){
                                                    echo $customer->last_name_beneficiary;
                                                }else{
                                                    echo $booking->last_name_beneficiary;
                                                }

                                            ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group ">
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">ความสัมพันธ์:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="10" type="text" name="relation_beneficiary"   value="<?php
                                                if(!empty($customer->relation_beneficiary)){
                                                    echo $customer->relation_beneficiary;
                                                }else{
                                                    echo $booking->relation_beneficiary;
                                                }

                                            ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">เบอร์โทร:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="10" type="text" name="phone_beneficiary"   value="<?php
                                                if(!empty($customer->phone_beneficiary)){
                                                    echo $customer->phone_beneficiary;
                                                }else{
                                                    echo $booking->phone_beneficiary;
                                                }

                                            ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="fa fa-gears"></i> <strong>รายละเอียด</strong>การเช่ารถยนต์</h3>
                            </div>

                            <div class="form-group ">
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" >รหัสรถยนต์:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <select class="form-control js-example-basic-single" name="branch_car_code" style="height:40px;">
                                                   <?php
                                                        foreach ($data['car'] as $key => $value) {
                                                            echo "<option value=".$value->branch_car_code."";
                                                            if($value->branch_car_code == $booking->branch_car_code){
                                                                echo " selected ";
                                                            }
                                                            echo " >";
                                                            echo strtoupper($value->branch_car_code);
                                                            echo "</option>";
                                                        }
                                                   ?>
                                              </select>
                                        </div>
                                    </div>
                                </div>

                                 <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">ทะเบียน:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                           <input  class="form-control " maxlength="10" type="text" name="license_plate" readonly  value="<?=$booking->license_plate?>">
                                        </div>
                                    </div>
                                </div>

                            </div>


                             <div class="form-group ">
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">เช่ารถยนต์ยี่ห้อ:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="10" type="text" name="car_brand" readonly  value="<?=strtoupper($booking->car_brand)?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">เช่ารถยนต์รุ่น:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="10" type="text" name="car_generation" readonly  value="<?=strtoupper($booking->car_generation)?>">
                                        </div>
                                    </div>
                                </div>
                            </div>


                          
                           

                            <div class="form-group ">
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">วันที่รับรถ:</label>
                                    <div class="col-sm-4">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="10" type="text" name="pick_up_date" readonly  value="<?=$booking->pick_up_date?>">
                                        </div>
                                    </div>
                             
                                    <label class="control-label col-sm-2" for="email">เวลา:</label>
                                    <div class="col-sm-3">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                           <input  class="form-control " maxlength="10" type="text" name="pick_up_time" readonly  value="<?=$booking->pick_up_time?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">สถานที่รับรถยนต์:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="10" type="text" name="pick_up" readonly  value="<?=$booking->pick_up?>">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group ">
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">วันที่คืนรถ:</label>
                                    <div class="col-sm-4">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                           <input  class="form-control " maxlength="10" type="text" name="drop_off_date" readonly  value="<?=$booking->drop_off_date?>">
                                        </div>
                                    </div>
                             
                                    <label class="control-label col-sm-2" for="email">เวลา:</label>
                                    <div class="col-sm-3">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                           <input  class="form-control " maxlength="10" type="text" name="drop_off_time" readonly  value="<?=$booking->drop_off_time?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">สถานที่คืนรถยนต์:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control " maxlength="10" type="text" name="drop_off" readonly  value="<?=$booking->drop_off?>">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="panel-heading">
                                <h3 class="panel-title">
                                <i class="fa fa-gears"></i> <strong>รายละเอียด</strong>ค่าใช้จ่ายอื่นๆ</h3>
                            </div>



                             <div class="form-group ">
                                    <div class="col-md-9">
                                            <label class="control-label col-sm-2" for="email"></label>
                                            <div class="col-md-10 fule_type" style="padding-left: 13px;">
                                                 <table class="tb-w-100p">

                            <?php
                                foreach ($booking->data_sesstion['query_option'] as $key => $value) {
                            ?>

                                            <tr class="tr-extra-options">
                                                            <td class="td-extra-icon" style="padding-bottom: 10px;    width: 42.5%;">
                                                                    <img src="<?=asset($value['extra_option_icon'])?>" alt="" class="icon-formstep2">
                                                                    
                                                                   

                                                                    <span class=" title-extra"><strong>{{$value['extra_option_name']}}</strong>

                                                                         <?php
                                                                        if($value['extra_option_qty'] > 1){

                                                                            foreach ($booking->data_sesstion['extra_option_qty'] as $key22 => $value22) {
                                                                               foreach ($booking->data_sesstion['extra_option'] as $key33 => $value33) {
                                                                                    if($key33 == $key22){
                                                                                        echo " จำนวน ";
                                                                                        echo $booking->data_sesstion['extra_option_qty'][$key22];
                                                                                        echo " หน่วย";
                                                                                    }
                                                                                }
                                                                            }
                                                                            
                                                                    ?>
                                                                       
                                                                    <?php        
                                                                        }
                                                                    ?>
                                                                    </span>
                                                            </td>



                                                            <td class="td-w-50p td-extra-price">
                                                                <label class="mb20 center-mobile">ราคา {{$value['extra_option_price']}} บาท/วัน</label>
                                                            </td>
                                            </tr>
                            <?php
                                }
                            ?>
                                                </table>
                                        </div>
                                    </div>
                                </div>
                           
                                              

                            

                        <?php
                            if($booking->type_pay != 'paypal'){
                        ?>
                           

                            <div class="panel-heading">
                                <h3 class="panel-title">
                                <i class="fa fa-gears"></i> <strong>สถานะ</strong>โอนเงินค่าจอง จำนวน <?=ENV('PRICE_OF_PRE_BOOK_CAR')?> บาท</h3>
                            </div>

                            <div class="form-group ">
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">สถานะตรวจสอบ:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <?php
                                                if($booking->deposit == 'pay'){
                                            ?>
                                                <input type="hidden"  name="deposit" value="paid">
                                                <input  class="form-control " style="color:green;font-weight: bolder;" maxlength="10" type="text"   readonly  value="ผ่านการตรวจสอบแล้ว">
                                            <?php
                                                }else{
                                            ?>
                                                <input type="hidden"  name="deposit" value="wait">
                                                <input  class="form-control " style="color:red;font-weight: bolder;" maxlength="10" type="text"  readonly  value="ยังไม่ได้ชำระ">
                                            <?php
                                                }
                                            ?>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">ผู้ตรวจสอบ:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input  class="form-control" name="fk_staff_deposit" maxlength="10" type="text"  readonly  value="<?=$staff?>">
                                        </div>
                                    </div>
                                </div>
                               
                            </div>

                        <?php
                            }
                        ?>


                            <div class="panel-heading">
                                <h3 class="panel-title">
                                <i class="fa fa-gears"></i> <strong>ยอดรวมค่าใช้จ่าย</strong>วันรับรถ</h3>
                            </div>


                             <?php
                                            
                                if($booking->paid == 0){
                                    $sum = $booking->invoice_price;
                                }else{
                                    $sum = $booking->price-$booking->invoice_price;
                                }


                                $total_price =  $booking->price-$sum;

                                $c = 0;
                                 if($booking->deposit == 'pay'){
                                    $c = $booking->deposit_amount;
                                }

                              ?>

                            <div class="form-group ">
                                <?php
                                    if($booking->paid == 0){
                                ?>
                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">ยอดเงินที่ต้องชำระ:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>



                                            <input type="hidden" name="invoice_price" value="<?=$total_price?>">
                                            <input  class="form-control " style="color:red;font-weight: bolder;font-size:15px;" maxlength="10" type="text"  readonly  value="<?=number_format($sum-$c)?>"><span class="input-group-addon txt_type_rental">
                                          บาท
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                    }
                                ?>

                                <?php
                                    if(number_format($total_price) != 0){

                                ?>
                                 <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">ชำระเงินแล้วจำนวน:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <input type="hidden" name="invoice_price" value="<?=$total_price?>">
                                            <input  class="form-control " style="color:green;font-weight: bolder;font-size:15px;" maxlength="10" type="text"  readonly  value="<?=number_format($total_price)?>">
                                            <span class="input-group-addon txt_type_rental">
                                          บาท
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                    }
                                ?>

                            </div>

                            <div class="form-group ">

                                 <div class="col-md-6">
                                    <label class="control-label col-sm-3" >สถานะการชำระเงิน:</label>
                                    <div class="col-sm-9">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                              <i class="fa fa-dashboard"></i>
                                            </span>
                                            <select class="form-control js-example-basic-single" name="paid" style="height:40px;">
                                                   <option value="not_pay" <?=$booking->paid==0?'selected':''?>>ยังไม่ได้ชำระ</option>
                                                   <option value="pay" <?=$booking->paid==1?'selected':''?>>ชำระเงินแล้ว</option>
                                              </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label class="control-label col-sm-3" for="email">หลักฐานการชำระเงิน:</label>
                                    <div class="col-sm-9">
                                        <?php
                                            if($booking->image_paid != null && file_exists(public_path('booking/paid/'.$booking->image_paid))){
                                        ?>
                                          
                                                    <div class="form-group ">
                                                        <div class="col-md-6">
                                                            <div class="col-sm-9">
                                                                <div class="col-sm-9">
                                                                     <a href="#" type="button" data-toggle="modal" data-target="#id-image_paid">
                                                                        <span class="thumb la cenpad">
                                                                            <?php
                                                                              if(!empty($booking->image_paid)){
                                                                                echo "<img style='max-width: 100%;' src='".asset("booking/paid/".$booking->image_paid)."' alt='ใบขับขี่' >";
                                                                              }else{
                                                                                echo "<img style='max-width: 100%;' src='".asset("customers/default/credit_card.png")."' alt='ใบขับขี่'>";
                                                                              }
                                                                            ?>
                                                                        </span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Modal บัตรประชาชน-->
                                                        <div id="id-image_paid" class="modal fade" role="dialog">
                                                            <div class="modal-dialog">

                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title">หลักฐานการชำระเงิน</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                         <?php
                                                                              if(!empty($booking->image_paid)){
                                                                                echo "<img style='max-width: 100%;' src='".asset("booking/paid/".$booking->image_paid)."' alt='ใบขับขี่' >";
                                                                              }
                                                                            ?>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                        <?php
                                            }
                                        ?>
                                        <div class="input-group ">
                                        <span class="input-group-addon" style="width: 41px;">
                                          <i class="fa fa-file-image-o"></i>
                                        </span>
                                        <input class=" form-control" name="image_paid" type="file" accept="image/*">
                                    </div>
                                    </div>
                                </div>

                            </div>



                            <div class="panel-heading">
                                <h3 class="panel-title">
                                <i class="fa fa-gears"></i> <strong>เอกสารที่ต้องใช้</strong>ในการรับรถ</h3>
                            </div>

                            <?php
                                if(!empty($customer->personal_card_img)){
                            ?>
                                    <div class="form-group ">
                                        <div class="col-md-6">
                                            <label class="control-label col-sm-3" for="email">บัตรประชาชน:</label>
                                            <div class="col-sm-9">
                                               

                                                <div class="input-group ">
                                                     <a href="#" type="button" data-toggle="modal" data-target="#id-card">
                                                        <span class="thumb la cenpad">
                                                            <?php
                                                              if(!empty($customer->personal_card_img)){
                                                                echo "<img style='max-width: 100%;' src='".asset("customers/".$customer->personal_card_img)."' alt='บัตรประชาชน' >";
                                                              }else{
                                                                echo "<img style='max-width: 100%;' src='".asset("customers/default/personal_id.png")."' alt='บัตรประชาชน'>";
                                                              }
                                                            ?>
                                                        </span>
                                                    </a>
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Modal บัตรประชาชน-->
                                        <div id="id-card" class="modal fade" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">บัตรประชาชน</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                         <?php
                                                              if(!empty($customer->personal_card_img)){
                                                                echo "<img style='max-width: 100%;' src='".asset("customers/".$customer->personal_card_img)."' alt='บัตรประชาชน' >";
                                                              }else{
                                                                echo "<img style='max-width: 100%;' src='".asset("customers/default/personal_id.png")."' alt='บัตรประชาชน'>";
                                                              }
                                                            ?>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                            <?php
                                }else{
                            ?>

                                    <div class="form-group ">
                                        <div class="col-md-6">
                                            <label class="control-label col-sm-3" for="email">บัตรประชาชน:</label>
                                            <div class="col-sm-9">
                                                <?php
                                                      if($booking->personal_card_img != null && file_exists(public_path('customers/'.$booking->personal_card_img))){
                                                ?>

                                                    <div class="form-group ">
                                                        <div class="col-md-6">
                                                            <div class="col-sm-9">
                                                                <div class="col-sm-9">
                                                                    <div class="input-group ">
                                                                         <a href="#" type="button" data-toggle="modal" data-target="#id-car">
                                                                            <span class="thumb la cenpad">
                                                                                <?php
                                                                                  if(!empty($booking->personal_card_img)){
                                                                                    echo "<img style='max-width: 100%;' src='".asset("customers/".$booking->personal_card_img)."' alt='ใบขับขี่' >";
                                                                                  }else{
                                                                                    echo "<img style='max-width: 100%;' src='".asset("customers/default/driver_license_card.png")."' alt='ใบขับขี่'>";
                                                                                  }
                                                                                ?>
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- Modal บัตรประชาชน-->
                                                        <div id="id-car" class="modal fade" role="dialog">
                                                            <div class="modal-dialog">

                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title">บัตรประชาชน</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                         <?php
                                                                              if(!empty($booking->personal_card_img)){
                                                                                echo "<img style='max-width: 100%;' src='".asset("customers/".$booking->personal_card_img)."' alt='ใบขับขี่' >";
                                                                              }else{
                                                                                echo "<img style='max-width: 100%;' src='".asset("customers/default/driver_license_card.png")."' alt='ใบขับขี่'>";
                                                                              }
                                                                            ?>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                <?php
                                                    }
                                                ?>
                                                <div class="input-group ">
                                                <span class="input-group-addon" style="width: 41px;">
                                                  <i class="fa fa-file-image-o"></i>
                                                </span>
                                                <input class=" form-control" name="personal_card_img" type="file" accept="image/*">
                                            </div>
                                            </div>
                                        </div>
                                    </div>

                            <?php
                                }
                            ?>


                            <?php
                                if(!empty($customer->driver_license_card_img)){
                            ?>
                                    <div class="form-group ">
                                        <div class="col-md-6">
                                            <label class="control-label col-sm-3" for="email">ใบขับขี่รถยนต์:</label>
                                            <div class="col-sm-9">
                                                <div class="input-group ">
                                                     <a href="#" type="button" data-toggle="modal" data-target="#id-car">
                                                        <span class="thumb la cenpad">
                                                            <?php
                                                              if(!empty($customer->driver_license_card_img)){
                                                                echo "<img style='max-width: 100%;' src='".asset("customers/".$customer->driver_license_card_img)."' alt='ใบขับขี่' >";
                                                              }else{
                                                                echo "<img style='max-width: 100%;' src='".asset("customers/default/driver_license_card.png")."' alt='ใบขับขี่'>";
                                                              }
                                                            ?>
                                                        </span>
                                                    </a>
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Modal บัตรประชาชน-->
                                        <div id="id-car" class="modal fade" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">ใบขับขี่รถยนต์</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                         <?php
                                                              if(!empty($booking->driver_license_card_img)){
                                                                echo "<img style='max-width: 100%;' src='".asset("customers/".$booking->driver_license_card_img)."' alt='ใบขับขี่' >";
                                                              }else{
                                                                echo "<img style='max-width: 100%;' src='".asset("customers/default/driver_license_card.png")."' alt='ใบขับขี่'>";
                                                              }
                                                            ?>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                            <?php
                                }else{
                            ?>
                                    <div class="form-group ">
                                        <div class="col-md-6">
                                            <label class="control-label col-sm-3" for="email">ใบขับขี่รถยนต์:</label>
                                            <div class="col-sm-9">
                                                <?php
                                                      if($booking->driver_license_card_img != null && file_exists(public_path('customers/'.$booking->driver_license_card_img))){
                                                ?>
                                                    <div class="form-group ">
                                                        <div class="col-md-6">
                                                            <div class="col-sm-9">
                                                                <div class="col-sm-9">
                                                                     <a href="#" type="button" data-toggle="modal" data-target="#id-driver_license_card_img">
                                                                        <span class="thumb la cenpad">
                                                                            <?php
                                                                              if(!empty($booking->driver_license_card_img)){
                                                                                echo "<img style='max-width: 100%;' src='".asset("customers/".$booking->driver_license_card_img)."' alt='ใบขับขี่' >";
                                                                              }else{
                                                                                echo "<img style='max-width: 100%;' src='".asset("customers/default/credit_card.png")."' alt='ใบขับขี่'>";
                                                                              }
                                                                            ?>
                                                                        </span>
                                                                    </a>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Modal บัตรประชาชน-->
                                                        <div id="id-driver_license_card_img" class="modal fade" role="dialog">
                                                            <div class="modal-dialog">

                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title">ใบขับขี่รถยนต์</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                         <?php
                                                                              if(!empty($booking->driver_license_card_img)){
                                                                                echo "<img style='max-width: 100%;' src='".asset("customers/".$booking->driver_license_card_img)."' alt='ใบขับขี่' >";
                                                                              }else{
                                                                                echo "<img style='max-width: 100%;' src='".asset("customers/default/credit_card.png")."' alt='ใบขับขี่'>";
                                                                              }
                                                                            ?>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                <?php
                                                  }
                                                ?>
                                                <div class="input-group ">
                                                <span class="input-group-addon" style="width: 41px;">
                                                  <i class="fa fa-file-image-o"></i>
                                                </span>
                                                <input class=" form-control" name="driver_license_card_img" type="file" accept="image/*">
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                            <?php
                                }
                            ?>




                            <?php
                                if(!empty($customer->credit_card_img)){
                            ?>
                                    <div class="form-group ">
                                        <div class="col-md-6">
                                            <label class="control-label col-sm-3" for="email">ใบขับขี่รถยนต์:</label>
                                            <div class="col-sm-9">
                                                <div class="input-group ">
                                                     <a href="#" type="button" data-toggle="modal" data-target="#id-credit_card">
                                                        <span class="thumb la cenpad">
                                                            <?php
                                                              if(!empty($customer->credit_card_img)){
                                                                echo "<img style='max-width: 100%;' src='".asset("customers/".$customer->credit_card_img)."' alt='ใบขับขี่' >";
                                                              }else{
                                                                echo "<img style='max-width: 100%;' src='".asset("customers/default/credit_card.png")."' alt='ใบขับขี่'>";
                                                              }
                                                            ?>
                                                        </span>
                                                    </a>
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Modal บัตรประชาชน-->
                                        <div id="id-credit_card" class="modal fade" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">ใบขับขี่รถยนต์</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                         <?php
                                                              if(!empty($customer->credit_card_img)){
                                                                echo "<img style='max-width: 100%;' src='".asset("customers/".$customer->credit_card_img)."' alt='ใบขับขี่' >";
                                                              }else{
                                                                echo "<img style='max-width: 100%;' src='".asset("customers/default/credit_card.png")."' alt='ใบขับขี่'>";
                                                              }
                                                            ?>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                            <?php
                                }else{
                            ?>
                                     <div class="form-group ">
                                        <div class="col-md-6">
                                            <label class="control-label col-sm-3" for="email">บัตรเคดิต:</label>
                                            <div class="col-sm-9">
                                                 <?php
                                                      if($booking->credit_card_img != null && file_exists(public_path('customers/'.$booking->credit_card_img))){
                                                ?>

                                                    <div class="form-group ">
                                                        <div class="col-md-6">
                                                            <div class="col-sm-9">
                                                                <div class="col-sm-9">
                                                                     <a href="#" type="button" data-toggle="modal" data-target="#id-credit_card_img">
                                                                        <span class="thumb la cenpad">
                                                                            <?php
                                                                              if(!empty($booking->credit_card_img)){
                                                                                echo "<img style='max-width: 100%;' src='".asset("customers/".$booking->credit_card_img)."' alt='ใบขับขี่' >";
                                                                              }else{
                                                                                echo "<img style='max-width: 100%;' src='".asset("customers/default/credit_card.png")."' alt='ใบขับขี่'>";
                                                                              }
                                                                            ?>
                                                                        </span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Modal บัตรประชาชน-->
                                                        <div id="id-credit_card_img" class="modal fade" role="dialog">
                                                            <div class="modal-dialog">

                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title">บัตรเคดิต</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                         <?php
                                                                              if(!empty($booking->credit_card_img)){
                                                                                echo "<img style='max-width: 100%;' src='".asset("customers/".$booking->credit_card_img)."' alt='ใบขับขี่' >";
                                                                              }else{
                                                                                echo "<img style='max-width: 100%;' src='".asset("customers/default/credit_card.png")."' alt='ใบขับขี่'>";
                                                                              }
                                                                            ?>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                <?php
                                                  }
                                                ?>
                                                <div class="input-group ">
                                                <span class="input-group-addon" style="width: 41px;">
                                                  <i class="fa fa-file-image-o"></i>
                                                </span>
                                                <input class=" form-control" name="credit_card_img" type="file" accept="image/*">
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                            <?php
                                }
                            ?>



                        </div>

                      
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <ul class="list-inline" style="float:right;">
                            <li><button type="submit" class="btn btn-success "><i class="fa fa-save"></i>&nbsp;บันทึก</button></li>
                            <li><a class="btn btn-warning prev-step" href="{{ route('admin.auth.user.index') }}"><i class="fa fa-times"></i>&nbsp;ยกเลิก</a></li>
                        </ul>
                    </div>
                </div>
                <input type="hidden" name="id" value="<?=$booking->id?>">
                <input type="hidden" name="car_booking_code" value="<?=$booking->car_booking_code?>">
                <input type="hidden" name="invoices_id" value="<?=$booking->invoices_id?>">
                
            </form>
        </div>
    </div>
</div>

@endsection


<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript') 
    <script src="{{ asset("plugins/select2/select2.min.js")}}"></script>
    <script src="{{ asset("plugins/ckeditor/ckeditor.js")}}"></script>
    <script src="{{ asset("plugins/bootstrap-switch/bootstrap-switch.js")}}"></script>
    <script src="{{ asset("plugins/bootstrap-progressbar/bootstrap-progressbar.js")}}"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset("vendor/jsvalidation/js/jsvalidation.js")}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\Backend\Booking\ReceiveRequest','#my-form'); !!}
    <script type="text/javascript">
           $('.js-example-basic-single').select2();
    </script>

@endsection

@section('begin_css')
    <link href="{{ asset("plugins/datetimepicker/jquery.datetimepicker.css")}}" rel="stylesheet">
    <link href="{{ asset("plugins/pickadate/themes/default.css")}}" rel="stylesheet">
    <link href="{{ asset("plugins/pickadate/themes/default.date.css")}}" rel="stylesheet">
    <link href="{{ asset("plugins/pickadate/themes/default.time.css")}}" rel="stylesheet">
    <link href="{{ asset("backend/dependentdrop/dependentdrop.css")}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset("plugins/wizard/wizard.css")}}" />
    <link rel="stylesheet" href="{{ asset("plugins/jquery-steps/jquery.steps.css")}}" />
    <link href="{{ asset("plugins/select2/select2.css")}}" rel="stylesheet">


    <link rel="stylesheet" href="{{ asset("frontend/css/font-awesome.css")}}">
    <link rel="stylesheet" href="{{ asset("frontend/css/styleecocar.css")}}">
 
      <style type="text/css">
            .select2-container {
                box-sizing: border-box;
                display: inline-block;
                margin: 0;
                position: relative;
                vertical-align: middle;
                width: 100% !important;
            }

            .ks-name {
                text-align: left;
                font-size: 18px;
                font-weight: 500;
                font-family: Montserrat,sans-serif;
                color: #333;

            }
            .ks-avatar {
                width: 167px;
                height: 167px;
                min-width: 167px;
                min-height: 167px;
                margin-right: 30px;
                -webkit-border-radius: 2px;
                border-radius: 2px;
                background-color: #fff;
                -webkit-box-shadow: 0 5px 10px 0 rgba(0,0,0,.1);
                box-shadow: 0 5px 10px 0 rgba(0,0,0,.1);
            }
            .ks-user {
                display: -webkit-box;
                display: -webkit-flex;
                display: -ms-flexbox;
                -js-display: flex;
                display: flex;
                -webkit-box-pack: justify;
                -webkit-justify-content: space-between;
                -ms-flex-pack: justify;
                justify-content: space-between;
                padding-left: 30px;
                padding-right: 30px;
                margin-top: -50px;
            }
            .ks-cover {
                background-color: #ccc;
                background-position: center center;
                background-repeat: no-repeat;
                background-size: cover;
                height: 250px;
            }
            .ks-description {
                font-size: 12px;
                margin-bottom: 5px;
            }
            .ks-actions{
                margin-top: 15px;
            }
            .ks-body {
                width: 100%;
                margin-top: 70px;
            }
            .ks-info {
                display: -webkit-box;
                display: -webkit-flex;
                display: -ms-flexbox;
                -js-display: flex;
                display: flex;
            }
            .ks-statistics {
                display: -webkit-box;
                display: -webkit-flex;
                display: -ms-flexbox;
                -js-display: flex;
                display: flex;
                -webkit-box-align: center;
                -webkit-align-items: center;
                -ms-flex-align: center;
                align-items: center;
                margin-top: 84px;
            }
            .ks-amount {
                color: #25628f;
                font-size: 28px;
                font-weight: 500;
                font-family: Montserrat,sans-serif;
            }
            .ks-item {
                margin-right: 40px;
                text-align: center;
            }
            .ks-text {
                font-weight: 600;
                text-align: left;
                font-size: 13px;
                font-family: Montserrat,sans-serif;
            }
            .btn {
                margin-right: 10px;
            }

            .btn-primary {
                background-color: #25628f;
                border: solid 1px #25628f;
            }
            .btn-primary:hover {
                color: #fff;
                background-color: #1d4c6f;
                border-color: #1d4c6f;
                border-radius: 3px;
            }
            .btn-success:hover {
                border-radius: 3px;
                color: #fff;
                background-color: #43a047;
                border-color: #43a047;
            }
            .ks-tabs-container.ks-full {
                margin-top: 35px;
                border: none;
                border-radius: 0;
            }
            .ks-profile .ks-tabs-container .ks-nav-tabs {
                background: #fff;
            }

            .ks-tabs-container.ks-full .ks-nav-tabs {
                padding-left: 30px;
                padding-right: 30px;
                border-color: rgba(57,80,155,.2);
                background: #265464;
                box-shadow: 1px 7px 10px #66666699;
            }
            .nav.ks-nav-tabs {
                border-bottom: 1px solid #d7dceb;
                border-color: #d7dceb;
                -webkit-flex-wrap: wrap;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
            }
            .nav {
                display: -webkit-box;
                display: -webkit-flex;
                display: -ms-flexbox;
                display: flex;
                padding-left: 0;
                margin-bottom: 0;
                list-style: none;
            }

            .ks-tabs-container.ks-full.ks-light>.ks-nav-tabs .nav-link:not(.active) {
                color: #ffffff;
            }
            .ks-tabs-container.ks-full>.ks-nav-tabs>.nav-item>.nav-link {
                font-size: 14px;
                padding: 20px 18px;
            }
            .ks-tabs-container.ks-tabs-default.ks-tabs-no-separator>.ks-nav-tabs .nav-link {
                border: none;
            }
            .ks-tabs-container.ks-tabs-default .nav-link {
                color: #00e9c2;
                line-height: 14px;
                position: relative;
                font-size: 14px;
                padding: 18px 20px;
            }
            .nav-link:focus, .nav-link:hover {
                text-decoration: none;
            }
            .ks-tabs-container.ks-full .tab-pane {
                padding: 30px;
            }
        /*
        .logout{
        background: #d2d2d2;;
        }
        */
        .ks-panel.panel-default, .panel.panel-default {
            border: solid 1px #265464;
        }
        .card {
            -webkit-border-radius: 2px;
            border-radius: 2px;
            border: 1px solid #dee0e1;
            -webkit-border-radius: 2px;
            border-radius: 2px;
            border: solid 1px #dee0e1;
            margin: 0;
        }
        .ks-panel, .panel {
            -webkit-border-radius: 2px;
            border-radius: 2px;
            /*border: solid 1px #dee0e1;*/
            background: #fff;
        }
        .card-header, .panel.panel-default .panel-heading {
            border-bottom: solid 1px #dee0e1;
        }
        .ks-panel.ks-information .card-header, .panel.ks-information .card-header {
            padding-left: 30px;
            padding-right: 30px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            -js-display: flex;
            display: flex;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
        }
        .card-header:first-child {
            -webkit-border-radius: 0;
            border-radius: 0;
        }
        .card>.card-header {
            line-height: 15px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            -js-display: flex;
            display: flex;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }
        .ks-panel .card-header, .ks-panel .panel-heading, .panel .card-header, .panel .panel-heading {
            padding: 16px 20px;
            font-weight: 500;
            background: 0 0;
            border-bottom: solid 1px #265464;
            margin: 0;
            -webkit-border-top-left-radius: 2px;
            border-top-left-radius: 2px;
            -webkit-border-top-right-radius: 2px;
            border-top-right-radius: 2px;
            font-size: 14px;
        }
        .panel.ks-information .card-block {
            padding: 30px;
        }
        .card-block {
            font-size: 13px;
            flex: 1 1 auto;
        }
        .ks-item {
            margin-bottom: 15px;
        }
        .ks-name-body{
            text-align: left;
            font-size: 16px;
            font-weight: 500;
            font-family: Montserrat,sans-serif;
            color: #333;
        }
        .ks-text-body{
            text-align: left;
            font-size: 13px;
            font-weight: 400;
            font-family: Montserrat,sans-serif;
            color: #737373;
        }
        .card {
            margin-top: 30px;
        }
        .ks-icon {
            width: 20px;
            padding-right: 15px;
            color: #25628f;
            line-height: 20px;
            opacity: .9;
            position: relative;
            top: 1px;
            text-align: center;
        }
        .la {
            font-size: inherit;
            text-decoration: inherit;
            text-rendering: optimizeLegibility;
            text-transform: none;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            font-smoothing: antialiased;
            display: inline-block;
        }
        .panel.ks-information .card-block>.ks-table-description td {
            vertical-align: top;
            font-size: 14px;
            padding-bottom: 10px;
        }
        .thumb{
            width: 90px;
            height: 90px;
            border-radius: 2px;
            background-color: #fff;
            border: solid 1px #dee0e1;
        }
        .ks-thumb{
            width: 90px;
            height: 90px;
            border-radius: 2px;
            background-color: #fff;
            border: solid 1px #dee0e1;
            margin-bottom: 5px;
            display: flex;
            -webkit-box-align: center;
            align-items: center;
        }
        .ks-filename{
            display:block;
            font-size:12px;
            color:#858585;
            text-align:center;
            padding-top:10px;
        }
        .cenpad{
            padding: 24px;
        }

        .chosen-single{
            line-height: 32px !important;
            height: 32px !important;
        }

        div.payan-chana ul.chosen-choices{
            height: 33px !important;
        }

        div#select_number ul.chosen-results li{
            background-size: 0px !important;
        }

        div#brand_cars ul.chosen-results li:first-child{
            background-size: 0px !important;
            background-position-x: 12px;
        }

        div#brand_cars ul.chosen-results li{
            background-size: 27px !important;
            text-indent: 7px;
            padding-top: 6px;
            -webkit-backface-visibility: hidden;
            -webkit-transform: translateZ(0) scale(1.0, 1.0);
        }

        div#car_type ul.chosen-results li{
            background-size: 40px !important;
            text-indent: 26px;
        }
        div#car_type ul.chosen-results li:first-child{
            background-size: 0px !important;
            background-position-x: 12px;
        }

        div#total_human ul.chosen-results li:first-child{
            background-size: 0px !important;
        }

        div#total_bag ul.chosen-results li:first-child{
            background-size: 0px !important;
        }
        div.fule_type > div.ui-checkbox{
            margin-top: 0;
        }
        div.fule_type > div.ui-checkbox label{
            font-size: 14px !important;
        }


        .registration-ui {
            /*   background: linear-gradient(to bottom, #f8d038 0%,#f5ca2e 100%);*/
            /* padding: .25em 1em .25em 1.75em;*/
        /*padding:0 .7em 1.14em .75em;
        font-weight: bold;
        font-size: 2em;
        border-radius: 5px;
        border: 1px solid #000;
        box-shadow: 1px 1px 1px #ddd;
        position: relative;
        font-family: helvetica, ariel, sans-serif;*/
        padding: 12.4px .7em 0.7em .75em;
        font-weight: bold;
        font-size: 2em;
        border-radius: 5px;
        border: 1px solid #000;
        box-shadow: 1px 1px 1px #ddd;
        position: relative;
        font-family: helvetica, ariel, sans-serif;
        }

        .registration-ui:before {
        /*content: 'ECOCAR';
        display: block;
        width: 30px;
        height: 100%;
        background: #063298;
        position: absolute;
        top: 0;
        border-radius: 5px 0 0 5px;
        color: #f8d038;
        font-size: .5em;
        line-height: 85px;
        padding-left: 5px;*/
        content: 'ECOCAR';
        display: block;
        text-align: center;
        width: 100%;
        height: 18px;
        background: #159077;
        position: absolute;
        top: 46.1px;
        border-radius: 5px 0 0 5px;
        color: #ffffff;
        font-size: .4em;
        line-height: 19px;
        left: 0px;
        border-bottom-right-radius: 4px;
        border-top-left-radius: 0;
        border-bottom-left-radius: 4px;
        }

        .registration-ui:after {
            content: '';
            display: block;
            position: absolute;
            top: 7px;
            left: 5px;
            width: 20px;
            height: 20px;
            border-radius: 30px;
            /* border: 1px dashed #f8d038;*/
        }

        div.panel-heading{
            margin-bottom: 17px !important;
        }

        div.section-bc{
            display: none !important;
        }
        </style>
    
@endsection
