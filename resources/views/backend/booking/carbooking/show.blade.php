@extends ('backend.layouts.app')

@section ('title', __('รายการลูกค้าทั้งหมด'))

@section('breadcrumb-links')
@include('backend.auth.user.includes.breadcrumb-links')
@endsection



@section('content')


<?php
// echo "<pre>";
//   print_r($data);
// echo "</pre>";
?>


<div class="panel panel-default">
  <div class="panel-heading" style="color: #fff !important;color: #fff !important;background: #60868b;padding: 10px 15px;">
    <h3 class="panel-title" style="float:left;padding: 10px 0 !important; margin: 0 !important;"><i class="fa fa-database"></i>&nbsp;ข้อมูลรายการ Booking</h3>
    <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups" >
    <a data-balloon="บันทึกการคืน" style="float:right;" data-balloon-pos="up" href="{{route("admin.booking.car-booking.edit",$data->id)}}" class="btn btn-primary" style="border-radius: 50px;background-color: #cc1156; border-color: transparent;padding: 4px 8px;"><i class="fa fa-pencil"></i> บันทึกรับรถ</a></div>
  </div>
  <div class="panel-body">
    

        <!--Home page style-->
        <header class="home-bg">
          <div class="overlay-img">
            <div class="container">
             
              <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                  <div class="header-details">

                        <div class="row">
                          <div class="col-md-12"><span><i class="fa fa-user-circle" aria-hidden="true"></i> ชื่อ - สกุล</span>
                              <p class="name">{{@$data->customer_first_name}}  {{@$data->customer_last_name}}</p>
                          </div>
                     
                          <?php
                            if(!empty($data->id_card_customer)){
                          ?>
                            <div class="col-md-12"><span class="label label-danger">เลขบัตรประชาชน</span>
                              <p class="id-number">{{@$data->id_card_customer}}</p>
                            </div>
                          <?php
                            }
                          ?>

                          <div class="col-md-12 "><span class="label label-warning">หมายเลขโทรศัพท์</span>
                              <p class="phone-number phone_wat">{{@$data->customer_phone}}</p>
                          </div>

                          <div class="col-md-12 "><span class="label label-success">อีเมล</span>
                              <p class="phone-number">{{@$data->customer_email}}</p>
                          </div>

                        </div>

                  </div>
                </div>
              </div>

            </div>
          </div>  
        </header>

        <!-- Sections -->
        <section id="" class="sections-1">
            <div class="col-md-12 ">
                <!-- Example row of columns -->
                <div class="row">
          <div class="col-sm-6 col-sm-12 col-xs-12" style="padding-left:0px;">
            <div class="promotion" style="    min-height: 304px;">
            
            <span class="headet">วันเวลา / สถานที่</span>
            <div class="border-h"></div>
            
            <div class="row">
            <div class="col-md-6">
              <p class="pick-up" style="color: #44861d;" > <i class="fa fa-map-marker" aria-hidden="true"></i>  เลือกสถานที่รับรถ</p>
            <p><b>Pick Up Location</b></p>
            <p>{{ $data->pick_up }}</p>
            <p><b>Pick-up Date</b></p>
            <p><?=$data->pick_up_date?></p>
            <p><b>Pick-up Time</b></p>
            <p>{{ $data->pick_up_time }} น.</p>

            </div>

            <div class="col-md-6">
              <p class="pick-up" style="color: #dc992e;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>  เลือกสถานที่คืนรถ</p>
            <p><b>Pick Up Location</b></p>
            <p>{{ $data->drop_off }}</p>
            <p><b>Pick-up Date</b></p>
            <p><?=$data->drop_off_date?></p>
            <p><b>Pick-up Time</b></p>
            <p>{{ $data->drop_off_time }} น.</p>

            </div>
          </div>
            
            
            </div>
          </div>
          
          <div class="col-sm-3 col-sm-12 col-xs-12">
            <div class="promotion">
            
            <span class="headet">รถยนต์</span>
            <div class="border-h"></div>
              <p><b>Code:&nbsp;</b>{{$data->branch_car_code}}</p>
              <p><b>เลขทะเบียน:&nbsp;</b> {{$data->license_plate }}</p>
              <p><b>รุ่น:&nbsp;</b><?=strtoupper($data->car_brand);?> <?=strtoupper($data->car_generation);?> <?=strtoupper(str_replace("_",".",$data->car_generation_detail));?></p>
              <p><b>เลขไมค์:&nbsp;</b> <?=number_format($data->car_mile)?></p>
              <p><b>วันหมดอายุ พรบ:&nbsp;</b> <br><?=$data->car_enddate_act?></p>
              <p><b>วันหมดอายุ ประกันภัย:&nbsp;</b> <br><?=$data->car_enddate_insurance?></p>
              <p><b>ราคาเช่า/วัน:&nbsp;</b> <br><?= number_format($data->car_price)?> บาท</p>
              
            </div>
          </div>
          
          <div class="col-sm-3 col-sm-12 col-xs-12">
            <div class="promotion"  style="    min-height: 354px;">
            
            <span class="headet"> อุปกรณ์เสริม</span>
            <div class="border-h"></div>
            

            <?php
              foreach (json_decode($data->car_option,true) as $key => $value) {
            ?>

              <p>
              <svg 
                 width="20px" height="20px" viewBox="0 0 261 265" enable-background="new 0 0 261 265" xml:space="preserve">
              <g>
                <g>
                  <path fill="#00B240" d="M253.537,37.681c-3.84-3.846-10.069-3.853-13.909-0.017L121.621,155.36l-42.577-46.242
                    c-3.679-3.994-9.899-4.252-13.899-0.574c-3.997,3.679-4.253,9.902-0.574,13.899l49.512,53.771
                    c1.813,1.971,4.351,3.115,7.026,3.171c0.072,0.003,0.142,0.003,0.21,0.003c2.6,0,5.102-1.032,6.944-2.869L253.518,51.593
                    C257.367,47.757,257.373,41.527,253.537,37.681z"/>
                </g>
              </g>
              <g>
                <g>
                  <path fill="#00B240" d="M251.163,122.663c-5.433,0-9.837,4.403-9.837,9.837c0,61.112-49.714,110.826-110.826,110.826
                    c-61.109,0-110.827-49.714-110.827-110.826c0-61.109,49.718-110.827,110.827-110.827c5.434,0,9.837-4.403,9.837-9.836
                    c0-5.434-4.403-9.837-9.837-9.837C58.542,2,0,60.542,0,132.5C0,204.455,58.542,263,130.5,263c71.955,0,130.5-58.545,130.5-130.5
                    C261,127.067,256.597,122.663,251.163,122.663z"/>
                </g>
              </g>   <span class="status-text"><?=strtoupper(str_replace('_',' ',$value))?></span>
              </svg>
            </p>

            <?php      
              }
            ?>
            
            </div>
          </div>

                </div>
            </div> <!-- /container -->       
        </section>
    
    
        <section id="" class="sections-1" style="padding-top: 0px;">

              <!-- Example row of columns -->
              <div class="row">
                

              <div class="col-sm-12">
                <div class="force-table-responsive promotion">
                        <span class="headersummary">ข้อมูลการชำระเงิน</span>                       
                        <table class="table table-condensed">
                          <thead>
                            <td class="col-md-6">รายการ</td>
                            <td class=" col-md-2 text-right">จำนวน</td>
                            <td class=" col-md-2 text-right">จำนวนวัน</td>
                          </thead>
                          <tbody>
                            
                            <?php
                              foreach ($data->data_sesstion->items as $key => $value) {

                                 if($data->data_sesstion->drop_off_code == $data->data_sesstion->pick_up_code){
                                    if($value->price != 0){
                                ?>

                              <tr>
                                <td>
                                  <span class=" font-montserrat fs-18 all-caps"><?=$value->name?> <?=$key==0?"(Vat excluded)":""?></span>
                                </td>
                                <td class="text-right">
                                  <span><?=number_format($value->qty)?> หน่วย</span>
                                </td>
                                 <td class="text-right">
                                  <span><?=number_format($data->data_sesstion->total_day)?> วัน</span>
                                </td>
                                <td class=" text-right">
                                  <span><?=number_format($value->price)?> บาท</span>
                                </td>
                                
                              </tr>
                                <?php
                                    }
                                 }else{
                            ?>

                              
                              <tr>
                                <td >
                                  <span class=" font-montserrat fs-18 all-caps"><?=$value->name?> <?=$key==0?"(Vat excluded)":""?></span>
                                </td>
                                <td class="text-right">
                                  <span><?=number_format($value->qty)?></span>
                                </td>
                                <td class=" text-right">
                                  <span><?=number_format($value->price)?> บาท</span>
                                </td>
                              
                              </tr>
                            <?php
                                }
                              }
                            ?>
                            <tr>
                              <td colspan="4" class=" col-md-3 text-right">
                                <h4 class="text-primary no-margin font-montserrat">ช่องทางการชำระเงิน: <?php
                                switch ($data->type_pay) {
                                  case 'pay_later':
                                    echo "ชำระทีหลัง";
                                    break;

                                  case 'paypal':
                                    echo "Paypal";
                                  break;
                                 
                                }

                                ?></h4>
                                <h4 class="text-primary no-margin font-montserrat">สถานะชำระเงิน: <?=$data->paid==0?"<label style='color:red;'>ค้างชำระ</label>":"<label style='color:green;'>ชำระเงินแล้ว</label>"?></h4>
                                <h4 class="text-primary no-margin font-montserrat">รวมราคา <?=number_format($data->price)?> บาท</h4>
                              </td>
                            </tr>

                        </tbody>
                        </table>
                    </div>
              </div>
          

                
            </div> 
            <!-- /container -->       
        </section>
    
  
    
    
  </div>
  
  @endsection




@section('begin_css')
    <link rel='stylesheet' href="{{ asset("plugins/dhtmlxScheduler/dhtmlxscheduler.css")}}" rel="stylesheet">
    <link rel='stylesheet' href="{{ asset("plugins/dhtmlxScheduler/styles.css")}}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">


    <link rel='stylesheet' type='text/css' href='https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css'/>

            <!--For Plugins external css-->
        <link rel="stylesheet" href="assets/css/plugins.css" />
        <link rel="stylesheet" href="assets/css/opensans-web-font.css" />
        <link rel="stylesheet" href="assets/css/montserrat-web-font.css" />

    <!--For font-awesome css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style type="text/css">
.promotion {
      border: 1px solid #cecece;
    padding: 18px;
    border-radius: 6px;
    color: #465065;
}
.id-number{
  font-weight: 600;
      padding-top: 8px;
          font-size: 30px;
    }
.phone-number{
      padding-top: 8px;
          font-size: 24px;
    }
.name{
    font-size: 39px;
    font-weight: 600;
    }
.headpargraph {
    font-weight: 800;
    color: #3379b7;
}

.overlay-img {
    background: rgba(58, 84, 59, 0.88);
    width: 100%;
    padding-top: 17px;
    padding-bottom: 14px;
    color: #ffffff;
}
    

.home-bg {
     background: url(/frontend/img/bg-06.jpg);
    min-height: 225px;
    -moz-background-size: cover;
    -webkit-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    width: 100%;
    overflow: hidden;
  }

.sections {
    padding-top: 80px;
    padding-bottom: 80px;
    position: relative;
    overflow: hidden;
}
.sections-1 {
    padding-top: 30px;
    padding-bottom: 30px;
    position: relative;
    overflow: hidden;
}
.label {
    font-weight: 400!important;
     font-size: 13px!important;
}
.headet{
      font-size: 19px;
    font-weight: 600;
}
.border-h{
        margin-bottom: 8px;
    padding: 5px;
    border-bottom: 1px dashed #d2d2d2;
  }
  .pick-up{
  color: #7d7d7d;
    font-size: 16px;
    font-weight: 600;
}
.status-text {
    position: absolute;
    margin: 5px 0px 0px 15px;
}
.headersummary {

    font-size: 13px;
    color: #333;
    font-family: 'Montserrat';
    font-size: 18px;
    text-transform: uppercase;
    font-weight: 600;
}
.status-box{
        margin-left: 14px;
    border: 1px solid #cecece;
    padding-top: 12px;
    padding-bottom: 11px;
    padding-left: 18px;
    margin-top: 10px;
    width: 48%;
    border-radius: 5px;
    background: #f7f7f7;
}
    </style>
@endsection


  <!--START IMPORT JAVASCRIPT -->
  @section('begin_javascript') 
  <script src="{{ asset("plugins/dhtmlxScheduler/dhtmlxscheduler.js")}}"></script>
  <script src="{{ asset("plugins/dhtmlxScheduler/ext/dhtmlxscheduler_limit.js")}}"></script>
  <script src="{{ asset("plugins/dhtmlxScheduler/ext/dhtmlxscheduler_collision.js")}}"></script>
  <script src="{{ asset("plugins/dhtmlxScheduler/ext/dhtmlxscheduler_timeline.js")}}"></script>
  <script src="{{ asset("plugins/dhtmlxScheduler/ext/dhtmlxscheduler_editors.js")}}"></script>
  <script src="{{ asset("plugins/dhtmlxScheduler/ext/dhtmlxscheduler_minical.js")}}"></script>
  <script src="{{ asset("plugins/dhtmlxScheduler/ext/dhtmlxscheduler_tooltip.js")}}"></script>
  <script src="{{ asset("js/backend/dhtmlxScheduler/mock_backend.js")}}"></script>
  <script src="{{ asset("js/backend/dhtmlxScheduler/scripts.js")}}"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

  <!--START CODING JS-->
  <script>
  $(document).ready(function () {
//////// DataTable ///////
var table = $('#DataTable').DataTable({
  "lengthMenu": [[10,25,50, -1], [10,25,50, "All"]],
  "order": [[ 0, "desc" ]],
  "language": {
    "lengthMenu": "แสดงผล _MENU_ ข้อมูลต่อหน้า",
    "zeroRecords": "Nothing found - sorry",
    "info": "ลำดับหน้าที่ _PAGE_ จาก _PAGES_ หน้า - จำนวนรายการทั้งหมด _TOTAL_ รายการ",
    "infoEmpty": "No records available",
    "infoFiltered": "( ค้นหาข้อมูลจาก _MAX_ รายการ )",
    "search": "<i class='fa fa-search'></i> ค้นหาข้อมูล:",
    "paginate": {
      "first":      "หน้าแรก",
      "last":       "หน้าสุดท้าย",
      "next":       "ถัดไป",
      "previous":   "ก่อนหน้า"
    },
  }
}); // End DataTable //

}); //End jQuery
  </script>
  <!--END CODING JS-->


<script type="text/javascript">
    
$(document).ready(function(){
    $(".phone_wat").text(function(i, text) {
        text = text.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "$1-$2-$3");
        return text;
    });
});

</script>

@endsection


