@extends('backend.layouts.app')

@section ('title', __('สร้าง แบบทดสอบ'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection


@section('content')
<?php
use Carbon\Carbon;
?>
<div class="panel panel-default">
    <div class="panel-heading " style="height: 55px; color: #fff !important;background:#607d8b;">
        <h3 class="panel-title" style="float:left;"><i class="fa fa-database"></i>&nbsp;สร้างคำถามประเภท ปรนัย</h3>
    </div>

    <div class="panel-body" style="padding: 15px;">
        <div class="row">

            <div class="col-md-12">
                 @if ($errors->any())
                  <div class="alert alert-danger" style="width:100%;">
                    <ul>
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif

            </div>
            <!-- START CONTAINER FLUID -->
            <div class="container-fluid">
                <div id="rootwizard">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-tabs-linetriangle nav-tabs-separator nav-stack-sm" style="width: 98%;/*display: table;*/ margin: 0 auto;">
                        
                        <li class="active">
                            <a data-toggle="tab" href="#tab2"><i class="fa fa-credit-card tab-icon"></i> <span>รายการ Booking</span></a>
                        </li>

                        <li >
                            <a data-toggle="tab" href="#tab1"><i class="fa fa-shopping-cart tab-icon"></i> <span>กำหนดรายละเอียดการค้นหา</span></a>
                        </li>
                      


                    </ul>
          <form method="POST" id="my-form" action="#" enctype="multipart/form-data" class="form-horizontal">   
            {{ csrf_field() }}
                        <!-- Tab panes -->
                        <div class="tab-content">
                           

                                <div class="tab-pane active slide-left padding-20" id="tab2" style="background: #dfe5e9;border-radius: 10px;border-bottom: 5px solid #37ab96;">
                                    <div class="row row-same-height">
                                        <div class="col-md-12 b-r b-dashed b-grey">
                                           <div class="panel-heading " style="height: 45px; color: #fff !important;background:#607d8b;">
                                                 
                                                      <h3 class="panel-title" style="float:left;font-size: 15px;"><i class="fa fa-database"></i>&nbsp;ตารางแสดงรายการ Booking</h3>

                                                      <h3 class="panel-title" style="float:right;font-size: 15px;">
                                                        <i style="float: right;" class="fa fa-refresh" id="reload_table_this"></i>
                                                    </h3>
                                                      
                                                  </div>

                                            <div class="col-md-12 col-sm-12 col-xs-12 table-responsive table-red filter-right">       
                                                 <table class="table table-striped table-hover"  id="DataTable">
                                                            <thead>
                                                                <tr>
                                                                    <th class="col-md-2" style="font-size: 12px;">ชื่อ</th>
                                                                    <th class="col-md-2" style="font-size: 12px;">สกุล</th>
                                                                    <th class="col-md-1" style="font-size: 12px;">โทรศัพท์</th>
                                                                    <th class="col-md-1" style="font-size: 12px;">อีเมล</th>
                                                                    <th class="col-md-1" style="font-size: 12px;">รหัสรถยนต์</th>
                                                                    <th class="col-md-1" style="font-size: 12px;">ทะเบียนรถยนต์</th>
                                                                    <th class="col-md-1" style="font-size: 12px;">วันที่รับรถ</th>
                                                                    <th class="col-md-1" style="font-size: 12px;">วันที่คืนรถ</th>
                                                                     <th class="col-md-1" style="font-size: 12px;">สถานะคืนรถ</th>
                                                                    <th class="col-md-1" style="padding-left: 0; text-align: center;">{{ __('labels.general.actions') }}</th>
                                                            </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                  </div>
                                                 
                                        </div>
                                         
                                </div>

                                 <div class="tab-pane  slide-left" id="tab1" style="border-bottom: 5px solid #37ab96;border-radius: 10px;">
                                <div class="row row-same-height">
                                    <div class="col-md-12 b-r b-dashed b-grey" style="background: #dfe5e9 !important;border-radius: 10px;"> 
                                        <div class="padding-30 ">
                                            <div class="form-group-attached">
                                                <div class="row clearfix">
                                                    <h4 style="color: #000;">กำหนดรายละเอียดค้นหารายการ Booking</h4> 
                                                </div>

                                                    <div class="form-group-attached">
                                                            <div class="row clearfix">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group form-group-default required">
                                                                        <label style="font-size:14px;">บริษัท</label>
                                                                        <select class="form-control js-example-basic-single" name="company_name" style="height:40px;">
                                                                         <option>เลือกบริษัท</option>
                                                                            <?php
                                                                                foreach (config('company.'.app()->getLocale().'.name') as $key => $value) {
                                                                                    if($value != null){
                                                                                        echo "<option value=".$key.">";
                                                                                        echo $value[0];
                                                                                        echo "</option>";
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select> 
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <div class="form-group form-group-default required">
                                                                        <label style="font-size:14px;">{{ __('validation.attributes.backend.access.users.position_name') }}</label>
                                                                            <select class="form-control js-example-basic-single" name="position_name" style="height:40px;">
                                                                              <option>สาขา</option>
                                                                                <?php
                                                                                    foreach (config('company.'.app()->getLocale().'.position') as $key => $value) {
                                                                                        if($value != null){
                                                                                            echo "<option value=".$key.">";
                                                                                            echo $value[0];
                                                                                            echo "</option>";
                                                                                        }
                                                                                    }
                                                                                ?>
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>

                                                    <div class="form-group-attached">
                                                            <div class="row clearfix">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group form-group-default required">
                                                                        <label style="font-size:14px;">รหัสรถยนต์</label>
                                                                        <select class="form-control js-example-basic-single" name="company_name" style="height:40px;">
                                                                         <option>เลือกบริษัท</option>
                                                                            <?php
                                                                                foreach (config('company.'.app()->getLocale().'.name') as $key => $value) {
                                                                                    if($value != null){
                                                                                        echo "<option value=".$key.">";
                                                                                        echo $value[0];
                                                                                        echo "</option>";
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select> 
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <div class="form-group form-group-default required">
                                                                        <label style="font-size:14px;">ทะเบียนรถยนต์</label>
                                                                            <select class="form-control js-example-basic-single" name="position_name" style="height:40px;">
                                                                              <option>สาขา</option>
                                                                                <?php
                                                                                    foreach (config('company.'.app()->getLocale().'.position') as $key => $value) {
                                                                                        if($value != null){
                                                                                            echo "<option value=".$key.">";
                                                                                            echo $value[0];
                                                                                            echo "</option>";
                                                                                        }
                                                                                    }
                                                                                ?>
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                            
                                                            </div>

                                                    </div>


                                                 

                                                    <div class="form-group-attached">
                                                            <div class="row clearfix">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group form-group-default required">
                                                                        <label style="font-size:14px;">ชื่อ (ผู้เช่า)</label>
                                                                        <select class="form-control js-example-basic-single" name="company_name" style="height:40px;">
                                                                         <option>เลือกบริษัท</option>
                                                                            <?php
                                                                                foreach (config('company.'.app()->getLocale().'.name') as $key => $value) {
                                                                                    if($value != null){
                                                                                        echo "<option value=".$key.">";
                                                                                        echo $value[0];
                                                                                        echo "</option>";
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select> 
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <div class="form-group form-group-default required">
                                                                        <label style="font-size:14px;">นามสกุล (ผู้เช่า)</label>
                                                                            <select class="form-control js-example-basic-single" name="position_name" style="height:40px;">
                                                                              <option>สาขา</option>
                                                                                <?php
                                                                                    foreach (config('company.'.app()->getLocale().'.position') as $key => $value) {
                                                                                        if($value != null){
                                                                                            echo "<option value=".$key.">";
                                                                                            echo $value[0];
                                                                                            echo "</option>";
                                                                                        }
                                                                                    }
                                                                                ?>
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                            
                                                            </div>

                                                    </div>


                                                    <div class="form-group-attached">
                                                            <div class="row clearfix">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group form-group-default required">
                                                                        <label style="font-size:14px;">หมายเลขโทรศัพท์ (ผู้เช่า)</label>
                                                                        <select class="form-control js-example-basic-single" name="company_name" style="height:40px;">
                                                                         <option>เลือกบริษัท</option>
                                                                            <?php
                                                                                foreach (config('company.'.app()->getLocale().'.name') as $key => $value) {
                                                                                    if($value != null){
                                                                                        echo "<option value=".$key.">";
                                                                                        echo $value[0];
                                                                                        echo "</option>";
                                                                                    }
                                                                                }
                                                                            ?>
                                                                        </select> 
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6" >
                                                                    <div class="form-group form-group-default required">
                                                                        <label style="font-size:14px;">อีเมล (ผู้เช่า)</label>
                                                                            <select class="form-control js-example-basic-single" name="position_name" style="height:40px;">
                                                                              <option>สาขา</option>
                                                                                <?php
                                                                                    foreach (config('company.'.app()->getLocale().'.position') as $key => $value) {
                                                                                        if($value != null){
                                                                                            echo "<option value=".$key.">";
                                                                                            echo $value[0];
                                                                                            echo "</option>";
                                                                                        }
                                                                                    }
                                                                                ?>
                                                                            </select>
                                                                    </div>
                                                                </div>
                                                            
                                                            </div>

                                                    </div>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                                <div class="padding-20 bg-white">
                                    <ul class="pager wizard">
                                        <li class="next">
                                            <button class="btn btn-primary btn-cons btn-animated from-left fa fa-truck pull-right" type="button">
                                                <span>Next</span>
                                            </button>
                                        </li>
                                       
                                        <li class="previous first hidden">
                                            <button class="btn btn-default btn-cons btn-animated from-left fa fa-cog pull-right" type="button">
                                                <span>First</span>
                                            </button>
                                        </li>
                                        <li class="previous">
                                            <button class="btn btn-default btn-cons pull-right" type="button">
                                                <span>Previous</span>
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- END CONTAINER FLUID -->
            </div>
        </div>
    </div>

@endsection


    
@section('begin_javascript')
    <script src="{{ asset("plugins/select2/select2.min.js")}}"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js")}}" type="text/javascript"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/js/form_wizard.js")}}" type="text/javascript"></script>
    <script src="{{ asset("backend/theme/theme-2/assets/plugins/boostrapv3/js/bootstrap.min.js")}}" type="text/javascript"></script>
    <script src="{{ asset("backend/theme/theme-2/pages/js/pages.min.js")}}" type="text/javascript"></script>
    <script src="{{ asset("plugins/ckeditor/ckeditor.js")}}"></script>
    <script src="{{ asset("plugins/nestable/jquery.nestable.js")}}"></script>
    <script src="{{ asset("plugins/sortable/jquery-sortable.js")}}"></script>
    <script src="{{ asset("plugins/magnific/jquery.magnific-popup.js")}}"></script>

    <script src="{{ asset("plugins/fancybox/js/peqjvl.js")}}"></script>


    <script src="http://rubaxa.github.io/Sortable/Sortable.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/css/dataTables.checkboxes.css" rel="stylesheet" />
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/js/dataTables.checkboxes.min.js"></script>
       

    <script>
        $('.js-example-basic-single').select2();
    </script>



     <!--START CODING JS-->
    <script>
            $(document).ready(function () {
                var tk = 0;
                //////// DataTable ///////
                var table = $('#DataTable').DataTable({
                    "lengthMenu": [[10,25,50, -1], [10,25,50, "All"]],
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "type": "GET",
                        "url": "{{ route('ajaxdata.getdata.return.car') }}",
                        "data": function (d) {
                            d.fk_exam_name = jQuery("select[name='fk_exam_name'] option:selected").val();
                            d.company_name = jQuery("select[name='company_name'] option:selected").val();
                            d.position_name = jQuery("select[name='position_name'] option:selected").val();
                            d.management_course_year = jQuery("select[name='management_course_year'] option:selected").val();
                        }
                    },
                    //"order": [[ 2, "desc" ]],
                    "language": {
                        "lengthMenu": "แสดงผล _MENU_ ข้อมูลต่อหน้า",
                        "zeroRecords": "ไม่พบข้อมูล กรุณาระบุข้อมูล",
                        "info": "หน้าที่ _PAGE_ / _PAGES_ หน้า - จำนวน _TOTAL_ รายการ",
                        "infoEmpty": "No records available",
                        "infoFiltered": "( ค้นหาข้อมูลจาก _MAX_ รายการ )",
                        "search": "<i class='fa fa-search'></i> ค้นหาข้อมูล:",
                        "paginate": {
                            "first":      "หน้าแรก",
                            "last":       "หน้าสุดท้าย",
                            "next":       "ถัดไป",
                            "previous":   "ก่อนหน้า"
                        },
                    },
                    "columns":[
                            {data: 'customer_first_name'},
                            {data: 'customer_last_name'},
                            {data: 'customer_phone'},
                            {data: 'customer_email'},
                            {data: 'branch_car_code'},
                            {data: 'license_plate'},
                            {data: 'pick_up_date'},
                            {data: 'drop_off_date'},
                            {data: 'status_return_car'},
                            {data: 'action'}
                    ],

                }); // End DataTable //
                
                        
                jQuery("select[name='fk_exam_name']").change(function(e){
                    table.ajax.reload();
                });

                jQuery("select[name='company_name']").change(function(e){
                    table.ajax.reload();
                });

                jQuery("select[name='position_name']").change(function(e){
                    table.ajax.reload();
                });

                jQuery("select[name='management_course_year']").change(function(e){
                    table.ajax.reload();
                });

                jQuery("i#reload_table_this").on('click',function(){
                    table.ajax.reload();
                });
             

         }); //End jQuery

       </script>
    <!--END CODING JS-->
    

@if(session()->has('message'))

    <script type="text/javascript">
        <?php
            $type = session()->get('message');
            $message = __('alerts.backend.database.'.$type.'');
        ?>

        <?=$type?>(
                '<i class="fa fa-check-square-o" style="color:#fff;padding-right:8px"></i><?=$message?>', {
                    HorizontalPosition: 'right',
                    VerticalPosition: 'top',
                    ShowOverlay: false,
                    TimeShown: 5000,
                    MinWidth:400
        });
    </script>
@endif

@endsection


@section('begin_css')

    <link rel='stylesheet' type='text/css' href='https://datatables.yajrabox.com/css/datatables.bootstrap.css'/>

<link href="{{ asset("plugins/fancybox/css/peqjvl.css")}}" rel="stylesheet">
<style type="text/css">
    table#DataTable > tbody >  tr > td{
         text-align: center;
         vertical-align: initial;
    }

    table#DataTable > tbody >  tr > td:nth-child(2),table#DataTable > tbody >  tr > td:nth-child(3){
          text-align: left;
    }

   .table-striped td:last-child {
        text-align: center !important;
}

::-webkit-scrollbar {
    width: 0px;
}

.fancybox-content{
    padding: 36px !important;
}

.fancybox-close-small svg{
        border-radius: 50%;
        background: tomato;
color: #fff;
}

</style>

    <link href="{{ asset("backend/theme/theme-2/assets/plugins/font-awesome/css/font-awesome.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("plugins/select2/select2.css")}}" rel="stylesheet">
    <link class="main-stylesheet" href="{{ asset("backend/theme/theme-2/pages/css/pages.css")}}" rel="stylesheet" type="text/css" />
    <link rel='stylesheet' type='text/css' href='https://datatables.yajrabox.com/css/datatables.bootstrap.css'/>

    <link rel="stylesheet" href="{{ asset("plugins/bootstrapMaterialDatePicker/css/bootstrap-material-datetimepicker.css")}}" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <style type="text/css">

        tbody {
            text-align: center;
        }
        .form-horizontal .form-group{
                padding-top: 12px;
                padding-bottom:5px;
        }
        .form-group-default{
            border:none !important;
        }


            div#rootwizard li.active {
                  border-bottom: 3px solid #37ab96;
            }
          span.select2.select2-container.select2-container--default {
            border-bottom: 1px solid #30ac96 !important;
          }

          .btn.btn-sm:hover{
            background-color: blanchedalmond !important;
          }

          div#DataTable_wrapper{
            border: 1px solid #5d5d5d;
            padding: 13px;
            background: #fff;
            
          }

           .select2-container{
            width: 100% !important;
          }

        .select2-container--default.select2-container--focus .select2-selection--multiple,.select2-container--default .select2-selection--multiple{
            border: none !important;
        }

        ul#editable li label {
            font-size: 13px;
        }

        .dataTables_wrapper .dataTables_info{
            padding:0px;
        }

        .bg-master-light {
            background-color: #e6e6e6;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0px;
        }
        .padding-l-0{
            padding-left: 0px !important;
        }
        .drag-handle {

            font: bold 20px Sans-Serif;
            color: #5F9EDF;
            display: inline-block;
            cursor: move;
            cursor: -webkit-grabbing;
        }

        .inden_1{
            text-indent: 1px;
        }

        .sortable-ghost{
            background-color: #DFE5E9 !important;
        }



        div#area_type_exam li {}

        div#area_type_exam li {
            position: relative;
            padding-top: 12px;
            padding-bottom: 35px;
            background: #fff;
            border-bottom: 1px solid rgba(230, 230, 230, 0.7);
            border-top: 0px;
            font-size: 13.5px;
            cursor: move;
            text-indent: 14px;
        }

        div#area_type_exam i {
            -webkit-transition: opacity .2s;
            transition: opacity .2s;
            opacity: 0;
            display: block;
            cursor: pointer;
            color: #c00;
            top: 19px;
            right: 0px;
            position: absolute;
            font-style: normal;
        }

        div#area_type_exam li:hover i {
            opacity: 1;
        }




        #editable {}

        #editable li {
            position: relative;
            padding-top: 12px;
            padding-bottom: 35px;
            background: #fff;
            border-bottom: 1px solid rgba(230, 230, 230, 0.7);
            border-top: 0px;
            font-size: 13.5px;
            cursor: move;
            text-indent: 14px;
        }

        #editable i {
            -webkit-transition: opacity .2s;
            transition: opacity .2s;
            opacity: 0;
            display: block;
            cursor: pointer;
            color: #c00;
            top: 19px;
            right: 40px;
            position: absolute;
            font-style: normal;
        }

        #editable li:hover i {
            opacity: 1;
        }


        #editable_tab3 {}
        #editable_tab3 li {
            position: relative;
            padding-top: 12px;
            padding-bottom: 35px;
            background: #fff;
            border-bottom: 1px solid rgba(230, 230, 230, 0.7);
            border-top: 0px;
            font-size: 13.5px;
            cursor: move;
            text-indent: 14px;
        }

        #editable_tab3 i {
            -webkit-transition: opacity .2s;
            transition: opacity .2s;
            opacity: 0;
            display: block;
            cursor: pointer;
            color: #c00;
            top: 19px;
            right: 40px;
            position: absolute;
            font-style: normal;
        }

        #editable_tab3 li:hover i {
            opacity: 1;
        }

        .nav.nav-tabs > li > a{
            background: transparent;
            box-shadow: none;
            border-color: transparent;
        }

        .nav.nav-tabs > li.active,.nav.nav-tabs > li.active >a {
            background-color: #dfe5e9;
            color: #121212;
            border-top-right-radius: 5px;
            border-top-left-radius: 5px;
        }

        .fix_label{
            font-size: 13px !important;
            color: gray;
            font-weight: 200;
        }

        .pager{
            margin:0;
        }

        span[class^='select2 select2-container select2-container--default']{
            width: 100% !important;
        }

        .select2-container--default .select2-selection--single {
            background-color: none !important;
            border:  none !important;
            border-radius:  none !important;
        }
        .list-group-item {
            cursor: move;
            cursor: -webkit-grabbing;
        }

        #droppable {
            border: 2px dashed #f60;
            min-height: 100px;
            position: relative;
        }

        #droppable::before {
            color: #ccc;
            font-size: 40px;
            content: 'Drop zone';
            display: block;
            text-align: center;
            padding-top: 15px;
        }

        #droppable > * {
            top: -2px;
            left: -2px;
            right: -2px;
            bottom: -2px;
            border: 2px dashed #060;
            position: absolute;
            line-height: 70px;
            font-size: 50px;
            text-align: center;
            text-decoration: line-through;
        }

        select.input-sm{
            line-height: normal !important;
        }

        table#DataTable tbody tr td {
            padding: 0px !important;
    padding-top: 6px !important;
    padding-bottom: 0px !important;
    vertical-align: baseline;
        }

        table#DataTable tbody tr td > p{
            margin: 0px !important;
        }

        div#DataTable_filter{
            float:right;
        }

        td.dataTables_empty{
            text-align: -webkit-center;
        }

        .js-remove-a{
            display: none;
        }

        .panel-default>.panel-heading {
            padding: 15px 15px 8px 15px;
            background-color: #dfe5e9;
            border-color: #fff;
            color: #1e9178;
            font-weight: 500;
        }

        .pagination > .active > a{
            background-color: #2b2e3329 !important;
        }
        .form-group-default .form-control {
            border-bottom: 1px solid #30ac96 !important;
        }


        div#rootwizard a:hover {
    color: #ffffff !important;
    text-decoration: none !important;
    background: #35383f !important;
}

    </style>

@endsection