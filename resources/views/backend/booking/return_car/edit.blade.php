@extends ('backend.layouts.app')

@section ('title', __('สร้างรายละเอียด ข้อมูลรถยนต์'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('begin_css')
<link href="{{ asset("plugins/datetimepicker/jquery.datetimepicker.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.date.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.time.css")}}" rel="stylesheet">
<link href="{{ asset("backend/dependentdrop/dependentdrop.css")}}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset("plugins/chosen/chosen.css")}}" />
<link rel="stylesheet" href="{{ asset("plugins/chosen/chosenImage/chosenImage.css")}}"/>
<link rel="stylesheet" href="{{ asset("plugins/wizard/wizard.css")}}" />
<link rel="stylesheet" href="{{ asset("plugins/jquery-steps/jquery.steps.css")}}" />
    

    <link rel="icon" type="image/png" href="{{ asset("vendorto/images/icons/favicon.ico")}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset("vendorto/animate/animate.css")}}">
    <link rel="stylesheet" type="text/css" href="{{ asset("vendorto/css-hamburgers/hamburgers.min.css")}}">
    <link rel="stylesheet" type="text/css" href="{{ asset("vendorto/animsition/css/animsition.min.css")}}">
    <link rel="stylesheet" type="text/css" href="{{ asset("vendorto/select2/select2.min.css")}}">
    <link rel="stylesheet" type="text/css" href="{{ asset("vendorto/daterangepicker/daterangepicker.css")}}">
    <link rel="stylesheet" type="text/css" href="{{ asset("css/util.css")}}">
    <link rel="stylesheet" type="text/css" href="{{ asset("css/main.css")}}">
    <link href="{{ asset("upload/dropzone.css")}}" rel="stylesheet" type="text/css">


<link rel="stylesheet" href="{{ asset("mdtime/mdtimepicker.css")}}" />
<link rel="stylesheet" href="{{ asset("material-datetime/bootstrap-material-datetimepicker.css")}}" />

    


<style type="text/css">
    .chosen-single{
        line-height: 32px !important;
        height: 32px !important;
    }

    div.payan-chana ul.chosen-choices{
            height: 33px !important;
    }

    div#select_number ul.chosen-results li{
         background-size: 0px !important;
    }

    div#brand_cars ul.chosen-results li:first-child{
        background-size: 0px !important;
        background-position-x: 12px;
    }

    div#brand_cars ul.chosen-results li{
        background-size: 27px !important;
        text-indent: 7px;
        padding-top: 6px;
        -webkit-backface-visibility: hidden;
        -webkit-transform: translateZ(0) scale(1.0, 1.0);
    }
   
    div#car_type ul.chosen-results li{
        background-size: 40px !important;
        text-indent: 26px;
    }
    div#car_type ul.chosen-results li:first-child{
        background-size: 0px !important;
        background-position-x: 12px;
    }

    div#total_human ul.chosen-results li:first-child{
        background-size: 0px !important;
    }

    div#total_bag ul.chosen-results li:first-child{
        background-size: 0px !important;
    }
    div.fule_type > div.ui-checkbox{
            margin-top: 0;
    }
    div.fule_type > div.ui-checkbox label{
           font-size: 14px !important;
    }


.registration-ui {
 /*   background: linear-gradient(to bottom, #f8d038 0%,#f5ca2e 100%);*/
   /* padding: .25em 1em .25em 1.75em;*/
   /*padding:0 .7em 1.14em .75em;
    font-weight: bold;
    font-size: 2em;
    border-radius: 5px;
    border: 1px solid #000;
    box-shadow: 1px 1px 1px #ddd;
    position: relative;
    font-family: helvetica, ariel, sans-serif;*/
    padding: 12.4px .7em 0.7em .75em;
    font-weight: bold;
    font-size: 2em;
    border-radius: 5px;
    border: 1px solid #000;
    box-shadow: 1px 1px 1px #ddd;
    position: relative;
    font-family: helvetica, ariel, sans-serif;
}

.registration-ui:before {
    /*content: 'ECOCAR';
    display: block;
    width: 30px;
    height: 100%;
    background: #063298;
    position: absolute;
    top: 0;
    border-radius: 5px 0 0 5px;
    color: #f8d038;
    font-size: .5em;
    line-height: 85px;
    padding-left: 5px;*/
    content: 'ECOCAR';
    display: block;
    text-align: center;
    width: 100%;
    height: 18px;
    background: #159077;
    position: absolute;
    top: 46.1px;
    border-radius: 5px 0 0 5px;
    color: #ffffff;
    font-size: .4em;
    line-height: 19px;
    left: 0px;
    border-bottom-right-radius: 4px;
    border-top-left-radius: 0;
    border-bottom-left-radius: 4px;
}

.registration-ui:after {
    content: '';
    display: block;
    position: absolute;
    top: 7px;
    left: 5px;
    width: 20px;
    height: 20px;
    border-radius: 30px;
   /* border: 1px dashed #f8d038;*/
}
.alert {
    padding: 20px;
    background-color: #f44336;
    color: white;
}
.closebtn {
    margin-left: 15px;
    color: white;
    font-weight: bold;
    float: right;
    font-size: 22px;
    line-height: 20px;
    cursor: pointer;
    transition: 0.3s;
}

.closebtn:hover {
    color: black;
}
.two {
    border: 1px solid lightgray;
    border-radius: 5px;
    outline: white solid 10px;
    margin: 5px;
    padding: 10px;
    width: 100%;
}
table.table{
    border: 0;
}
.contact100-more{
    width:100%;
    height: 100%;
}
.dropzone{
            border-style: dashed;
            border-color: #01A78B;
            border-radius: 5px;
        }
.txt1{
    font-size: 20px;
}
.wrap-contact100{
    width: 100%;
}
.label-input100{
    font-size: 16px;
    color: black;
}
.content-pic{
    padding-left: 30px;
    padding-bottom: 10px;
    padding-top: 10px;
    padding-right: 30px;
}
textarea.input100{
    background-color: white;
    color: #000;
}
table.txt2{
    color: #fff;
}
table, th, td {
    font-size:15px;
}
.btn.focus, .btn:focus, .btn:hover{
    color: #fff;
}
.btn-secondary {
    color: #fff;
    background-color: #5d666b;
}

</style>
@endsection

@section('content')


<?php

// echo "<pre>";
// print_r($data);
// echo "</pre>";
?>


<div class="panel panel-default">
    <div class="panel-heading " style="height: 55px; color: #fff !important;background:#00a788;">
        <h3 class="panel-title" style="float:left;"><i class="fa fa-database"></i>&nbsp;บันทึกการคืนรถ</h3>
    </div>

    <div class="panel-body">
        <div class="row">
            <form method="POST" action="{{ route('admin.booking.return-car.update',$data->id) }}" enctype="multipart/form-data" class="form-horizontal" id="my-form">
                {{ csrf_field() }}
                 {{ method_field('PATCH') }}
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="panel bd-t-red">    

                                @if ($errors->any())
                                    <div class="alert alert-danger" style="width:100%;">
                                      <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                      </ul>
                                    </div>
                                @endif
                             
                        </div>
                    </div>
                </div>

                                                <div>
                                                    <div class="wrap-contact100">
                                                        <div class="col-md-8 col-sm-12" style="padding-bottom: 20px;">
                                                                <!-- <span class="contact100-form-title" style="padding-top: 20px;">
                                                                    <U>แบบฟอร์มคืนรถ</U>
                                                                </span> -->
                                                            <div class="col-md-6" style="padding-left: 0; padding-right: 7px;">
                                                                <label class="label-input100" for="nameEmp"><i class="fa fa-calendar"></i>&nbsp;วันที่คืนรถ</label>
                                                                <div class="wrap-input100 content-pic">
                                                                   <input class=" form-control pickadate"  name="return_date" type="text" value="<?=old('return_date')?>" >
                                                                    <!-- <div class="form-group label-floating is-empty">
                                                                        <input type="text" id="date11" class="form-control" data-dtp="dtp_elaPp">
                                                                    </div> -->
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" style="padding-left: 7px; padding-right: 0; padding-bottom: 14px;">
                                                                <label class="label-input100" for="nameEmp"><i class="fa fa-clock-o"></i>&nbsp;เวลาคืนรถ</label>
                                                                <div class="wrap-input100 content-pic">
                                                                    <!-- <input class=" form-control" name="return_time" type="text" id="timepicker" value="<?=old('return_time')?>" /> -->

                                                                    <!-- <div class="md-form">
                                                                        <input placeholder="Selected time" type="text" id="input_starttime" class="form-control timepicker">
                                                                    </div> -->
                                                                    <div class="form-group label-floating is-empty" style="margin-bottom: 0px;">
                                                                        <input type="text" id="time11" name="return_time" class="form-control" data-dtp="dtp_LeGYU" value="<?=old('return_time')?>">
                                                                    </div>
                                                                    <!-- <input class=" form-control pickatime"  name="return_time" type="text" value="<?=old('return_time')?>"> -->
                                                                </div>
                                                            </div>

                                                                <label class="label-input100" for="detail"><i class="fa fa-file-text-o"></i>&nbsp;รายละเอียด</label>
                                                                <div class="wrap-input100 validate-input" data-validate = "Message is required">
                                                                    <textarea id="detail" class="input100" name="return_remark" placeholder=""></textarea>
                                                                    <span class="focus-input100"></span>
                                                                </div>

                                                                <!-- <label class="label-input100" for="uploadpic"><i class="fa fa-file-image-o"></i>&nbsp;upload รูปภาพ</label>
                                                                <div class="wrap-input100">
                                                                    <div class="content content-pic">
                                                                        <button type="button" class="btn btn-secondary btn-pic" data-toggle="modal" data-target="#myModal"><i class="fa fa-upload"></i>&nbsp;คลิกที่นี่เพื่อ upload รูป</button>
                                                                    </div>
                                                                </div> -->
                                                                <label class="label-input100" for="nameEmp"><i class="fa fa-user"></i>&nbsp;พนักงานรับรถ</label>
                                                                <div class="wrap-input100 content-pic">
                                                                    <select class="js-example-basic-single col-sm-12" name="fk_staff_receive" style="height:33px;">
                                                                       <?php
                                                                        foreach ($staff as $key => $value) {
                                                                            echo "<option value=".$value->id.">".$value->first_name.' '.$value->last_name."</option>";
                                                                        }
                                                                       ?>
                                                                            
                                                                    </select>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <ul class="list-inline" style="float:right; padding-top:20px;">
                                                                            <li><button type="submit" class="btn btn-success "><i class="fa fa-save"></i>&nbsp;บันทึก</button></li>
                                                                            <li><a class="btn btn-warning prev-step" href="{{ route('admin.auth.user.index') }}"><i class="fa fa-times"></i>&nbsp;ยกเลิก</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                        <div class="col-md-4 col-sm-12" style="padding-top: 15px; padding-bottom: 15px;">
                                                            <div class="contact100-more flex-col-c-m" style="background-image: url('{{ asset("vendorto/images/photo-car.jpg")}}');">
                                                                <div class="flex-w size1 p-b-47">
                                                                    <div class="txt1 p-r-25">
                                                                        <span class="lnr lnr-map-marker"></span>
                                                                    </div>

                                                                    <div class="flex-col size2">

                                                                        <span class="txt1 p-b-20">
                                                                            <i class="fa fa-user" style="color: #fff;"></i>&nbsp;&nbsp;ข้อมูลลูกค้า
                                                                        </span>

                                                                        <span>
                                                                            <table class="txt2">
                                                                                <tr>
                                                                                    <td align="right">คุณ:</td>
                                                                                    <td>&nbsp;&nbsp;</td>
                                                                                    <td><?php print_r($data->customer_first_name); ?>&nbsp;<?php print_r($data->customer_last_name); ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">อีเมล:</td>
                                                                                    <td>&nbsp;&nbsp;</td>
                                                                                    <td><?php print_r($data->customer_email); ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">เบอร์โทร:</td>
                                                                                    <td>&nbsp;&nbsp;</td>
                                                                                    <td><?php print_r($data->customer_phone); ?></p></td>
                                                                                </tr>
                                                                            </table>
                                                                            
                                                                        </span>
                                                                    </div>
                                                                </div>

                                                                <div class="dis-flex size1 p-b-47">
                                                                    <div class="txt1 p-r-25">
                                                                        <span class="lnr lnr-phone-handset"></span>
                                                                    </div>

                                                                    <div class="flex-col size2">
                                                                        <span class="txt1 p-b-20">
                                                                            <i class="fa fa-car" style="color: #fff;"></i>&nbsp;ข้อมูลรถยนต์
                                                                        </span>

                                                                        <span >
                                                                            <table class="txt2">
                                                                                <tr>
                                                                                    <td align="right">แบรนด์:</td>
                                                                                    <td>&nbsp;&nbsp;</td>
                                                                                    <td><?php print_r($data->car_brand); ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">รุ่น:</td>
                                                                                    <td>&nbsp;&nbsp;</td>
                                                                                    <td><?php print_r($data->car_generation); ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">&nbsp;ทะเบียน:</td>
                                                                                    <td>&nbsp;&nbsp;</td>
                                                                                    <td><?php print_r($data->license_plate); ?>&nbsp;<?php print_r($data->license_province); ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">สี:</td>
                                                                                    <td>&nbsp;&nbsp;</td>
                                                                                    <td><?php print_r($data->color); ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">สาขา:</td>
                                                                                    <td>&nbsp;&nbsp;</td>
                                                                                    <td><?php print_r($data->branch_name); ?></td>
                                                                                </tr>
                                                                            </table>
                                                                        </span>
                                                                    </div>
                                                                </div>

                                                                <div class="dis-flex size1 p-b-47">
                                                                    <div class="txt1 p-r-25">
                                                                        <span class="lnr lnr-phone-handset"></span>
                                                                    </div>

                                                                    <div class="flex-col size2">
                                                                        <span class="txt1 p-b-20">
                                                                            <i class="fa fa-file-text" style="color: #fff;"></i>&nbsp;ข้อมูลการเช่า
                                                                        </span>

                                                                        <span >
                                                                            <table class="txt2">
                                                                                <tr>
                                                                                    <td align="right">สาขาที่รับรถ:</td>
                                                                                    <td>&nbsp;&nbsp;</td>
                                                                                    <td><?php print_r($data->pick_up); ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">&nbsp;วันที่รับรถ:</td>
                                                                                    <td>&nbsp;&nbsp;</td>
                                                                                    <td><?php  $originalDate = $data->pick_up_date;
                                                                                        print_r($newDate = date("d-m-Y", strtotime($originalDate)));?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">เวลารับรถ:</td>
                                                                                    <td>&nbsp;&nbsp;</td>
                                                                                    <td><?php print_r($data->pick_up_time); ?>&nbsp;น.</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">สาขาที่คืนรถ:</td>
                                                                                    <td>&nbsp;&nbsp;</td>
                                                                                    <td><?php print_r($data->drop_off); ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">วันที่คืนรถ:</td>
                                                                                    <td>&nbsp;&nbsp;</td>
                                                                                    <td><?php  $originalDate = $data->drop_off_date;
                                                                                        print_r($newDate = date("d-m-Y", strtotime($originalDate)));?>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">เวลารับรถ:</td>
                                                                                    <td>&nbsp;&nbsp;</td>
                                                                                    <td><?php print_r($data->drop_off_time); ?>&nbsp;น.</td>
                                                                                </tr>
                                                                            </table>
                                                                        </span>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                    
                
                <input type="hidden" name="car_booking" value="{{$data->car_booking_id}}">
                <input type="hidden" name="car_id" value="{{$data->fk_car_id}}">
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><i class="fa fa-file-photo-o"></i>&nbsp;Upload รูปภาพ</h4>
        </div>
        <div class="modal-body">
         <form action="upload.php" class="dropzone dz-clickable" id="myAwesomeDropzone">
            {{ csrf_field() }}
            <div class="dz-message">ลากรูปที่ต้องการใส่หรือคลิกเพื่ออัพโหลด</div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-check"></i>&nbsp;Confirm</button>
        </div>
      </div>
      
    </div>
  </div>




@endsection


<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript') 
<script src="{{ asset("plugins/ckeditor/ckeditor.js")}}"></script>
<script src="{{ asset("plugins/bootstrap-progressbar/bootstrap-progressbar.js")}}"></script>

<script src="{{ asset("upload/dropzone.js")}}"></script>
<script src="{{ asset("vendorto/animsition/js/animsition.min.js")}}"></script>
<script src="{{ asset("vendorto/bootstrap/js/bootstrap.min.js")}}"></script>
<script src="{{ asset("vendorto/select2/select2.min.js")}}"></script>
<script src="{{ asset("vendorto/daterangepicker/moment.min.js")}}"></script>
<script src="{{ asset("vendorto/daterangepicker/daterangepicker.js")}}"></script>
<script src="{{ asset("vendorto/countdowntime/countdowntime.js")}}"></script>
<script src="{{ asset("vendorto/js/main.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.date.js")}}"></script>
<script src="{{ asset("plugins/pickadate/picker.time.js")}}"></script>
<script src="{{ asset("mdtime/mdtimepicker.js")}}"></script>
<script src="{{ asset("material-datetime/bootstrap-material-datetimepicker.js")}}"></script>


<script  type="text/javascript">
    $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>

    <script type="text/javascript">
   
        $(document).ready(function(){
            $('#time11').bootstrapMaterialDatePicker({ 
                date: false,
                format: 'HH:mm',
                themes: 'dark-mint-gradient'
            });
        });
    </script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-23581568-13');
    </script>

 <script type="text/javascript">
//       CKEDITOR.replace('customers_description',{
//            toolbar: [
//               { name: 'tools', items: [ 'Maximize'] },
//               { name: 'insert', items: ['Format' ] },
//               { name: 'document', items: [ '-', 'Undo', 'Redo' ] },
//               { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
//               { name: 'insert', items: ['Table', 'HorizontalRule', 'SpecialChar' ] },
//               { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] },
//               { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
//               { name: 'colors', groups: [ 'colors' ] },
//             ]
//         });

// </script>

<script type='text/javascript'>
        Dropzone.autoDiscover = false;
        $(".dropzone").dropzone({
            addRemoveLinks: true,
            removedfile: function(file) {
                var name = file.name;    
                
                $.ajax({
                    type: 'POST',
                    url: "{{ url('upload.php')}}",
                    data: {name: name,request: 2},
                    sucess: function(data){
                        console.log('success: ' + data);
                    }
                });
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            }
        });
        </script>

@endsection
