@extends('backend.layouts.app')

@section('content')

<div class="panel panel-default">

		<!-- Panel Heading -->
		<!-- Page Content -->
		<div class="container">

		    <div class="row" style="padding-top: 35px; padding-bottom: 35px;">

		        <!-- Post Content Column -->
		        <div class="col-lg-12">

		            <!-- Title -->
		            <h2 class="mt-4">{{ $data->title }}</h2>

		            <hr>

		            <!-- Date/Time -->
		            <p>Posted on {{ $data->created_at }}</p>

		            <hr>

		            <!-- Preview Image -->
		            <img style="width:100%;" src="<?=asset('article/'.$data->image_full.'') ?>" class="attachment-full size-full wp-post-image"></a>  

		            <hr>

		            <!-- Post Content -->
		            <?=$data->description ?>

		            <hr>

		        </div>

		        <!-- Sidebar Widgets Column -->
		        <div class="col-md-4" style="display:none;">

		            <!-- Search Widget -->
		             <!--
		            <div class="card my-4">
		                <div class="card-header">Search</div>
		                <div class="card-body">
		                    <div class="input-group">
		                        <input type="text" class="form-control" placeholder="Search for...">
		                        <span class="input-group-btn">
		                  <button class="btn btn-secondary" type="button">Go!</button>
		                </span>
		                    </div>
		                </div>
		            </div>
		            -->

		            <!-- Categories Widget -->
		            <!--
		            <div class="card my-4">
		                <div class="card-header">Categories</div>
		                <div class="card-body">
		                    <div class="row">
		                        <div class="col-lg-6">
		                            <ul class="list-unstyled mb-0">
		                                <li>
		                                    <a href="#">วิธีดูแลรถยนต์</a>
		                                </li>
		                                <li>
		                                    <a href="#">เช็คสภาพรถยนต์ </a>
		                                </li>
		                                <li>
		                                    <a href="#"> ประกันภัยรถยนต์ </a>
		                                </li>
		                            </ul>
		                        </div>
		                        <div class="col-lg-6">
		                            <ul class="list-unstyled mb-0">
		                                <li>
		                                    <a href="#">บริษัทประกันภัย</a>
		                                </li>
		                                <li>
		                                    <a href="#">ประกันภัยรถยนต์</a>
		                                </li>
		                                <li>
		                                    <a href="#">เคลม</a>
		                                </li>
		                            </ul>
		                        </div>
		                    </div>
		                </div>
		            </div>
		             -->

		            <!-- Side Widget -->
		            <!--
		            <div class="card my-4">
		                <div class="card-header">Tag</div>
		                <div class="card-body">
		                     <button type="button" class="btn btn-primary btn-sm">รถเช่า </button>
							  <button type="button" class="btn btn-success btn-sm">บทความ <span class="badge">3</span></button>    
							  <button type="button" class="btn btn-danger btn-sm">ข้อควรระวัง <span class="badge">5</span></button>    

							  <button type="button" class="btn btn-success btn-sm">คาร์แคร์ <span class="badge">2</span></button>
							  <button type="button" class="btn btn-info btn-sm">เทคนิค <span class="badge">13</span></button>    
							  <button type="button" class="btn btn-warning btn-sm">ล้างรถ <span class="badge">9</span></button>   

							  <button type="button" class="btn btn-danger btn-sm">รถยนต์ <span class="badge">2</span></button>
							  <button type="button" class="btn btn-success btn-sm">ล้างห้องเครื่อง <span class="badge">2</span></button>    
							  <button type="button" class="btn btn-danger btn-sm">ความรู้ยานยนต์<span class="badge">1</span></button>   
		                </div>
		            </div>
		             -->

		        </div>

		    </div>
		    <!-- /.row -->

		</div>
		<!-- /.container -->	

</div>



@endsection



<!-- ใส่ CSS -->

@section('begin_css')
	<style type="text/css">

.my-4 {
margin-bottom: 1.5rem!important;
}

.my-4 {
    margin-top: 1.5rem!important;
}
.card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}
.card-header:first-child {
    border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
}
.card-header {
	    font-weight: 800;
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0,0,0,.03);
    border-bottom: 1px solid rgba(0,0,0,.125);
}

.card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1.25rem;
}
.btn.btn-sm {
    margin: 4px;
}
	</style>


@endsection

<!-- ใส่ Javascript -->
@section('begin_javascript')

@endsection