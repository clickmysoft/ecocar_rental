@extends('backend.layouts.app')


@section('content')


<?php
  // echo "<pre>";
  // print_r($data->tag);
  // echo "</pre>";
  // exit;
?>

<div class="panel panel-default">
    <!-- Panel Heading -->
        <div class="panel-heading " style="height: 55px; color: #fff !important;background:#00a788;">
        <h3 class="panel-title" style="float:left;"><i class="fa fa-database"></i>&nbsp;แก้ไขบทความ</h3>
    </div>

    <!-- Panel body -->
    <div class="panel-body">

        <div class="row">

            <div class="col-md-12 m-t-20">
                <form method="POST" action="{{ route('admin.articles.articles.update',$data->id) }}" enctype="multipart/form-data" class="form-horizontal" id="my-form">
                         {{ csrf_field() }}
                           {{ method_field('PATCH') }}

                     <div class="form-group row"> 
                        <label class="col-md-2 col-form-label text-right" for="exampleFormControlFile1" style="display: inherit; ">เลือกประเภทบทความ</label>
                        <div class="col-md-10" >
                            <select class="form-control select2" name="type_category[]" multiple="multiple" required>
                                <?php
                                  foreach ($data['type_category'] as $key => $value) {
                                ?>
                                    <option value="<?=$value->id?>" 
                                <?php
                                    foreach ($data->category as $key123 => $value456) {
                                      if($value->id == $value456['category_id'] ){
                                        echo " selected";
                                      }
                                    }

                                ?>
                                    ><?=$value->category_name?></option>
                                <?php
                                  }
                                ?>
                          </select>
                        </div>
                    </div>



                  
                    <div class="form-group row"> 
                        <label class="col-md-2 col-form-label text-right" for="exampleFormControlFile1" style="display: inherit; ">เลือก Tags</label>
                        <div class="col-md-10" >
                            <select class="form-control select2" name="tags[]" multiple="multiple" required>

                                <?php
                                  foreach ($data['tags'] as $key => $value) {
                                ?>
                                   <option value="<?=$value->id?>"
                                <?php
                                    foreach ($data->tag as $key233 => $value333) {
                                      if($value->id == $value333['tag_id'] ){
                                        echo " selected";
                                      }
                                    }
                                ?>
                                   ><?=$value->tag_name?></option>
                                <?php
                                  }
                                ?>

                          </select>
                        </div>
                    </div>


                    <div class="form-group row" >
                        <label for="title" class="col-md-2 col-form-label text-right">หัวข้อบทความ</label>
                        <div class="col-md-10" >
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-font" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" name="title" value="{{ $data->title }}">
                            </div>
                        </div>
                    </div>

                    
                    <div class="form-group ">
                              <label class="col-md-2 control-label" for="password">รูปภาพตัวอย่าง</label>
                              <div class="col-md-10"> 

                                        <div class="avatar-upload">
                                         
                                            <div class="avatar-preview">
                                              <img id="imagePreview" src="{{ asset('frontend/article/'.$data->code.'/files/'.$data->image_thumbnail) }}" style="width: 20%;">
                                            </div>
                                        </div>

                              </div>
                      </div>

                     <div class="form-group row"> 
                        <label class="col-md-2 col-form-label text-right" for="exampleFormControlFile1" style="display: inherit; ">เลือกภาพทความ (ขนาดย่อ 348 X 261)</label>
                        <div class="col-md-10" >
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-file-image-o"></i>
                                </span>
                                <input class=" form-control" name="image_thumbnail" type="file" id="imageUpload" accept=".png, .jpg, .jpeg" >
                            </div>
                        </div>
                    </div>
                                                                  

                    <div class="form-group row" style="display: none; "> 
                        <label class="col-md-2 col-form-label text-right" for="exampleFormControlFile1" style="display: inherit; ">เลือกภาพบทความ (ขนาดขนาดเต็ม)</label>
                        <div class="col-md-10" >
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-file-image-o"></i>
                                </span>
                                <input class=" form-control" name="image_full" type="file">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="short-description" class="col-md-2 col-form-label text-right">เนื้อหาบทความ</label>
                        <div class="col-md-10" >
                            <textarea name="description" class="description" cols="45" rows="5"><?=$data->description?></textarea>
                        </div>
                    </div>

                    <div class="bd-t-red" >
                        <div class="panel-heading" style="padding-right: 0;">
                            <h3 class="panel-title">
                                <i class="fa fa-gears"></i>Optimize SEO Onpage</h3>

                                <div class="form-group row" >
                                    <label for="title" class="col-md-2 col-form-label text-right">Meta Title Tag</label>
                                    <div class="col-md-10" style="padding-left: 4px;">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-font" aria-hidden="true"></i>
                                            </span>
                                            <input type="text" class="form-control" name="mata_title" value="<?=$data->mata_title?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row" >
                                    <label for="title" class="col-md-2 col-form-label text-right">Meta Description Tag</label>
                                    <div class="col-md-10" style="padding-left: 4px;">

                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-font" aria-hidden="true"></i>
                                            </span>
                                            <input type="text" class="form-control" name="mata_description" value="<?=$data->mata_description?>">
                                        </div>

                                  
                                    </div>
                                </div>

                                <div class="form-group row" >
                                    <label for="title" class="col-md-2 col-form-label text-right">Meta Keywords Tag</label>
                                    <div class="col-md-10" style="padding-left: 4px;" >

                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-font" aria-hidden="true"></i>
                                            </span>
                                            <input type="text" class="form-control" name="mata_keyword" value="<?=$data->mata_keyword?>">
                                        </div>
                                        
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="control-label col-sm-2 text-right" for="pwd">
                                        สถานะการทำงาน:
                                    </label>
                                    <div class="col-sm-10" style="padding-left: 4px;">        
                                        <select class="form-control chosen-img" name="status" style="height:50px;">
                                            <option <?=old('status')=='enabled'?'selected':'';?> data-img-src="{{ asset("backend/img/checked.svg")}}" value="enabled">เปิดการใช้งาน</option>
                                            <option <?=old('status')=='closed'?'selected':'';?> data-img-src="{{ asset("backend/img/multiply.svg")}}" value="closed">ปิดการใช้งาน</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <ul class="list-inline" style="float:right;">
                                            <li><button type="submit" class="btn btn-success "><i class="fa fa-save"></i>&nbsp;บันทึก</button></li>
                                            <li><a class="btn btn-warning prev-step" href="{{ route('admin.auth.user.index') }}"><i class="fa fa-times"></i>&nbsp;ยกเลิก</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>       
                              <input type="hidden" name="code" value="<?=$data['code']?>">
                              <input type="hidden" name="old_image_thumbnail" value="{{ $data->image_thumbnail }}">
                        </form>
                    </div>

                </div>
            </div>
        </div>

@endsection



@section('begin_css')
        <!-- BEGIN PAGE LEVEL STYLE PROMOTION -->
        <link href="{{ asset("plugins/datetimepicker/jquery.datetimepicker.css")}}" rel="stylesheet">
        <link href="{{ asset("plugins/pickadate/themes/default.css")}}" rel="stylesheet">
        <link href="{{ asset("plugins/pickadate/themes/default.date.css")}}" rel="stylesheet">
        <link href="{{ asset("plugins/pickadate/themes/default.time.css")}}" rel="stylesheet">
        <link href="{{ asset("backend/dependentdrop/dependentdrop.css")}}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset("plugins/chosen/chosen.css")}}" />
        <link rel="stylesheet" href="{{ asset("plugins/chosen/chosenImage/chosenImage.css")}}"/>
        <link rel="stylesheet" href="{{ asset("plugins/wizard/wizard.css")}}" />
        <link rel="stylesheet" href="{{ asset("plugins/jquery-steps/jquery.steps.css")}}" />
        @include('backend.includes.froala-wysiwyg-editor.import-css')
        <style type="text/css">
            .chosen-search {
                display: none;
            }
            .chosen-single {
                height: 36px !important;
                padding-top: 5px !important;
            }
        </style>
@endsection

@section('begin_javascript')
        <script src="{{ asset("plugins/bootstrap-datepicker/bootstrap-datepicker.js")}}"></script>
        <script src="{{ asset("plugins/pickadate/picker.js")}}"></script>
        <script src="{{ asset("plugins/pickadate/picker.date.js")}}"></script>
        <script src="{{ asset("plugins/pickadate/picker.time.js")}}"></script>
        <script src="{{ asset("plugins/bootstrap-switch/bootstrap-switch.js")}}"></script>
        <script src="{{ asset("plugins/bootstrap-progressbar/bootstrap-progressbar.js")}}"></script>

        <script src="{{ asset("plugins/chosen/chosen.jquery.js")}}"></script>
        <script src="{{ asset("plugins/chosen/chosenImage/chosenImage.jquery.js")}}"></script>

        <script type="text/javascript" src="{{ asset("vendor/jsvalidation/js/jsvalidation.js")}}"></script>
        {!! JsValidator::formRequest('App\Http\Requests\Backend\Article\ArticleRequest','#my-form'); !!}


        <!-- Include external JS libs. -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>

<!-- Include Editor JS files. -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.5/js/froala_editor.pkgd.min.js"></script>
        @include('backend.includes.froala-wysiwyg-editor.import-javascript')

        <script type="text/javascript">

        function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
          jQuery('#imagePreview').attr('src',e.target.result);
          $('#imagePreview').hide();
          $('#imagePreview').fadeIn(650);
      }
      reader.readAsDataURL(input.files[0]);
  }
}
$("#imageUpload").change(function() {
    readURL(this);
});

            jQuery(document).ready(function(){
                jQuery(".chosen-img,.chosen-select").chosenImage();

                jQuery("#file-1").change(function(){
                    readURL(this);
                });


                jQuery('textarea.description').froalaEditor({
                      language: '{{app()->getLocale()}}',
                      heightMin: 200,

                      fontFamily: {
                        "Roboto,sans-serif": 'Roboto',
                        "Oswald,sans-serif": 'Oswald',
                        "Montserrat,sans-serif": 'Montserrat',
                        "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
                      },
                      fontFamilySelection: true,
                      
                      quickInsertTags: null,
                      
                      //------------ VDO Upload -------------- //
                      // Set the image upload parameter.
                      videoUploadParam: 'file_param',
                      // Set the image upload URL.
                      videoUploadURL: '{{ route('admin.articles.editor.upload.file') }}',
               
                      // Additional upload params.
                      videoUploadParams: {
                            froala: 'true',       
                            _token: "{{ csrf_token() }}",
                            code:'{{ $data['code'] }}'
                      },
               
                      // Set request type.
                      videoUploadMethod: 'POST',
                      
                      // Set max image size to 5MB.
                      videoMaxSize: 50 * 1024 * 1024,
                      
                      // Allow to upload PNG and JPG.
                      videoAllowedTypes: ['mp4','webm','ogg'],


                      //------------ Image Upload -------------- //
                       // Set the image upload parameter.
                      imageUploadParam: 'file_param',
                      // Set the image upload URL.
                      imageUploadURL: '{{ route('admin.articles.editor.upload.file') }}',
               
                      // Additional upload params.
                      imageUploadParams: {
                            froala: 'true',       
                            _token: "{{ csrf_token() }}",
                            code:'{{ $data['code'] }}'
                      },
               
                      // Set request type.
                      imageUploadMethod: 'POST',
                      
                      // Set max image size to 5MB.
                      imageMaxSize: 5 * 1024 * 1024,
                      
                      // Allow to upload PNG and JPG.
                      imageAllowedTypes: ['jpeg', 'jpg', 'png'],


                      //------------ File Upload -------------- //
                      // Set the file upload parameter.
                      fileUploadParam: 'file_param',
               
                      // Set the file upload URL.
                      fileUploadURL: '{{ route('admin.articles.editor.upload.file') }}',
               
                      // Additional upload params.
                      fileUploadParams: { 
                            froala: 'true',       
                            _token: "{{ csrf_token() }}",
                            code:'{{ $data['code'] }}'
                      },
               
                      // Set request type.
                      fileUploadMethod: 'POST',
               
                      // Set max file size to 20MB.
                      fileMaxSize: 20 * 1024 * 1024,
               
                      // Allow to upload any file.
                      fileAllowedTypes: ['*']

                    })
                    .on('froalaEditor.video.beforeUpload', function (e, editor, images) {
                      // Return false if you want to stop the image upload.
                    })
                    .on('froalaEditor.video.uploaded', function (e, editor, response) {
                      console.log('up');
                    })
                    .on('froalaEditor.video.inserted', function (e, editor, $img, response) {
                      // Image was inserted in the editor.
                    })
                    .on('froalaEditor.video.replaced', function (e, editor, $img, response) {
                      // Image was replaced in the editor.
                    })
                    .on('froalaEditor.video.error', function (e, editor, error, response) {
                      // Bad link.
                      if (error.code == 1) { alert('Bad link'); }
               
                      // No link in upload response.
                      else if (error.code == 2) { alert('No link in upload response'); }
               
                      // Error during image upload.
                      else if (error.code == 3) { alert('Error during image upload.'); }
               
                      // Parsing response failed.
                      else if (error.code == 4) { alert('Parsing response failed.'); }
               
                      // Image too text-large.
                      else if (error.code == 5) { alert('Image too text-large.'); }
               
                      // Invalid image type.
                      else if (error.code == 6) { alert('Invalid image type.'); }
               
                      // Image can be uploaded only to same domain in IE 8 and IE 9.
                      else if (error.code == 7) { alert('Image can be uploaded only to same domain in IE 8 and IE 9.'); }
               
                      // Response contains the original server response to the request if available.
                    })
                    .on('froalaEditor.image.beforeUpload', function (e, editor, images) {
                      // Return false if you want to stop the image upload.
                    })
                    .on('froalaEditor.image.uploaded', function (e, editor, response) {
                      console.log('up');
                    })
                    .on('froalaEditor.image.inserted', function (e, editor, $img, response) {
                      // Image was inserted in the editor.
                    })
                    .on('froalaEditor.image.replaced', function (e, editor, $img, response) {
                      // Image was replaced in the editor.
                    })
                    .on('froalaEditor.image.error', function (e, editor, error, response) {
                      // Bad link.
                      if (error.code == 1) { alert('Bad link'); }
               
                      // No link in upload response.
                      else if (error.code == 2) { alert('No link in upload response'); }
               
                      // Error during image upload.
                      else if (error.code == 3) { alert('Error during image upload.'); }
               
                      // Parsing response failed.
                      else if (error.code == 4) { alert('Parsing response failed.'); }
               
                      // Image too text-large.
                      else if (error.code == 5) { alert('Image too text-large.'); }
               
                      // Invalid image type.
                      else if (error.code == 6) { alert('Invalid image type.'); }
               
                      // Image can be uploaded only to same domain in IE 8 and IE 9.
                      else if (error.code == 7) { alert('Image can be uploaded only to same domain in IE 8 and IE 9.'); }
               
                      // Response contains the original server response to the request if available.
                    })

                    .on('froalaEditor.file.beforeUpload', function (e, editor, files) {
                    // Return false if you want to stop the file upload.
                    })
                    .on('froalaEditor.file.uploaded', function (e, editor, response) {
                      // File was uploaded to the server.
                    })
                    .on('froalaEditor.file.inserted', function (e, editor, $file, response) {
                      // File was inserted in the editor.
                    })
                    .on('froalaEditor.file.error', function (e, editor, error, response) {
                      // Bad link.
                      if (error.code == 1) { alert('Bad link'); }
               
                      // No link in upload response.
                      else if (error.code == 2) { alert('No link in upload response'); }
               
                      // Error during image upload.
                      else if (error.code == 3) { alert('Error during image upload.'); }
               
                      // Parsing response failed.
                      else if (error.code == 4) { alert('Parsing response failed.'); }
               
                      // Image too text-large.
                      else if (error.code == 5) { alert('Image too text-large.'); }
               
                      // Invalid image type.
                      else if (error.code == 6) { alert('Invalid image type.'); }
               
                      // Image can be uploaded only to same domain in IE 8 and IE 9.
                      else if (error.code == 7) { alert('Image can be uploaded only to same domain in IE 8 and IE 9.'); }
               
                      // Response contains the original server response to the request if available.
                    });

            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#blah').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }





            function readURL2(input) {
              if (input.files && input.files[0]) {
                  var reader = new FileReader();
                  reader.onload = function(e) {
                      jQuery('#imagePreview').attr('src',e.target.result);
                      $('#imagePreview').hide();
                      $('#imagePreview').fadeIn(650);
                  }
                  reader.readAsDataURL(input.files[0]);
              }
            }
            $("#imageUpload").change(function() {
                readURL2(this);
            });


        </script>

        
        @if(session()->has('message'))
            <script type="text/javascript">
                <?php
                    $type = session()->get('message');
                    $message = __('alerts.backend.database.'.$type.'');
                ?>

                <?=$type?>(
                        '<i class="fa fa-check-square-o" style="color:#fff;padding-right:8px"></i><?=$message?>', {
                            HorizontalPosition: 'right',
                            VerticalPosition: 'top',
                            ShowOverlay: false,
                            TimeShown: 5000,
                            MinWidth:400
                });
            </script>
        @endif

@endsection

        


     