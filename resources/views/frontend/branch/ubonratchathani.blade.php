@extends('frontend.layouts.app')

@section('begin_css')

<style>
    div#abcdef {
        padding-top: 70px;
    }
</style>

@endsection

@section('content')
<div class="container">
    <div class="row row-wrap">
        <div class=" text-center">
            <div class="gap gap-small"></div>
            <div class="col-md-12">
                <br/>
                <!-- <p style="font-weight: 300;text-align: left;font-size: 16px;line-height: 1.5em;"> รถเช่า ECOCAR เป็นผู้ให้บริการรถเช่าแบบขับเองและรถเช่าพร้อมคนขับ เราเปิดบริการตั้งแต่วันที่ 1 ธันวาคม 2555 จากความมุ่งมั่นในการสร้างการให้บริการที่เป็นเลิศ สร้างความแตกต่างจากคู่แข่งในตลาดรถเช่า ทำให้ ECOCAR เป็นแบรนด์รถเช่าที่ลูกค้าให้ความใว้วางใจ
                    ลูกค้าเก่ามาใช้อย่างต่อเนื่อง จนเราเป็นเพื่อนสนิทกัน เพื่อนที่พร้อมจะเดินทางไปกับคุณสู่จุดหมายอย่างปลอดภัย อีกทั้งมีลูกค้าใหม่ที่มาจากการบอกต่อกันแบบ ปากต่อปาก ลูกค้าของ ECOCAR เพิ่มขึ้นอย่างต่อเนื่อง ทำให้บริษัทขยายตัวอย่างต่อเนื่อง
                    อีกทั้งความเป็นน้ำหนึ่งใจเดียวกันของพนักงาน ECOCAR ที่มีจุดมุ่งหมายเดียวกันที่จะทำให้ ECOCAR เป็นแบรนด์รถเช่าชั้นนำของไทย ปัจจุบัน ECOCAR เปิดให้บริการ 9 สาขา คือ 
                </p> -->
                <div class="gap gap-small"></div>
                <br/>
                <h3>สำนักงาน Ecocar สาขาอุบลราชธานี</h3>
                <p class="line-bottom"></p>
                <div class="gap small-gap"></div>
                <div class="col-md-12">
                    <div class="box-card-ecocar text-left">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d123181.67064630135!2d104.80546187255862!3d15.244481106975414!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2b6145f4cd953a3f!2z4Lij4LiW4LmA4LiK4LmI4Liy4Lit4Li44Lia4LilIEVDT0NBUiDguKPguJbguYDguIrguYjguLLguKrguLLguILguLLguK3guLjguJrguKXguKPguLLguIrguJjguLLguJnguLUo4LmA4LiK4LmI4Liy4Lij4LiW4Lit4Li44Lia4LilKQ!5e0!3m2!1sen!2sth!4v1516933373314"
                            width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                        <div class="box-description">
                            <h5 class="title-branch">สำนักงานสาขาอุบลราชธานี (รถเช่าอุบล)</h5>
                            <p class="description-branch"   style="margin-bottom: 30px;">
                                ตั้งอยู่เลขที่ 456/9 ถนนเทพโยธี ตำบลในเมือง อำเภอเมือง จังหวัดอุบลราชธานี 34000 <br/> เวลาเปิดทำการ 08.00-20.00 น. ทุกวัน<br/> การติดต่อที่สำคัญ
                                <br/> เบอร์โทร 045-956-754<br/> แฟกส์ 045-956-754<br/> มือถือ 061-6485-438<br/> Line ID : 0616485438<br/> Call center 24 ชั่วโมง 090-6384-888<br/> E-mail :ubon@thairentecocar.com
                            </p>
                        </div>
                    </div>
                </div>
 
            </div>
        </div>
    </div>
</div>

@endsection

@section('begin_javascript')


@endsection
