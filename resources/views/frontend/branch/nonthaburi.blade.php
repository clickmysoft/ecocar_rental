@extends('frontend.layouts.app')

@section('begin_css')

<style>
    div#abcdef {
        padding-top: 70px;
    }
</style>

@endsection

@section('content')
<div class="container">
    <div class="row row-wrap">
        <div class=" text-center">
            <div class="gap gap-small"></div>
            <div class="col-md-12">
                <br/>
                <!-- <p style="font-weight: 300;text-align: left;font-size: 16px;line-height: 1.5em;"> รถเช่า ECOCAR เป็นผู้ให้บริการรถเช่าแบบขับเองและรถเช่าพร้อมคนขับ เราเปิดบริการตั้งแต่วันที่ 1 ธันวาคม 2555 จากความมุ่งมั่นในการสร้างการให้บริการที่เป็นเลิศ สร้างความแตกต่างจากคู่แข่งในตลาดรถเช่า ทำให้ ECOCAR เป็นแบรนด์รถเช่าที่ลูกค้าให้ความใว้วางใจ
                    ลูกค้าเก่ามาใช้อย่างต่อเนื่อง จนเราเป็นเพื่อนสนิทกัน เพื่อนที่พร้อมจะเดินทางไปกับคุณสู่จุดหมายอย่างปลอดภัย อีกทั้งมีลูกค้าใหม่ที่มาจากการบอกต่อกันแบบ ปากต่อปาก ลูกค้าของ ECOCAR เพิ่มขึ้นอย่างต่อเนื่อง ทำให้บริษัทขยายตัวอย่างต่อเนื่อง
                    อีกทั้งความเป็นน้ำหนึ่งใจเดียวกันของพนักงาน ECOCAR ที่มีจุดมุ่งหมายเดียวกันที่จะทำให้ ECOCAR เป็นแบรนด์รถเช่าชั้นนำของไทย ปัจจุบัน ECOCAR เปิดให้บริการ 9 สาขา คือ 
                </p> -->
                <div class="gap gap-small"></div>
                <br/>
                <h3>สำนักงาน Ecocar สาขานนทบุรี</h3>
                <p class="line-bottom"></p>
                <div class="gap small-gap"></div>
                <div class="col-md-12">
                    <div class="box-card-ecocar">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d61978.099619013476!2d100.509933!3d13.861161!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c10ea1835cd6c1b!2z4Lij4LiW4LmA4LiK4LmI4Liy4LiZ4LiZ4LiX4Lia4Li44Lij4Li1IHwgRUNPQ0FSIHJlbnQtYS1jYXI!5e0!3m2!1sen!2sth!4v1516933280792"
                            width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                        <div class="box-description">
                            <h5 class="title-branch">สำนักงานรถเช่าสาขานนทบุรี (รถเช่านนทบุรี)</h5>
                            <p class="description-branch" style="margin-bottom: 30px;">
                                ตั้งอยู่เลขที่ 160/1 ถนนรัตนาธิเบศร์ ซอย 18 ต.บางกระสอ อ.เมืองนนทบุรี จ.นนทบุรี 11000<br/> เวลาเปิดทำการ 08.00-20.00 น. ทุกวัน<br/> การติดต่อที่สำคัญ
                                <br/> เบอร์โทร 02-035-5880<br/> แฟกส์ 02-035-5881<br/> มือถือ 062-3489900<br/> Line ID :@ecocar<br/> Call center 24 ชั่วโมง 062-3489900<br/> E-mail : info@thairentecocar.com
                            </p>
                        </div>
                    </div>
                </div>
 
            </div>
        </div>
    </div>
</div>

@endsection

@section('begin_javascript')


@endsection
