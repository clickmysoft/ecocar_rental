@extends('frontend.layouts.app')

@section('begin_css')

<style>
    div#abcdef {
        padding-top: 70px;
    }
</style>

@endsection

@section('content')
<div class="container">
    <div class="row row-wrap">
        <div class=" text-center">
            <div class="gap gap-small"></div>
            <div class="col-md-12">
                <br/>
                <!-- <p style="font-weight: 300;text-align: left;font-size: 16px;line-height: 1.5em;"> รถเช่า ECOCAR เป็นผู้ให้บริการรถเช่าแบบขับเองและรถเช่าพร้อมคนขับ เราเปิดบริการตั้งแต่วันที่ 1 ธันวาคม 2555 จากความมุ่งมั่นในการสร้างการให้บริการที่เป็นเลิศ สร้างความแตกต่างจากคู่แข่งในตลาดรถเช่า ทำให้ ECOCAR เป็นแบรนด์รถเช่าที่ลูกค้าให้ความใว้วางใจ
                    ลูกค้าเก่ามาใช้อย่างต่อเนื่อง จนเราเป็นเพื่อนสนิทกัน เพื่อนที่พร้อมจะเดินทางไปกับคุณสู่จุดหมายอย่างปลอดภัย อีกทั้งมีลูกค้าใหม่ที่มาจากการบอกต่อกันแบบ ปากต่อปาก ลูกค้าของ ECOCAR เพิ่มขึ้นอย่างต่อเนื่อง ทำให้บริษัทขยายตัวอย่างต่อเนื่อง
                    อีกทั้งความเป็นน้ำหนึ่งใจเดียวกันของพนักงาน ECOCAR ที่มีจุดมุ่งหมายเดียวกันที่จะทำให้ ECOCAR เป็นแบรนด์รถเช่าชั้นนำของไทย ปัจจุบัน ECOCAR เปิดให้บริการ 9 สาขา คือ 
                </p> -->
                <div class="gap gap-small"></div>
                <br/>
                <h3>สำนักงาน Ecocar สาขาอ่อนนุช</h3>
                <p class="line-bottom"></p>
                <div class="gap small-gap"></div>
                <div class="col-md-12">
                    <div class="box-card-ecocar">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d496124.38434138114!2d100.680915!3d13.72016!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3de571231f47addf!2zRUNPQ0FSIOC4o-C4luC5gOC4iuC5iOC4suC4quC4suC4guC4suC4reC5iOC4reC4meC4meC4uOC4ig!5e0!3m2!1sen!2sth!4v1516932725759"
                            width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                        <div class="box-description">
                            <h5 class="title-branch">สำนักงานรถเช่าสาขาอ่อนนุช (รถเช่าอ่อนนุช)</h5>
                            <p class="description-branch">
                                ตั้งอยู่เลขที่ 268/13 ถนนอ่อนนุช แขวงประเวศ เขตประเวศ กรุงเทพมหานคร 10250<br/> เวลาเปิดทำการ 08.00-20.00 น. ทุกวัน<br/> การติดต่อที่สำคัญ
                                <br/> เบอร์โทร 02-720-0280<br/> แฟกส์ 02-720-0280<br/> มือถือ 094-512-1689<br/> Line ID :@ecocar<br/> Call center 24 ชั่วโมง 090-6384-888<br/> E-mail : info@thairentecocar.com
                            </p>
                        </div>
                    </div>
                </div>
 
            </div>
        </div>
    </div>
</div>

@endsection

@section('begin_javascript')


@endsection
