@extends('frontend.layouts.app')

@section('begin_css')

  <link rel="stylesheet" type="text/css" href="{{ asset("frontend/css/styleecocar.css")}}">

  <style>
  div#abcdef {
        padding-top: 130px;
    }
  </style>

@endsection

@section('content')
<div class="section-bc">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="bc-left">
                    <h2 class="h2-title-bc">
                        {{ __('index.frontend.car_rental_document') }}
                    </h2>
                </div>
            </div>
            <div class="col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">

                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="wrapper-inner-ecocar">
    <div class="container">
        <div class="tab-pane" role="tabpanel" id="step3">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 padding-terms">
                        <div class="gap">
                            <p>
                                <span style="padding-left: 30px;">
                                    ในการเช่ารถยนต์เพื่อใช้งานสักคัน จำเป็นต้องเตรียมเอกสารอะไรบ้าง สามารถดูเพิ่มด้านล่างนี้ 
                                    หลายคนอาจมีคำถามว่าทำไมบริษัทรถเช่าจึงขอเอกสารหลายอย่างยุ่งยากจัง
                                </span>
                                ทั้งนี้ก็เพราะบริษัทรถเช่า จำเป็นต้องตรวจสอบความน่าเชื่อถือของผู้เช่า ซึ่งแต่ละบริษัทก็จะมีนโยบายที่แตกต่างกัน ดังนั้นผู้เช่าจำเป็นต้องทราบและจัดเตรียมเอกสาร
                                เพื่อใช้ในการเช่ารถยนต์ ซึ่งโดยทั่วไปการเช่ารถยนต์จะต้องใช้เอกสารเช่ารถยนต์ ดังนี้
                            </p>
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>เอกสารเช่ารถยนต์ สำหรับบุคคลธรรมดา</h4>
                                    <ul class="py-1 list-unstyled">
                                        <li class="py-2"><i class="fa fa-check-circle pr-4" aria-hidden="true"></i> เอกสารเช่ารถยนต์ <u>สำหรับผู้มีบัตรเครดิต</u></li>
                                        <ol>
                                            <li>บัตรประชาชน</li>
                                            <li>ใบขับขี่รถยนต์</li>
                                            <li>บัตรเครดิต</li>
                                        </ol>
                                        <br>
                                        <li class="py-2"><i class="fa fa-check-circle pr-4" aria-hidden="true"></i> เอกสารเช่ารถยนต์ <u>สำหรับผู้ที่ไม่มีบัตรเครดิต</u></li>
                                        <ol>
                                            <li>บัตรประชาชน</li>
                                            <li>ใบขับขี่รถยนต์</li>
                                            <li>สลิปเงินเดือน(ใช้เดือนล่าสุด 1 เดือน)หรือหนังสือรับรองรายได้</li>
                                        </ol>
                                        <br>
                                        <li class="py-2"><i class="fa fa-check-circle pr-4" aria-hidden="true"></i> เอกสารเช่ารถยนต์ <u>สำหรับผู้ที่นำรถยนต์เข้าซ่อม</u></li>
                                        <ol>
                                            <li>บัตรประชาชน</li>
                                            <li>ใบขับขี่รถยนต์</li>
                                            <li>ใบรายการจดทะเบียนรถยนต์</li>
                                            <li>ใบรับรถที่อู่หรือศูนย์บริการออกให้</li>
                                        </ol>
                                        <br>
                                        <li class="py-2"><i class="fa fa-check-circle pr-4" aria-hidden="true"></i> เอกสารเช่ารถยนต์ <u>สำหรับเจ้าของกิจการ/บริษัท/นิติบุคคล</u></li>
                                        <ol>
                                            <li>บัตรประชาชน</li>
                                            <li>ใบขับขี่รถยนต์</li>
                                            <li>หนังสือรับรองบริษัท ฉบับล่าสุดไม่เกิน 3 เดือน</li>
                                            <li>ใบจดทะเบียนภาษีมูลค่าเพิ่ม (ภ.พ.20)</li>
                                            <p>ในกรณีกรรมการบริษัทไม่ได้ใช้งานรถยนต์เอง ขอเอกสารเพิ่ม ดังนี้ </p>
                                        </ol>
                                        <ol>
                                            <li>หนังสือมอบอำนาจ </li>
                                            <li>บัตรประชาชนของผู้รับมอบอำนาจ</li>
                                            <li>ใบขับขี่ของผู้รับมอบอำนาจ</li>
                                        </ol>
                                        <br>

                                        <li class="py-2"><i class="fa fa-check-circle pr-4" aria-hidden="true"></i> เอกสารเช่ารถยนต์ <u>สำหรับนักศึกษา</u></li>
                                        <ol>
                                            <li>บัตรประชาชน</li>
                                            <li>ใบขับขี่รถยนต์</li>
                                            <li>ใบรายการจดทะเบียนรถยนต์</li>
                                            <li>บัตรนักศึกษา</li>
                                            <li>ประวัตินักศึกษา</li>
                                        </ol>
                                        <br>

                                        <li class="py-2"><i class="fa fa-check-circle pr-4" aria-hidden="true"></i> เอกสารเช่ารถยนต์ <u>สำหรับนักเดินทาง</u></li>
                                        <ol>
                                            <li>บัตรประชาชน</li>
                                            <li>ใบขับขี่รถยนต์</li>
                                            <li>ใบรายการจดทะเบียนรถยนต์</li>
                                            <li>ตั๋วเครื่องบิน ขาไปและกลับ</li>
                                        </ol>
                                        <br>
                                    </ul>
                                </div>


                                <div class="col-md-6" style="border-bottom: 1px solid #dedede;margin-bottom: 20px;">
                                    <h4>เอกสารเช่ารถยนต์ สำหรับนิติบุคคลหรือบริษัท</h4>
                                    <ul class="py-1 list-unstyled">
                                        <ol>
                                            <li>สำเนาหนังสือรับรองบริษัท ฉบับล่าสุดไม่เกิน 3 เดือน</li>
                                            <li>สำเนาใบจดทะเบียนภาษีมูลค่าเพิ่ม (ภ.พ.20)</li>
                                            <li>สำเนาแบบแสดงรายการภาษีมูลค่าเพิ่ม (ภ.พ.30) เดือนล่าสุด 1 เดือน</li>
                                            <li>หนังสือมอบอำนาจจากกรรมการผู้มีอำนาจลงชื่อผูกพันบริษัทได้<br>(กรณีกรรมการ ไม่ได้มารับรถยนต์เอง)</li>
                                            <li>สำเนาบัตรประชาชนกรรมการผู้มีอำนาจลงชื่อผูกพันบริษัทได้</li>
                                            <li>สำเนาบัตรประชาชนของผู้รับมอบอำนาจ พร้อมตัวจริง</li>
                                            <li>สำเนาใบอนุญาติขับขี่รถยนต์ของผู้รับมอบอำนาจ พร้อมตัวจริง</li>
                                        </ol>
                                        <br>
                                    </ul>

                                </div>

                                <div class="col-md-6 terms">
                                    <h4 style="text-align: center; color: #00692f;">เอกสารเช่ารถทุกอย่างต้องไม่หมดอายุ กรณีมีผู้ขับเสริม จะต้องมาทำสัญญาเช่ารถด้วย</h4>
                                </div>

                                <div class="col-md-6 terms">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <img src="{{ asset("frontend/img/ecocar-line.png")}}" alt="">
                                        </div>

                                        <div class="col-md-8">
                                            <p class="line-text">New !! ส่งเอกสารเช่ารถ กับ ECOCAR ทาง LINE@ ยิ่งไลน์ ยิ่งใกล้</p>
                                            <p>Chat พูดคุยหรือจองรถเช่า กับเรา 24 ชั่วโมง LINE ID : @ecocar หรือคลิก </p>

                                            <div class="line-it-button" data-lang="en" data-type="friend" data-lineid="@ecocar" data-count="true" style="display: none;"></div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 terms">
                                    <h4 style="text-align: center; color: #00692f;">เงินมัดจำสำหรับเช่ารถยนต์</h4>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover ">

                                            <tr>
                                                <td style="text-align: center;">รถยนต์ขนาด 1,200 cc </td>
                                                <td style="text-align: center;">วางเงินมัดจำ 5,000 บาท</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center;">รถยนต์ขนาด 1,500 cc</td>
                                                <td style="text-align: center;">วางเงินมัดจำ 5,000 บาท</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center;">รถยนต์ขนาด 1,800 cc</td>
                                                <td style="text-align: center;">วางเงินมัดจำ 10,000 บาท</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center;">รถยนต์ Fortuner</td>
                                                <td style="text-align: center;">วางเงินมัดจำ 10,000 บาท</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-condition-ecocar"><a href="File/Condition-ECOCAR-Rent-A-Car1-23.3.60.pdf" target="_blank"><i class="fa fa-save "></i> ดาวน์โหลด เงื่อนไขการให้บริการรถเช่า</a></button>
                            </div>
                        </div>
                        </gap>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('begin_javascript')

@endsection
