<footer id="main-footer">
            <div class="container" >
                <div class="row row-wrap">
                    <div class="col-md-3">
                        <a class="logo" href="index.html">
                            <img src="{{ asset("frontend/img/logo-gree-transparent-bg.png")}}" alt="Image Alternative text" title="Image Title" style="height: 35px; width: 150px;" />
                        </a>
                        <p class="mb20" style="line-height: 2em;">
                        {{ __('index.frontend.address1') }}<br/>    
                        {{ __('index.frontend.address2') }}<br/>
                        {{ __('index.frontend.address3') }}<br/>
                        {{ __('index.frontend.address4') }}
                    </div>
                    <div class="col-md-2">
                        <h4>SITE MAP</h4>
                         <ul class="list list-footer">
                            <li><a href="#">{{ __('index.frontend.home') }}</a>
                            </li>
                            <li><a href="#">{{ __('index.frontend.about') }}</a>
                            </li>
                            <li><a href="#">{{ __('index.frontend.document') }}</a>
                            </li>
                            <li><a href="#">{{ __('index.frontend.blog') }}</a>
                            </li>
                            <li><a href="#">{{ __('index.frontend.contact') }}</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                    <h4>RECENT POST</h4>
                        <ul class="list list-footer">
                            <?php
                                use App\Models\Article\ArticleModel;
                                $article = ArticleModel::limit(5)->get();
                                if(isset($article)){
                                    foreach ($article as $key => $value) {
                            ?>
                                        <li><a href="<?php echo route('frontend.blog.show',$value['id']) ?>">{{ $value->title }}</a> </li>
                            <?php
                                    }
                                }
                            ?>
                        </ul>
                    </div>

                    <div class="col-md-3">
                        <h4>CONTACT</h4>
                            <p style="line-height: 2em;">
                                {{ __('index.frontend.contact1') }}<br>
                                {{ __('index.frontend.contact2') }}<br/>
                                {{ __('index.frontend.contact3') }}<br/>
                                {{ __('index.frontend.contact4') }}<br/>
                                {{ __('index.frontend.contact5') }}<br/>
                                {{ __('index.frontend.contact6') }}
                            </p>
                            <br>
                            <p style="line-height: 2em;">
                                {{ __('index.frontend.follow_us') }}<br>
                                <a class="btn-floating btn-lg btn-fb" type="button" role="button" href="https://www.facebook.com/ecocar.co.th/" target="_blank"><i class="fa fa-facebook"></i></a>
                                <a class="btn-floating btn-lg btn-yt" type="button" role="button" href="https://www.youtube.com/channel/UCCgFrlDGIlmjeZs0IEdiC-A" target="_blank"><i class="fa fa-youtube"></i></a>
                                <a class="btn-floating btn-lg btn-comm" type="button" role="button" href="http://line.me/ti/p/~@ecocar" target="_blank"><img src="https://www.jib.co.th/web/images/footer_2018/social-line-icon-300x300.png?v=11" style="
    width: 30px;
    margin-top: -7px;
"></a>
                                <a class="btn-floating btn-lg btn-email" type="button" role="button" href="mailto:info@thairentecocar.com" ><i class="fa fa-envelope"></i></a>


                                <div class="social-buttons hidable-content " style="display:none;">
                                      <div class="row">
                                        <div class="col-md-6">
                                            <iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-share-button twitter-share-button-rendered twitter-tweet-button" style="position: static; visibility: visible; width: 50px; height: 20px;" title="Twitter Tweet Button" src="https://platform.twitter.com/widgets/tweet_button.8a107686fd318b5c68b40d2c2ba1ea37.th.html#dnt=false&amp;id=twitter-widget-0&amp;lang=th&amp;original_referer=https%3A%2F%2Fwww.thairentecocar.com%2F&amp;size=m&amp;text=%E0%B9%80%E0%B8%8A%E0%B9%88%E0%B8%B2%E0%B8%A3%E0%B8%96%20%E0%B8%81%E0%B8%B1%E0%B8%9A%E0%B8%9A%E0%B8%A3%E0%B8%B4%E0%B8%A9%E0%B8%B1%E0%B8%97%20%E0%B8%A3%E0%B8%96%E0%B9%80%E0%B8%8A%E0%B9%88%E0%B8%B2%20%E0%B8%AD%E0%B8%B1%E0%B8%99%E0%B8%94%E0%B8%B1%E0%B8%9A%201%20%7C%20ECOCAR%20Rent-A-Car&amp;time=1540863414975&amp;type=share&amp;url=https%3A%2F%2Fwww.thairentecocar.com%2F"></iframe>
                                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                                        
                                        </div>
                                        <div class="col-md-6" >
                                            <div class="fb-like fb_iframe_widget" data-href="https://www.thairentecocar.com/" data-send="true" data-width="300" data-show-faces="true" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;container_width=0&amp;href=https%3A%2F%2Fwww.thairentecocar.com%2F&amp;locale=en_US&amp;sdk=joey&amp;send=true&amp;show_faces=true&amp;width=300">
                                                <span style="vertical-align: bottom; width: 100px; height: 28px;">
                                                    <iframe name="f28f0a1a9a6103"  style="width:101px !important;" width="101px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" title="fb:like Facebook Social Plugin" src="https://www.facebook.com/plugins/like.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2F__Bz3h5RzMx.js%3Fversion%3D42%23cb%3Df2ee3c058f12aa4%26domain%3Dwww.thairentecocar.com%26origin%3Dhttps%253A%252F%252Fwww.thairentecocar.com%252Ff2e7aaa07dfad6c%26relation%3Dparent.parent&amp;container_width=0&amp;href=https%3A%2F%2Fwww.thairentecocar.com%2F&amp;locale=en_US&amp;sdk=joey&amp;send=true&amp;show_faces=true&amp;width=300" style="border: none; visibility: visible; width: 300px; height: 28px;" class="">
                                                    </iframe>
                                                </span>
                                            </div>
                                        </div>
                                      </div>

                                    
                                    <div class="right">
                                            <!-- Twitter Share -->
                                        <!-- <iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-share-button twitter-share-button-rendered twitter-tweet-button" style="position: static; visibility: visible; width: 50px; height: 20px;" title="Twitter Tweet Button" src="https://platform.twitter.com/widgets/tweet_button.8a107686fd318b5c68b40d2c2ba1ea37.th.html#dnt=false&amp;id=twitter-widget-0&amp;lang=th&amp;original_referer=https%3A%2F%2Fwww.thairentecocar.com%2F&amp;size=m&amp;text=%E0%B9%80%E0%B8%8A%E0%B9%88%E0%B8%B2%E0%B8%A3%E0%B8%96%20%E0%B8%81%E0%B8%B1%E0%B8%9A%E0%B8%9A%E0%B8%A3%E0%B8%B4%E0%B8%A9%E0%B8%B1%E0%B8%97%20%E0%B8%A3%E0%B8%96%E0%B9%80%E0%B8%8A%E0%B9%88%E0%B8%B2%20%E0%B8%AD%E0%B8%B1%E0%B8%99%E0%B8%94%E0%B8%B1%E0%B8%9A%201%20%7C%20ECOCAR%20Rent-A-Car&amp;time=1540863414975&amp;type=share&amp;url=https%3A%2F%2Fwww.thairentecocar.com%2F"></iframe>
                                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
 -->
                                    </div>
                                </div>
                            </p>

                            <p style="line-height: 2em;">
                                          <div data-easyshare data-easyshare-url="https://www.kycosoftware.com/" style="display: none;">
                                            <!-- Twitter -->
                                            <button data-easyshare-button="twitter" data-easyshare-tweet-text="" style="margin-right:15px;">
                                              <span class="fa fa-twitter"></span>
                                              <span>Tweet</span>
                                            </button>
                                            <!-- Facebook -->
                                            <button data-easyshare-button="facebook">
                                              <span class="fa fa-facebook"></span>
                                              <span>Share</span>
                                            </button>
                                            <!-- <span data-easyshare-button-count="facebook">0</span> -->
                                            <!-- Google+ -->
                                            <button data-easyshare-button="google">
                                              <span class="fa fa-google-plus"></span>
                                              <span>+1</span>
                                            </button>
                                            <!-- <span data-easyshare-button-count="google">0</span> -->
                                            <!-- LinkedIn -->
                                            <button data-easyshare-button="linkedin">
                                              <span class="fa fa-linkedin"></span>
                                            </button>
                                            <!-- <span data-easyshare-button-count="linkedin">0</span> -->
                                            <!-- Pinterest -->
                                            <button data-easyshare-button="pinterest">
                                              <span class="fa fa-pinterest-p"></span>
                                            </button>
                                            <!-- <span data-easyshare-button-count="pinterest">0</span> -->
                                            <!-- Xing -->
                                            <button data-easyshare-button="xing">
                                              <span class="fa fa-xing"></span>
                                            </button>
                                            <!-- <span data-easyshare-button-count="xing">0</span> -->
                                            <!-- <div data-easyshare-loader>Loading...</div>
                                          </div> -->

                            </p>


                        <div class="line-it-button" data-lang="en" data-type="friend" data-lineid="@ecocar" data-count="true" style="display: none;" ></div>
                    </div>
                </div>
            </div>
        </footer>

        <div class="Copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h6> Copyright © 2018 ECOCAR. ALL Rights Reserved </h6>
                </div>
            </div>
            </div>
        </div>

        <?php

            // $test = array(
            //        0 => 'Copyright © 2018 ECOCAR. ALL Rights Reserved-1',
            //        1 => 'Copyright © 2018 ECOCAR. ALL Rights Reserved-2',
            //        2 => 'Copyright © 2018 ECOCAR. ALL Rights Reserved-3',
            //        3 => 'Copyright © 2018 ECOCAR. ALL Rights Reserved-4',
            //        4 => 'Copyright © 2018 ECOCAR. ALL Rights Reserved-5',
            //        5 => 'Copyright © 2018 ECOCAR. ALL Rights Reserved-6', 
            //     );


            // echo "<pre>";
            // print_r($test);
            // echo "</pre>";

        ?>

        <div class="container">
            <div class="row">

                <?php
                  // if ($test <= 5) {
                    // foreach ($test as $key => $value) {
                    //     // if(){
                    //         echo '<div class="col-md-6">';
                    //         if ($value <= 5) {
                    //             echo '<p>';
                    //             print_r($value);
                    //             echo '</p>';
                    //         }
                    //         echo '</div>';
                       //  } if (condition) {
                       //      echo '<div class="col-md-6">';
                       //      echo '<p>';
                       //      print_r($value);
                       //      echo '</p>';
                       //      echo '</div>';
                        // }
                  //   }
                  // }
                    
                ?>

                <!-- <div class="col-md-6">
                    <h6> Copyright © 2018 ECOCAR. ALL Rights Reserved </h6>
                     <h6> Copyright © 2018 ECOCAR. ALL Rights Reserved </h6>
                      <h6> Copyright © 2018 ECOCAR. ALL Rights Reserved </h6>
                       <h6> Copyright © 2018 ECOCAR. ALL Rights Reserved </h6>
                </div>
                <div class="col-md-6">
                    <h6> Copyright © 2018 ECOCAR. ALL Rights Reserved </h6>
                     <h6> Copyright © 2018 ECOCAR. ALL Rights Reserved </h6>
                      <h6> Copyright © 2018 ECOCAR. ALL Rights Reserved </h6>
                </div> -->
            </div>
        </div>





       
        <link rel="stylesheet" href="https://cdn.shr.one/1.1.1/shr.css">
        <link rel="preload" as="font" crossorigin type="font/woff2" href="https://cdn.shr.one/fonts/avenir-medium.woff2">
        <link rel="preload" as="font" crossorigin type="font/woff2" href="https://cdn.shr.one/fonts/avenir-bold.woff2">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="{{ asset("plugins/easyshare/jquery.kyco.easyshare.css")}}">

        <script src="{{ asset("plugins/jquery.simpleSocialShare.min.js")}}"></script>
        <script src="https://cdn.shr.one/1.1.1/shr.js"></script>
        


<script src="{{ asset("plugins/easyshare/jquery-1.11.3.min.js")}}"></script>
  <script>
    $.each($('.advanced [data-easyshare-button-count] + [data-easyshare-loader]'), function(i, e) {
      var el        = $(e);
      var done      = false;
      var attr      = el.prev().attr('data-easyshare-button-count')
      var target    = document.querySelector('.advanced [data-easyshare-button-count="' + attr + '"] + [data-easyshare-loader]');
      var startDate = new Date().getTime() / 1000;
      var endDate;

      var observer = new MutationObserver(function(mutations) {
        if (!done) {
          done = true;
          endDate = new Date().getTime() / 1000;
          el.after('Loaded in roughly ', (endDate - startDate).toFixed(2), 's');
        }
      });

      observer.observe(target, {
        attributes: true
      });
    });
  </script>
<script src="{{ asset("plugins/easyshare/jquery.kyco.easyshare.js")}}"></script>


 <style>
        table#u_0_0 > tbody > tr > td >table > tbody > tr > td::nth-child(3){
            display:none !important;
        }
        </style>





