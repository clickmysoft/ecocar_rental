<div id="top-header">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 hidden-sm hidden-md hidden-lg">
        <a href="{{ route('frontend.index') }}">
          <img id="logo-moblie" src="{{ asset("frontend/img/logo-gree-transparent-bg.png")}}"  alt="Image Alternative text" title="Image Title"  style="height: 35px; width: 150px; margin: 20px auto;" />
        </a>
        <div class="top-user-area clearfix user-login-mobile">
          
          <ul class="top-user-area-list list list-horizontal list-border">
            
             @if (config('locale.status') && count(config('locale.languages')) > 1)
                     <li class="top-user-area-lang nav-drop">
                        <?php
                            if(app()->getLocale() == 'en'){
                        ?>
                                <a href="#">
                                    <img src="{{ asset("backend/img/lang/uk.png")}}"/>ENG<i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i>
                                </a>
                        <?php
                            }else{
                        ?>  
                                <a href="#">
                                    <img src="{{ asset("frontend/img/flags/64/th.png")}}"/>ไทย<i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i>
                                </a>
                        <?php        
                            }
                        ?>
                        <ul class="list nav-drop-menu">
                            <li>
                                @include('includes.partials.lang_frontend')
                            </li>
                        </ul>
                    </li>
            @endif
            <li class="login-menu">
              <a href="login.html">
                <i class="fa fa-sign-in fa-2x" aria-hidden="true"></i>
                <span class="login-hidden-mobile">Login</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 hidden-xs">
        <i class="fa fa-home "></i> {{ __('index.frontend.home_text') }} 
      </div>
      <div class="col-md-2 hidden-xs"></div>
      <div class="col-md-6 hidden-xs contact-top-right">
        <svg version="1.1" id="Layer_1"
          xmlns="http://www.w3.org/2000/svg"
          xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="15px" height="18px" viewBox="186.5 42.5 581 475" enable-background="new 186.5 42.5 581 475" xml:space="preserve" style="vertical-align: middle; padding-bottom: 3px;">
          <g>
            <g>
              <path fill="#FFFFFF" d="M680.032,342.104c-2.632-18.367-16.856-32.703-35.112-37.016c-71.064-16.632-88.872-57.568-91.84-102.928
c-12.6-2.352-35-4.872-73.08-4.872s-60.48,2.52-73.08,4.872c-3.023,45.36-20.832,86.296-91.84,102.928
c-18.256,4.312-32.48,18.592-35.112,37.016l-13.72,94.921c-4.816,33.432,19.712,63.56,53.816,63.56H639.88
c34.104,0,58.688-30.128,53.816-63.56L680.032,342.104L680.032,342.104z M480,431.424c-38.472,0-69.664-30.855-69.664-68.936
c0-38.024,31.192-68.936,69.664-68.936s69.664,30.855,69.664,68.936C549.664,400.512,518.416,431.424,480,431.424z
 M754.344,169.736C753.672,128.408,648.448,59.472,480,59.472c-168.448,0-273.672,68.936-274.344,110.264
c-0.616,41.328,0.616,95.144,69.944,86.184c81.088-10.528,76.104-38.808,76.104-79.296c0-28.224,65.968-35.056,128.352-35.056
s128.296,6.832,128.353,35.056c0,40.488-4.984,68.768,76.104,79.296C753.728,264.88,754.96,211.064,754.344,169.736z"/>
            </g>
          </g>
        </svg> 02-030-5880 | 
        <i class="fa fa-phone" aria-hidden="true"></i> 062-348-9900 |
        <div class="line-it-button" data-lang="en" data-type="friend" data-lineid="@ecocar" style="display: none;"></div>
      </div>
    </div>
  </div>
</div>
<header id="main-header">
  <div class="container">
    <div class="row">
      <div class="nav inline">
        <div class="col-md-2 hidden-sm hidden-xs" id="logo-ecocar">
          <a href="{{ route('frontend.index') }}">
            <img src="{{ asset("frontend/img/logo-gree.jpg")}}"  alt="Image Alternative text" title="Image Title"  style="height: 35px; width: 150px; margin-top: 25px;" />
          </a>
        </div>
        <div class="col-md-8 col-sm-12 col-xs-12" id="menu-mobile">
          <ul class="slimmenu" id="slimmenu" style="margin-top: 18px;">
            <li class="active">
              <a href="{{ route('frontend.index') }}">{{ __('navs.general.home') }}</a>
            </li>
            <li id="about-menu">
              <a href="#">{{ __('navs.general.about') }}</a>
              <ul>
                <li class="sub-menu-about">
                  <a href="{{ route('frontend.about') }}">{{ __('navs.general.about_us') }}</a>
                </li>
                <li class="sub-menu-about">
                  <a href="{{ route('frontend.ceo') }}">{{ __('navs.general.ceo') }}</a>
                </li>
                <li class="sub-menu-about">
                  <a href="{{ route('frontend.branch') }}">{{ __('navs.general.branch') }}</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="{{ route('frontend.document') }}">{{ __('navs.general.document') }}</a>
            </li>
            <li>
              <a href="{{ route('frontend.promotion.index') }}">{{ __('navs.general.promotion') }}</a>
            </li>
            <li>
              <a href="{{ route('frontend.blog.index') }}">{{ __('navs.general.blog') }}</a>
            </li>
            <li>
              <a href="{{ route('frontend.contact') }}">{{ __('navs.general.contact_us') }}</a>
            </li>

            <li class="mobile-menu">
              <a href="#">ภาษา</a>
              <ul>

                <li class="sub-menu-about" style="    border-bottom: 1px solid rgba(50, 176, 23, 0.81);">
                  <a href="#" onclick="location.href='{{ '/lang/th' }}'">ภาษาไทย</a>
                </li>
                <li class="sub-menu-about" style="    border-bottom: 1px solid rgba(50, 176, 23, 0.81);">
                  <a href="#" onclick="location.href='{{ '/lang/en' }}'">ภาษาอังกฤษ</a>
                </li>
              </ul>
            </li>

            @guest  
                    <li class="login-menu mobile-menu">
                        <a href="{{route('frontend.auth.login')}}" class="nav-link {{ active_class(Active::checkRoute('frontend.auth.login')) }}">
                            {{ __('navs.frontend.login') }} 
                        </a>
                    </li>
                    @if (config('access.registration'))
                            <li class="login-menu mobile-menu">
                                <a href="{{route('frontend.auth.register')}}" class="nav-link {{ active_class(Active::checkRoute('frontend.auth.register')) }}">
                                {{ __('navs.frontend.register') }}
                                </a>
                            </li>
                    @endif

                    @else


                       <li class="mobile-menu">
                          <a href="#">{{ str_limit($logged_in_user->name,40)   }}</a>
                          <ul>
                            @can('view backend')
                                
                                <li class="sub-menu-about" style="    border-bottom: 1px solid rgba(50, 176, 23, 0.81);">
                                  <a href="{{ route('admin.dashboard') }}">{{ __('navs.frontend.user.administration') }}</a>
                                </li>

                            @endcan

                            <li class="sub-menu-about" style="    border-bottom: 1px solid rgba(50, 176, 23, 0.81);">
                              <a href="{{ route('frontend.user.dashboard') }}">{{ __('navs.frontend.user.account') }}</a>
                            </li>
                            <li class="sub-menu-about" >
                              <a href="{{ route('frontend.auth.logout') }}">{{ __('navs.general.logout') }}</a>
                            </li>
                          </ul>
                        </li>

                       
              @endguest

              <li class="mobile-menu">
                        <a href="tel:020024606" class="nav-link {{ active_class(Active::checkRoute('frontend.auth.login')) }}">
                            โทรติดต่อเรา
                        </a>
              </li>

          </ul>
        </div>
        <div class="col-md-3 hidden-sm hidden-xs" id="user-login">
          <div class="top-user-area clearfix" style="margin-top: 22px;width: 246px">
            <ul class="top-user-area-list list list-horizontal list-border">
                @if (config('locale.status') && count(config('locale.languages')) > 1)
                     <li class="top-user-area-lang nav-drop">
                        <?php
                            if(app()->getLocale() == 'en'){
                        ?>
                                <a href="#">
                                    <img src="{{ asset("backend/img/lang/uk.png")}}"/>ENG<i class="fa fa-angle-down" style="font-size: 18px;"></i><i class="fa fa-angle-up" style="font-size: 18px;"></i>
                                </a>
                        <?php
                            }else{
                        ?>
                                <a href="#">
                                    <img src="{{ asset("frontend/img/flags/64/th.png")}}"/>ไทย<i class="fa fa-angle-down" style="font-size: 18px;"></i><i class="fa fa-angle-up" style="font-size: 18px;"></i>
                                </a>
                        <?php        
                            }
                        ?>
                        <ul class="list nav-drop-menu">
                            <li>
                                @include('includes.partials.lang_frontend')
                            </li>
                        </ul>
                    </li>
                @endif
                
                @guest  
                    <li class="login-menu">
                        <a href="{{route('frontend.auth.login')}}" class="nav-link {{ active_class(Active::checkRoute('frontend.auth.login')) }}">
                             <i class="fa fa-sign-in fa-2x" aria-hidden="true"></i>&nbsp;{{ __('navs.frontend.login') }} 
                        </a>
                    </li>
                    @if (config('access.registration'))
                            <li class="login-menu">
                                <a href="{{route('frontend.auth.register')}}" class="nav-link {{ active_class(Active::checkRoute('frontend.auth.register')) }}">
                                <i class="fa fa-user fa-2x" aria-hidden="true" style="font-size: 18px;"></i>&nbsp;{{ __('navs.frontend.register') }}
                                </a>
                            </li>
                    @endif

                    @else
                        <li class="top-user-area-avatar nav-drop">
                                <a href="#" style="color: #fff;">
                                    <img class="origin round user-display" src="{{ asset($logged_in_user->user_image)}}" title="Clickmycom">
                                    {{ str_limit($logged_in_user->name,10)   }}<i class="fa fa-angle-down" style="font-size: 18px;"></i><i class="fa fa-angle-up" style="font-size: 18px;"></i>
                                </a>
                                <ul class="list nav-drop-menu" >
                                    @can('view backend')
                                        <li>
                                            <a title="setting" href="{{ route('admin.dashboard') }}" class="dropdown-item" onclick="location.href='{{ route('admin.dashboard') }}'">
                                               <i class="fa fa-tachometer" style="font-size: 18px; color: #fff;"></i><span class="" style="font-size: 14px; color: #fff; padding-left: 10px;">{{ __('navs.frontend.user.administration') }}</span>
                                            </a>
                                        </li>
                                    @endcan
                                    <li>
                                        <a id="bg-dropdown-display" title="setting" class="dropdown-item {{ active_class(Active::checkRoute('frontend.user.dashboard')) }}" onclick="location.href='{{ route('frontend.user.dashboard') }}'">
                                           <i class="fa fa-cogs" style="font-size: 18px; color: #fff;"></i><span class="" style="font-size: 14px; color: #fff; padding-left: 10px;">{{ __('navs.frontend.user.account') }}</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a id="bg-dropdown-display" title="Booking-History" class="dropdown-item" onclick="location.href='{{ route('frontend.auth.logout') }}'">
                                            <i class="fa fa-sign-out"  style="font-size: 18px; color: #fff;"></i><span class="" style="font-size: 14px; color: #fff; padding-left: 10px;">{{ __('navs.general.logout') }}</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                  @endguest
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>