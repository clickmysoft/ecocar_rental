<div class="owl-carousel owl-slider owl-carousel-area visible-lg" id="owl-carousel-slider">

    <div class="bg-holder full">
        <div class="bg-mask"></div>
        <div class="bg-img" style="background-image: url(../frontend/img/homeslider/<?=app()->getLocale()?>/eco-banner-01.jpg);"></div>
        <div class="bg-content">
            <div class="container">
                <div class="row">
                    
                </div>
            </div>
        </div>
    </div>
    <div class="bg-holder full">
        <div class="bg-mask"></div>
        <div class="bg-img" style="background-image: url(../frontend/img/homeslider/<?=app()->getLocale()?>/eco-banner-02.jpg);"> </div>
        <div class="bg-content">
            <div class="container">
                <div class="row">
                    
                </div>
            </div>
        </div>
    </div>
     <div class="bg-holder full">
        <div class="bg-mask"></div>
        <div class="bg-img" style="background-image: url(../frontend/img/homeslider/<?=app()->getLocale()?>/eco-banner-03.jpg);"> </div>
        <div class="bg-content">
            <div class="container">
                <div class="row">
                    
                </div>
            </div>
        </div>
    </div>

</div>