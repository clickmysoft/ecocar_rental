@extends('frontend.layouts.app')

@section('begin_css')

@endsection

@section('content')
<div class="container">
    <div class="row row-wrap">
        <div class=" text-center">
            <div class="gap gap-small"></div>
            <div class="col-md-12">
                <br/>
                <p style="font-weight: 300;text-align: left;font-size: 16px;line-height: 1.5em;"> รถเช่า ECOCAR เป็นผู้ให้บริการรถเช่าแบบขับเองและรถเช่าพร้อมคนขับ เราเปิดบริการตั้งแต่วันที่ 1 ธันวาคม 2555 จากความมุ่งมั่นในการสร้างการให้บริการที่เป็นเลิศ สร้างความแตกต่างจากคู่แข่งในตลาดรถเช่า ทำให้ ECOCAR เป็นแบรนด์รถเช่าที่ลูกค้าให้ความใว้วางใจ
                    ลูกค้าเก่ามาใช้อย่างต่อเนื่อง จนเราเป็นเพื่อนสนิทกัน เพื่อนที่พร้อมจะเดินทางไปกับคุณสู่จุดหมายอย่างปลอดภัย อีกทั้งมีลูกค้าใหม่ที่มาจากการบอกต่อกันแบบ ปากต่อปาก ลูกค้าของ ECOCAR เพิ่มขึ้นอย่างต่อเนื่อง ทำให้บริษัทขยายตัวอย่างต่อเนื่อง
                    อีกทั้งความเป็นน้ำหนึ่งใจเดียวกันของพนักงาน ECOCAR ที่มีจุดมุ่งหมายเดียวกันที่จะทำให้ ECOCAR เป็นแบรนด์รถเช่าชั้นนำของไทย ปัจจุบัน ECOCAR เปิดให้บริการ 9 สาขา คือ </p>
                <div class="gap gap-small"></div>
                <br/>
                <h3>สาขาสำนักงาน Ecocar ทั้งหมด</h3>
                <p class="line-bottom"></p>
                <div class="gap small-gap"></div>
                <div class="col-md-4">
                    <div class="box-card-ecocar">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d123908.2793638099!2d100.616644!3d13.950642!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xccb38d39364d87e2!2z4Lia4Lij4Li04Lip4Lix4LiXIOC5hOC4l-C4ouC5gOC4o-C5ieC4meC4l-C5jOC4reC4teC5guC4geC5ieC4hOC4suC4o-C5jCDguIjguY3guLLguIHguLHguJQ!5e0!3m2!1sen!2sth!4v1516930345024"
                            width="100%" height="200" frameborder="0" style="border:0;border-radius: 5px;padding-bottom:  0 !important;padding:  0;" allowfullscreen=""></iframe>
                        <div class="box-description">
                            <h5 class="title-branch text-left">1.สำนักงานใหญ่ รถเช่าดอนเมือง</h5>
                            <p class="description-branch" style="margin-bottom: 30px;">
                                ตั้งอยู่เลขที่ 279/57 ถนนวิภาวดีรังสิต แขวงสนามบิน เขตดอนเมือง กรุงเทพมหานคร 10210<br> เวลาเปิดทำการ 08.00-20.00 น. ทุกวัน<br> การติดต่อที่สำคัญ
                                <br> เบอร์โทร 02-002-4606<br> แฟกส์ 02-002-4607 <br> มือถือ 092-2848-660<br> Line ID :@ecocar<br> Call center 24 ชั่วโมง 090-6384-888<br> E-mail : info@thairentecocar.com
                            </p>
                            <a  href="{{ url('pattaya') }}" type="button" class="btn btn-default prev-step" style="background: #00551f;color: #fff;width: 150px;">
                                รายละเอียดเพิ่มเติม
                            </a>
                        </div>
                    </div>
                </div>
 
            </div>
        </div>
    </div>
</div>

@endsection

@section('begin_javascript')


@endsection
