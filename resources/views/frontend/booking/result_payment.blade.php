@extends('frontend.layouts.app')

@section('content')


<?php

// Clear Session
session()->forget('check_booking');

?>



  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>



<div class="container">
<br>
	<div class="showsuccess">
				<div class="success">

				<?php
					if(@$response['code'] == 'danger'){
				?>
						<picture><img src="{{ asset('frontend/img/cancel.png')}}" style="width:auto;height:auto;"></picture>
				<?php		
					}else{
				?>
						<picture><img src="{{ asset('frontend/img/correct.png')}}" style="width:auto;height:auto;"></picture>
				<?php
					}
				?>


					<!-- <picture><img src="http://www.nus.edu.sg/identity/images/identity/logo/correct.gif" style="width:auto;"></picture> -->
					<?php if(!empty($response['code'])) { ?>
						<div class="alert alert-<?php echo $response['code']; ?>">
						    <?php echo $response['message']; ?>
						</div>
					<?php } ?>


					<?php if ($data['type_pay']!='pay_now') { ?>
							<p>{{ __('index.frontend.hi') }} <?php print_r(@$data['customer_first_name']);?> <?php print_r(@$data['customer_last_name']);?>, {{ __('index.frontend.car_record') }}</p>
							<h2 class="thank">{{ __('index.frontend.thank_you') }}</h2>
							<h4>{{ __('index.frontend.total_amount') }}
								<font color="#008b00">
									<?php 
										$total_vat = ($data['total_price']/100)*7;
                                        $total_price_vat = $total_vat+$data['total_price'];
                                        echo number_format($total_price_vat);
										// print_r( number_format($data['total_price']));
									?>
								</font> {{ __('index.frontend.baht') }}
							</h4>
							<h4>{{ __('index.frontend.transfer_for_car') }} <font color="#008b00"><?php print_r(number_format(ENV('PRICE_OF_PRE_BOOK_CAR')));?></font> {{ __('index.frontend.baht') }}</h4>
							<h4>{{ __('index.frontend.to_account_no') }} <font color="#008b00">001-370-1156‬</font> {{ __('index.frontend.k_bank') }} บจก. รถเช่าอีโคคาร์</h4>
							<h4>{{ __('index.frontend.send_proof') }} <font color="#008b00">info@ecocar.co.th</font> {{ __('index.frontend.to_confirm') }}</h4>
					  <?php } else { 
					  		if($response['code'] != 'danger'){
					  	?>
					  			<p>{{ __('index.frontend.hi') }} <?php print_r(@$data['first_name']);?> <?php print_r(@$data['last_name']);?>, {{ __('index.frontend.car_record') }}</p>
							  	<h2 class="thank">{{ __('index.frontend.thank_you') }}</h2>
							  	<h4>{{ __('index.frontend.pay_for') }} <?php print_r( number_format($data['total_price']));?> {{ __('index.frontend.baht') }} {{ __('index.frontend.baht') }}</h4>
					  	<?php
					  		}else{
					  	?>
					  			<p>{{ __('index.frontend.hi') }} <?php print_r(@$data['first_name']);?> <?php print_r(@$data['last_name']);?>, <?=$data['invoice_description'] ?> ยอดชำระ <?=number_format($data['total_price'])?> {{ __('index.frontend.baht') }}
					  			<p>ผลการชำระเงินของท่านผ่านช่องทาง Paypal ของคุณ</p>
							  	<h2 style="color:red;">{{ __('index.frontend.payment_failed') }}</h2>
							  	<p>อันเนื่องมาจาก ยอดชำระของคุณยังไม่ผ่านการยืนยันจากทาง Paypal</p>
							  	<p>กรุณาติดต่อสอบถาม ฝ่ายบริการลูกค้า เพื่อแก้ไขปัญหาที่เกิดขึ้น</p>

					  	<?php		
					  		}
					} ?>
			
				</div>
			<hr style="border-top: 4px solid #287a5c" width="90%" />
		<div class="contact">
			<p>{{ __('index.frontend.check_car_email') }}</p>
			<p>{{ __('index.frontend.for_inquiries') }} <strong>{{ __('index.frontend.customer_service') }}</strong></p>	
		</div>
	</div>
	<br>
</div>




@endsection



@section('begin_javascript')


@endsection


@section('begin_css')
	<link rel="stylesheet" href="{{ asset("frontend/css/font-awesome.css")}}">
	<link rel="stylesheet" type="text/css" href="{{ asset("frontend/css/form-step.css")}}">
	<link rel="stylesheet" href="{{ asset("frontend/css/styleecocar.css")}}">
	<link rel="stylesheet" href="{{ asset("frontend/css/icomoon.css")}}">
	<style>
div#abcdef {
    padding-top: 130px;
}
	header#main-header {
    top: 40px !important;
}
			div.showsuccess{
				padding: 5%;
				margin-top: 0;
				border: 1px solid #4CAF50;
			}
			div.success{
				background-color: lightgray;
			    width: 100%;
			    padding: 25px;
			    text-align: center;
			    border-radius: 10px;
			}
li.active .title-formstep {
    color: #FFEB3B !important;
}
.wizard li.active span.round-tab i {
    color: #FF5722 !important;
}
			div.contact{
				text-align: center;
			}
			h2.thank{
				color: green;
			}
			table.text{
				width: auto;
			}
			th{
				padding: 10px;
          		border-bottom: 1px solid #ddd;
			}
			td{
				padding: 10px;
          		border-bottom: 1px solid #ddd;
			}
			p.follow{
				text-align: center;
			}
			div.logo{
				background-color: #287a5c;
				display: block;
				width: 100%;
			    padding: 20px;
			    text-align: center;
			}
			div.head_detail{
				padding-left: 5%;
				padding-right: 5%;
				display: block;
			}
			div.detail{
				padding-left: 10%;
				padding-right: 20%;
				display: block;
				margin: auto;
			}
		    .nav-tabs {
		        border-bottom: none; 
		    }
		    .form-group.form-group-icon-left .form-control {
		    padding-left: 28px;
		    }
		    
		    ul.pagination.results{
		            padding-left: 20px;
		    }
		    
		    @media (max-width: 576px){
		        .box-carrental-summery{
		            width: 100%;
		            padding: 0;
		        }
		        .price-carrental{
		            border-bottom: 4px solid;
		            margin-bottom: 10px;
		        }
		    }
		    
		    @media (max-width: 376px){
		    .pl30{
		        padding-left: 0 !Important;
		    }
		    
		 }
		    @media (max-width: 612px){
		    .title-extra{
		        width: 100%;
		        display: block;
		    }
		    .baby-seat{
		        display: block;
		        margin: 0 auto;
		        margin-top: 10px;
		    }
		    .center-mobile{
		        text-align: center;
		    }
		    td.td-extra-icon{
		        width: 100%;
		        display: block;
		        text-align: center;
		    }
		    td.td-extra-price{
		         width: 100%;
		         display: block;
		        border-bottom: 4px solid #000;
		    }
		     
		    input.checkbox-formstep{
		        float: none !important;
		    }
		    .pl30{
		        padding-left: 0 !Important;
		    }
		    td.td-extra-icon{
		        margin-top: 10px;
		    }
		    .title-extra{
		        padding-bottom: 0;
		    }
		    input.checkbox-formstep{
		        transform: scale(2);
		        height: 10px;
		        margin-right: 10px;
		        margin-top: -10px;
		    }
		   
			}

			@media (max-width: 991px){
			.img-paypal{
			    padding-left: 0;
			    }
			    .btn-payletter,.btn-now{
			        width: 100%;
			    }
			    
			}


			.form-group .input-icon{
			        top: 27px;
			}

	</style>
@endsection


