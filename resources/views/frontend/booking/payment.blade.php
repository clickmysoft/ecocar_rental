@extends('frontend.layouts.app')

@section('begin_css')
<link rel="stylesheet" href="{{ asset("frontend/css/font-awesome.css")}}">
<link rel="stylesheet" type="text/css" href="{{ asset("frontend/css/form-step.css")}}">
<link rel="stylesheet" href="{{ asset("frontend/css/styleecocar.css")}}">
<link rel="stylesheet" href="{{ asset("frontend/css/icomoon.css")}}">
 <style>
    .nav-tabs {
        border-bottom: none; 
    }
    .form-group.form-group-icon-left .form-control {
    padding-left: 28px;
    }
    
    ul.pagination.results{
            padding-left: 20px;
    }
    
    @media (max-width: 576px){
        .box-carrental-summery{
            width: 100%;
            padding: 0;
        }
        .price-carrental{
            border-bottom: 4px solid;
            margin-bottom: 10px;
        }
    }
    
    @media (max-width: 376px){
    .pl30{
        padding-left: 0 !important;
    }
    
 }
    @media (max-width: 612px){
    .title-extra{
        width: 100%;
        display: block;
    }
    .baby-seat{
        display: block;
        margin: 0 auto;
        margin-top: 10px;
    }
    .center-mobile{
        text-align: center;
    }
    td.td-extra-icon{
        width: 100%;
        display: block;
        text-align: center;
    }
    td.td-extra-price{
         width: 100%;
         display: block;
        border-bottom: 4px solid #000;
    }
     
    input.checkbox-formstep{
        float: none !important;
    }
    .pl30{
        padding-left: 0 !Important;
    }
    td.td-extra-icon{
        margin-top: 10px;
    }
    .title-extra{
        padding-bottom: 0;
    }
    input.checkbox-formstep{
        transform: scale(2);
        height: 10px;
        margin-right: 10px;
        margin-top: -10px;
    }
   
}

@media (max-width: 991px){
.img-paypal{
    padding-left: 0;
    }
    .btn-payletter,.btn-now{
        width: 100%;
    }
    
}


.form-group .input-icon{
        top: 27px;
}


* {margin: 0; padding: 0; box-sizing: border-box;}



/* clearfix */





ul.tp {list-style: none;}


/* Tab Panels */
.tabs-1 {
  position: relative;
  display: block;
  margin-top: 10px;
}

.tabs-1 h3 {margin-top: 10px;}

.tabs-1 li.tp-1 label {
  position: relative;
  display: inline-block;
  height: 40px;
  margin-right: 5px;
  padding: 0 15px;
  border: 1px solid #ddd;
  background: #eee;
  color: #333;    
  cursor: pointer;
  z-index: 99;          
}


input[type=radio]:checked ~ label {
  border-bottom: 1px solid #fff;
}

.panel-1 {
  padding: 10px 20px;
  background: #fff;
  border: 1px solid #ddd;
  margin-top: -1px;
  width: 100%;
  position: absolute;
  top: 40px;
  opacity: 0;
  z-index: 1;
  transition: opacity .3s;
}

/* Checkbox */
input.radio-hack[type=radio] {
  position: fixed;
  left: -9999px;
  opacity: 0;
  filter: alpha(opacity=0);
}

input[type=radio]:checked ~ div.panel-1 {
  opacity: 1;
}

.wrapper {
  width: 100%;
  max-width: 1000px;
  margin: 0 auto;
  padding: 0 15px;
}

@media (max-width: 282px){
    ul.tabs-1.cf.tp {
        min-height: 1690px;
    }  
}

@media (max-width: 294px){
    ul.tabs-1.cf.tp {
        min-height: 1690px;
    }  
}

@media (min-width: 295px){
    ul.tabs-1.cf.tp {
        min-height: 1670px;
    }  
}

@media (min-width: 322px){
    ul.tabs-1.cf.tp {
        min-height: 1648px;
    }  
}

@media (min-width: 334px){
    ul.tabs-1.cf.tp {
        min-height: 1625px;
    }  
}

@media (min-width: 345px){
    ul.tabs-1.cf.tp {
        min-height: 1580px;
    }  
}

@media (min-width: 358px){
    ul.tabs-1.cf.tp {
        min-height: 1560px;
    }  
}

@media (min-width: 370px){
    ul.tabs-1.cf.tp {
        min-height: 1535px;
    }  
}

@media (min-width: 425px){
    ul.tabs-1.cf.tp {
        min-height: 1535px;
    }  
}

@media (min-width: 495px){
    ul.tabs-1.cf.tp {
        min-height: 1540px;
    }  
}

@media (min-width: 503px){
    ul.tabs-1.cf.tp {
        min-height: 1545px;
    }  
}

@media (min-width: 512px){
    ul.tabs-1.cf.tp {
        min-height: 1560px;
    }  
}

@media (min-width: 536px){
    ul.tabs-1.cf.tp {
        min-height: 1540px;
    }  
}

@media (min-width: 538px){
    ul.tabs-1.cf.tp {
        min-height: 1520px;
    }  
}

@media (min-width: 586px){
    ul.tabs-1.cf.tp {
        min-height: 1506px;
    }  
}
@media (min-width: 613px){
    ul.tabs-1.cf.tp {
        min-height: 1387px;
    }
    div.paym {
      text-align: center;
    }  
}

/*@media (min-width: 768px){
    ul.tabs-1.cf.tp {
        min-height: 1386px;
    }  
}*/

@media (min-width: 991px){
    ul.tabs-1.cf.tp {
        min-height: 771px;
    }  
}


.tabs-in {
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-flex-wrap: wrap;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
}

.tabs-in label.intap {
  -webkit-box-ordinal-group: 2;
  -webkit-order: 1;
  -ms-flex-order: 1;
  order: 1;
  display: block;
  padding: 1rem 2rem;
  margin-right: 0.2rem;
  cursor: pointer;
  background: #90CAF9;
  font-weight: bold;
  -webkit-transition: background ease 0.2s;
  transition: background ease 0.2s;
}

.tabs-in .tab-in {
  -webkit-box-ordinal-group: 100;
  -webkit-order: 99;
  -ms-flex-order: 99;
  order: 99;
  -webkit-box-flex: 1;
  -webkit-flex-grow: 1;
  -ms-flex-positive: 1;
  flex-grow: 1;
  width: 100%;
  display: none;
  padding: 1rem;
  background: #fff;
}

.tabs-in input[type="radio"] {
  position: absolute;
  opacity: 0;
}



.tabs-in input[type="radio"]:checked + label.intap { background: #fff; }

.tabs-in input[type="radio"]:checked + label.intap + .tab-in { display: block; }
 @media (max-width: 45em) {

.tabs-in .tab-in,  .tabs-in label.intap {
  -webkit-box-ordinal-group: NaN;
  -webkit-order: initial;
  -ms-flex-order: initial;
  order: initial;
}

.tabs-in label.intap {
  width: 100%;
  margin-right: 0;
  margin-top: 0.2rem;
}
}

li.active .title-formstep {
    color: #FFEB3B !important;
}

.wizard li.active span.round-tab i {
    color: #FF5722 !important;
}

.connecting-line {
    height: 4px;
    background: #00551f;
    position: absolute;
    width: 75%;
    margin: 0 auto;
    left: 0;
    right: 0px;
    top: 52%;
    z-index: 1;
}

div#abcdef {
    padding-top: 135px;
}

ul.tabs-1.cf.tp {
    margin-top: -15px;
}


    </style>
@endsection

@section('content')



<div class="container">
            
    <div class="row">
      <section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

               <li role="presentation" class="disabled">
                        <span class="title-formstep">{{ __('index.frontend.vehicles') }}</span>
                        <a href="#" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            <span class="round-tab" onclick="#" style="background-color: #00551f;border: 2px solid rgb(255, 255, 255);">
                               <i class="fa fa-car"  style="color:#fff;"></i>
                               <!-- <img class="icon-formstep" src="{{ asset("frontend/img/car-formstep-02.png")}}" alt=""> -->
                            </span>
                        </a>
                    </li>
                    
                    <li role="presentation" class="disabled">
                       <span class="title-formstep">{{ __('index.frontend.extra') }}</span>
                        <a href="#" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2"> 
                            <span class="round-tab" onclick="#" style="background-color: #00551f;border: 2px solid rgb(255, 255, 255);"> 
                                <i class="fa fa-taxi" style="color:#fff;"></i>
                                <!-- <img class="icon-formstep" src="{{ asset("frontend/img/car-formstep-02.png")}}" alt=""> -->
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <span class="title-formstep">{{ __('index.frontend.customer_detail') }}</span>
                        <a href="#" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                            <span class="round-tab" onclick="#" style="background-color: #00551f;border: 2px solid rgb(255, 255, 255);">
                                <i class="fa fa-list-alt" style="color:#fff;"></i>
                                <!-- <img class="icon-formstep" src="{{ asset("frontend/img/img_311846.png")}}" alt=""> -->
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="active">
                        <span class="title-formstep">{{ __('index.frontend.summary') }}</span>
                        <a href="#" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab" onclick="#">
                                <i class="fa fa-file-text-o"></i>
                                <!-- <img class="icon-formstep" src="{{ asset("frontend/img/car-formstep-02.png")}}" alt=""> -->
                            </span>
                        </a>
                    </li>

                </ul>
            </div>

            

                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="complete">
                        <div class="step_21" style="border:0px;padding: 0;">
                            <div>
                                <ul class="tabs-1 cf tp">
                                    <li class="tp-1">
                                        <input class="radio-hack" type="radio" id="panel-nav-1" name="tab-link" checked>
                                        <label class="tapcheck" for="panel-nav-1" style="float: left;line-height: 40px;display:none;">{{ __('index.frontend.summary') }}</label>
                                        <div class="panel-1" style="padding: 0;min-height: 742px;">
                                            <div class="step44">
                                                <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="gap"></div>
                                                                <h5 style="font-weight: 500;">{{ __('index.frontend.your_information') }}</h5>
                                                                 <hr style="border-top: 4px solid #287a5c" />
                                                                 <p> {{ __('index.frontend.name') }}: {{ session()->get('check_booking')['first_name'] }} {{ session()->get('check_booking')['last_name'] }}</p>
                                                                 <p> {{ __('index.frontend.email') }}: {{ session()->get('check_booking')['email'] }}</p>
                                                                 <p class="phone"> {{ __('index.frontend.phone') }}: {{ session()->get('check_booking')['phone'] }} </p>
                                                                 <hr style="border-top: 4px solid #287a5c;" />


                                                                 <h5 style="font-weight: 500;">{{ __('index.frontend.car_specs') }} </h5>
                                                                 <table class="tb-w-100p">

                                                                    <tr>
                                                                        <td class="col-md-4"></td>
                                                                        <td class="col-md-2">{{ __('index.frontend.brand') }}</td>
                                                                        <td class="col-md-2">{{ __('index.frontend.generation') }}</td>
                                                                        <td class="col-md-2">{{ __('index.frontend.car_size') }}</td>
                                                                        <td class="col-md-2">{{ __('index.frontend.price') }}</td>
                                                                    </tr>
                                                                     <tr>
                                                                         <td class="col-md-4">
                                                                            <img style="max-width: 130px;" rel="tooltip" data-placement="top" title="{{ $data_car->license_plate }}" src="{{asset("cars/$data_car->car_brand/$data_car->car_generation/image-01.jpg")}}" />
                                                                        </td>
                                                                         <td class="col-md-2">{{ strtoupper($data_car->car_brand) }}</td>
                                                                         <td class="col-md-2">{{ str_replace('_',' ',strtoupper($data_car->car_generation)) }}</td>
                                                                         <td class="col-md-2"><?=number_format($car_engine_cc)?> CC</td>
                                                                         <td class="col-md-2">{{ number_format($data_car->price-$discount_for_pay_now) }} บาท</td>
                                                                     </tr>

                                                                </table>


                                                                <h5 style="font-weight: 500;">{{ __('index.frontend.extra_options_list') }} </h5>
                                                                 <table class="tb-w-100p">
                                                                 <tr>
                                                                 <td>{{ __('index.frontend.lists') }}</td>
                                                                 <td >{{ __('index.frontend.amount') }}</td>
                                                                 <td>{{ __('index.frontend.price_per_unit') }}</td>
                                                                 <td>{{ __('index.frontend.total_price') }}</td>
                                                                 </tr>
                                                                <?php
                                                                    $total_price_opion = 0;

                                                                    $start_date=date_create(session()->get('check_booking')['pick_up_date']);
                                                                    $end_date=date_create(session()->get('check_booking')['drop_off_date']);

                                                                    $diff=date_diff($start_date,$end_date);
                                                                    $diff_day = $diff->format("%a");
                                                                    
                                                                    if($diff_day == 0){

                                                                        $diff_day+=1;
                                                                    }

                                                                    if(!empty(session()->get('check_booking')['query_option'])){
                                                                        foreach (session()->get('check_booking')['query_option'] as $key => $value) {
                                                                            $price = 0;
                                                                            echo '<tr class="tr-extra-options">';
                                                                            echo '<td class="td-w-50p td-extra-icon">';
                                                                            echo '<label class="checkbox pl30 label-cover" style="background-color: #fff;border: #fff;">';
                                                                            echo "<img src='".asset($value['extra_option_icon'])."' class='icon-formstep2'>";
                                                                            echo "<span class='pd10 title-extra'>".$value['extra_option_name']."</span>";
                                                                            echo '</label>';
                                                                            echo '</td>';
                                                                            echo '<td>';

                                                                            if((!empty(session()->get('check_booking')['extra_option_qty'])) && $value['id'] == (!empty(session()->get('check_booking')['extra_option_qty'][$value['id']]))  ){

                                                                                if($value['extra_option_per'] == 'day'){
                                                                                    $price = ($value['extra_option_price']*session()->get('check_booking')['extra_option_qty'][$value['id']])*$diff_day;
                                                                                }else{
                                                                                    $price = $value['extra_option_price']*session()->get('check_booking')['extra_option_qty'][$value['id']];
                                                                                }
                                                                               
                                                                                echo session()->get('check_booking')['extra_option_qty'][$value['id']];
                                                                            }else{
                                                                                if($value['extra_option_per'] == 'day'){
                                                                                    $price =  $value['extra_option_price']*$value['extra_option_qty']*$diff_day;
                                                                                }else{
                                                                                    $price = $value['extra_option_price']*$value['extra_option_qty'];
                                                                                }
                                                                                
                                                                                 echo number_format($value['extra_option_qty']);
                                                                            }
                                                                           
                                                                            echo '</td>';

                                                                            echo '<td>';
                                                                            echo  $value['extra_option_price'];
                                                                            echo '/';
                                                                            if($value['extra_option_per'] == 'day'){
                                                                                echo "วัน";
                                                                            }else{
                                                                                echo "หน่วย";
                                                                            }
                                                                            echo '</td>';

                                                                            echo '<td>';
                                                                            echo number_format($price);
                                                                            echo '</td>';
                                                                            echo '</tr>';
                                                                            $total_price_opion+=$price;
                                                                        }

                                                                    }else{
                                                                        echo "<tr><td colspan='4' style='text-align: center;'>-- ไม่มีรายการอุปกรณ์เสริม --</td></tr>";
                                                                    }

                                                                ?>
                                                                <tr style="display:none;">
                                                                <td colspan="3" align="right">รวมราคา:&nbsp;&nbsp;</td>
                                                                <td colspan="1"><strong><?= number_format($total_price_opion)?> บาท</strong></td>
                                                                </tr>
                                                                </table>
                                                            </div>
                                                            <div class="col-md-4">
                                                            <div class="gap"></div>
                                                                <h5 style="font-weight: 500;"> {{ __('index.frontend.all_lists') }} </h5>
                                                                <h5> {{ __('index.frontend.pick_up_point') }} </h5>
                                                                <table style="width:100%">
                                                                    <tbody style="text-indent: 24px;">
                                                                        <tr>
                                                                            <td style="width: 50%;">{{ __('index.frontend.pick_up2') }}</td>
                                                                            <td style="width: 50%;">: {{ session()->get('check_booking')['pick_up_text'] }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%;">{{ __('index.frontend.pick_up_date2') }}</td>
                                                                            <td style="width: 50%;">: {{ session()->get('check_booking')['pick_up_date_thai'] }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%;">{{ __('index.frontend.pick_up_time2') }}</td>
                                                                            <td style="width: 50%;">: {{ session()->get('check_booking')['pick_up_time_text'] }} น.</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <div class="gap gap-small" style="padding-top: 7px;"></div>
                                                                <hr style="border-top: 4px solid #287a5c" />
                                                                <h5>{{ __('index.frontend.drop_off_point') }}</h5>
                                                                <table style="width:100%">
                                                                    <tbody style="text-indent: 24px;">
                                                                        <tr>
                                                                            <td style="width: 50%;">{{ __('index.frontend.drop_off2') }}</td>
                                                                            <td style="width: 50%;">: {{ session()->get('check_booking')['drop_off_text'] }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%;">{{ __('index.frontend.drop_off_date2') }}</td>
                                                                            <td style="width: 50%;">: {{ session()->get('check_booking')['drop_off_date_thai'] }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%;">{{ __('index.frontend.drop_off_time2') }}</td>
                                                                            <td style="width: 50%;">: {{ session()->get('check_booking')['drop_off_time_text'] }} น.</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <div class="gap gap-small"></div>
                                                                <hr style="border-top: 4px solid #287a5c" />
                                                                    <h5>{{ __('index.frontend.price_summary') }}</h5>
                                                                  <table style="width: 100%;">
                                                                  <tbody style="text-indent: 24px;">
                                                                      
                                                                    <tr>
                                                                        <td style="width: 50%;">{{ __('index.frontend.price_rental_car') }}</td>
                                                                        <td style="width: 50%;">: <?=number_format($car_piece)?>&nbsp;{{ __('index.frontend.baht') }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 50%;">{{ __('index.frontend.extra_options_price') }}</td>
                                                                        <td style="width: 50%;">: <?=number_format($total_price_opion)?>&nbsp;{{ __('index.frontend.baht') }}</td>
                                                                    </tr>

                                                                    <?php
                                                                        if(!empty($dropoff_outside)){
                                                                    ?>
                                                                        <tr>
                                                                            <td style="width: 50%;">{{ __('index.frontend.outside_charge_service') }}</td>
                                                                            <td style="width: 50%;">: <?=number_format($dropoff_outside)?>&nbsp;{{ __('index.frontend.baht') }}</td>
                                                                        </tr>
                                                                    <?php        
                                                                        }
                                                                    ?>

                                                                    <tr>
                                                                        <td style="width: 50%;">{{ __('index.frontend.deposit') }}</td>
                                                                        <td style="width: 50%;">: <?=number_format($price_car_engine_cc)?>&nbsp;{{ __('index.frontend.baht') }}</td>
                                                                    </tr>

                                                                    <?php
                                                                        if($type_pay == 'pay_later'){
                                                                    ?>
                                                                            <tr>
                                                                                <td style="width: 50%;">{{ __('index.frontend.car_booking') }}</td>
                                                                                <td style="width: 50%;">: <?=number_format($price_of_book_car)?>&nbsp;{{ __('index.frontend.baht') }}</td>
                                                                            </tr>
                                                                    <?php
                                                                        }
                                                                    ?>

                                                                  </tbody>
                                                                    
                                                                </table>

                                                                <?php
                                                                    $all_total_price = 0;
                                                                    $all_total_price= $total_price_opion+$car_piece+$dropoff_outside+$price_car_engine_cc+$price_of_book_car;
                                                                ?>
                                                                <div class="gap gap-small"></div>
                                                                <hr style="border-top: 4px solid #287a5c" />
                                                                    <table style="width: 100%;">
                                                                        <tr>
                                                                            <td style="width: 50%;"><p style="font-size: 20px;color: #000;"><strong>{{ __('index.frontend.total_price') }}</strong><span style="font-size: 12px;">(+vat7%)</span></p></td>
                                                                            <td style="width: 50%;"><p style="font-size: 20px; color: #000;"><strong>
                                                                              <?php
                                                                                  $final_price = $all_total_price*'1.07';
                                                                                  echo number_format($final_price);
                                                                              ?>

                                                                              &nbsp;{{ __('index.frontend.baht') }}</strong></p></td>
                                                                        </tr>
                                                                    </table>
                                                                    <div class="gap gap-small"></div>

                                                                    <?php
                                                                        if(session()->get('check_booking')['type_pay'] == 'pay_now'){
                                                                    ?>

                                                                        <div class="form-group">
                                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                                                <a href="{{ url('customer-detail') }}" type="button" class="btn btn-default prev-step col-md-12" style="background: rgb(255, 0, 0);color: rgb(255, 255, 255);width: 100%;    margin-bottom: 8px;">
                                                                                    <i class="fa fa-arrow-circle-left"></i>&nbsp;{{ __('index.frontend.previous') }}</a>
                                                                            </div>
                                                                        </div>


                                                                        <div class="form-group">
                                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                                                <button type="button" id="topayment" class="btn-forgetpass" style="background: #287a5c;width:100%;">{{ __('index.frontend.next') }}&nbsp;<i class="fa fa-arrow-circle-right"></i></button>
                                                                            </div>
                                                                        </div>

                                                                       
                                                                        <!-- <a style="padding: 3px;background: #ffc438;" href="{{url('paypal/ec-checkout')}}" type="button" class="btn btn-primary btn-lg" id="load" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Order">
                                                                            <img  style="width: 185px;" src="https://woocommerce.com/wp-content/uploads/2016/04/PayPal-Express@2x.png">
                                                                        </a> -->
                                                                    <?php
                                                                        }else{
                                                                    ?>
                                                                        <a style="padding: 3px;background: #297a5c;border-color: green;width: 100%;" href="{{url('/payment-pay-later')}}" type="button" class="btn btn-primary btn-lg" id="load" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Order">{{ __('index.frontend.book_now') }}</a>

                                                                    <?php
                                                                        }
                                                                    ?> 
                                                                    
                                                                   
                                                            </div>
                                                        </div>
                                                        <div class="gap"></div>
                                                </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        </div>
                                    </li>
                                    <li class="tp-1" >
                                        <input class="radio-hack" type="radio" id="panel-nav-2" name="tab-link">
                                        <label class="intap" for="panel-nav-2" id="section_payment" style="float: left;line-height: 40px;display:none;">{{ __('index.frontend.payment') }}</label>
                                        <div class="panel-1">
                                            <div style="display:none;">
                                                                    <?php
                                                                        if(session()->get('check_booking')['type_pay'] == 'pay_now'){
                                                                    ?>
                                                                        <a style="padding: 3px;background: #ffc438;" href="{{url('paypal/ec-checkout')}}" type="button" class="btn btn-primary btn-lg" id="load" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Order"><img  style="width: 185px;" src="https://woocommerce.com/wp-content/uploads/2016/04/PayPal-Express@2x.png"></a>
                                                                    <?php
                                                                        }else{
                                                                    ?>
                                                                        <a style="padding: 3px;background: #297a5c;border-color: green;width: 100%;" href="{{url('/payment-pay-later')}}" type="button" class="btn btn-primary btn-lg" id="load" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Order">{{ __('index.frontend.book_now') }}</a>

                                                                    <?php
                                                                        }
                                                                    ?>
                                            </div>
                                            <div>
                                                <h3>{{ __('index.frontend.payment') }}</h3>
                                            </div>
                                            <div class="tabs-in">
                                                <input type="radio" name="tabs" id="tabone" checked="checked">
                                                <label class="intap" for="tabone" style="text-align: center;padding-top: 14px;padding-left: 20px;padding-right: 20px;padding-bottom: 10px;height: 92px !important;">
                                                    <i class="fa fa-credit-card" style="font-size:45px;color: #005f21;"></i><br>{{ __('index.frontend.card') }}
                                                </label>
                                                <div class="tab-in">
                                                    <p style="margin: 0;">สำหรับ ธนาคารธนชาต เมื่อกดปุ่มแล้ว คุณจะถูกเชื่อมต่อไปยังแบบฟอร์มการจ่ายเงิน</p>
                                                    <p>สำหรับ PayPal เมื่อกดปุ่มแล้ว คุณจะถูกเชื่อมต่อไปยังเว็บไซต์ของ PayPal</p>
                                                    <div class="paym" style="padding: 10px;float: left;">
                                                            <div class="col-md-6 col-sm-6 col-ms-6" style="padding-bottom: 10px;padding-left: 0;">

                                                                <Form method="post" action="{{ 'https://ipay.thanachartbank.co.th/3dsecure/dccpayment/payment.aspx' }}">
                                                                    <Input Type="hidden" Name="MERID" value="21211800861">
                                                                    <Input Type="hidden" Name="TERMINALID" value="18000185">
                                                                    <Input Type="hidden" Name="INVOICENO" value="255308001"> 
                                                                    <Input Type="hidden" Name="AMOUNT" value="100"> 
                                                                    <Input Type="hidden" Name="POSTURL" value="http://www.testmerchant.com/return.asp">
                                                                    <Input Type="Hidden" Name="POSTURL2" value=" http://www.testmerchant.com/return2.asp ">
                                                                    <Input Type="Hidden" Name="AUTOREDIRECT" value="N">
                                                                    <input type="Hidden" name="PAYMENTFOR" value=" MIC12372384"> 


                                                                    <button type="submit" style="width: 193px;height: 44px;border: 1px solid #e68013;border-radius: 5px;">
                                                                      <img  style="width: 185px;border-radius: 3px;" src="{{ asset("img/payment/logo_tbank.jpg") }}">
                                                                    </button>
                                                                </Form>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-ms-6" style="padding-left: 0;">
                                                                <a style="padding: 3px;background: #ffc438;" href="{{url('paypal/ec-checkout')}}" type="button" class="btn btn-primary btn-lg" id="load" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Order">
                                                                    <img  style="width: 185px;" src="https://woocommerce.com/wp-content/uploads/2016/04/PayPal-Express@2x.png">
                                                                </a>
                                                            </div> 
                                                    </div>  
                                                    <!-- <div class="col-md-6">
                                                    </div>  -->

                                                </div>

                                                <input type="radio" name="tabs" id="tabtwo">
                                                <label class="intap" for="tabtwo" style="text-align: center;padding-top: 10px;padding-left: 20px;padding-right: 20px;padding-bottom: 10px;height: 92px !important;">
                                                      <i class="fa fa-money" style="font-size:48px;color: #005f21;"></i><br>{{ __('index.frontend.transfer') }}
                                                </label>
                                                <div class="tab-in">
                                                    <h4 style="display: none;">เลือกธนาคารของคุณ</h4>
                                                      
                                                      <div>
                                                        <!-- <a style="padding: 3px;background: #fff;" href="#" type="button" class="btn btn-primary btn-lg">
                                                          <input type="image" style="width: 185px;padding: 3px;height: 36px;padding-bottom: 0px;" src="https://sv1.picz.in.th/images/2018/11/07/3KTw2v.png" alt="3KTw2v.png">
                                                        </a> -->
                                                        </div>
                                                        <p>
                                                          ธนาคารกสิกรไทย  สาขา บิ๊กซี สะพานใหม่  ประเภทออมทรัพย์<br>
                                                          ชื่อบัญชี : บริษัท ไทยเร้นท์อีโก้คาร์ จำกัด<br>
                                                          เลขบัญชี : 632-2-18772-4  
                                                        </p>
                                                      <p>
                                                        หลังจากโอนเสร็จ ขอให้ลูกค้าเก็บ slip หลักฐานการโอนไว้ <br>
                                                        และนำมาด้วยในวันรับรถเช่า และขอให้โทรศัพท์แจ้งเมื่อโอนเสร็จ
                                                      </p>
                                                      <p> 
                                                        หมายเหตุ การจองรถเช่าจะเสร็จสมบูรณ์ เมื่อลูกค้าโอนเงินมัดจำแล้ว เท่านั้น
                                                      </p>
                                                </div>
                                                <!-- <input type="radio" name="tabs" id="tabthree">
                                                <label class="intap" for="tabthree" style="padding-top: 10px;padding-left: 30px;padding-right: 30px;">N/A</label>
                                                <div class="tab-in">
                                                    <h1>Tab Three Content</h1>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                                </div> -->
                                            </div>


                                        </div>
                                    </li>
                                    <!-- <li class="tp-1">
                                      <input class="radio-hack" type="radio" id="panel-nav-3" name="tab-link">
                                      <label for="panel-nav-3">Panel 3</label>
                                      <div class="panel-1" style="min-height: 742px;">
                                        <h3>Panel 3</h3>
                                        <p>Sagittis tortor fermentum id. Etiam massa urna, congue non lacinia in, luctus sed neque. Sed quis velit in felis vestibulum dapibus. Suspendisse pharetra malesuada porttitor. Morbi sit amet laoreet mauris. Morbi ante nisl, lacinia at lacus nec, imperdiet accumsan dolor.</p>
                                      </div>
                                    </li> -->
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                </div>
        </div>
    </section>
   </div>
</div>

@endsection


@section('begin_javascript')

<script>
    jQuery('span.round-tab').click(false);
</script>


<script type="text/javascript">
    
$(document).ready(function(){


    

    jQuery("button#topayment").click(function(){
        jQuery("label#section_payment").trigger("click");
    });
    $(".phone").text(function(i, text) {
        text = text.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "$1-$2-$3");
        return text;
    });

    $('.btn').on('click', function() {
    var $this = $(this);
  $this.button('loading');
    setTimeout(function() {
       $this.button('reset');
   }, 8000);
});
    
});

</script>
@endsection
