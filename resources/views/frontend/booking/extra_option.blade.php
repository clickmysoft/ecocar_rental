@extends('frontend.layouts.app')

@section('begin_css')
<link rel="stylesheet" href="{{ asset("frontend/css/font-awesome.css")}}">
<link rel="stylesheet" type="text/css" href="{{ asset("frontend/css/form-step.css")}}">
<link rel="stylesheet" href="{{ asset("frontend/css/styleecocar.css")}}">
<link rel="stylesheet" href="{{ asset("frontend/css/icomoon.css")}}">
 
<style>
    .nav-tabs {
        border-bottom: none; 
    }
    .form-group.form-group-icon-left .form-control {
    padding-left: 28px;
    }
    
    ul.pagination.results{
            padding-left: 20px;
    }

    div#abcdef {
        padding-top: 130px;
    }
    
    @media (max-width: 576px){
        .box-carrental-summery{
            width: 100%;
            padding: 0;
        }
        .price-carrental{
            border-bottom: 4px solid;
            margin-bottom: 10px;
        }
    }
    
    @media (max-width: 376px){
    .pl30{
        padding-left: 0 !Important;
    }
    
    }
    @media (max-width: 612px){
    .title-extra{
        width: 100%;
        display: block;
    }
    .baby-seat{
        display: block;
        margin: 0 auto;
        margin-top: 10px;
    }
    .center-mobile{
        text-align: center;
    }
    td.td-extra-icon{
        width: 100%;
        display: block;
        text-align: center;
    }
    td.td-extra-price{
         width: 100%;
         display: block;
        border-bottom: 4px solid #000;
    }
    input.checkbox-formstep{
        float: none !important;
    }
    .pl30{
        padding-left: 0 !Important;
    }
    td.td-extra-icon{
        margin-top: 10px;
    }
    .title-extra{
        padding-bottom: 0;
    }
    input.checkbox-formstep{
        transform: scale(2);
        height: 10px;
        margin-right: 10px;
        margin-top: -10px;
    }
   
    }
        @media (max-width: 991px){
        .img-paypal{
            padding-left: 0;
            }
            .btn-payletter,.btn-now{
                width: 100%;
            }
            
        }

        .form-group .input-icon{
                top: 27px;
        }
 li.active .title-formstep {
    color: #FFEB3B !important;
}

.wizard li.active span.round-tab i {
    color: #FF5722 !important;
}

.connecting-line {
    height: 4px;
    background: #00551f;
    position: absolute;
    width: 25%;
    margin: 0 auto;
    left: 0;
    right: 513px;
    top: 52%;
    z-index: 1;
}

.connecting-line2 {
    height: 4px;
    background: #ffffff;
    position: absolute;
    width: 80%;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: 52%;
    z-index: 0;
}
    </style>
@endsection

@section('content')



<div class="container">
            
    <div class="row">
      <section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <div class="connecting-line2"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="disabled">
                        <span class="title-formstep">{{ __('index.frontend.vehicles') }}</span>
                        <a href="#" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            <span class="round-tab" onclick="#" style="background-color: #00551f;border: 2px solid rgb(255, 255, 255);">
                               <i class="fa fa-car"  style="color:#fff;"></i>
                               <!-- <img class="icon-formstep" src="{{ asset("frontend/img/car-formstep-02.png")}}" alt=""> -->
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="active">
                       <span class="title-formstep">{{ __('index.frontend.extra') }}</span>
                        <a href="#" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                            <span class="round-tab" onclick="#">
                                <i class="fa fa-taxi"></i>
                                <!-- <img class="icon-formstep" src="{{ asset("frontend/img/car-formstep-02.png")}}" alt=""> -->
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <span class="title-formstep">{{ __('index.frontend.customer_detail') }}</span>
                        <a href="#" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                            <span class="round-tab" onclick="#">
                                <i class="fa fa-list-alt"></i>
                                <!-- <img class="icon-formstep" src="{{ asset("frontend/img/img_311846.png")}}" alt=""> -->
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <span class="title-formstep">{{ __('index.frontend.payment') }}</span>
                        <a href="#" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab" onclick="#">
                                <i class="fa fa-file-text-o"></i>
                                <!-- <img class="icon-formstep" src="{{ asset("frontend/img/car-formstep-02.png")}}" alt=""> -->
                            </span>
                        </a>
                    </li>

                </ul>
            </div>

              <form method="POST" action="{{ route('frontend.customer-detail.store') }}">
                    {{ csrf_field() }}
                <div class="tab-content">
            
                    <div class="tab-pane active" role="tabpanel" id="{{ url('extra-option') }}">
                        <div class="{{ url('extra-option') }}">
                            <div class="step_21">
                                <div class="row">
                                    <div class="container">
                                        <div class="col-md-12">
                                            <p class="bg-formstep pd10">{{ __('index.frontend.extra_options') }}</p>
                                                <table class="tb-w-100p">
                                                    <?php
                                                        foreach ($extra_option as $key => $value) {
                                                    ?>
                                                        <tr class="tr-extra-options">
                                                            <td class="td-w-50p td-extra-icon">
                                                                <label class="checkbox pl30 label-cover">
                                                                    <input type="checkbox" name="extra_option[]"  

                                                                    <?php
                                                                        if(!empty(session()->get('check_booking')['extra_option'])){
                                                                            foreach (session()->get('check_booking')['extra_option'] as $key3 => $value3) {
                                                                                if($value['id'] == $value3){
                                                                                    echo "checked ";
                                                                                }
                                                                            }
                                                                        }
                                                                        
                                                                    ?>

                                                                    value="<?=$value['id']?>" class="checkbox-formstep">
                                                                    <img src="<?=asset($value['extra_option_icon'])?>" alt="" class="icon-formstep2">
                                                                    
                                                                    <?php
                                                                        if($value['extra_option_qty'] > 1){
                                                                    ?>
                                                                        <select class="selectpicker baby-seat" name="extra_option_qty[<?=$value['id']?>]">
                                                                            <?php
                                                                                for ($i=1; $i<=$value['extra_option_qty']; $i++) { 
                                                                                    echo "<option  value=".$i.">Baby Seat $i</option>";
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    <?php        
                                                                        }
                                                                    ?>

                                                                    <span class="pd10 title-extra">{{$value['extra_option_name']}}</span>
                                                                </label>
                                                            </td>
                                                            <td class="td-w-50p td-extra-price">
                                                                
                                                            <?php
                                                                if($value['extra_option_code'] == 'AA112209' || $value['extra_option_code'] == 'AA313233'){
                                                            ?>
                                                                 <label class="mb20 center-mobile">{{$value['extra_option_price']}}&nbsp;{{ __('index.frontend.baht') }}</label>
                                                            <?php    
                                                                }else{
                                                            ?>
                                                                 <label class="mb20 center-mobile">{{$value['extra_option_price']}} &nbsp;{{ __('index.frontend.baht_day') }}</label>
                                                            <?php
                                                                }
                                                            ?>
                                                                
                                                            </td>
                                                        </tr>

                                                    <?php
                                                        }
                                                    ?>
                                                    
                                                </table>

                                            </div>

                                        <div class="col-md-12" style="display:none;">
                                            <p class="bg-formstep pd10">CAR RENTAL SUMMARY</p>

                                                <div class="box-carrental-summery">
                                          <div class="row">
                                              <div class="col-sm-9"><div class="">Car price (Vat excluded)</div></div>
                                                <div class="col-sm-3"><div class="text-right price-carrental">1,200.00 THB</div></div>
                                                <div class="col-sm-9"><div class="">Option Price</div></div>
                                                <div class="col-sm-3"><div class="text-right price-carrental" id="option_text">0.00 THB</div></div>
                                                <div class="col-sm-9"><div class="">VAT 7%</div></div>
                                                <div class="col-sm-3"><div class="text-right price-carrental" id="vat_text">84.00 THB</div></div>
                                                <div class="col-sm-9"><div class=""><strong>Total price</strong></div></div>
                                                <div class="col-sm-3"><div class="text-right price-carrental" id="total_text"><strong>1,284.00 THB</strong></div></div>
                                            </div><!--end row-->
                                        </div>

                                        </div>

                                         <div class="col-md-12">
                                            <p class="bg-formstep pd10">{{ __('index.frontend.remark') }}</p>
                                            <div class="form-group">
                                              <textarea class="form-control" rows="5" id="comment" name="comment_remark">{{ @session()->get('check_booking')['comment_remark'] }}</textarea>
                                            </div>
                                         </div>


                                    </div>
                                </div>
                            </div>
                            <div class="step-22">

                            </div>
                        </div>
                        <ul class="list-inline pull-right">
                            <li><a href="{{ url('vehicles') }}" type="button" class="btn btn-default prev-step" style="background: #ff0000;color: #fff;width: 150px;"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ __('index.frontend.previous') }}</a></li>
                            <li>
                                <button type="submit" class="btn-forgetpass next-step" style="width: 150px;background: #287a5c;">{{ __('index.frontend.next') }}&nbsp;<i class="fa fa-arrow-circle-right"></i></button>
                            </li>
                        </ul>
                    </div>
            </form>
        </div>
    </section>
   </div>
</div>

@endsection


@section('begin_javascript')

<script>
    jQuery('span.round-tab').click(false);
</script>

@endsection
