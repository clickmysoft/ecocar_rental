@extends('frontend.layouts.app')

@section('begin_css')
<link rel="stylesheet" href="{{ asset("frontend/css/font-awesome.css")}}">
<link rel="stylesheet" type="text/css" href="{{ asset("frontend/css/form-step.css")}}">
<link rel="stylesheet" href="{{ asset("frontend/css/styleecocar.css")}}">
<link rel="stylesheet" href="{{ asset("frontend/css/icomoon.css")}}">
 <style>


.pagination {
    /* padding-left: 70px; */
    /* margin: auto; */
    /* margin: auto; */
    margin: auto;
    display: -webkit-box;
    padding-left:0px;
}

 .pagination > li > a, .pagination > li > span{
    border:none !important;
 }


 .booking-filters{
    height: 1000px;
 }

 div#abcdef {
    padding-top: 130px;
}


    .nav-tabs {
        border-bottom: none; 
    }
    .form-group.form-group-icon-left .form-control {
    padding-left: 28px;
    }
    
    ul.pagination.results{
            padding-left: 20px;
    }
    
    @media (max-width: 576px){
        .box-carrental-summery{
            width: 100%;
            padding: 0;
        }
        .price-carrental{
            border-bottom: 4px solid;
            margin-bottom: 10px;
        }
    }
    
    @media (max-width: 376px){
    .pl30{
        padding-left: 0 !Important;
    }
    
 }
    @media (max-width: 612px){
    .title-extra{
        width: 100%;
        display: block;
    }
    .baby-seat{
        display: block;
        margin: 0 auto;
        margin-top: 10px;
    }
    .center-mobile{
        text-align: center;
    }
    td.td-extra-icon{
        width: 100%;
        display: block;
        text-align: center;
    }
    td.td-extra-price{
         width: 100%;
         display: block;
        border-bottom: 4px solid #000;
    }
     
    input.checkbox-formstep{
        float: none !important;
    }
    .pl30{
        padding-left: 0 !Important;
    }
    td.td-extra-icon{
        margin-top: 10px;
    }
    .title-extra{
        padding-bottom: 0;
    }
    input.checkbox-formstep{
        transform: scale(2);
        height: 10px;
        margin-right: 10px;
        margin-top: -10px;
    }

    
   
}

@media (max-width: 991px){
.img-paypal{
    padding-left: 0;
    }
    .btn-payletter,.btn-now{
        width: 100%;
    }
    
}
.booking-item-car-title {
    text-align: center;
}

.form-group .input-icon{
        top: 27px;
}

li.active .title-formstep {
    color: #FFEB3B !important;
}
.wizard li.active span.round-tab i {
    color: #FF5722 !important;
}

.connecting-line {
    height: 4px;
    }

    </style>
@endsection

@section('content')

<?php 
    // echo "<pre>";
    // print_r($cars);
    // echo "</pre>";
?>

<div class="container">
            
    <div class="row">
      <section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active">
                        <span class="title-formstep">{{ __('index.frontend.vehicles') }}</span>
                        <a href="#" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            <span class="round-tab" onclick="#">
                                <i class="fa fa-car"></i>
                               <!-- <img class="icon-formstep" src="{{ asset("frontend/img/car-formstep-02.png")}}" alt=""> -->
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                       <span class="title-formstep">{{ __('index.frontend.extra') }}</span>
                        <a href="#" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2"> 
                            <span class="round-tab" onclick="#">
                                <i class="fa fa-taxi"></i>
                                <!-- <img class="icon-formstep" src="{{ asset("frontend/img/car-formstep-02.png")}}" alt=""> -->
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <span class="title-formstep">{{ __('index.frontend.customer_detail') }}</span>
                        <a href="#" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                            <span class="round-tab">
                                <i class="fa fa-list-alt"></i>
                                <!-- <img class="icon-formstep" src="{{ asset("frontend/img/img_311846.png")}}" alt=""> -->
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <span class="title-formstep">{{ __('index.frontend.summary') }}</span>
                        <a href="#" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab">
                                <i class="fa fa-file-text-o"></i>
                                <!-- <img class="icon-formstep" src="{{ asset("frontend/img/car-formstep-02.png")}}" alt=""> -->
                            </span>
                        </a>
                    </li>

                </ul>

            </div>

           
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="{{ url('vehicles') }}">

                        <div class="step1 formstep-border">


                            <div class="row">
                                <div class="col-md-12 pl10-pr10">
                                          <div class="booking-item-dates-change mb10">
                                          
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group form-group-icon-left"><i class="fa fa-map-marker input-icon input-icon-hightlight"></i>
                                <label class="search-results">{{ __('index.frontend.pick_up2') }}</label>
                                <span class="twitter-typeahead" style="position: relative; display: block; direction: ltr;">
                                    <label class="typeahead form-control tt-input"  type="text" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: #fff;"><?=@str_limit($data['pick_up_text'],28)?>
                                    </lebel>
                                    <pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: &quot;Open Sans&quot;, Tahoma, Arial, helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;">
                                    </pre>
                                    <span class="tt-dropdown-menu" style="position: absolute; top: 100%; left: 0px; z-index: 100; display: none; right: auto;"><div class="tt-dataset-2"></div>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                                        <label class="search-results">{{ __('index.frontend.pick_up_date2') }}</label>
                                        <input class="form-control" name="start" type="text" value="<?=@$data['pick_up_date_text']?>" readonly>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group form-group-icon-left"><i class="fa fa-clock-o input-icon input-icon-hightlight"></i>
                                        <label class="search-results">{{ __('index.frontend.pick_up_time2') }}</label>
                                        <input class=" form-control" type="text" value="<?=@$data['pick_up_time_text']?>" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-group-icon-left"><i class="fa fa-map-marker input-icon input-icon-hightlight"></i>
                                <label class="search-results">{{ __('index.frontend.drop_off2') }}</label>
                                <span class="twitter-typeahead" style="position: relative; display: block; direction: ltr;">
                                    <label class="typeahead form-control tt-input"  type="text" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: #fff;"><?=@str_limit($data['drop_off_text'],28)?></lebel>
                                    <pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: &quot;Open Sans&quot;, Tahoma, Arial, helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre>
                                    <span class="tt-dropdown-menu" style="position: absolute; top: 100%; left: 0px; z-index: 100; display: none; right: auto;"><div class="tt-dataset-3"></div></span></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                                        <label class="search-results">{{ __('index.frontend.drop_off_date2') }}</label>
                                        <input class="form-control" name="end" type="text" value="{{ @$data['drop_off_date_text'] }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group form-group-icon-left"><i class="fa fa-clock-o input-icon input-icon-hightlight"></i>
                                        <label class="search-results">{{ __('index.frontend.drop_off_time2') }}</label>
                                        <input class=" form-control" value="{{ @$data['drop_off_time_text'] }}" type="text" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>

                                </div>
                                    <div class="col-md-3 col-lg-3 hidden-sm hidden-xs" style="display:none;">
                                                <aside class="booking-filters text-white">
                                                    <h3>{{ __('index.frontend.filter') }}</h3>
                                                    <ul class="list booking-filters-list">
                                                       
                                                       <!-- Price <li>
                                                            <h5 class="booking-filters-title">{{ __('index.frontend.price') }}</h5>
                                                            <input type="text" id="price-slider">
                                                        </li> -->
                                                        <li>
                                                            <h5 class="booking-filters-title">{{ __('index.frontend.car_group') }} </h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />{{ __('index.frontend.economy') }} (20)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />{{ __('index.frontend.compact') }} (20)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />{{ __('index.frontend.mid_size') }} (11)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Stabdart (12)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />{{ __('index.frontend.full_size') }} (5)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Premium/Luxury (3)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Minivan (5)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Crossover (10)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />{{ __('index.frontend.suv') }} (12)</label>
                                                            </div>
                                                        </li>
                                                      
                                                        <li>
                                                            <h5 class="booking-filters-title">Engine</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Gas (60)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Diesel (35)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Hybrid (22)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Electric (15)</label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </aside>
                                            </div>
                                    <div class="col-md-12">
                                               
                                                <ul class="booking-list">
                                                    <?php
                                                        if(count($cars) > 0){
                                                            foreach ($cars as $key => $car) {
                                                    ?>
                                                            <li>
                                                                <div class="booking-item" href="#">

                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <div class="booking-item-car-img">
                                                                                <img rel="tooltip" data-placement="top" title="{{ $car->id }}" src="{{asset("cars/$car->car_brand/$car->car_generation/image-01.jpg")}}" alt="Image Alternative text" title="Image Title" />
                                                                                <p class="booking-item-car-title">{{ strtoupper($car->car_brand) }} {{ strtoupper($car->car_generation) }} [{{ $car->license_plate }}]</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <ul class="booking-item-features booking-item-features-sign clearfix">
                                                                                        <li rel="tooltip" data-placement="top" title="Passengers"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x {{ $car->car_human_qty }}</span>
                                                                                        </li>
                                                                                        <li rel="tooltip" data-placement="top" title="Doors"><i class="im im-car-doors"></i><span class="booking-item-feature-sign">x {{ $car->car_door_qty }}</span>
                                                                                        </li>
                                                                                        <li rel="tooltip" data-placement="top" title="Baggage Quantity"><i class="fa fa-briefcase"></i><span class="booking-item-feature-sign">x {{ $car->car_baggage_qty }}</span>
                                                                                        </li>
                                                                                        <li rel="tooltip" data-placement="top" title="Automatic Transmission"><i class="im im-shift-auto"></i><span class="booking-item-feature-sign">
                                                                                            <?=$car->car_type_gear == 'automatic_gear'?'Auto':'Manual'?>
                                                                                                
                                                                                            </span>
                                                                                        </li>
                                                                                        <li rel="tooltip" data-placement="top" title="

                                                                                        <?php
                                                                                            if(!empty($car->car_type_fule)){

                                                                                                $fule = array();
                                                                                                $size = sizeof(json_decode($car->car_type_fule)) - 1;
                                                                                                foreach (json_decode($car->car_type_fule) as $key33 => $value33) {
                                                                                                    if($size !=$key33){
                                                                                                         echo str_replace('_',' ',strtoupper($value33).', ');
                                                                                                    }else{
                                                                                                         echo str_replace('_',' ',strtoupper($value33).'');
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        ?>

                                                                                        ">
                                                                                            <i class="im im-electric"></i>
                                                                                        </li>
                                                                                    </ul>
                                                                                    <ul class="booking-item-features booking-item-features-small clearfix">
                                                                                        <?php
                                                                                            foreach (json_decode($car->car_option,true) as $key => $value) {
                                                                                                // echo $value;
                                                                                                // echo "<br>";
                                                                                                if ($value == 'power_door') {
                                                                                        ?>
                                                                                                    <li rel="tooltip" data-placement="top" title="Power Windows"><i class="im im-car-window"></i>
                                                                                                    </li>
                                                                                        <?php
                                                                                                } else if ($value == 'satellite_navigation') {
                                                                                        ?>
                                                                                                    <li rel="tooltip" data-placement="top" title="Satellite Navigation"><i class="im im-satellite"></i>
                                                                                                    </li>
                                                                                        <?php
                                                                                                } else if ($value == 'fm_radio_and_stereo_cd') {
                                                                                        ?>
                                                                                                    <li rel="tooltip" data-placement="top" title="FM Radio"><i class="im im-fm"></i>
                                                                                                    </li>
                                                                                        <?php
                                                                                                } else if ($value == 'tilt_steering_wheel') {
                                                                                        ?>
                                                                                                    <li rel="tooltip" data-placement="top" title="Tilt Steering Wheel"><i class="im im-car-wheel"></i>
                                                                                                    </li>
                                                                                        <?php
                                                                                                } else if ($value == 'touch_screen_media_player') {
                                                                                        ?>
                                                                                                    <li rel="tooltip" data-placement="top" title="Touch screen media player"><i class="fa fa-mobile" style="font-size:24px"></i>
                                                                                                    </li>
                                                                                        <?php
                                                                                                } else if ($value == 'camera') {
                                                                                        ?>
                                                                                                    <li rel="tooltip" data-placement="top" title="Camera"><i class="fa fa-camera-retro"></i>
                                                                                                    </li>
                                                                                        <?php
                                                                                                } else if ($value == 'sensor') {
                                                                                        ?>
                                                                                                    <li rel="tooltip" data-placement="top" title="Sensor"><i class="fa fa-rss"></i>
                                                                                                    </li>
                                                                                        <?php
                                                                                                }
                                                                                            }
                                                                                        ?>
                                                                                        
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-md-4" style="text-align: center;margin-top: 5%;">

                                                                            <form method="POST" action="{{ route('frontend.extra-option.store') }}">
                                                                                {{ csrf_field() }}

                                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                    <?php
                                                                                         $price = $car->price-$discount_for_pay_now;
                                                                                    ?>
                                                                                    <div><?=number_format(@$price)?> {{ __('index.frontend.baht') }}</div>
                                                                                    <input type="hidden" name="type_pay" value="pay_now">
                                                                                    <input type="hidden" name="car_id" value="{{$car->id}}">
                                                                                    <button type="submit" class="btn-now" style="background-image: linear-gradient(to right, #df3333 0%, #f26168 51%, #c54d558a 100%);">
                                                                                        {{ __('index.frontend.pay_now') }}
                                                                                    </button>
                                                                                </div>
                                                                            </form>

                                                                             <form method="POST" action="{{ route('frontend.extra-option.store') }}">
                                                                                {{ csrf_field() }}
                                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                    <div><?=number_format(@$car->price)?> {{ __('index.frontend.baht') }}</div>
                                                                                    <input type="hidden" name="type_pay" value="pay_later">
                                                                                    <input type="hidden" name="car_id" value="{{$car->id}}">
                                                                                    <button type="submit" class="btn-payletter" style="background-image: linear-gradient(to right, #1e7b40 0%, #15976b 51%, #86dba6 100%);">
                                                                                        {{ __('index.frontend.pay_later') }}
                                                                                    </button>
                                                                                </div>
                                                                            </form>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        <?php
                                                            }
                                                        }else{
                                                    ?>
                                                        <li style="text-align: center;padding: 25px;margin-bottom: 0;">
                                                            <h4 style="margin-bottom: 0;">{{ __('index.frontend.branch_no_car') }}</h4>
                                                            <h5 style="margin-bottom: 0;">{{ __('index.frontend.select_new_branch') }}</h5>
                                                        </li>
                                                        <li style="text-align: center;">
                                                            <a  href="{{ url('extra-option') }}" type="button" class="btn btn-default prev-step" style="background: #ff0000;color: #fff;width: 150px;">
                                                                <!-- <i class="fa fa-arrow-circle-left"></i>&nbsp; -->{{ __('index.frontend.select_branch') }}
                                                            </a>
                                                        </li>

                                                    <?php        
                                                        }
                                                    ?>


                                                 </ul>



                                                <ul class="pagination">
                                                      {{ $cars->links() }}
                                                </ul>

                                                   

                                        </div>                                           
                             </div>
                        </div>
                        <!-- <ul class="list-inline pull-right">
                            <li><button type="button" class="btn-forgetpass next-step">Save and continue</button>
                            </li>
                        </ul> -->
                         <div class="col-xs-12 col-sm-12 hidden-md hidden-lg" style="padding-left: 0; padding-right: 0;">
                                                <aside class="booking-filters text-white" style="margin-bottom: 20px;">
                                                    <h3>Filter By:</h3>
                                                    <ul class="list booking-filters-list">
                                                        
                                                        <li>
                                                            <h5 class="booking-filters-title">Price </h5>
                                                            <input type="text" id="price-slider">
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">Car group</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Economy (20)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Compact (20)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Midsize (11)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Stabdart (12)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Fullsize (5)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Premium/Luxury (3)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Minivan (5)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Crossover (10)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />SUV (12)</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">Transmission</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Automatic (80)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Manual (25)</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">Engine</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Gas (60)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Diesel (35)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Hybrid (22)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Electric (15)</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">Equipment</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Climate Control (47)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Air Conditioning (66)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Satellite Navigation (30)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Power Door Locks (35)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />FM Radio (70)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Stereo CD/MP3 (53)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Tilt Steering Wheel (42)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Power Windows (68)</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">Pickup Options</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Terminal Pickup (80)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Shuttle Bus to Car (25)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Meet and Greet (13)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Car with Driver (7)</label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </aside>
                                            </div>
                    </div>
        </div>
    </section>
   </div>
</div>

@endsection


@section('begin_javascript')

<script>
    jQuery('span.round-tab').click(false);
</script>

@endsection
