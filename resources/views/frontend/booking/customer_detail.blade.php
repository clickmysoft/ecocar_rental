@extends('frontend.layouts.app')

@section('begin_css')
        <link rel="stylesheet" href="{{ asset("frontend/css/font-awesome.css")}}">
        <link rel="stylesheet" type="text/css" href="{{ asset("frontend/css/form-step.css")}}">
        <link rel="stylesheet" href="{{ asset("frontend/css/styleecocar.css")}}">
        <link rel="stylesheet" href="{{ asset("frontend/css/icomoon.css")}}">
         <style>
            .nav-tabs {
                border-bottom: none; 
            }
            .form-group.form-group-icon-left .form-control {
            padding-left: 28px;
            }
            
            ul.pagination.results{
                    padding-left: 20px;
            }
            div#abcdef {
                padding-top: 130px;
            }
            
            @media (max-width: 576px){
                .box-carrental-summery{
                    width: 100%;
                    padding: 0;
                }
                .price-carrental{
                    border-bottom: 4px solid;
                    margin-bottom: 10px;
                }
            }
            
            @media (max-width: 376px){
            .pl30{
                padding-left: 0 !Important;
            }
            
         }
            @media (max-width: 612px){
            .title-extra{
                width: 100%;
                display: block;
            }
            .baby-seat{
                display: block;
                margin: 0 auto;
                margin-top: 10px;
            }
            .center-mobile{
                text-align: center;
            }
            td.td-extra-icon{
                width: 100%;
                display: block;
                text-align: center;
            }
            td.td-extra-price{
                 width: 100%;
                 display: block;
                border-bottom: 4px solid #000;
            }
             
            input.checkbox-formstep{
                float: none !important;
            }
            .pl30{
                padding-left: 0 !Important;
            }
            td.td-extra-icon{
                margin-top: 10px;
            }
            .title-extra{
                padding-bottom: 0;
            }
            input.checkbox-formstep{
                transform: scale(2);
                height: 10px;
                margin-right: 10px;
                margin-top: -10px;
            }
           
        }

        @media (max-width: 991px){
        .img-paypal{
            padding-left: 0;
            }
            .btn-payletter,.btn-now{
                width: 100%;
            }
            
        }


        .form-group .input-icon{
                top: 27px;
        }

li.active .title-formstep {
    color: #FFEB3B !important;
}

.wizard li.active span.round-tab i {
    color: #FF5722 !important;
}

.connecting-line {
    height: 4px;
    background: #00551f;
    position: absolute;
    width: 50%;
    margin: 0 auto;
    left: 0;
    right: 300px;
    top: 52%;
    z-index: 1;
}

.connecting-line2 {
    height: 4px;
    background: #ffffff;
    position: absolute;
    width: 80%;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: 52%;
    z-index: 0;
}

            </style>
@endsection

@section('content')


<div class="container">
            
    <div class="row">
      <section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <div class="connecting-line2"></div>
                <ul class="nav nav-tabs" role="tablist">

                   <li role="presentation" class="disabled">
                        <span class="title-formstep">{{ __('index.frontend.vehicles') }}</span>
                        <a href="#" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            <span class="round-tab" onclick="#" style="background-color: #00551f;border: 2px solid rgb(255, 255, 255);">
                               <i class="fa fa-car"  style="color:#fff;"></i>
                               <!-- <img class="icon-formstep" src="{{ asset("frontend/img/car-formstep-02.png")}}" alt=""> -->
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                       <span class="title-formstep">{{ __('index.frontend.extra') }}</span>
                        <a href="#" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                            <span class="round-tab" onclick="#" style="background-color: #00551f;border: 2px solid rgb(255, 255, 255);">
                                <i class="fa fa-taxi" style="color:#fff;"></i>
                                <!-- <img class="icon-formstep" src="{{ asset("frontend/img/car-formstep-02.png")}}" alt=""> -->
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="active">
                        <span class="title-formstep">{{ __('index.frontend.customer_detail') }}</span>
                        <a href="#" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                            <span class="round-tab" onclick="#">
                                <i class="fa fa-list-alt"></i>
                                <!-- <img class="icon-formstep" src="{{ asset("frontend/img/img_311846.png")}}" alt=""> -->
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <span class="title-formstep">{{ __('index.frontend.payment') }}</span>
                        <a href="#" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab" onclick="#">
                                <i class="fa fa-file-text-o"></i>
                                <!-- <img class="icon-formstep" src="{{ asset("frontend/img/car-formstep-02.png")}}" alt=""> -->
                            </span>
                        </a>
                    </li>

                </ul>
            </div>

              <form method="POST" action="{{ route('frontend.customer.detail') }}">
                    {{ csrf_field() }}

                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step3">
                        <div class="step_21" style="padding: 0;">
                              <div class="step44">
                                 <div class="container">
            <div class="col-md-12">
               <p class="bg-formstep pd10 bar-cus">{{ __('index.frontend.customer') }}</p>
            </div>
            <div class="row" style="padding-top: 0px">
                
            
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="col-md-12">
                                  <p class="checkout pd10">{{ __('index.frontend.your_information') }}</p>
                             </div>
                                                          
                             <div class="row mar_ned">
                                <div class="col-md-4 col-xs-4">
                                    <p align="right"><stong>{{ __('index.frontend.first_name') }} </stong></p>
                                </div>
                                <div class="col-md-6 col-xs-8">

                                <?php
                                    $first_name = '';
                                    if(!empty($logged_in_user->first_name)){
                                         $first_name = $logged_in_user->first_name;
                                    }else{
                                         $first_name = @session()->get('check_booking')['first_name'];
                                    }
                                ?>

                                  <input type="text" class="form-control" id="exampleInputEmail1" name="first_name" value="{{ $first_name }}">
                                </div>
                            </div>
                            <div class="row mar_ned">
                                <div class="col-md-4 col-xs-4">
                                    <p align="right"><stong>{{ __('index.frontend.last_name') }} </stong></p>
                                </div>
                                <div class="col-md-6 col-xs-8">
                                    <?php
                                        $last_name = '';
                                        if(!empty($logged_in_user->last_name)){
                                             $last_name = $logged_in_user->last_name;
                                        }else{
                                             $last_name = @session()->get('check_booking')['last_name'];
                                        }
                                    ?>

                                    <input type="text" class="form-control" id="exampleInputEmail1" name="last_name" value="{{ $last_name }}">
                                </div>
                            </div>
                            <div class="row mar_ned">
                                <div class="col-md-4 col-xs-4">
                                    <p align="right"><stong>{{ __('index.frontend.email') }} </stong></p>
                                </div>
                                <div class="col-md-6 col-xs-8">

                                    <?php
                                        $email = '';
                                        if(!empty($logged_in_user->email)){
                                             $email = $logged_in_user->email;
                                        }else{
                                             $email = @session()->get('check_booking')['email'];
                                        }
                                    ?>

                                    <input type="email" class="form-control" id="exampleInputEmail1" name="email" value="{{ $email }}">
                                </div>
                            </div>
                            <div class="row mar_ned">
                                <div class="col-md-4 col-xs-4">
                                    <p align="right"><stong>{{ __('index.frontend.phone') }} </stong></p>
                                </div>
                                <div class="col-md-6 col-xs-8">
                                    <?php
                                        $phone = '';
                                        if(!empty($logged_in_user->phone)){
                                             $phone = $logged_in_user->phone;
                                        }else{
                                             $phone = @session()->get('check_booking')['phone'];
                                        }
                                    ?>

                                    <input type="tel" class="form-control" maxlength="10" id="exampleInputtel"  name="phone" value="{{ $phone }}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                  <p class="checkout pd10">{{ __('index.frontend.travel') }}</p>
                             </div>
                              <div class="row mar_ned">
                                <div class="col-md-4 col-xs-4">
                                    <p align="right"><stong>{{ __('index.frontend.airline') }}</stong></p>
                                </div>
                                <div class="col-md-6 col-xs-8">
                                    <?php
                                        $travel_flight_info = '';
                                        if(!empty($logged_in_user->travel_flight_info)){
                                             $travel_flight_info = $logged_in_user->travel_flight_info;
                                        }else{
                                             $travel_flight_info = @session()->get('check_booking')['travel_flight_info'];
                                        }
                                    ?>

                                    <input type="text" class="form-control" id="exampleInputtext"  name="travel_flight_info" value="{{$travel_flight_info}}">
                                </div>
                            </div>
                                        <div class="row mar_ned">
                                <div class="col-md-4 col-xs-4">
                                    <p align="right"><stong>{{ __('index.frontend.flight_number') }}</stong></p>
                                </div>
                                <div class="col-md-6 col-xs-8">
                                    <?php
                                        $travel_flight_number = '';
                                        if(!empty($logged_in_user->travel_flight_number)){
                                             $travel_flight_number = $logged_in_user->travel_flight_number;
                                        }else{
                                             $travel_flight_number = @session()->get('check_booking')['travel_flight_number'];
                                        }
                                    ?>

                                    <input type="tel" class="form-control" id="exampleInputtel" name="travel_flight_number" value="{{$travel_flight_number}}">
                                </div>
                            </div>
                        
                            
                        </div>
                    </div>
                </div>

               

            </div>

            <div class="gap"></div>

        </div>
                            </div>
                        </div>
                        <ul class="list-inline pull-right">
                            <li><a  href="{{ url('extra-option') }}" type="button" class="btn btn-default prev-step" style="background: #ff0000;color: #fff;width: 150px;"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ __('index.frontend.previous') }}</a></li>

                           <!-- <li><button type="button" class="btn btn-default next-step">Skip</button></li> -->
                            <li><button type="submit" class="btn-forgetpass next-step" style="width: 150px;background: #287a5c;">{{ __('index.frontend.next') }}&nbsp;<i class="fa fa-arrow-circle-right"></i></button></li>
                        </ul>
                    </div>
                    
            </form>
        </div>
    </section>
   </div>
</div>

@endsection


@section('begin_javascript')

<script>
    jQuery('span.round-tab').click(false);
</script>

@endsection
