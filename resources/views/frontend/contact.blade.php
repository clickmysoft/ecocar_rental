@extends('frontend.layouts.app')

@section('title', app_name() . ' | Contact Us')

@section('begin_css')
    <style>
        div#abcdef {
            padding-top: 130px;
        }
    </style>
@endsection

@section('content')

<div class="section-bc">
        <div class="container">
            <div class="row">
        <div class="col-md-6">
        <div class="bc-left">
                <h2 class="h2-title-bc">
           {{ __('index.frontend.contact') }}
            </h2>
        </div>
        </div>
            <div class="col-md-6">
            <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
 
  </ol>
</nav>
        </div>
        </div>
        </div>

        </div>

        <div class="container">
            <div class="gap"></div>  
            <div class="row" style="padding: 50px; background: #0d7d322e;box-shadow: 1px 1px 5px #a09797;">       
            <div class="text-center"><h1 style="color: #000;font-size: 3rem;">CALL CENTER : 062-348-9900 {{ __('index.frontend.open_24') }}</h1></div>
                <div class="gap"></div>
                    <!-- <div class="col-md-12">
                        <p style="color: #000;">Inceptos hac sagittis sit elit primis iaculis arcu quam justo per primis tempus ad iaculis cursus condimentum nullam pretium dui 
                        id sit lacus duis dignissim primis potenti aliquam malesuada ullamcorper</p>
                         <p style="color: #000;">Euismod volutpat risus luctus id varius volutpat adipiscing porttitor egestas nisl nunc luctus phasellus nibh tristique lacinia penatibus justo urna</p>
                    </div> -->
                <div class="gap"></div>
                <div class="col-md-7">            
                    <form class="mt30">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="font-size: 18px; font-weight: 300;
                                        color: #000;">FirstName</label>
                                    <input class="form-control" type="text" style="border: 1px solid #716e6e;" />
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="font-size: 18px; font-weight: 300;
                                        color: #000;">LastName</label>
                                    <input class="form-control" type="text" style="border: 1px solid #716e6e;"/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="font-size: 18px; font-weight: 300;
                                        color: #000; ">Phone</label>
                                    <input class="form-control" type="text" style="border: 1px solid #716e6e;"/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="font-size: 18px; font-weight: 300;
                                        color: #000; ">E-mail</label>
                                    <input class="form-control" type="text" style="border: 1px solid #716e6e;"/>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label style="font-size: 18px; font-weight: 300;
                                color: #000;">Message</label>
                            <textarea class="form-control" style="border: 1px solid #716e6e;"></textarea>
                        </div>
                        <input class="btn btn-primary btn-contact" type="submit" value="Send Message" style="padding: 16px; " />
                    </form>

                </div>

                <div class="col-md-4">
                    <div class="row">
                    <aside class="sidebar-right">
                        
                        <ul class="address-list list">
                            <li>
                              <div class="contact-right">
                                        <div class="col-md-2"> <i class="fa fa-envelope fa-2x" style="color: #004b1d;"></i></div>
                                        <div class="col-md-10"> <h5 style="color: #000;font-weight: 500;">Email</h5> <a href="#" style="color: #000;">info@ecocar.co.th</a> </div> 
                                </div>

                            </li>  
                            <div class="gap"></div>
                            <li>
                                <div class="contact-right">
                                    <div class="col-md-2"><i class="fa fa-phone fa-2x" style="color: #004b1d;"></i></div>
                                    <div class="col-md-10"><h5 style="color: #000;font-weight: 500;">Phone Number</h5><a href="#" style="color: #000;">02-0305880</a> , 
                                    <a href="#" style="color: #000;">062-348-9900</a></div>
                                 </div>
                            </li>
                             <div class="gap"></div>
                            <li>
                                <div class="contact-right">
                                    <div class="col-md-2"><i class="fa fa-fax fa-2x" style="color: #004b1d;"></i></div>
                                    <div class="col-md-10"><h5 style="color: #000;font-weight: 500;">Fax</h5><a href="#" style="color: #000;">02-0024607</a></div>
                                </div>
                            </li>
                             <div class="gap"></div>
                            <li>
                                <div class="contact-right"> 
                                     <div class="col-md-2"> <i class="fa fa-map-marker fa-2x" style="color: #004b1d;"></i> </div> 
                                     <div  class="col-md-10"> <h5 style="color: #000;font-weight: 500;">Address</h5> 
                                    <address style="color: #000;">{{ __('index.frontend.address1') }}{{ __('index.frontend.address2') }}
                                        {{ __('index.frontend.address3') }}
                                    </address> 
                                    </div> 
                                </div> 
                            </li>
                        </ul>
                        
                    </aside>
                    </div>

                </div>
            </div>
            <div class="row" style="padding: 50px; background: #fff;">
                    <div id="map" style="width: 100%; height: 500px;"></div>
            </div>
            <div class="gap"></div>
        </div>

    <div class="row justify-content-center" style="display:none;">
        <div class="col col-sm-8 align-self-center">
            <div class="card">
                <div class="card-header">
                    <strong>
                        {{ __('labels.frontend.contact.box_title') }}
                    </strong>
                </div><!--card-header-->

                <div class="card-body">
                    {{ html()->form('POST', route('frontend.contact.send'))->open() }}
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.frontend.name'))->for('name') }}

                                    {{ html()->text('name')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.name'))
                                        ->attribute('maxlength', 191)
                                        ->required()
                                        ->autofocus() }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}

                                    {{ html()->email('email')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.email'))
                                        ->attribute('maxlength', 191)
                                        ->required() }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.frontend.phone'))->for('phone') }}

                                    {{ html()->text('phone')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.phone'))
                                        ->attribute('maxlength', 191)
                                        ->required() }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.frontend.message'))->for('message') }}

                                    {{ html()->textarea('message')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.message'))
                                        ->attribute('rows', 3) }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group mb-0 clearfix">
                                    {{ form_submit(__('labels.frontend.contact.button')) }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                    {{ html()->form()->close() }}
                </div><!--card-body-->
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
@endsection

@section('begin_javascript')
<script type="text/javascript">
                var map;
                    function initMap() {
                        map = new google.maps.Map(document.getElementById('map'), {
                          zoom: 19,
                          center: new google.maps.LatLng(13.777407, 100.624819), 
                          mapTypeId: 'roadmap'
                        });
                        var iconBase = 'https://www.picz.in.th/images/2018/02/06/';  
                        var icons = {
                          parking: {
                            icon: iconBase + 'sedan-icon-google-map.png'
                          },
                          library: {
                            icon: iconBase + 'sedan-icon-google-map.png'
                          },
                          info: {
                            icon: iconBase + 'sedan-icon-google-map.png' 
                          }
                        };
                        var features = [
                          {
                            position: new google.maps.LatLng(13.9538709, 100.6136862), 
                            type: 'info',
                            content: '<h5>สำนักงานใหญ่ รถเช่าดอนเมือง</h5>ตั้งอยู่เลขที่ 279/57 ถนนวิภาวดีรังสิต แขวงสนามบิน เขตดอนเมือง กรุงเทพมหานคร 10210<br>เบอร์โทร 02-002-4606<br>มือถือ 092-2848-660'
                          },
                          {
                            position: new google.maps.LatLng(13.72016,100.680915), 
                            type: 'info',
                            content: '<h5>สำนักงานรถเช่าสาขา อ่อนนุช (รถเช่าอ่อนนุช)</h5>ตั้งอยู่เลขที่ 268/13 ถนนอ่อนนุช แขวงประเวศ เขตประเวศ  กรุงเทพมหานคร 10250<br>เบอร์โทร 02-720-0280<br>มือถือ 094-512-1689'
                          },
                           {
                            position: new google.maps.LatLng(13.718865,100.454871),  
                            type: 'info',
                            content: '<h5>สำนักงานรถเช่าสาขาบางหว้า (รถเช่าบางหว้า)</h5>ตั้งอยู่เลขที่ 9 ซอยเพชรเกษม 25/5 แขวงบางหว้า เขตภาษีเจริญ  กรุงเทพมหานคร 10160<br>เบอร์โทร 02-045-5580<br>มือถือ 098-828-9808'
                          },
                          {
                            position: new google.maps.LatLng(13.777351,100.624765,10),  
                            type: 'info',
                            content: '<h5>สำนักงานรถเช่าสาขาลาดพร้าว (รถเช่าลาดพร้าว)</h5>ตั้งอยู่เลขที่ 2438 ถนนลาดพร้าว แขวงพลับพลา เขตวังทองหลาง  กรุงเทพมหานคร 10310<br>เบอร์โทร 02-030-5880<br>มือถือ 062-3489900'
                          },
                          {
                            position: new google.maps.LatLng(13.861161,100.509933,13),  
                            type: 'info',
                            content: '<h5>สำนักงานรถเช่าสาขานนทบุรี (รถเช่านนทบุรี)</h5>ตั้งอยู่เลขที่ 160/1 ถนนรัตนาธิเบศร์ ซอย 18 ต.บางกระสอ อ.เมืองนนทบุรี จ.นนทบุรี 11000<br>เบอร์โทร 02-035-5880<br>มือถือ 062-3489900'
                          },
                          {
                            position: new google.maps.LatLng(13.6537822,100.5993279,13),  
                            type: 'info',
                            content: '<h5>สำนักงานรถเช่าสาขาสมุทรปราการ (รถเช่าสำโรง)</h5>ตั้งอยู่เลขที่ 1199 ถนนสุขุมวิท ตำบลสำโรงเหนือ อ.เมืองสมุทรปราการ จ.สมุทรปราการ 10270<br>เบอร์โทร 02-049-1180<br>มือถือ 098-828-9808'
                          },
                          {
                            position: new google.maps.LatLng(15.243653,104.86623,11),
                            type:'info',
                            content: '<h5>สำนักงานสาขาอุบลราชธานี(รถเช่าอุบล)</h5>ตั้งอยู่เลขที่ 456/9 ถนนเทพโยธี ตำบลในเมือง อำเภอเมือง จังหวัดอุบลราชธานี 34000<br>เบอร์โทร 045-956-754<br>มือถือ 061-6485-438'
                          },
                          {
                            position: new google.maps.LatLng(18.770019,98.96801,16),
                            type: 'info',
                            content:'<h5>สำนักงานสาขาเชียงใหม่(รถเช่าเชียงใหม่)</h5>ตั้งอยู่เลขที่ 92 ถนนสมโภชเชียงใหม่ 700 ปี ตำบลฟ้าฮ่าม อำเภอเมืองเชียงใหม่ จังหวัดเชียงใหม่ 50000<br>เบอร์โทร 06-3187-5726<br>มือถือ 06-3187-5726'
                          },
                          {
                            position: new google.maps.LatLng(12.923093,100.883794,13),
                            type: 'info',
                            content: '<h5>สำนักงานสาขาพัทยา (รถเช่าพัทยา)</h5>ตั้งอยู่เลขที่  388/9 ถนนสุขุมวิท ตำบลหนองปรือ อำเภอบางละมุง จังหวัดชลบุรี 20150<br>เบอร์โทร 06-3187-5726<br>มือถือ 06-3187-5726'
                          }
                        ];

                         var infowindow = new google.maps.InfoWindow();
                         var marker, i;
                            // Create markers.
                            features.forEach(function(feature) {
                                marker = new google.maps.Marker({
                                    position: feature.position,
                                    icon: icons[feature.type].icon,
                                    map: map
                                });

                                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                    return function() {
                                      infowindow.setContent(feature.content);
                                      infowindow.open(map, marker);
                                    }
                                })(marker, i));

                        });
                    }
        </script>
        <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyC1haSCyOX7dZLLdOsCjJEe_ZPBMyVxf90&callback=initMap"></script>

@endsection

