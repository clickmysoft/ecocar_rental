@extends('frontend.layouts.app')

@section('begin_css')
 <link rel="stylesheet" type="text/css" href="{{ asset("frontend/css/styleecocar.css")}}">
@endsection

@section('content')
<style type="text/css">

  ul.pagination{
    text-align: center;
  }
</style>
<div id="carousel-example-generic" class="carousel slide hidden-xs hidden-sm" data-ride="carousel" style="padding-top:130px;">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="{{ asset("frontend/img/backgrounds/ecocar-promotions-(Desktop).jpg")}}" alt="...">
      <div class="carousel-caption">
        ...
      </div>
    </div>
    <div class="item">
      <img src="{{ asset("frontend/img/backgrounds/ecocar-promotions-(Desktop).jpg")}}" alt="...">
      <div class="carousel-caption">
        ...
      </div>
    </div>

  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>



<div id="carousel-example-generic" class="carousel slide hidden-md hidden-lg" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="{{ asset("frontend/img/backgrounds/slider-promotion-desktop-lg.jpg")}}" alt="...">
      <div class="carousel-caption">
        ...
      </div>
    </div>
    <div class="item">
      <img src="{{ asset("frontend/img/backgrounds/slider-promotion-desktop-lg.jpg")}}" alt="...">
      <div class="carousel-caption">
        ...
      </div>
    </div>

  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<div class="wrapper-promotion" style="background:#fff; background-position: center left;">
  <div class="container">
    <div class="row row-wrap">
      <div class=" text-center">
        <div class="gap gap-small"></div>
        <h3> รายการโปรโมชั่น </h3>
        <p class="line-bottom"></p><br/>     


        <?php
          $i=0;
          foreach ($data['promotion'] as $key => $value) {
            $i++;
         ?>   
       
      
        </pre>
              <div class="col-md-4">
                <div class="thumb promotion-box-card">
                  <header class="thumb-header">
                    <a class="hover-img curved" href="promotion-inner.html">
                      <img src="<?=asset("promotions/".$value['image_thumbnail'])?>" alt="<?=$value['title']?>" title="<?=$value['title']?>">
                    </a>
                  </header>

                  <div class="thumb-caption text-center" id="description-promotion">
                    <h5 class="thumb-title" style="margin-bottom: 10px">ระหว่างวันที่ <?= strip_tags($value->start_date) ?> - <?= strip_tags($value->end_date) ?></h5>
                    <p style="font-size: 18.2px;font-weight: 500;color: #000;">{{ $value->title }}</p>
                    <p style="padding: 0 20px; font-weight: 300;">{{ str_limit(strip_tags($value->description),100) }}</p>
                    <p style="font-size: 18.2px;font-weight: 500;color: #ffffff;background: #01ad40;">รับส่วนลด {{ number_format($value->promotion_discount,0) }}
                     <?php

                     if($value->discount_type == 'percen'){
                        echo "เปอร์เซ็น";
                     }else{
                        echo "บาท";
                     }
                     

                     ?>
                       
                     </p>
                    <a href="<?php echo route('frontend.promotion.show',$value['id']) ?>" class="btn btn-default btn-more" role="button" style="margin-bottom: 10px;">ดูรายละเอียดเพิ่มเติม</a> 
                  </div>
                </div>
              </div>

      <?php
            if($i%3==0){
                echo '<div class="gap"></div>';
            }
        }
      ?>
        <div class="gap"></div>
          {{ $data['promotion']->links() }}

      </div>
    </div>
  </div>
  <div class="gap"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="sec-promo-contact"> 
        <div class="container">
          <div class="col-md-12">
            <h4 class="text-center">การติดต่อ ขอรับโปรโมชั่นรถเช่า กับพนักงานขายของเรา</h4>
            <p class="line-bottom"></p>
            <div class="gap gap-small"></div>
            <div class="col-md-3 text-center"><i class="fa fa-phone fa-2x" id="contact-promotion"></i>
              <br>
              คุณเอชิน สาขาลาดพร้าว โทร 062-3489900</div>
              <div class="col-md-3 text-center"><i class="fa fa-phone fa-2x" id="contact-promotion"></i><br>คุณโบว์ สาขาบางหว้า โทร 098-8289808</div>
              <div class="col-md-3 text-center"><i class="fa fa-phone fa-2x" id="contact-promotion"></i><br>คุณบอย สาขานนทบุรี โทร 062-3482370</div>
              <div class="col-md-3 text-center"><i class="fa fa-phone fa-2x" id="contact-promotion"></i><br>call center 02-0305880</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>    
</div>    
</div>
</div>

@endsection


@section('begin_javascript')

@endsection
