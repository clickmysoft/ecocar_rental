@extends('frontend.layouts.app')

@section('begin_css')

    <style type="text/css">
        .promotion-code-cover{
                width: 100%;
                display: block;
                background: #49a084;
                padding: 20px 0 40px 0;
                vertical-align: middle;
                height: 140px;
                max-width: 570px;
                margin: 0 auto;
                border-radius: 10px;
        }
        .content-promotioncode{
            color: #fff;
        }
    </style>


@endsection

@section('content')



<div class="top-area show-onload">
            <div class="bg-holder full">
                <div class="bg-front bg-front-mob-rel">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="owl-carousel owl-slider owl-carousel-area" id="owl-carousel-slider">
                    <div class="bg-holder full">
                        <div class="bg-mask"></div>
                        <div class="bg-img" style="background-image: url(../frontend/img/backgrounds/slide-promotion1.jpg);"></div>
                        <div class="bg-content">
                            <div class="container">
                                <div class="row">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bg-holder full">
                        <div class="bg-mask"></div>
                        <div class="bg-img" style="background-image: url(../frontend/img/backgrounds/slide-promotion1.jpg);"> </div>
                        <div class="bg-content">
                            <div class="container">
                                <div class="row">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bg-holder full">
                        <div class="bg-mask"></div>
                        <div class="bg-img" style="background-image: url(../frontend/img/backgrounds/slide-promotion1.jpg);"> </div>
                        <div class="bg-content">
                            <div class="container">
                                <div class="row">

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- END TOP AREA  -->

        <div class="wrapper-promotion" >

            <div class="container">
                <div class="row row-wrap">
                    <div class=" text-center">
                        <div class="gap gap-small"></div>
                        <div class="col-md-12">
                            <h3>รายละเอียดโปรโมชั่น</h3>
                            <p class="line-bottom"></p>
                            <div class="gap gap-small"></div>
                            <h4>{{ $data->title }}</h4>
                            <p> <?php echo $data->description ?>
                            </p>
                        </div>
                        <div class="gap"></div>
                            <div class="col-md-12">
                                    <div class="promotion-code-cover">
                                            <div class="content-promotioncode">
                                                <h3 style="color: #fff;">โปรโมชั่นโค้ด</h3>
                                                <p style="text-transform: uppercase; background: #ec6c6c;border-radius: 50px;width: 300px !important;display: block;margin: 0 auto;margin-bottom: 10px;"> {{ $data->promotion_code }} </p>
                                               
                                                <p>
                                                จะได้รับส่วนลด 
                                                <span style="color: white;font-weight: 500;font-size: 20px;">{{ $data->promotion_discount }}</span>

                                                <?php

                                                if ($data->discount_type == "bath"){
                                                    echo " บาท";
                                                }else{
                                                    echo " เปอร์เซ็น";
                                                }

                                                ?>
                                                </p>
                                            </div>
                                    </div>
                                <!-- <table style="width: 100%;height: 300px;"> 
                                     <tbody>
                                        <tr class="table-pro-inner">
                                            <td>Nissan Almera 1.2 AT</td> {{ $data->promotion_code }}
                                            <td>ราคาเริ่มต้น 963 บาท</td>
                                    </tr>
                                        <tr class="table-pro-inner">
                                          <td>Toyota Vios 1.5 AT</td>
                                          <td>ราคาเริ่มต้น 1,177 บาท</td>
                                        </tr>
                      <tr class="table-pro-inner">
                                          <td>Nissan Sylphy 1.6 AT</td>
                                          <td>ราคาเริ่มต้น 1,498 บาท</td>
                                        </tr>
                                        <tr class="table-pro-inner">
                                          <td>Nissan Teana 2.0 AT</td>
                                          <td>ราคาเริ่มต้น 2,461 บาท</td>
                                        </tr>
                                        <tr class="table-pro-inner">
                                          <td>Isuzu Mu x 1.9 AT</td>
                                          <td>ราคาเริ่มต้น 2,461 บาท</td>
                                        </tr>
                                </tbody>
                            </table> -->
                        </div>
                        

                        
                        <div class="col-md-12">
                            <div class="gap"></div>
                            <div class="col-md-3"></div>
                            <div class="col-md-6" id="sec-condition">
                                <h3>เงื่อนไขที่สำคัญ-โปรโมชั่นรถเช่า</h3>
                                <p class="line-bottom"></p>
                                <ul class="list-branch-ecocar text-left" style="line-height: 2.5em; padding-left: 0;">
                                    <li style="font-size: 16px; color: #000;">1. ราคานี้สำหรับการเช่ารถยนต์ ตั้งแต่ 2 วัน ขึ้นไป สำหรับการเช่า 1 วัน ส่วนลดจะน้อยกว่านี้</li>
                                    <li style="font-size: 16px; color: #000;">2. วันศุกร์-เสาร์-อาทิตย์ ราคาจะสูงกว่านี้ และไม่สามารถใช้โปรโมชั่นนี้ได้</li>
                                    <li style="font-size: 16px; color: #000;">3. ไม่สามารถใช้ร่วมกับโปรโมชั่นอื่นได้</li>
                                    <li style="font-size: 16px; color: #000;">4. ราคานี้ใช้ได้กับสาขาของเราทั้ง 9 สาขา</li>
                                    <li style="font-size: 16px; color: #000;">5. มีจำนวนรถยนต์จำกัด หากรถยนต์หมด จะไม่สามารถใช้สิทธิโปรโมชั่นนี้ได้</li>
                                </ul>
                            </div>
                            <div class="col-md-3"></div>
                        </div>

                      

                    </div>
                </div>
            </div>
            <div class="gap"></div>

            <div class="container-fluid">
                <div class="row">
                    <div class="sec-promo-contact">
                        <div class="container">
                            <div class="col-md-12">
                                <h4 class="text-center">การติดต่อ ขอรับโปรโมชั่นรถเช่า กับพนักงานขายของเรา</h4>
                                <p class="line-bottom"></p>
                                <div class="gap gap-small"></div>
                                <div class="col-md-3 text-center"><i class="fa fa-phone fa-2x" id="contact-promotion"></i>
                                    <br> คุณเอชิน สาขาลาดพร้าว โทร 062-3489900</div>
                                <div class="col-md-3 text-center"><i class="fa fa-phone fa-2x" id="contact-promotion"></i><br>คุณโบว์ สาขาบางหว้า โทร 098-8289808</div>
                                <div class="col-md-3 text-center"><i class="fa fa-phone fa-2x" id="contact-promotion"></i><br>คุณบอย สาขานนทบุรี โทร 062-3482370</div>
                                <div class="col-md-3 text-center"><i class="fa fa-phone fa-2x" id="contact-promotion"></i><br>call center 02-0305880</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




@endsection


@section('begin_javascript')

@endsection
