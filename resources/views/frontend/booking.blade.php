@extends('frontend.layouts.app')

@section('begin_css')
<link rel="stylesheet" href="{{ asset("frontend/css/font-awesome.css")}}">
<link rel="stylesheet" type="text/css" href="{{ asset("frontend/css/form-step.css")}}">
<link rel="stylesheet" href="{{ asset("frontend/css/styleecocar.css")}}">
<link rel="stylesheet" href="{{ asset("frontend/css/icomoon.css")}}">
 <style>
    .nav-tabs {
        border-bottom: none; 
    }
    .form-group.form-group-icon-left .form-control {
    padding-left: 28px;
    }
    
    ul.pagination.results{
            padding-left: 20px;
    }
    
    @media (max-width: 576px){
        .box-carrental-summery{
            width: 100%;
            padding: 0;
        }
        .price-carrental{
            border-bottom: 4px solid;
            margin-bottom: 10px;
        }
    }
    
    @media (max-width: 376px){
    .pl30{
        padding-left: 0 !Important;
    }
    
 }
    @media (max-width: 612px){
    .title-extra{
        width: 100%;
        display: block;
    }
    .baby-seat{
        display: block;
        margin: 0 auto;
        margin-top: 10px;
    }
    .center-mobile{
        text-align: center;
    }
    td.td-extra-icon{
        width: 100%;
        display: block;
        text-align: center;
    }
    td.td-extra-price{
         width: 100%;
         display: block;
        border-bottom: 4px solid #000;
    }
     
    input.checkbox-formstep{
        float: none !important;
    }
    .pl30{
        padding-left: 0 !Important;
    }
    td.td-extra-icon{
        margin-top: 10px;
    }
    .title-extra{
        padding-bottom: 0;
    }
    input.checkbox-formstep{
        transform: scale(2);
        height: 10px;
        margin-right: 10px;
        margin-top: -10px;
    }
   
}

@media (max-width: 991px){
.img-paypal{
    padding-left: 0;
    }
    .btn-payletter,.btn-now{
        width: 100%;
    }
    
}


.form-group .input-icon{
        top: 27px;
}

    </style>
@endsection

@section('content')

<div class="container">
            
    <div class="row">
      <section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active">
                        <span class="title-formstep">{{ __('index.frontend.vehicles') }}</span>
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            <span class="round-tab">
                               <img class="icon-formstep" src="{{ asset("frontend/img/car-formstep-02.png")}}" alt="">
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                       <span class="title-formstep">{{ __('index.frontend.extra') }}</span>
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                            <span class="round-tab">
                                <img class="icon-formstep" src="{{ asset("frontend/img/car-formstep-02.png")}}" alt="">
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <span class="title-formstep">{{ __('index.frontend.customer_detail') }}</span>
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                            <span class="round-tab">
                                <img class="icon-formstep" src="{{ asset("frontend/img/car-formstep-02.png")}}" alt="">
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <span class="title-formstep">{{ __('index.frontend.payment') }}</span>
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab">
                                <img class="icon-formstep" src="{{ asset("frontend/img/car-formstep-02.png")}}" alt="">
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

            <form role="form">
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">

                        <div class="step1 formstep-border">


                            <div class="row">
                                <div class="col-md-12 pl10-pr10">
                                          <div class="booking-item-dates-change mb10">
                                          
                <form class="input-daterange" data-date-format="MM d, D">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group form-group-icon-left"><i class="fa fa-map-marker input-icon input-icon-hightlight"></i>
                                <label class="search-results">Pick Up Location</label>
                                <span class="twitter-typeahead" style="position: relative; display: block; direction: ltr;">
                                    <input class="typeahead form-control tt-input" value="ดอนเมือง" type="text" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent; " readonly>
                                    <pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: &quot;Open Sans&quot;, Tahoma, Arial, helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre>
                                    <span class="tt-dropdown-menu" style="position: absolute; top: 100%; left: 0px; z-index: 100; display: none; right: auto;"
                                ><div class="tt-dataset-2"></div></span></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                                        <label class="search-results">Check in</label>
                                        <input class="form-control" name="start" type="text" value="29 January" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-icon-left"><i class="fa fa-clock-o input-icon input-icon-hightlight"></i>
                                        <label class="search-results">Time</label>
                                        <input class=" form-control" type="text" value="12:45" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group form-group-icon-left"><i class="fa fa-map-marker input-icon input-icon-hightlight"></i>
                                <label class="search-results">Drop Off Location</label>
                                <span class="twitter-typeahead" style="position: relative; display: block; direction: ltr;">
                                    <input class="typeahead form-control tt-input" value="ดอนเมือง" type="text" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;" readonly>
                                    <pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: &quot;Open Sans&quot;, Tahoma, Arial, helvetica, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre>
                                    <span class="tt-dropdown-menu" style="position: absolute; top: 100%; left: 0px; z-index: 100; display: none; right: auto;"><div class="tt-dataset-3"></div></span></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                                        <label class="search-results">Check out</label>
                                        <input class="form-control" name="end" type="text" value="31 January" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-icon-left"><i class="fa fa-clock-o input-icon input-icon-hightlight"></i>
                                        <label class="search-results">Time</label>
                                        <input class=" form-control" value="17:30" type="text" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
                                </div>
                                    <div class="col-md-3 col-lg-3 hidden-sm hidden-xs">
                                                <aside class="booking-filters text-white">
                                                    <h3>{{ __('index.frontend.filter') }}</h3>
                                                    <ul class="list booking-filters-list">
                                                        <li>
                                                            <h5 class="booking-filters-title">{{ __('index.frontend.passengers') }}</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />{{ __('index.frontend.passengers2') }} (11)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />{{ __('index.frontend.passengers3') }} (2)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />{{ __('index.frontend.passengers4') }} (17)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />{{ __('index.frontend.passengers5') }} (60)</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">{{ __('index.frontend.price') }}</h5>
                                                            <input type="text" id="price-slider">
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">{{ __('index.frontend.car_group') }} </h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />{{ __('index.frontend.economy') }} (20)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />{{ __('index.frontend.compact') }} (20)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />{{ __('index.frontend.mid_size') }} (11)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Stabdart (12)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />{{ __('index.frontend.full_size') }} (5)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Premium/Luxury (3)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Minivan (5)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Crossover (10)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />{{ __('index.frontend.suv') }} (12)</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">{{ __('index.frontend.transmission') }}</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />{{ __('index.frontend.auto_gear') }} (80)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />{{ __('index.frontend.manual') }} (25)</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">Engine</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Gas (60)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Diesel (35)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Hybrid (22)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Electric (15)</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">Equipment</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Climate Control (47)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Air Conditioning (66)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Satellite Navigation (30)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Power Door Locks (35)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />FM Radio (70)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Stereo CD/MP3 (53)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Tilt Steering Wheel (42)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Power Windows (68)</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">Pickup Options</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Terminal Pickup (80)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Shuttle Bus to Car (25)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Meet and Greet (13)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Car with Driver (7)</label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </aside>
                                            </div>
                                    <div class="col-md-9">
                                                <div class="nav-drop booking-sort">
                                                    <h5 class="booking-sort-title"><a href="#">Sort: Price (low to high)<i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></a></h5>
                                                    <ul class="nav-drop-menu">
                                                        <li><a href="#">Price (high to low)</a>
                                                        </li>
                                                        <li><a href="#">Car Name (A-Z)</a>
                                                        </li>
                                                        <li><a href="#">Car Name (Z-A)</a>
                                                        </li>
                                                        <li><a href="#">Car Type</a>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <ul class="booking-list">
                                                    <?php for ($i=0; $i < 5; $i++) { ?>

                                                    <li>
                                                        <a class="booking-item" href="#">

                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <div class="booking-item-car-img">
                                                                        <img src="{{asset("frontend/img/Land-Rover-Range-Rover-Evoque.png")}}" alt="Image Alternative text" title="Image Title" />
                                                                        <p class="booking-item-car-title">Range Rover Evoque</p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <ul class="booking-item-features booking-item-features-sign clearfix">
                                                                                <li rel="tooltip" data-placement="top" title="Passengers"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 6</span>
                                                                                </li>
                                                                                <li rel="tooltip" data-placement="top" title="Doors"><i class="im im-car-doors"></i><span class="booking-item-feature-sign">x 5</span>
                                                                                </li>
                                                                                <li rel="tooltip" data-placement="top" title="Baggage Quantity"><i class="fa fa-briefcase"></i><span class="booking-item-feature-sign">x 4</span>
                                                                                </li>
                                                                                <li rel="tooltip" data-placement="top" title="Automatic Transmission"><i class="im im-shift-auto"></i><span class="booking-item-feature-sign">auto</span>
                                                                                </li>
                                                                                <li rel="tooltip" data-placement="top" title="Electric Vehicle"><i class="im im-electric"></i>
                                                                                </li>
                                                                            </ul>
                                                                            <ul class="booking-item-features booking-item-features-small clearfix">
                                                                                <li rel="tooltip" data-placement="top" title="Satellite Navigation"><i class="im im-satellite"></i>
                                                                                </li>
                                                                                <li rel="tooltip" data-placement="top" title="FM Radio"><i class="im im-fm"></i>
                                                                                </li>
                                                                                <li rel="tooltip" data-placement="top" title="Tilt Steering Wheel"><i class="im im-car-wheel"></i>
                                                                                </li>
                                                                                <li rel="tooltip" data-placement="top" title="Power Windows"><i class="im im-car-window"></i>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4" style="text-align: center;">
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <div>1,500 THB</div>
                                                                        <span class="btn-payletter">PAY LATER</span>
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <div>1,500 THB</div>
                                                                        <span class="btn-now">PAY NOW</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <?php
                                                        }
                                                    ?>
                                                 </ul>
                                        </div>                                           
                             </div>
                        </div>
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn-forgetpass next-step">Save and continue</button></li>
                        </ul>
                        
                         <div class="col-xs-12 col-sm-12 hidden-md hidden-lg" style="padding-left: 0; padding-right: 0;">
                                                <aside class="booking-filters text-white" style="margin-bottom: 20px;">
                                                    <h3>Filter By:</h3>
                                                    <ul class="list booking-filters-list">
                                                        <li>
                                                            <h5 class="booking-filters-title">Passengers Quantity</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />2 Passengers (11)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />3 Passengers (2)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />4 Passengers (17)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />4+ Passengers (60)</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">Price </h5>
                                                            <input type="text" id="price-slider">
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">Car group</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Economy (20)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Compact (20)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Midsize (11)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Stabdart (12)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Fullsize (5)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Premium/Luxury (3)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Minivan (5)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Crossover (10)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />SUV (12)</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">Transmission</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Automatic (80)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Manual (25)</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">Engine</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Gas (60)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Diesel (35)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Hybrid (22)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Electric (15)</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">Equipment</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Climate Control (47)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Air Conditioning (66)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Satellite Navigation (30)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Power Door Locks (35)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />FM Radio (70)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Stereo CD/MP3 (53)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Tilt Steering Wheel (42)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Power Windows (68)</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <h5 class="booking-filters-title">Pickup Options</h5>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Terminal Pickup (80)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Shuttle Bus to Car (25)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Meet and Greet (13)</label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="i-check" type="checkbox" />Car with Driver (7)</label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </aside>
                                            </div>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step2">
                        <div class="step2">
                            <div class="step_21">
                                <div class="row">
                                    <div class="container">
                                        <div class="col-md-12">
                                            <p class="bg-formstep pd10">EXTRA OPTIONS</p>
                                                <table class="tb-w-100p">
                                                    <tr class="tr-extra-options">
                                                        <td class="td-w-50p td-extra-icon">
                                                            <label class="checkbox pl30 label-cover">
                                                                <input type="checkbox" name="inlineCheckboxOptions" value="option2" class="checkbox-formstep">
                                                                <img src="{{asset("frontend/img/gps-icon.png")}}" alt="" class="icon-formstep2">
                                                                <span class="pd10 title-extra">GPS</span>
                                                            </label>
                                                        </td>
                                                        <td class="td-w-50p td-extra-price">
                                                            <label class="mb20 center-mobile">200 THB / DAY</label>
                                                        </td>
                                                    </tr>

                                                     <tr class="tr-extra-options">
                                                        <td class="td-w-50p td-extra-icon ">
                                                            <label class="checkbox pl30 label-cover">
                                                                <input type="checkbox" name="inlineCheckboxOptions" value="option2" class="checkbox-formstep">
                                                                <img src="{{asset("frontend/img/baby-icons-fs.png")}}" alt="" class="icon-formstep2">
                                                                <select class="selectpicker baby-seat">
                                                                    <option>Baby Seat 1</option>
                                                                    <option class="special"> Baby Seat 2</option>
                                                                    <option>Baby Seat 3</option>
                                                                </select>
                                                            </label>
                                                        </td>
                                                        <td class="td-w-50p td-extra-price">
                                                            <label class="mb20 center-mobile">100 THB / DAY</label>
                                                        </td>
                                                    </tr>

                                                     <tr class="tr-extra-options">
                                                        <td class="td-w-50p td-extra-icon">
                                                            <label class="checkbox pl30 label-cover">
                                                                <input type="checkbox" name="inlineCheckboxOptions" value="option2" class="checkbox-formstep">
                                                                <img src="{{asset("frontend/img/icon-safety.png")}}" alt="" class="icon-formstep2">
                                                                <span class="pd10 title-extra">SLDW (Super Loss Damage Waiver)</span>
                                                            </label>
                                                        </td>
                                                        <td class="td-w-50p td-extra-price">
                                                            <label class="mb20 center-mobile">200 THB / DAY</label>
                                                        </td>
                                                    </tr>
                                                    
                                                         <tr class="tr-extra-options">
                                                        <td class="td-w-50p td-extra-icon">
                                                            <label class="checkbox pl30 label-cover">
                                                                <input type="checkbox" name="inlineCheckboxOptions" value="option2" class="checkbox-formstep">
                                                                <img src="{{asset("frontend/img/rent-a-car-white.png")}}" alt="" class="icon-formstep2">
                                                                <span class="pd10 title-extra">Delivery Service Car</span>
                                                            </label>
                                                        </td>
                                                        <td class="td-w-50p td-extra-price">
                                                            <label class="mb20 center-mobile">535 THB</label>
                                                        </td>
                                                    </tr>
                                                    
                                                      <tr class="tr-extra-options">
                                                        <td class="td-w-50p td-extra-icon">
                                                            <label class="checkbox pl30 label-cover">
                                                                <input type="checkbox" name="inlineCheckboxOptions" value="option2" class="checkbox-formstep">
                                                                <img src="{{asset("frontend/img/transit-car-return-white.png")}}" alt="" class="icon-formstep2">
                                                                <span class="pd10 title-extra">Service Return A Car</span>
                                                            </label>
                                                        </td>
                                                        <td class="td-w-50p td-extra-price">
                                                            <label class="mb20 center-mobile">535 THB</label>
                                                        </td>
                                                    </tr>
                                                    
                                                </table>

                                            </div>

                                        <div class="col-md-12">
                                            <p class="bg-formstep pd10">CAR RENTAL SUMMARY</p>

                                                <div class="box-carrental-summery">
                                          <div class="row">
                                              <div class="col-sm-9"><div class="">Car price (Vat excluded)</div></div>
                                                <div class="col-sm-3"><div class="text-right price-carrental">1,200.00 THB</div></div>
                                                <div class="col-sm-9"><div class="">Option Price</div></div>
                                                <div class="col-sm-3"><div class="text-right price-carrental" id="option_text">0.00 THB</div></div>
                                                <div class="col-sm-9"><div class="">VAT 7%</div></div>
                                                <div class="col-sm-3"><div class="text-right price-carrental" id="vat_text">84.00 THB</div></div>
                                                <div class="col-sm-9"><div class=""><strong>Total price</strong></div></div>
                                                <div class="col-sm-3"><div class="text-right price-carrental" id="total_text"><strong>1,284.00 THB</strong></div></div>
                                            </div><!--end row-->
                                        </div>

                                        </div>

                                         <div class="col-md-12">
                                            <p class="bg-formstep pd10">REMARK</p>
                                             <div class="container">
                                             <p>For contact us or Question more / at info@ecocar.in.th or Call center 24 hours : 090-638-4888</p>
                                             </div>
                                         </div>


                                    </div>
                                </div>
                            </div>
                            <div class="step-22">

                            </div>
                        </div>
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                            <li><button type="button" class="btn-forgetpass next-step">Save and continue</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step3">
                        <div class="step_21">
                              <div class="step44">
                                 <div class="container">
            <div class="col-md-12">
               <p class="bg-formstep pd10 bar-cus"
               >CUSTOMER</p>
            </div>
            <div class="row" style="padding-top: 0px">
                
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="col-md-12">
                                  <p class="checkout pd10">YOUR INFORMATION</p>
                             </div>
                             
                             
                             <div class="row mar_ned">
                                <div class="col-md-4 col-xs-4">
                                    <p align="right"><stong>First Name </stong></p>
                                </div>
                                <div class="col-md-6 col-xs-8">
                                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="First Name">
                                </div>
                            </div>
                            <div class="row mar_ned">
                                <div class="col-md-4 col-xs-4">
                                    <p align="right"><stong>Last Name </stong></p>
                                </div>
                                <div class="col-md-6 col-xs-8">
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Last Name">
                                </div>
                            </div>
                            <div class="row mar_ned">
                                <div class="col-md-4 col-xs-4">
                                    <p align="right"><stong>Email </stong></p>
                                </div>
                                <div class="col-md-6 col-xs-8">
                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                                </div>
                            </div>
                            <div class="row mar_ned">
                                <div class="col-md-4 col-xs-4">
                                    <p align="right"><stong>Phone </stong></p>
                                </div>
                                <div class="col-md-6 col-xs-8">
                                    <input type="tel" class="form-control" id="exampleInputtel" placeholder="Phone">
                                </div>
                            </div>
                            <div class="col-md-12">
                                  <p class="checkout pd10">TRAVEL INFORMATION</p>
                             </div>
                              <div class="row mar_ned">
                                <div class="col-md-4 col-xs-4">
                                    <p align="right"><stong>Flight InfoAirline</stong></p>
                                </div>
                                <div class="col-md-6 col-xs-8">
                                    <input type="text" class="form-control" id="exampleInputtext" placeholder="Flight InfoAirline">
                                </div>
                            </div>
                                        <div class="row mar_ned">
                                <div class="col-md-4 col-xs-4">
                                    <p align="right"><stong>Flight Number</stong></p>
                                </div>
                                <div class="col-md-6 col-xs-8">
                                    <input type="tel" class="form-control" id="exampleInputtel" placeholder="Flight Number">
                                </div>
                            </div>
                        
                            
                        </div>
                    </div>
                </div>

               

            </div>

            <div class="gap"></div>

        </div>
                            </div>
                        </div>
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                           <!-- <li><button type="button" class="btn btn-default next-step">Skip</button></li> -->
                            <li><button type="button" class="btn-forgetpass next-step">Save and continue</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="complete">

                        <div class="step_21" style="padding: 0;">
                            <div class="step44">
        
        
                                    <div class="container">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="gap"></div>
                                                    <h5 style="font-weight: 500;">Your Information</h5>
                                                     <hr style="border-top: 4px solid #004b1d" />
                                                     <p> Name Mr.Ecocar Bkkecocar</p>
                                                     <p> Email info@thairentecocar.com</p>
                                                     <p> Phone 02-0024606 </p>
                                                     <hr style="border-top: 4px solid #004b1d;" />
                                                    <h5 style="font-weight: 500;"> Extra Options </h5>
                                                    <div class="row">
                                                    <div class="col-md-6">Gps 1,400 THB / Day </div> <div class="col-md-6">Baby Seat 700 THB / 2</div> 
                                                    <div class="col-md-6">SLDW 1,400 THB / Day </div> <div class="col-md-6">Delivery Service Car 535 THB</div>   
                                                    <div class="col-md-6">Service Return A Car 535 THB</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                <div class="gap"></div>
                                                    <h5 style="font-weight: 500;"> Total Results </h5>
                                                    <h5> Pick-up </h5>
                                                    <table>
                                                        <tr>
                                                            <td style="width: 40%;">Location</td>
                                                            <td style="width: 60%;">Don Mueang Intl. Airport (DMK)</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 40%;">Date</td>
                                                            <td style="width: 60%;">01 Oct 2017</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 40%;">Time</td>
                                                            <td style="width: 60%;">8:30 AM</td>
                                                        </tr>
                                                    </table>
                                                    <div class="gap gap-small" style="padding-top: 7px;"></div>
                                                    <hr style="border-top: 4px solid #004b1d" />
                                                    <h5>Return</h5>
                                                    <table>
                                                        <tr>
                                                            <td style="width: 40%;">Location</td>
                                                            <td style="width: 60%;">Don Mueang Intl. Airport (DMK)</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 40%;">Date</td>
                                                            <td style="width: 60%;">07 Oct 2017</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 40%;">Time</td>
                                                            <td style="width: 60%;">8:30 AM</td>
                                                        </tr>
                                                    </table>
                                                    <div class="gap gap-small"></div>
                                                    <hr style="border-top: 4px solid #004b1d" />
                                                        <h5>Rate</h5>
                                                      <table style="width: 100%;">
                                                        <tr>
                                                            <td style="width: 40%;">Car Price</td>
                                                            <td style="width: 60%;">10,500 THB</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 40%;">Options Price</td>
                                                            <td style="width: 60%;">4,570 THB</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 40%;">VAT 7%</td>
                                                            <td style="width: 60%;">1054.9 THB</td>
                                                        </tr>
                                                    </table>
                                                    <div class="gap gap-small"></div>
                                                    <hr style="border-top: 4px solid #004b1d" />
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td style="width: 40%;"><p style="font-size: 20px;color: #000;"><strong>Total</strong></p></td>
                                                                <td style="width: 60%;"><p style="font-size: 20px; color: #000;"><strong>16,124.9 THB</strong></p></td>
                                                            </tr>
                                                        </table>
                                                        <div class="gap gap-small"></div>
                                                        <a href="#" style="margin-top: 20px; " ><img alt=""  class="img-paypal" src="https://woocommerce.com/wp-content/uploads/2016/04/PayPal-Express@2x.png" 
                                                        width="" height="" ></a>
                                                </div>
                                            </div>
            <div class="gap"></div>
        </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </section>
   </div>
</div>

@endsection


@section('begin_javascript')

@endsection
