@extends('frontend.layouts.app')

@section('title', app_name() . ' | Reset Password')

@section('begin_css')
<style type="text/css">
    h3.title-registrer {
           color: #ffffff;
           text-transform: uppercase;
           text-align: center;
           margin-bottom: 30px;
           text-shadow: 2px 2px 1px #000;
           border-bottom: 1px solid #fff;
    }
    .card{
        padding: 20px 70px 30px 70px;
        margin: 30px auto;
        max-width: 500px;
        border-radius: 50px;
        background: linear-gradient( rgba(20, 164, 99, 0.61), rgba(0, 0, 0, 0.72) ),  url(../frontend/img/bg-car6.jpg);
        box-shadow: -5px 5px 56px #a2a2a2;
    }
    label{
        color: #fff;
    }
    button.btn.btn-success.btn-sm.pull-right{
            height: 40px;
            width: 100%;
            border-radius: 60px;
            font-size: 18px;
            margin-top: 15px;
    }
    input.form-control{
        border-radius: 50px;
        height: 40px;
    }
    #app{
        background-color: ghostwhite;
    }
</style>

@endsection


@section('content')
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col col-md-12 align-self-center">
            <div class="card">
                <div class="card-header">
                    <h3 class="title-registrer">
                        {{ __('labels.frontend.passwords.reset_password_box_title') }}
                    </h3>
                </div><!--card-header-->

                <div class="card-body">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ html()->form('POST', route('frontend.auth.password.reset'))->class('form-horizontal')->open() }}
                        {{ html()->hidden('token', $token) }}

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}

                                    {{ html()->email('email')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.email'))
                                        ->attribute('maxlength', 191)
                                        ->required() }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}

                                    {{ html()->password('password')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.password'))
                                        ->required() }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.frontend.password_confirmation'))->for('password_confirmation') }}

                                    {{ html()->password('password_confirmation')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.password_confirmation'))
                                        ->required() }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group mb-0 clearfix">
                                    {{ form_submit(__('labels.frontend.passwords.reset_password_button')) }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                    {{ html()->form()->close() }}
                </div><!-- card-body -->
            </div><!-- card -->
        </div><!-- col-6 -->
    </div><!-- row -->
</div>
@endsection
