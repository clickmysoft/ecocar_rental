@extends('frontend.layouts.app')

@section('title', app_name() . ' | Update Password')

@section('content')


<div class="section-bc">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="bc-left">
                    <h2 class="h2-title-bc">
                        เปลี่ยนรหัสผ่าน
                    </h2>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="gap"></div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3">
                                    <aside class="user-profile-sidebar">
                                        <div class="user-profile-avatar text-center">
                                            <img src="{{ asset($logged_in_user->user_image)}}" alt="Image Alternative text" title="AMaze" />
                                            <h5>Welcome</h5>
                                            <h5>{{ $logged_in_user->first_name }} {{ $logged_in_user->last_name }} </h5>
                                        </div>
                                        <ul class="list user-profile-nav">
                                            <li>
                                                <a href="Profile-ecocar.html"><i class="fa fa-cog"></i>Settings</a>
                                            </li>
                                            <li><a href="booking-history.html"><i class="fa fa-clock-o"></i>Booking History</a>
                                             <li><a href="{{ route('frontend.auth.password.expired') }}"><i class="fa fa-crosshairs"></i>Change Password</a></li>
                                            </li>

                                            @can('view backend')
                                             <li>
                                                <a href="{{ route ('admin.dashboard')}}"><i class="fa fa-cog"></i>{{ __('navs.frontend.user.administration') }}</a>
                                            </li>
                                            @endcan
                                            </li>
                                                <li>
                                                    <a href="#" id="bg-dropdown-display" title="Booking-History" class="dropdown-item" onclick="location.href='{{ route('frontend.auth.logout') }}'">
                                            <i class="fa fa-sign-out" ></i><span>{{ __('navs.general.logout') }}</span>
                                        </a>
                                                </li>
<!--                                        
                                            <li> 
                                                    <a type="submit" class="btn btn-lg btn-default btn-upload" role="button" 
                                                    style="margin-bottom: 10px; margin:0 auto; width: 140px;border-radius: 20px;background: #cbe0c6;font-weight: 500;color: #000;font-size: 13px;border: none;box-shadow: 1px 2px 5px #f3f3f3;">
                                                        <span> Upload Photo </span> 
                                                    </a>  
                                            </li> -->
                                        </ul>
                                    </aside>
                                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-12">
                             {{ html()->form('PATCH', route('frontend.auth.password.expired.update'))->class('form-horizontal')->open() }}
                                <h4>Personal Infomation</h4>
                                <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                    {{ html()->label(__('validation.attributes.frontend.old_password'))->for('old_password') }}

                                    {{ html()->password('old_password')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.old_password'))
                                        ->required() }}
                                </div>
                                <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                    {{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}

                                    {{ html()->password('password')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.password'))
                                        ->required() }}
                                </div>
                                <div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
                                    {{ html()->label(__('validation.attributes.frontend.password_confirmation'))->for('password_confirmation') }}

                                    {{ html()->password('password_confirmation')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.password_confirmation'))
                                        ->required() }}
                                </div>
                                {{ form_submit(__('labels.frontend.passwords.update_password_button')) }}
                        
                                <div class="gap gap-small"></div>   
                                  <!-- <input type="submit" class="btn btn-primary btn-saveprofile" value="Save Changes"> -->
                            </form>
                        </div>
                      
                        <div class="col-md-4 col-md-offset-1" style="display:none;">
                                <form action="">
                                             <h4>Location</h4>
                                        <div class="form-group form-group-icon-left">
                                            <label>บ้านเลขที่</label>
                                            <input class="form-control no-home" value="787" type="text" />
                                        </div>
                                        <div class="form-group">
                                            <label>ตำบล</label>
                                            <input class="form-control" value="ชมพู" type="text" />
                                        </div>
                                        <div class="form-group">
                                            <label>อำเภอ</label>
                                            <input class="form-control" value="อำเภอเมือง" type="text" />
                                        </div>
                                        <div class="form-group">
                                            <label>จังหวัด</label>
                                            <input class="form-control" value="ลำปาง" type="text" />
                                        </div>
                                        <div class="form-group">
                                            <label>รหัสไปษณี</label>
                                            <input class="form-control" value="521000" type="text" />
                                        </div>
                                        <div class="form-group">
                                            <label>ประเทศ</label>
                                            <input class="form-control" value="ไทยแลนด์" type="text" />
                                        </div>
                                        <input type="submit" class="btn btn-primary btn-saveprofile" value="Save Changes">
                                {{ html()->form()->close() }}
                        </div> 
                    </div>
                </div> 
            </div>
        </div>

<div class="gap gap-small"></div>


@endsection