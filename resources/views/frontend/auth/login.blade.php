@extends('frontend.layouts.app')

@section('title', app_name() . ' | Login')

@section('begin_css')

<style>
    div#abcdef {
        padding-top: 70px;
    }
    
</style>

@endsection

@section('content')

  <div class="global-wrap" style="background-image: url('../frontend/img/bg-car6.jpg');">
      <!-- START HEADER -->
      <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <div class="container-fluid relative">
          <!-- LEFT SIDE -->
          <div class="pull-left full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="header-inner">
              <a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
                <span class="icon-set menu-hambuger"></span>
              </a>
            </div>
            <!-- END ACTION BAR -->
          </div>
         
          <!-- RIGHT SIDE -->
          <div class="pull-right full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="header-inner">
              <a href="#" class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview" data-toggle-element="#quickview">
                <span class="icon-set menu-hambuger-plus"></span>
              </a>
            </div>
            <!-- END ACTION BAR -->
          </div>
        </div>
        <!-- END MOBILE CONTROLS -->
        <div class=" pull-left sm-table hidden-xs hidden-sm">
          
        </div>
        <div class=" pull-right">
          <!-- START User Info-->
          <div class="visible-lg visible-md m-t-10">
            <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
              <span class="semi-bold">
                    
              </span>
            </div>
            <div class="dropdown pull-right">
              <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="thumbnail-wrapper d32 circular inline m-t-5">
                <div class="pg pg-settings" width="32" height="32">
                </div>
                </span>
              </button>
              <ul class="dropdown-menu profile-dropdown" role="menu">
                <li><a href="#"><i class="pg-settings_small"></i> Settings</a>
                </li>
                <li><a href="#"><i class="pg-outdent"></i> Feedback</a>
                </li>
                <li><a href="mailto:support@contentsme.com.au"><i class="pg-signals"></i> Help</a>
                </li>
                <li class="bg-master-lighter">
                  <a href="/logout" class="clearfix">
                    <span class="pull-left">Logout</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <!-- END User Info-->
        </div>
      </div>
      <!-- END HEADER -->
        <div class="container">
            <h1 class="page-title"></h1>
        </div>
        <div class="gap"></div>
        <div class="container">
            <div class="row" >
             
                </div>
                <div class="col-md-6 box-one">
                    <h3>{{ __('index.frontend.sign_in') }}</h3>
                      {{ html()->form('POST', route('frontend.auth.login.post'))->open() }}
                        <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon input-icon-show"></i>
                            <label>{{ __('index.frontend.username') }}</label>
                            <input type="text" name="username" id="username" placeholder="Username" maxlength="191" required="required" class="form-control" style="border-radius: 26px;padding: 25px !important;">
                        </div>
                        <div class="form-group form-group-icon-left"><i class="fa fa-lock input-icon input-icon-show" ></i>
                            <label>{{ __('index.frontend.password') }}</label>
                            <input type="password" name="password" id="password" placeholder="Password" required="required" class="form-control" style="border-radius: 26px;padding: 25px !important;">
                        </div>

                        <div class="form-group" align="center">
                            <a href="{{ route('frontend.auth.password.reset') }}">{{ __('labels.frontend.passwords.forgot_password') }}</a>
                        </div>


                        <button class="btn-forgetpass btn-login-page signin" type="submit" value="Sign in">
                            {{ __('index.frontend.sign_in') }}
                            &nbsp;<i class="fa fa-car" aria-hidden="true"></i>
                        </button>

                            <!--row-->
                                {{ html()->form()->close() }}
                                <div class="row">
                                    <div class="col">
                                        <div class="text-center">
                                            {!! $socialiteLinks !!}
                                        </div>
                                    </div>
                                    <!--col-->
                                </div>
                            <!--row-->


                        {{ html()->form()->close() }}
                </div>
                

                <div class="col-md-6 box-two" style="opacity:0.7; background-color: #054B20; padding: 150px 70px; margin-top: 30px; ">
                     <h3 class="p-signup" style="color: #fff">{{ __('index.frontend.sign_up') }}</h3>
                     <p class="p-signup2" style="color: #fff">คลิกที่ปุ่ม "ลงทะเบียน" เพื่อทำการลงทะเบียนบัญชีผู้ใช้งาน</p>
                    <a href="{{route('frontend.auth.register')}}" style="color: #000;"><button style="border-radius: 50px;
                            padding: 12px 60px; margin-top: 10px">{{ __('index.frontend.sign_up') }}</button></a>
                </div>
            </div>
        </div>



@endsection


@section('begin_javascript')
<style type="text/css">
  .form-group .input-icon{
        top: 38px !important;
  }
</style>
@endsection
