@extends('frontend.layouts.app')

@section('title', app_name() . ' | Register')

@section('begin_css')
<style type="text/css">
html{
    overflow: scroll !important;
}
    h3.title-registrer {
           color: #ffffff;
           text-transform: uppercase;
           text-align: center;
           margin-bottom: 30px;
           text-shadow: 2px 2px 1px #000;
           border-bottom: 1px solid #fff;
    } 
    .card{
        padding: 20px 70px 30px 70px;
        margin: 30px auto;
        max-width: 500px;
        border-radius: 50px;
        background: linear-gradient( rgba(20, 164, 99, 0.61), rgba(0, 0, 0, 0.72) ),  url(../frontend/img/bg-car6.jpg);
        box-shadow: -5px 5px 56px #a2a2a2;
    }
    label{
        color: #fff;
    }
    button.btn.btn-success.btn-sm.pull-right{
            height: 40px;
            width: 100%;
            border-radius: 60px;
            font-size: 18px;
            margin-top: 15px;
    }
    input.form-control{
        border-radius: 50px;
        height: 40px;
    }
    #app{
        background-color: ghostwhite;
    }

    #main-footer{
        margin-top: 0px !important;
    }
    div#abcdef {
        padding-top: 130px;
    }
    .alert.alert-danger {
        padding-top: 140px;
    }
</style>

@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col col-md-12 align-self-center">
            <div class="card">
                <div class="card-header">
                <img src="{{ asset("frontend/img/icon/icons8-fiat-500-50.png")}}" style="width: 62px;display:  block;margin: 0 auto;vertical-align:  middle;top: 22px;
    left:  0;right:  0;padding-bottom: 11px;color: #fff !important;"> 
                    <h3 class="title-registrer">
                        {{ __('labels.frontend.auth.register_box_title') }}
                    </h3>
                </div><!--card-header-->
                <div class="card-body">
                    {{ html()->form('POST', route('frontend.auth.register.post'))->open() }}
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.frontend.first_name'))->for('first_name') }}
                                    {{ html()->text('first_name')
                                        ->class('form-control')
                                        ->placeholder(__(''))
                                        ->attribute('maxlength', 191) }}
                                </div><!--col-->
                            </div><!--row-->
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.frontend.last_name'))->for('last_name') }}
                                    {{ html()->text('last_name')
                                        ->class('form-control')
                                        ->placeholder(__(''))
                                        ->attribute('maxlength', 191) }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col-md-12">
                               <div class="form-group">
                                    {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}

                                    {{ html()->email('email')
                                        ->class('form-control')
                                        ->placeholder(__(''))
                                        ->attribute('maxlength', 191)
                                        ->required() }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->


                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" style="margin-bottom: 0;">
                                    {{ html()->label(__('Phone'))->for('Phone') }}
                                    {{ html()->text('phone')
                                        ->class('form-control')
                                        ->placeholder(__(''))
                                        ->attribute('maxlength', 10) }}
                                          
                                </div><!--form-group-->
                            </div><!--col-->
                        </div>
                       
                         <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ html()->label(__('ขื่อผู้ใช้'))->for('username') }}

                                    <input type="text" name="username" id="username"   required="required" class="form-control">
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}
                                    {{ html()->password('password')
                                        ->class('form-control')
                                        ->placeholder(__(''))
                                        ->required() }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.frontend.password_confirmation'))->for('password_confirmation') }}
                                    {{ html()->password('password_confirmation')
                                        ->class('form-control')
                                        ->placeholder(__(''))
                                        ->required() }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                        @if (config('access.captcha.registration'))
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Captcha::display() !!}
                                    {{ html()->hidden('captcha_status', 'true') }}
                                </div><!--col-->
                            </div><!--row-->
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mb-0 clearfix" >
                                    <button type="submit" class="btn btn-primary btn-lg" id="load" data-loading-text="<i class='fa fa-spinner fa-spin '></i> กำลังประมวลผล" style="background: #5cb85c;border-color: #5cb85c;border-radius: 60px;width: 100%;font-size: 16px;">{{ __('index.frontend.sign_up') }}</button>
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                    {{ html()->form()->close() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">
                                {!! $socialiteLinks !!}
                            </div>
                        </div><!--/ .col -->
                    </div><!-- / .row -->
                </div><!-- card-body -->
            </div><!-- card -->
        </div><!-- col-md-8 -->
    </div><!-- row -->
</div>
@endsection

@push('after-scripts')
    @if (config('access.captcha.registration'))
        {!! Captcha::script() !!}
    @endif
@endpush

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    




</script>