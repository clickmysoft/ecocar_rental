@extends('frontend.layouts.app')

@section('content')

<div class="section-bc">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="bc-left">
                    <h2 class="h2-title-bc">
                        โปรไฟล์
                    </h2>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="gap"></div>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <aside class="user-profile-sidebar">
                <div class="user-profile-avatar text-center">
                    <img src="{{ asset($logged_in_user->user_image)}}" alt="Image Alternative text" title="AMaze" />
                    <h5>Welcome</h5>
                    <h5>{{ $logged_in_user->first_name }} {{ $logged_in_user->last_name }} </h5>
                </div>
                <ul class="list user-profile-nav">
                    <li>
                        <a href="{{ route('frontend.user.account') }}"><i class="fa fa-cog"></i>Settings</a>
                    </li>
                    <li><a href="booking-history.html"><i class="fa fa-clock-o"></i>Booking History</a>
                        <li><a href="{{ route('frontend.auth.password.expired') }}"><i class="fa fa-crosshairs"></i><i class="fa fa-address-book-o" aria-hidden="true"></i>Change Password</a></li>
                    </li>

                    @can('view backend')
                    <li>
                        <a href="{{ route ('admin.dashboard')}}"><i class="fa fa-cog"></i>{{ __('navs.frontend.user.administration') }}</a>
                    </li>
                    @endcan
                    <li>
                        <a href="#" id="bg-dropdown-display" title="Booking-History" class="dropdown-item" onclick="location.href='{{ route('frontend.auth.logout') }}'">
                            <i class="fa fa-sign-out" ></i><span>{{ __('navs.general.logout') }}</span>
                        </a>
                    </li>
                </ul>
            </aside>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <form method="POST" action="{{ route('admin.customer.management.store') }}" enctype="multipart/form-data" class="form-horizontal" id="my-form">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="panel bd-t-red"> 

                                        <div class="panel-heading">
                                            <h3 class="panel-title">
                                                <i class="fa fa-gear"></i>&nbsp;<strong>สร้างรายละเอียด&nbsp;</strong>ข้อมูลลูกค้า
                                            </h3>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="email">คำขึ้นต้น:</label>
                                            <div class="col-sm-10">
                                                <select name="prefix_name_customer"  tabindex="4" class="form-control chosen-img" style="height:40px;" >
                                                    <option value="" data-img-src="{{ asset("backend/img/various/clipboard-list.svg")}}">เลือกคำนำหน้า</option>
                                                    @include('includes.partials.customers.prefix_name')
                                                </select>

                                                @if($errors->has('prefix_name_customer'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('prefix_name_customer') }} </strong>
                                                </span>
                                                @endif

                                            </div>
                                        </div>


                                        <div class="form-group ">
                                            <label class="control-label col-sm-2" for="email">ชื่อ:</label>
                                            <div class="col-sm-10">
                                                <div class="input-group ">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-dashboard"></i>
                                                    </span>
                                                    <span class="input-group-addon" id="code_branch" style="padding: 10px;background-color: #eeeeee57;border-right: 0;">
                                                        xxxx - 
                                                    </span>
                                                    <input  class="form-control " type="text" name="first_name_customer" placeholder="Enter your name" value="<?=old('first_name_customer')?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="control-label col-sm-2" for="last name">นามสกุล:</label>
                                            <div class="col-sm-10">
                                                <div class="input-group ">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-dashboard"></i>
                                                    </span>
                                                    <input class="form-control " type="text" name="last_name_customer" placeholder="Enter your name" value="<?=old('last_name_customer')?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="control-label col-sm-2" for="">ชื่อเล่น:</label>
                                            <div class="col-sm-10">
                                                <div class="input-group ">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-dashboard"></i>
                                                    </span>
                                                    <input  class="form-control " type="text" name="nick_name_customer" placeholder="Enter your name" value="<?=old('nick_name_customer')?>">
                                                </div>
                                            </div>
                                        </div>










                                        <div class="form-group ">
                                            <label class="control-label col-sm-2">อีเมลล์:</label>
                                            <div class="col-sm-10">
                                                <div class="input-group ">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-dashboard"></i>
                                                    </span>
                                                    <input  class="form-control" type="email" name="email_customer" placeholder="Enter your name" >
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group ">
                                            <label class="control-label col-sm-2" for="email">เบอร์โทรศัพท์มือถือ:</label>
                                            <div class="col-sm-10">
                                                <div class="input-group ">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-dashboard"></i>
                                                    </span>
                                                    <input  class="form-control " maxlength="10" type="text" name="phone_customer" placeholder="Enter your name" value="<?=old('phone_customer')?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="control-label col-sm-2" for="lineid">Line ID:</label>
                                            <div class="col-sm-10">
                                                <div class="input-group ">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-dashboard"></i>
                                                    </span>
                                                    <input  class="form-control " type="text" name="line_id_customer" placeholder="Enter your name" value="<?=old('line_id_customer')?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="control-label col-sm-2" for="email">หมายเลขบัตรประชาชน:</label>
                                            <div class="col-sm-10">
                                                <div class="input-group ">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-dashboard"></i>
                                                    </span>
                                                    <input  class="form-control " maxlength="13" type="text" name="id_card_customer" placeholder="Enter your name" value="<?=old('id_card_customer')?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="email">ที่อยู่ตามบัตรประชาชน:</label>
                                            <div class="col-sm-10">
                                                <textarea name="personal_card_address" class="personal_card_address" rows="10" cols="80"><?=old('personal_card_address');?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="email">ที่อยู่ปัจจุบัน:</label>
                                            <div class="col-sm-10">
                                                <textarea name="present_address" class="present_address" rows="10" cols="80"><?=old('present_address');?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="pwd">
                                                ชื่อบริษัทหรือชื่อที่ทำงาน:
                                            </label>
                                            <div class="col-sm-10">
                                                <div class="input-group ">
                                                    <span class="input-group-addon" style="padding: 10px;">
                                                        <i class="fa fa-automobile"></i>
                                                    </span>
                                                    <input class=" form-control" name="company_name" type="text" required style="text-transform: uppercase;" value="<?=old('company_name');?>"> 
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group ">
                                            <label class="control-label col-sm-2" for="email">ตำแหน่งงาน:</label>
                                            <div class="col-sm-4">
                                                <div class="input-group ">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-dashboard"></i>
                                                    </span>
                                                    <input  class="form-control " type="text" name="position" placeholder="Enter your name" value="<?=old('position')?>">
                                                </div>
                                            </div>

                                            <div class="col-sm-5">
                                                <label class="control-label col-sm-4" for="email">อายุงาน:</label>
                                                <div class="input-group ">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-dashboard"></i>
                                                    </span>
                                                    <input  class="form-control " type="text" name="experience" placeholder="Enter your name" value="<?=old('experience')?>">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group ">
                                            <label class="control-label col-sm-2" for="email">เบอร์โทรที่ทำงาน:</label>
                                            <div class="col-sm-4">
                                                <div class="input-group ">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-dashboard"></i>
                                                    </span>
                                                    <input  class="form-control " type="text" name="tel_office" placeholder="Enter your name" value="<?=old('tel_office')?>">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="email">จุดประสงค์การใช้งานรถยนต์:</label>
                                            <div class="col-sm-10">
                                                <textarea name="explain_use_car" class="form-control" rows="10" cols="80"><?=old('explain_use_car');?></textarea>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="email">จังหวัดที่ใช้รถ:</label>
                                            <div class="col-sm-10">
                                                <select class="form-control chosen-img"  name="province_use_car" style="height:40px;" data-placeholder="เลือกจังหวัด" >
                                                    <option data-img-src="{{ asset("backend/img/various/clipboard-list.svg")}}">เลือกจังหวัด</option>
                                                    <?php
                                                    $arr_province =array("กระบี่","กรุงเทพมหานคร","กาญจนบุรี","กาฬสินธุ์","กำแพงเพชร","ขอนแก่น","จันทบุรี","ฉะเชิงเทรา" ,"ชลบุรี","ชัยนาท","ชัยภูมิ","ชุมพร","เชียงราย","เชียงใหม่","ตรัง","ตราด","ตาก","นครนายก","นครปฐม","นครพนม","นครราชสีมา" ,"นครศรีธรรมราช","นครสวรรค์","นนทบุรี","นราธิวาส","น่าน","บุรีรัมย์","ปทุมธานี","ประจวบคีรีขันธ์","ปราจีนบุรี","ปัตตานี" ,"พะเยา","พังงา","พัทลุง","พิจิตร","พิษณุโลก","เพชรบุรี","เพชรบูรณ์","แพร่","ภูเก็ต","มหาสารคาม","มุกดาหาร","แม่ฮ่องสอน" ,"ยโสธร","ยะลา","ร้อยเอ็ด","ระนอง","ระยอง","ราชบุรี","ลพบุรี","ลำปาง","ลำพูน","เลย","ศรีสะเกษ","สกลนคร","สงขลา" ,"สตูล","สมุทรปราการ","สมุทรสงคราม","สมุทรสาคร","สระแก้ว","สระบุรี","สิงห์บุรี","สุโขทัย","สุพรรณบุรี","สุราษฎร์ธานี" ,"สุรินทร์","หนองคาย","หนองบัวลำภู","อยุธยา","อ่างทอง","อำนาจเจริญ","อุดรธานี","อุตรดิตถ์","อุทัยธานี","อุบลราชธานี");
                                                    foreach ($arr_province as &$alphabet) {
                                                        $img = '';
                                                        echo "<option ";
                                                        echo " data-img-src='".$img."' value=".$alphabet.">";
                                                        echo $alphabet;
                                                        echo "</option>";
                                                    }
                                                    ?>

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel bd-t-red">
                                        <div class="panel-heading ">
                                            <h3 class="panel-title" style="width:100%;">
                                                <i class="fa fa-gear"></i>&nbsp;<strong>ผู้รับผลประโยชน์&nbsp;</strong>จากประกันอุบัติเหตุหรือบุคคลที่สามารถติดต่อได้ยามฉุกเฉิน(เน้นเป็นพ่อแม่หรือญาติสนิท)</h3>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="email">คำขึ้นต้น:</label>
                                                <div class="col-sm-10">
                                                    <select name="prefix_name_beneficiary"  tabindex="4" class="form-control chosen-select" style="height:40px;" >
                                                        <option value="" data-img-src="{{ asset("backend/img/various/clipboard-list.svg")}}">เลือกคำนำหน้า</option>
                                                        @include('includes.partials.customers.prefix_name')
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group ">
                                                <label class="control-label col-sm-2" for="email">ชื่อ:</label>
                                                <div class="col-sm-10">
                                                    <div class="input-group ">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-dashboard"></i>
                                                        </span>
                                                        <span class="input-group-addon" id="code_branch2" style="padding: 10px;background-color: #eeeeee57;border-right: 0;">
                                                            xxxx - 
                                                        </span>
                                                        <input  class="form-control " type="text" name="first_name_beneficiary" placeholder="Enter your name" value="<?=old('first_name_beneficiary')?>">
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group ">
                                                <label class="control-label col-sm-2" for="email">นามสกุล:</label>
                                                <div class="col-sm-10">
                                                    <div class="input-group ">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-dashboard"></i>
                                                        </span>
                                                        <input  class="form-control " type="text" name="last_name_beneficiary" placeholder="Enter your name" value="<?=old('last_name_beneficiary')?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="control-label col-sm-2" for="email">ความสัมพันธ์:</label>
                                                <div class="col-sm-10">
                                                    <div class="input-group ">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-dashboard"></i>
                                                        </span>
                                                        <input  class="form-control " type="text" name="relation_beneficiary" placeholder="Enter your name" value="<?=old('relation_beneficiary')?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="control-label col-sm-2" for="email">เบอร์โทร:</label>
                                                <div class="col-sm-10">
                                                    <div class="input-group ">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-dashboard"></i>
                                                        </span>
                                                        <input  class="form-control " type="text" name="phone_beneficiary" placeholder="Enter your name" value="<?=old('phone_beneficiary')?>">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>



                                        <div class=" bd-t-red">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">
                                                    <i class="fa fa-gears"></i> <strong>เอกสารที่ต้องใช้</strong>สำหรับเช่ารถ</h3>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-sm-2">บัตรประชาชน</label>
                                                    <div class="col-md-10">
                                                        <div class="input-group ">
                                                            <span class="input-group-addon" style="width: 41px;">
                                                                <i class="fa fa-file-image-o"></i>
                                                            </span>
                                                            <input class=" form-control" name="personal_card_img" type="file" accept="image/*">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-sm-2">ใบขับขี่</label>
                                                    <div class="col-md-10">
                                                        <div class="input-group ">
                                                            <span class="input-group-addon" style="width: 41px;">
                                                                <i class="fa fa-file-image-o"></i>
                                                            </span>
                                                            <input class=" form-control" name="driver_license_card_img" type="file" accept="image/*">
                                                        </div>
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label class="control-label col-sm-2">บัตรเคดิต</label>
                                                    <div class="col-md-10">
                                                        <div class="input-group ">
                                                            <span class="input-group-addon" style="width: 41px;">
                                                                <i class="fa fa-file-image-o"></i>
                                                            </span>
                                                            <input class=" form-control " name="credit_card_img" type="file" accept="image/*">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>



                                            <div class=" bd-t-red">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">
                                                        <i class="fa fa-gears"></i> <strong>กำหนด</strong>รายละเอียดเพิ่มเติม</h3>
                                                    </div>


                                                    <div class="form-group">
                                                        <label class="control-label col-sm-2">รูปภาพโปรไฟล์</label>
                                                        <div class="col-md-10">
                                                            <div class="input-group ">
                                                                <span class="input-group-addon" style="width: 41px;">
                                                                    <i class="fa fa-file-image-o"></i>
                                                                </span>
                                                                <input class=" form-control" name="image_profile" type="file" accept="image/*">
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <ul class="list-inline" style="float:right;">
                                                        <li><button type="submit" class="btn btn-success "><i class="fa fa-save"></i>&nbsp;บันทึก</button></li>
                                                        <li><a class="btn btn-warning prev-step" href="{{ route('admin.auth.user.index') }}"><i class="fa fa-times"></i>&nbsp;ยกเลิก</a></li>
                                                    </ul>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <div class="gap gap-small"></div>


@endsection




@section('begin_css')
<link href="{{ asset("plugins/datetimepicker/jquery.datetimepicker.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.date.css")}}" rel="stylesheet">
<link href="{{ asset("plugins/pickadate/themes/default.time.css")}}" rel="stylesheet">
<link href="{{ asset("backend/dependentdrop/dependentdrop.css")}}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset("plugins/chosen/chosen.css")}}" />
<link rel="stylesheet" href="{{ asset("plugins/chosen/chosenImage/chosenImage.css")}}"/>
<link rel="stylesheet" href="{{ asset("plugins/wizard/wizard.css")}}" />
<link rel="stylesheet" href="{{ asset("plugins/jquery-steps/jquery.steps.css")}}" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />


<style type="text/css">
    .chosen-single{
        line-height: 32px !important;
        height: 32px !important;
    }

    div.payan-chana ul.chosen-choices{
            height: 33px !important;
    }

    div#select_number ul.chosen-results li{
         background-size: 0px !important;
    }

    div#brand_cars ul.chosen-results li:first-child{
        background-size: 0px !important;
        background-position-x: 12px;
    }

    div#brand_cars ul.chosen-results li{
        background-size: 27px !important;
        text-indent: 7px;
        padding-top: 6px;
        -webkit-backface-visibility: hidden;
        -webkit-transform: translateZ(0) scale(1.0, 1.0);
    }
   
    div#car_type ul.chosen-results li{
        background-size: 40px !important;
        text-indent: 26px;
    }
    div#car_type ul.chosen-results li:first-child{
        background-size: 0px !important;
        background-position-x: 12px;
    }

    div#total_human ul.chosen-results li:first-child{
        background-size: 0px !important;
    }

    div#total_bag ul.chosen-results li:first-child{
        background-size: 0px !important;
    }
    div.fule_type > div.ui-checkbox{
            margin-top: 0;
    }
    div.fule_type > div.ui-checkbox label{
           font-size: 14px !important;
    }


.registration-ui {
 /*   background: linear-gradient(to bottom, #f8d038 0%,#f5ca2e 100%);*/
   /* padding: .25em 1em .25em 1.75em;*/
   /*padding:0 .7em 1.14em .75em;
    font-weight: bold;
    font-size: 2em;
    border-radius: 5px;
    border: 1px solid #000;
    box-shadow: 1px 1px 1px #ddd;
    position: relative;
    font-family: helvetica, ariel, sans-serif;*/
    padding: 12.4px .7em 0.7em .75em;
    font-weight: bold;
    font-size: 2em;
    border-radius: 5px;
    border: 1px solid #000;
    box-shadow: 1px 1px 1px #ddd;
    position: relative;
    font-family: helvetica, ariel, sans-serif;
}

.registration-ui:before {
    /*content: 'ECOCAR';
    display: block;
    width: 30px;
    height: 100%;
    background: #063298;
    position: absolute;
    top: 0;
    border-radius: 5px 0 0 5px;
    color: #f8d038;
    font-size: .5em;
    line-height: 85px;
    padding-left: 5px;*/
    content: 'ECOCAR';
    display: block;
    text-align: center;
    width: 100%;
    height: 18px;
    background: #159077;
    position: absolute;
    top: 46.1px;
    border-radius: 5px 0 0 5px;
    color: #ffffff;
    font-size: .4em;
    line-height: 19px;
    left: 0px;
    border-bottom-right-radius: 4px;
    border-top-left-radius: 0;
    border-bottom-left-radius: 4px;
}

.registration-ui:after {
    content: '';
    display: block;
    position: absolute;
    top: 7px;
    left: 5px;
    width: 20px;
    height: 20px;
    border-radius: 30px;
   /* border: 1px dashed #f8d038;*/
}


</style>
@endsection





<!--START IMPORT JAVASCRIPT -->
@section('begin_javascript') 
<script src="{{ asset("plugins/ckeditor/ckeditor.js")}}"></script>
<script src="{{ asset("plugins/bootstrap-switch/bootstrap-switch.js")}}"></script>
<script src="{{ asset("plugins/bootstrap-progressbar/bootstrap-progressbar.js")}}"></script>
<script src="{{ asset("plugins/chosen/chosen.jquery.js")}}"></script>
<script src="{{ asset("plugins/chosen/chosenImage/chosenImage.jquery.js")}}"></script>
<!-- Laravel Javascript Validation -->
<script type="text/javascript" src="{{ asset("vendor/jsvalidation/js/jsvalidation.js")}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\Backend\Customers\CustomerRequest','#my-form'); !!}

<script>
        CKEDITOR.replace('present_address',{
           toolbar: [
              { name: 'tools', items: [ 'Maximize'] },
              { name: 'insert', items: ['Format' ] },
              { name: 'document', items: [ '-', 'Undo', 'Redo' ] },
              { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
              { name: 'insert', items: ['Table', 'HorizontalRule', 'SpecialChar' ] },
              { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] },
              { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
              { name: 'colors', groups: [ 'colors' ] },
            ]
        });

        CKEDITOR.replace('personal_card_address',{
           toolbar: [
              { name: 'tools', items: [ 'Maximize'] },
              { name: 'insert', items: ['Format' ] },
              { name: 'document', items: [ '-', 'Undo', 'Redo' ] },
              { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
              { name: 'insert', items: ['Table', 'HorizontalRule', 'SpecialChar' ] },
              { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] },
              { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
              { name: 'colors', groups: [ 'colors' ] },
            ]
        });

        CKEDITOR.replace('explain_use_car',{
           toolbar: [
              { name: 'tools', items: [ 'Maximize'] },
              { name: 'insert', items: ['Format' ] },
              { name: 'document', items: [ '-', 'Undo', 'Redo' ] },
              { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
              { name: 'insert', items: ['Table', 'HorizontalRule', 'SpecialChar' ] },
              { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] },
              { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
              { name: 'colors', groups: [ 'colors' ] },
            ]
        });

         CKEDITOR.replace('customers_description',{
           toolbar: [
              { name: 'tools', items: [ 'Maximize'] },
              { name: 'insert', items: ['Format' ] },
              { name: 'document', items: [ '-', 'Undo', 'Redo' ] },
              { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
              { name: 'insert', items: ['Table', 'HorizontalRule', 'SpecialChar' ] },
              { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] },
              { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
              { name: 'colors', groups: [ 'colors' ] },
            ]
        });

</script>

<script type="text/javascript">
    jQuery(document).ready(function(){


        jQuery(".chosen-img,.chosen-select").chosenImage();

        jQuery('input.number').keyup(function(event) {
          // skip for arrow keys
          if(event.which >= 37 && event.which <= 40) return;

          // format number
          jQuery(this).val(function(index, value) {
            return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            ;
          });
        });

        jQuery("select[name='prefix_name_customer']").change(function(e){
            e.preventDefault();
            jQuery("span#code_branch").text(jQuery(this).val());
        });

        jQuery("select[name='prefix_name_beneficiary']").change(function(e){
            e.preventDefault();
            jQuery("span#code_branch2").text(jQuery(this).val());
        });
    });// End tag jQuery

</script>

@if(session()->has('message'))

    <script type="text/javascript">
        <?php
            $type = session()->get('message');
            $message = __('alerts.backend.database.'.$type.'');
        ?>

        <?=$type?>(
                '<i class="fa fa-check-square-o" style="color:#fff;padding-right:8px"></i><?=$message?>', {
                    HorizontalPosition: 'right',
                    VerticalPosition: 'top',
                    ShowOverlay: false,
                    TimeShown: 5000,
                    MinWidth:400
        });
    </script>
@endif

@endsection



