@extends('frontend.layouts.app')

@section('begin_css')

<style>
    div#abcdef {
        padding-top: 130px;
    }
    .box-card-ecocar {
        height: 380px;
    }
    .box-range {
        padding-bottom: 30px;
    }
</style>

@endsection

@section('content')
<div class="section-bc">
    <div class="container">
        <div class="col-md-6">
            <div class="bc-left">
                <h2 class="h2-title-bc">
                    {{ __('index.frontend.our_branch') }}
                </h2>
            </div>
        </div>
        <div class="col-md-6">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">เกี่ยวกับ</a></li>
                    <li class="breadcrumb-item"><a href="#">สาขาของเรา</a></li>

                </ol>
            </nav>
        </div>
    </div>

</div>
<div class="container-fluid">
    <div class="row">
        <div id="map" style="width: 100%; height: 500px;"></div>
    </div>

</div>
<div class="container">
    <div class="row row-wrap">
        <div class=" text-center">
            <div class="gap gap-small"></div>
            <div class="col-md-12">
                <br/>
                <p style="font-weight: 300;text-align: left;font-size: 16px;line-height: 1.5em;"> รถเช่า ECOCAR เป็นผู้ให้บริการรถเช่าแบบขับเองและรถเช่าพร้อมคนขับ เราเปิดบริการตั้งแต่วันที่ 1 ธันวาคม 2555 จากความมุ่งมั่นในการสร้างการให้บริการที่เป็นเลิศ สร้างความแตกต่างจากคู่แข่งในตลาดรถเช่า ทำให้ ECOCAR เป็นแบรนด์รถเช่าที่ลูกค้าให้ความใว้วางใจ
                    ลูกค้าเก่ามาใช้อย่างต่อเนื่อง จนเราเป็นเพื่อนสนิทกัน เพื่อนที่พร้อมจะเดินทางไปกับคุณสู่จุดหมายอย่างปลอดภัย อีกทั้งมีลูกค้าใหม่ที่มาจากการบอกต่อกันแบบ ปากต่อปาก ลูกค้าของ ECOCAR เพิ่มขึ้นอย่างต่อเนื่อง ทำให้บริษัทขยายตัวอย่างต่อเนื่อง
                    อีกทั้งความเป็นน้ำหนึ่งใจเดียวกันของพนักงาน ECOCAR ที่มีจุดมุ่งหมายเดียวกันที่จะทำให้ ECOCAR เป็นแบรนด์รถเช่าชั้นนำของไทย ปัจจุบัน ECOCAR เปิดให้บริการ 9 สาขา คือ </p>
                <div class="gap gap-small"></div>
                <br/>
                <h3>{{ __('index.frontend.all_ecocar_office') }}</h3>
                <p class="line-bottom"></p>
                <div class="gap small-gap"></div>
                <div class="col-md-4 box-range">
                    <!-- <div class="box-card-ecocar"> -->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d123908.2793638099!2d100.616644!3d13.950642!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xccb38d39364d87e2!2z4Lia4Lij4Li04Lip4Lix4LiXIOC5hOC4l-C4ouC5gOC4o-C5ieC4meC4l-C5jOC4reC4teC5guC4geC5ieC4hOC4suC4o-C5jCDguIjguY3guLLguIHguLHguJQ!5e0!3m2!1sen!2sth!4v1516930345024"
                            width="100%" height="200" frameborder="0" style="border:0;border-radius: 5px;padding-bottom:  0 !important;padding:  0;" allowfullscreen="">
                        </iframe>
                        <div class="box-description">
                            <h5 class="title-branch text-left">1.สำนักงานใหญ่ รถเช่าดอนเมือง</h5>
                            <p class="description-branch" style="margin-bottom: 30px;">
                                ตั้งอยู่เลขที่ 279/57 ถนนวิภาวดีรังสิต แขวงสนามบิน เขตดอนเมือง กรุงเทพมหานคร 10210
                            </p>
                            <a  href="{{ url('downtown') }}" type="button" class="btn btn-default prev-step" style="background: #00551f;color: #fff;width: 150px;">
                                {{ __('index.frontend.more_details') }}
                            </a>
                        </div>
                    <!-- </div> -->
                </div>
                <div class="col-md-4 box-range">
                    <!-- <div class="box-card-ecocar"> -->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d496124.38434138114!2d100.680915!3d13.72016!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3de571231f47addf!2zRUNPQ0FSIOC4o-C4luC5gOC4iuC5iOC4suC4quC4suC4guC4suC4reC5iOC4reC4meC4meC4uOC4ig!5e0!3m2!1sen!2sth!4v1516932725759"
                            width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                        <div class="box-description">
                            <h5 class="title-branch">2.สำนักงานรถเช่าสาขาอ่อนนุช <br/>(รถเช่าอ่อนนุช)</h5>
                            <p class="description-branch">
                                ตั้งอยู่เลขที่ 268/13 ถนนอ่อนนุช แขวงประเวศ เขตประเวศ กรุงเทพมหานคร 10250
                            </p>
                            <a  href="{{ url('onnut') }}" type="button" class="btn btn-default prev-step" style="background: #00551f;color: #fff;width: 150px;">
                                {{ __('index.frontend.more_details') }}
                            </a>
                        </div>
                    <!-- </div> -->
                </div>
                <div class="col-md-4 box-range">
                    <!-- <div class="box-card-ecocar"> -->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d496127.12192976405!2d100.454871!3d13.718865!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4bbcf7df67f7df06!2z4Lia4Lij4Li04Lip4Lix4LiXIOC4o-C4luC5gOC4iuC5iOC4suC4reC4teC5guC4hOC4hOC4suC4o-C5jCDguIjguLPguIHguLHguJQ!5e0!3m2!1sen!2sth!4v1516933143471"
                            width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                        <div class="box-description">
                            <h5 class="title-branch">3.สำนักงานรถเช่าสาขาบางหว้า <br>(รถเช่าบางหว้า)</h5>
                            <p class="description-branch">
                                ตั้งอยู่เลขที่ 9 ซอยเพชรเกษม 25/5 แขวงบางหว้า เขตภาษีเจริญ กรุงเทพมหานคร 10160
                            </p>
                            <a  href="{{ url('bangwa') }}" type="button" class="btn btn-default prev-step" style="background: #00551f;color: #fff;width: 150px;">
                                {{ __('index.frontend.more_details') }}
                            </a>
                        </div>
                    <!-- </div> -->
                </div>

                <div class="col-md-4 box-range">
                    <!-- <div class="box-card-ecocar"> -->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d496003.2316732164!2d100.624765!3d13.777351!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6ea54b49ef6a673b!2z4Lij4LiW4LmA4LiK4LmI4Liy4Lil4Liy4LiU4Lie4Lij4LmJ4Liy4LinIHwgRUNPQ0FSIHJlbnQtYS1jYXI!5e0!3m2!1sen!2sth!4v1516933209395"
                            width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                        <div class="box-description">
                            <h5 class="title-branch text-left">4.สำนักงานรถเช่าสาขาลาดพร้าว (รถเช่าลาดพร้าว)</h5>
                            <p class="description-branch">
                                ตั้งอยู่เลขที่ 2438 ถนนลาดพร้าว แขวงพลับพลา เขตวังทองหลาง กรุงเทพมหานคร 10310
                            </p>
                            <a  href="{{ url('ladprao') }}" type="button" class="btn btn-default prev-step" style="background: #00551f;color: #fff;width: 150px;">
                                {{ __('index.frontend.more_details') }}
                            </a>
                        </div>
                    <!-- </div> -->
                </div>
                <div class="col-md-4 box-range">
                    <!-- <div class="box-card-ecocar"> -->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d61978.099619013476!2d100.509933!3d13.861161!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c10ea1835cd6c1b!2z4Lij4LiW4LmA4LiK4LmI4Liy4LiZ4LiZ4LiX4Lia4Li44Lij4Li1IHwgRUNPQ0FSIHJlbnQtYS1jYXI!5e0!3m2!1sen!2sth!4v1516933280792"
                            width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                        <div class="box-description">
                            <h5 class="title-branch">5.สำนักงานรถเช่าสาขานนทบุรี (รถเช่านนทบุรี)</h5>
                            <p class="description-branch" style="margin-bottom: 30px;">
                                ตั้งอยู่เลขที่ 160/1 ถนนรัตนาธิเบศร์ ซอย 18 ต.บางกระสอ อ.เมืองนนทบุรี จ.นนทบุรี 11000
                            </p>
                            <a  href="{{ url('nonthaburi') }}" type="button" class="btn btn-default prev-step" style="background: #00551f;color: #fff;width: 150px;">
                                {{ __('index.frontend.more_details') }}
                            </a>
                        </div>
                    <!-- </div> -->
                </div>
                <div class="col-md-4 box-range">
                    <!-- <div class="box-card-ecocar"> -->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d62032.97502642401!2d100.599198!3d13.654057!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x460b84ebafa2330b!2sTMB+Bank+Public+Company+Limited!5e0!3m2!1sen!2sth!4v1516933328760" width="100%" height="200"
                            frameborder="0" style="border:0" allowfullscreen></iframe>
                        <div class="box-description">
                            <h5 class="title-branch">6.สำนักงานรถเช่าสาขาสมุทรปราการ (รถเช่าสำโรง)</h5>
                            <p class="description-branch">
                                ตั้งอยู่เลขที่ 1199 ถนนสุขุมวิท ตำบลสำโรงเหนือ อ.เมืองสมุทรปราการ จ.สมุทรปราการ 10270
                            </p>
                            <a  href="{{ url('samrong') }}" type="button" class="btn btn-default prev-step" style="background: #00551f;color: #fff;width: 150px;">
                                {{ __('index.frontend.more_details') }}
                            </a>
                        </div>
                    <!-- </div> -->
                </div>
                <div class="col-md-4 box-range">
                    <!-- <div class="box-card-ecocar text-left"> -->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d123181.67064630135!2d104.80546187255862!3d15.244481106975414!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2b6145f4cd953a3f!2z4Lij4LiW4LmA4LiK4LmI4Liy4Lit4Li44Lia4LilIEVDT0NBUiDguKPguJbguYDguIrguYjguLLguKrguLLguILguLLguK3guLjguJrguKXguKPguLLguIrguJjguLLguJnguLUo4LmA4LiK4LmI4Liy4Lij4LiW4Lit4Li44Lia4LilKQ!5e0!3m2!1sen!2sth!4v1516933373314"
                            width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                        <div class="box-description">
                            <h5 class="title-branch">7.สำนักงานสาขาอุบลราชธานี (รถเช่าอุบล)</h5>
                            <p class="description-branch"   style="margin-bottom: 30px;">
                                ตั้งอยู่เลขที่ 456/9 ถนนเทพโยธี ตำบลในเมือง อำเภอเมือง จังหวัดอุบลราชธานี 34000
                            </p>
                            <!-- <div style="text-align: center;"> -->
                                <a  href="{{ url('ubonratchathani') }}" type="button" class="btn btn-default prev-step" style="background: #00551f;color: #fff;width: 150px;">
                                    {{ __('index.frontend.more_details') }}
                                </a>
                            <!-- </div> -->
                        </div>
                    <!-- </div> -->
                </div>
                <div class="col-md-4 box-range">
                    <!-- <div class="box-card-ecocar"> -->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7555.261702226874!2d98.96801!3d18.770019!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfac3aeae696d82e1!2sChiang+Mai+International+Airport+T1!5e0!3m2!1sen!2sth!4v1516933439567" width="100%"
                            height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                        <div class="box-description">
                            <h5 class="title-branch">8.สำนักงานสาขาเชียงใหม่ (รถเช่าเชียงใหม่)</h5>
                            <p class="description-branch"  style="margin-bottom: 30px;">
                                ตั้งอยู่เลขที่ 92 ถนนสมโภชเชียงใหม่ 700 ปี ตำบลฟ้าฮ่าม อำเภอเมืองเชียงใหม่ จังหวัดเชียงใหม่ 50000
                            </p>
                            <a  href="{{ url('chiangmai') }}" type="button" class="btn btn-default prev-step" style="background: #00551f;color: #fff;width: 150px;">
                                {{ __('index.frontend.more_details') }}
                            </a>
                        </div>
                    <!-- </div> -->
                </div>

                <div class="col-md-4 box-range">
                    <!-- <div class="box-card-ecocar"> -->
                        <!-- <img class="img-ceo" src="{{ asset("frontend/img/Lamborghini-Mansory-Aventador.png")}}"> -->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d62220.171967630886!2d100.883794!3d12.923093!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x56ef2e310ecd81f2!2sMuang+Pattaya+Gas+Ltd.%2C+Part.!5e0!3m2!1sen!2sth!4v1516933479968" width="100%"
                            height="200" frameborder="0" style="border:0" allowfullscreen>
                        </iframe>
                        <div class="box-description">
                            <h5 class="title-branch">9.สำนักงานสาขาพัทยา (รถเช่าพัทยา)</h5>
                            <p class="description-branch"  style="margin-bottom: 30px;">
                                ตั้งอยู่เลขที่ 388/9 ถนนสุขุมวิท ตำบลหนองปรือ อำเภอบางละมุง จังหวัดชลบุรี 20150
                            </p>
                            <a  href="{{ url('pattaya') }}" type="button" class="btn btn-default prev-step" style="background: #00551f;color: #fff;width: 150px;">
                                {{ __('index.frontend.more_details') }}
                            </a>
                        </div>
                    <!-- </div> -->
                </div>
            </div>
        </div>
    </div>
</div>

</div>

<div class="gap gap-small"></div>
@endsection


@section('begin_javascript')


<script type="text/javascript">
    var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          center: new google.maps.LatLng(13.8222222, 100.5999999), 
          mapTypeId: 'roadmap'
        });
        var iconBase = 'https://www.picz.in.th/images/2018/02/06/';  
        var icons = {
          parking: {
            icon: iconBase + 'sedan-icon-google-map.png'
          },
          library: {
            icon: iconBase + 'sedan-icon-google-map.png'
          },
          info: {
            icon: iconBase + 'sedan-icon-google-map.png' 
          }
        };

        var features = [
          {
            position: new google.maps.LatLng(13.9538709, 100.6136862), 
            type: 'info',
            content: '<h5>สำนักงานใหญ่ รถเช่าดอนเมือง</h5>ตั้งอยู่เลขที่ 279/57 ถนนวิภาวดีรังสิต แขวงสนามบิน เขตดอนเมือง กรุงเทพมหานคร 10210<br>เบอร์โทร 02-002-4606<br>มือถือ 092-2848-660'
          },
          {
            position: new google.maps.LatLng(13.72016,100.680915), 
            type: 'info',
            content: '<h5>สำนักงานรถเช่าสาขา อ่อนนุช (รถเช่าอ่อนนุช)</h5>ตั้งอยู่เลขที่ 268/13 ถนนอ่อนนุช แขวงประเวศ เขตประเวศ  กรุงเทพมหานคร 10250<br>เบอร์โทร 02-720-0280<br>มือถือ 094-512-1689'
          },
           {
            position: new google.maps.LatLng(13.718865,100.454871),  
            type: 'info',
            content: '<h5>สำนักงานรถเช่าสาขาบางหว้า (รถเช่าบางหว้า)</h5>ตั้งอยู่เลขที่ 9 ซอยเพชรเกษม 25/5 แขวงบางหว้า เขตภาษีเจริญ  กรุงเทพมหานคร 10160<br>เบอร์โทร 02-045-5580<br>มือถือ 098-828-9808'
          },
          {
            position: new google.maps.LatLng(13.777351,100.624765,10),  
            type: 'info',
            content: '<h5>สำนักงานรถเช่าสาขาลาดพร้าว (รถเช่าลาดพร้าว)</h5>ตั้งอยู่เลขที่ 2438 ถนนลาดพร้าว แขวงพลับพลา เขตวังทองหลาง  กรุงเทพมหานคร 10310<br>เบอร์โทร 02-030-5880<br>มือถือ 062-3489900'
          },
          {
            position: new google.maps.LatLng(13.861161,100.509933,13),  
            type: 'info',
            content: '<h5>สำนักงานรถเช่าสาขานนทบุรี (รถเช่านนทบุรี)</h5>ตั้งอยู่เลขที่ 160/1 ถนนรัตนาธิเบศร์ ซอย 18 ต.บางกระสอ อ.เมืองนนทบุรี จ.นนทบุรี 11000<br>เบอร์โทร 02-035-5880<br>มือถือ 062-3489900'
          },
          {
            position: new google.maps.LatLng(13.6537822,100.5993279,13),  
            type: 'info',
            content: '<h5>สำนักงานรถเช่าสาขาสมุทรปราการ (รถเช่าสำโรง)</h5>ตั้งอยู่เลขที่ 1199 ถนนสุขุมวิท ตำบลสำโรงเหนือ อ.เมืองสมุทรปราการ จ.สมุทรปราการ 10270<br>เบอร์โทร 02-049-1180<br>มือถือ 098-828-9808'
          },
          {
            position: new google.maps.LatLng(15.243653,104.86623,11),
            type:'info',
            content: '<h5>สำนักงานสาขาอุบลราชธานี(รถเช่าอุบล)</h5>ตั้งอยู่เลขที่ 456/9 ถนนเทพโยธี ตำบลในเมือง อำเภอเมือง จังหวัดอุบลราชธานี 34000<br>เบอร์โทร 045-956-754<br>มือถือ 061-6485-438'
          },
          {
            position: new google.maps.LatLng(18.770019,98.96801,16),
            type: 'info',
            content:'<h5>สำนักงานสาขาเชียงใหม่(รถเช่าเชียงใหม่)</h5>ตั้งอยู่เลขที่ 92 ถนนสมโภชเชียงใหม่ 700 ปี ตำบลฟ้าฮ่าม อำเภอเมืองเชียงใหม่ จังหวัดเชียงใหม่ 50000<br>เบอร์โทร 06-3187-5726<br>มือถือ 06-3187-5726'
          },
          {
            position: new google.maps.LatLng(12.923093,100.883794,13),
            type: 'info',
            content: '<h5>สำนักงานสาขาพัทยา (รถเช่าพัทยา)</h5>ตั้งอยู่เลขที่  388/9 ถนนสุขุมวิท ตำบลหนองปรือ อำเภอบางละมุง จังหวัดชลบุรี 20150<br>เบอร์โทร 06-3187-5726<br>มือถือ 06-3187-5726'
          }
        ];

         var infowindow = new google.maps.InfoWindow();

         var marker, i;

            // Create markers.

            features.forEach(function(feature) {
                marker = new google.maps.Marker({
                    position: feature.position,
                    icon: icons[feature.type].icon,
                    map: map
                });

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                      infowindow.setContent(feature.content);
                      infowindow.open(map, marker);
                    }
                })(marker, i));

        });

      }

</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDLix1uVylT-bVTMkCqgRPuuEqUhwMYRc&callback=initMap"></script>
@endsection
