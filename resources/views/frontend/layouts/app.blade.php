<!DOCTYPE html>
@langrtl
    <html lang="{{ app()->getLocale() }}" dir="rtl">
@else
    <html lang="{{ app()->getLocale() }}">
@endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0' >
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title','เช่ารถ กับบริษัท รถเช่า อันดับ 1 | ECOCAR Rent-A-Car')</title>
        <link rel="icon" href="{{ asset('img/icon/icon-ecocar.ico') }}">
        <meta name="description" content="@yield('meta_description', 'รถเช่า ECOCAR rent-a-car คือ บริษัทรถเช่าอันดับ 1 ในกรุงเทพ ที่มีสาขามากที่สุด 6 สาขา ได้แก่ ดอนเมือง อ่อนนุช บางหว้า ลาดพร้าว สำโรงและนนทบุรี.')">
        <meta name="keywords" content="@yield('keywords','เช่ารถ,รถเช่า,บริการรถเช่า,บริษัทรถเช่า')">
        <meta name="author" content="@yield('meta_author', 'Ecocar')">

        <meta property="og:url" content="@yield('url','http://www.ecocar.co.th/')">
        <meta property="og:type" content="article">
        <meta property="og:title" content="@yield('title', 'เช่ารถ กับบริษัท รถเช่า อันดับ 1 | ECOCAR Rent-A-Car')">
        <meta property="og:description" content="@yield('description','รถเช่า ECOCAR rent-a-car คือ บริษัทรถเช่าอันดับ 1 ในกรุงเทพ ที่มีสาขามากที่สุด 6 สาขา ได้แก่ ดอนเมือง อ่อนนุช บางหว้า ลาดพร้าว สำโรงและนนทบุรี.')">
        <meta property="og:image"  content="@yield('image','https://static1-velaeasy.readyplanet.com/www.thairentecocar.com/images/background/header/crop-1535513492381.jpg')" />

        <meta property="og:locale" content="th_TH">
        @yield('meta')

        <!-- GOOGLE FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
        <!-- /GOOGLE FONTS -->

        <link rel="stylesheet" href="{{ asset("frontend/css/bootstrap.css")}}">
        <link rel="stylesheet" href="{{ asset("frontend/css/font-awesome.css")}}">
        <link rel="stylesheet" href="{{ asset("frontend/css/icomoon.css")}}">
        <link rel="stylesheet" href="{{ asset("frontend/css/styles.css")}}">
        <link rel="stylesheet" href="{{ asset("frontend/css/mystyles.css")}}">
        <link rel="stylesheet" type="text/css" href="{{ asset("frontend/css/ecocar.css")}}">
        <link rel="stylesheet" href="{{ asset("frontend/plugin/date_respon/pickadate/themes/default.css")}}">
        <link rel="stylesheet" href="{{ asset("frontend/plugin/date_respon/pickadate/themes/default.date.css")}}">
        <link rel="stylesheet" href="{{ asset("frontend/plugin/date_respon/pickadate/themes/default.time.css")}}">
     
        <script src="{{ asset("frontend/js/jquery.js")}}"></script>
        <link href="{{ asset("plugins/jnotify/jNotify.jquery.css")}}" rel="stylesheet" >


      
        @stack('before-styles')
       <!--  {{ style(mix('css/frontend.css')) }} -->
        @stack('after-styles')
        @yield('begin_css')
    </head>
    <body>

      
                
    <style type="text/css">
        .alert {
            text-align: center;
            margin-bottom:0px;
        }
        div#top-header, header#main-header {
            position: fixed;
            z-index: 999;
            width: 100%;
        }
        .top-area.show-onload{
        margin-top: 130px;
        }
        header#main-header {
            top: 42px;
        }
        @media (max-width: 767px){
            header#main-header {
                top: 0;
            }
            .slimmenu-menu-collapser {
                padding-bottom: 85px;
            }
        }
        @media (max-width: 576px){
            .slimmenu-menu-collapser {
                padding-bottom: 68px;
            }
        }

    </style>
    <script>
        jQuery(document).ready(function(){
            jQuery("#icon-close-mobile").click(function(){
                    jQuery(".pick-up-box").removeClass("pick-up-box-open");
                    jQuery('body').css('overflow-y','auto');
                    jQuery(".top-area.show-onload,.container-fluid,.section-video,.section-profile,.call-now,.box-news-ecocar,.btn-allnews").css('opacity','1');
            });

            
            
        });
    </script>

        <div id="app">
            @include('frontend.includes.header')
            @include('includes.partials.logged-in-as')
            <!-- @include('frontend.includes.nav') -->  
                @include('includes.partials.messages')
                <div id="abcdef">
                @yield('content')
                </div>
             @include('frontend.includes.footer')
        </div><!-- #app -->

        <!-- Scripts -->
        @stack('before-scripts')
        {!! script(mix('js/frontend.js')) !!}
        @stack('after-scripts')

        @include('includes.partials.ga')
           <script src="{{ asset("plugins/jquery-1.11.js")}}"></script>
        <script src="{{ asset("frontend/js/bootstrap.js")}}"></script>
        <script src="{{ asset("frontend/js/slimmenu.js")}}"></script>
        <script src="{{ asset("frontend/js/bootstrap-datepicker.js")}}"></script>
        <script src="{{ asset("frontend/js/bootstrap-timepicker.js")}}"></script>
        <script src="{{ asset("frontend/js/nicescroll.js")}}"></script>
        <script src="{{ asset("frontend/js/dropit.js")}}"></script>
        <script src="{{ asset("frontend/js/ionrangeslider.js")}}"></script>
        <script src="{{ asset("frontend/js/icheck.js")}}"></script>
        <script src="{{ asset("frontend/js/fotorama.js")}}"></script>
        <script src="{{ asset("frontend/js/typeahead.js")}}"></script>
        <script src="{{ asset("frontend/js/card-payment.js")}}"></script>
        <script src="{{ asset("frontend/js/magnific.js")}}"></script>
        <script src="{{ asset("frontend/js/owl-carousel.js")}}"></script>
        <script src="{{ asset("frontend/js/fitvids.js")}}"></script>
        <script src="{{ asset("frontend/js/tweet.js")}}"></script>
        <script src="{{ asset("frontend/js/countdown.js")}}"></script>
        <script src="{{ asset("frontend/js/gridrotator.js")}}"></script>
        <script src="{{ asset("frontend/js/custom.js")}}"></script>
        <script src="{{ asset("frontend/js/mobiscroll.jquery.min.js")}}"></script>
        <script src="{{ asset("frontend/plugin/date_respon/pickadate/picker.js")}}"></script>
        <script src="{{ asset("frontend/plugin/date_respon/pickadate/picker.date.js")}}"></script>
        <script src="{{ asset("frontend/plugin/date_respon/pickadate/picker.time.js")}}"></script>
        <script src="{{ asset("frontend/plugin/date_respon/pickadate/legacy.js")}}"></script>
           <script src="{{ asset("plugins/jnotify/jNotify.jquery.min.js")}}"></script>


         @yield('begin_javascript')
<style  type="text/css">
    table#u_0_0 > tbody > tr > td >table > tbody > tr > td::nth-child(3){
            display:none !important;
        }

        span#u_0_6 {
    display: none;
}

</style>
    </body>
</html>
