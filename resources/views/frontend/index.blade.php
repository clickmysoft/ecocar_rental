@extends('frontend.layouts.app')
@section('title', app_name() . ' | บริษัท ไทยเร้นท์อีโก้คาร์ จำกัด รถเช่าที่คุณวางใจ')

@section('begin_css')
    <style type="text/css">
         #owl-demo .item{
          margin: 3px;
        }
        #owl-demo .item img{
          display: block;
          width: 100%;
          height: auto;
        }
       .title-pickup-search{
          font-size: 14px;
    color: #134e26;
    padding-top: -18px !important;
    top: 0px !important;
    position: absolute;
    z-index: 99999;
    /* margin: 0 46px; */
    font-weight: 300;
    padding-top: 3px;
    z-index: 1;
    text-indent: 46px;
       }

       .search-tabs-bg > .tabbable >.tab-content > .tab-pane .form-control:hover{
        cursor: pointer;
       }
       .title-mobile{
            display: none;
        }
    @media (max-width: 576px) {
        .title-mobile{
            display: block;
        }

        h1 {
            font-size: 2.0rem !important;
        }
        h2 {
            font-size: 2.0rem !important;
        }
        h3 {
            font-size: 2.0rem !important;
        }

        .title-pickup-search{
            font-size: 14px;
            color: #134e26;
            padding-top: -18px !important;
            top: 0px !important;
            position: absolute;
            z-index: 99999;
            margin: 0px 33px;
            font-weight: 300;
            padding-top: 3px;
            z-index: 1;
            width: 100px;
            display: none;
        }
        input#drop-car-point{
            padding-left: 32px;
            font-size: 11px;
            text-transform: uppercase;
        }
        input#pick-up-point {
            padding-left: 32px;
            font-size: 11px;
            border-radius: 0 !Important;
            text-transform: uppercase;
        }
        
        input.datepicker_booking {
            padding-left: 32px !important;
            font-size: 11px !important;
            text-transform: uppercase;
        }
        input.time_booking {
            padding-left: 32px !important;
            font-size: 11px !important;
            text-transform: uppercase;
        }
        button.btn.btn-primary.btn-lg.search-btn{
            left: -15px !important;
            position: relative;
        }
        .form-group.form-group-lg .form-control.textbox-pickup{
            padding-top: 10px;
        }
        .form-group.form-group-lg .form-control.textbox-dropcar{
            padding-top: 10px;
        }
        input.time_booking{
            padding-top: 10px !Important;
        }
        input.datepicker_booking{
            padding-top: 10px !important;
        }
        i.fa.fa-calendar.input-icon.input-icon-highlight {
            color: #2b6846;
        }
        i.fa.fa-clock-o.input-icon.input-icon-highlight{
            color: #2b6846;
        }
        i.fa.fa-map-marker.input-icon.home{
            color: #2b6846;
        }
        
       }
    button.btn.btn-primary.btn-lg.search-btn {
    background: #fccb37;
    border-color: transparent;
    color: #333333;
    width: 150px;
    }

    @media (max-width: 576px){
    input.datepicker_booking_off {
           color: rgb(0, 0, 0);
        padding-top: 11px !important;
        padding-left: 32px !important;
        font-size: 11px !important;
        text-transform: uppercase;
            border-radius: 0;
    }
    .form-control{
        border:0px !important;
    }

    .form-group.form-group-lg{
        border-right: 1px solid;
    }
}

.alert.alert-success {
    padding-top: 150px;
}

</style>

@endsection

@section('content')

<?php
    $array = Lang::get('branch');
?>
    <!-- TOP AREA -->
                        <div class="container">
                        <div class="row">
                                        <div class="col-md-12">
                                                <div class="pick-up-box">
                                                    <div class="hidden-md hidden-lg">
                                                        <p class="text-center" style="font-size: 26px; color: #000; padding-top: 30px;"> เลือกสถานที่ </p>
                                                        <i class="fa fa-times fa-2x" id="icon-close-mobile" aria-hidden="true" style="right: 30px;float:  right;position: fixed;top: 25px;color: #000;"></i>
                                                    </div>
                                                    <?php
                                                        foreach ($array as $key => $value) {
                                                    ?>
                                                        <div class="col-md-3 pickup-branch-box" id="box-branch-<?php echo $value['code'];?>">
                                                            <label class="title-pickup" id="label-pickup-1" ><?php echo $value['name'];?></label>
                                                        </div>
                                                    <?php
                                                        }
                                                    ?>
                                                    </div>
                                                </div>
                                        </div>
                                </div>
        <div class="top-area show-onload">
            <div class="bg-holder full">
                <div class="bg-front bg-front-mob-rel">
                    <div class="container">
                    <div class="row">
                    <div class="col-md-12">
                        <div class="search-tabs search-tabs-bg search-tabs-abs-bottom">
                            <div class="tabbable">
                                <ul class="nav nav-tabs" id="myTab">
                                    <li class="active"><a href="" data-toggle="tab-4" class="tab-4-style"><i class="fa fa-car"></i> <span >
                                        {{ __('index.frontend.booking') }}</span></a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="tab-4">
                                          <form method="POST" action="{{ route('frontend.vehicles.find') }}"   id="my-form">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                    
                                                        <div class="col-xs-6 col-md-6" id="col-pickup">
                                                            <span class="title-mobile" style="color: #fff;">{{ __('index.frontend.pick_up') }}</span>
                                                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon home"></i>
                                                                <p class="title-pickup-search">{{ __('index.frontend.pick_up') }}</p>
                                                                <input class="typeahead form-control textbox-pickup" style="color: #000"  placeholder="{{ __('index.frontend.select_pick_up') }}" type="text"  readonly="true" name="pick-up-point" id="pick-up-point" />
                                                                <input type="hidden" name="pickup" value="">
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6 col-md-6" id="col-dropup">
                                                            <span class="title-mobile" style="color: #fff;">{{ __('index.frontend.drop_off') }}</span>
                                                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon home"></i>
                                                            <p class="title-pickup-search">{{ __('index.frontend.drop_off') }}</p>
                                                                <input class="typeahead form-control textbox-dropcar" style="color: #000" placeholder="{{ __('index.frontend.select_drop_off') }}" type="text"  readonly="true" name="drop-car-point" id="drop-car-point" />
                                                                <input type="hidden" name="dropoff" value=""> 
                                                            </div>
                                                        </div>

                                                    </div>

                                                     <div class="row">
                                                        <div data-date-format="M d, D">
                                                            <div class="col-xs-6 col-md-3" id="col-pick-date">
                                                                <span class="title-mobile" style="color: #fff;">{{ __('index.frontend.pick_up_date') }}</span>
                                                               <p class="title-pickup-search">{{ __('index.frontend.pick_up_date') }}</p>
                                                                <div class="form-group form-group-lg form-group-icon-left">
                                                                    <i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                                    <input style="padding-top: 26px;color:#000;"  class="datepicker_booking form-control carlenda_txt" name="pick_up_date" type="text"  placeholder="{{ __('index.frontend.select_pick_up_date') }}"/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6 col-md-3" id="col-pick-time">
                                                              <span class="title-mobile" style="color: #fff;">{{ __('index.frontend.pick_up_time') }}</span>
                                                            <p class="title-pickup-search">{{ __('index.frontend.pick_up_time') }}</p>
                                                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-clock-o input-icon input-icon-highlight"></i>
                                                            <input style="padding-top: 26px;color:#000;"  name="pick_up_time" class="datepicker_booking_up_time  form-control time_booking"  type="text" placeholder="{{ __('index.frontend.select_pick_up_time') }}" />
                                                            </div>
                                                        </div>

                                                        <div  data-date-format="M d, D">
                                                            <div class="col-xs-6 col-md-3" id="col-drop-date">
                                                                <span class="title-mobile" style="color: #fff;">{{ __('index.frontend.drop_off_date') }}</span>
                                                            <p class="title-pickup-search">{{ __('index.frontend.drop_off_date') }}</p>
                                                                <div class="form-group form-group-lg form-group-icon-left">
                                                                <i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                                <input style="padding-top: 26px;color:#000;" id="datepicker_booking_off" class="datepicker_booking_off form-control carlenda_txt" name="drop_off_date" type="text"  placeholder="{{ __('index.frontend.select_drop_off_date') }}"/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6 col-md-3" id="col-drop-time">
                                                            <span class="title-mobile" style="color: #fff;">{{ __('index.frontend.drop_off_time') }}</span>
                                                         <p class="title-pickup-search">{{ __('index.frontend.drop_off_time') }}</p>
                                                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-clock-o input-icon input-icon-highlight"></i>
                                                            <input style="padding-top: 26px;color:#000;"  class="datepicker_booking_off_time  form-control time_booking" name="drop_off_time" type="text" placeholder="{{ __('index.frontend.select_drop_off_time') }}"/>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div style="text-align:right;">
                                                <button class="btn btn-primary btn-lg search-btn" type="submit" style="padding-left: 0 !important;" >
                                                    <i class="fa fa-search" aria-hidden="true"></i>{{ __('index.frontend.search') }}
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>              
               <!-- นำเข้าภาพไลค์ -->
                @include('frontend.includes.slide_homepage')
                <div class="bg-img hidden-lg" style="background-image: url(../frontend/img//bg-city-slider02.jpg);"></div>
                <div class="bg-mask hidden-lg"></div>
            </div>
        </div>
        <!-- END TOP AREA  -->
        <div class="section-profile" style="background:#fff;">
            <div class="container">
            <div class="row row-wrap">
                <h1 style="text-align: center;text-transform: uppercase;padding-top:5%;    font-size: 3.5rem;">ECOCAR Rent-A-Car&nbsp;&nbsp;{{ __('index.frontend.text_h1') }}<h1>

            </div>
            <div class="gap gap-small"></div>
             <p class="line-bottom"></p>
                <br/>
                <p style="font-size: 22px; text-align: center;">{{ __('index.frontend.text1') }}<br/>{{ __('index.frontend.text2') }}</p> <br>
                <div class="col-md-4 s-3-service">
                 <div class="border-icon-main">
                    <svg class="svg-service" fill="#054b20" width="80px" height="80px" viewBox="0 0 1024 1024"><path d="M951.8 468c-28.8-32.6-82.4-61.6-82.4-61.6l31-6.2c0 0 0 0 0 0 14.2 0 44.8-4.6 51-10.8 6.2-6.4 8.4-10.8 8.4-16.4s-3.4-15.4-9.6-22.2c-6-6.8-32-10.4-47.4-12.4-15.2-2-17.4 0-21.4 2.4-4.6 3-6 23.4-6.4 34l-16.2 3.6c-9.6-25-22.8-71.2-43-108-23-41.6-47-54.8-57-58-9.8-3-18.8-5.2-86-12.2-67.4-7.2-126-8.2-160.8-8.2s-93.4 1-161.2 8.2c-67.2 6.8-76.2 9-86 12.2-10 3.2-34 16.4-57 58-20.2 36.8-33.4 83-43 108l-16.2-3.6c-0.4-10.6-1.8-31.2-6.4-34-4-2.6-6-4.4-21.4-2.4-15.2 2-41.2 5.6-47.4 12.4-6 6.8-9.4 16.6-9.4 22.2s2.2 10.2 8.4 16.6c6.2 6.4 37 10.8 51 10.8 0 0 0 0 0 0l31 6.2c0 0-53.6 29.2-82.4 61.6 0 0-8.2 49.4-8.2 116.2 0 96 11.6 176.8 11.6 176.8 53 9.2 92.4 12.6 131.8 13.6 19.8 0.4 37.8 0.4 58.4 0.2 49.6-0.6 31.2-15 54.2-14.6 22.6 0.4 110 8 192 8s169.4-7.6 192-8c23-0.4 4.6 14.2 54.2 14.6 20.6 0.2 38.8 0.2 58.4-0.2 39.4-1 79-4.4 131.8-13.6 0 0 11.6-80.6 11.6-176.8 0.2-67.2-8-116.4-8-116.4zM220.8 309.8c8.6-20.2 28-54.2 44.2-65.6 0 0 94-20.2 247-20.2s247 20.2 247 20.2c16.2 11.2 35.6 45.2 44.2 65.6 8.6 20.2 22.8 67.6 20.6 73s2.2 8.2-26.8 5.8c-28.8-2.2-199-4.6-285-4.6s-256.2 2.4-285 4.6c-29 2.4-24.8-0.4-26.8-5.8-2.2-5.6 12-52.8 20.6-73zM247.6 541.6c-19.8 0-59.6-2-69-2.4-9.4-0.2-17.6 7.6-22.4 7.6s-51-7.2-56-29.8c-5-22.4 0-46 0-45.6 31-1.4 61-0.4 117 15.6 56.2 15.8 87.2 46.6 87.2 46.6s-37 8-56.8 8zM684.4 679.6c-25.4 3.4-117 4.4-172.4 4.4s-147-1-172.4-4.4c-26.2-3.4-59.6-34.6-36.8-60.4 15.2-17 41.6-27.2 97.8-34.6 59.6-7.6 97.2-8.6 111.2-8.6s51.6 1 111.2 8.6c56.2 7.4 86.4 19.2 97.8 34.6 20.8 27.6-10.2 56.8-36.4 60.4zM923.8 517.2c-5 22.4-51.2 29.8-56 29.8s-13-7.8-22.4-7.6c-9.4 0.4-49.2 2.4-69 2.4s-56.8-8-56.8-8 31-30.8 87.2-46.6c56-16 86-17 117-15.6-0-0.4 5 23-0 45.6z M802 824.6c0 4.2 2.4 7.4 6.2 7.4 0 0 106 0 113 0 9.8 0 11.8-7 12.4-11 2.8-16.8 4-42.6 4-42.6-51.4 9.4-97.6 11-135.6 12v34.2z M90.2 821c0.6 4 2.6 11 12.4 11 7 0 113.4 0 113.4 0 3.8 0 6.2-3.2 6.2-7.4v-34.2c-38-1-84.4-2.6-135.8-12-0.2 0.2 1 25.8 3.8 42.6z"></path></svg>
                    </div>
                    <h4 class="title-service">{{ __('index.frontend.text_line1') }}</h4>
                    <p class="line-bottom"></p>

                </div>
                <div class="col-md-4 s-3-service">
                    <div class="border-icon-main">
                        <svg class="svg-service" fill="#054b20" width="80px" height="80px" viewBox="0 0 1024 1024"><path d="M512 576c141.4 0 256-114.6 256-256s-114.6-256-256-256-256 114.6-256 256 114.6 256 256 256zM512 144c97 0 176 79 176 176s-79 176-176 176-176-79-176-176 79-176 176-176z M512 464c79.4 0 144-64.6 144-144s-64.6-144-144-144-144 64.6-144 144 64.6 144 144 144z M512 608c-87 0-165-38.6-217.8-99.6l-166.2 291.6h192l96 160 96-211.6 67.2-148.4c-21.6 5.2-44 8-67.2 8z M729.8 508.4c-30.2 34.8-68.6 62.4-112 79.6l-89.6 196.2 79.8 175.8 96-160h192l-166.2-291.6z"></path></svg>
                    </div>
                    <h4 class="title-service">{{ __('index.frontend.text_line2') }}</h4>
                    <p class="line-bottom"></p>
                    <!-- <p style="line-height: 2em">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p> -->
                </div>
                <div class="col-md-4">
                 <div class="border-icon-main">
                   <svg class="svg-service" fill="#054b20" width="80px" height="80px" viewBox="0 0 1024 1024"><path d="M64 192v640h896v-640h-896zM860 511.6c0 41.8-2.2 74.4-5.6 123.8l-1 15.6c-3.8 55.6-32.8 85.2-89 90.4-64.4 6-153.8 6.6-224.6 6.6-9.4 0-18.6 0-27.8 0v0c-9.2 0-18.4 0-27.8 0-70.8 0-160.4-0.8-224.6-6.6-56-5.2-85.2-34.8-89-90.4l-1-15.4c-3.4-49.4-5.6-82-5.6-124 0-47 0.2-76.6 6.6-138.8 5.8-56.2 34-85 89.2-90.6 62.4-6.4 172.2-6.4 252.2-6.4 80.2 0 190 0 252.4 6.4 55 5.6 83.4 34.4 89.2 90.6 6.2 62.2 6.6 91.8 6.4 138.8z M675.4 324.8c-21.8 0-41.8 2.8-60 8.6s-34 15.8-47.2 30.2c-13.2 14.4-23.4 33.6-30.8 57.8-7.2 24.2-11 54.6-11 91.4 0 36 3 66 8.8 90.2 6 24.2 14.6 43.4 26.2 57.8s26.2 24.4 43.6 30c17.6 5.6 38.2 8.4 61.8 8.4 50 0 86-12.8 107.6-37.4s32.4-60.6 32.4-107.8h-92.2c0 0 0 5 0 6.8v1.4c0 32.6-20.2 51.8-47.2 51.8s-45.2-21.6-47.8-51.8c0 0-2.4-15.8-2.4-47.8s2.8-52 2.8-52c4.8-34 21.4-51.8 48.4-51.8 26.8 0 48.2 23.2 48.2 58.4 0 0.2 0 1 0 1h90.2c0-43.8-11-83.2-33.2-108-21.6-24.8-54.4-37.2-98.2-37.2v0z M365.6 324.8c-21.8 0-41.8 2.8-60 8.6s-34 15.8-47.2 30.2c-13.2 14.4-23.4 33.6-30.8 57.8-7.2 24.2-11 54.6-11 91.4 0 36 3 66 8.8 90.2 6 24.2 14.6 43.4 26.2 57.8s26.2 24.4 43.6 30c17.6 5.6 38.2 8.4 61.8 8.4 50 0 86-12.8 107.6-37.4s32.4-60.6 32.4-107.8h-92.2c0 0 0 5 0 6.8v1.4c0 32.6-20.2 51.8-47.2 51.8s-45.2-21.6-47.8-51.8c0 0-2.4-15.8-2.4-47.8s2.8-52 2.8-52c4.8-34 21.4-51.8 48.4-51.8 26.8 0 48.2 23.2 48.2 58.4 0 0.2 0 1 0 1h90.2c0-43.8-11-83.2-33.2-108-21.6-24.8-54.4-37.2-98.2-37.2v0z"></path></svg>
                    </div>
                    <h4 class="title-service">{{ __('index.frontend.text_line3') }}</h4>
                    <p class="line-bottom"></p>
                    <br><br><br>
                   <!--  <p style="line-height: 2em">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p> -->
                </div>
        </div>
    </div>    
<div class="container-fluid">
    <div class="row">
   <div class="col-md-6 hidden-sm hidden-xs" style="background-image: url(../frontend/img/backgrounds/shutterstock_424578970.jpg); background-size: cover; height: 500px;">
   </div>
   <div class="col-md-6" > 
          <div class="row">
                <div id="map" style="width: 100%; height: 500px;"></div>
                </div>
            </div>      
        </div>
   </div>
</div>
    <div class="section-video" style="background:#fff !important; ">
        <div class="container">
            <div class="row">
            <div class="gap"></div>
                <h2 style="text-align: center;text-transform: uppercase;font-size: 3.5rem;">ECOCAR Rent-A-Car &nbsp;&nbsp;{{ __('index.frontend.text_line4') }}</h2>
                <h3 class="text-center">{{ __('index.frontend.text_ecocar') }}</h3><br/>
                   <div class="col-md-6 col-sm-6 section-video-v1">   
                    <iframe class="video" width="600" height="500" src="https://www.youtube.com/embed/JPvAzuwk6Mk" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                    </div>
                    <div class="col-md-6 col-sm-6">
                    <iframe class="video" width="600" height="500" src="https://www.youtube.com/embed/BkCqkdawjtg" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                   </div>
                  <div class="col-md-12 col-sm-12">
                    <hr class="line-section-video" />
                  </div>    
             </div>
      </div>
   </div>
<div class="section-video" style="background:#fff !important; ">
    <div class="container">
                <div class="gap-small"></div>
                <h3>{{ __('index.frontend.blog') }}</h3>
                <p class="line-bottom-left"></p>
                <div class="row row-wrap">

                    <?php
                        foreach ($article as $key => $value) {
                            if($key <4){
                    ?>
                            <div class="col-md-3 col-sm-6">
                                <div class="thumb box-news-ecocar">
                                    <header class="thumb-header">
                                        <a class="hover-img curved" href="<?php echo route('frontend.blog.show',$value['id']) ?>">
                                           
                                            <img src="<?=asset("frontend/article/".@$value['code'].'/files/'.$value['image_thumbnail'])?>" alt="{{ $value['mata_keyword'] }}" title="{{ $value['mata_title'] }}">
                                        </a>
                                    </header>
                                        <div class="thumb-caption">
                                        <h5 class="thumb-title"><a class="text-darken" href="<?php echo route('frontend.blog.show',$value['id']) ?>">{{ $value->title }}</a></h5>
                                        <div class="thumb-caption">
                                            <p class="thumb-desc">&nbsp;&nbsp;{{ str_limit(strip_tags($value->description),100) }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <?php
                            }
                        }
                    ?>

                    <div class="col-md-12">
                    <div class="gap-small"></div>
                         <a href="<?php echo route('frontend.blog.index')?>" class="btn btn-lg btn-allnews">{{ __('index.frontend.read_more') }}</a>  
                    </div>
                </div>
</div>

  <div class="call-now" style="background-image: url(../frontend/img/backgrounds/bg-call-now.jpg); background-position: 80% 50%; border-bottom: 2px solid #00ad40;">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <h4>CALL CENTER : 098-828-9808&nbsp;&nbsp;{{ __('index.frontend.open_24') }}</h4>
                </div>
                <div class="col-md-2 center-sm right-sm" >
                    <a href="http://line.me/ti/p/~@ecocar" class="btn btn-lg btn-callnow" style="    background: #fccb37;color: #333333;">{{ __('index.frontend.call_us') }}</a>
                </div>
            </div>          
        </div>
        </div>
@endsection

@section('begin_javascript')
 <script>
        jQuery(document).ready(function(){
            jQuery("div.pickup-branch-box").click(function(){
                var x = jQuery(this).text();
                var s = x.split(" ");

                if(jQuery(".pick-up-box").hasClass('state-pickup')){
                    var h = $(this).attr('id');
                    jQuery("input[name^='pick-up-point']").val(s[0].trim());
                    jQuery("input[name^='drop-car-point']").val(s[0].trim());           
                    
                    jQuery("input[name^='pickup']").val(h);
                    jQuery("input[name^='dropoff']").val(h);
                    jQuery(".pick-up-box").removeClass("pick-up-box-open");
                }else{
                    var h = $(this).attr('id');
                    jQuery("input[name^='dropoff']").val(h);
                    jQuery("input[name^='drop-car-point']").val(s[0].trim());
                    jQuery(".pick-up-box").removeClass("pick-up-box-open");
                }

                jQuery('body').css('overflow-y','auto');
                jQuery(".top-area.show-onload,.container-fluid,.section-video,.section-profile,.call-now,.box-news-ecocar,.btn-allnews").css('opacity','1');
            });
            jQuery("#pick-up-point").click(function(){
                    jQuery('body').css('overflow-y','hidden');
                 
                    jQuery(".pick-up-box").addClass("pick-up-box-arrow state-pickup");
                    jQuery(".pick-up-box").removeClass("pick-up-box-arrow-right state-dropup");

                    jQuery(".pick-up-box").addClass("pick-up-box-open");
                    jQuery(".pick-up-box").removeClass("pick-up-box-open");
                    setTimeout(function(){
                         jQuery(".pick-up-box").addClass("pick-up-box-open");
                    }, 100);
            });
            jQuery("#drop-car-point").click(function(){
                jQuery('body').css('overflow-y','hidden');

                jQuery(".pick-up-box").addClass("pick-up-box-arrow-right state-dropup");
                jQuery(".pick-up-box").removeClass("pick-up-box-arrow state-pickup");

                jQuery(".pick-up-box").addClass("pick-up-box-open");
                jQuery(".pick-up-box").removeClass("pick-up-box-open");
                setTimeout(function(){
                         jQuery(".pick-up-box").addClass("pick-up-box-open");
                }, 100);

            });

        });
    </script>


        
<script type="text/javascript">
        jQuery(function( $ ){
            //borrowed from jQuery easing plugin
            //http://gsgd.co.uk/sandbox/jquery.easing.php
            $.easing.elasout = function(x, t, b, c, d) {
                var s=1.70158;var p=0;var a=c;
                if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
                if (a < Math.abs(c)) { a=c; var s=p/4; }
                else var s = p/(2*Math.PI) * Math.asin (c/a);
                return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
            };  
            
            $('input#pick-up-point,input#drop-car-point').click(function(){
                $(".top-area.show-onload,.container-fluid,.section-video,.section-profile,.call-now,.box-news-ecocar,.btn-allnews").css('opacity','0.5');
                $(".global-wrap").css('background','#333');

                $.scrollTo('#myTab', {duration:500} );
            });

            jQuery(".section-profile,#owl-carousel-slider").click(function(){
                if(jQuery(".pick-up-box").hasClass('pick-up-box-open')){
                    jQuery(".pick-up-box").removeClass("pick-up-box-open");
                    jQuery(".top-area.show-onload,.container-fluid,.section-video,.section-profile,.call-now,.box-news-ecocar,.btn-allnews").css('opacity','1');
                    $.scrollTo('#top-header', {duration:500} );
                }
            });

            // jQuery( window ).resize(function() {
            //    if(jQuery(".slimmenu-menu-collapser").css('display') == 'block'){

            //        jQuery('input.form-control.carlenda_txt').attr('id','demo-header');
            //    }else{
            //       jQuery('input.form-control.carlenda_txt').removeAttr('id');
            //    }
            // });

             var d = new Date();
             var date = d.getDate();
             var month = d.getMonth();
             var year = d.getFullYear();
            
            var $input = $('.datepicker_booking').pickadate({
                format: 'd mmmm yyyy',

                <?php
                    if(app()->getLocale() == 'th'){
                ?>
                    monthsFull: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
                    weekdaysShort: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],

                <?php
                    }else{
                ?>
                    monthsFull: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    weekdaysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                <?php
                    }
                ?>
                


                formatSubmit: 'yyyy/mm/dd',
                container: '#top-header',
                // editable: true,
                closeOnSelect: true,
                closeOnClear: true,
                min:[year,month,date],
                onSet: function(context) {
                   //x =  new Date(context.select);
                   var now_picup = '';
                  now_picup = this.get('select', 'yyyy-mm-dd');

                  //console.log(this.get('select', 'yyyy-mm-dd'));
                  var temp = new Array();
                  temp =  now_picup.split("-");

                  temp[2]++;
                  $('.datepicker_booking_off').pickadate('set').set('formatSubmit','yyyy/mm/dd');
                  $('.datepicker_booking_off').pickadate('set').set('closeOnSelect',true);
                  $('.datepicker_booking_off').pickadate('set').set('closeOnClear',true);
                  $('.datepicker_booking_off').pickadate('set').set('select', new Date(temp[0],--temp[1],temp[2]));
                  $('.datepicker_booking_off').pickadate('set').set('min', new Date(temp[0],temp[1],temp[2]));
                }
            })

            var $input = $('.datepicker_booking_off').pickadate({
                 format: 'd mmmm yyyy',
                 <?php
                    if(app()->getLocale() == 'th'){
                ?>
                    monthsFull: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
                    weekdaysShort: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],

                <?php
                    }else{
                ?>
                    monthsFull: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    weekdaysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                <?php
                    }
                ?>
                formatSubmit: 'yyyy/mm/dd',
                container: '#top-header',
                // editable: true,
                closeOnSelect: true,
                closeOnClear: true,
                min:[year,month,date],
            })

            
            var $input2 = $('.datepicker_booking_up_time' ).pickatime({
                min: [8,30],
                max: [22,0],
                interval:15,
                format: 'HH:i',
                onSet: function(context) {
                    var now_picup = '';
                    now_picup = this.get('select', 'HH:i');

                     var temp = new Array();
                     temp =  now_picup.split(":");

                     var picker = $('.datepicker_booking_off_time').pickatime().pickatime('picker');
                     picker.set('select', [temp[0],temp[1]]);
                }
            
            })


           $('.datepicker_booking_off_time').pickatime({
                min: [8,30],
                max: [22,0],
                interval:15,
                format: 'HH:i'
            })

        });

   
        jQuery("div.pickup-branch-box").click(function(){
            var x = jQuery(this).text();
            var s = x.split(" ");

            if(s[0].trim()=="อุบล/สนามบินอุบล") {
                jQuery("ul.picker__list > li").show();
            }else if(s[0].trim()=="เชียงใหม่/สนามบินเชียงใหม่") {
                jQuery("ul.picker__list > li").show();
            }else if (s[0].trim()=="สนามบินดอนเมือง") {
                jQuery("ul.picker__list > li").show();
            }else if (s[0].trim()=="สนามบินสุวรรณภูมิ") {
                jQuery("ul.picker__list > li").show();
            }else{
                jQuery("ul.picker__list > li").each(function(){
                    var x = jQuery(this).attr('aria-label');
                    if(x >"20:00"){
                      jQuery("ul.picker__list > li[aria-label='"+x+"']").hide();
                    }
                });
            }
        });
   

    </script>
        <script type="text/javascript">
                var map;
                    function initMap() {
                        map = new google.maps.Map(document.getElementById('map'), {
                          zoom: 11,
                          center: new google.maps.LatLng(13.8222222, 100.5999999), 
                          mapTypeId: 'roadmap'
                        });
                        var iconBase = 'https://www.picz.in.th/images/2018/02/06/';  
                        var icons = {
                          parking: {
                            icon: iconBase + 'sedan-icon-google-map.png'
                          },
                          library: {
                            icon: iconBase + 'sedan-icon-google-map.png'
                          },
                          info: {
                            icon: iconBase + 'sedan-icon-google-map.png' 
                          }
                        };
                        var features = [
                          {
                            position: new google.maps.LatLng(13.9538709, 100.6136862), 
                            type: 'info',
                            content: '<h5>สำนักงานใหญ่ รถเช่าดอนเมือง</h5>ตั้งอยู่เลขที่ 279/57 ถนนวิภาวดีรังสิต แขวงสนามบิน เขตดอนเมือง กรุงเทพมหานคร 10210<br>เบอร์โทร 02-002-4606<br>มือถือ 092-2848-660'
                          },
                          {
                            position: new google.maps.LatLng(13.72016,100.680915), 
                            type: 'info',
                            content: '<h5>สำนักงานรถเช่าสาขา อ่อนนุช (รถเช่าอ่อนนุช)</h5>ตั้งอยู่เลขที่ 268/13 ถนนอ่อนนุช แขวงประเวศ เขตประเวศ  กรุงเทพมหานคร 10250<br>เบอร์โทร 02-720-0280<br>มือถือ 094-512-1689'
                          },
                           {
                            position: new google.maps.LatLng(13.718865,100.454871),  
                            type: 'info',
                            content: '<h5>สำนักงานรถเช่าสาขาบางหว้า (รถเช่าบางหว้า)</h5>ตั้งอยู่เลขที่ 9 ซอยเพชรเกษม 25/5 แขวงบางหว้า เขตภาษีเจริญ  กรุงเทพมหานคร 10160<br>เบอร์โทร 02-045-5580<br>มือถือ 098-828-9808'
                          },
                          {
                            position: new google.maps.LatLng(13.777351,100.624765,10),  
                            type: 'info',
                            content: '<h5>สำนักงานรถเช่าสาขาลาดพร้าว (รถเช่าลาดพร้าว)</h5>ตั้งอยู่เลขที่ 2438 ถนนลาดพร้าว แขวงพลับพลา เขตวังทองหลาง  กรุงเทพมหานคร 10310<br>เบอร์โทร 02-030-5880<br>มือถือ 062-3489900'
                          },
                          {
                            position: new google.maps.LatLng(13.861161,100.509933,13),  
                            type: 'info',
                            content: '<h5>สำนักงานรถเช่าสาขานนทบุรี (รถเช่านนทบุรี)</h5>ตั้งอยู่เลขที่ 160/1 ถนนรัตนาธิเบศร์ ซอย 18 ต.บางกระสอ อ.เมืองนนทบุรี จ.นนทบุรี 11000<br>เบอร์โทร 02-035-5880<br>มือถือ 062-3489900'
                          },
                          {
                            position: new google.maps.LatLng(13.6537822,100.5993279,13),  
                            type: 'info',
                            content: '<h5>สำนักงานรถเช่าสาขาสมุทรปราการ (รถเช่าสำโรง)</h5>ตั้งอยู่เลขที่ 1199 ถนนสุขุมวิท ตำบลสำโรงเหนือ อ.เมืองสมุทรปราการ จ.สมุทรปราการ 10270<br>เบอร์โทร 02-049-1180<br>มือถือ 098-828-9808'
                          },
                          {
                            position: new google.maps.LatLng(15.243653,104.86623,11),
                            type:'info',
                            content: '<h5>สำนักงานสาขาอุบลราชธานี(รถเช่าอุบล)</h5>ตั้งอยู่เลขที่ 456/9 ถนนเทพโยธี ตำบลในเมือง อำเภอเมือง จังหวัดอุบลราชธานี 34000<br>เบอร์โทร 045-956-754<br>มือถือ 061-6485-438'
                          },
                          {
                            position: new google.maps.LatLng(18.770019,98.96801,16),
                            type: 'info',
                            content:'<h5>สำนักงานสาขาเชียงใหม่(รถเช่าเชียงใหม่)</h5>ตั้งอยู่เลขที่ 92 ถนนสมโภชเชียงใหม่ 700 ปี ตำบลฟ้าฮ่าม อำเภอเมืองเชียงใหม่ จังหวัดเชียงใหม่ 50000<br>เบอร์โทร 06-3187-5726<br>มือถือ 06-3187-5726'
                          },
                          {
                            position: new google.maps.LatLng(12.923093,100.883794,13),
                            type: 'info',
                            content: '<h5>สำนักงานสาขาพัทยา (รถเช่าพัทยา)</h5>ตั้งอยู่เลขที่  388/9 ถนนสุขุมวิท ตำบลหนองปรือ อำเภอบางละมุง จังหวัดชลบุรี 20150<br>เบอร์โทร 06-3187-5726<br>มือถือ 06-3187-5726'
                          }
                        ];

                         var infowindow = new google.maps.InfoWindow();
                         var marker, i;
                            // Create markers.
                            features.forEach(function(feature) {
                                marker = new google.maps.Marker({
                                    position: feature.position,
                                    icon: icons[feature.type].icon,
                                    map: map
                                });

                                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                    return function() {
                                      infowindow.setContent(feature.content);
                                      infowindow.open(map, marker);
                                    }
                                })(marker, i));

                        });
                    }
        </script>
        <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyC1haSCyOX7dZLLdOsCjJEe_ZPBMyVxf90&callback=initMap"></script>
@endsection
