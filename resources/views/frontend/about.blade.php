@extends('frontend.layouts.app')

@section('begin_css')
<style>
  div#abcdef {
    padding-top: 130px;
}
</style>
@endsection

@section('content')

<div class="section-bc">
        <div class="container">
            <div class="row">
        <div class="col-md-6">
        <div class="bc-left">
                <h2 class="h2-title-bc">
            {{ __('index.frontend.about_us') }}
            </h2>
        </div>
        </div>
            <div class="col-md-6">
            <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
 
    
   
  </ol>
</nav>
        </div>
        </div>
        </div>

        </div>

            
            <div class="container">
            <div class="row row-wrap">
            <div class=" text-center">
            <div class="gap"></div>       
                         
                            <!-- <div class="col-md-12">
                            <img src="img//banner-about-us.png" style="width: 300px; object-fit: contain;" />
                            </div> -->
                            <div class="gap gap-small"></div>
                                <div class="col-md-12">
                                
                                <h3>เรื่องเล่าของเรา ECOCAR rent-a-car</h3>
                                <p class="line-bottom"></p>
                                <div class="gap gap-small"></div>
                            <p> ECOCAR rent-a-car ก่อตั้งขึ้นโดย คุณอ๋า ฉัตรชัย โคตถา ในเดือนธันวาคม ปี 2555 โดยคุณอ๋า ซึ่งมีความมุ่งมั่นที่จะให้บริการเช่ารถยนต์ และสร้างแบรนด์รถเช่าของคนไทยขึ้นมาบริการอย่างมืออาชีพ เนื่องจากเราไม่มีทุนมากจึงเริ่มต้นจากการให้บริการรถเช่าเพียง 1 คัน และพนักงานคนเดียว นั่นก็คือ คุณอ๋า ซึ่งเป็นผู้ก่อตั้ง ด้วยความมุ่งมั่นและตั้งใจที่จะส่งมอบคุณค่า ในการให้บริการ และมองถึงเป้าหมายสูงสุด นั่นคือ ลูกค้ามีความพึงพอใจ สามารถเดินทาง ด้วยความสะดวกและปลอดภัย เราตั้งใจทำมาเรื่อยๆ จนปัจจุบัน (ปี 2560) เรามีรถยนต์ให้บริการประมาณ 250 คัน มีสาขาให้บริการ 8 สาขา และพนักงานมืออาชีพ ที่พร้อมให้บริการกว่า 40 คน มีเพียง 2 สิ่งที่เราตั้งใจปั้นออกมาอย่างสุดความสามารถ คือ การบริการด้วยใจ ให้ลูกค้าพึงพอใจ และการจัดเตรียมดูแลรถยนต์ ให้พร้อมสำหรับทุกการเดินทาง </p></div>
                            <div class="gap gap-small"></div>
                            <div class="col-md-12">
                            <div id="popup-gallery">
                            <div class="row row-col-gap">
                             <div class="col-md-4">
                        <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                            <img src="{{ asset("frontend/img/800x600.png")}}" alt="Image Alternative text" title="Gaviota en el Top" />
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                            <img src="{{ asset("frontend/img/800x600.png")}}" alt="Image Alternative text" title="Sydney Harbour" />
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="hover-img popup-gallery-image" href="img/800x600.png" data-effect="mfp-zoom-out">
                            <img src="{{ asset("frontend/img/800x600.png")}}" alt="Image Alternative text" title="Street" />
                        </a>
                    </div>
                        </div>
                            </div>
                                </div>

                            <div class="gap gap-small"></div>

                               <div class="col-md-12"> 
                                 <h4 class="text-center">จุดเริ่มความสำเร็จ</h4>   
                                    <p class="line-bottom"></p>   

                                    <div class="gap gap-small"></div>
                                        <div class="timeline">
                                                  <div class="container-timeline line-left">
                                                    <div class="content-timeline ">
                                                       <h4 style="color: #fff;">เดือนตุลาคม </h4> 
                                                      <p style="color: #fff;">เริ่มต้นจดโดเมนและทำเว็บไซต์ www.thairentecocar.com</p>
                                                    </div>
                                                  </div>
                                                  <div class="container-timeline line-right">
                                                    <div class="content-timeline ">
                                                      <h4 style="color: #fff;">เดือนพฤศจิกายน</h4>
                                                      <p style="color: #fff;">สร้าง Facebook page https://www.facebook.com/Thairentecocar/</p>
                                                    </div>
                                                  </div>
                                                  <div class="container-timeline line-left">
                                                    <div class="content-timeline ">
                                                      <h4 style="color: #fff;">เดือนพฤศจิกายน </h4> 
                                                      <p style="color: #fff;">สั่งซื้อรถยนต์ รถคันแรกที่ให้บริการรุ่น Mitsubishi Mirage</p>
                                                    </div>
                                                  </div>
                                            </div> 
                                        </div>


                            <!-- <div class="col-md-12">
                                <div class="gap gap-small"></div>
                                
                           <h4 class="text-center">จุดเริ่มความสำเร็จ</h4>    
                           <p class="line-bottom"></p>   
                           <ul class="list-branch-ecocar text-center" style="padding-left: 0;">
                               <li>เดือนตุลาคม เริ่มต้นจดโดเมนและทำเว็บไซต์ www.thairentecocar.com</li>
                               <li>เดือนพฤศจิกายน สร้าง Facebook page https://www.facebook.com/Thairentecocar/</li>
                               <li>เดือนพฤศจิกายน สั่งซื้อรถยนต์ รถคันแรกที่ให้บริการรุ่น Mitsubishi Mirage</li>
                           </ul>     
                           </div>-->
                           <div class="col-md-12">
                           <div class="gap gap-small"></div>
                                <h4 class="text-center"> บริษัทรถเช่า บริษัทไทยเร้นท์อีโก้คาร์จำกัด </h4>
                                <p class="line-bottom"></p>
                                <p class="text-center">เพราะชีวิตคือการเดินทาง และทุกการเดินทางมีเป้าหมายเสมอ 
เราจึงอยากร่วมเป็นส่วนหนึ่งในการเดินทางไปกับท่านสู่เป้าหมายนั้น ด้วยการให้บริการรถเช่า eco car รายแรกของไทย ด้วยแนวคิด รถใหม่ ใคร ใคร ก็เช่าได้ ECOCAR จึงก่อตั้งขึ้นด้วยปณิธานอันแน่วแน่ เราพร้อมที่จะเป็นเพื่อนร่วมเดินทางของคุณ เพื่อมุ่งสู่จุดหมายปลายทาง โดยสวัสดิภาพ
</p><br/>                                   <div class="col-md-4">
                                                
                                                    <p class="text-center"><strong>วิสัยทัศน์</strong> <br>"เราจะเป็นบริษัทรถเช่าอันดับ 1 ในกรุงเทพ ด้านการบริการลูกค้า ภายในปี 2565" </p>
                                                    
                                                </div>
                                        
                                                <div class="col-md-4"> 
                                                        <p class="text-center"><strong>พันธกิจ</strong><br> เรามุ่งมั่นให้บริการรถเช่า ด้วยรถยนต์ใหม่ทุกคัน คุณภาพดี การบริการที่เป็นเลิศและพร้อมเคียงข้างเป็นเพื่อนตลอดการเดินทางของคุณ</p>
                                                </div>

                                                <div class="col-md-4">
                                                       <p class="text-center"><strong>วิถีของเรา</strong><br> ECOCAR ที่นี่เราเรียนรู้ทุกวัน</p>
                                                </div>
                                                       
                                                    <div class="col-md-12 ">
                                                      <div class="gap gap-small">
                                                            <div class="border-responsibility">   
                                                            <p class="text-center"><strong>ความรับผิดชอบต่อลูกค้า</strong> เพราะชีวิตคือการเดินทาง เราพร้อมที่จะเป็นเพื่อนอยู่เคียงข้างตลอดการเดินทางของคุณ เริ่มตั้งแต่การจัดเตรียมรถยนต์ใหม่ คุณภาพดี ซึ่งได้รับการตรวจสอบความพร้อมใช้งานก่อนส่งมอบรถให้ลูกค้า ให้คำแนะนำเรื่องการใช้งานรถยนต์ รวมทั้งเมื่อท่านรับรถจากเราไปแล้ว หากท่านมีปัญหาขัดข้องในการใช้งานหรือประสบอุบัติเหตุในระหว่างการเดินทาง สามารถติดต่อขอความช่วยเหลือจากเราได้ตลอด 24 ชั่วโมง</p>
                                                            </div>
                                                    </div>
                                        
                            </div>
            </div>
            </div>
            </div>

    </div>
@endsection


@section('begin_javascript')

@endsection
