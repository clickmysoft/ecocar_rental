@extends('frontend.layouts.app')

@section('begin_css')
<style type="text/css">
    div#abcdef {
    padding-top: 130px;
}
    #owl-demo .item{
  margin: 3px;
}
#owl-demo .item img{
  display: block;
  width: 100%;
  height: auto;
}
.btn-article-more{
    background: #01ad40;
    color: #fff;
}
.btn-article-more:hover{
    background-color: #085927;
    color: #fff;
}
</style>
@endsection

@section('content')

<div class="section-bc">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="bc-left">
                    <h2 class="h2-title-bc">
                        {{ __('index.frontend.our_articles') }}
                    </h2>
                </div>
            </div>
            <div class="col-md-6">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">

                    </ol>
                </nav>
            </div>
        </div>
    </div>

</div>


<div class="gap">

</div>

<div class="container">
    <div class="row">

        <div class="col-md-12">

            <ul class="booking-list">

                <?php
                    foreach ($data as $key => $value) {
                ?>

                        <li>
                            <a class="booking-item" href="<?php echo route('frontend.blog.show',$value['id']) ?>">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="<?=asset("frontend/article/".@$value['code'].'/files/'.$value['image_thumbnail'])?>" alt="{{ $value['mata_keyword'] }}" title="{{ $value['mata_title'] }}">
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="booking-item-title">{{ $value->title }}</h5>
                                        <p class="booking-item-description" style="text-align: justify;"><?=str_limit(strip_tags($value->description),740) ?></p>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="gap"></div>
                                        <span type="submit" class="btn btn-lg btn-default btn-article-more" role="button" style="margin-bottom: 10px;">
                                        <span>อ่านเพิ่มเติม</span>
                                    </div>
                                </div>
                            </a>
                        </li>

                <?php
                    }
                ?>

                

            </ul>
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <ul class="pagination">
                      {{ $data->links() }}
                </ul>
            </div>
            <div class="col-md-3"></div>


        </div>
    </div>
</div>

<div class="gap gap-small"></div>
@endsection


@section('begin_javascript')

@endsection
