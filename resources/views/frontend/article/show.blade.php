@extends('frontend.layouts.app')

@section ('title',$data->title)
@section ('meta_description',$data->mata_title)
@section ('meta_description',$data->meta_author)
@section ('keywords',$data->mata_keyword)

@section ('url','http://ecocar.co.th/blog/'.$data->id)
@section ('title',$data->mata_title)
@section ('description',$data->mata_description)
@section ('image',asset("frontend/article/".@$data->code.'/files/'.$data->image_thumbnail))

@section('begin_css')

<style type="text/css">
    .fb-comments.fb_iframe_widget.fb_iframe_widget_fluid_desktop{
        padding-left: 16.2%;
    }

    .fluid-width-video-wrapper {
    padding-top: 35% !important;
}
</style>
@endsection

@section('content')

    <br>
  <div class="container">
            <div class="row">

                 <div class="col-md-12"   style="margin-top: 135px;">
                    <article class="post">
                        <header class="post-header" >
                            <div class="fotorama" data-allowfullscreen="true">
                                <img src="<?=asset("article/".$data->image_full)?>" alt="{{ $data->title }}" title="{{ $data->title }}">
                            </div>
                        </header>
                        <div class="post-inner">
                            <h1 class="post-title text-darken" style="font-size: 28px;line-height: 48px;">{{ $data->title }}</h1>
                            <ul class="post-meta" style="width:100%;">
                                <li><i class="fa fa-calendar"></i><a href="#">{{ $data->created_at_time }} ({{ $data->created_at_humans }})</a>
                                </li>
                                <li><i class="fa fa-user"></i><a href="#">{{ $data->fk_create_by }}</a>
                                </li>

                                <li>
                                        <i class="fa fa-folder-open-o"></i>
                                <?php
                                    $i=1;
                                    foreach ($data['category_post'] as $key => $value) {
                                        if($i==1){
                                ?>
                                            <a href="#">{{ $value->category_name }}</a> 
                                <?php
                                            $i++;
                                        }else{
                                ?>
                                            ,<a href="#">{{ $value->category_name }}</a>
                                <?php            
                                        }
                                    }
                                ?>

                                </li>



                                <li>
                                        <i class="fa fa-tags"></i>
                                <?php
                                    $i=1;
                                    foreach ($data['post_tag'] as $key => $value) {
                                        if($i==1){
                                ?>
                                            <a href="#">{{ $value->tag_name }}</a> 
                                <?php
                                            $i++;
                                        }else{
                                ?>
                                            ,<a href="#">{{ $value->tag_name }}</a>
                                <?php            
                                        }
                                    }
                                ?>

                                </li>

                                <li style="float:right;">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v3.2&appId=361675743907292&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-share-button" data-href="http://ecocar.co.th/blog/{{ $data->id }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fecocar.co.th%2Fblog%2F{{ $data->id }}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">แชร์</a></div>





                                </li>

                            </ul>
                            <?php echo $data->description ?>
                        </div>

                        <div id="fb-root"></div>
                        <script>(function(d, s, id) {
                          var js, fjs = d.getElementsByTagName(s)[0];
                          if (d.getElementById(id)) return;
                          js = d.createElement(s); js.id = id;
                          js.src = 'https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v3.1&appId=361675743907292&autoLogAppEvents=1';
                          fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));
                        </script>
                        <div class="fb-comments" data-href="http://ecocar.co.th/blog/{{ $data->id }}" data-width="750" data-numposts="10"></div>
                    </article>
                   
                </div>
                <div class="col-md-3" style="padding-top: 25px;display:none;">
                    <aside class="sidebar-left">
                        <div class="sidebar-widget">
                            <h4>ประเภทบทความ</h4>
                            <ul class="icon-list list-category">
                            <?php
                                foreach ($data['category'] as $key => $value) {
                            ?>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-angle-right"></i>{{$value->category_name}} <small >(72)</small></a>
                                    </li>
                            <?php
                                }
                            ?>
                            </ul>
                        </div>
                    </aside>
                </div>
               
            </div>
        </div>




<h2 style="display:none;">{{ $data->mata_keyword }}</h2>

@endsection


@section('begin_javascript')

@endsection
