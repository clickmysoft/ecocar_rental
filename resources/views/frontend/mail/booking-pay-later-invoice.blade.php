<!DOCTYPE html>
<html>
   <head>
      <title>Page Title</title>
      
   </head>
   <body>

<div style="border: 1px solid #004c1d; max-width:600px;">
<div class="logo" style="background:#287a5c; background-color: #004c1d; width: 100%; padding-top:15px; padding-bottom:15px; text-align: center;">
    <a href="{{ route('frontend.index') }}">
            <img src="https://www.picz.in.th/images/2018/08/01/BhLnuD.jpg" alt="BhLnuD.jpg" border="0" style="height: 34px; width: 155px;"/> 
          </a>
  </div>
<!--  <hr style="border-top: 21px solid #287a5c" width="90%" /> -->
<div style="padding-right: 5%; padding-left: 5%">
  <div class="head_detail">
    <p><B>{{ __('index.frontend.hi') }}&nbsp;<?php print_r($data['first_name']);?>&nbsp;<?php print_r($data['last_name']);?>,</B></p>
    <p style="text-indent: 2.5em;">{{ __('index.frontend.thank_you') }} {{ __('index.frontend.check_detail') }} {{ __('index.frontend.q_or_c') }}
      {{ __('index.frontend.contact_cus_ser') }}
    </p>
  </div>
  <hr width="100%" />
  <div class="head_detail">
    <p><B>{{ __('index.frontend.all_lists') }}</B></p>
  </div>
  <div class="detail">
    <table style="border-collapse: collapse; width: 100%; margin: auto;">
      <tr  style="border-top: 1px solid #ddd; border-bottom: 1px solid #ddd; padding: 10px;">
            <th align="left" colspan="3">{{ __('index.frontend.lists') }}</th>
            <th align="left" colspan="1" style="    width: 200px;"></th>
            <th align="left">{{ __('index.frontend.detail') }}</th>
          </tr>
        <tr style="padding: 10px; border-bottom: 1px solid #ddd;">
          <td colspan="3">{{ __('index.frontend.pick_up2') }}</td>
          <td colspan="1"></td>
          <td><?php 
            print_r($data['dataset']['pick_up_text']);
          ?></td>
        </tr>
        <tr style="padding: 10px; border-bottom: 1px solid #ddd;">
          <td colspan="3">{{ __('index.frontend.drop_off2') }}</td>
          <td colspan="1"></td>
          <td><?php print_r($data['dataset']['drop_off_text']);?></td>

        </tr>
        <tr  style="padding: 10px; border-bottom: 1px solid #ddd;">
          <td colspan="3">{{ __('index.frontend.pick_up_date2') }}</td>
          <td colspan="1"></td>
          <td><?php print_r($data['dataset']['pick_up_date']);?></td>
        </tr>
        <tr  style="padding: 10px; border-bottom: 1px solid #ddd;">
          <td colspan="3">{{ __('index.frontend.pick_up_time2') }}</td>
          <td colspan="1"></td>
          <td><?php print_r($data['dataset']['pick_up_time']);?></td>
        </tr>
        <tr style="padding: 10px; border-bottom: 1px solid #ddd;">
          <td colspan="3">{{ __('index.frontend.drop_off_date2') }}</td>
          <td colspan="1"></td>
          <td><?php print_r($data['dataset']['drop_off_date']);?></td>
        </tr>
        <tr style="padding: 10px; border-bottom: 1px solid #ddd;">
          <td colspan="3">{{ __('index.frontend.drop_off_time2') }}</td>
          <td colspan="1"></td>
          <td><?php print_r($data['dataset']['drop_off_time']);?></td>
        </tr>
    </table>
  </div>
  <br>
  <div class="head_detail">
    <p><B>{{ __('index.frontend.list_and_acc') }}</B></p>
  </div>
  
    <div class="detail">
      <table style="border-collapse: collapse; width: 100%;">
          <tr  style="border-top: 1px solid #ddd; border-bottom: 1px solid #ddd; padding: 10px;">
            <th align="left">{{ __('index.frontend.lists') }}</th>
            <th align="center">{{ __('index.frontend.amount') }}</th>
            <th  align="center" style="text-indent: 40px;">{{ __('index.frontend.price') }}</th>
          </tr>
          

          <?php
            if($data['dataset']['pick_up'] == $data['dataset']['drop_off']){
              foreach ($data['items'] as $key => $items) {
                  if($items['name'] !== 'Dropoff Outside'){
                  ?>
                    <tr style="padding: 8px; border-bottom: 1px solid #ddd;">
                      <td><?php echo $items['name']; ?></td>
                      <td align="center"><?php echo $items['qty']; ?></td>
                      <td align="right"><?php echo number_format($items['price']); ?>&nbsp;{{ __('index.frontend.baht') }}</td>
                    </tr>
                  <?php
                  }
              }
            }else{
                foreach ($data['items'] as $key => $items) {
                ?>
                  <tr style="padding: 8px; border-bottom: 1px solid #ddd;">
                    <td><?php echo $items['name']; ?></td>
                    <td align="center"><?php echo $items['qty']; ?></td>
                    <td align="right"><?php echo number_format($items['price']); ?>&nbsp;{{ __('index.frontend.baht') }}</td>
                  </tr>
            <?php
                }
            }
          ?>
        <tr>
          <td colspan="2"><B>{{ __('index.frontend.total_price') }}</B>(+vat7%)</td>
          <td align="right"><B>
            <?php 
                        $total_vat = ($data['total']/100)*7;
                        $total_price_vat = $total_vat+$data['total'];
                        echo number_format($total_price_vat);
              // print_r( number_format($data['total']));
            ?>&nbsp;{{ __('index.frontend.baht') }}
          </B>
          </td>
        </tr>    
      </table>
    <!-- <hr style="border-top: 2px solid black" width="100%" /> -->
    </div>

    <br>
   <div class="detail">
    <table style="border-collapse: collapse; width: 100%; margin: auto;">
      <tr  style=" border-bottom: 1px solid #ddd; padding: 10px;">
            <th align="left" colspan="3">{{ __('index.frontend.add_note') }}</th>
          </tr>
        <tr style="padding: 10px; border-bottom: 1px solid #ddd;">
          <td > <?php print_r($data['dataset']['comment_remark']);?></td>
        </tr>
    </table>
  </div>
  <br>

  <div class="detail">
    <table style="border-collapse: collapse; width: 100%; margin: auto;">
        <tr style="padding: 10px;">
          <td style="text-indent: 2.5em;">{{ __('index.frontend.transfer_for_car') }} 500 {{ __('index.frontend.baht') }} {{ __('index.frontend.to_account_no') }} 001-370-1156‬ {{ __('index.frontend.k_bank') }} บจก. รถเช่าอีโคคาร์ 
                                          {{ __('index.frontend.send_proof') }} info@ecocar.co.th {{ __('index.frontend.to_confirm') }}</td>
        </tr>
    </table>
  </div>

  <hr width="100%" />

<div class="detail">
    <table style="border-collapse: collapse; width: 100%; margin: auto;">
        <tr  style=" border-bottom: 1px">
          <th align="left" colspan="3">{{ __('index.frontend.doc_to_prepare') }}</th>
        </tr>
        <tr style="padding: 10px;">
          <td style="text-indent: 2.5em;">{{ __('index.frontend.id_card') }}</td>
        </tr>
        <tr style="padding: 10px;">
          <td style="text-indent: 2.5em;">{{ __('index.frontend.driver_license') }}</td>
        </tr>
        <tr style="padding: 10px;">
          <td style="text-indent: 2.5em;">{{ __('index.frontend.credit_pay') }}</td>
        </tr>
         <tr style="padding: 10px;">
          <td >{{ __('index.frontend.see_more') }}&nbsp;&nbsp;<a  href="{{ url('document') }}">{{ __('index.frontend.rental_document') }}</a></td>
        </tr>
    </table>
  </div>

  <hr width="100%" />
  <div>
    <p style="text-indent: 2.5em;">{{ __('index.frontend.ecocar_company') }}&nbsp;{{ __('index.frontend.address5') }}&nbsp;{{ __('index.frontend.address6') }}</p>
    <p style="text-indent: 2.5em;">02-0024606, 098-828-9808, Line: @ecocar</p>
</div>    
  </div>
</div>


 

   </body>
</html>