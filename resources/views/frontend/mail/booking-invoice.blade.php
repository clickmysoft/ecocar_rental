<!DOCTYPE html>
<html>
   <head>
      <title>Page Title</title>
      
   </head>
   <body>

<div style="border: 1px solid #004c1d; max-width:600px;">
<div class="logo" style="background:#287a5c; background-color: #004c1d; width: 100%; padding-top:15px; padding-bottom:15px; text-align: center;">
    <a href="{{ route('frontend.index') }}">
            <img src="https://www.picz.in.th/images/2018/08/01/BhLnuD.jpg" border="0" style="height: 34px; width: 155px;"/> 
          </a>
  </div>
<!--  <hr style="border-top: 21px solid #287a5c" width="90%" /> -->
<div style="padding-right: 5%; padding-left: 5%">
  <div class="head_detail">
    <p><B>สวัสดีคุณ <?php print_r($data['first_name']);?>&nbsp;<?php print_r($data['last_name']);?>,</B></p>
    <p style="text-indent: 2.5em;">ขอขอบคุณสำหรับการใช้บริการของคุณ โปรดตรวจสอบรายละเอียดด้านล่างเพื่อความถูกต้อง หากคุณมีคำถามหรือข้อกงวลใด ๆ 
      กรุณาติดต่อฝ่ายบริการลูกค้าเพื่อขอรับความช่วยเหลือ
    </p>
  </div>
  <hr width="100%" />
  <div class="head_detail">
    <p><B>รายการทั้งหมด</B></p>
  </div>
  <div class="detail">
    <table style="border-collapse: collapse; width: 100%; margin: auto;">
      <tr  style="border-top: 1px solid #ddd; border-bottom: 1px solid #ddd; padding: 10px;">
            <th align="left" colspan="3">รายการ</th>
            <th align="left" colspan="1" style="    width: 200px;"></th>
            <th align="left">รายละเอียด</th>
          </tr>
        <tr style="padding: 10px; border-bottom: 1px solid #ddd;">
          <td colspan="3">สาขาที่รับรถ</td>
          <td colspan="1"></td>
          <td><?php print_r($data['pick_up_text']);?></td>
        </tr>
        <tr style="padding: 10px; border-bottom: 1px solid #ddd;">
          <td colspan="3">สาขาที่คืนรถ</td>
          <td colspan="1"></td>
          <td><?php print_r($data['drop_off_text']);?></td>

        </tr>
        <tr  style="padding: 10px; border-bottom: 1px solid #ddd;">
          <td colspan="3">วัน-รับรถ</td>
          <td colspan="1"></td>
          <td><?php print_r($data['pick_up_date']);?></td>
        </tr>
        <tr  style="padding: 10px; border-bottom: 1px solid #ddd;">
          <td colspan="3">เวลารับรถ</td>
          <td colspan="1"></td>
          <td><?php print_r($data['pick_up_time_text']);?> น.</td>
        </tr>
        <tr style="padding: 10px; border-bottom: 1px solid #ddd;">
          <td colspan="3">วันที่คืนรถ</td>
          <td colspan="1"></td>
          <td><?php print_r($data['drop_off_date']);?></td>
        </tr>
        <tr style="padding: 10px; border-bottom: 1px solid #ddd;">
          <td colspan="3">เวลาคืนรถ</td>
          <td colspan="1"></td>
          <td><?php print_r($data['drop_off_time_text']);?> น.</td>
        </tr>
    </table>
  </div>
  <br>
  <div class="head_detail">
    <p><B>รายการรถยนต์และอุปกรณ์เสริม</B></p>
  </div>
  
    <div class="detail">
      <table style="border-collapse: collapse; width: 100%;">
          <tr  style="border-top: 1px solid #ddd; border-bottom: 1px solid #ddd; padding: 10px;">
            <th align="left">รายการ</th>
            <th align="center">จำนวน</th>
            <th  align="center" style="text-indent: 40px;">ราคา</th>
          </tr>
          

          <?php
            if($data['pick_up_code'] == $data['drop_off_code']){
              foreach ($data['items'] as $key => $items) {
                  if($items['name'] !== 'Dropoff Outside'){
                  ?>
                    <tr style="padding: 8px; border-bottom: 1px solid #ddd;">
                      <td><?php echo $items['name']; ?></td>
                      <td align="center"><?php echo $items['qty']; ?></td>
                      <td align="right"><?php echo number_format($items['price']); ?>&nbsp;บาท</td>
                    </tr>
                  <?php
                  }
              }
            }else{
                foreach ($data['items'] as $key => $items) {
                ?>
                  <tr style="padding: 8px; border-bottom: 1px solid #ddd;">
                    <td><?php echo $items['name']; ?></td>
                    <td align="center"><?php echo $items['qty']; ?></td>
                    <td align="right"><?php echo number_format($items['price']); ?>&nbsp;บาท</td>
                  </tr>
            <?php
                }
            }
          ?>
        <tr>
          <td colspan="2"><B>รวมราคา</td>
          <td  align="right"><B>
            <?php 
                        $total_vat = ($data['total']/100)*7;
                        $total_price_vat = $total_vat+$data['total'];
                        echo number_format($total_price_vat);
              // print_r( number_format($data['total']));
            ?>&nbsp;บาท
          </td>
        </tr>    
      </table>
    <!-- <hr style="border-top: 2px solid black" width="100%" /> -->
    </div>


 <br>
  <div class="detail">
    <table style="border-collapse: collapse; width: 100%; margin: auto;">
      <tr  style=" border-bottom: 1px solid #ddd; padding: 10px;">
            <th align="left" colspan="3">หมายเหตุเพิ่มเติม</th>
          </tr>
        <tr style="padding: 10px; border-bottom: 1px solid #ddd;">
          <td ><?php print_r($data['comment_remark']);?></td>
        </tr>
    </table>
  </div>
<hr width="100%" />
  <br>
<div class="detail">
    <table style="border-collapse: collapse; width: 100%; margin: auto;">
        <tr  style=" border-bottom: 1px solid #ddd; padding: 10px;">
          <th align="left" colspan="3">เอกสารที่ต้องเตรียมมารับรถ</th>
        </tr>
        <tr style="padding: 10px; border-bottom: 1px solid #ddd;">
          <td style="text-indent: 2.5em;">บัตรประชาชน</td>
        </tr>
        <tr style="padding: 10px; border-bottom: 1px solid #ddd;">
          <td style="text-indent: 2.5em;">ใบขับขี่รถยนต์</td>
        </tr>
    </table>
  </div>

 
  <div>
    <p style="text-indent: 2.5em;"></p>
  </div>  


  <hr width="100%" />
  <div>
    <p style="text-indent: 2.5em;">บริษัท ไทยเร้นท์อีโก้คาร์ จำกัด 279/57 หมู่บ้านแกรนด์ ไอ ดีไซน์ ถ.วิภาวดีรังสิต แขวงสนามบิน เขตดอนเมือง กรุงเทพมหานคร 10210</p>
    <p style="text-indent: 2.5em;">02-0024606, 098-828-9808, Line: @ecocar</p>
</div>    
  </div>
</div>


 

   </body>
</html>