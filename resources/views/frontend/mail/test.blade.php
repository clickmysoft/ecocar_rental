<div class="col-md-12 col-xs-12">
	<div style="border: 0px solid #004c1d;">
		<div class="logo" style="background:#287a5c; background-color: #004c1d; width: 100%; padding-top:30px; padding-bottom:20px; text-align: center;">
	    	<a href="{{ route('frontend.index') }}">
	            <img src="https://www.picz.in.th/images/2018/09/26/fMZc8V.jpg"  border="0" style="height: 44px; width: 170px;"/>
	          </a>
	  	</div>
	  	<div style="padding-left: 7%; padding-right: 7%; padding-top: 2%; padding-bottom: 2%;">
	  		<p style="text-align: center;color: darkgreen; font-size: 30px;"><B>แจ้งเตือนรับรถ</B></p>
	  		<div style="background-color:#5fa2791f;border-radius:5px;padding:15px">
			    <p style="font-size: 16px;"><B>เรียนคุณ <?php print_r($data->customer_first_name); ?> <?php print_r($data->customer_last_name); ?></B></p>
			    <p style="text-indent: 5%; font-size: 16px;">แจ้งเตือนรับรถล่วงหน้า 1 ชั่วโมง รายละเอียดการรับรถดังนี้ <B>รับรถวันที่</B> <?php print_r($data->pick_up_date); ?> <B>เวลา</B> <?php print_r($data->pick_up_time); ?> <B>น. สาขา</B> <?php print_r($data->pick_up); ?> <B>รถยนต์</B> <?php print_r(strtoupper($data->car_brand)); ?> <B>รุ่น</B> <?php print_r(strtoupper($data->car_generation)); ?>
			    </p>
			    	<?php if ($data->paid == 0) { 
			    	?>
			    	<label style="padding-left: 5%; font-size: 16px; margin-bottom: -15px;"><B>จำนวนเงินที่ต้องชำระ</B></label><br>
			    		<div style="padding-left: 10%; font-size: 16px;">
							ค่ามัดจำ <?php print_r(number_format($data->data_sesstion->price_car_engine_cc)); ?> บาท<br>
							ค่าเช่ารถ <?php $num1 = $data->price ; $num2 = $data->data_sesstion->price_car_engine_cc; $sum = $num1 - $num2; print_r(number_format($sum)); ?> บาท<br>
							รวมยอดชำระในการรับรถ <B><?php print_r(number_format($data->price)); ?></B> บาท
						</div>
			    	<?php
			    	} else {
			    	?>

			    	<?php
			    	} 
			    	?>
			    
			</div>
			<p style="text-align: right;color: darkgreen; font-size: 16px;">ขอขอบคุณที่ใช้บริการ<br>ECOCAR</p>
	  	</div>
	  	<div style="background:#287a5c; background-color: #004c1d; width: 100%; padding-top:12px; padding-bottom:12px; text-align: center;">
	    	<a style="color:#ffffff; font-size:18px; text-decoration: none;" href="https://www.facebook.com/ecocar.co.th/">ติดต่อเรา</a>
	  	</div>
	</div>
</div>