@extends('frontend.layouts.app')

@section('begin_css')
 <link rel="stylesheet" type="text/css" href="{{ asset("frontend/css/styleecocar.css")}}">


@endsection

@section('content')

<div id="carousel-example-generic" class="carousel slide hidden-xs hidden-sm" data-ride="carousel">
                                      <!-- Indicators -->
                                      <ol class="carousel-indicators">
                                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                      </ol>
                                    
                                      <!-- Wrapper for slides -->
                                      <div class="carousel-inner" role="listbox">
                                        <div class="item active">
                                          <img src="{{ asset("frontend/img/backgrounds/ecocar-promotions-(Desktop).jpg")}}" alt="...">
                                          <div class="carousel-caption">
                                            ...
                                          </div>
                                        </div>
                                        <div class="item">
                                          <img src="{{ asset("frontend/img/backgrounds/ecocar-promotions-(Desktop).jpg")}}" alt="...">
                                          <div class="carousel-caption">
                                            ...
                                          </div>
                                        </div>
                                       
                                      </div>
                                    
                                      <!-- Controls -->
                                      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                      </a>
                                      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                      </a>
                                </div>
                                
                                
                                
                                    
                                <div id="carousel-example-generic" class="carousel slide hidden-md hidden-lg" data-ride="carousel">
                                      <!-- Indicators -->
                                      <ol class="carousel-indicators">
                                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                      </ol>
                                    
                                      <!-- Wrapper for slides -->
                                      <div class="carousel-inner" role="listbox">
                                        <div class="item active">
                                          <img src="{{ asset("frontend/img/backgrounds/slider-promotion-desktop-lg.jpg")}}" alt="...">
                                          <div class="carousel-caption">
                                            ...
                                          </div>
                                        </div>
                                        <div class="item">
                                          <img src="{{ asset("frontend/img/backgrounds/slider-promotion-desktop-lg.jpg")}}" alt="...">
                                          <div class="carousel-caption">
                                            ...
                                          </div>
                                        </div>
                                       
                                      </div>
                                    
                                      <!-- Controls -->
                                      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                      </a>
                                      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                      </a>
                                </div>

<div class="wrapper-promotion" style="background:#fff; background-position: center left;">
            <div class="container">
            <div class="row row-wrap">
            <div class=" text-center">
                            <div class="gap gap-small"></div>
                            <h3> รายการโปรโมชั่น </h3>
                            <p class="line-bottom"></p><br/>     
                <div class="col-md-4">
                    <div class="thumb promotion-box-card">

                        <header class="thumb-header">
                            <a class="hover-img curved" href="promotion-inner.html">
                                <img src="{{ asset("frontend/img/pro-05.jpg")}}" alt="Image Alternative text" title="Street">
                            </a>
                        </header>

                         <div class="thumb-caption text-center" id="description-promotion">
                            <h5 class="thumb-title" style="margin-bottom: 10px">ตั้งแต่วันที่ 14 - 21 กุมภาพันธ์ 2561</h5>
                            <p style="font-size: 18.2px;font-weight: 500;color: #000;"> Nissan Almera 1.2 AT</p>
                            <p style="padding: 0 20px; font-weight: 300;">ไม่มีผู้ใดที่สมัครรักใคร่ในความเจ็บปวด หรือเสาะแสวงหาและปรารถนาจะครอบครองความเจ็บปวด นั่นก็เป็นเพราะว่ามันเจ็บปวด...</p>
                            <p style="font-size: 18.2px;font-weight: 500;color: #ffffff;background: #01ad40;">ราคาเริ่มต้น 963 บาท / วัน</p>
                            <a type="submit" class="btn btn-default btn-more" role="button" style="margin-bottom: 10px;">ดูรายละเอียดเพิ่มเติม</a> 
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="thumb promotion-box-card">

                        <header class="thumb-header">
                            <a class="hover-img curved" href="#">
                                <img src="{{ asset("frontend/img/pro-04.jpg")}}" alt="Image Alternative text" title="Street">
                            </a>
                        </header>

                        <div class="thumb-caption text-center" id="description-promotion">
                            <h5 class="thumb-title" style="margin-bottom: 10px">ตั้งแต่วันที่ 1 - 15 มกราคม 2561</h5>
                            <p style="font-size: 18.2px;font-weight: 500;color: #000;"> Toyota Vios 1.5 AT</p>
                            <p style="padding: 0 20px; font-weight: 300;">ไม่มีผู้ใดที่สมัครรักใคร่ในความเจ็บปวด หรือเสาะแสวงหาและปรารถนาจะครอบครองความเจ็บปวด นั่นก็เป็นเพราะว่ามันเจ็บปวด...</p>
                            <p style="font-size: 18.2px;font-weight: 500;color: #ffffff;background: #01ad40;">ราคาเริ่มต้น 1,177 บาท / วัน</p>
                            <a type="submit" class="btn btn-default btn-more" role="button" style="margin-bottom: 10px;">ดูรายละเอียดเพิ่มเติม</a> 
                        </div>

                    </div>
                    </div>
                <div class="col-md-4">
                    <div class="thumb promotion-box-card">

                        <header class="thumb-header">
                            <a class="hover-img curved" href="#">
                                <img src="{{ asset("frontend/img/pro-03.jpg")}}" alt="Image Alternative text" title="Street">
                            </a>
                        </header>

                        <div class="thumb-caption text-center" id="description-promotion">
                            <h5 class="thumb-title" style="margin-bottom: 10px">ตั้งแต่วันที่ 18 - 28 ธันวาคม 2560</h5>
                            <p style="font-size: 18.2px;font-weight: 500;color: #000;">Nissan Sylphy 1.6 AT</p>
                            <p style="padding: 0 20px; font-weight: 300;">ไม่มีผู้ใดที่สมัครรักใคร่ในความเจ็บปวด หรือเสาะแสวงหาและปรารถนาจะครอบครองความเจ็บปวด นั่นก็เป็นเพราะว่ามันเจ็บปวด...</p>
                            <p style="font-size: 18.2px;font-weight: 500;color: #ffffff;background: #01ad40;">ราคาเริ่มต้น 1,498 บาท / วัน</p>
                            <a type="submit" class="btn btn-default btn-more" role="button" style="margin-bottom: 10px;">ดูรายละเอียดเพิ่มเติม</a> 
                        </div>
                    </div>
                </div>
            <div class="gap"></div>
                <div class="col-md-4">
                    <div class="thumb promotion-box-card">
                        <header class="thumb-header">
                            <a class="hover-img curved" href="#">
                                <img src="{{ asset("frontend/img/pro-02.jpg")}}" alt="Image Alternative text" title="Street">
                            </a>
                        </header>

                        <div class="thumb-caption text-center" id="description-promotion">
                            <h5 class="thumb-title" style="margin-bottom: 10px">ตั้งแต่วันที่ 22 - 30 พฤศจิกายน 2560</h5>
                            <p style="font-size: 18.2px;font-weight: 500;color: #000;">Nissan Teana 2.0 AT</p>
                            <p style="padding: 0 20px; font-weight: 300;">ไม่มีผู้ใดที่สมัครรักใคร่ในความเจ็บปวด หรือเสาะแสวงหาและปรารถนาจะครอบครองความเจ็บปวด นั่นก็เป็นเพราะว่ามันเจ็บปวด...</p>
                            <p style="font-size: 18.2px;font-weight: 500;color: #ffffff;background: #01ad40;">ราคาเริ่มต้น 2,461 บาท / วัน</p>
                            <a type="submit" class="btn btn-default btn-more" role="button" style="margin-bottom: 10px;">ดูรายละเอียดเพิ่มเติม</a> 
                        </div>

                    </div>
                </div>

                  <div class="col-md-4">
                    <div class="thumb promotion-box-card">
                        <header class="thumb-header">
                            <a class="hover-img curved" href="#">
                                <img src="{{ asset("frontend/img/pro-01.jpg")}}" alt="Image Alternative text" title="Street">
                            </a>
                        </header>

                        <div class="thumb-caption text-center" id="description-promotion">
                            <h5 class="thumb-title" style="margin-bottom: 10px">ตั้งแต่วันที่ 3 - 30 ตุลาคม 2560</h5>
                            <p style="font-size: 18.2px;font-weight: 500;color: #000;">Isuzu Mu x 1.9 AT</p>
                            <p style="padding: 0 20px; font-weight: 300;">ไม่มีผู้ใดที่สมัครรักใคร่ในความเจ็บปวด หรือเสาะแสวงหาและปรารถนาจะครอบครองความเจ็บปวด นั่นก็เป็นเพราะว่ามันเจ็บปวด...</p>
                            <p style="font-size: 18.2px;font-weight: 500;color: #ffffff;background: #01ad40;">ราคาเริ่มต้น 2,461 บาท / วัน</p>
                            <a type="submit" class="btn btn-default btn-more" role="button" style="margin-bottom: 10px;">ดูรายละเอียดเพิ่มเติม</a> 
                        </div>

                    </div>
                </div>       


                 <div class="col-md-4">
                    <div class="thumb promotion-box-card">
                        <header class="thumb-header">
                            <a class="hover-img curved" href="#">
                                <img src="{{ asset("frontend/img/pro-01.jpg")}}" alt="Image Alternative text" title="Street">
                            </a>
                        </header>
                        <div class="thumb-caption text-center" id="description-promotion">
                            <h5 class="thumb-title" style="margin-bottom: 10px">ตั้งแต่วันที่ 5 - 16 สิงหาคม 2560</h5>
                            <p style="font-size: 18.2px;font-weight: 500;color: #000;">Isuzu Mu x 1.9 AT</p>
                            <p style="padding: 0 20px; font-weight: 300;">ไม่มีผู้ใดที่สมัครรักใคร่ในความเจ็บปวด หรือเสาะแสวงหาและปรารถนาจะครอบครองความเจ็บปวด นั่นก็เป็นเพราะว่ามันเจ็บปวด...</p>
                            <p style="font-size: 18.2px;font-weight: 500;color: #ffffff;background: #01ad40;">ราคาเริ่มต้น 2,461 บาท / วัน</p>
                            <a type="submit" class="btn btn-default btn-more" role="button" style="margin-bottom: 10px;">ดูรายละเอียดเพิ่มเติม</a> 
                        </div>

                    </div>
                </div>    

                            <div class="gap"></div>
                    <a type="submit" class="btn btn-lg btn-default btn-more-all" role="button" style="margin-bottom: 10px;"><span> ดูโปรโมชั่นทั้งหมด </span></a> 
                            
                                         </div>
                                     </div>
                                </div>
                                <div class="gap"></div>
                    <div class="container-fluid">
                       <div class="row">
                         <div class="sec-promo-contact"> 
                            <div class="container">
                            <div class="col-md-12">
                                    <h4 class="text-center">การติดต่อ ขอรับโปรโมชั่นรถเช่า กับพนักงานขายของเรา</h4>
                                    <p class="line-bottom"></p>
                                    <div class="gap gap-small"></div>
                                    <div class="col-md-3 text-center"><i class="fa fa-phone fa-2x" id="contact-promotion"></i>
                                    <br>
                                                                คุณเอชิน สาขาลาดพร้าว โทร 062-3489900</div>
                                    <div class="col-md-3 text-center"><i class="fa fa-phone fa-2x" id="contact-promotion"></i><br>คุณโบว์ สาขาบางหว้า โทร 098-8289808</div>
                                    <div class="col-md-3 text-center"><i class="fa fa-phone fa-2x" id="contact-promotion"></i><br>คุณบอย สาขานนทบุรี โทร 062-3482370</div>
                                    <div class="col-md-3 text-center"><i class="fa fa-phone fa-2x" id="contact-promotion"></i><br>call center 02-0305880</div>
                                </div>
                                </div>
                            </div>
                        </div>
                     </div>
                </div>    
            </div>    
        </div>
</div>

@endsection


@section('begin_javascript')

@endsection
