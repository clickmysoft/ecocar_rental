@extends('backend.layouts.app')

@push('after-styles')
    @include('log-viewer::_template.style')
@endpush

@section('begin_css')
<style type="text/css">
    .card-header{
            padding: 15px 15px;
            border-bottom: 1px solid rgba(0,0,0,.06);
            background: #395a6673;
            color: #000;
            border-radius: 3px;
            font-weight: 800;
                border-bottom-right-radius: 0px;
    border-bottom-left-radius: 0px;
    }
    .card-body{
        background-color: #fff;
    }
    .table{
        width: 100%;
    max-width: 100%;
    margin-bottom: 1rem;
    background-color: transparent;
    }
    .stack-content {
    background-color: #F6F6F6;
    color: #AE0E0E;
    font-family: consolas, Menlo, Courier, monospace;
    font-size: 12px;
    font-weight: 400;
    white-space: pre-line;
    max-width: 0;
    overflow-x: auto;
    }
    .table td, .table th {
    padding: .75rem;
    vertical-align: top;
    border-top: 1px solid #a4b7c1;
    }
    tr.collapse.in {
    display: table-row !important;
    }
    a.list-group-item.all{
        padding: 15px 10px 35px 7px;
        background: #0a9d93;
            border-radius: 0px;
    }
    a.list-group-item.disabled {
        padding: 15px 10px 30px 7px;
    }
    a.list-group-item.error {
        padding: 15px 10px 30px 7px;
        background-color: #0a9d93;
    }
    a.list-group-item.info {
        padding: 15px 10px 30px 10px;
        background-color: #0a9d93;
    }
    span.badge.level.level-all.float-left{
        float: left;
        background-color:transparent;
    }
    span.badge.badge-pill.level.level-all.float-right{
        background-color: #fff;
        color: #000;
    }
    span.badge.level.level-none{
        float: left;
        background: #fff;
        color: #000;
    }

    span.badge.level.level-error.float-left {
    float: left;
    background: transparent;
    }
    span.badge.badge-pill.level.level-error.float-right {
    background: #fff;
    color: #000;
    }
    span.badge.level.level-info.float-left {
    float: left;
    background: transparent;
    }
    span.badge.badge-pill.level.level-info.float-right {
    background: #fff;
    color: #000;
    }
    .list-group-item.disabled, .list-group-item.disabled:focus, .list-group-item.disabled:hover{
        background-color: #fff;
    }

    .svg-inline--fa.fa-w-18 {
    width: 1.125em;
    }

    .svg-inline--fa {
    display: inline-block;
    font-size: inherit;
    height: 1em;
    overflow: visible;
    vertical-align: -.125em;
    }
    a.btn-outline-info:hover {
    color: #151b1e;
    background-color: #63c2de !important;
    border-color: #63c2de;
    }
    .btn-outline-info{
    color: #63c2de;
    background-color: transparent;
    background-image: none;
    border-color: #63c2de;
    }
    span.badge.badge-env {
    background: #00a788;
    }
    span.badge.level.level-info {
    background: #2e96de;
    }
    .badge-default {
    background-color: #c95654 !important;
    }
    span.badge.level.level-error {
    background-color: #000;
    }
    
    /*.panel.panel-default {
        background: #E1E5EF;
    }*/

    .panel.panel-default {
        background: #FFF;
    }

    .progress-bar{
    height: .5rem!important;
    }
    i.fa.fa-fw.fa-times-circle {
    color: #ff5500;
    }

    .badge-info{
        display: none;
    }

    .list-group-item{
        border:0px !important;
        border-bottom: 1px solid #ddd !important;
    }
    .badge{
        font-size: 13px !important;
        font-weight: 500 !important;
    }
    .list-group-item{
        margin-bottom:0px !important;
    }

    .list-group-item:first-child{
        border-bottom: 1px solid #ddd !important;
    }
    .list-group-item:last-child{
        border-bottom: 1px solid #ddd !important;
    }
</style>

@endsection

@section('content')
<div class="panel panel-default">
     <div class="panel-heading" style="color: #fff !important;color: #fff !important;background: #60868b;padding: 10px 15px; ">
                            <h3 class="panel-title" style="float:left;padding: 10px 0 !important; margin: 0 !important;"><i class="fa fa-database"></i>&nbsp; Log [{{ $log->date }}]</h3>
                            <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                            </div>
                        </div>
    <div class="row" style="    padding: 13px;">
        <div class="col-md-2">
            @include('log-viewer::_partials.menu')
        </div>
        <div class="col-md-10">
            {{-- Log Details --}}
            <div class="card">
                <div class="card-header">
                    Log info :
                    <div class="float-right" style="float: right;">
                        <a href="{{ route('log-viewer::logs.download', [$log->date]) }}" class="btn btn-sm btn-success">
                            <i class="fa fa-download"></i> DOWNLOAD
                        </a>
                        <a href="#delete-log-modal" class="btn btn-sm btn-danger" data-toggle="modal" data-backdrop="false">
                            <i class="fa fa-trash-o"></i> DELETE
                        </a>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-sm-2">
                                File path:
                            </div>
                            <div class="col-sm-10">
                                {{ $log->getPath() }}
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-2">
                                Log entries: <span class="badge badge-primary">{{ $entries->total() }}</span>
                            </div>
                            <div class="col-md-3">
                                Size: <span class="badge badge-primary">{{ $log->size() }}</span>
                            </div>
                            <div class="col-md-3">
                                Created at: <span class="badge badge-primary">{{ $log->createdAt() }}</span>
                            </div>
                            <div class="col-md-3">
                                Updated at: <span class="badge badge-primary">{{ $log->updatedAt() }}</span>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            {{-- Log Entries --}}
            <div class="card mt-4">
                @if ($entries->hasPages())
                    <div class="card-header" style="display:none;">
                        <div class="row">
                            <div class="col">
                                {!! $entries->appends(compact('query'))->render('log-viewer::_pagination.bootstrap-4') !!}
                            </div>
                            <div class="col text-right">
                                <span class="badge badge-info">
                                    Page {!! $entries->currentPage() !!} of {!! $entries->lastPage() !!}
                                </span>
                            </div>
                        </div>
                    </div>
                @endif
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="entries" class="table" style="word-break: break-word;">
                                <thead style="    background: #acb9bd;">
                                <tr>
                                    <th>ENV</th>
                                    <th>Level</th>
                                    <th>Time</th>
                                    <th>Header</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($entries as $key => $entry)
                                    <tr>
                                        <td>
                                            <span class="badge badge-env">{{ $entry->env }}</span>
                                        </td>
                                        <td>
                                    <span class="badge level level-{{ $entry->level }}">
                                        {!! $entry->level() !!}
                                    </span>
                                        </td>
                                        <td>
                                    <span class="badge badge-default">
                                        {{ $entry->datetime->format('H:i:s') }}
                                    </span>
                                        </td>
                                        <td>
                                            {{ $entry->header }}
                                        </td>
                                        <td class="text-right">
                                            @if ($entry->hasStack())
                                                <!-- <a class="btn btn-sm btn-outline-info" role="button" data-toggle="collapse" href="#log-stack-{{ $key }}" aria-expanded="false" aria-controls="log-stack-{{ $key }}">
                                                    <i class="fa fa-toggle-on"></i> Stack
                                                </a> -->

                                                <a class="btn btn-sm btn-outline-info" role="button" data-toggle="collapse" href="#log-stack-{{ $key }}" aria-expanded="false" aria-controls="log-stack-{{ $key }}">
                                                <svg class="svg-inline--fa fa-toggle-on fa-w-18" aria-hidden="true" data-prefix="fa" data-icon="toggle-on" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M576 256c0 106.039-85.961 192-192 192H192C85.961 448 0 362.039 0 256S85.961 64 192 64h192c106.039 0 192 85.961 192 192zM384 128c-70.741 0-128 57.249-128 128 0 70.741 57.249 128 128 128 70.741 0 128-57.249 128-128 0-70.741-57.249-128-128-128"></path></svg> Stack
                                                </a>



                                            @endif
                                        </td>
                                    </tr>
                                    @if ($entry->hasStack())
                                        <tr class="stack-content collapse" id="log-stack-{{ $key }}">
                                            <td colspan="5" class="stack">
                                                {!! trim($entry->stack()) !!}
                                            </td>
                                        </tr>
                                    @endif
                                @empty
                                    <tr>
                                        <td colspan="5" class="text-center">
                                            <span class="badge badge-default">{{ __('log-viewer::general.empty-logs') }}</span>
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div><!--table-responsive-->
                    </div><!--card-body-->
                @if ($entries->hasPages())
                    <div class="card-footer">
                        <div class="row">
                            <div class="col" style="text-align: center;">
                                {!! $entries->appends(compact('query'))->render('log-viewer::_pagination.bootstrap-4') !!}
                            </div>
                            <div class="col text-right">
                            <span class="badge badge-info">
                                Page {!! $entries->currentPage() !!} of {!! $entries->lastPage() !!}
                            </span>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div id="delete-log-modal" class="modal fade">
        <div class="modal-dialog">
            <form id="delete-log-form" action="{{ route('log-viewer::logs.delete') }}" method="POST">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="date" value="{{ $log->date }}">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Log File</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to <span class="badge badge-danger">DELETE</span> this log file <span class="badge badge-primary">{{ $log->date }}</span> ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-danger" data-loading-text="Loading&hellip;"><i class="fa fa-trash-o"></i> DELETE FILE</button>
                        <button type="button" class="btn btn-sm btn-default pull-left" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div> 
@endsection

@push('after-scripts')
    <script>
        $(function () {
            var deleteLogModal = $('#delete-log-modal'),
                deleteLogForm  = $('#delete-log-form'),
                submitBtn      = deleteLogForm.find('button[type=submit]');

            deleteLogForm.on('submit', function(event) {
                event.preventDefault();
                submitBtn.button('loading');

                $.ajax({
                    url:      $(this).attr('action'),
                    type:     $(this).attr('method'),
                    dataType: 'json',
                    data:     $(this).serialize(),
                    success: function(data) {
                        submitBtn.button('reset');
                        if (data.result === 'success') {
                            deleteLogModal.modal('hide');
                            location.replace("{{ route('log-viewer::logs.list') }}");
                        }
                        else {
                            alert('OOPS ! This is a lack of coffee exception!')
                        }
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert('AJAX ERROR ! Check the console !');
                        console.error(errorThrown);
                        submitBtn.button('reset');
                    }
                });

                return false;
            });

            @unless (empty(log_styler()->toHighlight()))
                $('.stack-content').each(function() {
                    var $this = $(this);
                    var html = $this.html().trim()
                        .replace(/({!! join(log_styler()->toHighlight(), '|') !!})/gm, '<strong>$1</strong>');

                    $this.html(html);
                });
            @endunless
        });
    </script>
@endpush
