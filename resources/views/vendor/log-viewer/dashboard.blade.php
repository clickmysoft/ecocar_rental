@extends('backend.layouts.app')

@push('after-styles')
    @include('log-viewer::_template.style')
@endpush
@section('begin_css')
<style type="text/css">
   .card{   
    background: #f3f3f3;
    height: 110px;
    padding: 20px;
    box-shadow: 1px 1px 10px #cac8c8;
    margin-bottom: 20px;
    border-radius: .35rem;
    -webkit-transition: all .25s ease;
    -o-transition: all .25s ease;
    -moz-transition: all .25s ease;
    transition: all .25s ease;
}
.card:hover{
    -webkit-box-shadow: 0 14px 24px rgba(62,57,107,.2);
    box-shadow: 0 20px 24px rgba(62,57,107,.2);
    -webkit-transform: translateY(-4px) scale(1.02);
    -moz-transform: translateY(-4px) scale(1.02);
    -ms-transform: translateY(-4px) scale(1.02);
    -o-transform: translateY(-4px) scale(1.02);
    transform: translateY(-4px) scale(1.02);
}
 .progress-bar{
    height: .5rem!important;
    }
    i.fa.fa-fw.fa-times-circle {
        color: #ff5500;
    }
    i.fa.fa-fw.fa-info-circle{
        color: #0070d8;
    }
    i.fa.fa-fw.fa-list{
        color: #009075;
    }
    i.fa.fa-fw.fa-bug {
    color: #b91912;
    }
    i.fa.fa-fw.fa-bullhorn {
    color: #d52d28;
    }
    i.fa.fa-fw.fa-exclamation-triangle {
    color: #ff9100;
    }
    i.fa.fa-fw.fa-exclamation-circle {
    color: #47b04b;
    }
    i.fa.fa-fw.fa-life-ring {
    color: #8ec9fb;
    }
    div#progress-doughnut > div:nth-child(1) div.progress > div.progress-bar{
            background-color: #009075;
    }
    div#progress-doughnut > div:nth-child(5) div.progress > div.progress-bar{
            background-color: #ff5607;
    }
    div#progress-doughnut > div:nth-child(8) div.progress > div.progress-bar{
            background-color: #0c73d5;
    }
</style>

@endsection
@section('content')
<div class="panel panel-default">
     <div class="panel-heading" style="color: #fff !important;color: #fff !important;background: #60868b;padding: 10px 15px; margin-bottom: 30px;">
                            <h3 class="panel-title" style="float:left;padding: 10px 0 !important; margin: 0 !important;"><i class="fa fa-database"></i>&nbsp; Dashboard (Log Viewer)</h3>
                            <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                            </div>
                        </div>
     <div class="container-fluid">                   
    <div class="row">
        <div class="col-md-3">
            <canvas id="stats-doughnut-chart" height="300"></canvas>
        </div>

        <div class="col-md-9">
            <div class="row" id="progress-doughnut">
                @foreach($percents as $level => $item)
                    <div class="col-md-4 col-sm-6 mb-3">
                        <div class="card level-card level-{{ $level }} {{ $item['count'] === 0 ? 'level-empty' : '' }}" >
                            <div class="card-header">
                                <span class="level-icon">{!! log_styler()->icon($level) !!}</span> {{ $item['name'] }}
                            </div>
                            <div class="card-body">
                                {{ $item['count'] }} entries - {!! $item['percent'] !!}%
                                <div class="progress">
                                    <div class="progress-bar" style="width: {{ $item['percent'] }}%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
  </div>  
 </div> 
@endsection

@push('after-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>
    <script>
        Chart.defaults.global.responsive      = true;
        Chart.defaults.global.scaleFontFamily = "'Source Sans Pro'";
        Chart.defaults.global.animationEasing = "easeOutQuart";
    </script>
    <script>
        $(function() {
            new Chart($('canvas#stats-doughnut-chart'), {
                type: 'doughnut',
                data: {!! $chartData !!},
                options: {
                    legend: {
                        position: 'bottom'
                    }
                }
            });
        });
    </script>
@endpush
