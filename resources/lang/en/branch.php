<?php

return [
        'TifEfmFR' => [
            'id'        => 1,
            'code'      => 'TifEfmFR',
            'name'      => 'Don&nbsp;Mueang&nbsp;Airport(DMK)',
            'address'   => 'Airports of Thailand Public Company Limited, No.333 Cherd Wutthakat, Srikan, Donmuang, Bangkok, 10210',
        ],
        'ML05GTQO' => [
            'id'        => 2,
            'code'      => 'ML05GTQO',
            'name'      => 'Suvarnabhumi&nbsp;Airport(BKK)',
            'address'   => 'No.999 M.1 Nong Preu, Bangpli, Samut Prakan, 10540',
        ],
        'EbRREe9k' => [
            'id'        => 3,
             'code'     => 'EbRREe9k',
            'name'      => 'Hat&nbsp;Yai&nbsp;Airport(HDY)',
            'address'   => '999 หมู่ 1 ตำบลหนองปรือ อำเภอบางพลี จังหวัดสมุทรปราการ 10540',
        ],
        'uoSDDYle' => [
            'id'        => 4,
            'code'      => 'uoSDDYle',
            'name'      => 'Donmuang(downtown)',
            'address'   => 'No.279/57 Wipawadee Rd., Airport District, Donmuang, Bangkok, 10210',
        ],
        'izLH906r' => [
            'id'        => 5,
            'name'      => 'On&nbsp;Nut',
            'code'      => 'izLH906r',
            'address'   => 'No.268/13 Onnut Rd., Pravet District, Pravate, Bangkok, 10250',
        ],
        'uzIEA3or' => [
            'id'        => 6,
            'name'      => 'Bang&nbsp;Wa',
            'code'      => 'uzIEA3or',
            'address'   => 'No.9 Soi Petchkasaem 25/5, Bang wa District, Pasricharoen, Bangkok, 10160',
        ],
        'SAHbAkUY' => [
            'id'        => 7,
            'code'      => 'SAHbAkUY',
            'name'      => 'Ladprao',
            'address'   => 'No.2438 Ladprao Rd., Plabpla District, Wangthonglang, Bangkok, 10310',
        ],
        'Z07e1ySa' => [
            'id'        => 8,
            'code'      => 'Z07e1ySa',
            'name'      => 'Nonthaburi',
            'address'   => 'No.160/1, Rattanathibet Rd. soi 18, Bangkrasor, Muang Nonthaburi, Nonthaburi, 11000',
        ],
        'hfNms3YD' => [
            'id'        => 9,
            'code'      => 'hfNms3YD',
            'name'      => 'Samrong',
            'address'   => 'No.1199 Sukhumvit Rd., Samrongneau, Muang Samut Prakan, Samut Prakan, 10270',
        ],
        'zrYbZkkK' => [
            'id'        => 10,
            'code'      => 'zrYbZkkK',
            'name'      => 'Pattaya',
            'address'   => 'No.388/9 Sukhumvit Rd., Nong Preu, Amphur Bang La Mung, Chonburi, 20150',
        ],
        'tzRyDFfT' => [
            'id'        => 11,
            'code'      => 'tzRyDFfT',
            'name'      => 'Chiang&nbsp;Mai/Chiang&nbsp;Mai&nbsp;Airport',
            'address'   => 'No.92 Somposh Chiang Mai 700 years Rd., Fah ham District, Amphur Muang Chiang Mai, Chiang Mai, 50000',
        ],
        'E4Rd5kYV' => [
            'id'        => 12,
            'code'      => 'E4Rd5kYV',
            'name'      => 'Ubon&nbsp;Ratchathani/<br>Ubon&nbsp;Ratchathani&nbsp;Airport',
            'address'   => 'No.456 Thepyotee, Nai Muang District, Amphur Muang, Ubon Ratchathani, 34000',
        ],
        'TbRREe9u' => [
            'id'        => 13,
            'code'      => 'TbRREe9u',
            'name'      => 'Other',
            'address'   => '999 หมู่ 1 ตำบลหนองปรือ อำเภอบางพลี จังหวัดสมุทรปราการ 10540',
        ],
];