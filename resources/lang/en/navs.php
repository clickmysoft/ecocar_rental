<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home'   => 'Home',
        'about'  => 'About',
        'about_us' => 'About US',
        'ceo' => 'CEO',
        'branch' => 'Branch',
        'document' => 'Document',
        'promotion' => 'Promotion',
        'blog' => 'Blog',
        'contact_us' => 'Contact Us',
        'logout' => 'Logout',
        'csr' => 'CSR activities',
        'service' => 'Service',
        'ecocar_service' => 'Ecocar Service',
        'questions' => 'Questions',
        'terms_service' => 'Terms of service',
        'modelcar' => 'Car Model,Car Rental',
        'initial_rental_terms' => 'Initial Rental Terms',
        'non_credit' => 'Car Rental does not use credit card',
        'with_driver' => 'Car rental service with driver',
        'rental_service' => 'Car Rental Service',
        'rented_to' => 'Car service Rented to',
        'car_insurance' => 'Terms of Car Insurance',
        'insuarance_sup' => 'Insurance Condition (Supplement) SCDW',
    ],

    'frontend' => [
        'contact' => 'Contact',
        'dashboard' => 'Dashboard',
        'login'     => 'Login',
        'macros'    => 'Macros',
        'register'  => 'Register',

        'user' => [
            'account'         => 'My Account',
            'administration'  => 'Administration',
            'change_password' => 'Change Password',
            'my_information'  => 'My Information',
            'profile'         => 'Profile',
        ],
    ],
];
