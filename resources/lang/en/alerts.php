<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'roles' => [
            'created' => 'The role was successfully created.',
            'deleted' => 'The role was successfully deleted.',
            'updated' => 'The role was successfully updated.',
        ],

        'users' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email'  => 'A new confirmation e-mail has been sent to the address on file.',
            'confirmed'              => 'The user was successfully confirmed.',
            'created'             => 'The user was successfully created.',
            'deleted'             => 'The user was successfully deleted.',
            'deleted_permanently' => 'The user was deleted permanently.',
            'restored'            => 'The user was successfully restored.',
            'session_cleared'      => "The user's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated'             => 'The user was successfully updated.',
            'updated_password'    => "The user's password was successfully updated.",
        ],
    ],

    'frontend' => [
        'contact' => [
            'sent' => 'Your information was successfully sent. We will respond back to the e-mail provided as soon as we can.',
        ],
    ],

  
    'swal_title_delete'         =>  'Delete',
    'swal_text_delete_sec1'     =>  'คุณแน่ใจหรือไม่ต้องการลบ',
    'swal_text_delete_sec2'     =>  'การดำเนินการนี้ไม่สามารถกู้คืนได้',
    'swal_text_delete_sec3'     =>  'และลบอย่างถาวร',
    'swal_text_delete_sec4'     =>  'การดำเนินการนี้ไม่สามารถกู้คืนได้',
    'swal_text_success'         =>  'สำเร็จ',
    'swal_text_unsuccess'       =>  'ไม่สำเร็จ',
    'swal_text_success_del'     =>  'ถูกลบเรียบร้อยแล้ว',
    'swal_text_unseccess_del'   =>  'ไม่สามารถลบได้!',

    'DataTable_infoEmpty'       =>  'ไม่พบข้อมูล',
    'DataTable_lengthMenu'      =>  'จำนวนแสดง _MENU_ ข้อมูลต่อหน้า',
    'DataTable_search'          =>  'ค้นหา :',
    'DataTable_first'           =>  'หน้าแรก',
    'DataTable_last'            =>  'หน้าสุดท้าย',
    'DataTable_next'            =>  'หน้าถัดไป',
    'DataTable_previous'        =>  'หน้าที่แล้ว',
    'DataTable_info'            =>  'แสดงหน้า _PAGE_ จาก _PAGES_',
    'DataTable_zeroRecords'     =>  'ไม่พบข้อมูลที่ต้องการ',

    'yadcf_reset'               =>  'รีเซ็ตตัวกรอง',

    //SwitcherSystem
    'switch_title'              =>  'คุณต้องการเปลี่ยนระบบ ?',
    'switch_text'               =>  'กดปุ่มเปลี่ยนระบบ เพื่อเข้าสู่ระบบ',
    'switch_confirmButtonText'  =>  'เปลี่ยนระบบ',

];
