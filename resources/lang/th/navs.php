<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home'   => 'หน้าแรก',
        'about'  => 'เกี่ยวกับ',
        'about_us' => 'เกี่ยวกับเรา',
        'ceo' => 'ผู้บริหาร',
        'branch' => 'สาขา',
        'document' => 'เอกสารการเช่ารถ',
        'promotion' => 'โปรโมชั่น',
        'blog' => 'บทความ',
        'contact_us' => 'ติดต่อเรา',
        'logout' => 'ออกจากระบบ',
        'csr' => 'กิจกรรม CSR',
        'service' => 'การบริการ',
        'ecocar_service' => 'บริการของรถเช่าอีโคคาร์',
        'questions' => 'คำถามที่พบบ่อย',
        'terms_service' => 'เงื่อนไขการบริการ',
        'terms_service' => 'เงื่อนไขการเช่าเบื้องต้น',
        'modelcar' => 'รุ่นรถยนต์,ราคารถเช่า',
        'initial_rental_terms' => 'เงื่อนไขการเช่าเบื้องต้น',
        'non_credit' => 'บริการเช่ารถไม่ใช้บัตรเครดิต',
        'with_driver' => 'บริการเช่ารถยนต์พร้อมคนขับ',
        'rental_service' => 'บริการเช่ารถยนต์ขับเอง',
        'rented_to' => 'บริการส่งรถยนต์ที่เช่าถึงที่',
        'car_insurance' => 'เงื่อนไขกรมธรรม์ประกันภัยรถยนต์',
        'insuarance_sup' => 'เงื่อนไขประกันภัย(เสริม) SCDW',
    ],

    'frontend' => [
        'contact' => 'Contact',
        'dashboard' => 'แผงควบคุม',
        'login'     => 'เข้าสู่ระบบ',
        'macros'    => 'Macros',
        'register'  => 'ลงทะเบียน',

        'user' => [
        'account'             => 'บัญชีของฉัน',
            'administration'  => 'หน้าแอดมิน',
            'change_password' => 'เปลี่ยนรหัสผ่าน',
            'my_information'  => 'ข้อมูลของฉัน',
        'profile'             => 'ข้อมูลส่วนตัว',
        ],
    ],
];
