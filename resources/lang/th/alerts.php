<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */



    'backend' => [
        'roles' => [
            'created' => 'บทบาทถูกสร้างสำเร็จแล้ว',
            'deleted' => 'บทบาทถูกลบสำเร็จแล้ว',
            'updated' => 'บทบาทถูกแก้ไขสำเร็จแล้ว',
        ],

        'users' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email'  => 'อีเมลยืนยันตัวตนได้ถูกส่งไปยังปลายทางแล้ว',
            'confirmed'              => 'The user was successfully confirmed.',
            'created'             => 'ผู้ใช้ถูกสร้างสำเร็จแล้ว',
            'deleted'             => 'ผู้ใช้ถูกลบสำเร็จแล้ว',
            'deleted_permanently' => 'ผู้ใช้ถูกลบไปอย่างถาวร',
            'restored'            => 'ผู้ใช้ถูกกู้คืนสำเร็จแล้ว',
            'session_cleared'      => "The user's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated'             => 'ผู้ใช้ถูกแก้ไขสำเร็จแล้ว',
            'updated_password'    => 'รหัสผ่านของผู้ใช้ถูกแก้ไขสำเร็จแล้ว',
        ],
        'database' =>[
            'jSuccess'      => 'ดำเนินการเรียบร้อยแล้ว',
        ],
    ],

    'frontend' => [
        'contact' => [
            'sent' => 'Your information was successfully sent. We will respond back to the e-mail provided as soon as we can.',
        ],
    ],




    'please'            =>  'กรุณาระบุข้อมูล',
    'description'       =>  'คำอธิบายรายละเอียดเพิ่มเติม',
    'Button_Save'       =>  'บันทึกข้อมูล',
    'Button_Cancle'     =>  'ยกเลิก',
    'Butoon_Edit'       =>  'แก้ไขข้อมูล',
    'error'             =>  'เกิดความผิดพลาด',
    'active'            =>  'เปิดใช้งาน',
    'unactive'          =>  'ปิดใช้งาน',
    'status'            =>  'สถานะ',
    'statused'          =>  'ผลการทำงาน',
    'Button_Delete'     =>  'ลบข้อมูล',

    'swal_title_delete'         =>  'ยืนยันการลบ',
    'swal_text_delete_sec1'     =>  'คุณแน่ใจหรือไม่ต้องการลบ',
    'swal_text_delete_sec2'     =>  'การดำเนินการนี้ไม่สามารถกู้คืนได้',
    'swal_text_delete_sec3'     =>  'และลบอย่างถาวร',
    'swal_text_delete_sec4'     =>  'การดำเนินการนี้ไม่สามารถกู้คืนได้',
    'swal_text_success'         =>  'สำเร็จ',
    'swal_text_unsuccess'       =>  'ไม่สำเร็จ',
    'swal_text_success_del'     =>  'ถูกลบเรียบร้อยแล้ว',
    'swal_text_unseccess_del'   =>  'ไม่สามารถลบได้!',

    'DataTable_infoEmpty'       =>  'ไม่พบข้อมูล',
    'DataTable_lengthMenu'      =>  'จำนวนแสดง _MENU_ ข้อมูลต่อหน้า',
    'DataTable_search'          =>  'ค้นหา :',
    'DataTable_first'           =>  'หน้าแรก',
    'DataTable_last'            =>  'หน้าสุดท้าย',
    'DataTable_next'            =>  'หน้าถัดไป',
    'DataTable_previous'        =>  'หน้าที่แล้ว',
    'DataTable_info'            =>  'แสดงหน้า _PAGE_ จาก _PAGES_',
    'DataTable_zeroRecords'     =>  'ไม่พบข้อมูลที่ต้องการ',

    'yadcf_reset'               =>  'รีเซ็ตตัวกรอง',

    //SwitcherSystem
    'switch_title'              =>  'คุณต้องการเปลี่ยนระบบ ?',
    'switch_text'               =>  'กดปุ่มเปลี่ยนระบบ เพื่อเข้าสู่ระบบ',
    'switch_confirmButtonText'  =>  'เปลี่ยนระบบ',
];
