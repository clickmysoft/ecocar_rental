<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('image_thumbnail')->nullable();
            $table->string('image_full')->nullable();
            $table->text('description')->nullable();
            $table->string('mata_title')->nullable();
            $table->text('mata_description')->nullable();
            $table->text('mata_keyword')->nullable();
            $table->string('code')->nullable();
            $table->enum('status',['enabled','closed'])->default('enabled');
            $table->integer('fk_create_by')->unsigned()->nullable();
            $table->foreign('fk_create_by')->references('id')->on('users');
            $table->longtext('slug')->nullable();
            $table->timestamps();
        });



        Schema::create('category_post', function (Blueprint $table) {
            $table->unsignedInteger('post_id');
            $table->foreign('post_id')->references('id')->on('article')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade')->onUpdate('cascade');
            $table->unique(['post_id', 'category_id']);
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article');
    }
}
