<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_menu_types')->unsigned();
            $table->foreign('fk_menu_types')->references('id')->on('menu_types');
            $table->string('title')->nullable();
            $table->text('alias')->nullable();
            $table->text('type')->nullable();
            $table->integer('parent_id')->nullable();
            $table->integer('level')->nullable();
            $table->integer('fk_component_id')->unsigned();
            $table->foreign('fk_component_id')->references('id')->on('article');
            $table->integer('access')->nullable();
            $table->string('img')->nullable();
            $table->string('template_style_id')->nullable();
            $table->longtext('menu_sortable')->nullable();
            $table->integer('fk_user_create')->unsigned();
            $table->foreign('fk_user_create')->references('id')->on('users');
            $table->enum('status',['enabled','closed'])->default('enabled');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu');
    }
}
