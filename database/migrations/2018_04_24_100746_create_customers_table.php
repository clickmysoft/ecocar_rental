<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('prefix_name_customer')->nullable();
            $table->string('first_name_customer')->nullable();
            $table->string('last_name_customer')->nullable();
            $table->string('nick_name_customer')->nullable();
            $table->string('email_customer')->nullable();
            $table->string('phone_customer')->nullable();
            $table->string('line_id_customer')->nullable();
            $table->string('id_card_customer')->nullable();
            $table->text('personal_card_address')->nullable();
            $table->text('present_address')->nullable();
            $table->string('company_name')->nullable();
            $table->string('position')->nullable();
            $table->string('experience')->nullable();
            $table->string('tel_office')->nullable();
            $table->text('explain_use_car')->nullable();
            $table->string('province_use_car')->nullable();
            $table->string('prefix_name_beneficiary')->nullable();
            $table->string('first_name_beneficiary')->nullable();
            $table->string('last_name_beneficiary')->nullable();
            $table->string('relation_beneficiary')->nullable();
            $table->string('phone_beneficiary')->nullable();
            $table->string('personal_card_img')->nullable();
            $table->string('driver_license_card_img')->nullable();
            $table->string('credit_card_img')->nullable();
            $table->string('image_profile')->nullable();
            $table->text('customers_description')->nullable();
            
            $table->enum('status',['enabled','closed'])->default('enabled');
            $table->string('username')->unique();
            $table->string('password')->nullable();
            $table->enum('type_register',['customer','employee']);
            $table->timestamps();
            //$table->tinyInteger('active')->default(1)->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
