<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateUsersTable.
 */
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('access.table_names.users'), function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->string('employee_id')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->unique();
            $table->string('nickname')->nullable();
            $table->string('company_name')->nullable();
            $table->string('position_name')->nullable();
            $table->string('belong')->nullable();
            $table->string('avatar_type')->default('gravatar');
            $table->string('avatar_location')->nullable();
            $table->string('user_image')->default('avatars/dufult/avatar12_big.png');
            $table->string('start_work')->nullable();
            $table->string('username')->unique();
            $table->string('password')->nullable();
            $table->timestamp('password_changed_at')->nullable();
            $table->tinyInteger('active')->default(1)->unsigned();
            $table->string('confirmation_code')->nullable();
            $table->boolean('confirmed')->default(config('access.users.confirm_email') ? false : true);
            $table->string('timezone')->default('UTC');
            $table->enum('type_register',['customer','employee']);
            // $table->string('prefix_name_customer')->nullable();
            // $table->string('first_name_customer')->nullable();
            // $table->string('last_name_customer')->nullable();
            // $table->string('nick_name_customer')->nullable();
            // $table->string('email_customer')->nullable();
            // $table->string('phone_customer')->nullable();
            // $table->string('line_id_customer')->nullable();
            // $table->string('id_card_customer')->nullable();
            // $table->text('personal_card_address')->nullable();
            // $table->text('present_address')->nullable();
            // $table->string('position')->nullable();
            // $table->string('experience')->nullable();
            // $table->string('tel_office')->nullable();
            // $table->text('explain_use_car')->nullable();
            // $table->string('province_use_car')->nullable();
            // $table->string('prefix_name_beneficiary')->nullable();
            // $table->string('first_name_beneficiary')->nullable();
            // $table->string('last_name_beneficiary')->nullable();
            // $table->string('relation_beneficiary')->nullable();
            // $table->string('phone_beneficiary')->nullable();
            // $table->string('personal_card_img')->nullable();
            // $table->string('driver_license_card_img')->nullable();
            // $table->string('credit_card_img')->nullable();
            // $table->string('image_profile')->nullable();
            // $table->text('customers_description')->nullable();

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('access.table_names.users'));
    }
}
