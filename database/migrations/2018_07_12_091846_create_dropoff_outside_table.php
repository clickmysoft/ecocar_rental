<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDropoffOutsideTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dropoff_outside', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_area')->nullable();
            $table->string('area_code')->nullable();
            $table->float('price')->nullable();
            $table->enum('in_area',['in_bangkok','out_slide_bangkok']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dropoff_outside');
    }
}
