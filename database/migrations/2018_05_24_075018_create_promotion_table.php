<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image_thumbnail')->nullable();
            $table->string('image_full')->nullable();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('promotion_code')->nullable();
            $table->string('promotion_discount')->nullable();
            $table->enum('discount_type',['percen','bath'])->default('percen');
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->string('mata_title')->nullable();
            $table->text('mata_description')->nullable();
            $table->text('mata_keyword')->nullable();
            $table->enum('status',['enabled','closed'])->default('enabled');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion');
    }
}
