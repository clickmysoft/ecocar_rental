<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraOptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_option', function (Blueprint $table) {
            $table->increments('id');
            $table->string('extra_option_icon')->nullable();
            $table->string('extra_option_name')->nullable();
            $table->string('extra_option_qty')->nullable();
            $table->string('extra_option_price')->nullable();
            $table->string('extra_option_code')->nullable();
            $table->enum('extra_option_per',['day','unit']);
            $table->enum('status',['enabled','closed'])->default('enabled');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra_option');
    }
}
