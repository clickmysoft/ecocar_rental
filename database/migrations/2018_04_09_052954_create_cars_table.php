<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name')->nullable();
            $table->string('branch_code')->nullable();
            $table->string('branch_name')->nullable();
            $table->string('license_plate')->nullable();
            $table->string('license_province')->nullable();
            $table->string('color')->nullable();
            $table->string('body_number')->nullable();
            $table->string('engine_number')->nullable();
            $table->string('branch_car_code')->nullable();
            
            $table->string('car_brand')->nullable();
            $table->string('car_generation')->nullable();
            $table->string('car_nickname')->nullable();
            $table->string('car_generation_detail')->nullable();
            $table->string('car_engine_cc')->nullable();
            $table->string('car_type')->nullable();
            $table->string('car_human_qty')->nullable();
            $table->string('car_baggage_qty')->nullable();
            $table->text('car_type_fule')->nullable();
            $table->string('car_type_gear')->nullable();
            $table->string('car_pressure_front')->nullable();
            $table->string('car_pressure_back')->nullable();
            $table->string('car_mile')->nullable();
            $table->string('car_door_qty')->nullable();
            $table->string('car_image')->nullable();
            $table->text('car_description')->nullable();
            $table->text('car_option')->nullable();
            $table->string('car_company_insurance')->nullable();
            $table->string('car_type_insurance')->nullable();
            $table->date('car_date_register')->nullable();
            $table->date('car_enddate_insurance')->nullable();
            $table->date('car_enddate_act')->nullable();
            $table->date('car_enddate_tax')->nullable();
            $table->timestamps();
            $table->enum('status',['enabled','closed','booking'])->default('enabled');
            $table->float('price')->nullable();
            // $table->tinyInteger('active')->default(1)->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
