<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_booking', function (Blueprint $table) {
            $table->increments('id');
            $table->string('car_booking_code')->nullable();
            $table->integer('fk_car_id')->unsigned();
            $table->foreign('fk_car_id')->references('id')->on('cars');
            $table->integer('fk_invoice')->unsigned();
            $table->foreign('fk_invoice')->references('id')->on('invoices');
            
            $table->text('pick_up')->nullable();
            $table->text('drop_off')->nullable();
            $table->date('pick_up_date')->nullable();
            $table->string('pick_up_time')->nullable();
            $table->date('drop_off_date')->nullable();
            $table->string('drop_off_time')->nullable();
            $table->text('car_option')->nullable();
            $table->text('extra_option_qty')->nullable();
            $table->text('comment_remark')->nullable();
            $table->string('personal_card_id')->nullable();
            $table->string('customer_first_name')->nullable();
            $table->string('customer_last_name')->nullable();
            $table->string('customer_email')->nullable();
            $table->string('customer_phone')->nullable();
            $table->string('customer_flight_info')->nullable();
            $table->string('customer_flight_number')->nullable();
            
            $table->string('driver_license_card_img')->nullable();
            $table->string('credit_card_img')->nullable();
            $table->string('personal_card_img')->nullable();
            $table->string('image_profile')->nullable();
            
            // $table->float('total_car_price')->nullable();
            // $table->float('total_option_price')->nullable();
            // $table->float('total_total_price')->nullable();

            $table->enum('type_pay',['paypal','pay_later']);
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->longtext('data_sesstion')->nullable();

            $table->enum('deposit',['pay','not_pay']);
            $table->integer('fk_staff_approve_deposit')->nullable();
            $table->string('deposit_image')->nullable();
            $table->string('deposit_amount')->nullable();
            $table->string('deposit_pay_date')->nullable();
            $table->string('deposit_pay_time')->nullable();
            $table->text('deposit_remark')->nullable();

            $table->integer('fk_user_booking')->nullable();
            $table->enum('user_register',['no','yes'])->default('no');
            $table->integer('fk_staff_approve_booking')->unsigned()->nullable();
            $table->foreign('fk_staff_approve_booking')->references('id')->on('users');
            $table->enum('status_receive_car',['no','yes'])->default('no');


            $table->string('personal_card_address')->nullable();
            $table->string('present_address')->nullable();
            $table->string('company_name')->nullable();
            $table->string('position')->nullable();
            $table->string('tel_office')->nullable();
            $table->string('id_card_customer')->nullable();
            $table->string('first_name_beneficiary')->nullable();
            $table->string('last_name_beneficiary')->nullable();
            $table->string('relation_beneficiary')->nullable();
            $table->string('phone_beneficiary')->nullable();


            $table->string('return_date')->nullable();
            $table->string('return_time')->nullable();
            $table->string('return_remark')->nullable();
            $table->integer('fk_staff_receive')->unsigned()->nullable();
            $table->foreign('fk_staff_receive')->references('id')->on('users');
            $table->string('return_image')->nullable();
            $table->enum('status_return_car',['return','booking'])->default('booking');

            $table->string('image_paid')->nullable();
            $table->timestamps();
            $table->enum('send_message_before_one_hour',['true','false'])->default('false');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_booking');
    }
}
