<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_booking_id')->unsigned();
            $table->foreign('fk_booking_id')->references('id')->on('cars');
            $table->string('amount')->nullable();
            $table->string('pay_date')->nullable();
            $table->string('image_deposit')->nullable();
            $table->text('remark')->nullable();
            $table->integer('fk_user_create')->unsigned();
            $table->foreign('fk_user_create')->references('id')->on('users');
            $table->enum('deposit',['pay','not_pay']);
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit');
    }
}
