<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_prices', function (Blueprint $table) {
            $table->increments('id');
            // $table->integer('car_prices_id')->unsigned();
            // $table->foreign('car_prices_id')->references('id')->on('cars');
            // $table->string('car_prices_code')->nullable();
            $table->string('company_name')->nullable();
            $table->string('branch_name')->nullable();
            $table->string('car_brand')->nullable();
            $table->string('car_generation')->nullable();
            $table->enum('type_rental_prices',['per_day','per_month','per_year'])->default('per_day');
            $table->string('car_prices');
            $table->enum('status_event',['close','open'])->default('close');
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->enum('status',['enabled','closed'])->default('enabled');
            $table->text('remarks')->nullable();
            $table->enum('priority',['height','medium','low'])->default('height');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_prices');
    }
}
