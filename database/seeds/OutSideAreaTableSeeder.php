<?php

use Illuminate\Database\Seeder;
use App\Models\OutSideArea\OutSideAreaDB;

class OutSideAreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OutSideAreaDB::create([
	           	'name_area' 	=> 'ดอนเมือง',
				'area_code'		=> 'uoSDDYle',
				'price'			=> '535.00',
				'in_area'		=> 'in_bangkok'
	    ]);

	     OutSideAreaDB::create([
	           	'name_area' 	=> 'อ่อนนุช',
				'area_code'		=> 'izLH906r',
				'price'			=> '535.00',
				'in_area'		=> 'in_bangkok'
	    ]);

	     OutSideAreaDB::create([
	           	'name_area' 	=> 'บางหว้า',
				'area_code'		=> 'uzIEA3or',
				'price'			=> '535.00',
				'in_area'		=> 'in_bangkok'
	    ]);

	     OutSideAreaDB::create([
	           	'name_area' 	=> 'ลาดพร้าว',
				'area_code'		=> 'SAHbAkUY',
				'price'			=> '535.00',
				'in_area'		=> 'in_bangkok'
	    ]);

	     OutSideAreaDB::create([
	           	'name_area' 	=> 'นนทบุรี',
				'area_code'		=> 'Z07e1ySa',
				'price'			=> '535.00',
				'in_area'		=> 'in_bangkok'
	    ]);

	     OutSideAreaDB::create([
	           	'name_area' 	=> 'สำโรง',
				'area_code'		=> 'hfNms3YD',
				'price'			=> '535.00',
				'in_area'		=> 'in_bangkok'
	    ]);

	     OutSideAreaDB::create([
	           	'name_area' 	=> 'สนามบินดอนเมือง',
				'area_code'		=> 'TifEfmFR',
				'price'			=> '535.00',
				'in_area'		=> 'in_bangkok'
	    ]);

	     OutSideAreaDB::create([
	           	'name_area' 	=> 'สนามบินสุวรรณภูมิ',
				'area_code'		=> 'ML05GTQO',
				'price'			=> '535.00',
				'in_area'		=> 'in_bangkok'
	    ]);


	    OutSideAreaDB::create([
	           	'name_area' 	=> 'อุบล/สนามบินอุบล',
				'area_code'		=> 'E4Rd5kYV',
				'price'			=> '5350.00',
				'in_area'		=> 'out_slide_bangkok'
	    ]);

	    OutSideAreaDB::create([
	           	'name_area' 	=> 'เชียงใหม่/สนามบินเชียงใหม่',
				'area_code'		=> 'tzRyDFfT',
				'price'			=> '5350.00',
				'in_area'		=> 'out_slide_bangkok'
	    ]);

	    OutSideAreaDB::create([
	           	'name_area' 	=> 'สถานที่อื่นๆ นอกเหนือจากสาขา',
				'area_code'		=> 'TbRREe9u',
				'price'			=> '5350.00',
				'in_area'		=> 'out_slide_bangkok'
	    ]);

	     OutSideAreaDB::create([
	           	'name_area' 	=> 'พัทยา',
				'area_code'		=> 'zrYbZkkK',
				'price'			=> '2140.00',
				'in_area'		=> 'out_slide_bangkok'
	    ]);

    }
}
