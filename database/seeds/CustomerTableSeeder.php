<?php

use Illuminate\Database\Seeder;
use App\Models\Customers\Customers;
use App\Models\Auth\User;
class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    use DisableForeignKeys;

    public function run()
    {
        $this->disableForeignKeys();

        Customers::create([
	           	'prefix_name_customer'		=> '',
				'first_name_customer'		=> 'Admin',
				'last_name_customer'		=> 'Istrator',
				'nick_name_customer'		=> 'Admin - Wat',
				'email_customer'			=> 'admin2@admin.com',
				'phone_customer'			=> '012345678',
				'line_id_customer'			=> '',
				'id_card_customer'			=> mt_rand(1000000000000,9999999999999),
				'personal_card_address'		=> $this->address(),
				'present_address'			=> $this->address(),
				'company_name'				=> 0,
				'position'					=> 100,
				'experience'				=> mt_rand(0,10),
				'tel_office'				=> "0".mt_rand(100000000,999999999),
				'explain_use_car'			=> '',
				'province_use_car'			=> '',
				'prefix_name_beneficiary'	=> '',
				'first_name_beneficiary'	=> '',
				'last_name_beneficiary'		=> '',
				'relation_beneficiary'		=> '',
				'phone_beneficiary'			=> '',
				'personal_card_img'			=> '',
				'driver_license_card_img'	=> '',
				'credit_card_img'			=> '',
				'image_profile'				=> '',
				'customers_description'		=> $this->get_customers_description(),
	            'status'					=> $this->gen_status(),
	            'username'          		=> 'root',
	            'password'          		=> bcrypt('root'),
	            'type_register'				=> 'employee',
	    ]);


	    Customers::create([
	           	'prefix_name_customer'		=> '',
				'first_name_customer'		=> 'Backend',
				'last_name_customer'		=> 'User',
				'nick_name_customer'		=> 'Backend - User',
				'email_customer'			=> 'executive@executive.com',
				'phone_customer'			=> '0854067588',
				'line_id_customer'			=> '',
				'id_card_customer'			=> mt_rand(1000000000000,9999999999999),
				'personal_card_address'		=> $this->address(),
				'present_address'			=> $this->address(),
				'company_name'				=> 0,
				'position'					=> 100,
				'experience'				=> mt_rand(0,10),
				'tel_office'				=> "0".mt_rand(100000000,999999999),
				'explain_use_car'			=> '',
				'province_use_car'			=> '',
				'prefix_name_beneficiary'	=> '',
				'first_name_beneficiary'	=> '',
				'last_name_beneficiary'		=> '',
				'relation_beneficiary'		=> '',
				'phone_beneficiary'			=> '',
				'personal_card_img'			=> '',
				'driver_license_card_img'	=> '',
				'credit_card_img'			=> '',
				'image_profile'				=> '',
				'customers_description'		=> $this->get_customers_description(),
	            'status'					=> $this->gen_status(),
	            'username'          		=> 'customer',
	            'password'          		=> bcrypt('customer'),
	            'type_register'				=> 'employee',
	    ]);

	     Customers::create([
	           	'prefix_name_customer'		=> '',
				'first_name_customer'		=> 'Backend',
				'last_name_customer'		=> 'User',
				'nick_name_customer'		=> 'Default - User',
				'email_customer'			=> 'user@user.com',
				'phone_customer'			=> '0854067588',
				'line_id_customer'			=> '',
				'id_card_customer'			=> mt_rand(1000000000000,9999999999999),
				'personal_card_address'		=> $this->address(),
				'present_address'			=> $this->address(),
				'company_name'				=> 0,
				'position'					=> 100,
				'experience'				=> mt_rand(0,10),
				'tel_office'				=> "0".mt_rand(100000000,999999999),
				'explain_use_car'			=> '',
				'province_use_car'			=> '',
				'prefix_name_beneficiary'	=> '',
				'first_name_beneficiary'	=> '',
				'last_name_beneficiary'		=> '',
				'relation_beneficiary'		=> '',
				'phone_beneficiary'			=> '',
				'personal_card_img'			=> '',
				'driver_license_card_img'	=> '',
				'credit_card_img'			=> '',
				'image_profile'				=> '',
				'customers_description'		=> $this->get_customers_description(),
	            'status'					=> $this->gen_status(),
	            'username'          		=> 'user',
	            'password'          		=> bcrypt('user'),
	            'type_register'				=> 'employee',
	    ]);



        for ($i=4; $i <= 4; $i++) { 
        	 
        	 $email = str_random(10).'@gmail.com';
        	 $fname = $this->get_firstname();
        	 $lname = $this->get_lastname();
        	 $nickname = $this->get_nickname();

	        Customers::create([
	           	'prefix_name_customer'		=> $this->gen_prefix_name(),
				'first_name_customer'		=> $fname,
				'last_name_customer'		=> $lname,
				'nick_name_customer'		=> $nickname,
				'email_customer'			=> $email,
				'phone_customer'			=> "0".mt_rand(100000000,999999999),
				'line_id_customer'			=> $this->get_lastname(),
				'id_card_customer'			=> mt_rand(1000000000000,9999999999999),
				'personal_card_address'		=> $this->address(),
				'present_address'			=> $this->address(),
				'company_name'				=> 0,
				'position'					=> 100,
				'experience'				=> mt_rand(0,10),
				'tel_office'				=> "0".mt_rand(100000000,999999999),
				'explain_use_car'			=> $this->get_explain_use_car(),
				'province_use_car'			=> $this->get_province(),
				'prefix_name_beneficiary'	=> $this->gen_prefix_name(),
				'first_name_beneficiary'	=> $this->get_firstname(),
				'last_name_beneficiary'		=> $this->get_lastname(),
				'relation_beneficiary'		=> $this->gen_relation_beneficiary(),
				'phone_beneficiary'			=> "0".mt_rand(100000000,999999999),
				'personal_card_img'			=> '',
				'driver_license_card_img'	=> '',
				'credit_card_img'			=> '',
				'image_profile'				=> '',
				'customers_description'		=> $this->get_customers_description(),
	            'status'					=> $this->gen_status(),
	            'username'          		=> $email,
	            'password'          		=> bcrypt($email),
	            'type_register'     		=> 'customer',
	        ]);

	        User::create([
	            'employee_id'       => mt_rand(1000,9999),
	            'first_name'        => $fname,
	            'last_name'         => $lname,
	            'phone'             => '012345678',
	            'email'             => $email,
	            'nickname'          => $nickname,
	            'company_name'      => 0,
	            'position_name'     => 100,
	            'belong'            => 'Web Dev',
	            'start_work'        => '01-08-2017',
	            'username'          => $email,
	            'password'          => bcrypt($email),
	            'confirmation_code' => md5(uniqid(mt_rand(), true)),
	            'confirmed'         => true,
        	]);

        	User::find($i)->assignRole(config('access.users.default_role'));

        }
        
        $this->enableForeignKeys();

    }


     public function gen_status(){
        $status=array('enabled','closed');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }

    public function gen_relation_beneficiary(){
    	$relationships = array("father","mother","son","daughter","พ่อ","แม่","เมียน้อย","เมียใหม่","กิ๊ก");
    	$random_relationships = array_rand($relationships,1);
    	return $relationships[$random_relationships];
    }

    public function gen_prefix_name(){
    	$arrayName = array('Mr.','Mrs.','Ms.','Miss.','Dr.');
    	$random_arrayName = array_rand($arrayName,1);
    	return $arrayName[$random_arrayName];
    	
    }


    public function get_province(){
    	 $arrayName =array("กระบี่","กรุงเทพมหานคร","กาญจนบุรี","กาฬสินธุ์","กำแพงเพชร","ขอนแก่น","จันทบุรี","ฉะเชิงเทรา" ,"ชลบุรี","ชัยนาท","ชัยภูมิ","ชุมพร","เชียงราย","เชียงใหม่","ตรัง","ตราด","ตาก","นครนายก","นครปฐม","นครพนม","นครราชสีมา" ,"นครศรีธรรมราช","นครสวรรค์","นนทบุรี","นราธิวาส","น่าน","บุรีรัมย์","ปทุมธานี","ประจวบคีรีขันธ์","ปราจีนบุรี","ปัตตานี" ,"พะเยา","พังงา","พัทลุง","พิจิตร","พิษณุโลก","เพชรบุรี","เพชรบูรณ์","แพร่","ภูเก็ต","มหาสารคาม","มุกดาหาร","แม่ฮ่องสอน" ,"ยโสธร","ยะลา","ร้อยเอ็ด","ระนอง","ระยอง","ราชบุรี","ลพบุรี","ลำปาง","ลำพูน","เลย","ศรีสะเกษ","สกลนคร","สงขลา" ,"สตูล","สมุทรปราการ","สมุทรสงคราม","สมุทรสาคร","สระแก้ว","สระบุรี","สิงห์บุรี","สุโขทัย","สุพรรณบุรี","สุราษฎร์ธานี" ,"สุรินทร์","หนองคาย","หนองบัวลำภู","อยุธยา","อ่างทอง","อำนาจเจริญ","อุดรธานี","อุตรดิตถ์","อุทัยธานี","อุบลราชธานี");
    	$random_arrayName = array_rand($arrayName,1);
    	return $arrayName[$random_arrayName];
    }

    public function get_firstname(){
    	$arrayName = array(
		    'Christopher',
		    'Ryan',
		    'Ethan',
		    'John',
		    'Zoey',
		    'Sarah',
		    'Michelle',
		    'Samantha',
		);
		$random_arrayName = array_rand($arrayName,1);
    	return $arrayName[$random_arrayName];
    }

    public function get_lastname(){
    	$arrayName = array(
		    'Walker',
		    'Thompson',
		    'Anderson',
		    'Johnson',
		    'Tremblay',
		    'Peltier',
		    'Cunningham',
		    'Simpson',
		    'Mercado',
		    'Sellers'
		);

		$random_arrayName = array_rand($arrayName,1);
    	return $arrayName[$random_arrayName];
    }

    public function get_nickname(){
    	$arrayName = array(
		    'locate',
		    'slip',
		    'touch',
		    'stroke',
		    'criticise',
		    'scatter',
		    'rain',
		    'tremble',
		    'contact',
		    'warm'
		);

		$random_arrayName = array_rand($arrayName,1);
    	return $arrayName[$random_arrayName];
    }

    public function address(){
    	$arrayName = array(
		    '<p>ถนน ประชาอุทิศ แขวง สามเสนนอก เขต ห้วยขวาง กรุงเทพมหานคร 10310</p>',
		    '<p>63 อาคาร Fitness 7 ซอย รัชดาภิเษก 7 รัชดาภิเษก แขวง ดินแดง เขต ดินแดง กรุงเทพมหานคร 10400</p>',
		    '<p>125 ถนนรัชดาภิเษก แขวง ดินแดง เขต ดินแดง กรุงเทพมหานคร 10320</p>',
		    '<p>เลขที่ 121 ถนนรัชดาภิเษก แขวง ดินแดง เขต ดินแดง กรุงเทพมหานคร 10400</p>',
		    '<p>99 ถนนรัชดาภิเษก แขวง ดินแดง เขต ดินแดง กรุงเทพมหานคร 10400</p>',
		    '<p>18 ถนน เทียมร่วมมิตร แขวง ห้วยขวาง เขต ห้วยขวาง กรุงเทพมหานคร 10310</p>',
		    '<p>648 ซอย ชานเมือง แขวง ดินแดง เขต ดินแดง กรุงเทพมหานคร 10400</p>',
		    '<p>522/151 ถนน อโศก - ดินแดง แขวง ดินแดง เขต ดินแดง กรุงเทพมหานคร 10400</p>',
		    '<p>166/1-6 ถนน รัชดา 10 แขวง ห้วยขวาง เขต ห้วยขวาง กรุงเทพมหานคร 10320</p>',
		    '<p>6 ซอย หัสดีเสวี แขวง ห้วยขวาง เขต ห้วยขวาง กรุงเทพมหานคร 10310</p>'
		);
		$random_arrayName = array_rand($arrayName,1);
    	return $arrayName[$random_arrayName];

    }

    public function get_position(){
    	$arrayName = array(
		    'Executive Secretary',
		    'Office Assistant',
		    'Sales Administrator',
		    'Administrative Manager',
		    'Flight Attendances',
		    'Ground Crew',
		    'Pilot',
		    'tremble',
		    'contact',
		    'Operator',
		    'Receptionist',
		    'Customer Relationship',
		    'Customer Services Officer',
		    'Bill Collector',
		    'Driver',
		    'Messenger',
		    'Design Engineer',
		    'Electronics Engineer'
		);

		$random_arrayName = array_rand($arrayName,1);
    	return $arrayName[$random_arrayName];
    }

    function get_explain_use_car(){
    	$arrayName = array(
		    '<p>เพื่อไปเที่ยวสแกนดิเนเวียเทกซัสยุโรปลิสบอนจีน รีโอเดจาเนโร หลวงพระบางเวอร์มอนต์ดีทรอยต์มถุรา</p>',
		    '<p>เพื่อไปเที่ยวเวียนนา เดนมาร์กแมรีแลนด์กาบอง เจนไนแอฟริกากุสินาราตูวาลู</p>',
		    '<p>เพื่อไปเที่ยวเตหะรานเบลเยียมโรดไอแลนด์ บริติชโรมาเนียดูไบ โซโลมอน</p>',
		    '<p>เพื่อไปเที่ยวซาราวักเทลอาวีฟโรดไอแลนด์ ฮาราเรกาฐมาณฑุ สารนาถฮ่องกงซีเรีย</p>',
		    '<p>เพื่อไปเที่ยวอิรวดีเซเนกัลอลาสกาแคลิฟอร์เนียอินโดนีเซีย โอลิมปัสเซี่ยงไฮ้โดเวอร์เทลอาวีฟ ไอซ์แลนด์เทนเนสซีลุยเซียนาดาโคตาอิสราเอล</p>',
		    '<p>เพื่อไปเที่ยวบราซิลสวิตเซอร์แลนด์ชาดไวโอมิง แอริโซนา ตะนาวศรีลิทัวเนียโดมินิกันจำปาศักดิ์จาเมกา</p>',
		    '<p>เพื่อไปเที่ยวโปแลนด์พาราณสีฮอนโนลูลู นิโคบาร์ลูเซียนาอูรูบริติช โปรตุเกสบริติชเซเนกัล</p>',
		    '<p>เพื่อไปเที่ยวสก็อตแลนด์แอลเจียร์ โลซานน์เท็กซัสมอนเตเนโกรเฮติย่างกุ้ง ไมโครนีเซียเอสโตเนียเฮติ</p>',
		    '<p>เพื่อไปเที่ยวเซเชลส์ โมนาโก ตะนาวศรีเนวาดาจิบูตีบาร์บูดา</p>',
		    '<p>เพื่อไปเที่ยวเอธิโอเปียอิสตันบูลมินดาเนา เวสาลี เม็กซิโก</p>'
		);
		$random_arrayName = array_rand($arrayName,1);
    	return $arrayName[$random_arrayName];

    }

    function get_customers_description(){
    	$arrayName = array(
		    '<p>ติงต๊องบรารีเสิร์ช เวิร์กช็อปแซนด์วิช เวสต์เยอร์บีร่าจัมโบ้ นิรันดร์งั้น โอเลี้ยงเวิร์ลด์ บอดี้พาเหรดคีตกวีเซี้ยว บอยคอตต์จูเนียร์จูนบอกซ์มอคคา รีสอร์ตอีแต๋นฮิ ไมค์ รามเทพออดิทอเรียมเพรส โปรโมทวอล์คถูกต้อง สป็อตแจ๊สเพทนาการโอเวอร์เจ๊ ออโต้สเปกโหงวเฮ้งโมเดิร์นเซอร์ มอคคาภารตะ คอนเซ็ปต์เซนเซอร์ แดนเซอร์พุทธภูมิวอลนัตบ๋อยบอดี้</p>',
		    '<p>คันถธุระ บัสวาซาบิคอลเล็กชั่นเสกสรรค์ สแควร์ฟีเวอร์สป็อต ปูอัดฮิไหร่ดัมพ์ บลอนด์บรรพชน ไฮกุแชมปิยองวอเตอร์อุปัทวเหตุแพนงเชิญ วิภัชภาควิกสเก็ตช์ บาลานซ์วิภัชภาคเอ็กซ์โปถ่ายทำยูโร น็อกเบอร์รี เอ๋ออีสต์เย้วสป็อตจตุคาม วาริชศาสตร์นายพรานสหัชญาณแซ็กโซโฟน ออร์แกนเซลส์แมน พรีเมียร์โอเพ่น เบอร์เกอร์ อุปนายกเทอร์โบกระดี๊กระด๊าแอนด์แคชเชียร์ แชมปิยอง</p>',
		    '<p>ซาดิสม์สแตนเลสอัลบั้มบราวีไอพี บลอนด์โดนัทจ๊าบ แฟรีแซ็กโอ้ยรีไทร์ไทม์ คีตราชันยูวีดิสเครดิตมาร์ก จังโก้แรลลี่โมเดิร์นเอเซียสปอร์ต โบรชัวร์มาร์เก็ต คอนแท็ค สกรัมฟอร์มสไตล์ออร์เดอร์ ศากยบุตรอินเตอร์พาสต้าออดิชั่นสแตนดาร์ด โครนาจุ๊ยสไตรค์รองรับ บรรพชน โดมิโนโอเวอร์โฟนบูติครีสอร์ต จูนสต็อคโดมิโนใช้งาน เซ็นทรัล โปรเจ็กต์คาเฟ่โหงวเฮ้ง ซานตาคลอสวโรกาสเซลส์โบ้ยเกมส์</p>',
		    '<p>ซะ วัคค์ชัวร์พีเรียดชัตเตอร์ ปาสเตอร์ออทิสติกเคลื่อนย้ายอพาร์ตเมนท์แรลลี ง่าวแมนชั่นสเตย์หงวนซะ แรงดูดพาร์ไฮไลท์เบิร์ด ชิฟฟอนพุดดิ้งแบนเนอร์สตริงรีไทร์ ใช้งานเหมย โหลยโท่ย อีโรติกปอดแหกบริกรทอร์นาโด เซี้ยวแอโรบิคบอร์ดพ่อค้า เอนทรานซ์แอโรบิค ดอกเตอร์คอนโดเนิร์สเซอรี่จิ๊กเมจิค เมจิกรายชื่อเทอร์โบ แหม็บ พฤหัสน็อค หงวนออยล์</p>',
		    '<p>แมชชีนเพนตากอนสุริยยาตร์แครกเกอร์อุปการคุณ ล้มเหลวสมาพันธ์ครูเสด ฟลุทซีดานบูติคทัวร์นาเมนท์วิป อพาร์ทเมนต์บาร์บีคิวสปายว้อยแกงค์ นินจาไหร่สเตอริโอ ไฮเทคดิสเครดิตออร์แกน จ๊อกกี้ริคเตอร์ฮ็อตแอโรบิค ยูวีบลอนด์ดอกเตอร์โบกี้หล่อฮังก้วย โบ้ยอันเดอร์ ผ้าห่มวิทย์อะเคลียร์อะ โง่เขลาโชว์รูมเบบี้ รีวิว เต๊ะ วอร์รูมคอร์รัปชั่นน้องใหม่ฮีโร่เนอะ ปักขคณนาไวอากร้าแกสโซฮอล์สติ๊กเกอร์ แคนูรากหญ้าพอเพียงโอเพ่นซัพพลาย</p>',
		    '<p>ฮ็อตโบว์ปัจฉิมนิเทศ รีพอร์ทแฮปปี้ซิงแชมเปญ แบคโฮ สแตนดาร์ดดีพาร์ตเมนต์วานิลลาฮิแจ๊ส เอเซียคาราโอเกะสต๊อกเอสเพรสโซแจ๊ส มาเฟียฮ็อตด็อกรากหญ้าเวิร์ลด์ คอรัปชั่นเซอร์ไพรส์ภควัมบดีรีเสิร์ช โรแมนติคฉลุยจิ๊กเลสเบี้ยนว้อดก้า โปรเจ็คท์เทรลเล่อร์ สังโฆเพทนาการเดบิตอพาร์ทเมนต์ มาร์กเทคโนจิ๊กโก๋วีซ่าชีส ติ่มซำสโรชากิมจิทีวี ภควัมบดีซูเอี๋ยเซอร์วิสทิป ซีอีโอ วาริชศาสตร์โอเคแคนยอน ปาสคาลรีทัช</p>',
		    '<p>เรซินฟลุกสเตริโอ อัลมอนด์ป่าไม้รัม แมชชีน อาร์พีจีโลโก้เพลย์บอยวินเมี่ยงคำ นิวส์เคลียร์โปรดักชั่นสะเด่าพาสปอร์ต บอมบ์ แฟนซีดีพาร์ตเมนท์คอนเซ็ปต์ จิ๊กโก๋วานิลา คอนแทคเอสเปรสโซฮ็อตด็อกอีสต์โฟม ทริปวอลล์ คอมพ์ จตุคามควีนบุ๋น สุนทรีย์ซิมโฟนี่อมาตยาธิปไตยดีพาร์ทเมนต์หลวงปู่ ป่าไม้ท็อปบู๊ท ผ้าห่ม ลอจิสติกส์เต๊ะธุหร่ำพุทธศตวรรษ</p>',
		    '<p>สปาซาฟารีสันทนาการเอฟเฟ็กต์เนิร์สเซอรี ภคันทลาพาธโคโยตี้เรซินแชมเปี้ยนแคมปัส แฟรีโอเปอเรเตอร์เลคเชอร์ มอคคาเนิร์สเซอรี่ สเตริโอ ติว เมคอัพ อึ้มแฟ็กซ์แบล็กสุนทรีย์ ปัจฉิมนิเทศ เอ็นจีโอแรงใจแม็กกาซีนแบดแบด รันเวย์ จิ๊กโก๋อุรังคธาตุคาร์โก้ ว่ะแกรนด์แพลนขั้นตอนสป็อต ศึกษาศาสตร์ คอรัปชันแจ็กเก็ต ไฮกุหลวงพี่</p>',
		    '<p>วีเจไลท์เก๋ากี้แจ็กพอตแอปพริคอท โอเปอเรเตอร์สต็อค เซลส์ ซิตี้สุนทรีย์โมเต็ล ศิรินทร์ซาบะหมิงราสเบอร์รีคีตกวี เพรสสกายมหภาคปูอัด คาแรคเตอร์ศิลปวัฒนธรรมภคันทลาพาธ แรลลี่ เดโมฮากกาสต๊อค โหลนเบบี้พาสต้าชัตเตอร์ วีเจ ธุหร่ำอัลตราคอนแทค พาวเวอร์เรตติ้งมลภาวะ เซ่นไหว้ม้านั่ง ช็อปปิ้งเจ็ตมอบตัวอิกัวนารีทัช มินท์ปิกอัพชนะเลิศคาแร็คเตอร์แพทยสภา</p>',
		    '<p>การที่เที่ยวเอธิโอเปียอิสตันบูลมินดาเนา เวสาลี เม็กซิโก</p>'
		);
		$random_arrayName = array_rand($arrayName,1);
    	return $arrayName[$random_arrayName];
    }

    

}
