<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    use TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call([

                AuthTableSeeder::class,
                CustomerTableSeeder::class,
                CarTableSeeder::class,
                //ArticlesTableSeeder::class,
                PromotionTableSeeder::class,
                ExtraOptionTableSeeder::class,
                OutSideAreaTableSeeder::class
            ]);

        Model::reguard();
    }
}
