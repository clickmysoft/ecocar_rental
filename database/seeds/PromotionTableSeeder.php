<?php

use Illuminate\Database\Seeder;
use App\Models\Promotion\PromotionModel;
class PromotionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        	PromotionModel::create([
    				'image_thumbnail'		=> '24u3wS_thumbnail.jpg',
					'image_full'			=> '24u3wS_thumbnail.jpg',
					'title'					=> 'Nissan Almera 1.2 AT',
					'description'			=> $this->gen_des_1(),
					'promotion_code'		=> 'ecocar_abde', 
					'promotion_discount'	=> '900',
					'discount_type'			=> 'bath',
					'start_date'			=> '2018-05-31 00:00:00',
					'end_date'				=> '2018-06-09 00:00:00',
					'mata_title'			=> 'Meta title',
					'mata_description'		=> 'Meta Descriptio',
					'mata_keyword'			=> 'Meta Keywords',
					'status'				=> $this->gen_status()
	        ]);

            PromotionModel::create([
                    'image_thumbnail'       => 'Toyota-vios1-5.jpg',
                    'image_full'            => 'Toyota-vios1-5.jpg',
                    'title'                 => 'Toyota Vios 1.5 AT',
                    'description'           => $this->gen_des_2(),
                    'promotion_code'        => 'Ecocar-Toyota-Vios-15', 
                    'promotion_discount'    => '1000',
                    'discount_type'         => 'bath',
                    'start_date'            => '2018-06-01 00:00:00',
                    'end_date'              => '2018-06-29 00:00:00',
                    'mata_title'            => 'Meta title',
                    'mata_description'      => 'Meta Descriptio',
                    'mata_keyword'          => 'Meta Keywords',
                    'status'                => $this->gen_status()
            ]);

            PromotionModel::create([
                    'image_thumbnail'       => 'Nisaan-sylphy1-6at.jpg',
                    'image_full'            => 'Nisaan-sylphy1-6at.jpg',
                    'title'                 => 'Nissan Sylphy 1.6 AT',
                    'description'           => $this->gen_des_3(),
                    'promotion_code'        => 'ecocar_Nissan-Sylphy', 
                    'promotion_discount'    => '1500',
                    'discount_type'         => 'bath',
                    'start_date'            => '2018-06-10 00:00:00',
                    'end_date'              => '2018-07-05 00:00:00',
                    'mata_title'            => 'Meta title',
                    'mata_description'      => 'Meta Descriptio',
                    'mata_keyword'          => 'Meta Keywords',
                    'status'                => $this->gen_status()
            ]);

            PromotionModel::create([
                    'image_thumbnail'       => 'Nissan-teana2-0at.jpg',
                    'image_full'            => 'Nissan-teana2-0at.jpg',
                    'title'                 => 'Nissan Teana 2.0 AT',
                    'description'           => $this->gen_des_4(),
                    'promotion_code'        => 'ecocar_Nissan-Teana', 
                    'promotion_discount'    => '2000',
                    'discount_type'         => 'bath',
                    'start_date'            => '2018-06-01 00:00:00',
                    'end_date'              => '2018-07-01 00:00:00',
                    'mata_title'            => 'Meta title',
                    'mata_description'      => 'Meta Descriptio',
                    'mata_keyword'          => 'Meta Keywords',
                    'status'                => $this->gen_status()  
            ]);

            PromotionModel::create([
                    'image_thumbnail'       => 'Isuzu-mux-1-9at.jpg',
                    'image_full'            => 'Isuzu-mux-1-9at.jpg',
                    'title'                 => 'Isuzu MU X-1.9 AT',
                    'description'           => $this->gen_des_5(),
                    'promotion_code'        => 'ecocar_Isuzu-Mu-X-1.9-AT', 
                    'promotion_discount'    => '600',
                    'discount_type'         => 'bath',
                    'start_date'            => '2018-06-11 00:00:00',
                    'end_date'              => '2018-06-28 00:00:00',
                    'mata_title'            => 'Meta title',
                    'mata_description'      => 'Meta Descriptio',
                    'mata_keyword'          => 'Meta Keywords',
                    'status'                => $this->gen_status()  
            ]);

            PromotionModel::create([
                    'image_thumbnail'       => 'Isuzu-mux-1-9at.jpg',
                    'image_full'            => 'Isuzu-mux-1-9at.jpg',
                    'title'                 => 'Isuzu MU X-1.9 AT',
                    'description'           => $this->gen_des_6(),
                    'promotion_code'        => 'ecocar_Isuzu-Mu-X-1.9-AT', 
                    'promotion_discount'    => '600',
                    'discount_type'         => 'bath',
                    'start_date'            => '2018-06-11 00:00:00',
                    'end_date'              => '2018-06-28 00:00:00',
                    'mata_title'            => 'Meta title',
                    'mata_description'      => 'Meta Descriptio',
                    'mata_keyword'          => 'Meta Keywords',
                    'status'                => $this->gen_status()  
            ]);

            PromotionModel::create([
                    'image_thumbnail'       => 'Isuzu-mux-1-9at.jpg',
                    'image_full'            => 'Isuzu-mux-1-9at.jpg',
                    'title'                 => 'Isuzu MU X-1.9 AT',
                    'description'           => $this->gen_des_5(),
                    'promotion_code'        => 'ecocar_Isuzu-Mu-X-1.9-AT', 
                    'promotion_discount'    => '600',
                    'discount_type'         => 'bath',
                    'start_date'            => '2018-06-11 00:00:00',
                    'end_date'              => '2018-06-28 00:00:00',
                    'mata_title'            => 'Meta title',
                    'mata_description'      => 'Meta Descriptio',
                    'mata_keyword'          => 'Meta Keywords',
                    'status'                => $this->gen_status()  
            ]);

             PromotionModel::create([
                    'image_thumbnail'       => 'Isuzu-mux-1-9at.jpg',
                    'image_full'            => 'Isuzu-mux-1-9at.jpg',
                    'title'                 => 'Isuzu MU X-1.9 AT',
                    'description'           => $this->gen_des_5(),
                    'promotion_code'        => 'ecocar_Isuzu-Mu-X-1.9-AT', 
                    'promotion_discount'    => '600',
                    'discount_type'         => 'bath',
                    'start_date'            => '2018-06-11 00:00:00',
                    'end_date'              => '2018-06-28 00:00:00',
                    'mata_title'            => 'Meta title',
                    'mata_description'      => 'Meta Descriptio',
                    'mata_keyword'          => 'Meta Keywords',
                    'status'                => $this->gen_status()  
            ]);

    }
    public function gen_status(){
        $status=array('enabled','closed');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }

    public function gen_des_1(){
        $status=array('<p>เช่ารถ Nissan Almera 1.2 AT กับเราระหว่างวันที่ 31 พฤษาคม 2561 - 9 มิถุนายน 2561 รับส่วนลด 900 บาท เพียงใช้รหัสโปรโมชั่น ตามที่ขึ้นให้ด้านล่างนี้</p>');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }
    public function gen_des_2(){
        $status=array('<p>เช่ารถ Toyota Vios 1.5 AT กับเราระหว่างวันที่ 1 มิถุนายน 2561 - 29 มิถุนายน 2561 รับส่วนลด 1,000 บาท เพียงใช้รหัสโปรโมชั่น ตามที่ขึ้นให้ด้านล่างนี้</p>');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }
    public function gen_des_3(){
        $status=array('<p>เช่ารถ Nissan Sylphy 1.6 AT กับเราระหว่างวันที่ 10 มิถุนายน 2561 - 5 กรกฏาคม 2561 รับส่วนลด 1,500 บาท เพียงใช้รหัสโปรโมชั่น ตามที่ขึ้นให้ด้านล่างนี้/p>');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }
    public function gen_des_4(){
        $status=array('<p>เช่ารถ Nissan Teana 2.0 AT กับเราระหว่างวันที่ 1 มิถุนายน 2561 - 1 กรกฏาคม 2561 รับส่วนลด 2,000 บาท เพียงใช้รหัสโปรโมชั่น ตามที่ขึ้นให้ด้านล่างนี</p>');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }
    public function gen_des_5(){
        $status=array('<p>เช่ารถ Isuzu MU X-1.9 AT กับเราระหว่างวันที่ 11 มิถุนายน 2561 - 8 มิถุนายน 2561 รับส่วนลด 600 บาท เพียงใช้รหัสโปรโมชั่น ตามที่ขึ้นให้ด้านล่างนี</p>');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }
    public function gen_des_6(){
        $status=array('<p>เช่ารถ Isuzu MU X-1.9 AT กับเราระหว่างวันที่ 11 มิถุนายน 2561 - 8 มิถุนายน 2561 รับส่วนลด 600 บาท เพียงใช้รหัสโปรโมชั่น ตามที่ขึ้นให้ด้านล่างนี</p>');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }
}
