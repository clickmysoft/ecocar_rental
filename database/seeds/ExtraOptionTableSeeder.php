<?php

use Illuminate\Database\Seeder;
use App\Models\ExtraOption\ExtraOptionDB;
class ExtraOptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     use DisableForeignKeys;
    public function run()
    {
    	 $this->disableForeignKeys();
        ExtraOptionDB::create([
	           	'extra_option_icon' 	=> 'frontend/img/gps-icon.png',
				'extra_option_name'		=> 'GPS',
				'extra_option_qty'		=> '1',
				'extra_option_price'	=> '200',
				'extra_option_per'		=> 'day',
				'extra_option_code'		=> 'AA112233',
				'status'				=> 'enabled'
	    ]);

	    ExtraOptionDB::create([
	           	'extra_option_icon' 	=> 'frontend/img/baby-icons-fs.png',
				'extra_option_name'		=> 'Baby Seat',
				'extra_option_qty'		=> '5',
				'extra_option_price'	=> '200',
				'extra_option_per'		=> 'day',
				'extra_option_code'		=> 'AA112123',
				'status'				=> 'enabled'
	    ]);


	    ExtraOptionDB::create([
	           	'extra_option_icon' 	=> 'frontend/img/icon-safety.png',
				'extra_option_name'		=> 'SLDW (Super Loss Damage Waiver)',
				'extra_option_qty'		=> '1',
				'extra_option_price'	=> '200',
				'extra_option_per'		=> 'day',
				'extra_option_code'		=> 'AA11555',
				'status'				=> 'enabled'
	    ]);

	    ExtraOptionDB::create([
	           	'extra_option_icon' 	=> 'frontend/img/rent-a-car-white.png',
				'extra_option_name'		=> 'Delivery Service Car',
				'extra_option_qty'		=> '1',
				'extra_option_price'	=> '500',
				'extra_option_per'		=> 'unit',
				'extra_option_code'		=> 'AA112209',
				'status'				=> 'enabled'
	    ]);

	    ExtraOptionDB::create([
	           	'extra_option_icon' 	=> 'frontend/img/transit-car-return-white.png',
				'extra_option_name'		=> 'Service Return A Car',
				'extra_option_qty'		=> '1',
				'extra_option_price'	=> '500',
				'extra_option_per'		=> 'unit',
				'extra_option_code'		=> 'AA313233',
				'status'				=> 'enabled'
	    ]);


    }
}
