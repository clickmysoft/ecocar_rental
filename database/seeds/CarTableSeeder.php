<?php

use Illuminate\Database\Seeder;
use App\Models\Cars\Description\Cars;

class CarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    use DisableForeignKeys;


    //เพ่ิมเติม
    //branch_name   ไฟล์ company.php
    //branch_code   ไฟล์ branch.php
    public function run()
    {

        $this->disableForeignKeys();

        //// Create Data ////
        // $this->dummy_data();

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa05',
                'license_plate'         => '5กร5320',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0581194',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 90977,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => $this->change_date("22/11/2559"),
                'car_enddate_insurance' => $this->change_date("11/8/2561"),
                'car_enddate_act'       => $this->change_date("11/8/2561"),
                'car_enddate_tax'       => $this->change_date("22/11/2561"),
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa06',
                'license_plate'         => '5กร5313',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0581993',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 69551,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-22',
                'car_enddate_insurance' => $this->change_date("11/8/2561"),
                'car_enddate_act'       => $this->change_date("11/8/2561"),
                'car_enddate_tax'       => $this->change_date("22/11/2561"),
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa07',
                'license_plate'         => '5กย6820',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0583456',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 73235,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-8',
                'car_enddate_insurance' => '2018-8-31',
                'car_enddate_act'       => '2018-8-31',
                'car_enddate_tax'       => '2018-11-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa08',
                'license_plate'         => '5กย6821',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0853457',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 82162,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-8',
                'car_enddate_insurance' => '2018-8-31',
                'car_enddate_act'       => '2018-8-31',
                'car_enddate_tax'       => '2018-11-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa09',
                'license_plate'         => '5กย6835',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0583468',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 47954,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-8',
                'car_enddate_insurance' => '2018-8-31',
                'car_enddate_act'       => '2018-8-31',
                'car_enddate_tax'       => '2018-11-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa10',
                'license_plate'         => '5กร8521',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0583486',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 78024,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-28',
                'car_enddate_insurance' => '2018-9-2',
                'car_enddate_act'       => '2018-9-2',
                'car_enddate_tax'       => '2018-11-27',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa11',
                'license_plate'         => '5กย6831',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0583484',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 81305,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-8',
                'car_enddate_insurance' => '2018-9-2',
                'car_enddate_act'       => '2018-9-2',
                'car_enddate_tax'       => '2018-11-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa12',
                'license_plate'         => '5กย6825',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0583509',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 70404,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-9-2',
                'car_enddate_insurance' => '2018-9-2',
                'car_enddate_act'       => '2018-9-2',
                'car_enddate_tax'       => '2018-11-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง


            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa12',
                'license_plate'         => '5กย6825',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0583509',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 70404,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-9-2',
                'car_enddate_insurance' => '2018-9-2',
                'car_enddate_act'       => '2018-9-2',
                'car_enddate_tax'       => '2018-11-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง


            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa13',
                'license_plate'         => '5กย6825',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0583539',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 73497,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-8',
                'car_enddate_insurance' => '2018-9-9',
                'car_enddate_act'       => '2018-9-9',
                'car_enddate_tax'       => '2018-11-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa14',
                'license_plate'         => '5กย6822',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0583500',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 99164,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-8',
                'car_enddate_insurance' => '2018-9-9',
                'car_enddate_act'       => '2018-9-9',
                'car_enddate_tax'       => '2018-11-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa15',
                'license_plate'         => '5กย5423',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0583574',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 99014,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-4',
                'car_enddate_insurance' => '2018-9-9',
                'car_enddate_act'       => '2018-9-9',
                'car_enddate_tax'       => '2018-11-4',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa16',
                'license_plate'         => '5กร1641',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MRHGM6620HP102458',
                'engine_number'         => str_random(10),
                'car_brand'             => 'honda',
                'car_generation'        => 'city',
                'car_nickname'          => 'city_y14_18',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 35,
                'car_pressure_back'     => 33,
                'car_mile'              => 76667,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-15',
                'car_enddate_insurance' => '2018-10-27',
                'car_enddate_act'       => '2018-10-27',
                'car_enddate_tax'       => '2016-11-15',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa17',
                'license_plate'         => '5กร1646',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MRHGM6620HP102450',
                'engine_number'         => str_random(10),
                'car_brand'             => 'honda',
                'car_generation'        => 'city',
                'car_nickname'          => 'city_y14_18',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 35,
                'car_pressure_back'     => 33,
                'car_mile'              => 61517,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-15',
                'car_enddate_insurance' => '2018-10-27',
                'car_enddate_act'       => '2018-10-27',
                'car_enddate_tax'       => '2016-11-15',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa18',
                'license_plate'         => '5กร1640',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MRHGM6620HR102764',
                'engine_number'         => str_random(10),
                'car_brand'             => 'honda',
                'car_generation'        => 'city',
                'car_nickname'          => 'city_y14_18',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 35,
                'car_pressure_back'     => 33,
                'car_mile'              => 77237,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-15',
                'car_enddate_insurance' => '2018-10-27',
                'car_enddate_act'       => '2018-10-27',
                'car_enddate_tax'       => '2016-11-15',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa19',
                'license_plate'         => '5กร1645',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MRHGE6620HR104304',
                'engine_number'         => str_random(10),
                'car_brand'             => 'honda',
                'car_generation'        => 'city',
                'car_nickname'          => 'city_y14_18',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 35,
                'car_pressure_back'     => 33,
                'car_mile'              => 63612,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-15',
                'car_enddate_insurance' => '2018-10-27',
                'car_enddate_act'       => '2018-10-27',
                'car_enddate_tax'       => '2016-11-15',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa20',
                'license_plate'         => '5กร4092',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MRHGM6620HP102573',
                'engine_number'         => str_random(10),
                'car_brand'             => 'honda',
                'car_generation'        => 'city',
                'car_nickname'          => 'city_y14_18',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 35,
                'car_pressure_back'     => 33,
                'car_mile'              => 58132,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-18',
                'car_enddate_insurance' => '2018-11-3',
                'car_enddate_act'       => '2018-11-3',
                'car_enddate_tax'       => '2016-11-18',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa21',
                'license_plate'         => '5กร4095',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MRHGM6620HP103593',
                'engine_number'         => str_random(10),
                'car_brand'             => 'honda',
                'car_generation'        => 'city',
                'car_nickname'          => 'city_y14_18',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 35,
                'car_pressure_back'     => 33,
                'car_mile'              => 62716,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-18',
                'car_enddate_insurance' => '2018-11-3',
                'car_enddate_act'       => '2018-11-3',
                'car_enddate_tax'       => '2018-11-18',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa22',
                'license_plate'         => '5กร4094',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MRHGM6620HP102770',
                'engine_number'         => str_random(10),
                'car_brand'             => 'honda',
                'car_generation'        => 'city',
                'car_nickname'          => 'city_y14_18',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 35,
                'car_pressure_back'     => 33,
                'car_mile'              => 61944,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-18',
                'car_enddate_insurance' => '2018-11-3',
                'car_enddate_act'       => '2018-11-3',
                'car_enddate_tax'       => '2018-11-18',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa23',
                'license_plate'         => '5กร4093',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MRHGM6620HP103585',
                'engine_number'         => str_random(10),
                'car_brand'             => 'honda',
                'car_generation'        => 'city',
                'car_nickname'          => 'city_y14_18',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 35,
                'car_pressure_back'     => 33,
                'car_mile'              => 73214,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-18',
                'car_enddate_insurance' => '2016-11-3',
                'car_enddate_act'       => '2018-11-3',
                'car_enddate_tax'       => '2018-11-18',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa24',
                'license_plate'         => '6กค-1766',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0586549',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 40426,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-3-20',
                'car_enddate_insurance' => '2018-12-22',
                'car_enddate_act'       => '2018-12-22',
                'car_enddate_tax'       => '2018-3-20',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa25',
                'license_plate'         => '6กค1776',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0587419',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 78878,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-3-20',
                'car_enddate_insurance' => '2018-12-22',
                'car_enddate_act'       => '2018-12-22',
                'car_enddate_tax'       => '2018-3-20',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง


            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa26',
                'license_plate'         => '6กค1769',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0587768',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 59873,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-3-20',
                'car_enddate_insurance' => '2018-12-22',
                'car_enddate_act'       => '2018-12-22',
                'car_enddate_tax'       => '2018-3-20',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa27',
                'license_plate'         => '6กญ6404',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0586397',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 62218,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-5-25',
                'car_enddate_insurance' => '2018-12-22',
                'car_enddate_act'       => '2018-12-22',
                'car_enddate_tax'       => '2018-5-25',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa28',
                'license_plate'         => '6กค1770',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0588565',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 64069,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-3-20',
                'car_enddate_insurance' => '2018-12-27',
                'car_enddate_act'       => '2018-12-27',
                'car_enddate_tax'       => '2018-3-20',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa29',
                'license_plate'         => '6กค1782',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0587944',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 72675,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-3-20',
                'car_enddate_insurance' => '2018-12-27',
                'car_enddate_act'       => '2018-12-27',
                'car_enddate_tax'       => '2018-3-20',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa30',
                'license_plate'         => '6กค1774',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0587868',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 65071,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-3-20',
                'car_enddate_insurance' => '2018-12-27',
                'car_enddate_act'       => '2018-12-27',
                'car_enddate_tax'       => '2018-3-20',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa31',
                'license_plate'         => '6กค1786',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0587233',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 59382,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-3-20',
                'car_enddate_insurance' => '2018-12-28',
                'car_enddate_act'       => '2018-12-28',
                'car_enddate_tax'       => '2018-3-20',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa32',
                'license_plate'         => '6กญ8650',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0588567',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 59382,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-3-20',
                'car_enddate_insurance' => '2018-12-28',
                'car_enddate_act'       => '2018-12-28',
                'car_enddate_tax'       => '2018-5-25',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa33',
                'license_plate'         => '6กค1784',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0588562',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 40,
                'car_mile'              => 53245,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-3-20',
                'car_enddate_insurance' => '2018-12-28',
                'car_enddate_act'       => '2018-12-28',
                'car_enddate_tax'       => '2018-3-20',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa34',
                'license_plate'         => '6กช6072',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MR2B29F3501069228',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 34,
                'car_mile'              => 30273,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-5-11',
                'car_enddate_insurance' => '2018-4-27',
                'car_enddate_act'       => '2018-4-27',
                'car_enddate_tax'       => '2018-5-11',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa35',
                'license_plate'         => '6กช6078',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MR2B29F3501068077',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 34,
                'car_mile'              => 45718,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-5-11',
                'car_enddate_insurance' => '2018-4-27',
                'car_enddate_act'       => '2018-4-27',
                'car_enddate_tax'       => '2018-3-20',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa36',
                'license_plate'         => '6กช6073',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MR2B29F3X01069306',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 34,
                'car_mile'              => 55555,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-5-11',
                'car_enddate_insurance' => '2018-4-27',
                'car_enddate_act'       => '2018-4-27',
                'car_enddate_tax'       => '2018-5-25',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa37',
                'license_plate'         => '6กช6077',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MR2B29F3801069207',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 34,
                'car_mile'              => 35269,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-5-11',
                'car_enddate_insurance' => '2018-4-27',
                'car_enddate_act'       => '2018-4-27',
                'car_enddate_tax'       => '2018-3-20',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa38',
                'license_plate'         => '6กช6076',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MR2B29F3901069264',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 34,
                'car_mile'              => 39751,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-5-11',
                'car_enddate_insurance' => '2018-4-27',
                'car_enddate_act'       => '2018-4-27',
                'car_enddate_tax'       => '2018-3-20',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa39',
                'license_plate'         => '6กช6070',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MR2B29F3301068031',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 34,
                'car_mile'              => 85022,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-5-11',
                'car_enddate_insurance' => '2018-4-27',
                'car_enddate_act'       => '2018-4-27',
                'car_enddate_tax'       => '2018-3-20',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa40',
                'license_plate'         => '6กฌ3755',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MR2B29F3501069293',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 34,
                'car_mile'              => 47442,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-5-17',
                'car_enddate_insurance' => '2018-4-27',
                'car_enddate_act'       => '2018-4-27',
                'car_enddate_tax'       => '2018-3-20',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa41',
                'license_plate'         => '6กช6075',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MR2B29F3901069345',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 34,
                'car_mile'              => 42794,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-5-11',
                'car_enddate_insurance' => '2018-4-27',
                'car_enddate_act'       => '2018-4-27',
                'car_enddate_tax'       => '2018-5-26',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa42',
                'license_plate'         => '6กฎ8475',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MR2K39F3401150217',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'yaris',
                'car_nickname'          => 'yaris_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 30,
                'car_mile'              => 42726,
                'car_door_qty'          => 5,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-6-7',
                'car_enddate_insurance' => '2018-5-29',
                'car_enddate_act'       => '2018-5-29',
                'car_enddate_tax'       => '2018-6-7',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa43',
                'license_plate'         => '6กฎ8474',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MR2K39F3301149477',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'yaris',
                'car_nickname'          => 'yaris_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 30,
                'car_mile'              => 37517,
                'car_door_qty'          => 5,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-6-7',
                'car_enddate_insurance' => '2018-5-29',
                'car_enddate_act'       => '2018-5-29',
                'car_enddate_tax'       => '2018-6-7',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa44',
                'license_plate'         => '6กฎ8468',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MR2K39F3701148266',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'yaris',
                'car_nickname'          => 'yaris_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 30,
                'car_mile'              => 33072,
                'car_door_qty'          => 5,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-4-7',
                'car_enddate_insurance' => '2018-5-29',
                'car_enddate_act'       => '2018-5-29',
                'car_enddate_tax'       => '2018-6-7',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa45',
                'license_plate'         => '6กฎ8471',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MR2K39F3501149528',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'yaris',
                'car_nickname'          => 'yaris_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 30,
                'car_mile'              => 27177,
                'car_door_qty'          => 5,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-6-7',
                'car_enddate_insurance' => '2018-5-29',
                'car_enddate_act'       => '2018-5-29',
                'car_enddate_tax'       => '2018-6-7',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa46',
                'license_plate'         => '6กฎ8472',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MR2K39F3701149644',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'yaris',
                'car_nickname'          => 'yaris_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 30,
                'car_mile'              => 22310,
                'car_door_qty'          => 5,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-6-7',
                'car_enddate_insurance' => '2018-5-29',
                'car_enddate_act'       => '2018-5-29',
                'car_enddate_tax'       => '2018-6-7',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa47',
                'license_plate'         => '6กร6026',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B33F3101159356',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'yaris_ativ',
                'car_nickname'          => 'yaris_ativ_y17_21',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 30,
                'car_mile'              => 37365,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-10-24',
                'car_enddate_insurance' => '2018-8-30',
                'car_enddate_act'       => '2018-8-30',
                'car_enddate_tax'       => '2018-10-24',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa48',
                'license_plate'         => '6กร6020',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B33F3501161322',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'yaris_ativ',
                'car_nickname'          => 'yaris_ativ_y17_21',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 30,
                'car_mile'              => 22636,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-8-24',
                'car_enddate_insurance' => '2018-8-30',
                'car_enddate_act'       => '2018-8-30',
                'car_enddate_tax'       => '2018-10-24',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa49',
                'license_plate'         => '6กร6023',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B33F3301160931',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'yaris_ativ',
                'car_nickname'          => 'yaris_ativ_y17_21',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 30,
                'car_mile'              => 13370,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-8-24',
                'car_enddate_insurance' => '2018-8-30',
                'car_enddate_act'       => '2018-8-30',
                'car_enddate_tax'       => '2018-10-24',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa50',
                'license_plate'         => '6กร6019',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B33F3801161220',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'yaris_ativ',
                'car_nickname'          => 'yaris_ativ_y17_21',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 30,
                'car_mile'              => 32794,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-8-24',
                'car_enddate_insurance' => '2018-8-31',
                'car_enddate_act'       => '2018-8-31',
                'car_enddate_tax'       => '2018-10-24',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa109',
                'license_plate'         => '66กอ6527',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3801103209',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 34,
                'car_mile'              => 10848,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-25',
                'car_enddate_insurance' => '2018-12-18',
                'car_enddate_act'       => '2018-12-18',
                'car_enddate_tax'       => '2018-12-25',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa111',
                'license_plate'         => '6กอ6550',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B9F3701098715',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 34,
                'car_mile'              => 9966,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-25',
                'car_enddate_insurance' => '2018-12-18',
                'car_enddate_act'       => '2018-12-18',
                'car_enddate_tax'       => '2018-12-18',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa112',
                'license_plate'         => '6กอ6537',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3801098268',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 34,
                'car_mile'              => 8401,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-25',
                'car_enddate_insurance' => '2018-12-18',
                'car_enddate_act'       => '2018-12-18',
                'car_enddate_tax'       => '2018-12-25',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'BangWa',
                'branch_code'           => 'uzIEA3or',
                'branch_car_code'       => 'bangwa113',
                'license_plate'         => '6กอ6572',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3501099149',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'coupe',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 33,
                'car_pressure_back'     => 34,
                'car_mile'              => 8934,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-25',
                'car_enddate_insurance' => '2018-12-18',
                'car_enddate_act'       => '2018-12-18',
                'car_enddate_tax'       => '2018-12-25',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง
         //จบสาขาบางหว้า


        //สาขาลาดพร้าว

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP51',
                'license_plate'         => '6กล1675',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600686',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 23396,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-10-27',
                'car_enddate_insurance' => '2018-9-28',
                'car_enddate_act'       => '2018-9-28',
                'car_enddate_tax'       => '2018-10-27',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP52',
                'license_plate'         => '6กษ5746',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600687',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 23786,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-28',
                'car_enddate_act'       => '2018-9-28',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP53',
                'license_plate'         => '6กษ5753',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600691',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 30532,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-28',
                'car_enddate_act'       => '2018-9-28',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP54',
                'license_plate'         => '6กษ5781',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600701',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 30532,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-28',
                'car_enddate_act'       => '2018-9-28',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP54',
                'license_plate'         => '6กษ5781',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600701',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 18827,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-28',
                'car_enddate_act'       => '2018-9-28',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP55',
                'license_plate'         => '6กษ5773',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600703',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 22668,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-28',
                'car_enddate_act'       => '2018-9-28',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP56',
                'license_plate'         => '6กษ5756',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600622',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 22294,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-29',
                'car_enddate_act'       => '2018-9-29',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP57',
                'license_plate'         => '6กษ5764',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600603',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 26084,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-29',
                'car_enddate_act'       => '2018-9-29',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP58',
                'license_plate'         => '6กษ5778',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600632',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 28309,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-29',
                'car_enddate_act'       => '2018-9-29',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP59',
                'license_plate'         => '6กษ5745',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600629',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 22353,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-29',
                'car_enddate_act'       => '2018-9-29',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP60',
                'license_plate'         => '6กษ5732',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600592',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 24783,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-29',
                'car_enddate_act'       => '2018-9-29',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP61',
                'license_plate'         => '6กษ5771',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600597',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 23072,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-29',
                'car_enddate_act'       => '2018-9-29',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP62',
                'license_plate'         => '6กษ5730',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600626',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 25024,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-30',
                'car_enddate_act'       => '2018-9-30',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP63',
                'license_plate'         => '6กษ5774',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600631',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 22960,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-30',
                'car_enddate_act'       => '2018-9-30',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP64',
                'license_plate'         => '6กษ5735',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600634',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 27855,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-30',
                'car_enddate_act'       => '2018-9-30',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP65',
                'license_plate'         => '6กษ5742',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600634',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 24459,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-30',
                'car_enddate_act'       => '2018-9-30',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP66',
                'license_plate'         => '6กษ5766',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600623',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 28510,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-30',
                'car_enddate_act'       => '2018-9-30',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP67',
                'license_plate'         => '6กษ5733',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600611',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 19590,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-9-30',
                'car_enddate_act'       => '2018-9-30',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP68',
                'license_plate'         => '6กศ7731',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600518',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 24778,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-10-16',
                'car_enddate_act'       => '2018-10-16',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP69',
                'license_plate'         => '6กศ7752',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600641',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 21199,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-10-16',
                'car_enddate_act'       => '2018-10-16',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP70',
                'license_plate'         => '6กศ7719',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600667',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 10906,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-10-16',
                'car_enddate_act'       => '2018-10-16',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP71',
                'license_plate'         => '6กศ7714',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600770',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 19443,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-10-16',
                'car_enddate_act'       => '2018-10-16',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP72',
                'license_plate'         => '6กศ7721',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600773',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 21813,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-23',
                'car_enddate_insurance' => '2018-10-16',
                'car_enddate_act'       => '2018-10-16',
                'car_enddate_tax'       => '2018-11-23',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP73',
                'license_plate'         => '6กศ7715',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600832',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 22631,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-17',
                'car_enddate_insurance' => '2018-10-16',
                'car_enddate_act'       => '2018-10-16',
                'car_enddate_tax'       => '2018-11-17',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP74',
                'license_plate'         => '6กศ7746',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600784',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 17885,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-17',
                'car_enddate_insurance' => '2018-10-16',
                'car_enddate_act'       => '2018-10-16',
                'car_enddate_tax'       => '2018-11-17',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP75',
                'license_plate'         => '6กศ7748',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600658',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 23612,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-17',
                'car_enddate_insurance' => '2018-10-16',
                'car_enddate_act'       => '2018-10-16',
                'car_enddate_tax'       => '2018-11-17',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP76',
                'license_plate'         => '6กศ7713',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600528',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 27020,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-17',
                'car_enddate_insurance' => '2018-10-16',
                'car_enddate_act'       => '2018-10-16',
                'car_enddate_tax'       => '2018-11-17',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP77',
                'license_plate'         => '6กศ7734',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาวมุก',
                'body_number'           => 'MNTBAAN17Z0600763',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 21388,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-17',
                'car_enddate_insurance' => '2018-10-16',
                'car_enddate_act'       => '2018-10-16',
                'car_enddate_tax'       => '2018-11-17',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP94',
                'license_plate'         => '6กอ758',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR29F3301103179',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y1317',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1498',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 11170,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-12-19',
                'car_enddate_insurance' => '2018-12-12',
                'car_enddate_act'       => '2018-12-12',
                'car_enddate_tax'       => '2018-12-19',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP95',
                'license_plate'         => '6กอ760',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR29F3301103187',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y1317',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1498',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 11798,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-12-19',
                'car_enddate_insurance' => '2018-12-12',
                'car_enddate_act'       => '2018-12-12',
                'car_enddate_tax'       => '2018-12-19',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP96',
                'license_plate'         => '6กอ762',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR29F3301103251',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y1317',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1498',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 15600,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-12-19',
                'car_enddate_insurance' => '2018-12-12',
                'car_enddate_act'       => '2018-12-12',
                'car_enddate_tax'       => '2018-12-19',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP97',
                'license_plate'         => '6กอ764',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR29F3301103277',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y1317',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1498',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 13552,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-12-19',
                'car_enddate_insurance' => '2018-12-12',
                'car_enddate_act'       => '2018-12-12',
                'car_enddate_tax'       => '2018-12-19',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP98',
                'license_plate'         => '6กอ770',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR29F3301108466',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y1317',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1498',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 14413,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-12-19',
                'car_enddate_insurance' => '2018-12-12',
                'car_enddate_act'       => '2018-12-12',
                'car_enddate_tax'       => '2018-12-19',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP99',
                'license_plate'         => '6กอ771',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR29F3301108532',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y1317',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1498',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 23814,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-12-19',
                'car_enddate_insurance' => '2018-12-12',
                'car_enddate_act'       => '2018-12-12',
                'car_enddate_tax'       => '2018-12-19',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP100',
                'license_plate'         => '6กอ784',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR29F3301109303',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y1317',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1498',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 16285,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-12-19',
                'car_enddate_insurance' => '2018-12-12',
                'car_enddate_act'       => '2018-12-12',
                'car_enddate_tax'       => '2018-12-19',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP101',
                'license_plate'         => '6กอ794',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR29F3301109230',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y1317',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1498',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 17325,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-12-19',
                'car_enddate_insurance' => '2018-12-12',
                'car_enddate_act'       => '2018-12-12',
                'car_enddate_tax'       => '2018-12-19',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP102',
                'license_plate'         => '6กอ804',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR29F3301109248',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y1317',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1498',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 7100,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-12-19',
                'car_enddate_insurance' => '2018-12-12',
                'car_enddate_act'       => '2018-12-12',
                'car_enddate_tax'       => '2018-12-19',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'LP103',
                'license_plate'         => '6กอ805',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR29F3301108549',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y1317',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1498',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 15393,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-12-19',
                'car_enddate_insurance' => '2018-12-12',
                'car_enddate_act'       => '2018-12-12',
                'car_enddate_tax'       => '2018-12-19',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO26',
                'license_plate'         => '4กม3927',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0568531',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 30,
                'car_mile'              => 114119,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-10-27',
                'car_enddate_insurance' => '2018-08-27',
                'car_enddate_act'       => '2018-08-27',
                'car_enddate_tax'       => '2018-10-27',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO35',
                'license_plate'         => '5กญ8221',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0573582',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 30,
                'car_mile'              => 117005,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-17',
                'car_enddate_insurance' => '2018-11-28',
                'car_enddate_act'       => '2018-11-28',
                'car_enddate_tax'       => '2018-11-17',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง


            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO37',
                'license_plate'         => '5กญ8246',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ดำ',
                'body_number'           => 'MNTBAAN17Z0569569',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 30,
                'car_mile'              => 89170,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-17',
                'car_enddate_insurance' => '2018-11-28',
                'car_enddate_act'       => '2018-11-28',
                'car_enddate_tax'       => '2018-11-17',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO40',
                'license_plate'         => '5กญ8223',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นช๊อค',
                'body_number'           => 'MNTBAAN17Z0573304',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 30,
                'car_mile'              => 106412,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-17',
                'car_enddate_insurance' => '2018-12-6',
                'car_enddate_act'       => '2018-12-6',
                'car_enddate_tax'       => '2018-11-17',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO41',
                'license_plate'         => '5กญ8220',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นช๊อค',
                'body_number'           => 'MNTBAAN17Z0573800',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 30,
                'car_mile'              => 93378,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-17',
                'car_enddate_insurance' => '2018-12-6',
                'car_enddate_act'       => '2018-12-6',
                'car_enddate_tax'       => '2018-11-17',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO42',
                'license_plate'         => '5กญ8230',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นช๊อค',
                'body_number'           => 'MNTBAAN17Z0568840',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 30,
                'car_mile'              => 110569,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-11-17',
                'car_enddate_insurance' => '2018-12-6',
                'car_enddate_act'       => '2018-12-6',
                'car_enddate_tax'       => '2018-11-17',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO50',
                'license_plate'         => '5กต3832',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0577555',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 30,
                'car_mile'              => 74367,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-7-1',
                'car_enddate_insurance' => '2018-6-29',
                'car_enddate_act'       => '2018-6-29',
                'car_enddate_tax'       => '2018-7-1',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO55',
                'license_plate'         => '5กท9634กรุงเทพมหานคร',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0579641',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1198',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 30,
                'car_mile'              => 90085,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-8-1',
                'car_enddate_insurance' => '2018-6-29',
                'car_enddate_act'       => '2018-6-29',
                'car_enddate_tax'       => '2018-8-1',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO11',
                'license_plate'         => '4กง6821',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MR2BT9F3901178305',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1499',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 152836,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-4-7',
                'car_enddate_insurance' => '2019-3-30',
                'car_enddate_act'       => '2019-3-30',
                'car_enddate_tax'       => '2018-4-7',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO16',
                'license_plate'         => '4กผ6392',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MR2BT9F3701165925',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1499',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 129982,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-9-28',
                'car_enddate_insurance' => '2018-7-23',
                'car_enddate_act'       => '2018-7-23',
                'car_enddate_tax'       => '2018-9-28',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO21',
                'license_plate'         => '4กผ6402',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ดำ',
                'body_number'           => 'MR2BT9F3101153172',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1499',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 130351,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-9-28',
                'car_enddate_insurance' => '2018-8-20',
                'car_enddate_act'       => '2018-8-20',
                'car_enddate_tax'       => '2018-9-28',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO63',
                'license_plate'         => '5กธ3306',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นช๊อค',
                'body_number'           => 'MR2B29F3701002484',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1499',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 69495,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-8-3',
                'car_enddate_insurance' => '2018-7-6',
                'car_enddate_act'       => '2018-7-6',
                'car_enddate_tax'       => '2018-8-3',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO79',
                'license_plate'         => '6กฆ3640',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MR2B29F3401045079',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1499',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 69387,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-3-28',
                'car_enddate_insurance' => '2018-12-28',
                'car_enddate_act'       => '2018-12-28',
                'car_enddate_tax'       => '2019-3-28',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO80',
                'license_plate'         => '6กฆ1337',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3301043341',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1499',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 62475,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-3-28',
                'car_enddate_insurance' => '2018-12-28',
                'car_enddate_act'       => '2018-12-28',
                'car_enddate_tax'       => '2019-3-28',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO80',
                'license_plate'         => '6กฆ1337',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3301043341',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1499',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 62475,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-3-28',
                'car_enddate_insurance' => '2018-12-28',
                'car_enddate_act'       => '2018-12-28',
                'car_enddate_tax'       => '2019-3-28',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO81',
                'license_plate'         => '6กฆ1335',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F301042430',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1499',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 62475,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-3-28',
                'car_enddate_insurance' => '2018-12-28',
                'car_enddate_act'       => '2018-12-28',
                'car_enddate_tax'       => '2019-3-28',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO82',
                'license_plate'         => '6กฆ1343',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3301044859',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1499',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 40721,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-3-28',
                'car_enddate_insurance' => '2018-12-28',
                'car_enddate_act'       => '2018-12-28',
                'car_enddate_tax'       => '2019-3-28',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO83',
                'license_plate'         => '6กฆ3643',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ดำ',
                'body_number'           => 'MR2B29F3X01041098',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1499',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 52243,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-3-28',
                'car_enddate_insurance' => '2018-12-28',
                'car_enddate_act'       => '2018-12-28',
                'car_enddate_tax'       => '2019-3-28',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO85',
                'license_plate'         => '6กจ3593',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3X01031347',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1499',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 44696,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-4-19',
                'car_enddate_insurance' => '2019-2-11',
                'car_enddate_act'       => '2019-2-11',
                'car_enddate_tax'       => '2018-4-19',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Ladprao',
                'branch_code'           => 'SAHbAkUY',
                'branch_car_code'       => 'ECO86',
                'license_plate'         => '6กจ3591',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3901030724',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1499',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 32,
                'car_pressure_back'     => 30,
                'car_mile'              => 46337,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2018-4-19',
                'car_enddate_insurance' => '2019-2-11',
                'car_enddate_act'       => '2019-2-11',
                'car_enddate_tax'       => '2018-4-19',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง
         //จบสาขาลาดพร้าว



        //เริ่มสาขานนทบุรี
            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT61',
                'license_plate'         => '5กท9637',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0580490',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 78396,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-8-1',
                'car_enddate_insurance' => '2018-6-29',
                'car_enddate_act'       => '2018-6-29',
                'car_enddate_tax'       => '2018-4-19',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT01',
                'license_plate'         => '5กท6860',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0577428',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 87198,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-7-28',
                'car_enddate_insurance' => '2019-3-24',
                'car_enddate_act'       => '2019-3-24',
                'car_enddate_tax'       => '2018-6-26',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT02',
                'license_plate'         => '6กท6867',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0577453',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 108529,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-7-28',
                'car_enddate_insurance' => '2019-3-24',
                'car_enddate_act'       => '2019-3-24',
                'car_enddate_tax'       => '2018-6-26',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT03',
                'license_plate'         => '5กท6874',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0577455',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 95126,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-7-28',
                'car_enddate_insurance' => '2019-3-24',
                'car_enddate_act'       => '2019-3-24',
                'car_enddate_tax'       => '2018-6-26',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT04',
                'license_plate'         => '5กท6846',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'บอร์นเงิน',
                'body_number'           => 'MNTBAAN17Z0581921',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 62080,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2016-11-8',
                'car_enddate_insurance' => '2018-8-11',
                'car_enddate_act'       => '2018-8-11',
                'car_enddate_tax'       => '2018-6-28',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT78',
                'license_plate'         => '6กศ7725',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MNTBAAN17Z0600807',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 11542,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-11-17',
                'car_enddate_insurance' => '2018-10-16',
                'car_enddate_act'       => '2018-10-16',
                'car_enddate_tax'       => '2018-11-17',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT79',
                'license_plate'         => '6กศ7738',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MNTBAAN17Z0600635',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 22770,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-11-17',
                'car_enddate_insurance' => '2018-10-16',
                'car_enddate_act'       => '2018-10-16',
                'car_enddate_tax'       => '2018-11-17',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT80',
                'license_plate'         => '6กศ7730',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MNTBAAN17Z0600527',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 17364,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-11-17',
                'car_enddate_insurance' => '2018-10-16',
                'car_enddate_act'       => '2018-10-16',
                'car_enddate_tax'       => '2018-11-17',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT81',
                'license_plate'         => '7กฐ6347',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MNTBAAN17Z0600482',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 19116,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-11-17',
                'car_enddate_insurance' => '2018-10-16',
                'car_enddate_act'       => '2018-10-16',
                'car_enddate_tax'       => '2018-11-17',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT82',
                'license_plate'         => '6กศ7754',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MNTBAAN17Z0600664',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 20829,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-11-17',
                'car_enddate_insurance' => '2018-10-16',
                'car_enddate_act'       => '2018-10-16',
                'car_enddate_tax'       => '2018-11-17',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT83',
                'license_plate'         => '6กษ5740',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MNTBAAN17Z0600831',
                'engine_number'         => str_random(10),
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 14227,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-11-17',
                'car_enddate_insurance' => '2018-10-16',
                'car_enddate_act'       => '2018-10-16',
                'car_enddate_tax'       => '2018-11-17',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT84',
                'license_plate'         => '6กฬ2340',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3901099123',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 11392,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-8',
                'car_enddate_insurance' => '2018-11-30',
                'car_enddate_act'       => '2018-11-30',
                'car_enddate_tax'       => '2018-8-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT85',
                'license_plate'         => '6กฬ2390',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3501098910',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 9660,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-8',
                'car_enddate_insurance' => '2018-11-30',
                'car_enddate_act'       => '2018-11-30',
                'car_enddate_tax'       => '2018-8-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT86',
                'license_plate'         => '6กฬ2339',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3501099099',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 11577,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-8',
                'car_enddate_insurance' => '2018-11-30',
                'car_enddate_act'       => '2018-11-30',
                'car_enddate_tax'       => '2018-8-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi', //ไฟล์ company.php
                'branch_code'           => 'Z07e1ySa',   //ไฟล์ branch.php
                'branch_car_code'       => 'NONT87',
                'license_plate'         => '6กฬ2384',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3801098691',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 16457,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-8',
                'car_enddate_insurance' => '2018-11-30',
                'car_enddate_act'       => '2018-11-30',
                'car_enddate_tax'       => '2018-8-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT88',
                'license_plate'         => '6กฬ2377',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3701098441',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 6707,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-8',
                'car_enddate_insurance' => '2018-11-30',
                'car_enddate_act'       => '2018-11-30',
                'car_enddate_tax'       => '2018-8-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT89',
                'license_plate'         => '6กฬ2375',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3X01098420',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 11869,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-8',
                'car_enddate_insurance' => '2018-11-30',
                'car_enddate_act'       => '2018-11-30',
                'car_enddate_tax'       => '2018-8-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT90',
                'license_plate'         => '6กฬ2379',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3901098618',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 15100,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-8',
                'car_enddate_insurance' => '2018-11-30',
                'car_enddate_act'       => '2018-11-30',
                'car_enddate_tax'       => '2018-8-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT91',
                'license_plate'         => '6กฬ2344',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3X01099163',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 13871,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-8',
                'car_enddate_insurance' => '2018-11-30',
                'car_enddate_act'       => '2018-11-30',
                'car_enddate_tax'       => '2018-8-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT92',
                'license_plate'         => '6กฬ2381',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3X01098627',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 10103,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-8',
                'car_enddate_insurance' => '2018-11-30',
                'car_enddate_act'       => '2018-11-30',
                'car_enddate_tax'       => '2018-8-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT93',
                'license_plate'         => '6กฬ2385',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3401098865',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 13288,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-8',
                'car_enddate_insurance' => '2018-11-30',
                'car_enddate_act'       => '2018-11-30',
                'car_enddate_tax'       => '2018-8-8',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT104',
                'license_plate'         => '6กอ6540',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3507098311',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 12934,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-25',
                'car_enddate_insurance' => '2018-12-18',
                'car_enddate_act'       => '2018-12-18',
                'car_enddate_tax'       => '2018-12-25',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT105',
                'license_plate'         => '6กอ6568',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3501098776',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 10446,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-25',
                'car_enddate_insurance' => '2018-12-18',
                'car_enddate_act'       => '2018-12-18',
                'car_enddate_tax'       => '2018-12-25',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT106',
                'license_plate'         => '6กอ6570',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3801098934',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 14067,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-25',
                'car_enddate_insurance' => '2018-12-18',
                'car_enddate_act'       => '2018-12-18',
                'car_enddate_tax'       => '2018-12-25',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT107',
                'license_plate'         => '6กอ6548',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3501098602',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 6466,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-25',
                'car_enddate_insurance' => '2018-12-18',
                'car_enddate_act'       => '2018-12-18',
                'car_enddate_tax'       => '2018-12-25',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง

            //สร้างรถ
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Nonthaburi',
                'branch_code'           => 'Z07e1ySa',
                'branch_car_code'       => 'NONT108',
                'license_plate'         => '6กอ6531',
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => 'ขาว',
                'body_number'           => 'MR2B29F3301103232',
                'engine_number'         => str_random(10),
                'car_brand'             => 'toyota',
                'car_generation'        => 'vios',
                'car_nickname'          => 'vios_y13_17',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1500',
                'car_type'              => 'sedan',
                'car_human_qty'         => 5,
                'car_baggage_qty'       => 4,
                'car_type_fule'         => '["gasohol_95","gasohol_91"]',
                'car_type_gear'         => 'automatic_gear',
                'car_pressure_front'    => 30,
                'car_pressure_back'     => 32,
                'car_mile'              => 10714,
                'car_door_qty'          => 4,
                'car_image'             => '',
                'car_description'       => 'ไม่มีรายละเอียดเพิ่มเติม',
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => '2017-12-25',
                'car_enddate_insurance' => '2018-12-18',
                'car_enddate_act'       => '2018-12-18',
                'car_enddate_tax'       => '2018-12-25',
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => 2500
            ]);
            //จบการสร้าง
        //จบสาขานนทบุรี

        //$this->dummy_data();

        //// End Create Data ////

        $this->enableForeignKeys();
    }

    function dummy_data(){
        for ($i=1; $i <= 500; $i++) { 
            // Add the Car
            Cars::create([
                'company_name'          => '0',
                'branch_name'           => 'Test Cars',
                'branch_code'           => $this->gen_branch_code(),
                'license_plate'         => mt_rand(1,9).$this->gen_word_thai().$this->gen_word_thai().mt_rand(1000,9999),
                'license_province'      => 'กรุงเทพมหานคร',
                'color'                 => $this->gen_color(),
                'body_number'           => str_random(10),
                'engine_number'         => str_random(10),
                'branch_car_code'       => 'NONT0'.$i,
                'car_brand'             => 'nissan',
                'car_generation'        => 'almera',
                'car_nickname'          => 'almera_y11_16',
                'car_generation_detail' => '1_2_e',  
                'car_engine_cc'         => '1200',
                'car_type'              => 'coupe',
                'car_human_qty'         => mt_rand(2,8),
                'car_baggage_qty'       => mt_rand(3,7),
                'car_type_fule'         => '["gasohol_95","gasohol_91","e20","e85","gasoline_95"]',
                'car_type_gear'         => $this->gen_gear(),
                'car_pressure_front'    => mt_rand(29,39),
                'car_pressure_back'     => mt_rand(29,39),
                'car_mile'              => mt_rand(1000,999999),
                'car_door_qty'          => mt_rand(1,5),
                'car_image'             => '',
                'car_description' => $this->get_customers_description(),
                'car_company_insurance' => $this->gen_insurance_type(),
                'car_type_insurance'    => $this->gen_insurance(),
                'car_date_register'     => $this->gen_date(),
                'car_enddate_insurance' => $this->gen_date(),
                'car_enddate_act'       => $this->gen_date(),
                'car_enddate_tax'       => $this->gen_date(),
                'status'                => $this->gen_status(),
                'car_option'            => '[
                                                "power_door",
                                                "tilt_steering_wheel",
                                                "fm_radio_and_stereo_cd",
                                                "sensor"
                                            ]',
                'price'                 => '2500'
            ]);

        }
    }
    
    public function gen_branch_code(){
        $data = array('uoSDDYle','izLH906r','uzIEA3or','SAHbAkUY','Z07e1ySa','hfNms3YD','E4Rd5kYV','tzRyDFfT','zrYbZkkK','TifEfmFR','ML05GTQO');
        $random_data = array_rand($data,1);
        return $data[$random_data];
    }

    public function change_date($date){
        $date_array = explode("/",$date); // split the array
        $var_day = $date_array[0]; //day seqment
        $var_month = $date_array[1]; //month segment
        $var_year = $date_array[2]-543; //year segment
        $new_date_format = $var_year.'-'.$var_month.'-'.$var_day; // join them together
        return $new_date_format;
    }

    public function gen_gear(){
        $data = array('automatic_gear','manual_gear');
        $random_data = array_rand($data,1);
        return $data[$random_data];
    }

     public function gen_date(){
    	//Generate a random year using mt_rand.
		$year= mt_rand(2018, date("Y"));
		 
		//Generate a random month.
		$month= mt_rand(1, 12);
		 
		//Generate a random day.
		$day= mt_rand(1, 28);
		 
		//Using the Y-M-D format.
		$randomDate = $year . "-" . $month . "-" . $day;

		return $randomDate;
    }


    public function gen_status(){
        $status=array('enabled','closed','booking');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }

    public function gen_color(){
    	$color=array("red","green","blue","yellow","brown");
    	$random_color=array_rand($color,1);
    	return $color[$random_color];
    }


    public function gen_insurance(){
    	$insurance = array("type1","type2","type3","type3+");
    	$random_insurance = array_rand($insurance,1);
    	return $insurance[$random_insurance];
    }

    public function gen_insurance_type(){
    	$insurance_type = array('bangkok_insurance','viri_insurance','synmankong_insurance','thanachart_insurance','direct_asia_insurance','axa_insurance');
    	$random_insurance_type = array_rand($insurance_type,1);

    	return $insurance_type[$random_insurance_type];
    }

    public function gen_word_thai(){
        $thaichar = array('ก','ข','ค','ง','จ','ฉ','ช','ซ','ฌ','ญ','ฐ','ฑ','ฒ','ณ','ด','ต','ถ','ท','ธ','น','บ','ป','ผ','ฝ','พ','ฟ','ภ','ม','ย','ร','ล','ว','ศ','ษ','ส','ห','ฬ','อ','ฮ');
        $random_thaichar = array_rand($thaichar,1);
        return $thaichar[$random_thaichar];
    }

    public function gen_branch(){
        $data = array('OnNut','BangWa','LatPhrao','Nonthaburi','Samrong','UbonAirport','ChiangMaiAirport','Pattaya','DonMueangAirport','SuvarnabhumiAirport');
        $random_data = array_rand($data,1);
        return $data[$random_data];
    }

    public function get_customers_description(){
        $arrayName = array(
            '<p>ติงต๊องบรารีเสิร์ช เวิร์กช็อปแซนด์วิช เวสต์เยอร์บีร่าจัมโบ้ นิรันดร์งั้น โอเลี้ยงเวิร์ลด์ บอดี้พาเหรดคีตกวีเซี้ยว บอยคอตต์จูเนียร์จูนบอกซ์มอคคา รีสอร์ตอีแต๋นฮิ ไมค์ รามเทพออดิทอเรียมเพรส โปรโมทวอล์คถูกต้อง สป็อตแจ๊สเพทนาการโอเวอร์เจ๊ ออโต้สเปกโหงวเฮ้งโมเดิร์นเซอร์ มอคคาภารตะ คอนเซ็ปต์เซนเซอร์ แดนเซอร์พุทธภูมิวอลนัตบ๋อยบอดี้</p>',
            '<p>คันถธุระ บัสวาซาบิคอลเล็กชั่นเสกสรรค์ สแควร์ฟีเวอร์สป็อต ปูอัดฮิไหร่ดัมพ์ บลอนด์บรรพชน ไฮกุแชมปิยองวอเตอร์อุปัทวเหตุแพนงเชิญ วิภัชภาควิกสเก็ตช์ บาลานซ์วิภัชภาคเอ็กซ์โปถ่ายทำยูโร น็อกเบอร์รี เอ๋ออีสต์เย้วสป็อตจตุคาม วาริชศาสตร์นายพรานสหัชญาณแซ็กโซโฟน ออร์แกนเซลส์แมน พรีเมียร์โอเพ่น เบอร์เกอร์ อุปนายกเทอร์โบกระดี๊กระด๊าแอนด์แคชเชียร์ แชมปิยอง</p>',
            '<p>ซาดิสม์สแตนเลสอัลบั้มบราวีไอพี บลอนด์โดนัทจ๊าบ แฟรีแซ็กโอ้ยรีไทร์ไทม์ คีตราชันยูวีดิสเครดิตมาร์ก จังโก้แรลลี่โมเดิร์นเอเซียสปอร์ต โบรชัวร์มาร์เก็ต คอนแท็ค สกรัมฟอร์มสไตล์ออร์เดอร์ ศากยบุตรอินเตอร์พาสต้าออดิชั่นสแตนดาร์ด โครนาจุ๊ยสไตรค์รองรับ บรรพชน โดมิโนโอเวอร์โฟนบูติครีสอร์ต จูนสต็อคโดมิโนใช้งาน เซ็นทรัล โปรเจ็กต์คาเฟ่โหงวเฮ้ง ซานตาคลอสวโรกาสเซลส์โบ้ยเกมส์</p>',
            '<p>ซะ วัคค์ชัวร์พีเรียดชัตเตอร์ ปาสเตอร์ออทิสติกเคลื่อนย้ายอพาร์ตเมนท์แรลลี ง่าวแมนชั่นสเตย์หงวนซะ แรงดูดพาร์ไฮไลท์เบิร์ด ชิฟฟอนพุดดิ้งแบนเนอร์สตริงรีไทร์ ใช้งานเหมย โหลยโท่ย อีโรติกปอดแหกบริกรทอร์นาโด เซี้ยวแอโรบิคบอร์ดพ่อค้า เอนทรานซ์แอโรบิค ดอกเตอร์คอนโดเนิร์สเซอรี่จิ๊กเมจิค เมจิกรายชื่อเทอร์โบ แหม็บ พฤหัสน็อค หงวนออยล์</p>',
            '<p>แมชชีนเพนตากอนสุริยยาตร์แครกเกอร์อุปการคุณ ล้มเหลวสมาพันธ์ครูเสด ฟลุทซีดานบูติคทัวร์นาเมนท์วิป อพาร์ทเมนต์บาร์บีคิวสปายว้อยแกงค์ นินจาไหร่สเตอริโอ ไฮเทคดิสเครดิตออร์แกน จ๊อกกี้ริคเตอร์ฮ็อตแอโรบิค ยูวีบลอนด์ดอกเตอร์โบกี้หล่อฮังก้วย โบ้ยอันเดอร์ ผ้าห่มวิทย์อะเคลียร์อะ โง่เขลาโชว์รูมเบบี้ รีวิว เต๊ะ วอร์รูมคอร์รัปชั่นน้องใหม่ฮีโร่เนอะ ปักขคณนาไวอากร้าแกสโซฮอล์สติ๊กเกอร์ แคนูรากหญ้าพอเพียงโอเพ่นซัพพลาย</p>',
            '<p>ฮ็อตโบว์ปัจฉิมนิเทศ รีพอร์ทแฮปปี้ซิงแชมเปญ แบคโฮ สแตนดาร์ดดีพาร์ตเมนต์วานิลลาฮิแจ๊ส เอเซียคาราโอเกะสต๊อกเอสเพรสโซแจ๊ส มาเฟียฮ็อตด็อกรากหญ้าเวิร์ลด์ คอรัปชั่นเซอร์ไพรส์ภควัมบดีรีเสิร์ช โรแมนติคฉลุยจิ๊กเลสเบี้ยนว้อดก้า โปรเจ็คท์เทรลเล่อร์ สังโฆเพทนาการเดบิตอพาร์ทเมนต์ มาร์กเทคโนจิ๊กโก๋วีซ่าชีส ติ่มซำสโรชากิมจิทีวี ภควัมบดีซูเอี๋ยเซอร์วิสทิป ซีอีโอ วาริชศาสตร์โอเคแคนยอน ปาสคาลรีทัช</p>',
            '<p>เรซินฟลุกสเตริโอ อัลมอนด์ป่าไม้รัม แมชชีน อาร์พีจีโลโก้เพลย์บอยวินเมี่ยงคำ นิวส์เคลียร์โปรดักชั่นสะเด่าพาสปอร์ต บอมบ์ แฟนซีดีพาร์ตเมนท์คอนเซ็ปต์ จิ๊กโก๋วานิลา คอนแทคเอสเปรสโซฮ็อตด็อกอีสต์โฟม ทริปวอลล์ คอมพ์ จตุคามควีนบุ๋น สุนทรีย์ซิมโฟนี่อมาตยาธิปไตยดีพาร์ทเมนต์หลวงปู่ ป่าไม้ท็อปบู๊ท ผ้าห่ม ลอจิสติกส์เต๊ะธุหร่ำพุทธศตวรรษ</p>',
            '<p>สปาซาฟารีสันทนาการเอฟเฟ็กต์เนิร์สเซอรี ภคันทลาพาธโคโยตี้เรซินแชมเปี้ยนแคมปัส แฟรีโอเปอเรเตอร์เลคเชอร์ มอคคาเนิร์สเซอรี่ สเตริโอ ติว เมคอัพ อึ้มแฟ็กซ์แบล็กสุนทรีย์ ปัจฉิมนิเทศ เอ็นจีโอแรงใจแม็กกาซีนแบดแบด รันเวย์ จิ๊กโก๋อุรังคธาตุคาร์โก้ ว่ะแกรนด์แพลนขั้นตอนสป็อต ศึกษาศาสตร์ คอรัปชันแจ็กเก็ต ไฮกุหลวงพี่</p>',
            '<p>วีเจไลท์เก๋ากี้แจ็กพอตแอปพริคอท โอเปอเรเตอร์สต็อค เซลส์ ซิตี้สุนทรีย์โมเต็ล ศิรินทร์ซาบะหมิงราสเบอร์รีคีตกวี เพรสสกายมหภาคปูอัด คาแรคเตอร์ศิลปวัฒนธรรมภคันทลาพาธ แรลลี่ เดโมฮากกาสต๊อค โหลนเบบี้พาสต้าชัตเตอร์ วีเจ ธุหร่ำอัลตราคอนแทค พาวเวอร์เรตติ้งมลภาวะ เซ่นไหว้ม้านั่ง ช็อปปิ้งเจ็ตมอบตัวอิกัวนารีทัช มินท์ปิกอัพชนะเลิศคาแร็คเตอร์แพทยสภา</p>',
            '<p>การที่เที่ยวเอธิโอเปียอิสตันบูลมินดาเนา เวสาลี เม็กซิโก</p>'
        );
        $random_arrayName = array_rand($arrayName,1);
        return $arrayName[$random_arrayName];
    }

}
