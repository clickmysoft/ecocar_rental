<?php

use Illuminate\Database\Seeder;
use App\Models\Article\ArticleModel;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

    	ArticleModel::create([
    			'title'						=> 'Mazda เพิ่มออพชั่นล้นคันใน Mazda2 ใหม่ เอาใจนักขับวัยรุ่น',
				'image_thumbnail'			=> 'thumbnail-a1.jpg',
				'image_full'				=> 'full_img-a1.jpg',
				'description'				=> $this->description_1(),
				'mata_title'				=> 'Mazda เพิ่มออพชั่นล้นคันใน Mazda2 ใหม่ เอาใจนักขับวัยรุ่น', 
				'mata_description'			=> 'Mazda เพิ่มออพชั่นล้นคันใน Mazda2 ใหม่ เอาใจนักขับวัยรุ่น',
				'mata_keyword'				=> 'Mazda,Mazda2',
	            'status'					=> $this->gen_status(),
	   ]);


        ArticleModel::create([
                'title'                     => '5 จุดให้เราเช็ครถ พร้อมลุยหน้าฝน',
                'image_thumbnail'           => 'thumbnail-a2.jpg',
                'image_full'                => 'full_img-a2.jpg',
                'description'               => $this->description_2(),
                'mata_title'                => 'เข็ครถ', 
                'mata_description'          => 'เช็ครถหน้าฝน',
                'mata_keyword'              => 'เช็ครถหน้าฝน ',
                'status'                    => $this->gen_status(),
       ]);


        ArticleModel::create([
                'title'                     => '5 รถใหม่ ที่จะเข้าไทยในปี 61',
                'image_thumbnail'           => 'thumbnail-a3.jpg',
                'image_full'                => 'full_img-a3.jpg',
                'description'               => $this->description_3(),
                'mata_title'                => '5 รถใหม่', 
                'mata_description'          => '5 รถใหม่ ที่จะเข้าไทยในปี 61',
                'mata_keyword'              => '5 รถใหม่ ปี61 ',
                'status'                    => $this->gen_status(),
       ]);


        ArticleModel::create([
                'title'                     => 'รวมรถ SUV สุดเจ๋งในไทย ประจำปี 2017',
                'image_thumbnail'           => 'thumbnail-a4.jpg',
                'image_full'                => 'full_img-a4.jpg',
                'description'               => $this->description_4(),
                'mata_title'                => 'รวมรถ SUV', 
                'mata_description'          => 'รวมรถ SUV สุดเจ๋งในไทย ประจำปี 2017',
                'mata_keyword'              => 'รวมรถ SUV สุดเจ๋งในไทย ปี 2017 ',
                'status'                    => $this->gen_status(),
       ]);


        ArticleModel::create([
                'title'                     => 'รวมยานยนต์ Ecocar ของเซเล็บชื่อดังเมืองผู้ดี',
                'image_thumbnail'           => 'thumbnail-a5.jpg',
                'image_full'                => 'full_img-a5.jpg',
                'description'               => $this->description_5(),
                'mata_title'                => 'รวมยานยนต์ Ecocar ของเซเล็บ', 
                'mata_description'          => 'รวมยานยนต์ Ecocar ของเซเล็บชื่อดังเมืองผู้ดี',
                'mata_keyword'              => 'รวมยานยนต์ Ecocar ของเซเล็บ',
                'status'                    => $this->gen_status(),
       ]);



        ArticleModel::create([
                'title'                     => 'Suzuki เปิดตัว All New Suzuki SWIFT สปอร์ตคอมแพคคาร์ มาตรฐานระดับโลก เริ่ม 4.99 แสนบาท',
                'image_thumbnail'           => 'thumbnail-a6.jpg',
                'image_full'                => 'full_img-a6.jpg',
                'description'               => $this->description_6(),
                'mata_title'                => 'Suzuki เปิดตัว All New Suzuki SWIFT', 
                'mata_description'          => 'Suzuki เปิดตัว All New Suzuki SWIFT สปอร์ตคอมแพคคาร์ มาตรฐานระดับโลก เริ่ม 4.99 แสนบาท',
                'mata_keyword'              => 'Suzuki เปิดตัว All New Suzuki SWIFT',
                'status'                    => $this->gen_status(),
       ]);


        ArticleModel::create([
                'title'                     => 'Nissan Qashqai รุ่นเพิ่มระบบขับขี่อัตโนมัติบุก สหราชอาณาจักร ราคาเริ่มต้น 1.3ล้านบาท',
                'image_thumbnail'           => 'thumbnail-a7.jpg',
                'image_full'                => 'full_img-a7.jpg',
                'description'               => $this->description_7(),
                'mata_title'                => 'Nissan Qashqai รุ่นเพิ่มระบบขับขี่อัตโนมัติบุก สหราชอาณาจักร', 
                'mata_description'          => 'Nissan Qashqai รุ่นเพิ่มระบบขับขี่อัตโนมัติบุก สหราชอาณาจักร ราคาเริ่มต้น 1.3ล้านบาท',
                'mata_keyword'              => 'Nissan Qashqai รุ่นเพิ่มระบบขับขี่อัตโนมัติบุก สหราชอาณาจักร',
                'status'                    => $this->gen_status(),
       ]);


        ArticleModel::create([
                'title'                     => 'Nissan Kicks 2018 ใหม่ ประกาศราคาเริ่มต้นที่ 5.73 แสนบาท',
                'image_thumbnail'           => 'thumbnail-a8.jpg',
                'image_full'                => 'full_img-a8.jpg',
                'description'               => $this->description_8(),
                'mata_title'                => 'Nissan Kicks 2018 ใหม่ ', 
                'mata_description'          => 'Nissan Kicks 2018 ใหม่ ประกาศราคาเริ่มต้นที่ 5.73 แสนบาท',
                'mata_keyword'              => 'Nissan Kicks 2018 ใหม่',
                'status'                    => $this->gen_status(),
       ]);


        ArticleModel::create([
                'title'                     => 'Test Drive Toyota C-HR 2018 สัมผัสใหม่พลัง Hybrid สมรรถนะเต็มเปี่ยม ยอดเยี่ยมเรื่องประหยัด',
                'image_thumbnail'           => 'thumbnail-a9.jpg',
                'image_full'                => 'full_img-a9.jpg',
                'description'               => $this->description_9(),
                'mata_title'                => 'Test Drive Toyota C-HR 2018', 
                'mata_description'          => 'Test Drive Toyota C-HR 2018 สัมผัสใหม่พลัง Hybrid สมรรถนะเต็มเปี่ยม ยอดเยี่ยมเรื่องประหยัด',
                'mata_keyword'              => 'Test Drive Toyota C-HR 2018',
                'status'                    => $this->gen_status(),
       ]);


        ArticleModel::create([
                'title'                     => 'Toyota Aqua Rirvie 2018 ใหม่ รุ่นตกแต่งพิเศษ ราคา 6.39 แสนบาท',
                'image_thumbnail'           => 'thumbnail-a10.jpg',
                'image_full'                => 'full_img-a10.jpg',
                'description'               => $this->description_10(),
                'mata_title'                => 'Toyota Aqua Rirvie 2018 ใหม', 
                'mata_description'          => 'Toyota Aqua Rirvie 2018 ใหม่ รุ่นตกแต่งพิเศษ ราคา 6.39 แสนบาท',
                'mata_keyword'              => 'Toyota Aqua Rirvie 2018 ใหม',
                'status'                    => $this->gen_status(),
       ]);


    }

   


    public function gen_status(){
        $status=array('enabled','closed');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }



    public function description_1(){
        $status=array('<p>หลังจากก้าวขึ้นครองบัลลังก์ยึดแชมป์ยอดขายสูงสุดอันดับหนึ่งของตลาดรถเล็ก&nbsp;<strong>มาสด้า&nbsp;</strong>พร้อมเดินเครื่องต่อยอดทันที ประเดิมด้วยการเปิดตัวแนะนำมาสด้า2 รุ่น 2018 คอลเลคชั่นลุยตลาด ภายใต้คอนเซ็ปต์ใหม่ล่าสุด&nbsp;<strong>&ldquo;EXCITEMENT NEVER ENDS&rdquo;&nbsp;</strong>หรือ &ldquo;เร้าใจ ไม่เคยหยุด&rdquo; ใส่ออพชั่นเพิ่มจนล้นคันหวังมัดใจสาวก ซูม-ซูม พร้อมเทคโนโลยีสุดล้ำอย่าง SKYACTIV-VEHICLE DYNAMICS ที่มาพร้อมกับระบบ G-VECTORING CONTROL เอกลักษณ์เฉพาะตัวจาก&nbsp;<strong>มาสด้า</strong>&nbsp;เพิ่มออพชั่นเต็มคันแต่ไม่เพิ่มราคา</p>

            <p style="text-align: center;"><img alt="" src="https://auto.mthai.com/app/uploads/2018/02/Mazda2-4-600x378.jpg" style="height:378px; width:600px" /></p>

            <p style="text-align: center;"><img alt="" src="https://auto.mthai.com/app/uploads/2018/02/Mazda2-8-600x485.jpg" style="height:485px; width:600px" /></p>

            <p><strong>นายชาญชัย ตระการอุดมสุข ประธานบริหาร บริษัท มาสด้า เซลส์ (ประเทศไทย) จำกัด&nbsp;</strong>กล่าวว่า การเปิดตัว&nbsp;<strong>มาสด้า2</strong>&nbsp;รุ่นปี 2018 ในครั้งนี้ จะเป็นการสร้างความเชื่อมั่นให้แก่ผู้บริโภคมากยิ่งขึ้นไปหลังจากโมเดลล่าสุดที่&nbsp;<strong>มาสด้า&nbsp;</strong>แสดงให้เห็นว่ามีการนำเทคโนโลยีใหม่ล่าสุดมาใส่ในรถยนต์นั่งขนาดเล็ก และเรายังมีการต่อยอดพัฒนาอย่างไม่หยุดยั้ง เพื่อให้ผู้บริโภคได้สัมผัสประสบการณ์การขับขี่ที่สนุกสนาน เร้าใจตามแบบฉบับของ&nbsp;<strong>มาสด้า</strong>&nbsp;ทั้งนี้<strong>มาสด้า2</strong>&nbsp;ถือเป็นรถยนต์ที่ได้รับความนิยมในประเทศไทยมาโดยตลอด เพราะรถยนต์รุ่นนี้มียอดขายในประเทศไทยมากกว่า 200,000 คัน นับตั้งแต่เปิดตัวครั้งแรก และรุ่นเจนเนอเรชั่นปัจจุบันที่ใช้เทคโนโลยีสกายแอคทีฟ เปิดตัวเมื่อปี 2558 มียอดขายสูงถึงกว่า 80,000 คัน การเปิดตัว&nbsp;<strong>มาสด้า2</strong>&nbsp;รุ่นปี 2018 ในวันนี้ คืออีกหนึ่งในเป้าหมายของความสำเร็จทางด้านยอดขาย ซึ่งในปีนี้<strong>&nbsp;มาสด้า&nbsp;</strong>วางเป้ายอดขายไว้สูงถึง 60,000 คัน หรือเติบโตเพิ่มขึ้นถึง 15%</p>

            <p><strong>นายธีร์ เพิ่มพงศ์พันธ์ ผู้อำนวยการอาวุโสฝ่ายการตลาด&nbsp;</strong>กล่าวว่า สำหรับการสื่อสารของรถ&nbsp;<strong>มาสด้า2</strong>รุ่นปี 2018 ในปีนี้ เราจะสื่อสารผ่านทั้งช่องทางออฟไลน์และออนไลน์ โดยมุ่งเน้นช่องทางออฟไลน์เพื่อสร้างการรับรู้ในช่วงเปิดตัว และใช้ช่องทางออนไลน์ในการเข้าถึงไลฟ์สไตล์ของกลุ่มเป้าหมาย โดยเน้นการสื่อสารและเลือกประเภทกิจกรรมหรือช่องทางที่กลุ่มเป้าหมายซึ่งเป็นคนรุ่นใหม่ที่ให้ความสำคัญกับเทคโนโลยีและการขับขี่ที่สนุกสนาน ไม่ว่าจะเป็นเว็บเพจที่เกี่ยวกับไลฟ์สไตล์ต่างๆ หรือนำเสนอกิจกรรมที่สนุกเร้าใจในแบบที่แตกต่าง พร้อมสื่อสารเอกลักษณ์ของมาสด้า2 ใหม่ 2018 คอลเลคชั่น&nbsp; ที่ตอบสนองความต้องการได้ครอบคลุมหลากหลายด้าน ทั้งด้านฟังก์ชั่น และด้านอารมณ์ ที่ให้ทั้งความสวยงาม ความสปอร์ตพรีเมี่ยมด้วยสีแดงใหม่ โซลเรด คริสตัล&nbsp; ระบบความปลอดภัยสุดล้ำ พร้อมความแรง ความประหยัด ความบันเทิง และเทคโนโลยีสุดล้ำต่างๆ อย่างลงตัว</p>

            <p>และเพื่อเป็นการขอบคุณลูกค้ามาสด้าทุกท่านที่ให้ความไว้วางใจทั้งเรื่องคุณภาพของรถยนต์ รวมทั้งการบริการตั้งแต่ก้าวเข้ามาในโชว์รูมไปจนถึงการบริการหลังการขาย<strong>&nbsp;มาสด้า&nbsp;</strong>ได้เพิ่มออพชั่นสำหรับ&nbsp;<strong>มาสด้า2</strong>&nbsp;รุ่น 2018 คอลเลคชั่น ทั้งเครื่องยนต์สกายแอคทีฟเบนซิน 1.3L และเครื่องยนต์สกายแอคทีฟคลีนดีเซล 1.5L ทั้งตัวถังแบบซีดานและแฮตช์แบค หวังมัดใจกลุ่มลูกค้าวัยรุ่นและคนรุ่นใหม่ในราคาที่จับต้องได้ง่าย ด้วยการเพิ่มอุปกรณ์มาตรฐานที่หลากหลาย ประกอบด้วย</p>

            <ul>
                <li><strong>มาสด้า2</strong>&nbsp;รุ่นปี 2018 พร้อมให้ทุกคนได้สัมผัสและทดลองขับได้แล้วตั้งแต่วันนี้ที่โชว์รูมมาสด้าทั่วประเทศ</li>
                <li>สีใหม่ล่าสุด สีแดงโซลเรด คริสตัล ที่ให้ความสดใสเป็นประกายของสีแดง</li>
                <li>ระบบเชื่อมต่อโลกการสื่อสาร MZD Connect</li>
                <li>ระบบไฟหน้า เปิด-ปิด แบบอัตโนมัติ</li>
                <li>ที่ปัดน้ำฝนแบบกระจกหน้าแบบอัตโนมัติ</li>
                <li>ไฟหน้าแบบ LED โปรเจคเตอร์ พร้อม Daytime Running Light</li>
                <li>ระบบควบคุมความเร็วคงที่ (Cruise Control)</li>
                <li>ระบบเตือนเมื่อมีรถในจุดอับสายตาขณะเปลี่ยนเลน (Advance Blind Spot Monitoring, ABSM)</li>
                <li>ระบบเตือนเมื่อมีรถในจุดอับสายตาขณะถอยหลัง (Rear Cross Traffic Alert, RCTA)</li>
                <li>ระบบกุญแจรีโมทอัจฉริยะ (Smart Keyless Entry)</li>
                <li>หน้าจอ Active Driving Display แสดงข้อมูลการขับขี่แบบสี
                <p><strong>มาสด้า2</strong>&nbsp;รุ่นปี 2018 นั้นได้รับการสร้างสรรค์ขึ้นมาเพื่อตอบโจทย์กลุ่มวัยรุ่น วัยทำงานและคนรุ่นใหม่ที่มีไลฟ์สไตล์ที่ไม่ซ้ำแบบใคร ทั้งยังมีพลังและเอกลักษณ์เฉพาะตัวที่ท้าทายแบบใหม่ๆ ให้ทั้งความแรงและประหยัด สามารถครอบคลุมความต้องการใช้งานของกลุ่มลูกค้าได้หลากหลาย</p>

                <p>รถยนต์&nbsp;<strong>มาสด้า2</strong>&nbsp;รุ่นปรับโฉมใหม่ มีรูปแบบตัวถังให้เลือกทั้งแบบซีดาน 4 ประตู และแบบแฮตช์แบค 5 ประตู โดยในแต่ละรูปแบบตัวถังจะมี 7 รุ่นย่อย แบ่งเป็นเครื่องยนต์เบนซิน 4 รุ่นและเครื่องยนต์คลีนดีเซล 3 รุ่น โดยสีภายนอกมี มีให้เลือกทั้งหมด 7 สี ซึ่งสีใหม่ล่าสุดคือ สีแดง โซลเรด คริสตัล, สีขาว สโนว์เฟค ไวท์ เพิร์ล, สีน้ำตาล ไททาเนียมแฟลช, สีเงิน อลูมินัม เมทัลลิค, สีน้ำเงิน อีเทอนอล บลู, สีเทา เมทิเออ เกรย์ และสีดำ เจ็ท แบล็ก</p>

                <p><strong>มาสด้า2</strong>&nbsp;รุ่นปี 2018 พร้อมให้ทุกคนได้สัมผัสและทดลองขับได้แล้วตั้งแต่วันนี้ที่โชว์รูม&nbsp;<strong>มาสด้า&nbsp;</strong>ทั่วประเทศ</p>

                <p>โปรดติดตามความเคลื่อนไหวและกิจกรรมของมาสด้าผ่านทางทางโซเชียลมีเดีย</p>

                <p>เว็บไซต์&nbsp;<a href="http://www.mazda.co.xn--th%20-7dr3dug/">www.mazda.co.th และ</a>&nbsp;MazdaThailandOfficial Facebook/YouTube/Instagram/LINE</p>
                </li>
            </ul>

                    ');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }

    public function description_2(){
        $status=array('
           

            <p>เป็นส่วนที่สำคัญมากๆ ในการขับรถในช่วงเปียกๆ ชิ้นๆ ในหน้าฝนเช่นนี้ ซึ่ง<strong>เราก็สามารถเช็คได้ด้วยตัวเองในขั้นเบสิก ด้วยการเอาเล็บจิกเนื้อยาง หากจิกเข้าก็ถือว่ายังดี จากนั้นดูหน้ายางว่าสึกหรือไม่ บวกกับร่องยาง จะต้องลึกไม่กว่า 3-4 มิลลิเมตร เป็นอย่างน้อย ปิดท้ายควรเติมลมยางให้มากกว่าเดิมถึง 2-3 ปอนด์ต่อตารางนิ้ว (PSI) เพื่อให้ยางรีดน้ำได้ดีขึ้น</strong>&nbsp;หากไม่สมบูรณ์ ควรลดความเร็วในการขับขี่ไว้เลย เพื่อยืดเวลาการหมดสภาพของยางรถยนต์ให้นานขึ้น จนกว่าจะมีเงินไปเปลี่ยนเส้นใหม่</p>

            <p><strong>รายละเอียดเรื่องนี้ Click เข้าไปที่&nbsp;<a href="https://www.thairentecocar.com/14495922/%E0%B8%A2%E0%B8%B2%E0%B8%87%E0%B8%A3%E0%B8%96%E0%B8%A2%E0%B8%99%E0%B8%95%E0%B9%8C" target="_blank">&quot;ยางรถยนต์&quot;</a>&nbsp;ของ ECOCAR ได้เลย</strong></p>

            <h3><br />
            5 จุดเช็ครถ ลุยหน้าฝน : สัญญาณไฟต้องใช้ได้หมดทุกดวง</h3>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/%E0%B8%A3%E0%B8%B0%E0%B8%9A%E0%B8%9A%E0%B8%AA%E0%B8%B1%E0%B8%8D%E0%B8%8D%E0%B8%B2%E0%B8%93%E0%B9%84%E0%B8%9F_%E0%B9%80%E0%B8%8A%E0%B9%87%E0%B8%84%E0%B8%A3%E0%B8%96%E0%B8%AB%E0%B8%99%E0%B9%89%E0%B8%B2%E0%B8%9D%E0%B8%99.jpg" style="height:409px; width:716px" /></p>

            <p>ในช่วงหน้าฝนเช่นนี้ ทัศนวิสัยจะแย่มากๆ&nbsp;<strong>ฉะนั้นเราควรเช็คไฟทุกดวง อาทิ ไฟเลี้ยว, ไฟหน้า, ไฟสูง และ ไฟเบรก หากยิ่งขับตอนกลางคืน แถมมีฝนตกลงมาอีก การขับขี่ยิ่งอันตรายมากๆ หากดวงไฟพวกนี้ เกิดขัดข้องขึ้นมา</strong></p>

            <p><strong>รายละเอียดเรื่องนี้ Click เข้าไปที่&nbsp;<a href="https://www.thairentecocar.com/16742853/%E0%B8%A7%E0%B8%B4%E0%B8%98%E0%B8%B4%E0%B8%82%E0%B8%B1%E0%B8%9A%E0%B8%A3%E0%B8%96%E0%B8%81%E0%B8%A5%E0%B8%B2%E0%B8%87%E0%B8%84%E0%B8%B7%E0%B8%99-%E0%B8%AD%E0%B8%A2%E0%B9%88%E0%B8%B2%E0%B8%87%E0%B9%84%E0%B8%A3-%E0%B9%83%E0%B8%AB%E0%B9%89%E0%B8%9B%E0%B8%A5%E0%B8%AD%E0%B8%94%E0%B8%A0%E0%B8%B1%E0%B8%A2" target="_blank">&quot;วิธีขับรถกลางคืน&quot;</a>&nbsp;ของ ECOCAR ได้เลย</strong></p>

            <h4><br />
            5 จุดเช็ครถ ลุยหน้าฝน : ที่ปัดน้ำฝน</h4>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B8%9B%E0%B8%B1%E0%B8%94%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%9D%E0%B8%99_%E0%B9%80%E0%B8%8A%E0%B9%87%E0%B8%84%E0%B8%A3%E0%B8%96%E0%B8%AB%E0%B8%99%E0%B9%89%E0%B8%B2%E0%B8%9D%E0%B8%99.jpg" style="height:404px; width:718px" /></p>

            <p><strong>หน้าฝนแล้ว นับว่าเป็นจุดที่ให้ความสำคัญอย่างมาก ในการขับรถช่วงพระพิรุณโปรยปรายเช่นนี้ ใครที่มีอยู่ ก็ควรทำความสะอาดให้มีความหนืดกวาดน้ำฝนได้หมดจด หากมีเสียงเอี๊ยดอ๊าดๆ ก็ควรมาสำรองเปลี่ยนไว้เลยนะ</strong>&nbsp;ซึ่งต้องเราต้องรู้ว่า รถของเรามีขนาดที่ปัดน้ำฝน ยาวเท่าไหร่ด้วย ส่วนวิธีเปลี่ยนใบปัดน้ำฝน ให้ทำตามนี้เลยนะ&nbsp;<strong><a href="https://car.kapook.com/view60114.html" target="_blank">CLICK</a></strong>&nbsp;เลย</p>

            <p><strong>(เครดิต kapook.com)</strong></p>

            <h5><br />
            5 จุดเช็ครถ ลุยหน้าฝน : ที่ฉีดน้ำล้างกระจก</h5>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B8%89%E0%B8%B5%E0%B8%81%E0%B8%A5%E0%B9%89%E0%B8%B2%E0%B8%87%E0%B8%81%E0%B8%A3%E0%B8%B0%E0%B8%88%E0%B8%81_%E0%B9%80%E0%B8%8A%E0%B9%87%E0%B8%84%E0%B8%A3%E0%B8%96%E0%B8%AB%E0%B8%99%E0%B9%89%E0%B8%B2%E0%B8%9D%E0%B8%99.jpg" style="height:406px; width:722px" /></p>

            <p><strong>ให้สังเกตตรงรูพ่นน้ำล้างกระจกว่าคราบเศษผงฝุ่นไปอุดไว้หรือไม่ หากมีก็ให้หาเครื่องเป่าลม ก็สามารถเอามาเป่าออกได้ หากไม่มีก็ให้ที่ล้างรถเป่าออกให้ก็ได้เช่นกัน</strong></p>

            <h6><br />
            5 จุดเช็ครถ ลุยหน้าฝน : น้ำมันเบรคต้องพร้อม</h6>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%A1%E0%B8%B1%E0%B8%99%E0%B9%80%E0%B8%9A%E0%B8%A3%E0%B8%84_%E0%B9%80%E0%B8%8A%E0%B9%87%E0%B8%84%E0%B8%A3%E0%B8%96%E0%B8%AB%E0%B8%99%E0%B9%89%E0%B8%B2%E0%B8%9D%E0%B8%99.jpg" style="height:408px; width:725px" /></p>

            <p><strong>หากมีน้ำมันเบรคของคุณ เป็นสีดำเข้ม ให้หาช่างเปลี่ยนถ่ายน้ำมันเบรค และลูกยางเบรค รวมไปถึงผ้าเบรค หากเกิดเสียงเอี๊ยดๆ ก็ให้เปลี่ยนด้วยเลย เพราะมันเป็นสัญญาณว่า ผ้าเบรคกำลังจะหมดแล้ว นั่นเอง</strong></p>

            <p><strong>รายละเอียดเรื่องนี้ Click เข้าไปที่&nbsp;<a href="http://ecocar.in.th/%E0%B8%A3%E0%B8%B2%E0%B8%A2%E0%B8%A5%E0%B8%B0%E0%B9%80%E0%B8%AD%E0%B8%B5%E0%B8%A2%E0%B8%94/59367dee313bdf0015236af8/%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%A1%E0%B8%B1%E0%B8%99%E0%B9%80%E0%B8%9A%E0%B8%A3%E0%B8%84_Und_%E0%B9%81%E0%B8%A5%E0%B8%B0%E0%B9%80%E0%B8%9B%E0%B8%A5%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%99%E0%B8%9C%E0%B9%89%E0%B8%B2%E0%B9%80%E0%B8%9A%E0%B8%A3%E0%B8%84_Und_%E0%B9%80%E0%B8%A1%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B9%84%E0%B8%AB%E0%B8%A3%E0%B9%88:!5934cfdf8eb95c00016d580e/59f06680b22f720001495342/59367b7d313bdf0015236ab5?q=TP1" target="_blank">&quot;น้ำมันเบรค-ผ้าเบรค&quot;</a>&nbsp;ของ ECOCAR ได้เลย</strong></p>
            ');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }

    public function description_3(){ 
        $status=array('
            <p>หลายคนกำลังที่จะหารถใหม่ มาใช้งานในปี 2561 ที่กำลังจะถึงนี้ และจะเข้ามาในประเทศไทย อย่างเป็นทางการ&nbsp;<strong>มาดูว่า รถใหม่ ที่ ECOCAR จะมาแนะนำนั้น มารถรุ่นไหนบ้าง !?&nbsp;&nbsp;</strong></p>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/MG-ZS-Blue.jpg" style="height:324px; width:617px" /></p>

            <p>รถครอบครัวระบบอัจฉริยะ i-SMART ไซส์กำลังดี สัญชาติอังกฤษ ที่มีความใกล้เคียงกับ MG GS แต่โครงสร้างของรถ จะเล็กลง ซึ่งจุดเด่นของรถรุ่นนี้คือ ระบบการสั่งการด้วยเสียง หรือ Voice Control ที่รองรับภาษาไทยไว้ให้เรียบร้อย หากถามเรื่องราคาแล้วไซร้ บอกได้เลยว่า พอลุ้นได้อยู่ เพราะราคาเริ่มต้นของรถอัจฉริยะจากเมืองผู้ดี รุ่นนี้ จะเริ่มเต้นที่ 6.79 แสนบาท เท่านั้น</p>

            <p>&nbsp;</p>

            <h3>5 รถใหม่เข้าไทย ปี 61 : Nissan Almera 2018</h3>
                <br>
            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/Nissan_Almera_2018.jpg" style="height:319px; width:581px" /></p>

            <p>ความใหม่ที่หลายคนตั้งตารอดูว่าจะเป็นเช่นนั้นหรือไม่ ที่มีข่าวว่า จะออกจำหน่ายแบบ Global Model หรือมาตรฐานเดียวกันในการเปิดตัวทั่วโลก ทั้ง รถพวงมาลัยซ้าย กับ รถพวงมาลัยขวา สำหรับรถซีดานราคาประหยัดรุ่นนี้ แต่อย่างไรแล้ว รถเวอร์ชัน 2018 จะเปิดตัวที่ประเทศอินเดีย อย่างแน่นอน ซึ่งจะเข้ามาไทย ในช่วงไหนของปีดังกล่าว ต้องติดตามกันต่อไป หวังว่าจะเข้ามา ณ แดนสยาม ในช่วงเวลาไล่เลี่ยกับแดนโรตี</p>

            <p>&nbsp;</p>

            <h3>5 รถใหม่เข้าไทย ปี 61 : Nissan March 2018</h3>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/Nissan_March_Micra.jpg" style="height:377px; width:566px" /></p>

            <p>ยังเป็นปริศนาอยู่ว่า รถรุ่นนี้จะยังคงพัฒนาและเปิดตัวในรูปโฉมใหม่ หรือ จะปิดตำนานลงในปีนี้ไปเลย เนื่องจากทางผู้ผลิตยานยนต์ สัญชาติญี่ปุ่น กำลังเตรียมเปิดตัวรถรุ่นใหม่ที่จะมาเสียบแทนที่ ซึ่งมีสมรรถนะใกล้เคียงกับ March นั่นก็คือ Nissan Micra ที่ได้เปิดตัวในภาคพื้นยุโรป ไปเรียบร้อยแล้วในปีนี้ เมื่อเดือนมีนาคม ที่ผ่านมา</p>

            <p>&nbsp;</p>

            <h3>5 รถใหม่เข้าไทย ปี 61 : Nissan Note e-Power 2018</h3>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/Nissan_Note_2018.jpg" style="height:343px; width:572px" /></p>

            <p>รถที่ได้รับแรงบันดาลใจจาก Nissan Leaf ที่ขับเคลื่อนด้วยระบบไฟฟ้า ซึ่งได้นำนวัตกรรมดังกล่าวมาประยุกต์ใช้กับ Nissan Note เวอร์ชัน 2018 ด้วยการใช้เครื่องยนต์เบนซิน 3 สูบ เพื่อสร้างกระแสไฟฟ้าเก็บไว้ในแบตเตอรี ซึ่งจะเข้ามาเปิดตลาดในแดนสยาม ช่วงไตรมาสแรกของปี 61 นี้ เตรียมตั้งตารอได้เลย</p>

            <p>&nbsp;</p>

            <h3>5 รถใหม่เข้าไทย ปี 61 : Toyota Avanza Veloz 2018</h3>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/Avanza_Veloz.jpg" style="height:371px; width:561px" /></p>

            <p>รถครอบครัวที่ได้รับความนิยมทั้ง มาเลเซีย, อินโดนีเซีย และไทย โดยในรุ่นปี 2018 จะออกแบบเพื่อเป็นมิตรกับสิ่งแวดล้อมให้มากที่สุด ซึ่งมีการเปิดตัวไปแล้วที่ประเทศอินโดนีเซีย และประเทศฟิลิปปินส์ ตามลำดับ ส่วนวันที่จะเข้ามาเปิดตัว ณ เมืองสยามนั้น อาจจะเป็นช่วงปีหน้า แต่ยังไม่ทราบว่าจะเป็นช่วงใดใน 4 ไตรมาสของปีดังกล่าว</p>
            ');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }

    public function description_4(){ 
        $status=array('
            <p>หากคุณต้องการรถคันหนึ่ง ต้องอยากได้รถที่มีสมรรถนะสูง และมีระบบที่ช่วยให้คุณเดินทางได้สะดวก วันนี้ เราจะมารวมรถ SUV หรือ รถ Sport Utility Vehicle ที่หาได้ในเมืองไทย ว่ามียี่ห้อรถของจ้าวไหนกันบ้าง</p>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/Chevrolet-Captiva.jpg" style="height:358px; width:545px" /></p>

            <p>รถ SUV 7 ที่นั่ง ที่ขับเคลื่อนด้วยเครื่องยนต์ดีเซล สามารถเดินทางไปได้ ทุกสถานการณ์ ไม่ว่าจะเป็นการทำงาน ท่องเที่ยว หรือ ช็อปปิ้ง ทั้งในเมือง หรือ ตามชนบท รถสัญชาติอเมริกันรายนี้ ก็สามารถพาคุณไปสู่ที่หมายได้ พร้อมกับรูปแบบดีไซน์ที่หรู และสปอร์ต อย่างมีระดับ ตามที่คุณต้องการได้</p>

            <h3><br />
            รวมรถ SUV สุดเจ๋งในปี 2017 : Nissan X-TRAIL</h3>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/Nissan_X-Trail.jpg" style="height:347px; width:544px" /></p>

            <p>รถ SUV 7 ที่นั่ง ที่ขับเคลื่อนด้วยระบบ Hybrid สั่งตรงจากประเทศญี่ปุ่น เหมาะสำหรับคนที่ชอบประยัดน้ำมัน รถรุ่นนี้ อาจตอบโจทย์ได้แน่นอน นอกจากนี้ยังทำงานร่วมกับ Application สุดล้ำของ Nissan ด้วยแอพ Nissan Connect ที่โหลดสมาร์ทโฟนได้ ซึ่งสามารถเชื่อมไปยังสังคมโซเชียล และ Google Search ช่วยให้การเดินทางของคุณ เป็นไปได้ด้วยดี และโรยด้วยกลีบกุหลาบ<br />
            &nbsp;</p>

            <h3>รวมรถ SUV สุดเจ๋งในปี 2017 : Range Rover EVO</h3>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/range_rover1.jpg" style="height:362px; width:543px" /></p>

            <p>รถ SUV สุดหรู 7 ที่นั่ง ที่ได้รับความนิยมอย่างมากในประเทศสหรัฐอเมริกา ด้วยรูปแบบดีไซน์ที่ล้ำสมัย พร้อมสิ่งอำนวยความสะดวกแบบจัดเต็ม ด้วยระบบที่มีชื่อว่า &quot;Touch Pro Duo&quot; ที่สามารถทำอะไรอย่างอื่นได้ ในระหว่างขับขี่ ส่วนเบาะที่นั่งอื่นๆ ก็มีระบบความบันเทิงภายในรถมากมาย ให้คุณเลือกสรร ในระหว่างการเดินทางบนท้องถนน</p>

            <h3><br />
            รวมรถ SUV สุดเจ๋งในปี 2017 : Ford Everest</h3>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/Ford_Everest.jpg" style="height:364px; width:545px" /></p>

            <p>รถ SUV 7 ที่นั่ง ที่เหมาะสำหรับพวกขาลุยป่าดงพงไพร หรือพื้นที่ริมเขา ตามที่คุณต้องการได้ ดั่งใจ ซึ่งมาพร้อมกับเทคโนโลยีอัจฉริยะ อาทิ การสั่งการด้วยเสียง &quot;SYNC&trade;3&quot; พร้อมเมนูภาษาไทย อีกทั้งสามารถปรับเบาะที่นั่งให้เหมาะสม และเพิ่มความเป็นส่วนตัวได้มากขึ้น ในการผจญภัยตามพื้นที่ต่างๆ</p>

            <h3><br />
            รวมรถ SUV สุดเจ๋งในปี 2017 : Isuzu MU-X</h3>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/MU-X.jpg" style="height:355px; width:581px" /></p>

            <p>รถ SUV 7 ที่นั่ง ยอดฮิตในเมืองไทย ที่มียนตรกรรมที่มีรูปแบบดีไซน์ ที่ผสมผสานระหว่าง สปอร์ต แอดเวนเจอร์ กับ ความโอ่อ่า หรูหรา อย่างเข้าขาลงตัว นอกจากนี้ เป็นรถที่เป็นมิตรกับสิ่งแวดล้อม ไม่ปล่อยก๊าซพิษไปเป็นมลภาวะ&nbsp;<strong>ซึ่งรถรุ่นนี้ ทาง ECOCAR มีให้คุณลองสัมผัสความคุ้มค่าให้แล้ว ทุกสาขาในเขตกรุงเทพมหานคร และสาขาตามต่างจังหวัด อาทิ เชียงใหม่, อุบลราชธานี และ พัทยา</strong></p>
            ');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }

    public function description_5(){ 
        $status=array('
            <p>พูดถึงรถ Ecocar จัดได้เป็นทางเลือกที่ดีที่สุดในการขับรถ เพื่อรักษาสิ่งแวดล้อมที่มีน้อยลงทุกวันๆ จากมลภาวะทางอากาศ จนทำให้ผู้คนหันมาใช้มากขึ้น และนี่คือตัวอย่างของเหล่าบุคคลที่มีชื่อเสียง ว่าเขาใช้ยานยนต์ Ecocar ยี่ห้ออะไร และรุ่นไหน ที่เว็บไซต์ The Sun สื่อแท็บลอยด์ชื่อดังของประเทศอังกฤษ ได้รวบรวมเอาไว้</p>

            <h2>ยานยนต์ Ecocar ของเหล่าเซเล็บ : จอร์จ คลูนีย์ (Commuter Tango Car)</h2>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/Tango.jpg" style="height:412px; width:568px" /></p>

            <p><strong>รถพลังงานไฟฟ้า ไซส์ขนาดจิ๋วที่ดาราฮอลลีวู้ด ที่โด่งดังจากเรื่อง Batman &amp; Robin ได้ใช้งานเมื่อปี 2009</strong>&nbsp;ซึ่งตัวรถมีความกว้างเพียง 39 นิ้ว ภายในตัวรถมีคนนั่งเพียงคนเดียว แถมเป็นรถที่หมดปัญหาเรื่องการหาที่จอดรถไปเลย แถมมีอัตราการวิ่งได้ถึง 60 ไมล์ต่อชั่วโมง ภายใน 4 วินาที</p>

            <p>&nbsp;</p>

            <h2>ยานยนต์ Ecocar ของเหล่าเซเล็บ : ลอร์เรน เคลลี (Nissan LEAF)</h2>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/LEAF.jpg" style="height:377px; width:566px" /></p>

            <p><strong>นักข่าวสาวใหญ่ ชาวสก็อตแลนด์ ของสถานีโทรทัศน์ BBC สื่อชื่อดังของอังกฤษ</strong>&nbsp;ได้ใช้รถแบรนด์ดัง สัญชาติญี่ปุ่น อย่าง Nissan LEAF ที่มีความโดดเด่นคือ เป็นรถที่ใช้พลังงานไฟฟ้า ที่ให้พละกำลังขับเคลื่อนถึง&nbsp;<strong>110 กิโลวัตต์</strong>&nbsp;เลยทีเดียว</p>

            <p>&nbsp;</p>

            <h2>ยานยนต์ Ecocar ของเหล่าเซเล็บ : แกรี่ เนวิลล์ (BMW i8)</h2>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/BMW_i8.jpg" style="height:407px; width:565px" /></p>

            <p><strong>อดีตนักฟุตบอลของสโมสร แมนเชสเตอร์ ยูไนเต็ด ที่ทำหน้าที่เป็นนักวิเคราะห์เกมลูกหนังของ Sky Sports</strong>&nbsp;ที่ใช้รถสปอร์ตหรู ที่ใครๆ มองว่าเป็นรถที่ใช้พลังงานสิ้นเปลือง แต่รถหรูที่อดีตนักเตะทีมชาติอังกฤษ และ&nbsp;<strong>&quot;ปีศาจแดง&quot;</strong>&nbsp;ใช้นั้น เป็นรถที่เป็นมิตรกับสิ่งแวดล้อมสุดๆ ด้วยเทคโนโลยี&nbsp;<strong>Plug-In Hybrid</strong>&nbsp;ที่เชื่อมต่อกับมอเตอร์ไฟฟ้า ไปถึงเครื่องยนต์ ทำให้สมรรถนะของรถรุ่นนี้ มีความแรงไม่ต่างจากรถสปอร์ตทั่วไป<br />
            &nbsp;</p>

            <h2>ยานยนต์ Ecocar ของเหล่าเซเล็บ : แคโรล วอร์เดอร์แมน (Vauxhall Ampera)</h2>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/Vauxhall.jpg" style="height:354px; width:552px" /></p>

            <p><strong>อดีตพิธีกรรายการเกมโชว์ ทางช่อง Channel 4 ช่องฟรีทีวีชื่อดังของประเทศอังกฤษ</strong>&nbsp;ได้ใช้รถของ Vauxhall รุ่น Ampera รถที่ได้เปิดตัวตั้งแต่ในปี 2012 ซึ่งความพิเศษของรถรุ่นนี้คือ เป็นรถยนต์พลังงานไฟฟ้า สัญชาติอังกฤษรุ่นแรกที่ใช้ระบบ&nbsp;<strong>Plug-In Hybrid</strong>แถมเป็นรถยุโรปที่ดีที่สุดแห่งปีนั้นอีกด้วย</p>

            <p>&nbsp;</p>

            <h2>ยานยนต์ Ecocar ของเหล่าเซเล็บ : โรแวน แอตคินสัน (BMW i3)</h2>

            <p style="text-align:center"><img alt="" src="https://www.thairentecocar.com/images/editor/rowan-atkinson-bmw-i3.jpg" style="height:372px; width:578px" /></p>

            <p><strong>ดาราตลก หน้าตาย แอบทะเล้น จากซิทคอมชื่อดังระดับโลก เรื่อง Mr.Bean</strong>&nbsp;ที่หันมาใช้รถยนต์พลังงานไฟฟ้าสุดเท่ สัญชาติเยอรมัน อย่าง BMW i3 ที่เป็นรถรุ่นแรกที่เป็นยานยนต์แบบ&nbsp;<strong>ZEV</strong>&nbsp;หรือ&nbsp;<strong>Zero-emissions vehicle</strong>&nbsp;ที่มีความหมายว่า รถยนต์ที่ปล่อยมลพิษเท่ากับศูนย์ หรือเรียกง่ายๆ ก็คือ รถยนต์ที่ใช้พลังงานสีเขียว และเป็นมิตรกับสิ่งแวดล้อม นั่นเอง</p>
            ');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }

    public function description_6(){ 
        $status=array('<p>บริษัท&nbsp;<strong>ซูซูกิ</strong>&nbsp;มอเตอร์ (ประเทศไทย) จำกัด เผยโฉม All New&nbsp;<strong>Suzuki&nbsp;SWIFT</strong>&nbsp;สไตล์เด่นบนเส้นทางที่แตกต่าง WE STANDOUT ด้วยสปอร์ตคอมแพคคาร์มาตรฐานระดับโลก ชูจุดเด่นเทคโนโลยีเครื่องยนต์แบบ DUALJET และแพลตฟอร์มใหม่ HEARTECT พร้อมดีไซน์สปอร์ตคงเอกลักษณ์ DNA ของ&nbsp;<strong>SWIFT</strong>&nbsp;กับเทคโนโลยีอันทันสมัยช่วยในการขับขี่ และอุปกรณ์อำนวยความสะดวกครบครัน เจาะกลุ่มคนหนุ่มสาววัยทำงาน ตั้งเป้ายอดขายปีนี้ 15,700 คัน</p>
            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/02/2-3-600x492.jpg" style="height:492px; width:600px" /></p>
            <p>นายโยจิ มุโรซากะ กรรมการผู้จัดการใหญ่ บริษัท&nbsp;<strong>ซูซูกิ</strong>&nbsp;มอเตอร์ (ประเทศไทย) จำกัด กล่าวว่า &ldquo;<strong>SWIFT</strong>&nbsp;เป็นรถรุ่นสำคัญของ&nbsp;<strong>ซูซูกิ&nbsp;</strong>ในระดับโลก แสดงให้เห็นถึงความเชี่ยวชาญของ&nbsp;<strong>ซูซูกิ&nbsp;</strong>ด้านรถยนต์คอมแพคและความทุ่มเทในการสร้างสรรค์รถยนต์ที่มีทั้งดีไซน์สวยทันสมัยและความสนุกในการขับขี่&nbsp;<strong>Suzuki&nbsp;SWIFT</strong>&nbsp;สไตล์เด่นบนเส้นทางที่แตกต่าง WE STANDOUT ที่ผ่านมา สำหรับรุ่นล่าสุดนี้ซึ่งเป็น เจเนอเรชั่นที่ 3 ของ&nbsp;<strong>SWIFT</strong>&nbsp;ยังเป็นรถยนต์หนึ่งเดียวที่ได้รับรางวัลอันทรงเกียรติ RJC Car of the Year 2018 จากการคัดเลือกโดยสถาบันนักวิจัยและผู้สื่อข่าวยานยนต์แห่งญี่ปุ่น หลังจากที่ 2 เจเนอเรชั่นก่อนได้รับรางวัลนี้มาแล้วในปี 2005 และ 2010 ตามลำดับ&nbsp;<strong>ซูซูกิ&nbsp;</strong>จึงมีความภาคภูมิใจอย่างยิ่งที่จะแนะนำ ALL NEW&nbsp;<strong>Suzuki&nbsp;SWIFT</strong>&nbsp;สู่ประเทศไทย เราเชื่อว่า&nbsp;<strong>SWIFT</strong>&nbsp;เจเนอเรชัน 3 นี้จะสร้างปรากฏการณ์ครั้งใหม่ในตลาดรถยนต์ของไทยได้อย่างแน่นอน&rdquo;</p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/02/3-2-600x392.jpg" style="height:392px; width:600px" /></p>

            <p>นายมาซาโอะ โกโบริ หัวหน้าวิศวกร&nbsp;<strong>ซูซูกิ</strong>&nbsp;มอเตอร์ คอร์ปอเรชัน ประเทศญี่ปุ่น เปิดเผยว่า &ldquo;รถรุ่นนี้พัฒนาขึ้นภายใต้คอนเซ็ปต์ &ldquo;INNOVATION &ndash; Fun &amp; Sporty&rdquo; โดยออกแบบใหม่ทั้งหมดให้ All New&nbsp;<strong>Suzuki&nbsp;SWIFT</strong>&nbsp;มีความโดดเด่นทั้งรูปลักษณ์ภายนอกและสมรรถนะในการขับขี่ที่ดีเยี่ยม ในด้านการออกแบบภายนอกยังคงความโดดเด่นด้วยดีไซน์ที่เป็นเอกลักษณ์แต่มีกลิ่นอายของรถยุโรปมากยิ่งขึ้น ด้วยมิติของตัวรถซึ่งความสูงอยู่ที่ 1,495 มิลลิเมตร และกว้างขึ้น 40 มิลลิเมตร ทำให้ All New&nbsp;<strong>Suzuki&nbsp;SWIFT</strong>&nbsp;มีความสปอร์ตและดูปราดเปรียวมากขึ้น โดยเรายังเน้นให้ All New&nbsp;<strong>SuzukiSWIFT</strong>&nbsp;มีภาพลักษณ์ที่ดูสปอร์ตดุดัน ด้วยเส้นสีแดงตัดกระจังหน้าสีดำ ไฟหน้า LED Projector และไฟหลัง LED ล้ออะลูมิเนียมอัลลอยขนาด 16 นิ้ว ในส่วนของสมรรถนะได้เพิ่มเทคโนโลยีใหม่คือหัวฉีดคู่หรือ DUALJET ที่จะเพิ่มประสิทธิภาพในการเผาไหม้ของเครื่องยนต์ให้สมบูรณ์ยิ่งขึ้น จึงประหยัดน้ำมันกว่าเดิมมากกว่า 23 กม. ต่อลิตร ขับขี่เร้าใจด้วยเครื่องยนต์ใหม่ K12M 1.2 ลิตร</p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/02/4-3-600x356.jpg" style="height:356px; width:600px" /></p>

            <p>นอกจากนั้น ในด้านความปลอดภัยมีการนำแพลตฟอร์มใหม่ HEARTECT มาใช้เพื่อช่วยให้รถมีน้ำหนักน้อยลงแต่คงความแข็งแกร่งและช่วยประหยัดน้ำมัน รวมถึงโครงสร้างตัวถังแบบ TECT พร้อมระบบกันการสั่นสะเทือน ระบบ TCS ช่วยในการควบคุมรถขณะขับขี่บนถนนลื่นหรือในทางโค้ง และยังเหมาะกับการขับในเมืองด้วยระบบ IDLING STOP ที่ช่วยลดการสิ้นเปลืองน้ำมันขณะรถหยุดนิ่ง ขับขี่อย่างมั่นใจในทุกเส้นทางด้วยระบบ Hill Hold Control ที่จะช่วยออกตัวขณะอยู่บนทางลาดชัน และปลอดภัยมากขึ้นด้วยถุงลมนิรภัย SRS ถึง 6 ตำแหน่ง</p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/02/5-2-600x348.jpg" style="height:348px; width:600px" /></p>

            <p>ทั้งนี้ All New&nbsp;<strong>Suzuki&nbsp;SWIFT</strong>&nbsp;ยังมาพร้อมอุปกรณ์อำนวยความสะดวกมากมาย แผงคอนโซลกลางด้านหน้าเบนเข้าหาคนขับเพื่อการใช้งานที่สะดวกสบายยิ่งขึ้น มาตรวัดสไตล์สปอร์ตที่ตกแต่งด้วยลายเส้นสีแดง พร้อมจอแสดงข้อมูลขับขี่แบบ LCD มาพร้อมกับจอสัมผัส Suzuki Smart Connect ขนาด 7 นิ้ว ที่ควบรวมระบบนำทางที่แม่นยำ กับฟังก์ชั่นการเชื่อมต่อโทรศัพท์มือถือผ่าน Bluetooth พร้อมโปรแกรมสุดล้ำ Apple CarPlay สำหรับ iOS รวมถึงพวงมาลัยที่ออกแบบใหม่เป็นรูปตัว D เพื่อเพิ่มพื้นที่วางเท้าระหว่างเบาะและพวงมาลัย</p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/02/8-600x450.jpg" style="height:450px; width:600px" /></p>

            <p>นายวัลลภ ตรีฤกษ์งาม กรรมการบริหารด้านการขายและการตลาด บริษัท ซูซูกิ มอเตอร์ (ประเทศไทย) จำกัด กล่าวถึง กลยุทธ์การตลาดว่า &ldquo;กลุ่มเป้าหมายของ All New&nbsp;<strong>Suzuki&nbsp;SWIFT</strong>&nbsp;คือผู้ที่ซื้อรถเพื่อใช้งานเป็นรถคันแรก รายได้ระดับปานกลางขึ้นไป อายุตั้งแต่ 21-39 ปี ซึ่งเป็นวัยทำงานและเริ่มต้นสร้างครอบครัว โดยแบ่งออกเป็นกลุ่มอายุ 21-29 ปี ซึ่งให้ความสำคัญกับดีไซน์เพื่อสะท้อนถึงภาพลักษณ์ของตนเอง ชอบรถที่ขับสนุก ควบคุมง่าย และกลุ่มที่มีอายุ 30-39 ปี ซึ่งชอบรถที่มีดีไซน์ที่บอกถึงตัวตนและมาพร้อมฟังก์ชันการใช้งานที่คุ้มค่า ตอบโจทย์ทุกไลฟ์สไตล์ จึงเป็นที่มาของคอนเซ็ปต์ &lsquo;All New&nbsp;<strong>Suzuki&nbsp;SWIFT</strong>,<br />
            WE STANDOUT สไตล์เด่นบนเส้นทางที่แตกต่าง&rsquo;</p>

            <p>ในด้านการตลาด ครั้งนี้ทางซูซูกิได้ดึง ชาริล ชัปปุยส์ และ &ldquo;วี&rdquo; วิโอเลต วอเทียร์ ขึ้นแท่นพรีเซนเตอร์คนใหม่ล่าสุดของ All New&nbsp;<strong>Suzuki&nbsp;SWIFT</strong>&nbsp;เพื่อสะท้อนภาพลักษณ์ที่ทันสมัย และไลฟ์สไตล์ที่แตกต่างอย่างชัดเจน ผ่านช่องทางการสื่อสารที่ครอบคลุม ซึ่งเน้นสื่อออนไลน์มากขึ้น เพื่อให้เข้าถึงกลุ่มเป้าหมายได้โดยตรง สื่ออื่นๆ ทั้งโทรทัศน์ หนังสือพิมพ์ สื่อวิทยุ จอดิจิทัลและบิลบอร์ดทั่วประเทศ รวมถึงด้านบริการของพนักงานที่ผ่านขั้นตอนโปรแกรมการฝึกอบรมที่ได้มาตรฐานของซูซูกิ พร้อมบริการให้คำปรึกษาและแนะนำลูกค้าอย่างเต็มรูปแบบด้วยโชว์รูมผู้จำหน่ายรถยนต์&nbsp;<strong>ซูซูกิ&nbsp;</strong>ซึ่งจะครอบคลุม 120 สาขาทั่วประเทศ ภายในเดือนมีนาคมนี้</p>

            <p>&ldquo;นอกจากนี้&nbsp;<strong>ซูซูกิ&nbsp;</strong>ยังร่วมมือกับสถาบันการเงินจัดโปรแกรม My Way ผ่อนเริ่มต้นเพียง 3,999 บาท ต่อเดือน และแคมเปญพิเศษ Loyalty Program สำหรับลูกค้า&nbsp;<strong>SWIFT</strong>&nbsp;เพียงนำ&nbsp;<strong>SWIFT</strong>&nbsp;คันเดิมมาเปลี่ยนเป็น<strong>&nbsp;SWIFT</strong>&nbsp;ใหม่ รับทันทีส่วนลดอุปกรณ์ตกแต่ง 20,000 บาท อีกทั้งเราจะจัดกิจกรรมพิเศษที่โชว์รูม&nbsp;<strong>ซูซูกิ&nbsp;</strong>ทั้งหมดทั่วประเทศในวันที่ 10-11 กุมภาพันธ์ และกิจกรรมพิเศษที่ห้างสรรพสินค้าชั้นนำทั่วประเทศในวันที่ 17-18 กุมภาพันธ์ ซึ่งทุกท่านสามารถติดตามรายละเอียดเพิ่มเติมได้ที่&nbsp;<a href="http://www.suzuki.co.th/">www.suzuki.co.th</a>&nbsp;หรือ&nbsp;<a href="http://suzuki%20swift/">www.allnewsuzukiswift.com</a>&rdquo;</p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/02/6-1-600x400.jpg" style="height:400px; width:600px" /></p>

            <p>นายวัลลภ กล่าวเพิ่มเติมว่า &ldquo;สำหรับตลาดรถยนต์ในกลุ่มอีโคคาร์มียอดจำหน่ายรวมทั้งหมดในปี 2560 อยู่ที่ 156,239 คัน ซึ่ง&nbsp;<strong>ซูซูกิ</strong>มียอดจำหน่ายรวมเฉพาะรถยนต์อีโคคาร์จำนวน 21,300 คัน เติบโตขึ้นถึง 13.39% และมีส่วนแบ่งทางการตลาดของรถในกลุ่มนี้อยู่ที่ 13.63% สำหรับ&nbsp;<strong>ซูซูกิ สวิฟท์</strong>&nbsp;อีโคคาร์สายพันธุ์สปอร์ต ยังคงเป็นรถที่ได้รับความนิยม มียอดจำหน่ายสูงถึง 8,080 คัน เชื่อมั่นว่าในปี 2561 จากการที่แนวโน้มสภาพตลาดรถยนต์ที่มีทิศทางดีขึ้น&nbsp;<strong>ซูซูกิ</strong>&nbsp;ก็จะสามารถสร้างการเติบโตได้ดีขึ้นด้วยเช่นกัน&rdquo;</p>

            <p>All New&nbsp;<strong>Suzuki&nbsp;SWIFT</strong>&nbsp;มีให้เลือกทั้งหมด 6 สี ได้แก่ Ablaze Red Pearl, Star Silver Metallic, Mineral Gray Metallic, Super Black Pearl และ 2 สีใหม่ คือ Speedy Blue Metallic และ Pure White Pearl โดยมีทั้งหมด 4 รุ่นด้วยกัน ได้แก่ GA CVT, GL CVT, GLX CVT และ GLX-Navi CVT</p>

            <p>โดยมีทั้งหมด&nbsp;4 รุ่นด้วยกัน ได้แก่</p>

            <p>รุ่น&nbsp;GA CVT ราคา 499,000 บาท</p>

            <p>รุ่น&nbsp;GL CVT ราคา 536,000 บาท</p>

            <p>รุ่น&nbsp;GLX CVT ราคา 609,000 บาท</p>

            <p>รุ่น&nbsp;GLX-Navi CVT ราคา 629,000 บาท</p>
            ');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }

    public function description_7(){ 
        $status=array('<p>มีการเปิดจอง&nbsp;<strong>Nissan&nbsp;Qashqai</strong>&nbsp;โฉมใหม่รุ่นขับอัตโนมัติไปแล้วในสหราชอาณาจักร โดยมีในรุ่น Tekna เเละ Tekna+ ที่มาพร้อมกับเครื่องยนต์ดีเซล 130แรงม้า 1.6ลิตร เกียร์ออโตเมติกในราคาเริ่มต้น 31,275 ปอนด์หรือราวๆ 1.3ล้านบาท</p>
            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/02/3-6-600x400.jpg" style="height:400px; width:600px" /></p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/02/1-13-600x404.jpg" style="height:404px; width:600px" /></p>

            <p>เทคโนโลยีการขับเคลื่อนด้วยตนเองทำงานผ่านปุ่มคำสั่งพิเศษด้านขวาของพวงมาลัยที่จะช่วยควบคุมพวงมาลัยให้กับผู้ขับขี่, อัตราเร่งเเละเบรก ที่ถูกออกแบบมาให้ใช้ในการขับขี่ความเร็วสูงได้ แต่ควรใช้บนสภาพถนนที่การจราจรไม่คับคั่งเช่นถนนไฮเวย์เท่านั้น</p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/02/4-6-600x401.jpg" style="height:401px; width:600px" /></p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/02/5-5-600x400.jpg" style="height:400px; width:600px" /></p>

            <p>สำหรับคนที่ต้องการระบบขับเคลื่อนด้วยเกียร์ธรรมดา&nbsp;<strong>Qashqai</strong>&nbsp;ก็ใช้เครื่องยนต์แบบเดียวกันกับ dCi 130 ที่มาพร้อมกับระบบช่วยการขับขี่ หรือหากเพิ่มเงินอีก 495 ปอนด์ หรือราวๆ 2.1หมื่นบาทจะได้ระบบที่เพิ่มเข้ามาอย่าง ระบบช่วยรักษาช่องทางจราจร (Intelligent Lane Intervention), ควบคุมความเร็วในการขับขี่ (Intelligent Cruise Control) และ ระบบช่วยควบคุมรถให้อยู่ในเลน (Lane Keep Assist) ซึ่งระบบทั้งหมดนี้ทางบริษัทผู้ผลิตกล่าวว่าสามารถนำมาใช้กับระบบขับเคลื่อนอัตโนมัติได้</p>
            ');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }

    public function description_8(){ 
        $status=array('<p><strong>Nissan&nbsp;</strong>ประกาศราคา<strong>รถครอสโอเวอร์</strong>SUV ขนาดเล็กในตลาดสหรัฐอย่าง&nbsp;<strong>Nissan Kicks 2018&nbsp;</strong>ใหม่เริ่มต้นที่17,990 ดอลลาร์ หรือประมาณ5.73 แสนบาทและคาดว่าพร้อมที่จะจำหน่ายช่วงปลายเดือนพฤษภาคมนี้</p>
            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/05/18_Nis_Kicks_04-600x403.jpg" style="height:403px; width:600px" /></p>
            <p><strong>Nissan Kicks 2018&nbsp;</strong>ใหม่ ในตลาดอเมริกาเหนือมีให้เลือกทั้งหมด 3 รุ่นย่อย<strong>&nbsp;ได้แก่</strong></p>
            <ul>
                <li>Nissan Kicks S ราคา 17,990 ดอลลาร์ หรือประมาณ 73 แสนบาท</li>
                <li>Nissan Kicks SV ราคา 19,690 ดอลลาร์ หรือประมาณ 28 แสนบาท</li>
                <li>Nissan Kicks SR ราคา 20,290 ดอลลาร์ หรือประมาณ 47 แสนบาท</li>
            </ul>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/05/18_Nis_Kicks_05-600x325.jpg" style="height:325px; width:600px" /></p>

            <p>ขุมพลังของ&nbsp;<strong>Nissan Kicks 2018</strong>ใหม่ ในทุกรุ่นย่อย ติดตั้งเครื่องยนต์เบนซินขนาด 1.6 ลิตร ที่ให้กำลังสูงสุดถึง 125 แรงม้า และแรงบิดสูงสุด 156 นิวตันเมตร ทำงานร่วมกับเกียร์อัตโนมัติ XTRONIC (CVT) ขับเคลื่อนล้อหน้า เป็นมาตรฐานสำหรับทุกรุ่นย่อย</p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/05/2018_Nissan_KICKS_white_3_4_2__1_-600x353.jpg" style="height:353px; width:600px" /></p>

            <p>ภายในของ&nbsp;<strong>Nissan Kicks 2018&nbsp;</strong>ใหม่ เน้นพื้นที่ที่กว้างขวางกว่าเดิม ติดตั้งหน้าจอสัมผัสขนาด 7 นิ้ว ที่รอบรับทั้ง Android Auto และ Apple CarPlay มีกล้องมองรอบทิศทาง</p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/05/2018_Nissan_KICKS_Interior3-600x396.jpg" style="height:396px; width:600px" /></p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/05/2018_Nissan_KICKS_Interior2-600x400.jpg" style="height:400px; width:600px" /></p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/05/2018_Nissan_KICKS_Interior-600x400.jpg" style="height:400px; width:600px" /></p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/05/2018_Nissan_KICKS_Bose-600x400.jpg" style="height:400px; width:600px" /></p>

            <p>ระบบด้านความปลอดภัยของ&nbsp;<strong>Nissan Kicks 2018&nbsp;</strong>ใหม่ ในทุกรุ่นย่อย ประกอบไปด้วย ถุงลมนิรภัย 7 ตำแหน่ง, ระบบเบรกฉุกเฉิน และกล้องมองหลัง แต่ในรุ่น Nissan Kicks SV และ Nissan Kicks SR จะมีระบบเตือนจุดอับสายตา และระบบเตือนการจราจรด้านหลัง ซึ่งเป็นอุกปกรณ์มาตรฐานของ&nbsp;<strong>Nissan Kicks 2018</strong></p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/05/18_Nis_Kicks_03-600x389.jpg" style="height:389px; width:600px" /></p>

            <p><strong>Nissan Kicks 2018</strong>ใหม่มีสีตัวถังให้เลือกทั้งแบบโมโนโทนอีก 7 สี และทูโทน 5 คู่สี ได้แก่ หลังคาสีดำ-ตัวถังสีเขียว, หลังคาสีดำ-ตัวถังสีส้ม, หลังคาสีดำ-ตัวถังสีแดง, หลังคาสีส้ม-ตัวถังสีเทา และหลังคาสีขาว-ตัวถังสีน้ำเงิน</p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/05/2018_Nissan_Kicks_Blue-600x350.jpg" style="height:350px; width:600px" /></p>
            ');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }

    public function description_9(){ 
        $status=array('<p>หลังจากที่ได้เห็นข่าวตั้งแต่ยังไม่มีการผลิต รวมไปถึงเป็นรถยนต์ตัว concept ออกโชว์ตัวกันไปหลายต่อหลายเทศกาลยานยนต์ จนกระทั่งเปิดตัวกันไปหลายต่อหลายประเทศ และนำมาเปิดตัวให้ชาวไทยได้จับจองกันที่งาน Motor Expo เมื่อเดือนธันวาคม ปี 2017 ที่ผ่านมา&nbsp; ล่าสุดทาง&nbsp;<strong>Toyota</strong>&nbsp;ก็ได้จัด&nbsp;<strong>Toyota&nbsp;C-HR</strong>&nbsp;มาให้เราชาวยานยนต์อย่างเราได้ทดสอบตัวจริงๆกันซะที</p>
                <p><strong>Line: Toyota Thailand</strong></p>
                <p>&nbsp;</p>
                <p style="text-align:center"><iframe frameborder="0" height="360" src="https://seeme.me/ch/auto/embed/9BPmOq" width="640"></iframe></p>
                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_004-600x400.jpg" style="height:400px; width:600px" /></p>
                <p>ตั้งแต่มีการเปิดตัว<strong>&nbsp;Toyota&nbsp;C-HR</strong>&nbsp;ออกมา ก็มียอดจำหน่ายสะสมรวมทั้งสิ้นกว่า 283,000 คัน ใน 51 ประเทศทั่วโลก โดยสร้างยอดจำหน่ายสูงสุด เป็นอันดับหนึ่งในประเทศญี่ปุ่น และติด 1 ใน 5 รถรุ่นที่มียอดจำหน่ายสูงสุดในยุโรปในปีที่ผ่านมา</p>
                <p><strong>Toyota&nbsp;C-HR (Coupe High Rider)</strong>&nbsp;เป็นรถ&nbsp;<strong>Sub-Compact SUV</strong>&nbsp;ที่ได้แรงบันดาลใจในการออกแบบมาจากเพชร โดยจะเห็นได้จากการดีไซน์ที่เน้นความเหลี่ยม ตั้งแต่ภายนอกจน ไปจนถึงภายใน เราจะเห็นลูกเล่นรูปทรงเพชรซ่อนอยู่ภายในเต็มไปหมด ไม่ว่าจะเป็นในส่วนของการบุหลังคา แผงประตูข้าง ไปจนถึงลวดลายของเบาะโดยสาร ถึงแม้ว่าทาง&nbsp;<strong>Toyota</strong>&nbsp;จะให้รายละเอียดการออกแบบในส่วนนี้ว่า เป็นเพียงกิมมิคเล็กๆในการออกแบบเท่านั้น แต่กลับทำให้เราคิดว่าเป็นการใส่ใจในรายละเอียด ที่ถึงแม้จะดูเล็กน้อย แต่ก็แสดงถึงความตั้งใจในการออกแบบที่ดูจริงจังไม่น้อยเลยทีเดียว</p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_026-600x400.jpg" style="height:400px; width:600px" /></p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_027-600x400.jpg" style="height:400px; width:600px" /></p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_024-600x400.jpg" style="height:400px; width:600px" /></p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_025-600x400.jpg" style="height:400px; width:600px" /></p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_028-600x400.jpg" style="height:400px; width:600px" /></p>

                <p><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_053-600x400.jpg" style="height:400px; width:600px" /></p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/l1-600x386.jpg" style="height:386px; width:600px" /></p>

                <p>ในส่วนเทคโนโลยี&nbsp;<strong>Toyota&nbsp;C-HR</strong>&nbsp;ก็มาพร้อมกับ 4 เทคโนโลยีใหม่แบบ คือ</p>

                <ul>
                    <li>ระบบ<strong>&nbsp;New Generation Of Hybrid</strong>&nbsp;ที่นิยามถึงยนตกรรมอัจฉริยะของโลกยานยนต์ยุคใหม่ โดยระบบ&nbsp;Hybrid&nbsp;Gen 4 ที่อยู่ใน&nbsp;<strong>Toyota&nbsp;C-HR</strong>&nbsp;ก็ได้มีการพัฒนา มาจากรุ่นพี่อย่าง&nbsp;<strong>ToyotaPrius</strong>&nbsp;ใน Gen 3 มีแบตเตอรี่ขนาดเล็กลง สามารถเก็บประจุพลังงานได้เร็วและมากขึ้นกว่าเดิม รวมไปถึงการวางตำแหน่งของแบตเตอรี่ในจุดใหม่ ก็ทำให้ระบายความร้อนได้ดีมากขึ้น มีความทนทานสำหรับการใช้งาน และมีอัตราการประหยัดน้ำมันสูงถึง 24.4 กม./ ลิตร ส่วนการรับประกันนั้นก็หายห่วงเพราะทาง&nbsp;<strong>Toyota</strong>&nbsp;เค้ารับประกันคุณภาพระบบ&nbsp;<strong>Hybrid</strong>&nbsp;5 ปี และ รับประกันคุณภาพแบตเตอรี่ไปยาวๆถึง 10 ปี</li>
                    <li>โครงสร้าง&nbsp;<strong>TNGA (Toyota New Global Architecture)</strong>&nbsp;โครงสร้างที่ถูกพัฒนาให้ดีขึ้นกว่าเดิม ไม่ว่าจะเป็นในเรื่องของความแกร่งในส่วนของตัวถัง จุดศูนย์ถ่วงต่ำลง ลดการโคลงตัวของตัวถัง เพิ่มความมั่นใจในการเข้าโค้ง มีความคล่องตัว ช่วงล่างแบบ Double Wishbone ก็ช่วยเพิ่มในเรื่องของการยึดเกาะถนนและความนุ่มนวลในการขับขี่</li>
                    <li>มาตรฐานความปลอดภัยระดับโลก&nbsp;<strong>TSS</strong>&nbsp;หรือ<strong>&nbsp;Toyota&nbsp;Safety Sense</strong>&nbsp;มาแบบจัดเต็ม ด้วย ระบบความปลอดภัยก่อนชน ระบบนี้มีความน่าสนใจไม่น้อย เพราะระบบจะช่วยเบรคเพื่อลดความเร็วลงมาที่ 38 กม./ชม. ในระดับความเร็วที่ 50 กม./ชม.หรือมากกว่า ในส่วนนี้หลายๆคนคงสงสัยว่าทำไมระบบไม่ช่วยเบรคแบบ 100% ทางทีมงาน&nbsp;<strong>Toyota</strong>&nbsp;ก็ให้คำตอบว่า การที่วิศวกร set ระบบมาแบบนี้ก็เพื่อ ระบบจะช่วยครึ่งนึง และอีกครึ่งนึงก็คือให้ผู้ขับขี่เองตัดตินใจด้วยตัวเอง รวมไปถึงเพื่อลดอุบัติเหตุบนท้องถนน เพราะถ้าระบบช่วยเบรคทำงานแบบ 100% รถจะหยุดชะงักทันที ซึ่งนั่นก็จะส่งผลให้รถคันหลังที่ตามมาแบบกระชั้นชิดเบรคไม่ทันและอาจชนได้ ในส่วนระบบความปลอดภัยอื่นๆก็จะมี ระบบควบคุมความเร็วอัตโนมัติ, ระบบปรับไฟสูงอัตโนมัติ และระบบแจ้งเตือนรถออกนอกเลนพร้อมหน่วงพวงมาลัยอัตโนมัติ</li>
                    <li><strong>Toyota&nbsp;T-Connect Telematics</strong>&nbsp;ระบบเชื่อมต่อผู้ขับขี่ผ่าน smart phone และ apple watch ที่ช่วยในเรื่องของ GPS ที่สามารถขอความช่วยเหลือจาก operator ได้ 24 ชม. ตรวจสอบตำแหน่งพิกัดของรถ ในกรณีที่รถถูกโจรกรรม และมีบริการ Wi-Fi ซึ่งจะใช้ได้ฟรีในส่วนของปีแรก และสามารถต่อการใช้บริการได้แบบปีต่อปี ด้วยค่าบริการแบบเหมารายปีประมาณ 2,000 บาท</li>
                </ul>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_052-600x400.jpg" style="height:400px; width:600px" /></p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_044-600x400.jpg" style="height:400px; width:600px" /></p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_045-600x400.jpg" style="height:400px; width:600px" /></p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_046-600x400.jpg" style="height:400px; width:600px" /></p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_049-600x400.jpg" style="height:400px; width:600px" /></p>

                <p><strong>Toyota&nbsp;C-HR</strong>&nbsp;ใช้เครื่องยนต์ 2ZR-FXE ขนาด 1.8 ลิตร ระบบเผาใหม้แบบ Atkinson Cycle พร้อมระบบ VVT-I ที่นอกจากจะประหยัดแล้ว ยังเป็นมิตรกับสิ่งแวดล้อมอีกด้วย ส่งกำลังด้วยระบบเกียร์อัจฉริยะ E-CVT ให้กำลัง 98 แรงม้า ที่ 5,200 รอบต่อนาที แรงบิดสูงสุด 142 นิวตัน-เมตร ที่ 3,600 รอบต่อนาที ประสานการทำงานร่วมกับ&nbsp;มอเตอร์ไฟฟ้าขนาด 53 กิโลวัตต์ ที่แรงบิดอีก 163 นิวตัน-เมตร</p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_031-600x312.jpg" style="height:312px; width:600px" /></p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_050-600x400.jpg" style="height:400px; width:600px" /></p>

                <p>มาถึงช่วงของการ&nbsp;<strong>Test Drive</strong>&nbsp;ด้วย&nbsp;<strong>Toyota&nbsp;C-HR</strong>&nbsp;ตัว Top รหัส&nbsp;<strong>HV Hi</strong><strong>&nbsp;</strong>เครื่องยนต์&nbsp;<strong>Hybrid</strong>&nbsp;เส้นทาง&nbsp;ลำปาง &ndash; น่าน ที่มีระยะทาง 220 กม. ในส่วนของการขับขี่ สัมผัสแรกที่สัมผัสได้เลยหลังจากอยู่หลังพวงมาลัยก็คือ เครื่องยนต์เงียบมาก ออกตัวนิ่ม&nbsp; แต่พุ่ง ไม่มีความอืดเลยแม้แต่น้อย พวงมาลัยเบา การบังคับทิศทางค่อนข้างคม เพราะด้วยเส้นทางที่มีโค้งเยอะ เราเลยสัมผัสในส่วนนี้ได้อย่างชัดเจน ความคล่องตัวและ balance ดีเยี่ยม ในส่วนของอัตราเร่งอยู่ในระดับ กลางๆ ไม่หวือหวามาก จังหวะคิกดาวน์ช่วงจะเร่งแซงต้องรอรอบนิดนึง ส่วนตัวคิดว่าเป็นเรื่องปกติของรถ&nbsp;<strong>Hybrid</strong>&nbsp;ความเร็วในการขับขี่ครั้งนี้ เราใช้ความเร็วอยู่ที่ประมาณ 120 กม./ ชม บวกลบนิดหน่อย แล้วแต่เส้นทางในบางช่วง ในตอนแรกคิดว่าเครื่องยนต์น่าจะซดน้ำมันสุกเลย แต่เอาจริงๆแล้ว กลับกลายเป็นเข็มน้ำมันแทบไม่กระดิกลง ประหยัดแบบน่าประหลาดใจสุดๆ ส่วนตัวเรารู้สึกประทับใจมากในส่วนนี้ ทาง&nbsp;<strong>Toyota</strong>&nbsp;เคลมไว้ว่าอัตรสิ้นเปลืองน้้ำมันของ&nbsp;&nbsp;<strong>Toyota&nbsp;C-HR&nbsp;</strong>อยู่ที่ 24.4 กม.&nbsp;ตลอดเส้นทางการเดินทางมีช่วงให้แบตเตอรี่ได้ชาร์จไฟ ระหว่างการขับขี่ตลอดเวลา ส่วนตัวคิดว่าถ้าเป็นรถใช้ขับขี่ในเมืองเป็นส่วนใหญ่ น่าจะประหยัดได้มากกว่านี้อย่างแน่นอน</p>

                <p>มาถึงห้องโดยสารกันบ้าง ห้องโดยสารสีทูโทนดูสบายตา (สีดำและน้ำตาล) เบาะคู่หน้าทรงสปอร์ตใช้วัสดุหนังสังเคราะห์&nbsp; เบาะนั่งด้านหลังสามารถแยกพับได้ 60:40 กระจกหน้าต่างไฟฟ้า มาพร้อมกับระบบป้องกันการหนีบ 4 ตำแหน่ง ส่วนตัวคิดว่าภายในห้องโดยสารค่อนข้างกว้าง ห้องโดยหลังนั่งสบายเหมาะกับครอบครัวขนาดเล็กถึงกลางนั่งได้สบาย 3-4 คนก่อนหน้านี้ได้ยินเรื่องมุมมอง ในเรื่องของหน้าต่างในส่วนของผู้โดยสารด้านหลังมาพอสมควร ว่าเล็กไป แต่ทางกลับกันหากลองมองดูรถยนต์&nbsp;<strong>Sub-Compact SUV&nbsp;</strong>ปัจจุบัน หลายๆ แบรนด์มีการปรับโฉมด้านหลังให้มีมุมมองแบบนี้กันทั้งนั้น เรื่องนี้น่าจะเป็นเทรนด์ใหม่ที่คนยังไม่คุ้นเคยชินมากนัก คาดว่าหากให้เวลาอีกนิดหลายคนน่าจะชอบดีไซน์ของมุมมองด้านหลังในห้องโดยสารได้ไม่ยาก หน้าจอเป็นแบบ TFT ขนาด 4.2 นิ้วแสดงข้อมูลการขับขี่แบบ MID ที่จะมีค่าบอกในเรื่องของพลังงานแบตเตอรี่อยู่ตรงส่วนกลางจอ หน้าปัดเรือนไมล์ และแสดงผลในส่วนของโหมดการขับขี่ที่มีให้เลือกทั้ง Sport และ ECO รวมไปถึงระบบแจ้งเตือนต่างๆ</p>

                <p><strong>Toyota&nbsp;C-HR&nbsp;</strong>มาพร้อมกับระบบความปลอดภัย Trustable Safety มาตรฐานความปลอดภัยขั้นสูงสุดในทุกสถานการณ์ด้วยระบบป้องกันล้อล็อค ABS (Anti-lock Brake System),ระบบกระจายแรงเบรก EBD (Electronic Brake-force Distribution),ระบบเสริมแรงเบรก BA (Brake Assist),ระบบควบคุมการทรงตัว VSC (Vehicle Stability Control),ระบบป้องกันล้อหมุนฟรี TRC (Traction Control),ระบบช่วยการออกตัวบนทางลาดชัน HAC (Hill-start Assist Control),ถุงลมเสริมความปลอดภัย ระบบ SRS (คู่หน้า/ด้านข้าง/ม่านด้านข้าง/หัวเข่าฝั่งคนขับ),สัญญาณเตือนกะระยะด้านหน้า ท้าย และที่มุมกันชน,ระบบช่วยเตือนมุมอับสายตาที่กระจกมองข้าง (Blind Spot Monitor),ระบบช่วยเตือนขณะถอยรถ RCTA (Rear Cross Traffic Alert),ระบบความปลอดภัยก่อนการชน (Pre-Collision System),ระบบควบคุมความเร็วอัตโนมัติ (Cruise Control),ระบบปรับไฟสูงอัตโนมัติ (Auto High Beams),ระบบเตือนเมื่อออกนอกเลน พร้อมพวงมาลัยหน่วงอัตโนมัติ (Lane Departure Alert with Steering Assist),ระบบช่วยเตือนเมื่อเหนื่อยล้าขณะขับขี่,สัญญาณเตือนการโจรกรรม TDS และระบบกุญแจนิรภัย Immobilizer</p>

                <p>สำหรับอุปกรณ์อำนวยความสะดวกก็มี ระบบควบคุมการเปิด-ปิดไฟหน้าอัตโนมัติ พร้อมระบบ Follow-Me-Home, ปุ่ม&nbsp;Push Start และระบบเปิดประตูอัจฉริยะ ,เบรกมือไฟฟ้า ,ระบบ Auto Brake Hold, พวงมาลัย Multi function,ที่ปัดน้ำฝนแบบหน่วงเวลาและปรับตั้งเวลาได้, กระจกมองหลังแบบปรับลดแสงอัตโนมัติ และระบบกรองอากาศภายในห้องโดยสาร nanoe</p>

                <p>ในส่วนของระบบ Infotainment ก็ประกอบไปด้วย หน้าจอ DVD แบบ touchscreen ขนาด 7 นิ้ว ลำโพง 6 ตัว เชื่อมต่อด้วยบลูทูธ&nbsp;สามารถเชื่อมต่อได้ทั้ง USB/HDMII และ Micro SD Card พร้อมระบบนำทาง navigator ที่จะมีเฉพาะในรุ่น HV HI เท่านั้น</p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_019-600x400.jpg" style="height:400px; width:600px" /></p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_013-600x396.jpg" style="height:396px; width:600px" /></p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_010-600x383.jpg" style="height:383px; width:600px" /></p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_006-600x362.jpg" style="height:362px; width:600px" /></p>

                <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_020-600x400.jpg" style="height:400px; width:600px" /></p>

                <p><img alt="" src="https://auto.mthai.com/app/uploads/2018/03/CHR-G.2_039-600x400.jpg" style="height:400px; width:600px" /></p>

                <p>หลังจากที่ได้สัมผัส&nbsp;<strong>Toyota&nbsp;C-HR</strong>&nbsp;ก็ทำให้รู้สึกได้เลยว่า&nbsp;&ldquo;คุ้มค่าที่ได้ลอง&rdquo; จริงๆ ตอบโจทย์ในเรื่องของการขับขี่ได้อย่างลงตัว ไม่ว่าจะเป็นเรื่องเทคโนโลยีของเครื่องยนต์ อัตราการประหยัดน้ำมันที่การทดสอบขับครั้งนี้เเม้เส้นทางส่วนใหญ่จะเป็นทางขึ้นเขา แต่&nbsp;<strong>Toyota&nbsp;C-HR&nbsp;</strong>&nbsp;ก็ทำให้เรารู้ว่าตลอดระยะทาง 220 กม. ราใช้น้ำมันไปเพียง 19.2 ล/กม. &nbsp;<strong>Toyota&nbsp;C-HR&nbsp;</strong>เป็นรถที่ขับขี่สนุก เหมาะกับการใช้งานทั้งในเมือง รวมไปถึงการขับออกต่างจังหวัดก็ถือว่าไม่น้อยหน้าใครเช่นเดียวกัน เรียกว่าเป็นรถยนต์อีกหนึ่งรุ่นอเนกประสงค์สำหรับครอบครัวที่สร้างความประทับใจในการขับขี่ให้กับใครหลายๆ คนได้แบบไม่ยาก</p>

                <p><strong>ราคา&nbsp;Toyota&nbsp;C-HR มีดังนี้</strong></p>

                <p><strong>Toyota&nbsp;C-HR&nbsp;2018 รุ่น 1.8 Entry ราคา&nbsp;979,000 บาท</strong></p>

                <p><strong>Toyota&nbsp;C-HR&nbsp;2018 รุ่น 1.8 MID ราคา&nbsp;1,039,000 บาท</strong></p>

                <p><strong>Toyota&nbsp;C-HR&nbsp;2018 รุ่น HV MID&nbsp;ราคา&nbsp;1,069,000 บาท</strong></p>

                <p><strong>Toyota&nbsp;C-HR&nbsp;2018 รุ่น HV HI ราคา&nbsp;1,159,000 บาท</strong></p>

                <p>สัมผัสประสบการณ์สุดพิเศษ และทดลองขับ&nbsp;<strong>Toyota&nbsp;C-HR ที่&nbsp;Toyota&nbsp;Driving Experience Park&nbsp;</strong>ถนนบางนา &ndash; ตราด กม. 3 หรือที่โชว์รูม ผู้แทนจำหน่าย&nbsp;<strong>Toyota&nbsp;</strong>437 แห่ง ทั่วประเทศ และ พิเศษสุด สำหรับผู้ที่ร่วมทดลองขับ&nbsp;<strong>Toyota&nbsp;C-HR&nbsp;</strong>ทุกรุ่น ตั้งแต่วันนี้ &ndash; 8 เมษายน 2561 รับสสิทธิร่วมลุ้นรับ Apple Watch Series 3 รุ่น GPS จำนวน 100 รางวัล</p>

                <p>ติดตามข้อมูลข่าวสาร และข้อมูลสินค้าได้ที่<br />
                <a href="https://www.toyota.co.th/index.php"><strong>www.toyota.co.th</strong></a></p>
                ');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }

    public function description_10(){ 
        $status=array('<p><strong>Toyota&nbsp;Aqua Rirvie&nbsp;</strong>2018 ใหม่ รุ่นตกแต่งพิเศษเน้นความหรูหรามากกว่ารุ่นปกติ โดยสำนักแต่งจาก&nbsp;<strong>Modellista</strong>&nbsp;ราคาเริ่มต้นอยู่ที่ 2,197,800 &ndash; 2,351,160 เยน หรือประมาณ 639,000 &ndash; 684,000 บาท</p>
            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/04/Toyota-Aqua-Rirvie-600x306.jpg" style="height:306px; width:600px" /></p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/04/Toyota-Aqua-Rirvie-Toyota-Aqua-Rirvie_07-600x314.jpg" style="height:314px; width:600px" /></p>

            <p>ภายในของ&nbsp;<strong>Toyota&nbsp;Aqua Rirvie&nbsp;</strong>2018 ใหม่ ตกแต่งวัสดุหนังสังเคราะห์ที่แผงคอนโซล เบาะที่นั่งเป็นสีเบจหุ้มด้วยวัสดุหนัง พร้อมเดินตะเข็บไขว้ แถมระบบเบาะอุ่นที่คู่หน้า, มีระบบฟอกอากาศ Nanoe, มีกระจกหน้าต่างที่สามารถกรองรังสี UV และอินฟราเรดได้อีกด้วย</p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/04/Toyota-Aqua-Rirvie_09-600x305.jpg" style="height:305px; width:600px" /></p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/04/Toyota-Aqua-Rirvie_08-600x306.jpg" style="height:306px; width:600px" /></p>

            <p>ภายนอกของ&nbsp;<strong>Toyota&nbsp;Aqua Rirvie&nbsp;</strong>2018 ใหม่ ติดตั้งคิ้วตกแต่งไฟหน้าสีดำ เพิ่มคิ้วกระจังหน้าแบบโครเมียม ประตูท้ายเป็นสีดำ สปอยเลอร์สีดำ ฝาครอบกระจกข้างเป็นสีดำ ติดป้ายสัญลักษณ์ Rirvie บริเวณด้านข้างของด้านหน้า พร้อมล้ออัลลอย Modellista SpinAir II สี Gun Metallic ขนาด 15 นิ้ว เป็นต้น</p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/04/Toyota-Aqua-Rirvie_05-600x314.jpg" style="height:314px; width:600px" /></p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/04/Toyota-Aqua-Rirvie_06-600x517.jpg" style="height:517px; width:600px" /></p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/04/Toyota-Aqua-Rirvie_02-600x565.jpg" style="height:565px; width:600px" /></p>

            <p><strong>Toyota&nbsp;Aqua Rirvie&nbsp;</strong>2018 ใหม่ ติดตั้งเครื่องยนต์เบนซินไฮบริด ที่มีขนาด 1.5 ลิตร ให้กำลังสูงสุด 74 แรงม้า ที่ 4,800 รอบต่อนาที และแรงบิดอยู่ที่ 111 นิวตันเมตร ที่ 3,600-4,400 รอบต่อนาที ทำงานคู่กับมอเตอร์ไฟฟ้าที่ให้กำลัง 61 แรงม้า และแรงงบิด 169 นิวตันเมตร</p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/04/Toyota-Aqua-Rirvie_10-600x590.jpg" style="height:590px; width:600px" /></p>

            <p>นอกจากนี้ ตัวถังของ&nbsp;<strong>Toyota&nbsp;Aqua Rirvie&nbsp;</strong>2018 ใหม่ มีให้เลือกทั้งหมด 3 สี ได้แก่ สีขาวมุก White Pearl Crystal Shine, สีแดง Super Red V และสีน้ำเงิน Blue Metallic</p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/04/Toyota-Aqua-Rirvie_13-600x321.jpg" style="height:321px; width:600px" /></p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/04/Toyota-Aqua-Rirvie_11-600x321.jpg" style="height:321px; width:600px" /></p>

            <p style="text-align:center"><img alt="" src="https://auto.mthai.com/app/uploads/2018/04/Toyota-Aqua-Rirvie_12-600x321.jpg" style="height:321px; width:600px" /></p>

            <p>&nbsp;</p>
            ');
        $random_status=array_rand($status,1);
        return $status[$random_status];
    }


}


