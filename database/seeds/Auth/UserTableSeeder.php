<?php

use App\Models\Auth\User;
use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class UserTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Add the master administrator, user id of 1
        User::create([
            'employee_id'       => '2001',
            'first_name'        => 'Admin',
            'last_name'         => 'Istrator',
            'phone'             => '012345678',
            'email'             => 'admin2@admin.com',
            'nickname'          => 'Admin - Wat',
            'company_name'      => 0,
            'position_name'     => 'Programmer',
            'belong'            => 'Web Dev',
            'start_work'        => '01-08-2017',
            'username'          => 'root',
            'password'          => bcrypt('root'),
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
            'type_register'     => 'employee'
        ]);

        User::create([
            'employee_id'       => '2002',
            'first_name'        => 'Backend',
            'last_name'         => 'User',
            'phone'             => '0854067588',
            'email'             => 'executive@executive.com',
            'nickname'          => 'Backend - june',
            'company_name'      => 0,
            'position_name'     => 'Programmer',
            'belong'            => 'Web Dev',
            'start_work'        => '01-08-2017',
            'username'          => 'customer',
            'password'          => bcrypt('customer'),
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
            'type_register'     => 'employee'
        ]);

        User::create([
            'employee_id'       => '2003',
            'first_name'        => 'Default',
            'last_name'         => 'User',
            'phone'             => '012345678',
            'email'             => 'user@user.com',
            'nickname'          => 'Default - June',
            'company_name'      => 0,
            'position_name'     => 'Programmer',
            'belong'            => 'Web Dev',
            'start_work'        => '01-08-2017',
            'username'          => 'user',
            'password'          => bcrypt('user'),
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
            'type_register'     => 'employee'
        ]);

        $this->enableForeignKeys();
    }
}
