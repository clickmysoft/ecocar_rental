<?php

return [
    'status' => true,

    'carid' =>[
            0   =>  ['ecocar'],
            1   =>  ['LP'],
            2   =>  ['lab']
    ],

    /*
     * ภาษาไทย
     */

    'th' => [
        'name' => [
            0   => ['อีโก้คาร์'],
        ],
        'position' =>[
            0   =>  ['ผู้จัดการเขต'],
            1   =>  ['เจ้าหน้าที่ขาย'],
            2   =>  ['ผู้จัดการสาขา'],
            3   =>  ['เจ้าหน้าที่บริการลูกค้า'],
            4   =>  ['ผู้สร้างเนื้อหา'],
            100 =>  ['ผู้ใช้งาน'],
        ],

        'belong'    =>[
                    'DonmueangAirport'      =>  ['สนามบินดอนเมือง'],
                    'SuvarnabhumiAirport'   =>  ['สนามบินสุวรรณภูมิ'],
                    'HatyaiAirport'         =>  ['สนามบินหาดใหญ่'],
                    'Downtown'              =>  ['ดอนเมือง'],
                    'Onnut'                 =>  ['อ่อนนุช'],
                    'Bangwa'                =>  ['บางหว้า'],
                    'Ladprao'               =>  ['ลาดพร้าว'],
                    'Nonthaburi'            =>  ['นนทบุรี'],
                    'Samrong'               =>  ['สำโรง'],
                    'UbonAirport'           =>  ['อุบล/สนามบินอุบล'],
                    'ChiangmaiAirport'      =>  ['เชียงใหม่/สนามบินเชียงใหม่'],
                    'Pattaya'               =>  ['พัทยา']
                    
        ],

    ],

     /*
     * ภาษาอังกฤษ
     */

    'en' => [
        'name' => [
                'ecocar'   => ['ECOCAR'],
        ],

        'name' => [
                0   => ['ECOCAR'],
        ],

        'position'  =>[
                    0   =>  ['Manager'],
                    1   =>  ['Sale'],
                    2   =>  ['Branch manager'],
                    3   =>  ['Customer Service Officer'],
                    4   =>  ['Content Creator']
        ],

        'belong'    =>[
                    'DonmueangAirport'      =>  ['DonMueangAirport(DMK)'],
                    'SuvarnabhumiAirport'   =>  ['SuvarnabhumiAirport(BKK)'],
                    'HatyaiAirport'         =>  ['HatYaiAirport(HDY)'],
                    'Downtown'              =>  ['Don Mueang'],
                    'Onnut'                 =>  ['On Nut'],
                    'Bangwa'                =>  ['Bang Wa'],
                    'Ladprao'              =>  ['Lat Phrao'],
                    'Nonthaburi'            =>  ['Nonthaburi'],
                    'Samrong'               =>  ['Samrong'],
                    'UbonAirport'           =>  ['Ubon Airport'],
                    'ChiangMaiAirport'      =>  ['ChiangMai Airport'],
                    'Pattaya'               =>  ['Pattaya']
                    
        ],
    ],

    
];
