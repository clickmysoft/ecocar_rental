<?php

return [


    'status' => true,

    'th' => [
        'name'  =>[
                    'bangkok_insurance'         => ['กรุงเทพประกันภัย'],
                    'viri_insurance'            => ['วิริยะประกันภัย'],
                    'synmankong_insurance'      => ['สินมั่นคงประกันภัย'],
                    'thanachart_insurance'      => ['ธนชาตประกันภัย'],
                    'assets_insurance'          => ['สินทรัพย์ประกันภัย'],
                    'direct_asia_insurance'     => ['Direct Asia Insurance'],
                    'axa_insurance'             => ['AXA Insurance'],
        ],
        'bangkok_insurance'     =>[
                    'type1'                     => ['Comprehensive'],
                    'type2+'                     => ['Third party Fire and Theft + vehicle collision'],
                    'type2'                     => ['Third-Party Fire and Theft'],
                    'type3+'                    => ['Third Party + vehicle collision'],
                    'type3'                     => ['Third-Party Only']
        ],

        'viri_insurance'        =>[
                    'type1'                     => ['Maximum coverage'],
                    'type2'                     => ['Coverage slightly less to Type 1'],
                    'type3'                     => ['Providing coverage for third parties only'],
                    'type4'                     => ['This policy provides coverage for property of third parties'],
                    'type5'                     => ['Insurance coverage for specific perils']

        ],

        'synmankong_insurance'  =>[
                    'type1'                     => ['First class insurance premium starting 9,600 baht'],
                    'type2+'                    => ['Coverage for both collision and lost fire'],
                    'type2'                     => ['Does not cover damage to the insured vehicle.'],
                    'type3'                     => ['Will only cover damage to life and property to third parties.'],
                    'type3+'                    => ['Covers both car and car insurance up to a maximum of 150,000 baht.']

        ],
        'thanachart_insurance'  =>[
                    'type1'                     => ['Comprehensive'],
                    'type2+'                     => ['Third party Fire and Theft + vehicle collision'],
                    'type3+'                    => ['Third Party + vehicle collision'],
                    'type2'                     => ['Third-Party Fire and Theft'],
                    'type3'                     => ['Third-Party Only']

        ],
        'assets_insurance'        =>[
                    'type1'                      => ['กรมธรรม์ประเภท 1'],
                    'type2'                      => ['กรมธรรม์ประเภท 2 '],
                    'type3'                      => ['กรมธรรม์ประเภท 3'],
                    'type2+'                     => ['กรมธรรม์ประกันภัยรถยนต์แบบคุ้มครองเฉพาะภัย ชั้น 2+'],
                    'type3+'                     => ['กรมธรรม์ประกันภัยรถยนต์แบบคุ้มครองเฉพาะภัย ชั้น 3+']

        ],
        'direct_asia_insurance' =>[
                    'type1'                     => ['Comprehensive'],
                    'type2'                     => ['Third party Fire and Theft + vehicle collision'],
                    'type3+'                    => ['Third Party + vehicle collision'],
                    'type2'                     => ['Third-Party Fire and Theft'],
                    'type3'                     => ['Third-Party Only']

        ],

        'axa_insurance' =>[
                    'axa_smart_drive_1'         => ['Our most extensive coverage for a worry-free drive.'],
                    'axa_smart_drive_1_save'    => ['The less you drive, the less you pay.'],
                    'axa_motor_2+_max'          => ['Class 2+ motor insurance with maximum coverage'],
                    'axa_motor_2+_save'         => ['Class 2+ motor insurance with a variety of coverage options.'],
                    'axa_motor_2+_super_save'   => ['Third-Party Only'],
                    'axa_motor_3+'              => ['Ready to take care ofyou 24/7'],
                    'axa_motor_3_extra'         => ['3rd Class insurance with free Roadside Assistance'],
                    'motor_add_on'

        ],

        'aig_insurance' =>[
                    'class1'                    => ['Comprehensive'],
                    'class2+'                   => ['Third-Party Fire and Theft'],
                    'class3+'                   => ['Third-Party Fire and Theft'],
                    'class2'                    => ['Protection for your car from fire or theft'],
                    'class3'                    => ['protection for your legal liability to any third party'],

        ],

        'Option Extra' =>[
                    'gearbox'                   =>  ['Gearbox'],
                    'baby_seat'                 =>  ['Baby Seat'],
                    'parking_sensors'           =>  ['Parking sensors'],
                    'bluetooth'                 =>  ['Bluetooth'],
                    'manual_gearbox'            =>  ['Manual gearbox '],
                    'big_wheels'                =>  ['Big wheels']


        ],
    ],

    'en' => [
        'name'  =>[
                    'bangkok_insurance'         => ['Bangkok Insurance'],
                    'viri_insurance'            => ['Viriyah Insurance'],
                    'synmankong_insurance'      => ['Syn Man Kong Insurance'],
                    'thanachart_insurance'      => ['Thanachart Insurance'],
                    'assets_insurance'          => ['Assets Insurance'],
                    'direct_asia_insurance'     => ['Direct Asia Insurance'],
                    'axa_insurance'             => ['AXA Insurance'],
        ],            
        'insurance'             =>[
                'bangkok'                   => ['Bangkok Insurance'],
                'viriyah'                   => ['Viriyah Insurance'],
                'synmankong'                => ['Syn Man Kong Insurance'],
                'thanachart'                => ['Thanachart Insurance'],
                'directasia'                => ['Direct Asia Insurance'],
                'axa'                       => ['AXA Insurance'],
                'gobear'                    => ['Go Bear Insurance'],
                'aig'                       => ['AIG Insurance'],
        ],
        'bangkok_insurance'     =>[
                    'type1'                     => ['Comprehensive'],
                    'type2'                     => ['Third party Fire and Theft + vehicle collision'],
                    'type3+'                    => ['Third Party + vehicle collision'],
                    'type2'                     => ['Third-Party Fire and Theft'],
                    'type3'                     => ['Third-Party Only']

        ],

        'viri_insurance'        =>[
                    'type1'                     => ['Maximum coverage'],
                    'type2'                     => ['Coverage slightly less to Type 1'],
                    'type3'                     => ['Providing coverage for third parties only'],
                    'type4'                     => ['This policy provides coverage for property of third parties'],
                    'type5'                     => ['Insurance coverage for specific perils']

        ],

        'synmankong_insurance'  =>[
                    'type1'                     => ['First class insurance premium starting 9,600 baht'],
                    'type2'                     => ['Does not cover damage to the insured vehicle.'],
                    'type2+'                    => ['Coverage for both collision and lost fire'],
                    'type3'                     => ['Will only cover damage to life and property to third parties.'],
                    'type3+'                    => ['Covers both car and car insurance up to a maximum of 150,000 baht.']

        ],
        'thanachart_insurance'  =>[
                    'type1'                     => ['Comprehensive'],
                    'type2'                     => ['Third party Fire and Theft + vehicle collision'],
                    'type3+'                    => ['Third Party + vehicle collision'],
                    'type2'                     => ['Third-Party Fire and Theft'],
                    'type3'                     => ['Third-Party Only']

        ],
        'assets_insurance'        =>[
                    'type1'                     => ['Comprehensive'],
                    'type2'                     => ['Third Party Liability, Fire and Theft'],
                    'type3'                     => ['Third Party Liability Only'],
                    'type2+'                     => ['This policy provides coverage for property of third parties'],
                    'type3+'                     => ['Insurance coverage for specific perils']

        ],
        'direct_asia_insurance' =>[
                    'type1'                     => ['Comprehensive'],
                    'type2'                     => ['Third party Fire and Theft + vehicle collision'],
                    'type3+'                    => ['Third Party + vehicle collision'],
                    'type2'                     => ['Third-Party Fire and Theft'],
                    'type3'                     => ['Third-Party Only']

        ],

        'axa_insurance' =>[
                    'axa_smart_drive_1'         => ['Our most extensive coverage for a worry-free drive.'],
                    'axa_smart_drive_1_save'    => ['The less you drive, the less you pay.'],
                    'axa_motor_2+_max'          => ['Class 2+ motor insurance with maximum coverage'],
                    'axa_motor_2+_save'         => ['Class 2+ motor insurance with a variety of coverage options.'],
                    'axa_motor_2+_super_save'   => ['Third-Party Only'],
                    'axa_motor_3+'              => ['Ready to take care ofyou 24/7'],
                    'axa_motor_3_extra'         => ['3rd Class insurance with free Roadside Assistance'],
                    'motor_add_on'

        ],

        'aig_insurance' =>[
                    'class1'                    => ['Comprehensive'],
                    'class2+'                   => ['Third-Party Fire and Theft'],
                    'class3+'                   => ['Third-Party Fire and Theft'],
                    'class2'                    => ['Protection for your car from fire or theft'],
                    'class3'                    => ['protection for your legal liability to any third party'],

        ],

        'Option Extra' =>[
                    'gearbox'                   =>  ['Gearbox'],
                    'baby_seat'                 =>  ['Baby Seat'],
                    'parking_sensors'           =>  ['Parking sensors'],
                    'bluetooth'                 =>  ['Bluetooth'],
                    'manual_gearbox'            =>  ['Manual gearbox '],
                    'big_wheels'                =>  ['Big wheels']


        ],
    ],

];