<?php

return [

    'gear' => [
        'automatic_gear' => ['Automatic Gear'],
        'manual_gear'    => ['Manual Gear']
    ],

    'status' => true,

    'brands_img' => [
            'honda'             => ['honda.png'],
            'toyota'            => ['toyota.png'],
            'isuzu'             => ['isuzu.png'],
            'nissan'            => ['nissan.png'],
            'mitsubishi'        => ['mitsubishi.png'],
            'mercedes_benz'     => ['mercedes_benz.png'],
            'mazda'             => ['mazda.png'],
            'ford'              => ['ford.png'],
            'chevrolet'         => ['chevrolet.png'],
    ],

    'type_img' => [
            'cabriolet'         =>  ['cabriolet.png'],
            'mpv'               =>  ['mpv.png'],
            'suv'               =>  ['suv.png'],
            'truck'             =>  ['truck.png'],
            'wagon'             =>  ['wagon.png'],
            'pickup'            =>  ['pickup.png'],
            'coupe'             =>  ['coupe.png'],
            'sedan'             =>  ['sedan.png'],
            'hatchback'         =>  ['hatchback.png'],
            'van'               =>  ['van.png'],
            'convertible'       =>  ['convertible.png']
    ],

    /*
     * ภาษาไทย
     */

    'th' => [

            'brands' => [
                'honda' => [
                            'accord'                =>  ['Accord'],
                            'br_v'                  =>  ['BR-V'],
                            'brio'                  =>  ['Brio'],
                            'city'                  =>  ['City'],
                            'civic'                 =>  ['Civic'],
                            'cr_v'                  =>  ['CR-V'],
                            'cr_x_del_sol'          =>  ['CR-X del Sol'],
                            'cr_z'                  =>  ['CR-Z'],
                            'crossroad'             =>  ['CROSSROAD'],
                            'elysion'               =>  ['Elysion'],
                            'fit'                   =>  ['Fit'],
                            'freed'                 =>  ['Freed'],
                            'hr_v'                  =>  ['HR-V'],
                            'inspire'               =>  ['Inspire'],
                            'integra'               =>  ['Integra'],
                            'jazz'                  =>  ['Jazz'],
                            'legend'                =>  ['Legend'],
                            'mobillio'              =>  ['Mobillio'],
                            'nsx'                   =>  ['NSX'],
                            'odyssey'               =>  ['Odyssey'],
                            'prelude'               =>  ['Prelude'],
                            's660'                  =>  ['S660'],
                            'stepwgn'               =>  ['STEPWGN'],
                            'stepwgn_spada'         =>  ['STEPWGN SPADA'],
                            'stream'                =>  ['Stream'],
                            'today'                 =>  ['Today']
                        ],
                'toyota' => [
                            '4runner'               =>  ['4Runner'],
                            '86gt'                  =>  ['86GT'],
                            'alphard'               =>  ['Alphard'],
                            'aristo'                =>  ['Aristo'],
                            'avanza'                =>  ['Avanza'],
                            'avensis'               =>  ['Avensis'],
                            'aygo'                  =>  ['Aygo'],
                            'db'                    =>  ['bB'],
                            'c_hr'                  =>  ['C-HR'],
                            'camry'                 =>  ['Camry'],
                            'carina'                =>  ['Carina'],
                            'celica'                =>  ['Celica'],
                            'celsior'               =>  ['Celsior'],
                            'century'               =>  ['Century'],
                            'commuter'              =>  ['Commuter'],
                            'corolla'               =>  ['Corolla'],
                            'corolla_altis'         =>  ['Corolla Altis'],
                            'corona'                =>  ['Corona'],
                            'cressida'              =>  ['Cressida'],
                            'crown'                 =>  ['Crown'],
                            'dyna'                  =>  ['Dyna'],
                            'esquire'               =>  ['Esquire'],
                            'estima'                =>  ['Estima'],
                            'fj_cruiser'            =>  ['FJ CRUISER'],
                            'fortuner'              =>  ['Fortuner'],
                            'grand_hiace'           =>  ['Grand Hiace'],
                            'grand_wagon'           =>  ['Grabd Wagon'],
                            'granvia'               =>  ['Granvia'],
                            'harrier'               =>  ['Harrier'],
                            'hiace'                 =>  ['Hiace'],
                            'hilux_hero'            =>  ['Hilux Hero'],
                            'hilux_mighty_x'        =>  ['Hilux Mighty-X'],
                            'hilux_revo'            =>  ['Hilux Revo'],
                            'hilux_surf'            =>  ['Hilux Surf'],
                            'hilux_tiger'           =>  ['Hilux Tiger'],
                            'hilux_vigo'            =>  ['Hilux Vigo'],
                            'innova'                =>  ['Innova'],
                            'iq'                    =>  ['IQ'],
                            'kluger'                =>  ['Kluger'],
                            'land_cruiser'          =>  ['Land Cruiser'],
                            'landcruiser_prado'     =>  ['Landcruiser Prado'],
                            'ln'                    =>  ['LN'],
                            'lucida'                =>  ['Lucida'],
                            'mark_2'                =>  ['Mark 2'],
                            'mark_x'                =>  ['Mark X'],
                            'mega_cruiser'          =>  ['Mega Cruiser'],
                            'mr_s'                  =>  ['MR-S'],
                            'mr_2'                  =>  ['MR2'],
                            'noah'                  =>  ['Noah'],
                            'opa'                   =>  ['Opa'],
                            'porte'                 =>  ['Porte'],
                            'prius'                 =>  ['Prius'],
                            'rav4'                  =>  ['Rav4'],
                            'sienta'                =>  ['Sienta'],
                            'soarer'                =>  ['Soarer'],
                            'soluna'                =>  ['Soluna'],
                            'sport_cruiser'         =>  ['Sport Cruiser'],
                            'sport_rider'           =>  ['Sport Rider'],
                            'starlet'               =>  ['Starlet'],
                            'super_custom'          =>  ['Super Custom'],
                            'super_wagon'           =>  ['Super Wagon'],
                            'supra'                 =>  ['Supra'],
                            'vellfire'              =>  ['Vellfire'],
                            'ventury'               =>  ['Ventury'],
                            'vios'                  =>  ['Vios'],
                            'vista'                 =>  ['Vista'],
                            'vitz'                  =>  ['Vitz'],
                            'voxy'                  =>  ['Voxy'],
                            'wish'                  =>  ['Wish'],
                            'yaris'                 =>  ['Yaris'],
                            'yaris_ativ'            =>  ['Yaris Ativ']
                        ],

                'isuzu' =>[
                            'adventure'             =>  ['Adventure'],
                            'adventure_master'      =>  ['Adventure Master'],
                            'buddy'                 =>  ['Buddy'],
                            'cameo'                 =>  ['Cameo'],
                            'd_max'                 =>  ['D-Max'],
                            'dragon_eyes'           =>  ['Dragon Eyes'],
                            'dragon_power'          =>  ['Dragon Power'],
                            'elf'                   =>  ['ELF'],
                            'faster'                =>  ['Faster'],
                            'grand_adventure'       =>  ['Grand Adventure'],
                            'hr'                    =>  ['HR'],
                            'mu_7'                  =>  ['MU-7'],
                            'mu_x'                  =>  ['MU-X'],
                            'trf'                   =>  ['TRF'],
                            'trooper'               =>  ['Trooper'],
                            'vega'                  =>  ['Vega'],
                            'vertex'                =>  ['Vertex']
                ],

                'nissan' =>[
                            '200sx'                 =>  ['200SX'],
                            '300zx'                 =>  ['300ZX'],
                            '350z'                  =>  ['350Z'],  
                            '370z'                  =>  ['370Z'],  
                            'almera'                =>  ['Almera'],  
                            'bigm'                  =>  ['BigM'],  
                            'bluebird_atessa'       =>  ['Bluebird Atessa'],  
                            'cedric'                =>  ['Cedric'],
                            'cefiro'                =>  ['Cefiro'],  
                            'cube'                  =>  ['Cube'],  
                            'elgrand'               =>  ['Elgrand'],
                            'figaro'                =>  ['Figaro'],     
                            'frontier'              =>  ['Frontier'],
                            'frontier_nacara'       =>  ['Frontier Navara'],     
                            'gt_r'                  =>  ['GT-R'],     
                            'infiniti'              =>  ['Infiniti'],
                            'juke'                  =>  ['juke'],
                            'leaf'                  =>  ['Leaf'], 
                            'livina'                =>  ['Livina'], 
                            'march'                 =>  ['March'],
                            'murano'                =>  ['Murano'],
                            'note'                  =>  ['Note'],  
                            'np_300_navara'         =>  ['NP 300 Navara'],  
                            'nv'                    =>  ['NV'],
                            'nx'                    =>  ['NX'], 
                            'pao'                   =>  ['Pao'], 
                            'patro'                 =>  ['Patro'], 
                            'presea'                =>  ['Presea'],
                            'president'             =>  ['President'],
                            'primera'               =>  ['Primera'],                    
                            'pulsar'                =>  ['Pulsar'],
                            'sentra'                =>  ['Sentra'],
                            'serena'                =>  ['Serena'],
                            'silv3ia'               =>  ['Silvia'],
                            'skyline'               =>  ['Skyline'],
                            'sunny'                 =>  ['Sunny'],
                            'sylphy'                =>  ['Sylphy'],
                            'teana'                 =>  ['Teana'],
                            'terrano_ii'            =>  ['Terrano II'],
                            'tiida'                 =>  ['Tiida'],
                            'urvan'                 =>  ['Urvan'],
                            'x-trail'               =>  ['X-Trail'],
                            'xciter'                =>  ['Xciter']

                ],          


                'mitsubishi' =>[
                            '300gt'                 =>  ['300GT'],
                            'attrage'               =>  ['Attrage'],
                            'canter'                =>  ['Canter'],
                            'cyblone'               =>  ['Cyclone'],
                            'delica_space_wagon'    =>  ['Delica Space Wagon'],
                            'evolution'             =>  ['Evolution'],
                            'fto'                   =>  ['FTO'],
                            'galant'                =>  ['Galant'],
                            'l300'                  =>  ['L300'],
                            'lancer'                =>  ['Lancer'],
                            'lancer_ex'             =>  ['Lancer EX'],
                            'mirage'                =>  ['Mirage'],
                            'pajero'                =>  ['Pajero'],
                            'pajero_io'             =>  ['Pajero IO'],
                            'pajero_junior'         =>  ['Pajerojunior'],
                            'pajero_sport'          =>  ['Pajero Sport'],
                            'space_runner'          =>  ['Space Runner'],
                            'space_wagon'           =>  ['Space Wagon'],
                            'strada'                =>  ['Strada'],
                            'strada_g_wagon'        =>  ['Strada G-Wagon'],
                            'trition'               =>  ['Triton']

                ],

                'mercedes_benz'                     =>[
                            'a_class'               =>  ['A-Class'],
                            'a170'                  =>  ['A170'],
                            'a180'                  =>  ['A180'],
                            'a180_cdi'              =>  ['A180 CDI'],
                            'a250'                  =>  ['A250'],
                            'a45'                   =>  ['A45'],
                            'b_class'               =>  ['B-Class'],
                            'b180'                  =>  ['B180'],
                            'b180_cdi'              =>  ['B180 CDI'],
                            'b200'                  =>  ['B200'],
                            'c_class'               =>  ['C-Class'],
                            'c180'                  =>  ['C180'],
                            'c180_amg'              =>  ['C180 AMG'],
                            'c180_kompressor'       =>  ['C180 Kompressor'],
                            'c220'                  =>  ['C220'],
                            'c220_cdi'              =>  ['C220 CDI'],
                            'c230'                  =>  ['C230'],
                            'c230_kompressor'       =>  ['C230 Kompressor'],
                            'c240'                  =>  ['C240'],
                            'c250'                  =>  ['C250'],
                            'c250_cdi'              =>  ['C250 CDI'],
                            'c250_cgi'              =>  ['C250 CGI'],
                            'c300'                  =>  ['C300'],
                            'c32_amg'               =>  ['C32 AMG'],
                            'c350'                  =>  ['C350'],
                            'c55_amg'               =>  ['C55 AMG'],
                            'cl_class'              =>  ['CL-Class'],
                            'cl500'                 =>  ['CL500'],
                            'cl500_amg'             =>  ['CL500 AMG'],
                            'cla_class'             =>  ['CLA-Class'],
                            'cla180'                =>  ['CLA180'],
                            'cla200'                =>  ['CLA200'],
                        
                ],

                'mazda'                             =>[
                            '2'                     =>  ['2'],
                            '3'                     =>  ['3'],
                            '6'                     =>  ['6'],
                            '121'                   =>  ['121'],
                            '323'                   =>  ['323'],
                            '626'                   =>  ['626'],
                            'bt_50'                 =>  ['BT-50'],
                            'bt_50_pro'             =>  ['BT-50 PRO'],
                            'cx_3'                  =>  ['CX-3'],
                            'cx_5'                  =>  ['CX-5'],
                            'cx_7'                  =>  ['CX-7'],
                            'famillia'              =>  ['Famillia'],
                            'fighter'               =>  ['Fighter'],
                            'magnum_thunder'        =>  ['Magnum Thunder'],
                            'lantis'                =>  ['Lantis'],
                            'mpv'                   =>  ['MPV'],
                            'mx_5'                  =>  ['MX-5'],
                            'rx_7'                  =>  ['RX-7'],
                            'rx_8'                  =>  ['RX-8'],
                            'tribute'               =>  ['Tribute']

                ],

                'ford'                              =>[
                            'aspire'                =>  ['Aspire'],
                            'courier'               =>  ['Courier'],
                            'ecosport'              =>  ['EcoSport'],
                            'escape'                =>  ['Escape'],
                            'everest'               =>  ['Everest'],
                            'explorer'              =>  ['Explorer'],
                            'festiva'               =>  ['Festiva'],
                            'fiesta'                =>  ['Fiesta'],
                            'focus'                 =>  ['Focus'],
                            'lacer'                 =>  ['Lacer'],
                            'lincoln'               =>  ['Lincoln'],
                            'focus'                 =>  ['Focus'],
                            'nustang'               =>  ['Mustang'],
                            'ranger'                =>  ['Ranger'],
                            'territory'             =>  ['Territory']

                ],

                'chevrolet'                         =>[
                            'allroader'             =>  ['Allroader'],
                            'astro'                 =>  ['Astro'],
                            'aveo'                  =>  ['Aveo'],
                            'c10'                   =>  ['C10'],
                            'camaro'                =>  ['Camaro'],
                            'captiva'               =>  ['Captiva'],
                            'colorado'              =>  ['Colorado'],
                            'corvette'              =>  ['Corvette'],
                            'cruze'                 =>  ['Cruze'],
                            'express'               =>  ['Express'],
                            'optra'                 =>  ['Optra'],
                            'savana'                =>  ['Savana'],
                            'sonic'                 =>  ['Sonic'],
                            'spin'                  =>  ['Spin'],
                            'traiblazer'            =>  ['Trailblazer'],
                            'van'                   =>  ['Van'],
                            'zafira'                =>  ['Zafira']
                ],
            ], //ปิดแบรนด์

            'nickname'  =>[
                'honda'  => [
                    'accord'      =>[
                              'year_3_7'            => ['(ปี 03-07)'],
                              'year_7_13'           => ['(ปี 07-13)'],
                              'year_13_17'          => ['(ปี 13-17)'],
                              'accord_snake'        => ['งูเห่า (ปี 97-02)'],
                              'accord_eye'          => ['ตาเพชร (ปี 89-93)'],
                              'accord_abcd'         => ['ท้ายก้อน-ท้ายสองก้อน (ปี 93-97)']
                    ],
                    'br_v'        =>[
                              'br_v_y16_20'         => ['(ปี 16-20)']
                    ],
                    'brio'        =>[
                              'brio_y11_16'         => ['(ปี 11-16)']
                    ],
                    'city'        =>[
                              'city_y02_05'         => ['(ปี 02-05)'],
                              'city_y08_14'         => ['(ปี 08-14)'],
                              'city_y14_18'         => ['(ปี 14-18)'],
                              'city_y95_99'         => ['(ปี 95-99)'],
                              'type-z_y99_02'       => ['TYPE-Z (ปี 99-02)'],
                              'zx_y05_07'           => ['ZX (ปี 05-07)']     
                    ],
                    'civic'        =>[
                              '3dr_4dr'             => ['3Dr-4Dr เตารีด (ปี 92-95)'],
                              'coupre'              => ['COUPE (ปี 96-00)'],
                              'dimension_y00_04'    => ['Dimension (ปี 00-04)'],
                              'dimension_y04_06'    => ['Dimension (ปี 04-06)'],
                              'fb'                  => ['FB (ปี 12-16)'],
                              'fc'                  => ['FC (ปี 16-20)'],
                              'fc_fc'               => ['FC (ปี 16-20) FC (ปี 16-20)'],
                              'fd'                  => ['FD (ปี 05-12)'],
                              'fk2'                 => ['FK2 (ปี 16-20)'],
                              'big_eye'             => ['ตาโต (ปี 96-00)']
                              
                    ],
                    'cr_v'         =>[
                              'cr_V_y02_05'         => ['(ปี 02-06)'],
                              'cr_v_y06_12'         => ['(ปี 06-12)'],
                              'cr_v_y12_16'         => ['(ปี 12-16)'],
                              'cr_v_y17_21'         => ['(ปี 17-21)'],
                              'cr_v_y95_02'         => ['(ปี 95-02)']
                                
                    ],
                    'cr_x_del_sol' =>[
                              'cr_x_del_sol_y02_98' => ['(ปี 92-98)']                 
                    ],
                    'cr_z'         =>[
                              'cr_z_y10_16'         => ['(ปี 10-16)']                 
                    ],
                    'crossroad'    =>[
                              'crossroad_y08_10'    => ['(ปี 08-10)']                 
                    ],
                    'elysion'      =>[
                              'elysion_y04_13'      => ['(ปี 04-13)']                 
                    ],
                    'fit'          =>[
                              'fit_y08_14'          => ['(ปี 08-14)']
                    ],  
                    'freed'        =>[
                              'freed_y08_16'        => ['(ปี 08-16)']
                    ],
                    'hr_v'         =>[
                              'hr_v_y14_18'         => ['(ปี 14-18)']
                    ], 
                    'inspire'      =>[
                              'inspire_y95_98'      => ['(ปี 95-98)']
                    ],
                    'integra'      =>[
                              'dc2_y94_01'          => ['DC2 (ปี 94-01)'],
                              'dc5_y01_06'          => ['DC5 (ปี 01-06)']
                    ],
                    'jazz'         =>[
                              'jazz_y03_07'         => ['(ปี 03-07)'],
                              'jazz_y08_14'         => ['(ปี 08-14)'],
                              'jazz_y14_18'         => ['(ปี 14-18)']
                    ], 
                    'legend'       =>[
                              'legend_y04_12'       => ['(ปี 04-12)'],
                              'legend_y91_98'       => ['(ปี 91-98)'],
                              'legend_y95_04'       => ['(ปี 95-04)']
                    ],
                    'mobillio'     =>[
                              'mobillio_y14_17'     => ['(ปี 14-17)']                            
                    ],
                    'nsx'          =>[
                              'nsx_y16_20'          => ['(ปี 16-20)']                            
                    ],
                    'odyssey'      =>[
                              'odyssey_y03_08'      => ['(ปี 03-08)'],
                              'odyssey_y08_13'      => ['(ปี 08-13)'],  
                              'odyssey_y13_16'      => ['(ปี 13-16)'],  
                              'odyssey_y95_99'      => ['(ปี 95-99)'],  
                              'odyssey_y99_03'      => ['(ปี 99-03)'],                              
                    ],
                    'prelude'      =>[
                              'prelude_y91_98'      => ['(ปี 91-98)']                          
                    ],
                    's660'         =>[
                              's660_y15_18'         => ['(ปี 15-18)']                          
                    ], 
                    'stepwgn'      =>[
                              'stepwgn_y05_09'      => ['(ปี 05-09)']                          
                    ],
                    'stepwgn_spada'=>[
                              'stepwgn_spada_y09_16'=> ['(ปี 09-16)']                          
                    ],  
                    'stream'       =>[
                              'stream_y00_06'       => ['(ปี 00-06)']                          
                    ], 
                    'today'        =>[
                              'today_y85_98'        => ['(ปี 85-98)']                          
                    ], 
                
                ], 
                'toyota' => [
                    '4runner'      =>[
                              '4runner_y90_95'      => ['(ปี 90-95)']
                    ],
                    '86gt'         =>[
                              '86gt_y90_95'         => ['(ปี 90-95)']
                    ],
                    'alphard'      =>[
                              'alphard_y02_07'      => ['(ปี 02-07)'],
                              'alphard_y08_14'      => ['(ปี 08-14)'],
                              'alphard_y15_18'      => ['(ปี 15-18)']
                    ],
                    'aristo'       =>[
                              'aristo_y97_04'       => ['(ปี 97-04)']
                    ],
                    'avanza'       =>[
                              'aristo_y04_11'       => ['(ปี 04-11)'],
                              'aristo_y12_16'       => ['(ปี 12-16)']
                    ],
                    'avensis'      =>[
                              'avensis_y10_16'      => ['(ปี 10-16)']
                    ],  
                    'aygo'         =>[
                              'avensis_y15_20'      => ['(ปี 15-20)']
                    ], 
                    'bb'           =>[
                              'bb_y06_12'           => ['(ปี 06-12)']
                    ],  
                    'c_hr'         =>[
                              'c_hr_y17_21'         => ['(ปี 17-21)']
                    ],
                    'camry'        =>[
                              'year_2_6'            => ['(ปี 02-06)'],
                              'year_6_12'           => ['(ปี 06-12)'],
                              'year_12_16'          => ['(ปี 12-16)'],
                              'year_93_97'          => ['โฉมแรก (ปี 93-97)'],
                              'light_long'          => ['โฉมไฟท้ายยาว (ปี 98-00)'],
                              'light_long_sub'      => ['โฉมไฟท้ายย้อย (ปี 01-02)'],
                    ],
                    'carina'       =>[
                              'carina_y96_01'       => ['(ปี 96-01)']
                    ],
                    'celica'       =>[
                              'celica_y00_05'       => ['(ปี 00-05)'],
                              '4eye'                => ['4EYE ตากลม (ปี 94-99)'],
                              'pop_up'              => ['POP-UP (ปี 90-94)']
                    ],   
                    'celsior'      =>[
                              'celsior_y00_y06'     => ['(ปี 00-06)']
                    ],
                    'century'      =>[
                              'century_y97_15'      => ['(ปี 97-15)']
                    ],   
                    'corolla'      =>[
                              'corolla_y66_70'      => ['(ปี 66-70)'],
                              'hi_torque'           => ['HI-TORQUE (ปี 98-01)'],
                              'ke'                  => ['KE (ปี 74-83)'],
                              'ass_duck'            => ['ตูดเป็ด (ปี 95-99)'],
                              'three_circle'        => ['สามห่วง (ปี 91-96)'],
                              'Doremon'             => ['โดเรม่อน (ปี 88-92) (2)']
                    ],
                    'corolla_altis'=>[
                              'y14_18'              => ['(ปี 14-18)'],
                              'altis_y80_13'        => ['ALTIS (ปี 08-13)'],
                              'altis_y14_18'        => ['ALTIS (ปี 14-18)'],
                              'face_pig'            => ['ALTIS หน้าหมู (ปี 01-07)']
                    ],  
                    'corona'       =>[
                              'exsior'              => ['Exsior (ปี 96-99)'],
                              'tuyen'               => ['ตู้เย็น (ปี 83-89)'],
                              'end_separate'        => ['ท้ายแยก (ปี 93-97)'],
                              'end_high'            => ['ท้ายโด่ง (ปี 92-94)'],
                              'facegiant_facesmile' => ['หน้ายักษ์-หน้ายิ้ม (ปี 88-94)'],
                              'facesharp'           => ['หน้าแหลม (ปี 82-89)']
                    ],
                    'cressida'     =>[
                              'cressida_y88_92'     => ['(ปี 88-92)']
                    ],
                    'crown'        =>[
                              'crown_y00_05'        => ['(ปี 00-05)'],
                              'crown_y13_17'        => ['(ปี 13-17)'],
                              'lower_y2000'         => ['ต่ำกว่าปี 2000']
                    ],
                    'dyna'         =>[
                              'dyna_y95_02'         => ['(ปี 95-02)']
                    ],
                    'esquire'      =>[
                              'esquire_y14_17'      => ['(ปี 14-17)']
                    ],
                    'estima'       =>[
                              'estima_y00_05'       => ['(ปี 00-05)'],
                              'estima_y06_14'       => ['(ปี 06-14)'],
                              'estima_y16_19'       => ['(ปี 16-19)']
                    ],
                    'fj_cruiser'   =>[
                              'fj_cruiser_y07_15'   => ['(ปี 07-15)']
                    ],
                    'fortuner'     =>[
                              'fortuner_y04_08'     => ['(ปี 04-08)'],
                              'fortuner_y08_11'     => ['(ปี 08-11)'],
                              'fortuner_y12_15'     => ['(ปี 12-15)'],
                              'fortuner_y15_18'     => ['(ปี 15-18)'],
                              'fortuner_y1518'      => ['(ปี 15-18) (ปี 15-18)']
                    ],
                    'grand_hiace'  =>[
                              'grand_hiace_y92_02'  => ['(ปี 92-02)']
                    ],
                    'granvia'      =>[
                              'granvia_y95_02'      => ['(ปี 95-02)']
                    ],
                    'harrier'      =>[
                              'harrier_y03_13'      => ['(ปี 03-13)'],
                              'harrier_y14_17'      => ['(ปี 14-17)'],
                              'harrier_y97_03'      => ['(ปี 97-03)']
                    ],
                    'hiace'        =>[
                              'commuter_y05_16'     => ['COMMUTER (ปี 05-16)'],
                              'short_y05_16'        => ['ตัวเตี้ย (ปี 05-16'],
                              'rocket_head'         => ['หัวจรวด (ปี 92-04)']
                    ],
                    'hilux_hero'   =>[
                              'single'              => ['Single']
                    ],
                    'hilux_mighty_x'=>[
                              'double_cab'          => ['DOUBLE CAB'],
                              'extracab'            => ['EXTRACAB'],
                              'single'              => ['SINGLE']
                    ],
                    'hilux_revo'   =>[
                              'revo_double_cab'     => ['DOUBLE CAB'],
                              'revo_single'         => ['SINGLE'],
                              'revo_smartcab'       => ['SMARTCAB'],
                              'revo_smartcab_smartcab' => ['SMARTCAB SMARTCAB']
                    ],
                    'hilux_surf'   =>[
                              'surf_y88_97'         => ['(ปี 88-97)']
                    ],  
                    'hilux_tiger'  =>[
                              'tiger_doublecab'     => ['DOUBLE CAB'],
                              'tiger_extracab'      => ['EXTRACAB'],
                              'tiger_single'        => ['SINGLE'],
                              'sport_cruiser'       => ['SPORT CRUISER']
                    ],
                    'hilux_vigo'   =>[
                              'champ_double_cab'    => ['CHAMP DOUBLE CAB (ปี 11-15)'],
                              'champ_extracab'      => ['CHAMP EXTRACAB (ปี 11-15)'],
                              'champ_single'        => ['CHAMP SINGLE (ปี 11-15)'],
                              'champ_smartcab'      => ['CHAMP SMARTCAB (ปี 11-15)'],
                              'vigo_bdcab_y05_08'   => ['DOUBLE CAB (ปี 04-08)'],
                              'vigo_bdcab_y08_11'   => ['DOUBLE CAB (ปี 08-11)'],
                              'vigo_extracab_y04_08'=> ['EXTRACAB (ปี 04-08)'],
                              'vigo_extracab_y08_11'=> ['EXTRACAB (ปี 08-11)'],
                              'vigo_single_y04_08'  => ['SINGLE (ปี 04-08)'],
                              'vigo_single_y08_11'  => ['SINGLE (ปี 08-11)'],
                              'vigo_smartcab_y08_11'=> ['SMARTCAB (ปี 08-11)'],
                              'vigo_sportvan_y08_11'=> ['Sport Van (ปี 08-11)']
                    ],
                     'innova'     =>[
                              'innova_y04_11'       => ['(ปี 04-11)'],
                              'innova_y11_15'       => ['(ปี 11-15)'],
                              'innova_y16_20'       => ['(ปี 16-20)']
                    ],
                    'iq'          =>[
                              'iq_y08_15'           => ['(ปี 08-15)']
                    ],
                    'kluger'      =>[
                              'kluger_y01_07'       => ['(ปี 01-07)'] 
                    ],
                    'land_cruiser'=>[
                              'land_cruiser_43'     => ['43'],
                              'land_cruiser_45'     => ['45'],
                              'land_cruiser_70'     => ['70'],
                              'land_cruiser_80'     => ['80'],
                              'land_cruiser_100'    => ['100'],
                              'land_cruiser_200'    => ['200'] 
                    ],
                    'landcruiser_prado'=>[
                              'land_cruiser_prado_90'     => ['90'],
                              'land_cruiser_prado_120'     => ['120'],
                              'land_cruiser_prado_150'     => ['150'],
    
                    ],
                    'ln'          =>[
                              'all_nickname'        => ['รวมทุกโฉม']
                    ],
                    'lucida'      =>[
                              'lucida_y90_99'       => ['(ปี 90-99)']
                    ],
                    'mark_2'      =>[
                              'mark_2_y92-96'       => ['(ปี 92-96)']
                    ], 
                    'mark_x'      =>[
                              'mark_x_y04_10'       => ['(ปี 04-10)']
                    ], 
                    'mega_cruiser'=>[
                              'mega_cruiser_y95_02' => ['(ปี 95-02)']
                    ],  
                    'mr_s'        =>[
                              'mr_s_y00_07'         => ['(ปี 00-07)']
                    ], 
                    'mr_2'        =>[
                              'mr_2_y89_99'         => ['(ปี 89-99)']
                    ], 
                    'noah'        =>[
                              'noah_y01_13'         => ['(ปี 01-13)'], 
                              'noah_y14_17'         => ['(ปี 14-17)']  
                    ], 
                    'opa'         =>[
                              'noah_y00_05'         => ['(ปี 00-05)']
                    ], 
                    'porte'       =>[
                              'porte_y05_15'        => ['(ปี 05-15)']
                    ],
                    'prius'       =>[
                              'prius_y09_16'        => ['(ปี 09-16)']
                    ],
                    'rav4'        =>[
                              'rav4_y02_12'         => ['(ปี 02-12)'],
                              'rav4_y95_02'         => ['(ปี 95-02)']
                    ], 
                    'sienta'      =>[
                              'sienta_y16_20'       => ['(ปี 16-20)']
                    ], 
                    'soarer'      =>[
                              'soarer_y01_05'       => ['(ปี 01-05)'],
                    ], 
                    'soluna'      =>[
                              'al50_first_nickname' => ['AL50 โฉมแรก (ปี 97-00)'], 
                              'al50_droplet_y00_03' => ['AL50 ไฟท้ายหยดน้ำ (ปี 00-03)'],   
                              'droplet_al50_y00_03' => ['ไฟท้ายหยดน้ำAL50 (ปี 00-03)']
                    ],
                    'sport_cruiser'=>[
                              'sport_cruiser_y00_04'=> ['(ปี 00-04)']    
                    ],
                    'sport_rider' =>[
                              'sport_rider_y98_02'  => ['(ปี 98-02)'],
                              'd4d_y02_04'          => ['D4D (ปี 02-04)']     
                    ], 
                    'starlet'     =>[
                              'starlet_ep71'        => ['EP71 (ปี 84-89)'],
                              'starlet_ep82'        => ['EP82 (ปี 89-97)']     
                    ],
                    'super_custom'=>[
                              'super_custom_y96_04' => ['(ปี 96-04)']
                    ],
                    'super_wagon' =>[
                              'super_wagon_y96_04'  => ['(ปี 96-04)']
                    ],
                    'supra'       =>[
                              'supra_y02_02'        => ['(ปี 92-02)']
                    ], 
                    'vellfire'    =>[
                              'vellfire_y08_14'     => ['(ปี 08-14)'],
                              'vellfire_y15_18'     => ['(ปี 15-18)'] 
                    ],
                    'ventury'     =>[
                              'ventury_y05_16'      => ['(ปี 05-16)']     
                    ],
                    'vios'        =>[
                              'vios_y02_07'         => ['(ปี 02-07)'],
                              'vios_y07_13'         => ['(ปี 07-13)'], 
                              'vios_y13_17'         => ['(ปี 13-17)'],  
                              // 'vios_y1317'          => ['(ปี 13-17) (ปี 13-17)']   
                    ],
                    'vista'       =>[
                              'vista_y90_91'        => ['(ปี 90-94)']      
                    ],
                    'vitz'        =>[
                              'vitz_y00_05'         => ['(ปี 00-05)']          
                    ],
                    'voxy'        =>[
                              'voxy_y14_17'         => ['(ปี 14-17)']          
                    ],
                    'wish'        =>[
                              'wish_y03_10'         => ['(ปี 03-10)']         
                    ],
                    'yaris'       =>[
                              'yaris_y06_13'        => ['(ปี 06-13)'],
                              'yaris_y13_17'        => ['(ปี 13-17)'] 
                    ],
                    'yaris_ativ'  =>[
                              'yaris_ativ_y17_21'   => ['(ปี 17-21)']         
                    ],
                
                ],
                'isuzu'  => [
                    'adventure'  =>[
                              'adventure_y96_00'    => ['(ปี 96-00)']         
                    ], 
                    'adventure_master'=>[
                              'amaster_y02_06'      => ['(ปี 02-06)']         
                    ],
                    'cameo'      =>[
                              'cameo_y91_97'        => ['(ปี 91-97)']         
                    ],
                    'd_max'      =>[
                              'cab_4_y02_06'        => ['CAB-4 (ปี 02-06)'],
                              'cab_4_y07_11'        => ['CAB-4 (ปี 07-11)'],
                              'cab_4_y11_17'        => ['CAB-4 (ปี 11-17)'],
                              'spacecab_y02_06'     => ['SPACE CAB (ปี 02-06)'],
                              'spacecab_y07_11'     => ['SPACE CAB (ปี 07-11)'],
                              'spacecab_y11_17'     => ['SPACE CAB (ปี 11-17)'], 
                              'spacecab_y1117'      => ['SPACE CAB (ปี 11-17) SPACE CAB (ปี 11-17)'],  
                              'spark_y02_96'        => ['SPARK (ปี 02-06)'],  
                              'spark_y07_11'        => ['SPARK (ปี 07-11)'],  
                              'spark_y11_17'        => ['SPARK (ปี 11-17)']            
                    ], 
                    'dragon_eyes'=>[
                              'dragon_eyes_spacecab'=> ['SPACE CAB'],
                              'spacecab_y96_99'     => ['SPACE CAB (ปี 96-99)'],
                              'spark_y96_99'        => ['SPARK (ปี 96-99)']         
                    ], 
                    'dragon_power'=>[
                              'dragonpower_cab4'    => ['CAB-4'],
                              'cab4_y00_02'         => ['CAB-4 (ปี 00-02)'],
                              'dragonpower_spacecab'=> ['SPACE CAB'],
                              'spacecab_y00_02'     => ['SPACE CAB (ปี 00-02)'], 
                              'spark_y00_02'        => ['SPARK (ปี 00-02)']         
                    ], 
                    'elf'        =>[
                              'elf_all'             => ['รวมทุกโฉม']         
                    ],
                    'grand_adventure'=>[
                              'g_adventure_y_96_02' => ['(ปี 96-02)']         
                    ],
                    'hr'        =>[
                              'hr_all'             => ['รวมทุกโฉม'] 
                    ],
                    'mu_7'       =>[
                              'mu_7_y04_06'         => ['(ปี 04-06)'], 
                              'mu_7_y07_13'         => ['(ปี 07-13)']        
                    ],
                    'mu_x'       =>[
                              'mu_x_y13_17'         => ['(ปี 13-17)'], 
                              'mu_x_1317'           => ['(ปี 13-17) (ปี 13-17)']        
                    ],
                    'trf'        =>[
                              'dragon_gold'         => ['มังกรทอง']    
                    ],
                    'trooper'    =>[
                              'trooper_y91_03'      => ['(ปี 91-03)']    
                    ],
                    'vega'       =>[
                              'vega_y97_03'         => ['(ปี 97-03)']    
                    ],

                ],
                'nissan' => [
                    '200sx'      =>[
                              '200sx_y92_95'        => ['(ปี 92-95)']         
                    ],
                    '300zx'      =>[
                              '300zx_y92_98'        => ['(ปี 92-98)']         
                    ],
                    '350z'       =>[
                              '350z_y03_09'         => ['(ปี 03-09)']         
                    ],
                    '370z'       =>[
                              '300zx_y09_15'        => ['(ปี 09-15)']         
                    ], 
                    'almera'     =>[
                              'almera_y11_16'       => ['(ปี 11-16)']         
                    ],
                    'bigm'       =>[
                              'bigm_4dr'            => ['4Dr'],
                              'bigm_kingcab'        => ['KING CAB'], 
                              'bigm_single'         => ['SINGLE']          
                    ],
                    'bluebird_atessa'=>[
                              'bluebird_a_y93_97'   => ['(ปี 93-97)']         
                    ],
                    'cedric'     =>[
                              'cedric_y92_95'       => ['(ปี 92-95)']
                    ],
                    'cefiro'     =>[
                              'A31'                 => ['A31 (ปี 90-95)'],
                              'A32'                 => ['A32 (ปี 96-02)'],
                              'A33'                 => ['A33 (ปี 01-04)']
                    ],
                    'cube'       =>[
                              'cube_y02_08'         => ['(ปี 02-08)'],
                              'cube_y02_15'         => ['(ปี 08-15)']
                    ],
                    'elgrand'    =>[
                              'elgrand_y02_10'      => ['(ปี 02-10)'],
                              'elgrand_y10_15'      => ['(ปี 10-15)'],
                              'elgrand_97_02'       => ['(ปี 97-02)'] 
                    ],  
                    'figaro'     =>[
                              'figaro_y91_95'       => ['(ปี 91-95)']
                    ],
                    'frontier'   =>[
                              'frontier_4dr'        => ['4DR'],
                              'frontier_kingcab'    => ['KING CAB'],
                              'frontier_single'     => ['SINGLE']
                    ],
                    'frontier_nacara'=>[
                              'f_nacara_4dr'        => ['4DR'],
                              'f_nacara_kingcab'    => ['KING CAB'],
                              'f_nacara_single'     => ['SINGLE']
                    ],
                     'gt_r'      =>[
                              'gt_r_y08_15'         => ['(ปี 08-15)']
                    ],  
                    'infiniti_r' =>[
                              'infinit_r_y92_97'    => ['(ปี 92-97)']
                    ],
                    'juke'       =>[
                              'juke_y10_16'         => ['(ปี 10-16)']
                    ],
                    'leaf'       =>[
                              'leaf_y12_16'         => ['(ปี 12-16)']
                    ],
                    'livina'     =>[
                              'livina_y14_17'       => ['(ปี 14-17)']
                    ], 
                    'march'      =>[
                              'march_y10_16'        => ['(ปี 10-16)']
                    ],  
                    'murano'     =>[
                              'murano_y04_10'       => ['(ปี 04-10)']
                    ],
                    'note'       =>[
                              'note_y17_21'         => ['(ปี 17-21)']
                    ],
                    'np_300_navara'=>[
                              'np300_dbcab'         => ['DOUBLE CAB'],
                              'np_300_kcab'         => ['KING CAB'],
                              'np_300_single'       => ['SINGLE']
                    ],
                    'nv'         =>[
                              'nv_queencab'         => ['QUEENCAB'],
                              'nv_single'           => ['SINGLE'],
                              'nv_wingroad'         => ['WINGROAD ']
                    ],
                    'nx'         =>[
                              'nx_y92_95'           => ['(ปี 92-95)']
                    ], 
                    'pao'        =>[
                              'pao_y89_92'          => ['(ปี 89-92)']
                    ], 
                    'presea'     =>[
                              'presea_y92_97'       => ['(ปี 92-97)']
                    ], 
                    'president'  =>[
                              'president_y93_95'    => ['(ปี 93-95)']
                    ], 
                    'primera'    =>[
                              'primera_y97_99'      => ['(ปี 97-99)']
                    ], 
                    'pulsar'     =>[
                              'pulsar_y12_16'       => ['(ปี 12-16)']
                    ],       
                    'sentra'     =>[
                              'sentra_y92_96'       => ['(ปี 92-96)']
                    ], 
                    'serena'     =>[
                              'serena_y00_05'       => ['(ปี 00-05)']
                    ],   
                    'silvia'     =>[
                              'silvia_y00_05'       => ['(ปี 00-05)']
                    ],   
                    'skyline'    =>[
                              'skyline_r33'         => ['R33 (ปี 94-99)'],
                              'skyline_v35'         => ['V35 (ปี 02-07)'],
                              'skyline_v36'         => ['V36 (ปี 07-15)']
                    ], 
                    'sunny'      =>[
                              'b14_15'              => ['B14-15 (ปี 94-00)'],
                              'sunny_neo'           => ['NEO (ปี 01-04)'],
                              'sunny_neo_y04_06'    => ['NEO ท้ายแตงโม (ปี 04-06)']
                    ], 
                    'sylphy'     =>[
                              'sylphy_y12_16'       => ['(ปี 12-16)']
                    ],   
                    'teana'      =>[
                              'teana_y04_08'        => ['(ปี 04-08)'],
                              'teana_y09_13'        => ['(ปี 09-13)'],
                              'teana_13_16'         => ['(ปี 13-16)']
                    ],
                    'terrano_ii' =>[
                              'terrano_ii_y95_00'   => ['(ปี 95-00)']
                    ],
                    'tiida'      =>[
                              'tiida_y06_12'        => ['(ปี 06-12)']
                    ],  
                    'urvan'      =>[
                              'urvan_y01_12'        => ['(ปี 01-12)'],
                              'urvan_y13_17'        => ['(ปี 13-17)'],
                              'urvan_y90_01'        => ['(ปี 90-01)']
                    ],   
                    'x-trail'    =>[
                              'x-trail_y02_08'      => ['(ปี 02-08)'],
                              'x-trail_y08_13'      => ['(ปี 08-13)'],
                              'x-trail_y14_17'      => ['(ปี 14-17)'],
                              'x-trail_15_19'       => ['(ปี 15-19)']
                    ],
                    'xciter'      =>[
                              'xciter_y01_06'       => ['(ปี 01-06)']
                    ],

                ],

                'mitsubishi' => [
                    '300gt'      =>[
                              '300gt_y92_97'         => ['(ปี 92-97)']
                    ],   
                    'attrage'    =>[
                              'attrage_y13_16'       => ['(ปี 13-16)']
                    ],
                    'canter'     =>[
                              'all_canter'           => ['รวมทุกโฉม']
                    ],
                    'cyblone'    =>[
                              'aero_body_y09_05'     => ['AERO BODY (ปี 89-95)'],
                              'cyblone_single'       => ['SINGLE (ปี 89-95)']
                    ],   
                    'delica_space_wagon'=>[
                              'aero_body_y15_18'     => ['(ปี 15-18)']
                    ],
                    'evolution'  =>[
                              'iv_face_benz'         => ['IV (โฉมท้ายเบนซ์)'],
                              'ix_lancer'            => ['IX (โฉมแลนเซอร์)'],
                              'v_end_benz'           => ['V (โฉมท้ายเบนซ์)'],
                              'vi_end_benz'          => ['VI (โฉมท้ายเบนซ์)'],
                              'vii_Cedia'            => ['VII (โฉมซีเดีย)'],
                              'viii_lancer'          => ['VIII (โฉมแลนเซอร์)'],
                              'x_lancer_ex'          => ['X (โฉมแลนเซอร์ EX)']
                    ],  
                    'galant'     =>[
                              'galant_y76_80'        => ['(ปี 76-80)'],
                              'galant_y80_87'        => ['(ปี 80-87)'],
                              'galant_y92_99'        => ['(ปี 92-99)']
                    ], 
                    'l300'       =>[
                              'l300_y92_99'          => ['(ปี 92-99)']
                    ],
                    'lancer'     =>[
                              'lancer_y04_12'        => ['(ปี 04-12)'],
                              'cedia_y01_04'         => ['CEDIA (ปี 01-04)'],
                              'champ_i6_y84_85'      => ['CHAMP I-II-III (ปี 84-95)'],
                              'ecar_y92_96'          => ['E-CAR (ปี 92-96)'],
                              'f_style_endbenz'      => ['F Style ท้ายเบนซ์ (ปี 96-02)']
                    ],   
                    'lancer_ex'  =>[
                              'lancer_ex_y09_15'     => ['(ปี 09-15)']
                    ],
                    'mirage'     =>[
                              'mirage_y12_16'        => ['(ปี 12-16)']
                    ], 
                    'pajero'     =>[
                              'pajero_y00_06'        => ['(ปี 00-06)'],
                              'pajero_y08_15'        => ['(ปี 08-15)'],
                              'pajero_y92_02'        => ['(ปี 92-02)']
                    ],
                    'pajero_io'  =>[
                              'pajero_io_y98_07'     => ['(ปี 98-07)']
                    ],
                    'pajero_junior'=>[
                              'pajero_junior_y95_98' => ['(ปี 95-98)']
                    ],
                    'pajero_sport'=>[
                              'pajero_sport_y08_15'  => ['(ปี 08-15)'],
                              'pajero_sport_y15_18'  => ['(ปี 15-18)']
                    ],
                    'space_runner'=>[
                              'space_runner_y92_00'  => ['(ปี 92-00)']
                    ], 
                    'space_wagon'=>[
                              'space_wagon_y04_12'   => ['(ปี 04-12)']
                    ],   
                    'strada'     =>[
                              'strada_grandis4dr'    => ['GRANDIS 4DR'],
                              'strada_megacab'       => ['MEGA CAB'],
                              'strada_sngle'         => ['SINGLE']
                    ],
                    'strada_g_wagon'=>[
                              'strada_g_wagon_y01_06'=> ['(ปี 01-06)']
                    ], 

                    'triton'=>[
                              'double_cab_y05_15'   => ['(ปี 05-15)'],
                              'double_cab_y14_19'   => ['(ปี 14-19)'],
                              'mega_cab_y05_15'     => ['(ปี 05-15)'],
                              'mega_cab_y14_19'     => ['(ปี 14-19)'],
                              'megacab_y05_15'      => ['(ปี 05-15)'],
                              'megacab_y14_19'      => ['(ปี 14-19)'],
                              'single_y05_15'       => ['(ปี 05-15)'],
                              'single_y14_19'       => ['(ปี 14-19)']
                    ], 

                ],


                'mazda' => [
                    '2'      =>[
                              '2_y09_14'            => ['(ปี 09-14)'],
                              '2_y15_18'            => ['(ปี 15-18)']
                    ],
                    '3'      =>[
                              '3_y05_10'            => ['(ปี 05-10)'],
                              '3_y11_14'            => ['(ปี 11-14)'],
                              '3_y14_17'            => ['(ปี 14-17)']
                    ],      
                    '6'    =>[
                              '6_y02_08'            => ['(ปี 02-08)']
                    ],
                    '121'     =>[
                              '121_y92_96'          => ['(ปี 92-96)']
                    ],
                    '323'    =>[
                              '323_y95_98'          => ['(ปี 95-98)'],
                              'protege_y00_06'      => ['Protege (ปี 00-06)']
                    ],   
                    '626'=>[    
                              '626_y97_00'          => ['(ปี 97-00)'],
                              'cronos_y92_97'       => [' Cronos (ปี 92-97)']
                    ],
                    'bt_50'  =>[
                              'double_cab'          => ['DOUBLE CAB'],
                              'free_style_cab'      => ['FREE STYLE CAB'],
                    ],  
                    'bt_50_pro'     =>[
                              'double_cab'          => ['DOUBLE CAB'],
                              'free_style_cab'      => ['FREE STYLE CAB'],
                              'single'              => ['SINGLE'],
                    ], 
                    'cx_3'       =>[
                              'y15_19'              => ['(ปี 15-19)']
                    ],
                    'cx_5'       =>[
                              'y13_16'              => ['(ปี 13-16)'],
                              'y17_20'              => ['(ปี 17-20)']
                    ],
                    'famillia'       =>[
                              'single'              => ['SINGLE'],
                              'super_cab'           => ['SUPER CAB'],
                    ],
                    'fighter'       =>[
                              'double_cab'          => ['DOUBLE CAB'],
                              'free_style_cab'      => ['FREE STYLE CAB'],
                              'super_saloon'        => ['SUPER SALOON'],
                    ],
                    'lantis'       =>[
                              'y94_00'              => ['(ปี 94-00)'],
                    ],
                    'magnum_thunder' =>[
                              'y92_98'              => ['(ปี 92-98)'],
                    ],
                    'mpv'       =>[
                              'y02_06'              => ['(ปี 02-06)'],
                    ],
                    'mx_5'       =>[
                              'y01_05'              => ['(ปี 01-05)'],
                              'y05_15'              => ['(ปี 05-15)'],
                              'y16_20'              => ['(ปี 16-20)'],
                              'y92_98'              => ['(ปี 92-98)'],
                    ],
                    'rx_7'       =>[
                              'y92_00'              => ['(ปี 92-00)'],
                    ],
                    'rx_8'       =>[
                              'y03_08'              => ['(ปี 03-08)'],
                    ],
                    'tribute'       =>[
                              'y02_06'              => ['(ปี 02-06)'],
                    ],

                ],


                'ford' => [
                    'aspire'      =>[
                              'y94_97'              => ['(ปี 94-97)'],
                    ],
                    'courier'      =>[
                              'y77_85'              => ['(ปี 77-85)'],
                    ],      
                    'ecosport'    =>[
                              'y13_16'              => ['(ปี 13-16)']
                    ],
                    'escape'     =>[
                              'y03_07'              => ['(ปี 03-07)'],
                              'y09_12'              => ['(ปี 09-12)']
                    ],
                    'everest'    =>[
                              'y03_06'              => ['(ปี 03-06)'],
                              'y07_13'              => ['(ปี 07-13)'],
                              'y15_18'              => ['(ปี 15-18)'],
                              'y15_18_2'            => ['(ปี 15-18)(ปี 15-18)']
                    ],   
                    'festiva'=>[    
                              'y92_96'              => ['(ปี 92-96)'],
                    ],
                    'fiesta'  =>[
                              'y10_16'              => ['(ปี 10-16)']
                    ],  
                    'focus'     =>[
                              'y04_08'              => ['(ปี 04-08)'],
                              'y09_12'              => ['(ปี 09-12)'],
                              'y12_16'              => ['(ปี 12-16)']
                    ], 
                    'laser'       =>[
                              'y00_05'              => ['(ปี 00-05)'],
                              'y91_94'              => ['(ปี 91-94)']
                    ],
                    'lincoln_continental'=>[
                              'y70_79'              => ['(ปี 70-79)']
                    ],
                    'mustang'       =>[
                              'y05_15'              => ['(ปี 05-15)'],
                              'y15_20'              => ['(ปี 15-20)']
                    ],
                    'ranger'       =>[
                              'double_cab_y03_05'           => ['DOUBLE CAB (ปี 03-05)'],
                              'double_cab_y06_08'           => ['DOUBLE CAB (ปี 06-08)'],
                              'double_cab_y09_12'           => ['DOUBLE CAB (ปี 09-12)'],
                              'double_cab_y12_15'           => ['DOUBLE CAB (ปี 12-15)'],
                              'double_cab_y15_18'           => ['DOUBLE CAB (ปี 15-18)'],
                              'double_cab_y99_02'           => ['DOUBLE CAB (ปี 99-02)'],
                              'open_cab_y03_05'             => ['OPEN CAB (ปี 03-05)'],
                              'open_cab_y06_08'             => ['OPEN CAB (ปี 06-08)'],
                              'open_cab_y09_12'             => ['OPEN CAB (ปี 09-12)'],
                              'open_cab_y12_15'             => ['OPEN CAB (ปี 12-15)'],
                              'open_cab_y15_18'             => ['OPEN CAB (ปี 15-18)'],
                              'single_cab_y03_05'           => ['SINGLE CAB (ปี 03-05)'],
                              'single_cab_y12_15'           => ['SINGLE CAB (ปี 12-15)'],
                              'single_cab_y15_18'           => ['SINGLE CAB (ปี 15-18)'],
                              'single_cab_y99_02'           => ['SINGLE CAB (ปี 99-02)'],
                              'super_cab_y03_05'            => ['SUPER CAB (ปี 03-05)'],
                              'super_cab_y09_12'            => ['SUPER CAB (ปี 99-02)'],
                    ],
                    'territory'       =>[
                              'y12_16'                      => ['(ปี 12-16)'],
                    ],
                    

                ],

            ], //ปิดnickname


            'variant' =>[
                'honda' => [
                    'accord'  =>[
                                'year_3_7'              => [
                                      '2_0_e_i_vtec'    => ['2.0 E i-VTEC'],
                                      '2_4_e_i_vtec'    => ['2.4 E i-VTEC'],
                                      '2_4_el'          => ['2.4 EL'],
                                      '2_4_el_i_vtec'   => ['2.4 EL i-VTEC'],
                                      '2_4_s_i_vtec'    => ['2.4 S i-VTEC'],
                                      '3_0_v6_i_vtec'   => ['3.0 V6 i-VTEC'],
                                ],
                                'year_7_13'             => [
                                      '2_0_e_'          => ['2.0 E'],
                                      '2_0_e_i_vtec'    => ['2.0 E i-VTEC'],
                                      '2_0_el_i_vtec'   => ['2.0 EL i-VTEC'],
                                      '2_0_jp'          => ['2.0 JP'],
                                      '2_4_e_i_vtec'    => ['2.4 E i-VTEC'],
                                      '2_4_el_i_vtec'   => ['2.4 EL i-VTEC'],
                                      '2_4_el_navi'     => ['2.4 EL NAVI'],
                                      '2_4_jp'          => ['2.4 JP'],
                                      '3_5_v6_i_vtec'   => ['3.5 V6 i-VTEC'],

                                ],
                                'year_13_17'            => [
                                        '2_0_e_i_vtec'          => ['2.0 E i-VTEC'],
                                        '2_0_el'                => ['2.0 EL'],
                                        '2_0_el_i_vtec'         => ['2.0 EL i-VTEC'],
                                        '2_0_el_navi'           => ['2.0 EL NAVI'],
                                        '2_0_hybird_i_vtec'     => ['2.0 Hybrid i-VTEC'],
                                        '2_0_hybird_tech_i_vtec'=> ['2.0 Hybrid TECH i-VTEC'],
                                        '2_4_el'                => ['2.4 EL'],
                                        '2_4_el_i_vtec'         => ['2.4 EL i-VTEC'],
                                        '2_4_el_navi'           => ['2.4 EL NAVI'],
                                        '2_4_tech'              => ['2.4 TECH'],
                                ],
                                'accord_snake'          => [
                                        '2_3_exi'               => ['2.3 EXi'],
                                        '2_3_millennium_vti'    => ['2.3 Millennium VTi'],
                                        '2_3_vti'               => ['2.3 VTi'],
                                        '2_3_vti_prestige'      => ['2.3 VTi Prestige'],
                                        '3_0_v6'                => ['3.0 V6'],
                                        '3_0_vti_prestige'      => ['3.0 VTi Prestige'],
                                ],
                                'accord_eye'            => [
                                        '2_0_ex'                => ['2.0 EX'],
                                        '2_0_exi'               => ['2.0 Exi'],
                                        '2_3_lxi'               => ['2.0 LXi'],
                                ],
                                'accord_abcd'            => [
                                        '2_2_exi'        => ['2.2 EXi'],
                                        '2_2_vti_ex'     => ['2.2 VTi EX'],
                                        '2_2_vti_lx'     => ['2.2 VTi LX'],

                                ],

                    ],

                    'br_v' =>[
                                'br_v_y16_20'   => [
                                    '1_5_sv'        => ['1.5 SV'],
                                    '1_5_v'         => ['1.5 V'],
                                ],
                    ],

                    'brio' =>[
                                'brio_y11_16'           => [
                                        '1_2_amaze_s'   => ['1.2 Amaze S'],
                                        '1_2_amaze_sv'  => ['1.2 Amaze SV'],
                                ],
                    ],

                    'city' =>[
                        'city_y02_05'                   => [
                                '1_5_a_i-dsi'           => ['1.5 A i-DSi'],
                                '1_5_e_i-dsi'           => ['1.5 E i-DSi'],
                                '1_5_e_v_vtec'          => ['1.5 E-V VTEC'],
                                '1_5_si_dsi'            => ['1.5 S i-DSi'],
                         ],

                        'city_y08_14'                        => [
                                '1_5_modulo'                 => ['1.5 MODULO'],
                                '1_5_s'                      => ['1.5 S'],
                                '1_5_s_cng'                  => ['1.5 S CNG'],
                                '1_5_s_i_vtec'               => ['1.5 S i-VTEC'],
                                '1_5_sv'                     => ['1.5 SV'],
                                '1_5_sv_i_vtec'              => ['1.5 SV i-VTEC'],
                                '1_5_v'                      => ['1.5 V'],
                                '1_5_v_cng '                 => ['1.5 V CNG '],
                                '1_5_v_i-vtec'               => ['1.5 V i-VTEC'],
                                '1_5_v-i_vtec_modulo'        => ['1.5 V i-VTEC Modulo'],
                                '1_5_v_i-vtec_society'       => ['1.5 V i-VTEC Society'],
                                '1_5_v_wise_edition_i_vtec'  => ['1.5 V Wise Edition i-VTEC'],
                            ],

                        'city_y14_18'                   => [
                                '1_5_s_cng'             => ['1.5 S CNG'],
                                '1_5_s_i_vtec'          => ['1.5 S i-VTEC'],
                                '1_5_sv_i_vtec'         => ['1.5 SV i-VTEC'],
                                '1_5_sv_plus_i_vtec'    => ['1.5 SV+ i-VTEC'],
                                '1_5_v_cng'             => ['1.5 V CNG'],
                                '1_5_v_i_vtec'          => ['1.5 V i-VTEC'],
                                '1_5_v_plus_i_vtec'     => ['1.5 V+ i-VTEC'],
                            ],


                        'city_y95_99'                   => [
                                '1_3_exi'               => ['1.3 EXi'],
                                '1_5_exi'               => ['1.5 EXi'],
                                '1_5_lxi'               => ['1.5 LXi'],
                            ],

                        'type-z_y99_02'                 => [
                                '1_5_exi'               => ['1.5 EXi'],
                                '1_5_type_z_exi'        => ['1.5 Type-Z EXi'],
                                '1_5_type_z_lxi'        => ['1.5 Type-Z LXi'],
                                '1_5_type_z_vti'        => ['1.5 Type-Z VTi'],
                            ],

                        'zx_y05_07'                     => [
                                '1_5_zx_a_i_dsi'        => ['1.5 ZX A i-DSi'],
                                '1_5_zx_ev_vtec'        => ['1.5 ZX EV VTEC'],
                                '1_5_zx_sv_vtec'        => ['1.5 ZX SV VTEC'],
                                '1_5_zx_v_vtec'         => ['1.5 ZX V VTEC'],
                            ],
                    ],


                    'civic' =>[
                        '3dr_4dr'                       => [
                                '1_5_ex'                => ['1.5 EX'],
                                '1_5_lx'                => ['1.5 LX'],
                                '1_6_exi'               => ['1.6 EXi'],
                                '1_6_lxi'               => ['1.6 LXi'],
                                '1_6_vti_ex'            => ['1.6 VTi EX'],
                                '1_6_vti_lx'            => ['1.6 VTi LX'],
                            ],

                        'coupre'                        => [
                                '1_6_vti'               => ['1.6 VTi'],
                                '1_7_vti'               => ['1.7 VTi'],
                                '1_6_vti_exa'           => ['1.6 VTi EXA'],
                            ],

                        'dimension_y00_04'              => [
                                '1_7_exi'               => ['1.7 EXi'],
                                '1_7_vti'               => ['1.7 VTi'],
                                '2_0_excites_i_vtec'    => ['2.0 Excites i-VTEC'],
                            ],

                        'dimension_y04_06'                    => [
                                '1_7_dimension_rx_sports'     => ['1.7 Dimension RX Sports'],
                                '1_7_exclusive_exi'           => ['1.7 Exclusive EXi'],
                                '1_7_exi'                     => ['1.7 EXi'],
                                '1_7_rx_sports_vtec'          => ['1.7 RX Sports VTEC'],
                                '1_7_vti'                     => ['1.7 VTi'],
                                '2_0_excites'                 => ['2.0 Excites'],
                                '2_0_excites_i_vtec'          => ['2.0 Excites i-VTEC'],
                            ],

                        'fb'                            => [
                                '1_5_hybrid'            => ['1.5 Hybrid (18)'],
                                '1_8_e_i_vtec'          => ['1.8 E i-VTEC'],
                                '1_8_e_modulo'          => ['1.8 E Modulo'],
                                '1_8_es_i_vtec'         => ['1.8 ES i-VTEC'],
                                '1_8_modulo'            => ['1.8 Modulo'],
                                '1_8_s_i_vtec'          => ['1.8 S i-VTEC'],
                                '2_0_el_i-vtec'         => ['2.0 EL i-VTEC'],
                            ],

                        'fc'                            => [
                                '1_5_turbo'             => ['1.5 Turbo'],
                                '1_5_turbo_rs'          => ['1.5 Turbo RS'],
                                '1_8_e_i-vtec'          => ['1.8 E i-VTEC'],
                                '1_8_el_i-vtec'         => ['1.8 EL i-VTEC'],
                            ],

                        'fc_fc'                         => [
                                '1_5_turbo'             => ['1.5 Turbo'],
                                '1_5_turbo_rs'          => ['1.5 Turbo RS'],
                                '1_8_e_i-vtec'          => ['1.8 E i-VTEC'],
                                '1_8_el_i-vtec'         => ['1.8 EL i-VTEC'],
                            ],

                        'fd'                                    => [
                                '1_8_e_i_vtec'                  => ['1.8 E i-VTEC'],
                                '1_8_e_sport_pearl'             => ['1.8 E Sport Pearl'],
                                '1_8_e_wise_edition_i-vtec'     => ['1.8 E Wise Edition i-VTEC'],
                                '1_8_s_i_vtec'                  => ['1.8 S i-VTEC'],
                                '2_0_el_i_vtec'                 => ['2.0 EL i-VTEC'],
                            ],

                        'fk'                            => [
                                '1_5_turbo'             => ['1.5 Turbo'],
                            ],

                        'fk2'                           => [
                                '2_0_type_r'            => ['2.0 Type R'],
                            ],

                        'big_eye'                       => [
                                '1_6_exi'               => ['1.6 EXi'],
                                '1_6_lxi'               => ['1.6 LXi'],
                                '1_6_vti'               => ['1.6 VTi'],
                                '1_6_vti_ex'            => ['1.6 VTi EX'],
                                '1_6_vti_exa'           => ['1.6 VTi EXA'],
                                '1_6_vti_lx'            => ['1.6 VTi LX'],
                                '1_8_vti'               => ['1.8 VTi'],
                            ],

                        'back_light'                    => [
                                '1_5_ex'                => ['1.5 EX'],

                            ],
                    ],
                



                    'cr_v' =>[
                        'cr_V_y02_06'                   => [
                                '2_0_e'                 => ['2.0 E'],
                                '2_0_ef'                => ['2.0 EF'],
                                '2_0_s'                 => ['2.0 S'],
                                '2_0_sf'                => ['2.0 SF'],
                                '2_4_el'                => ['2.4 EL'],
                                '2_4_elf'               => ['2.4 ELF'],
                            ],

                        'cr_v_y06_12'                   => [
                                '2_0_e'                 => ['2.0 E'],
                                '2_0_s'                 => ['2.0 S'],
                                '2_4_el'                => ['2.4 EL'],
                                '2_4_el_prestige'       => ['2.4 EL Prestige'],
                            ],

                        'cr_v_y12_16'                   => [
                                '2_0_e'                 => ['2.0 E'],
                                '2_0_s'                 => ['2.0 S'],
                                '2_4_el'                => ['2.4 EL'],
                            ],

                        'cr_v_y17_21'                   => [
                                '1_6_dt_e'              => ['1.6 DT E'],
                                '1_6_dt_el'             => ['1.6 DT EL'],
                                '2_4_e'                 => ['2.4 E'],
                                '2_4_el'                => ['2.4 EL'],
                            ],

                        'cr_v_y95_02'                   => [
                                '2_0_exi'               => ['2.0 EXi '],
                                '2_0_exi_limited'       => ['2.0 EXi Limited'],

                            ],
                    ],


                    'cr_x_del_sol' =>[
                        'cr_x_del_sol_y02_98'           => [
                                '1_6'                   => ['1.6'],
                            ],
                    ],


                    'cr_z' =>[
                        'cr_z_y10_16'                   => [
                                '1_5_jp'                => ['1.5 JP'],
                            ],
                    ],

                    'crossroad' =>[
                        'crossroad_y08_10'              => [
                                '2_0'                   => ['2.0'],
                            ],
                    ],

                    'elysion' =>[
                        'elysion_y04_13'                => [
                                '2_4_s_prestige'        => ['2.4 S Prestige'],
                            ],
                    ],

                    'elysion' =>[
                        'elysion_y04_13'                => [
                                '2_4_s_prestige'        => ['2.4 S Prestige'],
                            ],
                    ],

                    'fit' =>[
                        'fit_y08_14'                    => [
                                '1_5_rs'                => ['1.5 RS'],
                            ],
                    ],

                    'freed' =>[
                        'freed_y08_16'                  => [
                                '1_5_e'                 => ['1.5 E'],
                                '1_5_e_sport'           => ['1.5 E Sport'],
                                '1_5_el'                => ['1.5 EL'],
                                '1_5_limited'           => ['1.5 Limited'],
                                '1_5_s'                 => ['1.5 S'],
                                '1_5_se'                => ['1.5 SE'],
                            ],
                    ],

                    'hr_v' =>[
                        'hr_v_y14_18'                   => [
                                '1_8_e'                 => ['1.8 E'],
                                '1_8_e_limited'         => ['1.8 E Limited'],
                                '1_8_el'                => ['1.8 EL'],
                                '1_8_limited'           => ['1.8 Limited'],
                                '1_8_el'                => ['1.8 EL'],
                                '1_8_s'                 => ['1.8 S'],
                            ],
                    ],


                    'inspire' =>[
                        'inspire_y95_98'                => [
                                '2_0'                   => ['2.0'],
                            ],
                    ],


                    'integra' =>[
                        'dc2_y94_01'                    => [
                                '1_8_type_r'            => ['1.8 TYPE-R'],
                                '1_8_vti-lx'            => ['1.8 VTi-LX'],
                            ],
                        'dc5_y01_06'                    => [
                                '2_0_type_r'            => ['2.0 TYPE-R'],
                            ],
                    ],

                    'jazz' =>[
                        'jazz_y03_07'                   => [
                                '1_5_cool'              => ['1.5 Cool'],
                                '1_5_e'                 => ['1.5 E'],
                                '1_5_e_i-dsi'           => ['1.5 E i-DSi'],
                                '1_5_e_i_dsi_safety'    => ['1.5 E i-DSi Safety'],
                                '1_5_e_v'               => ['1.5 E-V'],
                                '1_5_e_v_vtec'          => ['1.5 E-V VTEC'],
                                '1_5_e_v_vtec_cool'     => ['1.5 E-V VTEC Cool'],
                                '1_5_e_v_vtec_x_treme'  => ['1.5 E-V VTEC X-Treme'],
                                '1_5_s'                 => ['1.5 S'],
                                '1_5_s_i_dsi'           => ['1.5 S i-DSi'],
                                '1_5_sv_vtec'           => ['1.5 SV VTEC'],
                                '1_5_v_vtec'            => ['1.5 V VTEC'],
                            ],

                        'jazz_y08_14'                   => [
                                '1_3_hybrid'            => ['1.3 Hybrid'],
                                '1_5_active_plus'       => ['1.5 Active Plus'],
                                '1_5_jp'                => ['1.5 JP'],
                                '1_5_s_i_dsi'           => ['1.5 S i-DSi'],
                                '1_5_s_i_vtec'          => ['1.5 S i-VTEC'],
                                '1_5_sv'                => ['1.5 SV'],
                                '1_5_sv_i_vtec'         => ['1.5 SV i-VTEC'],
                                '1_5_sv_vtec'           => ['1.5 SV VTEC'],
                                '1_5_v'                 => ['1.5 V'],
                                '1_5_v_active_plus'     => ['1.5 V Active Plus'],
                                '1_5_v_i_vtec'          => ['1.5 V i-VTEC'],
                                '1_5_v_i_vtec_modulo'   => ['1.5 V i-VTEC Modulo'],
                                '1_5_v_modulo'          => ['1.5 V Modulo'],
 
                            ],

                        'jazz_y14_18'                   => [
                                '1_5_rs_i_vtec'         => ['1.5 RS i-VTEC'],
                                '1_5_rs_plus_i_vtec'    => ['1.5 RS+ i-VTEC'],
                                '1_5_s_i_vtec'          => ['1.5 S i-VTEC'],
                                '1_5_sv_i_vtec'         => ['11.5 SV i-VTEC'],
                                '1_5_sv_plus_i_vtec'    => ['1.5 SV+ i-VTEC'],
                                '1_5_v_i-vtec'          => ['1.5 V i-VTEC'],
                                '1_5_v_plus_i_vtec'     => ['1.5 V+ i-VTEC'],
                               
                            ],
                    ],



                    'legend' =>[
                        'legend_y04_12'                 => [
                                '3_5_v6'                => ['3.5 V6'],
                            ],

                        'legend_y91_98'                 => [
                                '3_2_exi'               => ['3.2 EXi'],
                            ],

                        'legend_y95_04'                 => [
                                '3_5'                   => ['3.5'],        
                            ],
                    ],


                    'mobillio' =>[
                        'mobillio_y14_17'               => [
                                '1_5_rs'                => ['1.5 RS'],
                                '1_5_s'                 => ['1.5 S'],
                                '1_5_v'                 => ['1.5 V'],
                            ],
                    ],


                    'nsx' =>[
                        'nsx_y16_20'                   => [
                                '3_5'                  => ['3.5'],
                            ],
                    ],


                    'odyssey' =>[
                        'odyssey_y03_08'               => [
                                '2_4_el'               => ['2.4 EL'],
                                '2_4_elx'              => ['2.4 ELX'],
                            ],
                        'odyssey_y08_13'               => [
                                '2_4_e'                => ['2.4 E'],
                                '2_4_el'               => ['2.4 EL'],
                                '2_4_jp'               => ['2.4 JP'],
                            ],
                        'odyssey_y13_16'               => [
                                '2_4_el'               => ['2.4 EL'],
                            ],
                        'odyssey_y95_99'               => [
                                '2_2_exclusive'        => ['2.2 Exclusive'],
                                '2_2 EXi'              => ['2.2 EXi'],
                            ],
                        'odyssey_y99_03'               => [
                                '2_3_vti'              => ['2.3 VTi'],
                                '3_0_prestige'         => ['3.0 Prestige'],
                            ],
                    ],



                    'prelude' =>[
                        'prelude_y91_98'               => [
                                '2_2_exi'              => ['2.2 EXi'],
                                '2_2_lxi_cx'           => ['2.2 LXi CX'],
                                '2_2_vti_ex'           => ['2.2 VTi-EX'],
                            ],
                    ],


                    's660' =>[
                        's660_y15_18'                  => [
                                '660_jdm'              => ['660 JDM'],
                            ],
                    ],


                    'stepwgn' =>[
                        'stepwgn_y05_09'                  => [
                                '2_4_i_vtec'              => ['2.4 i-VTEC'],
                                '2_0_i_vtec'              => ['2.0 i-VTEC'],
                            ],
                    ],


                    'stepwgn_spada' =>[
                        'stepwgn_spada_y09_16'          => [
                                '2_0_e'                 => ['2.0 E'],
                                '2_0_el'                => ['2.0 EL'],
                                '2_0_jp'                => ['2.0 JP'],
                            ],
                    ],


                    'stream' =>[
                        'stream_y00_06'                 => [
                                '2_0_e'                 => ['2.0 E'],
                                '2_0_s'                 => ['2.0 S'],
                            ],
                    ],


                    'today' =>[
                        'today_y85_98'                  => [
                                '660'                   => ['660'],
                            ],
                    ],

                ],//ปิด Honda

                'toyota' => [
                    '4runner' => [
                            '4runner_y90_95'            => [
                                    '3_0'               => ['3.0']
                            ]
                    ],
                    '86gt' => [
                        '86gt_y90_95'                   => [
                                '2_0'                   => ['2.0']
                            ]
                    ],
                    'alphard' => [
                        'alphard_y02_07'            => [
                                '2_4_g'             => ['2.4 G'],
                                '2_4_hybrid'        => ['2.4 HYBRID'],
                                '2.4_hybrid_e4'     => ['2.4 Hybrid E-Four'],
                                '2_4_v'             => ['2.4 V'],
                                '3_0_g_v6'          => ['3.0 G V6'],
                                '3_0_v_v6'          => ['3.0 V V6'],
                                '3_0_v6'            => ['3.0 V6']
                        ],

                        'alphard_y08_14'            => [
                                '2_4_g'             => ['2.4 G'], 
                                '2_4_gs'            => ['2.4 GS'],
                                '2_4_hv'            => ['2.4 HV'],
                                '2_4_v'             => ['2.4 V'],
                                '2_4_welcab'        => ['2.4 Welcab'],
                                '3_5_q'             => ['3.5 Q'],
                                '3_5_v'             => ['3.5 V']
                        ],
                        'alphard_y15_18'                        => [
                                '2_4_sa_package'                => ['2.4 SA Package'],
                                '2_5_g'                         => ['2.5 G'],
                                '2_5_hv'                        => ['2.5 HV'],
                                '2_5_hybrid_e4'                 => ['2.5 HYBRID E-Four'],
                                '2_5_hybrid_g_f_package_e4'     => ['2.5 HYBRID G F-Package E-Four'],
                                '2_5_hybrid_sr_c_package_e4'    => ['2.5 HYBRID SR C Package E-Four'],
                                '2_5_hybrid_sr_e4_welcab'       => ['2.5 HYBRID SR E-Four Welcab'],
                                '2_5_s_a_package'               => ['2.5 S A-Package'],
                                '2_5_s_a_package_type_black'    => ['2.5 S A-Package Type Black'],
                                '2_5_sc'                        =>  ['2.5 SC'],
                                '2_5_welcab'                    => ['2.5 Welcab']
                        ],
                    ],
                    'aristo' => [
                        'aristo_y97_04'                         => [
                                '3_0_v300'                      => ['3.0 V300']
                        ],
                    ],
                    'avanza' => [
                        'aristo_y04_11'                         => [
                                '1_3_e'                         => ['1.3 E'],
                                '1_3_e_limited'                 => ['1.3 E Limited'],
                                '1_3_s'                         => ['1.3 S'],
                                '1_5_e'                         => ['1.5 E'],
                                '1_5_e_exclusive'               => ['1.5 E Exclusive'],
                                '1_5_e_limited'                 => ['1.5 E Limited'],
                                '1_5_j'                         => ['1.5 J'],
                                '1_5_s'                         => ['1.5 S']
                        ],
                        'aristo_y12_16'                         => [
                                '1_5_e'                         => ['1.5 E'],
                                '1_5_g'                         => ['1.5 G'],
                                '1_5_s'                         => ['1.5 S'],
                                '1_5_s_touring'                 => ['1.5 S Touring']
                        ],
                    ],
                    'avensis' => [
                        'avensis_y10_16'                        => [
                                '2_0_li'                        => ['2.0 Li']
                        ],
                    ],
                    'aygo'  => [
                        'avensis_y15_20'                        =>[
                                '1_0'                           => ['1.0']
                            ],
                    ],
                    'bb'   => [
                        'db_y06_12'                             =>[
                            '1_3_s'                             => ['1_3_S'],
                            '1_5_z'                             => ['1.5 Z']     
                        ],
                    ], 
                    'c_hr' => [
                        'c_hr_y17_21'                           =>[
                            '1.8 hybrid'                        => ['1.8 Hybrid']
                        ],
                    ],
                    'camry' => [
                        'year_2_6'                              => [
                                '2_0_e'                         => ['2.0 E'],
                                '2_0_g'                         => ['2.0 G'],
                                '2_4_g'                         => ['2.4 G'],
                                '2_4_q'                         => ['2.4 Q']
                            ],
                        'year_6_12'                             => [
                                '2_0_e'                         => ['2.0 E'],
                                '2_0_g'                         => ['2.0 G'],
                                '2_0_g_ex'                      => ['2.0 G Extremo'],
                                '2_4_e'                         => ['2.4 G'],
                                '2_4_hybrid'                    => ['2.4 Hybrid'],
                                '2_4_hybrid_ex'                 => ['2.4 Hybrid Extremo'],
                                '2_4_v'                         => ['2.4 V'],
                                '3_5_q'                         => ['3.5 Q']
                        ],
                        'year_12_16'                            => [
                                '2_0_g'                         => ['2.0 G'],
                                '2_0_g_ex'                      => ['2.0 G Extremo'],
                                '2_5_esport'                    => ['2.5 ESPORT'],
                                '2_5_g'                         => ['2.5 G'],
                                '2_5_hybrid'                    => ['2.5 Hybrid']
                        ],
                        'year_93_97'                            => [
                                '2_2_gxi'                       => ['2.2 GXi']
                        ],
                        'light_long'                            => [
                                '2_2_seg'                       => ['2.2 SEG'],
                                '3_0_glx_v6'                    => ['3.0 GLX V6']
                        ],
                        'light_long_sub'                        => [
                                '2_2_seg'                       => ['2.2 SEG']
                        ],
                    ],
                    'carina' => [
                        'carina_y96_01'                         => [
                            '1_5_ti'                            => ['1.5 Ti']
                        ],
                    ],
                    'celica' => [
                        'celica_y00_05'                         => [
                            '1_8'                               => ['1.8'] 
                        ],
                        '4eye'                                  => [
                            '2_2'                               => ['2.2']
                        ], 
                        'pop_up'                                => [
                            '2_0'                               => ['2.0'],
                            '2_2'                               => ['2.2']
                        ],  
                    ],
                    'celsior' => [
                        'celsior_y00_y06'                       => [
                            '4_0'                               => ['4.0']
                        ]
                    ],
                    'century' => [
                        'century_y97_15'                        => [
                            '5_0'                               => ['5.0']
                        ]
                    ],
                    'corolla' => [
                        'corolla_y66_70'                        => [
                            '1_2_sprinter'                      => ['1.2 Sprinter']
                        ],   
                        'hi_torque'                         => [
                            '1_6_dxis'                      => ['1.6 DXIS'],
                            '1_6_gxi'                       => ['1.6 GXi'],
                            '1_6_gxis'                      => ['1.6 GXIS'],
                            '1_6_seg'                       => ['1.6 SEG'],
                            '1_8_seg'                       => ['1.8 SEG'],
                            '1_8_seg_altis'                 => ['1.8 SEG Altis']
                        ], 
                        'ke'                                => [
                            '1_3_xx'                        => ['1.3 XX']
                        ],
                        'ass_duck'                      => [
                            '1_5_dxi'                   => ['1.5 DXi'],
                            '1_5_gxi'                   => ['1.5 GXi'],
                            '1_5_gxi_saloon'            => ['1.5 GXi Saloon'],
                            '1_6_gxi'                   => ['1.6 GXi'],
                            '1_6_gxi_saloon'            => ['1.6 GXi Saloon'],
                            '1_6_seg'                   => ['1.6 SEG']
                        ],
                        'three_circle'                  => [
                            '1_3_dxi'                   => ['1.3 DXi'],
                            '1_3_gl'                    => ['1.3 GL'],
                            '1_3_gxi'                   => ['1.3 GXi'],
                            '1_6_gli'                   => ['1.6 GLi'],
                            '1_6_gxi'                   => ['1.6 GXi']
                        ],
                        'Doremon_y88_92'                => [
                            '1_6_gli'                   => ['1.6 GLi'],
                            '1_6_sel_limited'           => ['1.6 SEL Limited']
                        ]
                    ],
                    'corolla_altis'=> [
                        'y14_18'                        => [
                            '1_6_g'                     => ['1.6 G'],
                            '1_8_e'                     => ['1.8 E'],
                            '1_8_esport'                => ['1.8 ESPORT'],
                            '1_8_v'                     => ['1.8 V']
                        ],
                        'altis_y80_13'                  => [
                            '1_6_cng'                   => ['1.6 CNG'],
                            '1_6_e'                     => ['1.6 E'],
                            '1_6_g'                     => ['1.6 G'],
                            '1_6_j'                     => ['1.6 J'],
                            '1_6_ss-i'                  => ['1.6 SS-I'],
                            '1_6_trd_sportivo'          => ['1.6 TRD Sportivo'],
                            '1_8_e'                     => ['1.8 E'],
                            '1_8_g'                     => ['1.8 G'],
                            '1_8_trd_sportivo'          => ['1.8 TRD Sportivo'],
                            '2_0_g'                     => ['2.0 G'],
                            '2_0_v'                     => ['2.0 V']
                        ],
                        'altis_y14_18'                  => [
                            '1_6_e'                     => ['1.6 E'],
                            '1_6_g_cng'                 => ['1.6 E CNG'],
                            '1_6_g'                     => ['1.6 G'],
                            '1_6_j'                     => ['1.6 J'],
                            '1_8_e'                     => ['1.8 E'],
                            '1_8_esport'                => ['1.8 ESPORT'],
                            '1_8_g'                     => ['1.8 G'],
                            '1_8_s'                     => ['1.8 S'],
                            '1_8_v'                     => ['1.8 V']
                        ],
                        'face_pig'=> [
                            '1_6_e'                     =>  ['1.6 E'],
                            '1_6_e_limited'             => ['1.6 E Limited'],
                            '1_6_g'                     => ['1.6 G'],
                            '1_6_j'                     => ['1.6 J'],
                            '1_8_e'                     => ['1.8 E'],
                            '1_8_e_limited'             => ['1.8 E Limited'],
                            '1_8_g'                     => ['1.8 G'],
                            '1_8_s'                     => ['1.8 S']
                        ]
                    ],
                    'corona'=> [
                        'exsior'                        =>[
                            '1_6_exsior_exi'            => ['1.6 Exsior GXi'],
                            '2_0_exsior_sed'            => ['2.0 Exsior SEG']
                        ],
                        'tuyen'                         => [
                            '1_6_xl'                    => ['1.6 XL']
                        ],
                        'end_separate'                  => [
                            '1_6_gli'                   => ['1.6 GLi'],
                            '2_0'                       => ['2.0'], 
                            '2_0_gli'                   => ['2.0 GLi']
                            
                        ],
                        'end_high'                      => [
                            '2_0_gli'                   => ['2.0 GLi']
                        ],
                        'facegiant_facesmile'           => [
                            '1_6_gl'                    => ['1.6 GL'],
                            '2_0_gl'                    => ['2.0 GL'],   
                            '2_0_gli'                   => ['2.0 GLi']
                        ],
                        'facesharp'                     => [
                            '1_6_gx'                    => ['1.6 GX']
                        ]
                    ],

                    'cressida'=> [
                        'cressida_y88_92'               => [
                           '2_5'                        => ['2.5']
                        ]
                    ],
                    'crown'=> [
                        'crown_y00_05'                  => [
                            '2_5_royal_saloon'          => ['2.5 Royal Saloon'],
                            '3_0_royal_saloon'          => ['3.0 Royal Saloon']
                        ],
                        'crown_y13_17'                  => [
                            '2_5_hybrid_royal_saloon'   => ['2.5 Hybrid Royal Saloon'],
                            '2_5_royal_saloon'          => ['2.5 Royal Saloon']
                        ],
                        'lower_y2000'                   => [
                            '2_8_royal_saloon'          => ['2.8 Royal Saloon'],
                            '3_0_royal_saloon'          => ['3.0 Royal Saloon']
                        ]
                    ],
                    'dyna'=> [
                        'dyna_y95_02'                   => [
                            '2_7'                       => ['2.7'] 
                        ]
                    ],
                    'esquire'=> [
                        'esquire_y14_17'                => [
                            '1_8_hybrid_Gi'             => ['1.8 HYBRID Gi'],
                            '2_0_gi'                    => ['2.0 Gi']
                        ]
                    ],
                    'estima'=> [
                        'estima_y00_05'                 => [
                            '2_4_aeras'                 => ['2.4 Aeras'],
                            '2_4_g'                     => ['2.4 G'],
                            '2_4_hybrid_E4'             => ['2.4 Hybrid E-Four'],
                            '2_4_l'                     => ['2.4 L']
                        ],
                        'estima_y06_14'                 => [
                            '2_4_aeras'                 => ['2.4 Aeras'],
                            '2_4_g'                     => ['2.4 G'],
                            '2_4_hybrid_e4'             => ['2.4 Hybrid E-Four']
                        ],
                        'estima_y16_19'                 => [
                            '2_4_aeras_premium'         => ['2.4 Aeras Premium']
                        ]
                    ],
                    'fj_cruiser'=> [
                        'fj_cruiser_y07_15'             => [
                            '4_0_4x4'                   => ['4.0 4x4']
                        ]
                    ],
                    'fortuner'=> [
                        'fortuner_y04_08' => [
                            '2_7_v'             => ['2.7 V'],
                            '3_0_exclusive_v'   => ['3.0 Exclusive V'],
                            '3_0_g'             => ['3.0 G'],
                            '3_0_smart_v'       => ['3.0 Smart V'],
                            '3_0_v'             => ['3.0 V'],
                            '3_0_v_exclusive'   => ['3.0 V Exclusive'],
                            '3_0_v_smart'       => ['3.0 V Smart']

                        ],
                        'fortuner_y08_11' => [
                            '2_5_g'                 => ['2.5 G'],
                            '2_7_v'                 => ['2.7 V'],
                            '3_0_g'                 => ['3.0 G'],
                            '3_0_trd_sportivo_I'    => ['3.0 TRD Sportivo I'],
                            '3_0_trd_sportivo_ii'   => ['3.0 TRD Sportivo II'],
                            '3_0_trd_sportivo_iii'  => ['3.0 TRD Sportivo III'],
                            '3_0_V'                 => ['3.0 V'],
                            '3_0_V_aperto'          => ['3.0 V Aperto'],
                            '3_0_V_smart'           => ['3.0 V Smart']
                        ],
                        'fortuner_y12_15'=> [
                            '2_5_g'                 => ['2.5 G'],
                            '2_5_v'                 => ['2.5 V'],
                            '2_7_v'                 => ['2.7 V'],
                            '3_0_trd_sportivo'      => ['3.0 TRD Sportivo'],
                            '3_0_v'                 => ['3.0 V']
                        ],
                        'fortuner_y15_18'=> [
                            '2_4_g'                 => ['2.4 G'],
                            '2_4_v'                 => ['2.4 V'],
                            '2_7_v'                 => ['2.7 V'],
                            '2_8_trd_sportivo'      => ['2.8 TRD Sportivo'],
                            '2_8_v'                 => ['2.8 V'],
                            '2_8_v_4wd'             => ['2.8 V 4WD']
                        ],
                        'fortuner_y1518'            => [
                            '2_8_trd_sportivo'      => ['2.8 TRD Sportivo']
                        ]
                    ],

                    'grand_hiace'=> [
                            'grand_hiace_y92_02'    => [
                                '3_4'               => ['3.4']       
                            ]
                    ],
                    'granvia'=> [
                            'granvia_y95_02'        => [
                                '2_7_g'             => ['2.7 G'],
                                '3_0'               => ['3.0'],
                                '3_4_v6'            => ['3.4 V6']
                            ]
                    ],
                    'harrier'=> [
                            'harrier_y03_13'        => [
                                '2_4_240g'          => ['2.4 240G'],
                                '3_0_300g'          => ['3.0 300G']
                            ],
                            'harrier_y14_17' => [
                                '2_0_elegance_fs'   => ['2.0 Elegance GS'],
                                '2_0_premium'       => ['2.0 PREMIUM'],
                                '2_5_hybrid'        => ['2.5 HYBRID'],
                                '2_5_hybrid_e4'     => ['2.5 Hybrid E- Four'],
                                '2_5_hybrid_premium'=> ['2.5 Hybrid PREMIUM']
                            ],
                            'harrier_y97_03'        => [
                                '2_4_240g'          => ['2.4 240G'],
                                '3_0_300g'          => ['3.0 300G']
                            ]
                    ],

                    'hiace'=> [
                            'commuter_y05_16'       => [
                                '2_5_d4d'           => ['2.5 D4D'],
                                '2_7_vvti'          => ['2.7 VVTi'],
                                '3_0_d4d'           => ['3.0 D4D ']
                            ],
                            'short_y05_16'          => [
                                '2_5_d4d'           => ['2.5 D4D'],
                                '3_0_d4d'           => ['3.0 D4D']
                            ],
                            'rocket_head'           => [
                                '2_0'               => ['2.0'],
                                '2_4'               => ['2.4'],
                                '2_4_economy'       => ['2.4 Economy'],
                                '2_4_gl'            => ['2.4 GL'],
                                '2_5_commuter'      => ['2.5 Commuter'],
                                '2_8_gl'            => ['2.8 GL'],
                                '3_0_commuter'      => ['3.0 Commuter'],
                                '3_0_economy'       => ['3.0 Economy'],
                                '3_0_gl'            => ['3.0 GL']
                            ]
                    ],
                    'hilux_hero' => [
                            'single'                => [
                                '2_5'               => ['2.5']
                            ]
                    ],
                    'hilux_mighty_x' => [
                            'double_cab'            =>  [
                                '2_4_gl'            => ['2.4 GL']
                            ],
                            'extracab'              => [
                                '2_4'               => ['2.4'],
                                '2_4_gl'            => ['2.4 GL'],
                                '2_4_sgl_luxury'    => ['2.4 SGL Luxury'],
                                '2_4_standard'      => ['2.4 Standard'],
                                '2_4_super_gl'      => ['2.4 Super GL'],
                                '2_5_gl'            => ['2.5 GL']
                            ],
                            'single'  => [
                                '2_4_standard'      => ['2.4 Standard']
                            ]
                    ],
                    
                    'hilux_revo' => [
                            'revo_double_cab'                   => [
                                '2_4_e'                         => ['2.4 E'],
                                '2_4_e_plus'                    => ['2.4 E Plus'],
                                '2_4_e_prerunner'               => ['2.4 E Prerunner'],
                                '2_4_g_prerunner'               => ['2.4 G Prerunner'],
                                '2_4_j_plus_prerunner'          => ['2.4 J Plus Prerunner'],
                                '2_4_prerunner_e'               => ['2.4 Prerunner E (30)'],
                                '2_4_prerunner_e_plus'          => ['2.4 Prerunner E Plus'],
                                '2_4_prerunner_g'               => ['2.4 Prerunner G'],
                                '2_4_prerunner_j_plus'          =>  ['2.4 Prerunner J Plus'],
                                '2_4_prerunner_trd_sportivo'    => ['2.4 Prerunner TRD Sportivo '],
                                '2_4_trd_sportivo'              => ['2.4 TRD Sportivo'],
                                '2_8_g'                         => ['2.8 G'],
                                '2_8_g_prerunner'               => ['2.8 G Prerunner'],
                                '2_8_prerunner_g'               => ['2.8 Prerunner G']
                            ],
                            'revo_single'       => [
                                '2_4_j'         => ['2.4 J'],
                                '2_4_j_plus'    => ['2.4 J Plus'],
                                '2_7_j'         => ['2.7 J'],
                                '2_8_j'         => ['2.8 J'],
                                '2_8_4x4'       => ['2.8 J 4x4'],
                                '2_8_j_plus'    => ['2.8 J Plus']
                            ],
                            'revo_smartcab'                     => [
                                '2_4_e'                         => ['2.4 E'],
                                '2_4_e_prerunner'               => ['2.4 E Prerunner'],
                                '2_4_g'                         => ['2.4 G'],
                                '2_4_g_prerunner'               => ['2.4 G Prerunner'],
                                '2_4_j'                         => ['2.4 J'],
                                '2_4_j_plus'                    => ['2.4 J Plus'],
                                '2_4_j_plus_prerunner'          => ['2.4 J Plus Prerunner'],
                                '2_4_prerunner_e_plus'          => ['2.4 Prerunner E Plus'],
                                '2_4_prerunner_g'               => ['2.4 Prerunner G'],
                                '2_4_prerunner_j_plus'          => ['2.4 Prerunner J Plus'],
                                '2_4_prerunner_trd_sportivo'    => ['2.4 Prerunner TRD Sportivo'],
                                '2_4_trd_sportivo'              => ['2.4 TRD Sportivo'],
                                '2_7__e_prerunner'              => ['2.7 E Prerunner'],
                                '2_7_prerunner_e'               => ['2.7 Prerunner E'],
                                '2_8_g'                         => ['2.8 G'],
                                '2_8_g_Prerunner'               => ['2.8 G Prerunner'],
                                '2_8_prerunner_g'               => ['2.8 Prerunner G']
                            ],
                            'revo_smartcab_smartcab'            => [
                                '2_4_prerunner_trd_sportivo'    => ['2.4 Prerunner TRD Sportivo'],
                                '2_4_trd_sportivo'              => ['2.4 TRD Sportivo']
                            ],

                    ],

                    'hilux_surf'=> [
                            'surf_y88_97'       => [
                                '2_8'           => ['2.8'],
                                '3_0_ssr'       => ['3.0 SSR']
                            ]

                    ],
                    'hilux_tiger'=> [
                            'tiger_doublecab'   => [
                                '2_4'           => ['2.4'],
                                '2_4_gl'        => ['2.4 GL'],
                                '2_4_super_gl'  => ['2.4 Super GL'],
                                '2_5_gl'        => ['2.5 GL'],
                                '3_0_gl'        => ['3.0 GL']
                            ],

                    ],
                    'hilux_vigo'=> [

                            'champ_double_cab'                  => [
                                '2_5_e'                         => ['2.5 E'],
                                '2_5_e_prerunner_vn_turbo'      => ['2.5 E Prerunner VN Turbo'],
                                '2_5_e_prerunner_vn_turbo_trd'  => ['2.5 E Prerunner VN Turbo TRD'],
                                '2_5_g'                         => ['2.5 G'],
                                '2_5_g_prerunner_vn_turbo'      => ['2.5 G Prerunner VN Turbo'],
                                '2_5_g_vn_turbo'                => ['2.5 G VN Turbo'],
                                '2_5_j'                         => ['2.5 J'],
                                '2_5_prerunner_e'               => ['2.5 Prerunner E'],
                                '2_5_prerunner_e_trd_sportivo'  => ['2.5 Prerunner E TRD Sportivo'],
                                '2_5_prerunner_g'               => ['2.5 Prerunner G'],
                                '2_7_e'                         => ['2.7 E'],
                                '3_0_g'                         => ['3.0 G'],
                                '3_0_g_4x4_vn_turbo'            => ['3.0 G 4x4 VN Turbo'],
                                '3_0_g_prerunner_vn_turbo'      => ['3.0 G Prerunner VN Turbo'],
                                '3_0_prerunner_g'               => ['3.0 Prerunner G']
                            ],
                            'champ_extracab'                    => [
                                '2_5_j'                         => ['2.5 J'],
                                '2_7_j'                         => ['2.7 J']
                            ],
                            'champ_single'                      =>[
                                '2_5_j'                         => ['2.5 J'],
                                '2_5_j_std'                     => ['2.5 J STD'],
                                '2_7_cng'                       => ['2.7 CNG'],
                                '2_7_j'                         => ['2.7 J'],
                                '3_0_j'                         => ['3.0 J']
                            ],
                            'champ_smartcab'                    => [
                                '2_5_e'                         => ['2.5 E'],
                                '2_5_e_prerunner'               => ['2.5 E Prerunner'],
                                '2_5_e_prerunner_vn_turbo'      => ['2.5 E Prerunner VN Turbo'],
                                '2_5_e_prerunner_vn_turbo_trd'  => ['2.5 E Prerunner VN Turbo TRD'],
                                '2_5_e_trd'                     => ['2.5 E TRD'],
                                '2_5_e_trd_sportivo'            => ['2.5 E TRD Sportivo'],
                                '2_5_e_vn_turbo_trd'            => ['2.5 E VN Turbo TRD'],
                                '2_5_g'                         => ['2.5 G'],
                                '2_5_g_prerunner_vn_turbo'      => ['2.5 G Prerunner VN Turbo'],
                                '2_5_j'                         => ['2.5 J'],
                                '2_5_j_std'                     => ['2.5 J STD'],
                                '2_5_prerunner_e'               => ['2.5 Prerunner E'],
                                '2_5_prerunner_e_trd_sportivo'  => ['2.5 Prerunner E TRD Sportivo'],
                                '2_5_prerunner_g'               => ['2.5 Prerunner G'],
                                '2_7_cng'                       => ['2.7 CNG'],
                                '2_7_j'                         => ['2.7 J'],
                                '2_7_j_cng'                     => ['2.7 J CNG'],
                                '2_7_j_std'                     => ['2.7 J STD'],
                                '3_0_g'                         => ['3.0 G'],
                                '3_0_g_prerunner_vn_turbo'      => ['3.0 G Prerunner VN Turbo'],
                                '3_0_prerunner_g'               => ['3.0 Prerunner G']
                           ],
                           'vigo_bdcab_y05_08'                  => [
                                '2_5_e'                         => ['2.5 E'],
                                '2_5_g'                         => ['2.5 G'],
                                '2_5_j'                         => ['2.5 J'],
                                '2_7_g'                         => ['2.7 G'],
                                '3_0_e'                         => ['3.0 E'],
                                '3_0_e_prerunner'               => ['3.0 E Prerunner'],
                                '3_0_g'                         => ['3.0 G'],
                                '3_0_prerunner_e'               => ['3.0 Prerunner E']
                           ],
                           'vigo_bdcab_y08_11'                  => [
                                '2_5_e'                         => ['2.5 E'],
                                '2_5_e_prerunner_vn_turbo'      => ['2.5 E Prerunner VN Turbo'],
                                '2_5_g'                         => ['2.5 G'],
                                '2_5_j'                         => ['2.5 J'],
                                '2_5_prerunner_e'               => ['2.5 Prerunner E'],
                                '2_7_e'                         => ['2_7_E'],
                                '3_0_e'                         => ['3.0 E'],
                                '3_0_e_prerunner'               => ['3.0 E Prerunner'],
                                '3_0_e_prerunner_vn_turbo'      => ['3.0 E Prerunner VN Turbo'],
                                '3_0_g'                         => ['3.0 G'],
                                '3_0_g_4x4'                     => ['3.0 G 4x4'],
                                '3_0_g_prerunner'               => ['3.0 G Prerunner'],
                                '3_0_g_prerunner_vn_turbo'      => ['3.0 G Prerunner VN Turbo'],
                                '3_0_g_vn_turbo'                => ['3.0 G VN Turbo'],
                                '3_0_prerunner_e'               => ['3.0 Prerunner E'],
                                '3_0_prerunner_g'               => ['3.0 Prerunner G']
                           ],
                           'vigo_extracab_y04_08'               => [
                                '2_5_e'                         => ['2.5 E'],
                                '2_5_g'                         => ['2.5 G'],
                                '2_5_j'                         => ['2.5 J'],
                                '2_5_prerunner'                 => ['2.5 Prerunner'],
                                '2_7_g'                         => ['2.7 G'],
                                '3_0_e'                         => ['3.0 E'],
                                '3_0_e_prerunner'               => ['3.0 E Prerunner'],
                                '3_0_g'                         => ['3.0 G'],
                                '3_0_j'                         => ['3.0 J'],
                                '3.0 Prerunner'                 => ['3.0 Prerunner']
                           ],
                           'vigo_extracab_y08_11'               => [
                                '2_5_e'                         => ['2.5 E'],
                                '2_5_j'                         => ['2.5 J'],
                                '3_0_e Prerunner'               => ['3.0 E Prerunner']
                           ],
                           'vigo_single_y04_08'                 => [
                                '2_5_j'                         => ['2.5 J'],
                                '3_0_j'                         => ['3.0 J']
                           ],
                           'vigo_single_y08_11'                 => [
                                '2_5_j'                         => ['2.5 J'],
                                '2_7_j'                         => ['2.7 J'],
                                '3_0_j'                         => ['3.0 J']
                           ],
                           'vigo_smartcab_y08_11'           => [
                                '2_5_e'                     => ['2.5 E'],
                                '2_5_e_limited'             => ['2.5 E Limited'],
                                '2_5_e_prerunner'           => ['2.5 E Prerunner'],
                                '2_5_e_prerunner_vn_turbo'  => ['2.5 E Prerunner VN Turbo'],
                                '2_5_g'                     => ['2.5 G'],
                                '2_5_g_prerunner'           => ['2.5 G Prerunner'],
                                '2_5_j'                     => ['2.5 J'],
                                '2_5_prerunner_e'           => ['2.5 Prerunner E'],
                                '2_7_g'                     => ['2.7 G'],
                                '2_7_j'                     => ['2.7 J'],
                                '3_0_e'                     => ['3.0 E'],
                                '3_0_e_prerunner'           => ['3.0 E Prerunner'],
                                '3_0_g'                     => ['3.0 G'],
                                '3_0_g_prerunner_vn_turbo'  => ['3.0 G Prerunner VN Turbo'],
                                '3_0_g_vn_turbo'            => ['3.0 G VN Turbo'],
                                '3_0_prerunner_e'           => ['3.0 Prerunner E'],
                                '3_0_prerunner_e'           => ['3.0 Prerunner G']
                           ],
                           'vigo_sportvan_y08_11'           => [
                                '3_0'                       => ['3.0']
                           ],
                    ],

                    'innova'=> [
                        'innova_y04_11'                     => [
                                '2_0_e'                     => ['2.0 E'],
                                '2_0_g'                     => ['2.0 G'],
                                '2_0_g_exclusive'           => ['2_0_g_exclusive'],
                                '2_0_v'                     => ['2.0 V'],
                                '2_5_v'                     => ['2.5 V']
                        ],
                        'innova_y11_15'                     => [
                                '2_0_e'                     => ['2.0 E'],
                                '2_0_g'                     => ['2.0 G'],
                                '2_0_g_option'              => ['2.0 G Option'],
                                '2_0_v'                     => ['2.0 V']
                        ],
                        'innova_y16_20'                     => [
                                '2_8_crysta_v'              => ['2.8 Crysta V']
                        ],
                    ],

                    'iq'=> [
                        'iq_y08_15'                     => [
                                '1.0'                   => ['1.0']
                        ],
                    ],
                    'kluger'=> [
                        'kluger_y01_07'                 =>[
                                '3_0_v6'                => ['3.0 V6']
                        ],
                    ],

                    'land_cruiser'=> [
                        'land_cruiser_43'               => [
                                '3_4'                   => ['3.4']
                        ],
                        'land_cruiser_45'               => [
                                '4_2'                   => ['4.2']
                        ],
                        'land_cruiser_70'               => [
                                '3_4'                   => ['3.4'],
                                '4_0_30th_anniversary'  => ['4.0 30TH ANNIVERSARY']
                        ],
                        'land_cruiser_80'               => [
                                '4_5_vx_limited'        => ['4.5 VX Limited']
                        ],
                        'land_cruiser_100'              => [
                                '4_2_vx'                => ['4.2 VX'],
                                '4_7_cygnus'            => ['4.7 Cygnus'],
                                '4_7_vx_limited'        => ['4.7 VX Limited']
                        ],
                        'land_cruiser_200'              => [
                                '4_5_vx'                => ['4.5 VX'],
                                '4_6_zx'                => ['4.6 ZX'],
                                '4_7_vx'                => ['4.7 VX']
                        ]

                    ],
                    'landcruiser_prado'=> [
                        'land_cruiser_prado_90'         => [
                            '2_7_rx'                    => ['2.7 RX'],
                            '3_0'                       => ['3.0'],
                            '3_4'                       => ['3.4']
                        ],
                        'land_cruiser_prado_120'        => [
                            '2_7_tx'                    => ['2.7 TX'],
                            '3_4_rz'                    => ['3.4 RZ'],
                            '3_4_tx'                    => ['3.4 TX'],
                            '3_4_tz'                    => ['3.4 TZ']
                        ],
                        'land_prado_90'                 => [
                            '2_7_rx'                    => ['2.7 RX'],
                            '2_7_tx'                    => ['2.7 TX'],
                            '2_8_tx'                    => ['2.8 TX'],
                            '2_8_tz-g'                  => ['2.8 TZ-G'],
                            '3_0 d4d'                   => ['3.0 D4D']
                        ],
                    ],
                    'ln'=> [
                        'all_nickname'                  => [
                            '2_8_single'                => ['2.8 Single']
                        ],    
                    ],
                    'lucida'=> [
                        'lucida_y90_99'                 => [
                            '2_2'                       => ['2.2']
                        ],  
                    ],
                    'mark_2'=> [
                        'mark_2_y92-96'                 => [
                            '2_0'                       => ['2.0']
                        ],
                    ],
                    'mark_x'=> [
                        'mark_x_y04_10'                 => [
                            '2_5_250g'                  => ['2.5 250G']
                        ],
                    ],
                    'mega_cruiser'=> [
                        'mega_cruiser_y95_02'           => [
                            '4_1'                       => ['4.1']
                        ],
                    ],
                    'mega_cruiser'=> [
                        'mega_cruiser_y95_02'           => [
                            '4_1'                       => ['4.1']
                        ],
                    ],
                    'mr_s'=> [
                        'mr_s_y00_07'                   => [
                            '1_8'                       => ['1.8'],
                            '1_8_s'                     => ['1.8 S']
                        ],
                    ],
                    'mr_2'=> [
                        'mr_2_y89_99'                   => [
                            '2_0'                       => ['2.0'],
                            '2_0_g'                     => ['2.0 G']
                        ],
                    ],
                    'noah'=> [
                        'noah_y01_13'                   => [
                            '2_0_super_extra_limo'      => ['2.0 Super Extra Limo']
                        ],
                        'noah_y14_17'                   => [
                            '2_0_si'                    => ['2.0 Si']
                        ],
                    ],
                    'opa'=> [
                        'noah_y00_05'                   => [
                            '2_2017'                    => ['2.0']
                        ],
                    ],
                    'porte'=> [
                        'porte_y05_15'                  => [
                            '1_5_vvt_i'                 => ['1.5 VVT-i']
                        ],
                    ],
                    'prius'=> [
                        'porte_y05_15'                          => [
                            '1_8_hybrid'                        => ['1.8 Hybrid'],
                            '1_8_hybrid_e_trd_sportivo'         => ['1.8 Hybrid E TRD Sportivo'],
                            '1_8_hybrid_standard_grade'         => ['1.8 Hybrid Standard grade'],
                            '1_8_hybrid_top_grade'              => ['1.8 Hybrid Top grade'],
                            '1_8_hybrid_top_option_grade'       => ['1.8 Hybrid Top option grade'],
                            '1_8_hybrid_trd_sportivo_ii'        => ['1.8 Hybrid TRD Sportivo II '],
                            '1_8_hybrid_trd_sportivo_top_option' => ['1.8 Hybrid TRD Sportivo Top option'],
                            '1_8_trd_sportivo'                  => ['1.8 TRD Sportivo']
                        ],
                    ],
                    'rav4'=> [
                        'av4_y02_12'                    => [
                            '2_0_sporty'                => ['2.0 Sporty'],
                            '2_4_g'                     => ['2.4 G']
                        ],
                        'rav4_y95_02'                   => [
                            '2_0'                       => ['2.0']
                        ],
                    ],
                    'sienta'=> [
                        'ienta_y16_20'                  => [
                            '1_5_g'                     => ['1.5 G'],
                            '1_5_v'                     => ['1.5 V']
                        ],

                    ],
                    'soarer'=> [
                        'soarer_y01_05'                 => [
                            '4_3'                       => ['4.3']
                        ],
                    ],
                    'soluna'=> [
                        'al50_first_nickname'           => [
                            '1_5_gli'                   => ['1.5 GLi'],
                            '1_5_gli_v_version'         => ['1.5 GLi V-Version'],
                            '1_5_xli'                   => ['1.5 XLi']
                        ],
                        'al50_droplet_y00_03'           => [
                            '1_5_e'                     => ['1.5 E'],
                            '1_5_gli'                   => ['1.5 GLi'],
                            '1_5_sli'                   => ['1.5 SLi'],
                            '1_5_xli'                   => ['1.5 XLi']
                        ],
                        'droplet_al50_y00_03'           => [
                            '1_5_e'                     => ['1.5 E']
                        ],
                    ],
                    'soluna'=> [
                        'al50_first_nickname'           => [
                            '1_5_gli'                   => ['1.5 GLi'],
                            '1_5_gli_v_version'         => ['1.5 GLi V-Version'],
                            '1_5_xli'                   => ['1.5 XLi']
                        ],
                        'al50_droplet_y00_03'           => [
                            '1_5_e'                     => ['1.5 E'],
                            '1_5_gli'                   => ['1.5 GLi'],
                            '1_5_sli'                   => ['1.5 SLi'],
                            '1_5_xli'                   => ['1.5 XLi']
                        ],
                        'droplet_al50_y00_03'           => [
                            '1_5_e'                     => ['1.5 E']
                        ],
                    ],
                    'sport_cruiser'=> [
                        'sport_cruiser_y00_04'          => [
                            '2_5_e'                     => ['2.5 E'],
                            '2_5_prerunner_e'           => ['2.5 Prerunner E'],
                            '2_5_s'                     => ['2.5 S'],
                            '3_0_g'                     => ['3.0 G'],
                            '3_0_g_limited'             => ['3.0 G Limited']
                        ],
                    ],
                    'sport_rider'=> [
                        'sport_rider_y98_02'            => [
                            '3_0_prerunner'             => ['3.0 Prerunner'],
                            '3_0_prerunner_sgl_limited' => ['3.0 Prerunner SGL Limited'],
                            '3_0_sr5'                   => ['3.0 SR5'],
                            '3_0_sr5_limited'           => ['3.0 SR5 Limited'],
                            '3_0_sr5_limited_g_series'  => ['3.0 SR5 Limited G-Series']
                        ],
                        'd4d_y02_04'                    => [
                            '3_0_prerunner'             => ['2.5 Prerunner E'],
                            '2_5_s'                     => ['2.5 S'],
                            '3_0_g'                     => ['3.0 G'],
                            '3_0_sr5_limited'           => ['3.0 SR5 Limited'],
                            '3_0_limited'               => ['3.0 Limited']
                        ],
                    ],
                    'starlet'=> [
                        'starlet_ep71'                  => [
                            '1_3_xl'                    => ['1.3 XL']
                        ],
                        'starlet_ep82'                  => [
                            '1_3_xli'                   => ['1.3 XLi']
                        ],
                    ],
                    'super_custom'=> [
                        'super_custom_y96_04'           => [
                            '2_4'                       => ['2.4']
                        ],
                    ],
                    'super_wagon'=> [
                        'super_wagon_y96_04'            => [
                            '2_4'                       => ['2.4']
                        ],
                    ],
                    'supra'=> [
                        'supra_y02_02'                  => [
                            '3_0'                       => ['3.0'],
                            '3_0_a80'                   => ['3.0 A80']
                        ],
                    ],
                    'vellfire'=> [
                        'vellfire_y08_14'               => [
                            '2_4_gs'                    => ['2.4 GS'],
                            '2_4_hybrid_e_four'         => ['2.4 Hybrid E-Four'],
                            '2_4_v'                     => ['2.4 V'],
                            '2_4_z'                     => ['2.4 Z']
                        ],
                        'vellfire_y15_18'                           => [
                            '2_5'                                   => ['2.5'],
                            '2_5_e_four_hybrid'                     => ['2.5 E-Four Hybrid'],
                            '2_5_hybrid_e_four'                     => ['2.5 Hybrid E-Four'],
                            '2_5_hybrid_executive_lounge_e_four'    => ['2.5 HYBRID Executive Lounge E-Four'],
                            '2_5_hybrid_zr_g_edition_e_four'        => ['2.5 Hybrid ZR G Edition E-Four'],
                            '2_5_v'                                 => ['2.5 V'],
                            '2_5_welcab'                            => ['2.5 Welcab'],
                            '2_5_z'                                 => ['2.5 Z'],
                            '2_5_z_g_edition'                       => ['2.5 Z G EDITION'],
                            '2_5_za_edition'                        => ['2.5 ZA EDITION'],
                            '2_5_za_edition_golden_eyes'            => ['2.5 ZA EDITION GOLDEN EYES']
                        ],
                    ],
                    'ventury'=> [
                        'vellfire_y08_14'       => [
                            '2_7_g'             => ['2.7 G'],
                            '2_7_majesty'       => ['2.7 Majesty'],
                            '2_7_v'             => ['2.7 V'],
                            '2_7_v_majesty'     => ['2.7 V Majesty'],
                            '3_0_g'             => ['3.0 G'],
                            '3_0_v'             => ['3.0 V']
                        ],
                    ],
                    'vios'=> [
                        'vios_y02_07'           => [
                            '1_5_e'             => ['1.5 E'],
                            '1_5_e_ivory'       => ['1.5 E IVORY'],
                            '1_5_j'             => ['1.5 J'],
                            '2_7_v_majesty'     => ['1.5 S'],
                            '1_5_s'             => ['3.0 G'],
                            '1_5_s_sporty'      => ['1.5 S Sporty']
                        ],
                        'vios_y07_13'           => [
                            '1_5_e'             => ['1.5 E'],
                            '1_5_es'            => ['1.5 ES'],
                            '1_5_g'             => ['1.5 G'],
                            '1_5_g_limited'     => ['1.5 G Limited'],
                            '1_5_gt_street'     => ['1.5 GT Street'],
                            '1_5_j'             => ['1.5 J'],
                            '1_5_j_sportivo'    => ['1.5 J Sportivo'],
                            '1_5_s_limited'     => ['1.5 S Limited'],
                            '1_5_trd'           => ['1.5 TRD'],
                            '1_5_trd_sportivo'  => ['1.5 TRD Sportivo']
                        ],
                        'vios_y13_17'           => [
                            '1_5_e'             => ['1.5 E'],
                            '1_5_g'             => ['1.5 G'],
                            '1_5_j'             => ['1.5 J'],
                            '1_5_s'             => ['1.5 S'],
                            '1_5_trd'           => ['1.5 TRD'],
                            '1_5_trd_sportivo'  => ['1.5 TRD Sportivo']
                        ],
                        'vios_y1317'            => [
                            '1_5_e'             => ['1.5 E'],
                            '1_5_g'             => ['1.5 G'],
                            '1_5_j'             => ['1.5 J'],
                            '1_5_s'             => ['1.5 S']
                        ],
                    ],
                    'vista'=> [
                        'vista_y90_91'          => [
                            '2.0 vl'            => ['2.0 VL']
                        ],
                    ],
                    'vitz'=> [
                        'vitz_y00_05'           => [
                            '1.0'               => ['1.0']
                        ],
                    ],
                    'voxy'=> [
                        'voxy_y14_17'           => [
                            '2_0_azr60g'        => ['2.0 AZR60G']
                        ],
                    ],
                    'wish'=> [
                        'wish_y03_10'                   => [
                            '2_0_q'                     => ['2.0 Q'],
                            '2_0_q_limited'             => ['2.0 Q Limited'],
                            '2_0_q_limited_option'      => ['2.0 Q Limited Option'],
                            '2_0_q_sport_touring_ii'    => ['2.0 Q Sport Touring II'],
                            '2_0_q_sport_touring_iii'   => ['2.0 Q Sport Touring III'],
                            '2_0_s'                     => ['2.0 S'],
                            '2_0_st2'                   => ['2.0 ST2'],
                            '2_0_st3'                   => ['2.0 ST3']
                        ],
                    ],
                    'yaris'=> [
                        'yaris_y06_13'                  => [
                            '1_5_ace'                   => ['1.5 ACE'],
                            '1_5_e'                     => ['1.5 E'],
                            '1_5_e_limited'             => ['1.5 E Limited'],
                            '1_5_g'                     => ['1.5 G'],
                            '1_5_g_limited'             => ['1.5 G Limited'],
                            '1_5_j'                     => ['1.5 J'],
                            '1.5 rs'                    => ['1.5 RS'],
                            '1_5_s'                     => ['1.5 S'],
                            '1_5_s_limited'             => ['1.5 S Limited'],
                            '1_5_trd_sportivo'          => ['1.5 TRD Sportivo']
                        ],
                        'yaris_y13_17'                  => [
                            '1_2_e'                     => ['1.2 E'],
                            '1_2_g'                     => ['1.2 G'],
                            '1_2_j'                     => ['1.2 J'],
                            '1_2_j_eco'                 => ['1.2 J ECO'],
                            '1_2_trd_sportivo'          => ['1.2 TRD Sportivo']
                        ],
                        
                    ],

                    'yaris_ativ'=> [
                            'yaris_ativ_y17_21'             => [
                                '1_5_e'                     => ['1.5 E'],
                                '1_5_j'                     => ['1.5 J'],
                                '1_2_s'                     => ['1.2 S']
                            ],
                    ],
                ], //ปิด Toyota

                'isuzu' =>[
                    'adventure' =>[
                                'adventure_y96_00'          => [
                                    '2_5_4x4'               => ['2.5 4x4'],
                                    '2_8_4x2'               => ['2.8 4x2'],
                                    '2_8_4x4'               => ['2.8 4x4']
                            ],

                    ], 

                    'adventure_master' =>[
                                'amaster_y02_06'            => [
                                    '3_0_4x2'               => ['3.0 4x2'],
                                    '3_0_4x4'               => ['3.0 4x4']
                            ],
                    ],           

                    'cameo' =>[
                                'cameo_y91_97'              => [
                                    '2_5'                   => ['2.5'],
                            ],
                    ],

                    'd_max' =>[
                                'cab_4_y02_06'                  => [
                                    '2_5_hi_lander'             => ['2.5 Hi-Lander'],
                                    '2_5_sl'                    => ['2.5 SL'],
                                    '2_5_sl_ddi_i_teq'          => ['2.5 SL Ddi i-TEQ'],
                                    '2_5_slx_ddi_i_teq'         => ['2.5 SLX Ddi i-TEQ'],
                                    '2_5_sx'                    => ['2.5 SX'],
                                    '2_5_sx_ddi_i_teq'          => ['2.5 SX Ddi i-TEQ'],
                                    '3_0_hi_lander'             => ['3.0 Hi-Lander'],
                                    '3_0_hi_lander_ddi_i_teq'   => ['3.0 Hi-Lander Ddi i-TEQ'],
                                    '3_0_ls'                    => ['3.0 LS'],
                                    '3_0_ls_ddi_i_teq'          => ['3.0 LS Ddi i-TEQ'],
                                    '3_0_slx'                   => ['3.0 SLX'],
                                    '3_0_slx_ddi_i_teq'         => ['3.0 SLX Ddi i-TEQ']
                                ],

                                'cab_4_y07_11'                              => [
                                    '2_5_hi_lander'                         => ['2.5 Hi-Lander'],
                                    '2_5_hi_lander_ddi_i_teq'               => ['2.5 Hi-Lander Ddi i-TEQ'],
                                    '2_5_hi_lander_l'                       => ['2.5 Hi-Lander L'],
                                    '2_5_hi_lander_platinuml'               => ['2.5 Hi-Lander PlatinumL'],
                                    '2_5_hi_lander_super_platinuml'         => ['2.5 Hi-Lander Super PlatinumL'],
                                    '2_5_hi_lander_super_titanium'          => ['2.5 Hi-Lander Super Titanium'],
                                    '2_5_hi_lander_x_series'                => ['2.5 Hi-Lander X-Series'],
                                    '2_5_hi_lander_x_series_super_titanium' => ['2.5 Hi-Lander X-Series Super Titanium'],
                                    '2_5_sl_ddi_i_teq'                      => ['2.5 SL Ddi i-TEQ'],
                                    '2_5_slx'                               => ['2.5 SLX'],
                                    '2_5_slx_ddi_i_teq'                     => ['2.5 SLX Ddi i-TEQ'],
                                    '2_5_slx_super_platinum'                => ['2.5 SLX Super Platinum'],
                                    '2_5_slx_super_titanium'                => ['2.5 SLX Super Titanium'],
                                    '2_5_sx_ddi_i_teq'                      => ['2.5 SX Ddi i-TEQ'],
                                    '2_5_sx_platinum'                       => ['2.5 SX Platinum'],
                                    '2_5_sx_super_titanium'                 => ['2.5 SX Super Titanium'],
                                    '3_0_hi_lander'                         => ['3.0 Hi-Lander'],
                                    '3_0_hi_lander_ddi_i_teq'               => ['3.0 Hi-Lander Ddi i-TEQ'],
                                    '3_0_hi_lander_l'                       => ['3.0 Hi-Lander L'],
                                    '3_0_hi_lander_platinuml'               => ['3.0 Hi-Lander Platinum'],
                                    '3_0_hi_lander_super_platinuml'         => ['3.0 Hi-Lander Super Platinum'],
                                    '3_0_hi_lander_super_titanium'          => ['3.0 Hi-Lander Super Titanium'],
                                    '3_0_hi_lander_x_series'                => ['3.0 Hi-Lander X-Series'],
                                    '3_0_hi_lander_x_series_super_titanium' => ['3.0 Hi-Lander X-Series Super Titanium'],
                                    '3_0_ls_ddi_i-teq'                      => ['3.0 LS Ddi i-TEQ'],
                                    '3_0_ls_gt_ddi_i_teq'                   => ['3.0 LS GT Ddi i-TEQ'],
                                    '3_0_ls_super_platinum'                 => ['3.0 LS Super Platinum'],
                                    '3_0_ls_super_titanium'                 => ['3.0 LS Super Titanium'],
                                    '3_0_slx'                               => ['3.0 SLX'],
                                    '3_0_slx_ddi_i_teq'                     => ['3.0 SLX Ddi i-TEQ'],
                                ],


                                'cab_4_y11_17'                              => [
                                    '1_9_hi_lander_l'                       => ['1.9 Hi-Lander L'],
                                    '1_9_hi_lander_z'                       => ['1.9 Hi-Lander Z'],
                                    '1_9_hi_lander_z_prestige'              => ['1.9 Hi-Lander Z-Prestige'],
                                    '1_9_s'                                 => ['1.9 S'],
                                    '1_9_z'                                 => ['1.9 Z'],
                                    '2_5_hi_lander_l'                       => ['2.5 Hi-Lander L'],
                                    '2_5_hi_lander_x_series'                => ['2.5 Hi-Lander X-Series'],
                                    '2_5_hi_lander_x_series_z'              => ['2.5 Hi-Lander X-Series Z'],
                                    '2_5_hi_lander_x_series_z_prestige'     => ['2.5 Hi-Lander X-Series Z Prestige'],
                                    '2_5_hi_lander_z'                       => ['2.5 Hi-Lander Z'],
                                    '2_5_hi_lander_z_prestige_ddi_vgs_turbo'=> ['2.5 Hi-Lander Z Prestige Ddi VGS Turbo'],
                                    '2_5_hi_lander_z_prestige'              => ['2.5 Hi-Lander Z-Prestige'],
                                    '2_5_l'                                 => ['2.5 L'],
                                    '2_5_s'                                 => ['2.5 S'],
                                    '2_5_vcross_l'                          => ['2.5 Vcross L'],
                                    '2_5_vcross_z'                          => ['2.5 Vcross Z'],
                                    '2_5_z'                                 => ['2.5 Z'],
                                    '3_0_hi_lander_z_prestige_ddi_vgs_turbo'=> ['3.0 Hi-Lander Z Prestige Ddi VGS Turbo'],
                                    '3_0_hi_lander_z_prestige'              => ['3.0 Hi-Lander Z-Prestige'],
                                    '3_0_vcross_max_z_prestige'             => ['3.0 Vcross MAX Z-Prestige'],
                                    '3_0_vcross_z_prestige'                 => ['3.0 Vcross Z-Prestige']
                                ],


                                'spacecab_y02_06'                           => [
                                    '2_5_hi_lander_ddi_i_teq'               => ['2.5 Hi-Lander Ddi i-TEQ'],
                                    '2_5_sl'                                => ['2.5 SL'],
                                    '2_5_slx'                               => ['2.5 SLX'],
                                    '2_5_slx_ddi_i_teq'                     => ['2.5 SLX Ddi i-TEQ'],
                                    '2_5_sx'                                => ['2.5 SX'],
                                    '2_5_sx_ddi_i_teq'                      => ['2.5 SX Ddi i-TEQ'],
                                    '3_0_hi_lander'                         => ['3.0 Hi-Lander'],
                                    '3_0_hi_lander_ddi_i_teq'               => ['3.0 Hi-Lander Ddi i-TEQ'],
                                    '3_0_rodeo_ls'                          => ['3.0 Rodeo LS'],
                                    '3_0_rodeo_ls_ddi_i-teq'                => ['3.0 Rodeo LS Ddi i-TEQ'],
                                    '3_0_rodeo_s'                           => ['3.0 Rodeo S'],
                                    '3_0_rodeo_s_ddi_i-teq'                 => ['3.0 Rodeo S Ddi i-TEQ'],
                                    '3_0_slx'                               => ['3.0 SLX'],
                                    '3_0_slx_ddi_i-teq'                     => ['3.0 SLX Ddi i-TEQ'],
                                ],


                                'spacecab_y07_11'                           => [
                                    '2_5_hi_lander'                         => ['2.5 Hi-Lander'],
                                    '2_5_hi_lander_ddi_i_teq'               => ['2.5 Hi-Lander Ddi i-TEQ'],
                                    '2_5_hi_lander_l'                       => ['2.5 Hi-Lander L'],
                                    '2_5_hi_lander_platinum'                => ['2.5 Hi-Lander Platinum'],
                                    '2_5_hi_lander_super_platinum_smart'    => ['2.5 Hi-Lander Platinum Smart'],
                                    '2_5_hi_lander_super_platinum'          => ['2.5 Hi-Lander Super Platinum'],
                                    '2_5_hi_lander_super_titanium'          => ['2.5 Hi-Lander Super Titanium'],
                                    '2_5_hi_lander_super_titanium_smart'    => ['2.5 Hi-Lander Super Titanium Smart'],
                                    '2_5_hi_lander_x_series'                => ['2.5 Hi-Lander X-Series'],
                                    '2_5_hi_lander_x_series_super_titanium' => ['2.5 Hi-Lander X-Series Super Titanium'],
                                    '2_5_rodeo_ls_ddi_i_teq'                => ['2.5 Rodeo LS Ddi i-TEQ'],
                                    '2_5_rodeo_ls_super_titanium'           => ['2.5 Rodeo LS Super Titanium'],
                                    '2_5_sl_ddi_i_teq'                      => ['2.5 SL Ddi i-TEQ'],
                                    '2_5_slx'                               => ['2.5 SLX'],
                                    '2_5_slx_ddi_i_teq'                     => ['2.5 SLX Ddi i-TEQ'],
                                    '2_5_slx_platinum'                      => ['2.5 SLX Platinum'],
                                    '2_5_slx_platinum_smart'                => ['2.5 SLX Platinum Smart'],
                                    '2_5_slx_speed_ddi_i_teq'               => ['2.5 SLX Speed Ddi i-TEQ'],
                                    '2_5_slx_super_platinum'                => ['2.5 SLX Super Platinum'],
                                    '2_5_slx_super_titanium'                => ['2.5 SLX Super Platinum Smart'],
                                    '2_5_slx_super_titanium_smart'          => ['2.5 SLX Super Titanium Smart'],
                                    '2_5_slx_x_series'                      => ['2.5 SLX X-Series'],
                                    '2_5_slx_x_series_super_titanium'       => ['2.5 SLX X-Series Super Titanium'],
                                    '2_5_sx'                                => ['2.5 SX'],
                                    '2_5_sx_ddi_i_teq'                      => ['2.5 SX Ddi i-TEQ'],
                                    '2_5_sx_platinum'                       => ['2.5 SX Platinum'],
                                    '2_5_slx_super_platinum'                => ['2.5 SLX Super Platinum'],
                                    '2_5_sx_super_titanium'                 => ['2.5 SX Super Titanium'],
                                    '2_5_x_series'                          => ['2.5 X-Series'],
                                    '3_0_hi_lander'                         => ['3.0 Hi-Lander'],
                                    '3_0_hi_lander_ddi_i_teq'               => ['3.0 Hi-Lander Ddi i-TEQ'],
                                    '3_0_hi_lander_platinuml'               => ['3.0 Hi-Lander Platinum'],
                                    '3_0_hi_lander_super_platinuml'         => ['3.0 Hi-Lander Super Platinum'],
                                    '3_0_hi_lander_super_titanium'          => ['3.0 Hi-Lander Super Titanium'],
                                    '3_0_hi_lander_x_series'                => ['3.0 Hi-Lander X-Series'],
                                    '3_0_rodeo_ls'                          => ['3.0 Rodeo LS'],
                                    '3_0_rodeo_ls_ddi_i-teq'                => ['3.0 Rodeo LS Ddi i-TEQ'],
                                    '3_0_rodeo_ls_super_platinum'           => ['3.0 Rodeo LS Super Platinum'],
                                    '3_0_rodeo_ls_super_titanium'           => ['3.0 Rodeo LS Super Titanium'],
                                    '3_0_rodeo_ls_x-series_super_titanium'  => ['3.0 Rodeo LS X-Series Super Titanium'],
                                    '3_0_slx'                               => ['3.0 SLX'],
                                    '3_0_slx_ddi_i_teq'                     => ['3.0 SLX Ddi i-TEQ'],
                                    '3_0_slx_platinum'                      => ['3.0 SLX Platinum'],
                                    '3_0_slx_super_platinum'                => ['3.0 SLX Super Platinum'],
                                    '3_0_slx_super_titanium'                => ['3.0 SLX Super Titanium'],
                                ],


                                'spacecab_y11_17'                               => [
                                    '1_9_hi_lander_l'                           => ['1.9 Hi-Lander L'],
                                    '1_9_hi_lander_x_series_z'                  => ['1.9 Hi-Lander X-Series Z'],
                                    '1_9_hi_lander_z'                           => ['1.9 Hi-Lander Z'],
                                    '1_9_l'                                     => ['1.9 L'],
                                    '1_9_s'                                     => ['1.9 S'],
                                    '1_9_x_series_z'                            => ['1.9 X-Series Z'],
                                    '1_9_z'                                     => ['1.9 Z'],
                                    '2_5_hi_lander_99_th_anniversary_edition'   => ['2.5 Hi-Lander 99 th anniversary Edition'],
                                    '2_5_hi_lander l'                           => ['2.5 Hi-Lander L'],
                                    '2_5_hi_lander_x_series'                    => ['2.5 Hi-Lander X-Series'],
                                    '2_5_hi_lander_x_series_z'                  => ['2.5 Hi-Lander X-Series Z'],
                                    '2_5_hi_lander_z'                           => ['2.5 Hi-Lander Z'],
                                    '2_5_hi_lander_z_prestige_ddi_vgs_turbo'    => ['2.5 Hi-Lander Z Prestige Ddi VGS Turbo'],
                                    '2_5_hi_lander_z_prestige'                  => ['2.5 Hi-Lander Z-Prestige'],
                                    '2_5_l'                                     => ['2.5 L'],
                                    '2_5_s'                                     => ['2.5 S'],
                                    '2_5_vcross_l'                              => ['2.5 Vcross L'],
                                    '2_5_vcross_z'                              => ['2.5 Vcross Z'],
                                    '2_5_x_series'                              => ['2.5 X-Series'],
                                    '2_5_z'                                     => ['2.5 Z'],
                                    '2_5_z_prestige_ddi_vgs_turbo'              => ['2.5 Z Prestige Ddi VGS Turbo'],
                                    '3_0_hi_lander_z_prestige_ddi_vgs_turbo'    => ['3.0 Hi-Lander Z Prestige Ddi VGS Turbo'],
                                    '3_0_hi_lander_z_prestige'                  => ['3.0 Hi-Lander Z-Prestige'],
                                    '3_0_vcross_z'                              => ['3.0 Vcross Z'],
                                    '3_0_vcross_z_prestige'                     => ['3.0 Vcross Z-Prestige'],

                                ],


                                'spacecab_y1117'                                => [
                                    '1_9_hi_lander_z'                           => ['1.9 Hi-Lander Z'],
                                ],


                                'spark_y02_96'                                  => [
                                    '2_5_ex'                                    => ['2.5 EX'],
                                    '2_5_ex_ddi_i_teq'                          => ['2.5 EX Ddi i-TEQ'],
                                    '3_0_ex'                                    => ['3.0 EX'],
                                ],

                                'spark_y07_11'                                  => [
                                    '2_5_ex'                                    => ['2.5 EX'],
                                    '2_5_ex_ddi_i_teq'                          => ['2.5 EX Ddi i-TEQ'],
                                    '2_5_ex_platinum'                           => ['2.5 EX Platinum'],
                                    '2_5_ex_super_platinum'                     => ['2.5 EX Super Platinum'],
                                    '2_5_ex_super_titanium'                     => ['2.5 EX Super Titanium'],
                                    '2_5_exl_platinum'                          => ['2.5 EXL Platinum'],
                                ],

                                'spark_y11_17'                                  => [
                                    '1_9_b'                                     => ['1.9 B'],
                                    '1_9_s'                                     => ['1.9 S'],
                                    '2_5_b'                                     => ['2.5 B'],
                                    '2_5_spark_vgs_s'                           => ['2.5 SPARK VGS S'],
                                    '3_0_b'                                     => ['3.0 B'],
                                    '3_0_s'                                     => ['3.0 S'],
                                ],
                    ],



                    'dragon_eyes' =>[
                                'dragon_eyes_spacecab'                      => [
                                    '2_5_slx'                               => ['2.5 SLX'],
                                    '2_8_rodeo'                             => ['2.8 Rodeo'],
                                    '2_8_slx'                               => ['2.8 SLX'],
                                    '2_8_sx'                                => ['2.8 SX'],
                                ],

                                'spacecab_y96_99'                           => [
                                    '2_5_slx'                               => ['2.5 SLX'],
                                    '2_5_sx'                                => ['2.5 SX'],
                                    '2_8_rodeo'                             => ['2.8 Rodeo'],
                                    '2_8_slx'                               => ['2.8 SLX'],
                                ],

                                'spark_y96_99'                              => [
                                    '2_5_exy_std'                           => ['2.5 EXY STD'],
                                ],

                    ],


                    'dragon_power' =>[
                                'dragonpower_cab4'                          => [
                                    '2_5_sx'                                => ['2.5 SX'],
                                ],

                                'cab4_y00_02'                               => [
                                    '3_0_rodeo'                             => ['3.0 Rodeo'],
                                    '3_0_slx'                               => ['3.0 SLX'],
                                    '3_0_slx_ltd'                           => ['3.0 SLX LTD'],
                                ],

                                'dragonpower_spacecab'                      => [
                                    '2_8_rodeo_ls'                          => ['2.8 Rodeo LS'],
                                    '2_8_slx'                               => ['2.8 SLX'],
                                ],

                                'spacecab_y00_02'                           => [
                                    '2_5_sl'                                => ['2.5 SL'],
                                    '2_5_slx'                               => ['2.5 SLX'],
                                    '2_5_sx'                                => ['2.5 SX'],
                                    '3_0_slx'                               => ['3.0 SLX'],
                                ],


                                'spark_y00_02'                              => [
                                    '2_5_exy'                               => ['2.5 EXY'],
                                ],

                    ],



                    'elf' =>[
                                'elf_all'                                   => [
                                    '2_8'                                   => ['2.8'],
                                    '2_8_nkr'                               => ['2.8 NKR'],
                                    '3_0_nlr'                               => ['3.0 NLR'],
                                ],

                    ],


                    'grand_adventure' =>[
                                'g_adventure_y_96_02'                       => [
                                    '2_8_4x4'                               => ['2.8 4x4'],
                                    '3_0_4x2'                               => ['3.0 4x2'],
                                    '3_0_4x4'                               => ['3.0 4x4'],
                                ],

                    ],

                    'hr' =>[
                                'hr_all'                                    => [
                                    '1_9'                                   => ['1.9Ddi'],
                                ],

                    ],

                    'mu_7' =>[
                                'mu_7_y04_06'                               => [
                                    '3_0'                                   => ['3.0'],
                                ],

                                'mu_7_y07_13'                               => [
                                    '3_0_activo'                            => ['3.0 Activo'],
                                    '3_0_activo_super_platinum'             => ['3.0 Activo Super Platinum'],
                                    '3_0_activo_super_titanium'             => ['3.0 Activo Super Titanium (8)'],
                                    '3_0_choiz'                             => ['3.0 CHOIZ'],
                                    '3_0_primo'                             => ['3.0 Primo'],
                                    '3_0_primo_platinum'                    => ['3.0 Primo Platinum'],
                                    '3_0 primo_super_platinum'              => ['3.0 Primo Super Platinum (17)'],
                                    '3_0_primo_super_titanium'              => ['3.0 Primo Super Titanium (35)'],
                                ],
                    ],



                    'mu_x' =>[
                                'mu_x_y13_17'                               => [
                                    '1_9'                                   => ['1.9'],
                                    '2_5'                                   => ['2.5'],
                                    '3_0'                                   => ['3.0'],
                                ],

                                'mu_x_1317'                                 => [
                                    '1_9'                                   => ['1.9'],
                                    '3_0'                                   => ['3.0'],
                                ],
                    ],


                    'trf' =>[
                                'dragon_gold'                               => [
                                    '2_5_sl'                                => ['2.5 SL'],
                                    '2_5_space_cab'                         => ['2.5 Space Cab'],
                                    '2_5_station_wagon'                     => ['2.5 Station Wagon'],
                                    '2_5_supreme'                           => ['2.5 Supreme'],
                                ],
                    ],


                    'trooper' =>[
                                'trooper_y91_03'                            => [
                                    '3_2_se'                                => ['3.2 SE'],
                                    '3_2_xs'                                => ['3.2 XS'],
                                ],
                    ],


                    'vega' =>[
                                'vega_y97_03'                               => [
                                    '2_8'                                   => ['2.8'],
                                    '3_0'                                   => ['3.0'],
                                ],
                    ],
                ], //ปิด isuzu

                'nissan' => [
                    '200sx'  =>[
                                '200sx_y92_95'          => [
                                      '1_8'             => ['1.8'],
                                ],
                    ],

                    '300zx'  =>[
                                '300zx_y92_98'          => [
                                      '3_0'             => ['3.0'],
                                ],
                    ],

                    '350z'  =>[
                                '350z_y03_09'           => [
                                      '3_5'             => ['3.5'],
                                      '3_5_v6'          => ['3.5 V6'],
                                      '3_5_z33'         => ['3.5 Z33'],
                                ],
                    ],

                    '370z'  =>[
                                '300zx_y09_15'          => [
                                      '3_7'             => ['3.7'],
                                ],
                    ],

                    'almera'  =>[
                                'almera_y11_16'          => [
                                      '1_2_e'            => ['1.2 E'],
                                      '1_2_e_nismo'      => ['1.2 E Nismo'],
                                      '1.2_e_sportech'   => ['1.2 E SPORTECH'],
                                      '1_2_el'           => ['1.2 EL'],
                                      '1_2_es'           => ['1.2 ES'],
                                      '1_2_v'            => ['1.2 V'],
                                      '1_2_v_sportech'   => ['1.2 V SPORTECH'],
                                      '1_2_vl'           => ['1.2 VL'],
                                      '1_2_nismo'        => ['1.2 Nismo'],
                                      '1_2_vl_sportech'  => ['1.2 VL SPORTECH'],
                                ],
                    ],

                    'bigm'  =>[
                                'bigm_4dr'              => [
                                      '2_0'             => ['2.0'],
                                ],

                                'bigm_kingcab'          => [
                                      '2_5_super_dx'    => ['2.5 Super DX'],
                                ],

                                'bigm_single'           => [
                                      '1_6_super_gl'    => ['1.6 Super GL'],
                                      '2_5_super_dx'    => ['2.5 Super DX'],
                                      '2_7'             => ['2.7'],
                                ],
                    ],

                    'bluebird_atessa'  =>[
                                'bluebird_a_y93_97'     => [
                                      '2_0_sss_g'       => ['2.0 SSS-G'],
                                ],
                    ],

                    'cedric'  =>[
                                'cedric_y92_95'         => [
                                      '2_0_classic'     => ['2.0 Classic'],
                                      '3_0_gran_turismo'=> ['3.0 Gran Turismo'],

                                ],
                    ],

                    'cefiro'  =>[
                                'A31'                   => [
                                      '2_0'             => ['2.0'],
                                      '2_0_12v'         => ['2.0 12V'],
                                ],

                                'A32'          => [
                                      '2_0_20g'         => ['2.0 20G'],
                                      '2_0_vip'         => ['2.0 VIP'],
                                      '2_0_vq20'        => ['2.0 VQ20'],
                                      '3_0_vip'         => ['3.0 VIP'],
                                ],

                                'A33'          => [
                                      '2_0_excimo'      => ['2.0 Excimo'],
                                      '2_0_executive'   => ['2.0 Executive'],
                                      '3_0_brougham'    => ['3.0 Brougham'],
                                ],
                    ],


                    'cube'  =>[
                                'cube_y02_08'          => [
                                      '1_4_z11'        => ['1.4 Z11'],
                                      '1_4_z11_e_4wd'  => ['1.4 Z11 e-4WD'],
                                      '1_5_z11'        => ['1.5 Z11'],
                                ],

                                'cube_y02_15'          => [
                                      '1_5_z12'        => ['1.5 Z12'],
                                ],
                    ],


                    'elgrand'  =>[
                                'elgrand_y02_10'            => [
                                      '2_5_rider'           => ['2.5 Rider'],
                                ],

                                'elgrand_y10_15'            => [
                                      '2.5 high-way star'   => ['2.5 High-Way Star'],
                                ],
                                'elgrand_97_02'             => [
                                      '3_0_di'              => ['3.0 Di'],
                                      '3_3_v'               => ['3.3 V'],
                                      '3_5_v6'              => ['3.5 v6'],
                                ],
                    ],


                    'figaro'  =>[
                                'figaro_y91_95'             => [
                                      '1_0_turbo'           => ['1.0 Turbo '],
                                ],
                    ],


                    'frontier'  =>[
                                'frontier_4dr'              => [
                                      '2_5'                 => ['2.5'],
                                      '2_5_ax_l'            => ['2.5 AX-L'],
                                      '2_7_tl'              => ['2.7 TL'],
                                      '3_0_zdi'             => ['3.0 ZDi'],
                                      '3_0_zdi_t'           => ['3.0 ZDi-T'],
                                ],

                                'frontier_kingcab'          => [
                                      '2_5_al'              => ['2.5 AL'],
                                      '2_5_ax'              => ['2.5 AX'],
                                      '2_5_ax_super'        => ['2.5 AX Super'],
                                      '2_5_ax_l'            => ['2.5 AX-L'],
                                      '2_5_yd'              => ['2.5 YD'],
                                      '2_7_tl'              => ['2.7 TL'],
                                      '2_7_txp'             => ['2.7 TXP'],
                                      '3_0_zdi'             => ['3.0 ZDi '],
                                      '3_0_zdi_t'           => ['3.0 ZDi-T'],
                                ],

                                'frontier_single'           => [
                                      '2_5_ae'              => ['2.5 AE'],
                                      '2_5_aep'             => ['2.5 AEP'],
                                      '2_5_sx'              => ['2.5 SX'],
                                      '2_7_tx'              => ['2.7 TX'],
                                      '2_7_txp'             => ['2.7 TXP'],
                                ],
                    ],

                    'frontier_nacara'  =>[
                                'f_nacara_4dr'                          => [
                                      '2_5_black_star_limited'          => ['2.5 Black Star Limited'],
                                      '2_5_calibre'                     => ['2.5 Calibre'],
                                      '2_5_calibre_le'                  => ['2.5 Calibre LE'],
                                      '2_5_calibre_le_grand_titanium'   => ['2.5 CALIBRE LE Grand Titanium'],
                                      '2_5_calibre_se'                  => ['2.5 Calibre SE'],
                                      '2_5_calibre_sport_version'       => ['2.5 Calibre Sport Version'],
                                      '2_5_gt'                          => ['2.5 GT'],
                                      '2_5_le'                          => ['2.5 LE'],
                                      '2_5_le_calibre'                  => ['2.5 LE Calibre'],
                                      '2_5_se'                          => ['2.5 SE'],
                                      '2_5_sv_calibre_le'               => ['2.5 SV Calibre LE'],
                                      '2_5_sv_calibre_se'               => ['2.5 SV Calibre SE'],
                                      '2_5_sv_le'                       => ['2.5 SV LE'],
                                ],

                                'f_nacara_kingcab'                      => [
                                      '2_5_calibre'                     => ['2.5 Calibre'],
                                      '2_5_calibre_le'                  => ['2.5 Calibre LE'],
                                      '2_5_calibre_le_grand_titanium'   => ['2.5 Calibre LE Grand Titanium'],
                                      '2_5_calibre_se'                  => ['2.5 Calibre SE'],
                                      '2_5_calibre_sport_version'       => ['2.5 Calibre Sport Version'],
                                      '2_5_gt_calibre_le'               => ['2.5 GT Calibre LE'],
                                      '2_5_le'                          => ['2.5 LE'],
                                      '2_5_le_calibre'                  => ['2.5 LE Calibre'],
                                      '2_5_le_calibre_gt'               => ['2.5 LE Calibre GT'],
                                      '2_5_se'                          => ['2.5 SE'],
                                      '2_5_se_cng'                      => ['2.5 SE CNG'],
                                      '2_5_sv'                          => ['2.5 SV'],
                                      '2_5_sv_calibre'                  => ['2.5 SV Calibre'],
                                      '2_5_sv_le'                       => ['2.5 SV LE'],
                                ],


                                'f_nacara_single'                       => [
                                      '2_5_xe'                          => ['2.5 XE'],
                                      '2_5_xe_cng'                      => ['2.5 XE CNG'],
                                ],
                    ],



                    'gt_r'  =>[
                                'gt_r_y08_15'                           => [
                                      '3_8_r35'                         => ['3.8 R35'],
                                ],
                    ],

                    'infiniti_r'  =>[
                                'gt_r_y08_15'                           => [
                                      '4_1'                             => ['4.1'],
                                ],
                    ],

                    'juke'  =>[
                                'juke_y10_16'                           => [
                                      '1_5_jp'                          => ['1.5 JP'],
                                      '1_6_e'                           => ['1.6 E'],
                                      '1_6_invader'                     => ['1.6 Invader'],
                                      '1_6_tokyo_edition'               => ['1.6 Tokyo Edition'],
                                      '1_6_turbo'                       => ['1.6 Turbo'],
                                      '1_6_v'                           => ['1.6 V'],
                                ],
                    ],

                    
                    'leaf'  =>[
                                'leaf_y12_16'                           => [
                                      '1_5_jp'                          => ['1.5 JP'],
                                      '1_6_e'                           => ['1.6 E'],
                                      '1_6_invader'                     => ['1.6 Invader'],
                                      '1_6_tokyo_edition'               => ['1.6 Tokyo Edition'],
                                      '1_6_turbo'                       => ['1.6 Turbo'],
                                      '1_6_v'                           => ['1.6 V'],
                                ],
                    ],

                    'livina'  =>[
                                'livina_y14_17'                         => [
                                      '1_6_e'                           => ['1.6 E'],
                                      '1_6_v'                           => ['1.6 V'],
                                ],
                    ],

                    'march'  =>[
                                'march_y10_16'                          => [
                                      '1_2_e'                           => ['1.2 E'],
                                      '1_2_e_limited_edition'           => ['1.2 E Limited Edition'],
                                      '1_2_e_smart_edition'             => ['1.2 E Smart Edition'],
                                      '1_2_el'                          => ['1.2 EL'],
                                      '1_2_el_limited_edition'          => ['1.2 EL Limited Edition'],
                                      '1_2_s'                           => ['1.2 EL Sport Version'],
                                      '1_2_e_limited_edition'           => ['1.2 S'],
                                      '1_2_sport_version'               => ['1.2 Sport Version'],
                                      '1_2_v'                           => ['1.2 V'],
                                      '1.2 vl'                          => ['1.2 VL'],
                                      '1_2_vl_sport_version'            => ['1.2 VL Sport Version'],

                                ],
                    ],

                    'murano'  =>[
                                'murano_y04_10'                         => [
                                      '2_5'                             => ['2.5'],
                                      '3_5'                             => ['3.5'],
                                ],
                    ],


                    'note'  =>[
                                'note_y17_21'                           => [
                                      '1_2_v'                           => ['1.2 V'],
                                      '1_2_vl'                          => ['1.2 VL'],
                                ],
                    ],

                    'np_300_navara'  =>[
                                'np300_dbcab'                           => [
                                      '2_5_calibre_e'                   => ['2.5 Calibre E'],
                                      '2_5_calibre_el'                  => ['2.5 Calibre EL'],
                                      '2_5_calibre_sportech'            => ['2.5 Calibre Sportech'],
                                      '2_5_calibre_v'                   => ['2.5 Calibre V'],
                                      '2_5_calibre_vl'                  => ['2.5 Calibre VL'],
                                      '2_5_e'                           => ['2.5 E'],
                                      '2_5_s'                           => ['2.5 S'],
                                      '2_5_vl'                          => ['2.5 VL'],
                                ],

                                'np_300_kcab'                           => [
                                      '2_5_calibre_e'                   => ['2.5 Calibre E'],
                                      '2_5_calibre_el'                  => ['2.5 Calibre EL'],
                                      '2_5_calibre_s'                   => ['2.5 Calibre S'],
                                      '2_5_calibre_sportech'            => ['2.5 Calibre Sportech'],
                                      '2_5_calibre_v'                   => ['2.5 Calibre V'],
                                      '2_5_e'                           => ['2.5 E'],
                                      '2_5_el_calibre'                  => ['2.5 EL Calibre'],
                                      '2_5_s'                           => ['2.5 S'],
                                      '2_5_v'                           => ['2.5 V'],
                                ],

                                'np_300_single'                         => [
                                      '2_5_s'                           => ['2.5 S'],
                                      '2_5_sl'                          => ['2.5 Calibre SL'],
                                ],
                    ],

                    'nv'  =>[
                                'nv_queencab'                           => [
                                      '1_6_queen_cab'                   => ['1.6 Queen Cab'],
                                      '1_6_queen_cab_lxt'               => ['1.6 Queen Cab LXT'],
                                      '1_6_queen_cab_slx'               => ['1.6 Queen Cab SLX'],
                                      '1_6_slx'                         => ['1.6 SLX'],
                                ],

                                'nv_single'                             => [
                                      '1_6_slx'                         => ['1.6 SLX'],
                                ],

                                'nv_wingroad'                           => [
                                      '1_6_slx'                         => ['1.6 SLX'],
                                      '1_6_wing_road_slx'               => ['1.6 Wing Road SLX'],
                                ],
                    ],

                    'nx'  =>[
                                'nx_y92_95'                             => [
                                      '1_6'                             => ['1.6'],
                                ],
                    ],

                    'pao'  =>[
                                'pao_y89_92'                            => [
                                      '1_0'                             => ['1.0'],
                                ],
                    ],

                    'presea'  =>[
                                'presea_y92_97'                         => [
                                      '1_8'                             => ['1.8'],
                                ],
                    ],

                    'president'  =>[
                                'president_y93_95'                      => [
                                      '4_5'                             => ['4.5'],
                                ],
                    ],

                    'primera'  =>[
                                'primera_y97_99'                        => [
                                      '2_0'                             => ['2.0'],
                                ],
                    ],

                    'pulsar'  =>[
                                'pulsar_y12_16'                         => [
                                      '1_6_s'                           => ['1.6 S'],
                                      '1_6_sv'                          => ['1.6 SV'],
                                      '1_6_v'                           => ['1.6 V'],
                                      '1_8_v'                           => ['1.8 V'],
                                ],
                    ],

                    'sentra'  =>[
                                'sentra_y92_96'                         => [
                                      '1_6_ex_saloon'                   => ['1.6 EX Saloon'],
                                ],
                    ],

                    'serena'  =>[
                                'serena_y00_05'                         => [
                                      '2_0_high_way_star'               => ['2.0 High-Way Star'],
                                ],
                    ],

                    'silvia'  =>[
                                'silvia_y00_05'                         => [
                                      '2_0_s15'                         => ['2.0 S15'],
                                      '2_0_spec_r'                      => ['2.0 Spec-R'],
                                ],
                    ],

                    'skyline'  =>[
                                'skyline_r33'                           => [
                                      '2_6_gt_r'                        => ['2.6 GT-R'],
                                ],
                                'skyline_v35'                           => [
                                      '3_5_350gt'                       => ['3.5 350GT'],
                                ],
                                'skyline_v36'                           => [
                                      '3_7_370gt'                       => ['3.7 370GT'],
                                ],
                    ],

                    'sunny'  =>[
                                'b14_15'                                => [
                                      '1_5_ex_saloon'                   => ['1.5 EX Saloon'],
                                      '1_6_super_gl_saloon'             => ['1.6 Super GL Saloon'],
                                      '1_6_super_saloon'                => ['1.6 Super Saloon'],
                                ],
                                'sunny_neo'                             => [
                                      '1_6_gl_neo'                      => ['1.6 GL Neo'],
                                      '1_6_super_gl'                    => ['1.6 Super GL'],
                                      '1_6_super_neo'                   => ['1.6 Super Neo'],
                                      '1_8_almera'                      => ['1.8 Almera'],
                                      '1_8_almera_young'                => ['1.8 Almera Young'],
                                      '1_8_vip_neo'                     => ['1.8 VIP Neo'],
                                ],
                                'sunny_neo_y04_06'                      => [
                                      '1_6_gl'                          => ['1.6 GL'],
                                      '1_6_gl_neo'                      => ['1.6 GL Neo'],
                                      '1_6_super_neo'                   => ['1.6 Super Neo'],
                                      '1_8_super_neo'                   => ['1.8 Super Neo'],
                                      '1_8_vip_neo'                     => ['1.8 VIP Neo'],
                                ],
                    ],

                    'sylphy'  =>[
                                'sylphy_y12_16'                         => [
                                      '1_6_e'                           => ['1.6 E'],
                                      '1_6_e_cng'                       => ['1.6 E CNG'],
                                      '1_6_s'                           => ['1.6 S'],
                                      '1_6_sv'                          => ['1.6 SV'],
                                      '1_6_v'                           => ['1.6 V'],
                                      '1_8_V'                           => ['1.8 V'],
                                ],
                    ],

                    'teana'  =>[
                                'teana_y04_08'                         => [
                                      '2_0_200_jk'                     => ['2.0 200 JK'],
                                      '2_0_200jk'                      => ['2.0 200JK'],
                                      '2_3_230_jk'                     => ['2.3 230 JK'],
                                      '2_3_230_jm'                     => ['2.3 230 JM'],
                                      '2_3_230_js'                     => ['2.3 230 JS'],
                                      '2_3_230jm'                      => ['2.3 230JM'],
                                      '2_3_230js'                      => ['2.3 230JS'],
                                ],

                                'teana_y09_13'                         => [
                                      '2_0_200_xl'                     => ['2.0 200 XL'],
                                      '2_0_200_xl_sport'               => ['2.0 200 XL Sport'],
                                      '2_0_200_xl_sports_series_navi'  => ['2.0 200 XL Sports Series Navi'],
                                      '2_5_250_xv'                     => ['2.5 250 XV'],
                                      '2_5_250_xv_sport'               => ['2.5 250 XV Sport'],
                                      '2_5_250_xv_sports_series_navi'  => ['2.5 250 XV Sports Series Navi'],
                                ],

                                'teana_13_16'                         => [
                                      '2_0_xe'                        => ['2.0 XE'],
                                      '2_0_xl'                        => ['2.0 XL'],
                                      '2_5_xv'                        => ['2.5 XV'],
                                ],
                    ],

                    'terrano_ii'  =>[
                                'terrano_ii_y95_00'                   => [
                                      '2_4'                           => ['2.4'],
                                ],
                    ],


                    'tiida'  =>[
                                'tiida_y06_12'                        => [
                                      '2_4'                           => ['2.4'],
                                ],
                    ],

                    'urvan'  =>[
                                'urvan_y01_12'                        => [
                                      '3_0_gx'                        => ['3.0 GX'],
                                ],

                                'urvan_y13_17'                        => [
                                      '2_5_nv350'                     => ['2.5 NV350'],
                                      '2_5_nv350_cng'                 => ['2.5 NV350 CNG'],
                                ],


                                'urvan_y13_17'                        => [
                                      '2_5_nv350'                     => ['2.5 NV350'],
                                      '2_5_nv350_cng'                 => ['2.5 NV350 CNG'],
                                ],
                    ],

                    'x-trail'  =>[
                                'x-trail_y02_08'                      => [
                                      '2_5_comfort'                   => ['2.5 Comfort'],
                                      '2_5_luxury'                    => ['2.5 Luxury'],
                                ],

                                'x-trail_y08_13'                      => [
                                      '2_0'                           => ['2.0'],
                                      '2_0_v'                         => ['2.0 V'],
                                ],


                                'x-trail_y14_17'                      => [
                                      '2_0_e'                         => ['2.0 E'],
                                      '2_0_vs'                        => ['2.0 V'],
                                      '2_5_v'                         => ['2.5 V'],
                                ],
                                'x-trail_15_19'                       => [
                                      '2_0_e'                         => ['2.0 E'],
                                      '2_0_e_hybrid'                  => ['2.0 E Hybrid'],
                                      '2_0_v'                         => ['2.0 V'],
                                      '2_0_v_hybrid'                  => ['2.0 V Hybrid'],
                                ],
                    ],

                    'xciter'  =>[
                                'xciter_y01_06'                       => [
                                      '3_0_super_gl'                  => ['3.0 Super GL'],
                                ],
                    ],
                ],//ปิด nissan






                'mitsubishi' => [
                    '300gt'  =>[
                                '300gt_y92_97'                      => [
                                      '3_0'                         => ['3.0'],
                                ],
                    ],

                    'attrage'  =>[
                                'attrage_y13_16'                    => [
                                      '1_2_gls'                     => ['1.2 GLS'],
                                      '1_2_gls_limited'             => ['1.2 GLS Limited'],
                                      '1_2_gls_ltd'                 => ['1.2 GLS LTD'],
                                      '1_2_glx'                     => ['1.2 GLX'],
                                ],
                    ],

                        'canter'  =>[
                                    'all_canter'                    => [
                                      '2_8'                         => ['2.8'],
                                ],
                    ],

                    'cyblone'  =>[
                                'aero_body_y09_05'                  => [
                                      '2_5_aero_body'               => ['2.5 Aero body'],
                                ],
                                'cyblone_single'                    => [
                                      '2_5'                         => ['2.5'],
                                ],
                    ],

                    'delica_space_wagon'  =>[
                                'aero_body_y15_18'                  => [
                                      '2_0'                         => ['2.0'],
                                ],
                    ],

                        'evolution'  =>[
                                    'iv_face_benz'                  => [
                                      '2_0_iv'                      => ['2.0 IV'],
                                ],

                                'ix_lancer'                         => [
                                      '2_0_v'                       => ['2.0 V'],
                                ],

                                'v_end_benz'                        => [
                                      '2_0_v'                       => ['2.0 V'],
                                ],

                                'vi_end_benz'                       => [
                                      '2_0_vi'                      => ['2.0 VI'],
                                ],


                                'vii_Cedia'                         => [
                                      '2_0_vii'                     => ['2.0 VII'],
                                ],

                                'viii_lancer'                       => [
                                      '2_0_viii'                    => ['2.0 VIII'],
                                ],

                                'x_lancer_ex'                       => [
                                      '2_0_x'                       => ['2.0 X'],
                                ],
                    ],

                    'galant'  =>[
                                'galant_y76_80'                     => [
                                      '2_0_lambda'                  => ['2.0 Lambda'],
                                      '2_6_lambda'                  => ['2.6 Lambda'],
                                ],

                                'galant_y80_87'                     => [
                                      '1_6'                         => ['1.6'],
                                ],


                                'galant_y92_99'                     => [
                                      '2_0_ultima'                  => ['2.0 Ultima'],
                                      '2_0_ultima_glsi'             => ['2.0 Ultima GLSi'],
                                ],
                    ],

                    'l300'  =>[
                                'l300_y92_99'                       => [
                                      '2_5'                         => ['2.5'],
                                ],
                    ],


                    'lancer'  =>[
                                'lancer_y04_12'                     => [
                                      '1_6_glx'                     => ['1.6 GLX'],
                                      '1_6_glx_cng'                 => ['1.6 GLX CNG'],
                                      '1_6_glxi'                    => ['1.6 GLXi'],
                                      '1_6_glxi_ltd'                => ['1.6 GLXi LTD'],
                                      '1_6_glxi_ltd_ralliart'       => ['1.6 GLXi LTD Ralliart'],
                                      '1_6_sei'                     => ['1.6 SEi'],
                                      '2_0_sei_ltd'                 => ['2.0 SEi LTD'],
                                      '2.0 sei ralliart'            => ['2.0 SEi Ralliart'],
                                ],

                                'cedia_y01_04'                      => [
                                      '1_6_cedia_glxi'              => ['1.6 Cedia GLXi'],
                                      '1_6_cedia_glxi_ltd.'         => ['1.6 Cedia GLXi-LTD.'],
                                      '1_8_cedia_sei_ltd'           => ['1.8 Cedia SEi-LTD'],
                                ],

                                'champ_i6_y84_85'                   => [
                                      '1_3_gl'                      => ['1.3 GL'],
                                      '1_5'                         => ['1.5'],
                                ],

                                'ecar_y92_96'                       => [
                                      '1_5_glx'                     => ['1.5 GLX'],
                                      '1_5_glxi'                    => ['1.5 GLXi'],
                                      '1_6_glxi'                    => ['1.6 GLXi'],
                                      '1_8_gli'                     => ['1.8 GLi'],
                                ],


                                'f_style_endbenz'                   => [
                                      '1_5_glxi'                    => ['1.5 GLXi'],
                                      '1_6_glxi'                    => ['1.6 GLXi'],
                                      '1_6_glxi_limited'            => ['1.6 GLXi Limited'],
                                      '1_8_sei'                     => ['1.8 SEi'],
                                      '1_8_sei_ltd'                 => ['1.8 SEi LTD'],
                                ],
                    ],


                    'lancer_ex'  =>[
                                'lancer_ex_y09_15'                  => [
                                      '1_8_gls'                     => ['1.8 GLS'],
                                      '1_8_gls_ltd'                 => ['1.8 GLS LTD'],
                                      '1_8_glx'                     => ['1.8 GLX'],
                                      '2_0_gt'                      => ['2.0 GT'],
                                ],
                    ],


                    'lancer_ex'  =>[
                                'lancer_ex_y09_15'                  => [
                                      '1_8_gls'                     => ['1.8 GLS'],
                                      '1_8_gls_ltd'                 => ['1.8 GLS LTD'],
                                      '1_8_glx'                     => ['1.8 GLX'],
                                      '2_0_gt'                      => ['2.0 GT'],
                                ],
                    ],

                    'mirage'  =>[
                                'mirage_y12_16'                         => [
                                      '1_2_gl'                          => ['1.2 GL'],
                                      '1_2_gls'                         => ['1.2 GLS'],
                                      '1_2_gls_limited'                 => ['1.2 GLS Limited'],
                                      '1_2_gls_limited_bloom_edition'   => ['1.2 GLS Limited Bloom Edition'],
                                      '1_2_gls_ltd'                     => ['1.2 GLS LTD'],
                                ],
                    ],


                    'pajero'  =>[
                                'pajero_y00_06'                     => [
                                      '2_8'                         => ['2.8'],
                                      '3_5_gls'                     => ['3.5 GLS'],
                                ],

                                'pajero_y08_15'                     => [
                                      '3.8 exceed'                  => ['3.8 Exceed'],
                                ],

                                'pajero_y92_02'                     => [
                                      '2_5'                         => ['2.5 '],
                                ],
                    ],

                    'pajero'  =>[
                                'pajero_y00_06'                     => [
                                      '2_8'                         => ['2.8'],
                                      '3_5_gls'                     => ['3.5 GLS'],
                                ],

                                'pajero_y08_15'                     => [
                                      '3.8 exceed'                  => ['3.8 Exceed'],
                                ],

                                'pajero_y92_02'                     => [
                                      '2_5'                         => ['2.5'],
                                      '3_0_glx'                     => ['3.0 GLX'],
                                      '3_5_gls'                     => ['3.5 GLS'],
                                ],
                    ],

                    'pajero_io'  =>[
                                'pajero_io_y98_07'                  => [
                                      '1_8_gdi'                     => ['1.8 GDI'],
                                ],
                    ],

                    'pajero_junior'  =>[
                                'pajero_junior_y95_98'              => [
                                      '1_1_zr_ii'                   => ['1.1 ZR-II'],
                                ],
                    ],

                    'pajero_sport'  =>[
                                'pajero_sport_y08_15'               => [
                                      '2_4_gls'                     => ['2.4 GLS'],
                                      '2_5_gls'                     => ['2.5 GLS'],
                                      '2_5_gt'                      => ['2.5 GT'],
                                      '3_0_gt'                      => ['3.0 GT'],
                                      '3_2_gls'                     => ['3.2 GLS'],
                                      '3_2_gt'                      => ['3.2 GT'],
                                ],

                                'pajero_sport_y15_18'               => [
                                      '2_4_gls_ltd'                 => ['2.4 GLS LTD'],
                                      '2_4_gt'                      => ['2.4 GT'],
                                      '2_4_gt_premium'              => ['2.4 GT Premium'],
                                ],
                    ],


                    'space_runner'  =>[
                                'space_runner_y92_00'               => [
                                      '1_8'                         => ['1.8'],
                                ],
                    ],


                    'space_wagon'  =>[
                                'space_wagon_y04_12'                => [
                                      '2_4_gls'                     => ['2.4 GLS'],
                                      '2_4_gls_limited'             => ['2.4 GLS Limited'],
                                      '2_4_gt'                      => ['2.4 GT'],
                                ],
                    ],


                    'strada'  =>[
                                'strada_grandis4dr'                  => [
                                      '2_5_grandis'                  => ['2.5 Grandis'],
                                      '2_8_grandis_gls'              => ['2.8 Grandis GLS'],
                                      '2_8_grandis_glx'              => ['2.8 Grandis GLX'],
                                ],

                                'strada_megacab'                     => [
                                      '2_5_grandis'                  => ['2.5 Grandis'],
                                      '2_8_grandis_gls'              => ['2.8 Grandis GLS'],
                                      '2_8_grandis_glx'              => ['2.8 Grandis GLX'],
                                ],

                                'strada_sngle'                       => [
                                      '2_5_std'                      => ['2.5 STD'],
                                ],
                    ],

                    'strada_g_wagon'  =>[
                                'strada_g_wagon_y01_06'              => [
                                      '2_5_vg_turbo'                 => ['2.5 VG Turbo'],
                                      '2_8_euro_evolution'           => ['2.8 Euro Evolution'],
                                      '2_8_euro_evolution_ii'        => ['2.8 Euro Evolution II'],
                                      '2_8_gls'                      => ['2.8 GLS'],
                                      '2_8_glx'                      => ['2.8 GLX']
                                ],
                    ],

                    'triton'  =>[
                                'double_cab_y05_15'                  => [
                                      '2_4_cng'                      => ['2.4 CNG'],
                                      '2_4_gls_plus'                 => ['2.4 GLS Plus'],
                                      '2_4_glx'                      => ['2.4 GLX'],
                                      '2_4_plus'                     => ['2.4 PLUS'],
                                      '2_4_plus_cng'                 => ['2.4 PLUS CNG'],
                                      '2_5_gl'                       => ['2.5 GL'],
                                      '2_5_gls'                      => ['2.5 GLS'],
                                      '2_5_gls_plus'                 => ['2.5 GLS Plus'],
                                      '2_5_gls_limited'              => ['2.5 GLS-Limited'],
                                      '2_5_glx'                      => ['2.5 GLX'],
                                      '2_5_plus'                     => ['2.5 PLUS'],
                                      '2_5_plus_vg_turbo'            => ['2.5 PLUS VG TURBO'],
                                      '2_5_plus_gls'                 => ['2.5 PLUS GLS'],
                                      '3_2_gls'                      => ['3.2 GLS'],
                                ],

                                'double_cab_y14_19'                  => [
                                      '2_4_gls'                      => ['2.4 GLS'],
                                      '2_4_gls_plus'                 => ['2.4 GLS Plus'],
                                      '2_4_gls_limited'              => ['2.4 GLS-Limited'],
                                      '2_4_gls_limited_plus'         => ['2.4 GLS-Limited_Plus'],
                                      '2_4_glx'                      => ['2.4 GLX'],
                                      '2_4_glx_plus'                 => ['2.4 GLX Plus'],
                                      '2_4_plus_gls'                 => ['2.4 PLUS GLS'],
                                      '2_4_gls_limited'              => ['2.4 GLS-Limited'],
                                      '2_5_glx'                      => ['2.5 GLX'],
                                ],

                                'mega_cab_y05_15'                    => [
                                      '2_4_gl'                       => ['2.4 GL'],
                                      '2_4_gls_plus'                 => ['2.4 GLS Plus'],
                                      '2_4_glx'                      => ['2.4 GLX'],
                                      '2_4_plus'                     => ['2.4 PLUS'],
                                      '2_5_gl'                       => ['2.5 GL'],
                                      '2_5_gls'                      => ['2.5 GLS'],
                                      '2_5_gls_plus'                 => ['2.5 GLS Plus'],
                                      '2_5_glx'                      => ['2.5 GLX'],
                                      '2_5_glx_plus'                 => ['2.5 GLX Plus'],
                                      '2_5_plus_vg_turbo'            => ['2.5 PLUS VG TURBO'],
                                ],

                                'mega_cab_y14_19'                    => [
                                      '2_4_glx'                      => ['2.4 GLX'],
                                      '2_4_glx_plus'                 => ['2.4 GLX Plus'],
                                      '2_5_gl'                       => ['2.5 GL'],
                                      '2_5_glx'                      => ['2.5 GLX'],
                                ],

                                'megacab_y05_15'                     => [
                                      '2_4_cng'                      => ['2.4 CNG'],
                                      '2_4_plus'                     => ['2.4 PLUS'],
                                      '2_4_plus_cng'                 => ['2.4 PLUS CNG'],
                                      '2_5_gls'                      => ['2.5 GLS'],
                                      '2_5_plus'                     => ['2.5 PLUS'],
                                      '2_5_plus_gls'                 => ['2.5 PLUS GLS'],
                                      '2_5_plus_gls_vg_turbo'        => ['2.5 PLUS GLS VG TURBO'],
                                      '2_5_plus_vg_turbo'            => ['2.5 PLUS VG TURBO'],
                                ],

                                'megacab_y14_19'                     => [
                                      '2_4_gls_plus'                 => ['2.4 GLS Plus'],
                                      '2_4_gls_limited_plus'         => ['2.4 GLS-Limited Plus'],
                                      '2_4_glx'                      => ['2.4 GLX'],
                                      '2_4_glx_plus'                 => ['2.4 GLX Plus'],
                                      '2_4_plus_gls_limited'         => ['2.4 PLUS GLS-Limited'],
                                      '2_4_plus_glx'                 => ['2.4 PLUS GLX'],
                                      '2_5_gl'                       => ['2.5 GL'],
                                      '2_5_gls'                      => ['2.5 GLS'],
                                      '2_5_glx'                      => ['2.5 GLX'],
                                ],

                                'single_y05_15'                      => [
                                      '2_4_cng'                      => ['2.4 CNG'],
                                      '2_4_gl'                       => ['2.4 GL'],
                                      '2_5_gl'                       => ['2.5 GL'],
                                      '2_5_gl_vg_turbo_4wd'          => ['2.5 GL VG Turbo 4WD'],
                                      '3.2 gl 4wd'                   => ['3.2 GL 4WD'],
                                ],

                                'single_y14_19'                      => [
                                      '2_4_gl'                       => ['2.4 GL'],
                                      '2_5_gl'                       => ['2.5 GL'],
                                ],
                    ],

                ], // ปิด mitsubishi

                'mazda' => [
                    '2'     =>[
                                '2_y09_14'                          => [
                                      '1_5_elegance_groove'         => ['1.5 Elegance Groove'],
                                      '1_5_elegance_maxx'           => ['1.5 Elegance Maxx'],
                                      '1_5_elegance_racing_series'  => ['1.5 Elegance Racing Series'],
                                      '1_5_elegance_spirit'         => ['1.5 Elegance Spirit'],
                                      '1_5_groove'                  => ['1.5 Groove'],
                                      '1_5_maxx_sports'             => ['1.5 Maxx Sports'],
                                      '1_5_navi'                    => ['1.5 Navi'],
                                      '1_5_R'                       => ['1.5 R'],
                                      '1_5_S'                       => ['1.5 S'],
                                      '1_5_sports_maxx'             => ['1.5 Sports Maxx'],
                                      '1_5_sports_maxx_sports'      => ['1.5 Sports Maxx Sports'],
                                      '1_5_sports_racing_series'    => ['1.5 Sports Racing Series'],
                                      '1_5_sports_spirit'           => ['15 Sports Spirit'],
                                      '1.5 v'                       => ['1.5 V'],


                                ],

                                '2_y15_18'                          => [
                                      '1_3_high'                    => ['1.3 High'],
                                      '1_3_high_plus'               => ['1.3 High Plus'],
                                      '1_3_sports'                  => ['1.3 Sports'],
                                      '1_3_sports_high'             => ['1.3 Sports High '],
                                      '1_3_sports_high_connect'     => ['1.3 Sports High Connect'],
                                      '1_3_sports_high_plus'        => ['1.3 Sports High Plus'],
                                      '1_3_sports_standard'         => ['1.3 Sports Standard'],
                                      '1_3_standard'                => ['1.3 Standard'],
                                      '1_5_xd'                      => ['1_5_XD'],
                                      '1_5_xd_high'                 => ['1.5 XD High'],
                                      '1_5_xd_high_connect'         => ['1.5 XD High Connect'],
                                      '1_5_xd_high_plus'            => ['1.5 XD High Plus'],
                                      '1_5_xd_high_plus_l'          => ['1.5 XD High Plus L'],
                                      '1_5_xd_sports'               => ['1.5 XD Sports'],
                                      '1_5_xd_sports_high'          => ['1.5 XD Sports High'],
                                      '1_5_xd_sports_high_connect'  => ['1.5 XD Sports High Connect'],
                                      '1.5 xd sports high plus'     => ['1.5 XD Sports High Plus'],


                                ],

                    ],

                    '3'     =>[
                                '3_y05_10'                          => [
                                      '1_6_groove'                  => ['1.6 Groove'],
                                      '1_6_life'                    => ['1.6 Life'],
                                      '1_6_s'                       => ['1.6 S'],
                                      '1_6_spirit'                  => ['1.6 Spirit'],
                                      '1_6_spirit_sports'           => ['1.6 Spirit Sports'],
                                      '1_6_v'                       => ['1.6 V'],
                                      '2.0 maxx'                    => ['2.0 Maxx'],
                                      '2_0_maxx sports'             => ['2.0 Maxx Sports'],
                                      '2_0_play'                    => ['2.0 Play'],
                                      '2_0_r'                       => ['2.0 R'],
                                      '2_0_r_sport'                 => ['2.0 R Sport'],
                                ],

                                '3_y11_14'                          => [
                                      '1_6_groove'                  => ['1.6 Groove'],
                                      '1_6_s_plus'                  => ['1.6 S Plus'],
                                      '1_6_spirit'                  => ['1.6 Spirit'],
                                      '1_6_spirit_sports'           => ['1.6 Spirit Sports'],
                                      '1_6_spirit_sports_plus'      => ['1.6 Spirit Sports Plus'],
                                      '2.0 maxx'                    => ['2.0 Maxx'],
                                      '2_0_maxx sports'             => ['2.0 Maxx Sports'],
                                      '2_0_spirit'                  => ['2.0 Spirit'],
                                ],

                                '3_y14_17'                                  => [
                                      '2_0_c'                               => ['2.0 C'],
                                      '2_0_c_sports'                        => ['2.0 C Sports'],
                                      '2_0_e'                               => ['2.0 E'],
                                      '2_0_e_sports'                        => ['2.0 E Sports'],
                                      '2_0_racing_series_limited_edition'   => ['2.0 Racing Series Limited Edition'],
                                      '2_0_s'                               => ['2.0 S'],
                                      '2_0_s_sports'                        => ['2.0 S Sports'],
                                      '2_0_sp_sports'                       => ['2.0 SP Sports'],
                                ],

                    ],

                    '6'  =>[
                                '6_y02_08'                          => [
                                      '2_3'                         => ['2.3'],
                                ],
                    ],

                    '121'  =>[
                                    '121_y92_96'                    => [
                                      '1_3'                         => ['1.3'],
                                ],
                    ],

                    '323'  =>[
                                '323_y95_98'                        => [
                                      '1_8_astina_gti'              => ['1.8 Astina GTi'],
                                ],
                                'protege_y00_06'                    => [
                                      '1_6_protege_glx'             => ['1.6 Protege GLX'],
                                      '1_6_protege_lx'              => ['1.6 Protege LX'],
                                      '1_8_protege_gt'              => ['1.8 Protege GT'],
                                      '2_0_protege_gt'              => ['2.0 Protege GT'],
                                ],
                    ],

                    '626'  =>[
                                '626_y97_00'                        => [
                                      '2_0_glx'                     => ['2.0 GLX'],
                                ],
                                'cronos_y92_97'                     => [
                                      '2_0_cronos'                  => ['2.0 Cronos'],
                                ],
                    ],

                    'bt_50'  =>[
                                'double_cab'                        => [
                                      '2_5_hi_racer'                => ['2.5 Hi-Racer'],
                                      '2_5_s'                       => ['2.5 S'],
                                      '3_0_r'                       => ['3.0 R'],
                                ],
                                'free_style_cab'                    => [
                                      '2_5_hi_racer'                => ['2.5 Hi-Racer'],
                                      '2_5_s'                       => ['2.5 S'],
                                      '2_5_v'                       => ['2.5 V'],
                                      '2_5_v_high'                  => ['2.5 V High'],
                                      '3_0_r'                       => ['3.0 R'],
                                ],
                    ],

                    'bt_50_pro'  =>[
                                'double_cab'                        => [
                                      '2_2_eclipse'                 => ['2.2 ECLIPSE'],
                                      '2_2_hi_racer'                => ['2.2 Hi-Racer'],
                                      '2_2_hi_racer_eclipse'        => ['2.2 Hi-Racer Eclipse'],
                                      '2_2_hi_racer_proseries'      => ['2.2 Hi-Racer PROSERIES'],
                                      '2_2_s'                       => ['2.2 S'],
                                      '2_2_v'                       => ['2.2 V'],
                                      '2_5_s'                       => ['2.5 S'],
                                      '3_2_r'                       => ['3.2 R'],
                                ],
                                'free_style_cab'                    => [
                                      '2_2_hi_racer'                => ['2.2 Hi-Racer'],
                                      '2_2_hi_racer_eclipse'        => ['2.2 Hi-Racer Eclipse'],
                                      '2_2_hi_racer_proseries'      => ['2.2 Hi-Racer PROSERIES'],
                                      '2_2_s'                       => ['2.2 S'],
                                      '2_2_v'                       => ['2.2 V'],
                                      '2_5_v'                       => ['2.5 V'],
                                ],
                                'single'                            => [
                                      '2_2_s'                       => ['2.2 S'],
                                      '2_5_s'                       => ['2.5 S'],
                                ],
                    ],

                    'cx_3'  =>[
                                'y15_19'                            => [
                                      '1_5_xdl'                     => ['1.5 XDL'],
                                      '2_0_s'                       => ['2.0 S'],
                                      '2_0_sp'                      => ['2.0 SP'],
                                ],
                    ],


                    'cx_5'  =>[
                                'y13_16'                            => [
                                      '2_0_c'                       => ['2.0 C'],
                                      '2_0_s'                       => ['2.0 S'],
                                      '2_2_xdl'                     => ['2.2 XDL'],
                                      '2_5_s'                       => ['2.5 S'],
                                ],

                                'y17_20'                            => [
                                      '2_0_s'                       => ['2.0 S'],
                                      '2_0_sp'                      => ['2.0 SP'],
                                      '2_2_xdl'                     => ['2.2 XDL'],
                                ],
                    ],

                    'famillia'  =>[
                                'single'                            => [
                                      '1_4_str'                     => ['1.4 STR'],
                                ],

                                'super_cab'                         => [
                                      '1_4_std'                     => ['1.4 STD '],
                                ],
                    ],

                    'fighter'  =>[
                                'double_cab'                        => [
                                      '2_5'                         => ['2.5'],
                                      '2_5_lux'                     => ['2.5 Lux'],
                                      '2_5_mid'                     => ['2.5 Mid'],
                                      '2_9'                         => ['2.9'],
                                ],

                                'free_style_cab'                    => [
                                      '2_5_lux'                     => ['2.5 LUX'],
                                      '2_5_mid'                     => ['2.5 Mid'],
                                      '2_5_se1'                     => ['2.5 SE1'],
                                ],

                                'super_saloon'                      => [
                                      '2_5_super_saloon'            => ['2.5 Super Saloon'],
                                      '2_5_super_saloon_str_base'   => ['2.5 Super Saloon STR Base'],
                                      '2_5_super_saloon_str_lux'    => ['2.5 Super Saloon STR Lux'],
                                      '2_5_super_saloon_str_mid'    => ['2.5 Super Saloon STR Mid'],
                                ],
                    ],

                    'lantis'  =>[
                                'y94_00'                            => [
                                      '1_8'                         => ['1.8'],
                                ],
                    ],

                    'magnum_thunders'  =>[
                                'y92_98'                            => [
                                      '2_5_b2500_str'               => ['2.5 B2500 STR'],
                                      '2_5_std'                     => ['2.5 STD'],
                                      '2_5_str'                     => ['2.5 STR'],
                                ],
                    ],

                    'mpv'  =>[
                                'y02_06'                            => [
                                      '3_0'                         => ['3.0'],
                                ],
                    ],

                    'mx_5'  =>[
                                'y01_05'                            => [
                                      '1_8'                         => ['1.8'],
                                ],
                                'y05_15'                            => [
                                      '2_0'                         => ['2.0'],
                                ],
                                'y16_20'                            => [
                                      '2_0'                         => ['2.0'],
                                ],
                                'y92_98'                            => [
                                      '1_6'                         => ['1.6'],
                                      '1_8'                         => ['1.8'],
                                      '1_8_roadster'                => ['1.8 Roadster'],
                                ],
                    ],

                    'rx_7'  =>[
                                'y92_00'                            => [
                                      '1_3'                         => ['1.3'],
                                      '1_3_fd'                      => ['1.3_FD'],
                                ],
                    ],

                    'rx_8'  =>[
                                'y03_08'                            => [
                                      '1_3'                         => ['1.3'],
                                      '1_3_roadster'                => ['1.3 Roadster'],
                                      '1_3_se3p'                    => ['1.3 SE3P'],
                                ],
                    ],

                    'tribute'  =>[
                                'y02_06'                            => [
                                      '2_3_sdx'                     => ['2.3 SDX'],
                                      '3_0_sdx'                     => ['3.0 SDX'],
                                ],
                    ],

                ],// ปิด mazda




                'ford' => [
                    'aspire'     =>[
                                'y94_97'                            => [
                                      '1_3_gl'                      => ['1.3 GL'],
                                ],
                    ],

                    'courier'     =>[
                                'y77_85'                            => [
                                      '2_2'                         => ['2.2'],
                                ],
                    ],

                    'ecosport'     =>[
                                'y13_16'                            => [
                                      '1_5_ambiente'                => ['1.5 Ambiente'],
                                      '1_5_titanium'                => ['1.5 Titanium'],
                                      '1_5_trend'                   => ['1.5 Trend'],
                                ],
                    ],

                    'escape'     =>[
                                'y03_07'                            => [
                                      '2_0'                         => ['2.0'],
                                      '2_3_xls'                     => ['2.3 XLS'],
                                      '2_3_xlt'                     => ['2.3 XLT'],
                                      '3_0_ltd'                     => ['3.0 LTD'],
                                      '3_0_xlt'                     => ['3.0 XLT'],
                                ],

                                'y09_12'                            => [
                                      '2_3_xls'                     => ['2.3 XLS'],
                                      '2_3_xlt'                     => ['2.3 XLT'],
                                ],
                    ],

                    'everest'     =>[
                                'y03_06'                            => [
                                      '2_5_ltd'                     => ['2.5 LTD'],
                                      '2_5_xlt'                     => ['2.5 XLT'],

                                ],

                                'y07_13'                            => [
                                      '2_5_ltd_sport_tdci'          => ['2.5 LTD Sport TDCi'],
                                      '2_5_ltd_tdci'                => ['2.5 LTD TDCi'],
                                      '2_5_xlt_tdci'                => ['2.5 XLT TDCi'],
                                      '3_0_ltd_tdci'                => ['3.0 LTD TDCi'],

                                ],

                                'y15_18'                            => [
                                      '2_2_titanium'                => ['2.2 Titanium'],
                                      '2_2_titanium_plus'           => ['2.2 Titanium+'],
                                      '3_2_titanium'                => ['3.2 Titanium'],
                                      '3_2_titanium_plus'           => ['3.2 Titanium+'],

                                ],

                                'y15_18_2'                          => [
                                      '3_2_titanium_plus'           => ['3.2 Titanium+'],

                                ],
                    ],

                    'festiva'     =>[
                                'y92_96'                            => [
                                      '1_3_gl'                      => ['1.3 GL'],

                                ],

                    ],


                    'fiesta'     =>[
                                'y10_16'                            => [
                                      '1_0_sport'                   => ['1.0 Sport'],
                                      '1_0_titanium'                => ['1.0 Titanium'],
                                      '1_4_style'                   => ['1.4 Style'],
                                      '1_4_trend'                   => ['1.4 Trend'],
                                      '1_5_ambiente'                => ['1.5 Ambiente'],
                                      '1_5_sport'                   => ['1.5 Sport'],
                                      '1_5_titanium'                => ['1.5 Titanium'],
                                      '1_5_trend'                   => ['1.5 Trend'],
                                      '1_6_sport'                   => ['1.6 Sport'],
                                      '1_6_sport_ultimate'          => ['1.6 Sport Ultimate'],
                                      '1_6_sport_plus'              => ['1.6 Sport+'],
                                      '1_6_trend'                   => ['1.6 Trend'],
                                      

                                ],

                    ],

                    'focus'     =>[
                                'y04_08'                            => [
                                      '1_8_finesse'                 => ['1.8 Finesse'],
                                      '1_8_ghia'                    => ['1.8 Ghia'],
                                      '1_8_trend'                   => ['1.8 Trend'],
                                      '2_0_sport'                   => ['2.0 Sport'],
                                ],

                                'y09_12'                            => [
                                      '1_8_ambiente'                => ['1.8 Ambiente'],
                                      '1_8_finesse'                 => ['1.8 Finesse'],
                                      '2_0_ghia'                    => ['2.0 Ghia'],
                                      '2_0_sport'                   => ['2.0 Sport'],
                                ],

                                'y12_16'                            => [
                                      '1_5_sport'                   => ['1.5 Sport'],
                                      '1_6_ambiente'                => ['1.6 Ambiente'],
                                      '1_6_trend'                   => ['1.6 Trend'],
                                      '2_0_sport'                   => ['2.0 Sport'],
                                      '2_0_sport_plus'              => ['2.0 Sport+'],
                                      '2_0_titanium'                => ['2.0 Titanium'],
                                      '2_0_titanium_plus'           => ['2.0 Titanium+'],
                                ],


                    ],

                    'laser'     =>[
                                'y00_05'                            => [
                                      '1_6_glx'                     => ['1.6 GLX'],
                                      '1_6_glxi'                    => ['1.6 GLXi'],
                                      '1_6_tierra_vxi'              => ['1.6 Tierra VXI'],
                                      '1_8_ghia'                    => ['1.8 Ghia'],
                                      '1_8_tierra_ghia'             => ['1.8 Tierra Ghia'],
                                      '1.8 tierra rs'               => ['1.8 Tierra RS'],
                                ],

                                'y91_94'                            => [
                                      '1_6_gl'                      => ['1.6 GL'],
                                ],
                    ],


                    'lincoln_continental'     =>[
                                'y70_79'                            => [
                                      '6_6'                         => ['6.6'],
                                ],
                    ],

                    'mustang'     =>[
                                'y05_15'                            => [
                                      '3_7'                         => ['3.7'],
                                      '4_6'                         => ['4.6'],
                                ],
                                'y15_20'                            => [
                                      '2_3_ecoboost'                => ['2.3 EcoBoost'],
                                      '5_0_gt'                      => ['5.0 GT'],
                                      '5_2_shelby_gt350'            => ['5.2 Shelby GT350'],
                                ],
                    ],

                    'ranger'     =>[
                                'double_cab_y03_05'                 => [
                                      '2_5_xl'                      => ['2.5 XL'],
                                      '2_5_xls'                     => ['2.5 XLS'],
                                      '2_5_xlt'                     => ['2.5 XLT'],
                                      '2_5_xlt_limited'             => ['2.5 XLT Limited'],
                                ],
                                'double_cab_y06_08'                 => [
                                      '2_5_hi_rider_xlt'            => ['2.5 Hi-Rider XLT'],
                                      '2_5_xls'                     => ['2.5 XLS'],
                                      '3_0_xlt_tdci'                => ['3.0 XLT TDCi'],
                                ],
                                'double_cab_y09_12'                 => [
                                      '2_5_hi_rider_xlt'            => ['2.2 Hi-Rider XLT'],
                                      '2_5_hi_rider_wildtrak_ltd'   => ['2.5 Hi-Rider WildTrak LTD'],
                                      '2_5_hi_rider_wildtrak_xlt'   => ['2.5 Hi-Rider WildTrak XLT'],
                                      '2_5_xls_tdci'                => ['2.5 XLS TDCi'],
                                      '2_5_wildtrak'                => ['2.5 WildTrak'],
                                      '3_0_xlt_tdci'                => ['3.0 XLT TDCi'],
                                ],
                                'double_cab_y12_15'                 => [
                                      '2_2_hi_rider_wildtrak'       => ['2.2 Hi-Rider WildTrak'],
                                      '2_2_hi_rider_wildtrak_tdci'  => ['2.2 Hi-Rider WildTrak TDCi'],
                                      '2_2_hi_rider_xls'            => ['2.2 Hi-Rider XLS'],
                                      '2_2_hi_rider_xlt'            => ['2.2 Hi-Rider XLT'],
                                      '2_2_wildtrak'                => ['2.2 WildTrak'],
                                      '2_2_xlt'                     => ['2.2 XLT'],
                                      '3_2_wildtrak'                => ['3.2 WildTrak'],
                                ],
                                'double_cab_y15_18'                 => [
                                      '2_2_hi_rider'                => ['2.2 Hi-Rider'],
                                      '2_2_hi_rider_wildtrak_tdci'  => ['2.2 Hi-Rider WildTrak TDCi'],
                                      '2_2_hi_rider_fx4'            => ['2.2 Hi-Rider FX4'],
                                      '2_2_hi_rider_wildtrak'       => ['2.2 Hi-Rider WildTrak'],
                                      '2_2_hi_rider_xls'            => ['2.2 Hi-Rider XLS'],
                                      '2_2_hi_rider_xlt'            => ['2.2 Hi-Rider XLT'],
                                      '2_2_wildtrak'                => ['2.2 WildTrak'],
                                      '2_5_xlt'                     => ['2.5 XLT'],
                                      '3_2_wildtrak'                => ['3.2 WildTrak'],
                                      '3_2_xlt'                     => ['3.2 XLT'],
                                ],
                                'double_cab_y99_02'                 => [
                                      '2_5_xlt'                     => ['2.5 XLT'],
                                      '2_9_xlt'                     => ['2.9 XLT'],
                                ],
                                'open_cab_y03_05'                   => [
                                      '2_5_hi_rider_xlt'            => ['2.5 Hi-Rider XLT'],
                                      '2_5_xl'                      => ['2.5 XL'],
                                      '2_5_xls'                     => ['2.5 XLS'],
                                      '2_5_xlt'                     => ['2.5 XLT'],
                                ],
                                'open_cab_y06_08'                   => [
                                      '2_5_hd_limited'              => ['2.5 HD Limited'],
                                      '2_5_hi_rider_wildtrak'       => ['2.5 Hi-Rider WildTrak'],
                                      '2_5_hi_rider_xls'            => ['2.5 Hi-Rider XLS'],
                                      '2_5_hi_rider_xlt'            => ['2.5 Hi-Rider XLT'],
                                      '2_5_xl'                      => ['2.5 XL'],
                                      '2_5_xls'                     => ['2.5 XLS'],
                                      '2_5_xlt'                     => ['2.5 XLT'],
                                      '3_0_hi_rider_xlt_tdci'       => ['3.0 Hi-Rider XLT TDCi'],
                                ],
                                'open_cab_y09_12'                   => [
                                      '2_5_hi_rider_wildtrak_ltd'   => ['2.5 Hi-Rider WildTrak LTD'],
                                      '2_5_hi_rider_wildtrak_xlt'   => ['2.5 Hi-Rider WildTrak XLT'],
                                      '2_5_hi_rider_xls_tdci'       => ['2.5 Hi-Rider XLS TDCi'],
                                      '2_5_hi_rider_xlt_tdci'       => ['2.5 Hi-Rider XLT TDCi'],
                                      '2_5_wildtrak'                => ['2.5 WildTrak'],
                                      '2_5_xls'                     => ['2.5 XLS'],
                                      '2_5_xls_tdci'                => ['2.5 XLS TDCi'],
                                      '2_5_xlt_tdci'                => ['2.5 XLT TDCi'],
                                      '3_0_hi_rider_xlt_tdci'       => ['3.0 Hi-Rider XLT TDCi'],
                                ],
                                'open_cab_y12_15'                   => [
                                      '2_2_hi_rider_wildtrak'       => ['2.2 Hi-Rider WildTrak'],
                                      '2_2_hi_rider_xls'            => ['2.2 Hi-Rider XLS'],
                                      '2_2_hi_rider_xls_tdci'       => ['2.2 Hi-Rider XLS TDCi'],
                                      '2_2_hi_rider_xlt'            => ['2.2 Hi-Rider XLT'],
                                      '2_2_wildtrak'                => ['2.2 WildTrak'],
                                      '2_2_xls'                     => ['2.2 XLS'],
                                      '2_2_xls_tdci'                => ['2.2 XLS TDCi'],
                                      '2_2_xlt'                     => ['2.2 XLT'],
                                      '2_5_xl'                      => ['2.5 XL'],
                                ],
                                'open_cab_y15_18'                   => [
                                      '2_2_hi_rider_xl_plus'        => ['2.2 Hi-Rider XL+'],
                                      '2_2_hi_rider_xls'            => ['2.2 Hi-Rider XLS'],
                                      '2_2_hi_rider_xlt'            => ['2.2 Hi-Rider XLT'],
                                      '2_2_xl'                      => ['2.2 XL'],
                                      '2_2_xls'                     => ['2.2 XLS'],
                                ],
                                'single_cab_y03_05'                 => [
                                      '2_5_xl'                      => ['2.5 XL'],
                                ],
                                'single_cab_y12_15'                 => [
                                      '2_2_xl'                      => ['2.2 XL'],
                                      '2_5_xl'                      => ['2.5 XL'],
                                ],
                                'single_cab_y15_18'                 => [
                                      '2_2_standard_xl'             => ['2.2 Standard XL'],
                                ],
                                'single_cab_y99_02'                 => [
                                      '2_5_xl'                      => ['2.5 XL'],
                                ],
                                'super_cab_y03_05'                  => [
                                      '2_5_xl'                      => ['2.5 XL'],
                                      '2_5_xls'                     => ['2.5 XLS'],
                                ],
                                'super_cab_y09_12'                  => [
                                      '2_5_xl'                      => ['2.5 XL'],
                                      '2_5_xlt'                     => ['2.5 XLT'],
                                      '2_9_xlt'                     => ['2.9 XLT'],
                                ],
                    ],


                ], // ปิด ford

            ], //  ปิดรายละเอียดรุ่น


            'type' => [
                'cabriolet'             =>  ['Cabriolet'],
                'mpv'                   =>  ['MPV'],
                'suv'                   =>  ['SUV'],
                'truck'                 =>  ['Truck'],
                'wagon'                 =>  ['Wagon'],
                'pickup'                =>  ['Pickup'],
                'coupe'                 =>  ['Coupe'],
                'sedan'                 =>  ['Sedan'],
                'hatchback'             =>  ['Hatchback'],
                'van'                   =>  ['Van'],
                'convertible'           =>  ['Convertible']
            ], //


            'passengers_quantity' => [
                2   => ['2 คน'],
                3   => ['3 คน'],
                4   => ['4 คน'],
                5   => ['5 คน'],
                6   => ['6 คน'],
                7   => ['7 คน'],
                8   => ['8 คน'],
                9   => ['9 คน'],
                10   => ['10+ คนขึ้นไป'],
            ], // ปิด passengers_quantity

            'baggage_quantity' => [
                1   => ['1 สัมภาระ'],
                2   => ['2 สัมภาระ'],
                3   => ['3 สัมภาระ'],
                4   => ['4 สัมภาระ'],
                5   => ['5 สัมภาระ'],
                6   => ['6 สัมภาระ'],
                7   => ['7 สัมภาระ'],
                8   => ['8 สัมภาระ'],
                9   => ['9 สัมภาระ'],
                10   => ['10 สัมภาระขึ้นไป'],
            ], // ปิด baggage_quantity

            'insurance'                             =>[
                        'bangkok'                   => ['Bangkok Insurance'],
                        'viriyah'                   => ['Viriyah Insurance'],
                        'synmankong'                => ['Syn Man Kong Insurance'],
                        'thanachart'                => ['Thanachart Insurance'],
                        'directasia'                => ['Direct Asia Insurance'],
                        'axa'                       => ['AXA Insurance'],
                        'gobear'                    => ['Go Bear Insurance'],
                        'aig'                       => ['AIG Insurance'],
            ], //ปิด insurance

            'bangkok'                     =>[
                        'type1'                     => ['Comprehensive'],
                        'type2'                     => ['Third party Fire and Theft + vehicle collision'],
                        'type3+'                    => ['Third Party + vehicle collision'],
                        'type2'                     => ['Third-Party Fire and Theft'],
                        'type3'                     => ['Third-Party Only']
            ], //ปิด bangkok_insurance

            'viriyah'                        =>[
                        'type1'                     => ['Maximum coverage'],
                        'type2'                     => ['Coverage slightly less to Type 1'],
                        'type3'                     => ['Providing coverage for third parties only'],
                        'type4'                     => ['This policy provides coverage for property of third parties'],
                        'type5'                     => ['Insurance coverage for specific perils']
            ], //ปิด viri_insurance

            'synmankong'                  =>[
                        'type1'                     => ['First class insurance premium starting 9,600 baht'],
                        'type2'                     => ['Does not cover damage to the insured vehicle.'],
                        'type2+'                    => ['Coverage for both collision and lost fire'],
                        'type3'                     => ['Will only cover damage to life and property to third parties.'],
                        'type3+'                    => ['Covers both car and car insurance up to a maximum of 150,000 baht.']
            ],//ปิด synmankong_insurance

            'thanachart'                  =>[
                        'type1'                     => ['Comprehensive'],
                        'type2'                     => ['Third party Fire and Theft + vehicle collision'],
                        'type3+'                    => ['Third Party + vehicle collision'],
                        'type2'                     => ['Third-Party Fire and Theft'],
                        'type3'                     => ['Third-Party Only']
            ], //ปิด thanachart_insurance


            'directasia'                 =>[
                        'type1'                     => ['Comprehensive'],
                        'type2'                     => ['Third party Fire and Theft + vehicle collision'],
                        'type2+'                    => ['Third Party + vehicle collision'],
                        'type3'                     => ['Third-Party Fire and Theft'],
                        'type3+'                     => ['Third-Party Only']
            ], //ปิด direct_asia_insurance

            'axa'                         =>[
                        'type1'                     => ['Our most extensive coverage for a worry-free drive.'],
                        'type1+'                    => ['The less you drive, the less you pay.'],
                        'type2'                     => ['Class 2+ motor insurance with maximum coverage'],
                        'type2+'                    => ['Class 2+ motor insurance with a variety of coverage options.'],
                        'type3'                     => ['Third-Party Only'],
                        'type3+'                    => ['Ready to take care ofyou 24/7'],
                        'axa_motor_3_extra'         => ['3rd Class insurance with free Roadside Assistance'],
                        'motor_add_on'
            ], //ปิด axa_insurance

            'aig'                         =>[
                        'type1'                    => ['Comprehensive'],
                        'type2'                   => ['Third-Party Fire and Theft'],
                        'type2+'                    => ['Protection for your car from fire or theft'],
                        'type3'                    => ['protection for your legal liability to any third party'],
            ], //ปิด aig_insurance

            'option_extra'                          =>[
                        'gearbox'                   =>  ['Gearbox'],
                        'baby_seat'                 =>  ['Baby Seat'],
                        'parking_sensors'           =>  ['Parking sensors'],
                        'bluetooth'                 =>  ['Bluetooth'],
                        'manual_gearbox'            =>  ['Manual gearbox '],
                        'big_wheels'                =>  ['Big wheels']
            ], //ปิด option_extra

    ], //ปิดไทย

     /*
     * ภาษาอังกฤษ
     */

   'en' => [

            'brands' => [
                'honda' => [
                            'accord'                =>  ['Accord'],
                            'br_v'                  =>  ['BR-V'],
                            'brio'                  =>  ['Brio'],
                            'city'                  =>  ['City'],
                            'civic'                 =>  ['Civic'],
                            'cr_v'                  =>  ['CR-V'],
                            'cr_x_del_sol'          =>  ['CR-X del Sol'],
                            'cr_z'                  =>  ['CR-Z'],
                            'crossroad'             =>  ['CROSSROAD'],
                            'elysion'               =>  ['Elysion'],
                            'fit'                   =>  ['Fit'],
                            'freed'                 =>  ['Freed'],
                            'hr_v'                  =>  ['HR-V'],
                            'inspire'               =>  ['Inspire'],
                            'integra'               =>  ['Integra'],
                            'jazz'                  =>  ['Jazz'],
                            'legend'                =>  ['Legend'],
                            'mobillio'              =>  ['Mobillio'],
                            'nsx'                   =>  ['NSX'],
                            'odyssey'               =>  ['Odyssey'],
                            'prelude'               =>  ['Prelude'],
                            's660'                  =>  ['S660'],
                            'stepwgn'               =>  ['STEPWGN'],
                            'stepwgn_spada'         =>  ['STEPWGN SPADA'],
                            'stream'                =>  ['Stream'],
                            'today'                 =>  ['Today']
                        ],
                'toyota' => [
                            '4runner'               =>  ['4Runner'],
                            '86gt'                  =>  ['86GT'],
                            'alphard'               =>  ['Alphard'],
                            'aristo'                =>  ['Aristo'],
                            'avanza'                =>  ['Avanza'],
                            'avensis'               =>  ['Avensis'],
                            'aygo'                  =>  ['Aygo'],
                            'db'                    =>  ['bB'],
                            'c_hr'                  =>  ['C-HR'],
                            'camry'                 =>  ['Camry'],
                            'carina'                =>  ['Carina'],
                            'celica'                =>  ['Celica'],
                            'celsior'               =>  ['Celsior'],
                            'century'               =>  ['Century'],
                            'commuter'              =>  ['Commuter'],
                            'corolla'               =>  ['Corolla'],
                            'corolla_altis'         =>  ['Corolla Altis'],
                            'corona'                =>  ['Corona'],
                            'cressida'              =>  ['Cressida'],
                            'crown'                 =>  ['Crown'],
                            'dyna'                  =>  ['Dyna'],
                            'esquire'               =>  ['Esquire'],
                            'estima'                =>  ['Estima'],
                            'fj_cruiser'            =>  ['FJ CRUISER'],
                            'fortuner'              =>  ['Fortuner'],
                            'grand_hiace'           =>  ['Grand Hiace'],
                            'grand_wagon'           =>  ['Grabd Wagon'],
                            'granvia'               =>  ['Granvia'],
                            'harrier'               =>  ['Harrier'],
                            'hiace'                 =>  ['Hiace'],
                            'hilux_hero'            =>  ['Hilux Hero'],
                            'hilux_mighty_x'        =>  ['Hilux Mighty-X'],
                            'hilux_revo'            =>  ['Hilux Revo'],
                            'hilux_surf'            =>  ['Hilux Surf'],
                            'hilux_tiger'           =>  ['Hilux Tiger'],
                            'hilux_vigo'            =>  ['Hilux Vigo'],
                            'innova'                =>  ['Innova'],
                            'iq'                    =>  ['IQ'],
                            'kluger'                =>  ['Kluger'],
                            'land_cruiser'          =>  ['Land Cruiser'],
                            'landcruiser_prado'     =>  ['Landcruiser Prado'],
                            'ln'                    =>  ['LN'],
                            'lucida'                =>  ['Lucida'],
                            'mark_2'                =>  ['Mark 2'],
                            'mark_x'                =>  ['Mark X'],
                            'mega_cruiser'          =>  ['Mega Cruiser'],
                            'mr_s'                  =>  ['MR-S'],
                            'mr_2'                  =>  ['MR2'],
                            'noah'                  =>  ['Noah'],
                            'opa'                   =>  ['Opa'],
                            'porte'                 =>  ['Porte'],
                            'prius'                 =>  ['Prius'],
                            'rav4'                  =>  ['Rav4'],
                            'sienta'                =>  ['Sienta'],
                            'soarer'                =>  ['Soarer'],
                            'soluna'                =>  ['Soluna'],
                            'sport_cruiser'         =>  ['Sport Cruiser'],
                            'sport_rider'           =>  ['Sport Rider'],
                            'starlet'               =>  ['Starlet'],
                            'super_custom'          =>  ['Super Custom'],
                            'super_wagon'           =>  ['Super Wagon'],
                            'supra'                 =>  ['Supra'],
                            'vellfire'              =>  ['Vellfire'],
                            'ventury'               =>  ['Ventury'],
                            'vios'                  =>  ['Vios'],
                            'vista'                 =>  ['Vista'],
                            'vitz'                  =>  ['Vitz'],
                            'voxy'                  =>  ['Voxy'],
                            'wish'                  =>  ['Wish'],
                            'yaris'                 =>  ['Yaris'],
                            'yaris_ativ'            =>  ['Yaris Ativ']
                        ],

                'isuzu' =>[
                            'adventure'             =>  ['Adventure'],
                            'adventure_master'      =>  ['Adventure Master'],
                            'buddy'                 =>  ['Buddy'],
                            'cameo'                 =>  ['Cameo'],
                            'd_max'                 =>  ['D-Max'],
                            'dragon_eyes'           =>  ['Dragon Eyes'],
                            'dragon_power'          =>  ['Dragon Power'],
                            'elf'                   =>  ['ELF'],
                            'faster'                =>  ['Faster'],
                            'grand_adventure'       =>  ['Grand Adventure'],
                            'mu_7'                  =>  ['MU-7'],
                            'mu_x'                  =>  ['MU-X'],
                            'trf'                   =>  ['TRF'],
                            'trooper'               =>  ['Trooper'],
                            'vega'                  =>  ['Vega'],
                            'vertex'                =>  ['Vertex']
                ],

                'nissan' =>[
                            '200sx'                 =>  ['200SX'],
                            '300zx'                 =>  ['300ZX'],
                            '350z'                  =>  ['350Z'],  
                            '370z'                  =>  ['370Z'],  
                            'almera'                =>  ['Almera'],  
                            'bigm'                  =>  ['BigM'],  
                            'bluebird_atessa'       =>  ['Bluebird Atessa'],  
                            'cedric'                =>  ['Cedric'],
                            'cefiro'                =>  ['Cefiro'],  
                            'cube'                  =>  ['Cube'],  
                            'elgrand'               =>  ['Elgrand'],
                            'figaro'                =>  ['Figaro'],     
                            'frontier'              =>  ['Frontier'],
                            'frontier_nacara'       =>  ['Frontier Navara'],     
                            'gt_r'                  =>  ['GT-R'],     
                            'infiniti'              =>  ['Infiniti'],
                            'juke'                  =>  ['juke'],
                            'leaf'                  =>  ['Leaf'], 
                            'livina'                =>  ['Livina'], 
                            'march'                 =>  ['March'],
                            'murano'                =>  ['Murano'],
                            'note'                  =>  ['Note'],  
                            'np_300_navara'         =>  ['NP 300 Navara'],  
                            'nv'                    =>  ['NV'],
                            'nx'                    =>  ['NX'], 
                            'pao'                   =>  ['Pao'], 
                            'patro'                 =>  ['Patro'], 
                            'presea'                =>  ['Presea'],
                            'president'             =>  ['President'],
                            'primera'               =>  ['Primera'],                    
                            'pulsar'                =>  ['Pulsar'],
                            'sentra'                =>  ['Sentra'],
                            'serena'                =>  ['Serena'],
                            'silv3ia'               =>  ['Silvia'],
                            'skyline'               =>  ['Skyline'],
                            'sunny'                 =>  ['Sunny'],
                            'sylphy'                =>  ['Sylphy'],
                            'teena'                 =>  ['Teena'],
                            'terrano_ii'            =>  ['Terrano II'],
                            'tiida'                 =>  ['Tiida'],
                            'urvan'                 =>  ['Urvan'],
                            'x-trail'               =>  ['X-Trail'],
                            'xciter'                =>  ['Xciter']

                ],          


                'mitsubishi' =>[
                            '300gt'                 =>  ['300GT'],
                            'attrage'               =>  ['Attrage'],
                            'canter'                =>  ['Canter'],
                            'cyblone'               =>  ['Cyclone'],
                            'delica_space_wagon'    =>  ['Delica Space Wagon'],
                            'evolution'             =>  ['Evolution'],
                            'fto'                   =>  ['FTO'],
                            'galant'                =>  ['Galant'],
                            'l300'                  =>  ['L300'],
                            'lancer'                =>  ['Lancer'],
                            'lancer_ex'             =>  ['Lancer EX'],
                            'mirage'                =>  ['Mirage'],
                            'pajero'                =>  ['Pajero'],
                            'pajero_io'             =>  ['Pajero IO'],
                            'pajero_junior'         =>  ['Pajerojunior'],
                            'pajero_sport'          =>  ['Pajero Sport'],
                            'space_runner'          =>  ['Space Runner'],
                            'space_wagon'           =>  ['Space Wagon'],
                            'strada'                =>  ['Strada'],
                            'strada_g_wagon'        =>  ['Strada G-Wagon'],
                            'trition'               =>  ['Triton']

                ],

                'mercedes_benz'                     =>[
                            'a_class'               =>  ['A-Class'],
                            'a170'                  =>  ['A170'],
                            'a180'                  =>  ['A180'],
                            'a180_cdi'              =>  ['A180 CDI'],
                            'a250'                  =>  ['A250'],
                            'a45'                   =>  ['A45'],
                            'b_class'               =>  ['B-Class'],
                            'b180'                  =>  ['B180'],
                            'b180_cdi'              =>  ['B180 CDI'],
                            'b200'                  =>  ['B200'],
                            'c_class'               =>  ['C-Class'],
                            'c180'                  =>  ['C180'],
                            'c180_amg'              =>  ['C180 AMG'],
                            'c180_kompressor'       =>  ['C180 Kompressor'],
                            'c220'                  =>  ['C220'],
                            'c220_cdi'              =>  ['C220 CDI'],
                            'c230'                  =>  ['C230'],
                            'c230_kompressor'       =>  ['C230 Kompressor'],
                            'c240'                  =>  ['C240'],
                            'c250'                  =>  ['C250'],
                            'c250_cdi'              =>  ['C250 CDI'],
                            'c250_cgi'              =>  ['C250 CGI'],
                            'c300'                  =>  ['C300'],
                            'c32_amg'               =>  ['C32 AMG'],
                            'c350'                  =>  ['C350'],
                            'c55_amg'               =>  ['C55 AMG'],
                            'cl_class'              =>  ['CL-Class'],
                            'cl500'                 =>  ['CL500'],
                            'cl500_amg'             =>  ['CL500 AMG'],
                            'cla_class'             =>  ['CLA-Class'],
                            'cla180'                =>  ['CLA180'],
                            'cla200'                =>  ['CLA200'],
                        
                ],

                'mazda'                             =>[
                            '2'                     =>  ['2'],
                            '3'                     =>  ['3'],
                            '6'                     =>  ['6'],
                            '121'                   =>  ['121'],
                            '323'                   =>  ['323'],
                            '626'                   =>  ['626'],
                            'bt_50'                 =>  ['BT-50'],
                            'bt_50_pro'             =>  ['BT-50 PRO'],
                            'cx_3'                  =>  ['CX-3'],
                            'cx_5'                  =>  ['CX-5'],
                            'cx_7'                  =>  ['CX-7'],
                            'famillia'              =>  ['Famillia'],
                            'fighter'               =>  ['Fighter'],
                            'magnum_thunder'        =>  ['Magnum Thunder'],
                            'lantis'                =>  ['Lantis'],
                            'mpv'                   =>  ['MPV'],
                            'mx_5'                  =>  ['MX-5'],
                            'rx_7'                  =>  ['RX-7'],
                            'rx_8'                  =>  ['RX-8'],
                            'tribute'               =>  ['Tribute']

                ],

                'ford'                              =>[
                            'aspire'                =>  ['Aspire'],
                            'courier'               =>  ['Courier'],
                            'ecosport'              =>  ['Eco Sport'],
                            'escape'                =>  ['Escape'],
                            'everest'               =>  ['Everest'],
                            'explorer'              =>  ['Explorer'],
                            'festiva'               =>  ['Festiva'],
                            'fiesta'                =>  ['Fiesta'],
                            'focus'                 =>  ['Focus'],
                            'lacer'                 =>  ['Lacer'],
                            'lincoln'               =>  ['Lincoln'],
                            'focus'                 =>  ['Focus'],
                            'nustang'               =>  ['Mustang'],
                            'ranger'                =>  ['Ranger'],
                            'territory'             =>  ['Territory']

                ],

                'chevrolet'                         =>[
                            'allroader'             =>  ['Allroader'],
                            'astro'                 =>  ['Astro'],
                            'aveo'                  =>  ['Aveo'],
                            'c10'                   =>  ['C10'],
                            'camaro'                =>  ['Camaro'],
                            'captiva'               =>  ['Captiva'],
                            'colorado'              =>  ['Colorado'],
                            'corvette'              =>  ['Corvette'],
                            'cruze'                 =>  ['Cruze'],
                            'express'               =>  ['Express'],
                            'optra'                 =>  ['Optra'],
                            'savana'                =>  ['Savana'],
                            'sonic'                 =>  ['Sonic'],
                            'spin'                  =>  ['Spin'],
                            'traiblazer'            =>  ['Trailblazer'],
                            'van'                   =>  ['Van'],
                            'zafira'                =>  ['Zafira']
                ],
            ], //ปิดแบรนด์

            'nickname'  =>[
                'honda'  => [
                    'accord'      =>[
                              'year_3_7'            => ['(ปี 03-07)'],
                              'year_7_13'           => ['(ปี 07-13)'],
                              'year_13_17'          => ['(ปี 13-17)'],
                              'accord_snake'        => ['งูเห่า (ปี 97-02)'],
                              'accord_eye'          => ['ตาเพชร (ปี 89-93)'],
                              'accord_abcd'         => ['ท้ายก้อน-ท้ายสองก้อน (ปี 93-97)']
                    ],
                    'br_v'        =>[
                              'br_v_y16_20'         => ['(ปี 16-20)']
                    ],
                    'brio'        =>[
                              'brio_y11_16'         => ['(ปี 11-16)']
                    ],
                    'city'        =>[
                              'city_y02_05'         => ['(ปี 02-05)'],
                              'city_y08_14'         => ['(ปี 08-14)'],
                              'city_y14_18'         => ['(ปี 14-18)'],
                              'city_y95_99'         => ['(ปี 95-99)'],
                              'type-z_y99_02'       => ['TYPE-Z (ปี 99-02)'],
                              'zx_y05_07'           => ['ZX (ปี 05-07)']     
                    ],
                    'civic'        =>[
                              '3dr_4dr'             => ['3Dr-4Dr เตารีด (ปี 92-95)'],
                              'coupre'              => ['COUPE (ปี 96-00)'],
                              'dimension_y00_04'    => ['Dimension (ปี 00-04)'],
                              'dimension_y04_06'    => ['Dimension (ปี 04-06)'],
                              'fb'                  => ['FB (ปี 12-16)'],
                              'fc'                  => ['FC (ปี 16-20)'],
                              'fc_fc'               => ['FC (ปี 16-20) FC (ปี 16-20)'],
                              'fd'                  => ['FD (ปี 05-12)'],
                              'fk2'                 => ['FK2 (ปี 16-20)'],
                              'big_eye'             => ['ตาโต (ปี 96-00)']
                              
                    ],
                    'cr_v'         =>[
                              'cr_V_y02_05'         => ['(ปี 02-06)'],
                              'cr_v_y06_12'         => ['(ปี 06-12)'],
                              'cr_v_y12_16'         => ['(ปี 12-16)'],
                              'cr_v_y17_21'         => ['(ปี 17-21)'],
                              'cr_v_y95_02'         => ['(ปี 95-02)']
                                
                    ],
                    'cr_x_del_sol' =>[
                              'cr_x_del_sol_y02_98' => ['(ปี 92-98)']                 
                    ],
                    'cr_z'         =>[
                              'cr_z_y10_16'         => ['(ปี 10-16)']                 
                    ],
                    'crossroad'    =>[
                              'crossroad_y08_10'    => ['(ปี 08-10)']                 
                    ],
                    'elysion'      =>[
                              'elysion_y04_13'      => ['(ปี 04-13)']                 
                    ],
                    'fit'          =>[
                              'fit_y08_14'          => ['(ปี 08-14)']
                    ],  
                    'freed'        =>[
                              'freed_y08_16'        => ['(ปี 08-16)']
                    ],
                    'hr_v'         =>[
                              'hr_v_y14_18'         => ['(ปี 14-18)']
                    ], 
                    'inspire'      =>[
                              'inspire_y95_98'      => ['(ปี 95-98)']
                    ],
                    'integra'      =>[
                              'dc2_y94_01'          => ['DC2 (ปี 94-01)'],
                              'dc5_y01_06'          => ['DC5 (ปี 01-06)']
                    ],
                    'jazz'         =>[
                              'jazz_y03_07'         => ['(ปี 03-07)'],
                              'jazz_y08_14'         => ['(ปี 08-14)'],
                              'jazz_y14_18'         => ['(ปี 14-18)']
                    ], 
                    'legend'       =>[
                              'legend_y04_12'       => ['(ปี 04-12)'],
                              'legend_y91_98'       => ['(ปี 91-98)'],
                              'legend_y95_04'       => ['(ปี 95-04)']
                    ],
                    'mobillio'     =>[
                              'mobillio_y14_17'     => ['(ปี 14-17)']                            
                    ],
                    'nsx'          =>[
                              'nsx_y16_20'          => ['(ปี 16-20)']                            
                    ],
                    'odyssey'      =>[
                              'odyssey_y03_08'      => ['(ปี 03-08)'],
                              'odyssey_y08_13'      => ['(ปี 08-13)'],  
                              'odyssey_y13_16'      => ['(ปี 13-16)'],  
                              'odyssey_y95_99'      => ['(ปี 95-99)'],  
                              'odyssey_y99_03'      => ['(ปี 99-03)'],                              
                    ],
                    'prelude'      =>[
                              'prelude_y91_98'      => ['(ปี 91-98)']                          
                    ],
                    's660'         =>[
                              's660_y15_18'         => ['(ปี 15-18)']                          
                    ], 
                    'stepwgn'      =>[
                              'stepwgn_y05_09'      => ['(ปี 05-09)']                          
                    ],
                    'stepwgn_spada'=>[
                              'stepwgn_spada_y09_16'=> ['(ปี 09-16)']                          
                    ],  
                    'stream'       =>[
                              'stream_y00_06'       => ['(ปี 00-06)']                          
                    ], 
                    'today'        =>[
                              'today_y85_98'        => ['(ปี 85-98)']                          
                    ], 
                
                ], 
                'toyota' => [
                    '4runner'      =>[
                              '4runner_y90_95'      => ['(ปี 90-95)']
                    ],
                    '86gt'         =>[
                              '86gt_y90_95'         => ['(ปี 90-95)']
                    ],
                    'alphard'      =>[
                              'alphard_y02_07'      => ['(ปี 02-07)'],
                              'alphard_y08_14'      => ['(ปี 08-14)'],
                              'alphard_y15_18'      => ['(ปี 15-18)']
                    ],
                    'aristo'       =>[
                              'aristo_y97_04'       => ['(ปี 97-04)']
                    ],
                    'avanza'       =>[
                              'aristo_y04_11'       => ['(ปี 04-11)'],
                              'aristo_y12_16'       => ['(ปี 12-16)']
                    ],
                    'avensis'      =>[
                              'avensis_y10_16'      => ['(ปี 10-16)']
                    ],  
                    'aygo'         =>[
                              'avensis_y15_20'      => ['(ปี 15-20)']
                    ], 
                    'bb'           =>[
                              'bb_y06_12'           => ['(ปี 06-12)']
                    ],  
                    'c_hr'         =>[
                              'c_hr_y17_21'         => ['(ปี 17-21)']
                    ],
                    'camry'        =>[
                              'year_2_6'            => ['(ปี 02-06)'],
                              'year_6_12'           => ['(ปี 06-12)'],
                              'year_12_16'          => ['(ปี 12-16)'],
                              'year_93_97'          => ['โฉมแรก (ปี 93-97)'],
                              'light_long'          => ['โฉมไฟท้ายยาว (ปี 98-00)'],
                              'light_long_sub'      => ['โฉมไฟท้ายย้อย (ปี 01-02)'],
                    ],
                    'carina'       =>[
                              'carina_y96_01'       => ['(ปี 96-01)']
                    ],
                    'celica'       =>[
                              'celica_y00_05'       => ['(ปี 00-05)'],
                              '4eye'                => ['4EYE ตากลม (ปี 94-99)'],
                              'pop_up'              => ['POP-UP (ปี 90-94)']
                    ],   
                    'celsior'      =>[
                              'celsior_y00_y06'     => ['(ปี 00-06)']
                    ],
                    'century'      =>[
                              'century_y97_15'      => ['(ปี 97-15)']
                    ],   
                    'corolla'      =>[
                              'corolla_y66_70'      => ['(ปี 66-70)'],
                              'hi_torque'           => ['HI-TORQUE (ปี 98-01)'],
                              'ke'                  => ['KE (ปี 74-83)'],
                              'ass_duck'            => ['ตูดเป็ด (ปี 95-99)'],
                              'three_circle'        => ['สามห่วง (ปี 91-96)'],
                              'Doremon'             => ['โดเรม่อน (ปี 88-92) (2)']
                    ],
                    'corolla_altis'=>[
                              'y14_18'              => ['(ปี 14-18)'],
                              'altis_y80_13'        => ['ALTIS (ปี 08-13)'],
                              'altis_y14_18'        => ['ALTIS (ปี 14-18)'],
                              'face_pig'            => ['ALTIS หน้าหมู (ปี 01-07)']
                    ],  
                    'corona'       =>[
                              'exsior'              => ['Exsior (ปี 96-99)'],
                              'tuyen'               => ['ตู้เย็น (ปี 83-89)'],
                              'end_separate'        => ['ท้ายแยก (ปี 93-97)'],
                              'end_high'            => ['ท้ายโด่ง (ปี 92-94)'],
                              'facegiant_facesmile' => ['หน้ายักษ์-หน้ายิ้ม (ปี 88-94)'],
                              'facesharp'           => ['หน้าแหลม (ปี 82-89)']
                    ],
                    'cressida'     =>[
                              'cressida_y88_92'     => ['(ปี 88-92)']
                    ],
                    'crown'        =>[
                              'crown_y00_05'        => ['(ปี 00-05)'],
                              'crown_y13_17'        => ['(ปี 13-17)'],
                              'lower_y2000'         => ['ต่ำกว่าปี 2000']
                    ],
                    'dyna'         =>[
                              'dyna_y95_02'         => ['(ปี 95-02)']
                    ],
                    'esquire'      =>[
                              'esquire_y14_17'      => ['(ปี 14-17)']
                    ],
                    'estima'       =>[
                              'estima_y00_05'       => ['(ปี 00-05)'],
                              'estima_y06_14'       => ['(ปี 06-14)'],
                              'estima_y16_19'       => ['(ปี 16-19)']
                    ],
                    'fj_cruiser'   =>[
                              'fj_cruiser_y07_15'   => ['(ปี 07-15)']
                    ],
                    'fortuner'     =>[
                              'fortuner_y04_08'     => ['(ปี 04-08)'],
                              'fortuner_y08_11'     => ['(ปี 08-11)'],
                              'fortuner_y12_15'     => ['(ปี 12-15)'],
                              'fortuner_y15_18'     => ['(ปี 15-18)'],
                              'fortuner_y1518'      => ['(ปี 15-18) (ปี 15-18)']
                    ],
                    'grand_hiace'  =>[
                              'grand_hiace_y92_02'  => ['(ปี 92-02)']
                    ],
                    'granvia'      =>[
                              'granvia_y95_02'      => ['(ปี 95-02)']
                    ],
                    'harrier'      =>[
                              'harrier_y03_13'      => ['(ปี 03-13)'],
                              'harrier_y14_17'      => ['(ปี 14-17)'],
                              'harrier_y97_03'      => ['(ปี 97-03)']
                    ],
                    'hiace'        =>[
                              'commuter_y05_16'     => ['COMMUTER (ปี 05-16)'],
                              'short_y05_16'        => ['ตัวเตี้ย (ปี 05-16'],
                              'rocket_head'         => ['หัวจรวด (ปี 92-04)']
                    ],
                    'hilux_hero'   =>[
                              'single'              => ['Single']
                    ],
                    'hilux_mighty_x'=>[
                              'double_cab'          => ['DOUBLE CAB'],
                              'extracab'            => ['EXTRACAB'],
                              'single'              => ['SINGLE']
                    ],
                    'hilux_revo'   =>[
                              'revo_double_cab'     => ['DOUBLE CAB'],
                              'revo_single'         => ['SINGLE'],
                              'revo_smartcab'       => ['SMARTCAB'],
                              'revo_smartcab_smartcab' => ['SMARTCAB SMARTCAB']
                    ],
                    'hilux_surf'   =>[
                              'surf_y88_97'         => ['(ปี 88-97)']
                    ],  
                    'hilux_tiger'  =>[
                              'tiger_doublecab'     => ['DOUBLE CAB'],
                              'tiger_extracab'      => ['EXTRACAB'],
                              'tiger_single'        => ['SINGLE'],
                              'sport_cruiser'       => ['SPORT CRUISER']
                    ],
                    'hilux_vigo'   =>[
                              'champ_double_cab'    => ['CHAMP DOUBLE CAB (ปี 11-15)'],
                              'champ_extracab'      => ['CHAMP EXTRACAB (ปี 11-15)'],
                              'champ_single'        => ['CHAMP SINGLE (ปี 11-15)'],
                              'champ_smartcab'      => ['CHAMP SMARTCAB (ปี 11-15)'],
                              'vigo_bdcab_y05_08'   => ['DOUBLE CAB (ปี 04-08)'],
                              'vigo_bdcab_y08_11'   => ['DOUBLE CAB (ปี 08-11)'],
                              'vigo_extracab_y04_08'=> ['EXTRACAB (ปี 04-08)'],
                              'vigo_extracab_y08_11'=> ['EXTRACAB (ปี 08-11)'],
                              'vigo_single_y04_08'  => ['SINGLE (ปี 04-08)'],
                              'vigo_single_y08_11'  => ['SINGLE (ปี 08-11)'],
                              'vigo_smartcab_y08_11'=> ['SMARTCAB (ปี 08-11)'],
                              'vigo_sportvan_y08_11'=> ['Sport Van (ปี 08-11)']
                    ],
                     'innova'     =>[
                              'innova_y04_11'       => ['(ปี 04-11)'],
                              'innova_y11_15'       => ['(ปี 11-15)'],
                              'innova_y16_20'       => ['(ปี 16-20)']
                    ],
                    'iq'          =>[
                              'iq_y08_15'           => ['(ปี 08-15)']
                    ],
                    'kluger'      =>[
                              'kluger_y01_07'       => ['(ปี 01-07)'] 
                    ],
                    'land_cruiser'=>[
                              'land_cruiser_43'     => ['43'],
                              'land_cruiser_45'     => ['45'],
                              'land_cruiser_70'     => ['70'],
                              'land_cruiser_80'     => ['80'],
                              'land_cruiser_100'    => ['100'],
                              'land_cruiser_200'    => ['200'] 
                    ],
                    'landcruiser_prado'=>[
                              'land_cruiser_prado_90'     => ['90'],
                              'land_cruiser_prado_120'     => ['120'],
                              'land_cruiser_prado_150'     => ['150'],
    
                    ],
                    'ln'          =>[
                              'all_nickname'        => ['รวมทุกโฉม']
                    ],
                    'lucida'      =>[
                              'lucida_y90_99'       => ['(ปี 90-99)']
                    ],
                    'mark_2'      =>[
                              'mark_2_y92-96'       => ['(ปี 92-96)']
                    ], 
                    'mark_x'      =>[
                              'mark_x_y04_10'       => ['(ปี 04-10)']
                    ], 
                    'mega_cruiser'=>[
                              'mega_cruiser_y95_02' => ['(ปี 95-02)']
                    ],  
                    'mr_s'        =>[
                              'mr_s_y00_07'         => ['(ปี 00-07)']
                    ], 
                    'mr_2'        =>[
                              'mr_2_y89_99'         => ['(ปี 89-99)']
                    ], 
                    'noah'        =>[
                              'noah_y01_13'         => ['(ปี 01-13)'], 
                              'noah_y14_17'         => ['(ปี 14-17)']  
                    ], 
                    'opa'         =>[
                              'noah_y00_05'         => ['(ปี 00-05)']
                    ], 
                    'porte'       =>[
                              'porte_y05_15'        => ['(ปี 05-15)']
                    ],
                    'prius'       =>[
                              'prius_y09_16'        => ['(ปี 09-16)']
                    ],
                    'rav4'        =>[
                              'rav4_y02_12'         => ['(ปี 02-12)'],
                              'rav4_y95_02'         => ['(ปี 95-02)']
                    ], 
                    'sienta'      =>[
                              'sienta_y16_20'       => ['(ปี 16-20)']
                    ], 
                    'soarer'      =>[
                              'soarer_y01_05'       => ['(ปี 01-05)'],
                    ], 
                    'soluna'      =>[
                              'al50_first_nickname' => ['AL50 โฉมแรก (ปี 97-00)'], 
                              'al50_droplet_y00_03' => ['AL50 ไฟท้ายหยดน้ำ (ปี 00-03)'],   
                              'droplet_al50_y00_03' => ['ไฟท้ายหยดน้ำAL50 (ปี 00-03)']
                    ],
                    'sport_cruiser'=>[
                              'sport_cruiser_y00_04'=> ['(ปี 00-04)']    
                    ],
                    'sport_rider' =>[
                              'sport_rider_y98_02'  => ['(ปี 98-02)'],
                              'd4d_y02_04'          => ['D4D (ปี 02-04)']     
                    ], 
                    'starlet'     =>[
                              'starlet_ep71'        => ['EP71 (ปี 84-89)'],
                              'starlet_ep82'        => ['EP82 (ปี 89-97)']     
                    ],
                    'super_custom'=>[
                              'super_custom_y96_04' => ['(ปี 96-04)']
                    ],
                    'super_wagon' =>[
                              'super_wagon_y96_04'  => ['(ปี 96-04)']
                    ],
                    'supra'       =>[
                              'supra_y02_02'        => ['(ปี 92-02)']
                    ], 
                    'vellfire'    =>[
                              'vellfire_y08_14'     => ['(ปี 08-14)'],
                              'vellfire_y15_18'     => ['(ปี 15-18)'] 
                    ],
                    'ventury'     =>[
                              'ventury_y05_16'      => ['(ปี 05-16)']     
                    ],
                    'vios'        =>[
                              'vios_y02_07'         => ['(ปี 02-07)'],
                              'vios_y07_13'         => ['(ปี 07-13)'], 
                              'vios_y13_17'         => ['(ปี 13-17)'],  
                              'vios_y1317'          => ['(ปี 13-17) (ปี 13-17)']   
                    ],
                    'vista'       =>[
                              'vista_y90_91'        => ['(ปี 90-94)']      
                    ],
                    'vitz'        =>[
                              'vitz_y00_05'         => ['(ปี 00-05)']          
                    ],
                    'voxy'        =>[
                              'voxy_y14_17'         => ['(ปี 14-17)']          
                    ],
                    'wish'        =>[
                              'wish_y03_10'         => ['(ปี 03-10)']         
                    ],
                    'yaris'       =>[
                              'yaris_y06_13'        => ['(ปี 06-13)'],
                              'yaris_y13_17'        => ['(ปี 13-17)'] 
                    ],
                    'yaris_ativ'  =>[
                              'yaris_ativ_y17_21'   => ['(ปี 17-21)']         
                    ],
                
                ],
                'isuzu'  => [
                    'adventure'  =>[
                              'adventure_y96_00'    => ['(ปี 96-00)']         
                    ], 
                    'adventure_master'=>[
                              'amaster_y02_06'      => ['(ปี 02-06)']         
                    ],
                    'cameo'      =>[
                              'cameo_y91_97'        => ['(ปี 91-97)']         
                    ],
                    'd_max'      =>[
                              'cab_4_y02_06'        => ['CAB-4 (ปี 02-06)'],
                              'cab_4_y07_11'        => ['CAB-4 (ปี 07-11)'],
                              'cab_4_y11_17'        => ['CAB-4 (ปี 11-17)'],
                              'spacecab_y02_06'     => ['SPACE CAB (ปี 02-06)'],
                              'spacecab_y07_11'     => ['SPACE CAB (ปี 07-11)'],
                              'spacecab_y11_17'     => ['SPACE CAB (ปี 11-17)'], 
                              'spacecab_y1117'      => ['SPACE CAB (ปี 11-17) SPACE CAB (ปี 11-17)'],  
                              'spark_y02_96'        => ['SPARK (ปี 02-06)'],  
                              'spark_y07_11'        => ['SPARK (ปี 07-11)'],  
                              'spark_y11_17'        => ['SPARK (ปี 11-17)']            
                    ], 
                    'dragon_eyes'=>[
                              'dragon_eyes_spacecab'=> ['SPACE CAB'],
                              'spacecab_y96_99'     => ['SPACE CAB (ปี 96-99)'],
                              'spark_y96_99'        => ['SPARK (ปี 96-99)']         
                    ], 
                    'dragon_power'=>[
                              'dragonpower_cab4'    => ['CAB-4'],
                              'cab4_y00_02'         => ['CAB-4 (ปี 00-02)'],
                              'dragonpower_spacecab'=> ['SPACE CAB'],
                              'spacecab_y00_02'     => ['SPACE CAB (ปี 00-02)'], 
                              'spark_y00_02'        => ['SPARK (ปี 00-02)']         
                    ], 
                    'elf'        =>[
                              'elf_all'             => ['รวมทุกโฉม']         
                    ],
                    'grand_adventure'=>[
                              'g_adventure_y_96_02' => ['(ปี 96-02)']         
                    ],
                    'mu_7'       =>[
                              'mu_7_y04_06'         => ['(ปี 04-06)'], 
                              'mu_7_y07_13'         => ['(ปี 07-13)']        
                    ],
                    'mu_x'       =>[
                              'mu_x_y13_17'         => ['(ปี 13-17)'], 
                              'mu_x_1317'           => ['(ปี 13-17) (ปี 13-17)']        
                    ],
                    'trf'        =>[
                              'dragon_gold'         => ['มังกรทอง']    
                    ],
                    'trooper'    =>[
                              'trooper_y91_03'      => ['(ปี 91-03)']    
                    ],
                    'vega'       =>[
                              'vega_y97_03'         => ['(ปี 97-03)']    
                    ],

                ],
                'nissan' => [
                    '200sx'      =>[
                              '200sx_y92_95'        => ['(ปี 92-95)']         
                    ],
                    '300zx'      =>[
                              '300zx_y92_98'        => ['(ปี 92-98)']         
                    ],
                    '350z'       =>[
                              '350z_y03_09'         => ['(ปี 03-09)']         
                    ],
                    '370z'       =>[
                              '300zx_y09_15'        => ['(ปี 09-15)']         
                    ], 
                    'almera'     =>[
                              'almera_y11_16'       => ['(ปี 11-16)'],
                              'almera_y17'          => ['(ปี 17)']        
                    ],
                    'bigm'       =>[
                              'bigm_4dr'            => ['4Dr'],
                              'bigm_kingcab'        => ['KING CAB'], 
                              'bigm_single'         => ['SINGLE']          
                    ],
                    'bluebird_atessa'=>[
                              'bluebird_a_y93_97'   => ['(ปี 93-97)']         
                    ],
                    'cedric'     =>[
                              'cedric_y92_95'       => ['(ปี 92-95)']
                    ],
                    'cefiro'     =>[
                              'A31'                 => ['A31 (ปี 90-95)'],
                              'A32'                 => ['A32 (ปี 96-02)'],
                              'A33'                 => ['A33 (ปี 01-04)']
                    ],
                    'cube'       =>[
                              'cube_y02_08'         => ['(ปี 02-08)'],
                              'cube_y02_15'         => ['(ปี 08-15)']
                    ],
                    'elgrand'    =>[
                              'elgrand_y02_10'      => ['(ปี 02-10)'],
                              'elgrand_y10_15'      => ['(ปี 10-15)'],
                              'elgrand_97_02'       => ['(ปี 97-02)'] 
                    ],  
                    'figaro'     =>[
                              'figaro_y91_95'       => ['(ปี 91-95)']
                    ],
                    'frontier'   =>[
                              'frontier_4dr'        => ['4DR'],
                              'frontier_kingcab'    => ['KING CAB'],
                              'frontier_single'     => ['SINGLE']
                    ],
                    'frontier_nacara'=>[
                              'f_nacara_4dr'        => ['4DR'],
                              'f_nacara_kingcab'    => ['KING CAB'],
                              'f_nacara_single'     => ['SINGLE']
                    ],
                     'gt_r'      =>[
                              'gt_r_y08_15'         => ['(ปี 08-15)']
                    ],  
                    'infiniti_r' =>[
                              'infinit_r_y92_97'    => ['(ปี 92-97)']
                    ],
                    'juke'       =>[
                              'juke_y10_16'         => ['(ปี 10-16)']
                    ],
                    'leaf'       =>[
                              'leaf_y12_16'         => ['(ปี 12-16)']
                    ],
                    'livina'     =>[
                              'livina_y14_17'       => ['(ปี 14-17)']
                    ], 
                    'march'      =>[
                              'march_y10_16'        => ['(ปี 10-16)']
                    ],  
                    'murano'     =>[
                              'murano_y04_10'       => ['(ปี 04-10)']
                    ],
                    'note'       =>[
                              'note_y17_21'         => ['(ปี 17-21)']
                    ],
                    'np_300_navara'=>[
                              'np300_dbcab'         => ['DOUBLE CAB'],
                              'np_300_kcab'         => ['KING CAB'],
                              'np_300_single'       => ['SINGLE']
                    ],
                    'nv'         =>[
                              'nv_queencab'         => ['QUEENCAB'],
                              'nv_single'           => ['SINGLE'],
                              'nv_wingroad'         => ['WINGROAD ']
                    ],
                    'nx'         =>[
                              'nx_y92_95'           => ['(ปี 92-95)']
                    ], 
                    'pao'        =>[
                              'pao_y89_92'          => ['(ปี 89-92)']
                    ], 
                    'presea'     =>[
                              'presea_y92_97'       => ['(ปี 92-97)']
                    ], 
                    'president'  =>[
                              'president_y93_95'    => ['(ปี 93-95)']
                    ], 
                    'primera'    =>[
                              'primera_y97_99'      => ['(ปี 97-99)']
                    ], 
                    'pulsar'     =>[
                              'pulsar_y12_16'       => ['(ปี 12-16)']
                    ],       
                    'sentra'     =>[
                              'sentra_y92_96'       => ['(ปี 92-96)']
                    ], 
                    'serena'     =>[
                              'serena_y00_05'       => ['(ปี 00-05)']
                    ],   
                    'silvia'     =>[
                              'silvia_y00_05'       => ['(ปี 00-05)']
                    ],   
                    'skyline'    =>[
                              'skyline_r33'         => ['R33 (ปี 94-99)'],
                              'skyline_v35'         => ['V35 (ปี 02-07)'],
                              'skyline_v36'         => ['V36 (ปี 07-15)']
                    ], 
                    'sunny'      =>[
                              'b14_15'              => ['B14-15 (ปี 94-00)'],
                              'sunny_neo'           => ['NEO (ปี 01-04)'],
                              'sunny_neo_y04_06'    => ['NEO ท้ายแตงโม (ปี 04-06)']
                    ], 
                    'sylphy'     =>[
                              'sylphy_y12_16'       => ['(ปี 12-16)']
                    ],   
                    'teena'      =>[
                              'teena_y04_08'        => ['(ปี 04-08)'],
                              'teena_y09_13'        => ['(ปี 09-13)'],
                              'teena_13_16'         => ['(ปี 13-16)']
                    ],
                    'terrano_ii' =>[
                              'terrano_ii_y95_00'   => ['(ปี 95-00)']
                    ],
                    'tiida'      =>[
                              'tiida_y06_12'        => ['(ปี 06-12)']
                    ],  
                    'urvan'      =>[
                              'urvan_y01_12'        => ['(ปี 01-12)'],
                              'urvan_y13_17'        => ['(ปี 13-17)'],
                              'urvan_y90_01'        => ['(ปี 90-01)']
                    ],   
                    'x-trail'    =>[
                              'x-trail_y02_08'      => ['(ปี 02-08)'],
                              'x-trail_y08_13'      => ['(ปี 08-13)'],
                              'x-trail_y14_17'      => ['(ปี 14-17)'],
                              'x-trail_15_19'       => ['(ปี 15-19)']
                    ],
                    'xciter'      =>[
                              'xciter_y01_06'       => ['(ปี 01-06)']
                    ],

                ],

                'mitsubishi' => [
                    '300gt'      =>[
                              '300gt_y92_97'         => ['(ปี 92-97)']
                    ],   
                    'attrage'    =>[
                              'attrage_y13_16'       => ['(ปี 13-16)']
                    ],
                    'canter'     =>[
                              'all_canter'           => ['รวมทุกโฉม']
                    ],
                    'cyblone'    =>[
                              'aero_body_y09_05'     => ['AERO BODY (ปี 89-95)'],
                              'cyblone_single'       => ['SINGLE (ปี 89-95)']
                    ],   
                    'delica_space_wagon'=>[
                              'aero_body_y15_18'     => ['(ปี 15-18)']
                    ],
                    'evolution'  =>[
                              'iv_face_benz'         => ['IV (โฉมท้ายเบนซ์)'],
                              'ix_lancer'            => ['IX (โฉมแลนเซอร์)'],
                              'v_end_benz'           => ['V (โฉมท้ายเบนซ์)'],
                              'vi_end_benz'          => ['VI (โฉมท้ายเบนซ์)'],
                              'vii_Cedia'            => ['VII (โฉมซีเดีย)'],
                              'viii_lancer'          => ['VIII (โฉมแลนเซอร์)'],
                              'x_lancer_ex'          => ['X (โฉมแลนเซอร์ EX)']
                    ],  
                    'galant'     =>[
                              'galant_y76_80'        => ['(ปี 76-80)'],
                              'galant_y80_87'        => ['(ปี 80-87)'],
                              'galant_y92_99'        => ['(ปี 92-99)']
                    ], 
                    'l300'       =>[
                              'l300_y92_99'          => ['(ปี 92-99)']
                    ],
                    'lancer'     =>[
                              'lancer_y04_12'        => ['(ปี 04-12)'],
                              'cedia_y01_04'         => ['CEDIA (ปี 01-04)'],
                              'champ_i6_y84_85'      => ['CHAMP I-II-III (ปี 84-95)'],
                              'ecar_y92_96'          => ['E-CAR (ปี 92-96)'],
                              'f_style_endbenz'      => ['F Style ท้ายเบนซ์ (ปี 96-02)']
                    ],   
                    'lancer_ex'  =>[
                              'lancer_ex_y09_15'     => ['(ปี 09-15)']
                    ],
                    'mirage'     =>[
                              'mirage_y12_16'        => ['(ปี 12-16)']
                    ], 
                    'pajero'     =>[
                              'pajero_y00_06'        => ['(ปี 00-06)'],
                              'pajero_y08_15'        => ['(ปี 08-15)'],
                              'pajero_y92_02'        => ['(ปี 92-02)']
                    ],
                    'pajero_io'  =>[
                              'pajero_io_y98_07'     => ['(ปี 98-07)']
                    ],
                    'pajero_junior'=>[
                              'pajero_junior_y95_98' => ['(ปี 95-98)']
                    ],
                    'pajero_sport'=>[
                              'pajero_sport_y08_15'  => ['(ปี 08-15)'],
                              'pajero_sport_y15_18'  => ['(ปี 15-18)']
                    ],
                    'space_runner'=>[
                              'space_runner_y92_00'  => ['(ปี 92-00)']
                    ], 
                    'space_wagon'=>[
                              'space_wagon_y04_12'   => ['(ปี 04-12)']
                    ],   
                    'strada'     =>[
                              'strada_grandis4dr'    => ['GRANDIS 4DR'],
                              'strada_megacab'       => ['MEGA CAB'],
                              'strada_sngle'         => ['SINGLE']
                    ],
                    'strada_g_wagon'=>[
                              'strada_g_wagon_y01_06'=> ['(ปี 01-06)']
                    ], 

                    'triton'=>[
                              'double_cab_y05_15'   => ['(ปี 05-15)'],
                              'double_cab_y14_19'   => ['(ปี 14-19)'],
                              'mega_cab_y05_15'     => ['(ปี 05-15)'],
                              'mega_cab_y14_19'     => ['(ปี 14-19)'],
                              'megacab_y05_15'      => ['(ปี 05-15)'],
                              'megacab_y14_19'      => ['(ปี 14-19)'],
                              'single_y05_15'       => ['(ปี 05-15)'],
                              'single_y14_19'       => ['(ปี 14-19)']
                    ], 

                ],


                'mazda' => [
                    '2'      =>[
                              '2_y09_14'            => ['(ปี 09-14)'],
                              '2_y15_18'            => ['(ปี 15-18)']
                    ],
                    '3'      =>[
                              '3_y05_10'            => ['(ปี 05-10)'],
                              '3_y11_14'            => ['(ปี 11-14)'],
                              '3_y14_17'            => ['(ปี 14-17)']
                    ],      
                    '6'    =>[
                              '6_y02_08'            => ['(ปี 02-08)']
                    ],
                    '121'     =>[
                              '121_y92_96'          => ['(ปี 92-96)']
                    ],
                    '323'    =>[
                              '323_y95_98'          => ['(ปี 95-98)'],
                              'protege_y00_06'      => ['Protege (ปี 00-06)']
                    ],   
                    '626'=>[    
                              '626_y97_00'          => ['(ปี 97-00)'],
                              'cronos_y92_97'       => [' Cronos (ปี 92-97)']
                    ],
                    'bt_50'  =>[
                              'double_cab'          => ['DOUBLE CAB'],
                              'free_style_cab'      => ['FREE STYLE CAB'],
                    ],  
                    'bt_50_pro'     =>[
                              'double_cab'          => ['DOUBLE CAB'],
                              'free_style_cab'      => ['FREE STYLE CAB'],
                              'single'              => ['SINGLE'],
                    ], 
                    'cx_3'       =>[
                              'y15_19'              => ['(ปี 15-19)']
                    ],
                    'cx_5'       =>[
                              'y13_16'              => ['(ปี 13-16)'],
                              'y17_20'              => ['(ปี 17-20)']
                    ],
                    'famillia'       =>[
                              'single'              => ['SINGLE'],
                              'super_cab'           => ['SUPER CAB'],
                    ],
                    'fighter'       =>[
                              'double_cab'          => ['DOUBLE CAB'],
                              'free_style_cab'      => ['FREE STYLE CAB'],
                              'super_saloon'        => ['SUPER SALOON'],
                    ],
                    'lantis'       =>[
                              'y94_00'              => ['(ปี 94-00)'],
                    ],
                    'magnum_thunder' =>[
                              'y92_98'              => ['(ปี 92-98)'],
                    ],
                    'mpv'       =>[
                              'y02_06'              => ['(ปี 02-06)'],
                    ],
                    'mx_5'       =>[
                              'y01_05'              => ['(ปี 01-05)'],
                              'y05_15'              => ['(ปี 05-15)'],
                              'y16_20'              => ['(ปี 16-20)'],
                              'y92_98'              => ['(ปี 92-98)'],
                    ],
                    'rx_7'       =>[
                              'y92_00'              => ['(ปี 92-00)'],
                    ],
                    'rx_8'       =>[
                              'y03_08'              => ['(ปี 03-08)'],
                    ],
                    'tribute'       =>[
                              'y02_06'              => ['(ปี 02-06)'],
                    ],

                ],


                'ford' => [
                    'aspire'      =>[
                              'y94_97'              => ['(ปี 94-97)'],
                    ],
                    'courier'      =>[
                              'y77_85'              => ['(ปี 77-85)'],
                    ],      
                    'ecosport'    =>[
                              'y13_16'              => ['(ปี 13-16)']
                    ],
                    'escape'     =>[
                              'y03_07'              => ['(ปี 03-07)'],
                              'y09_12'              => ['(ปี 09-12)']
                    ],
                    'everest'    =>[
                              'y03_06'              => ['(ปี 03-06)'],
                              'y07_13'              => ['(ปี 07-13)'],
                              'y15_18'              => ['(ปี 15-18)'],
                              'y15_18_2'            => ['(ปี 15-18)(ปี 15-18)']
                    ],   
                    'festiva'=>[    
                              'y92_96'              => ['(ปี 92-96)'],
                    ],
                    'fiesta'  =>[
                              'y10_16'              => ['(ปี 10-16)']
                    ],  
                    'focus'     =>[
                              'y04_08'              => ['(ปี 04-08)'],
                              'y09_12'              => ['(ปี 09-12)'],
                              'y12_16'              => ['(ปี 12-16)']
                    ], 
                    'laser'       =>[
                              'y00_05'              => ['(ปี 00-05)'],
                              'y91_94'              => ['(ปี 91-94)']
                    ],
                    'lincoln_continental'=>[
                              'y70_79'              => ['(ปี 70-79)']
                    ],
                    'mustang'       =>[
                              'y05_15'              => ['(ปี 05-15)'],
                              'y15_20'              => ['(ปี 15-20)']
                    ],
                    'ranger'       =>[
                              'double_cab_y03_05'           => ['DOUBLE CAB (ปี 03-05)'],
                              'double_cab_y06_08'           => ['DOUBLE CAB (ปี 06-08)'],
                              'double_cab_y09_12'           => ['DOUBLE CAB (ปี 09-12)'],
                              'double_cab_y12_15'           => ['DOUBLE CAB (ปี 12-15)'],
                              'double_cab_y15_18'           => ['DOUBLE CAB (ปี 15-18)'],
                              'double_cab_y99_02'           => ['DOUBLE CAB (ปี 99-02)'],
                              'open_cab_y03_05'             => ['OPEN CAB (ปี 03-05)'],
                              'open_cab_y06_08'             => ['OPEN CAB (ปี 06-08)'],
                              'open_cab_y09_12'             => ['OPEN CAB (ปี 09-12)'],
                              'open_cab_y12_15'             => ['OPEN CAB (ปี 12-15)'],
                              'open_cab_y15_18'             => ['OPEN CAB (ปี 15-18)'],
                              'single_cab_y03_05'           => ['SINGLE CAB (ปี 03-05)'],
                              'single_cab_y12_15'           => ['SINGLE CAB (ปี 12-15)'],
                              'single_cab_y15_18'           => ['SINGLE CAB (ปี 15-18)'],
                              'single_cab_y99_02'           => ['SINGLE CAB (ปี 99-02)'],
                              'super_cab_y03_05'            => ['SUPER CAB (ปี 03-05)'],
                              'super_cab_y09_12'            => ['SUPER CAB (ปี 99-02)'],
                    ],
                    'territory'       =>[
                              'y12_16'                      => ['(ปี 12-16)'],
                    ],
                    

                ],

            ], //ปิดnickname


            'variant' =>[
                'honda' => [
                    'accord'  =>[
                                'year_3_7'              => [
                                      '2_0_e_i_vtec'    => ['2.0 E i-VTEC'],
                                      '2_4_e_i_vtec'    => ['2.4 E i-VTEC'],
                                      '2_4_el'          => ['2.4 EL'],
                                      '2_4_el_i_vtec'   => ['2.4 EL i-VTEC'],
                                      '2_4_s_i_vtec'    => ['2.4 S i-VTEC'],
                                      '3_0_v6_i_vtec'   => ['3.0 V6 i-VTEC'],
                                ],
                                'year_7_13'             => [
                                      '2_0_e_'          => ['2.0 E'],
                                      '2_0_e_i_vtec'    => ['2.0 E i-VTEC'],
                                      '2_0_el_i_vtec'   => ['2.0 EL i-VTEC'],
                                      '2_0_jp'          => ['2.0 JP'],
                                      '2_4_e_i_vtec'    => ['2.4 E i-VTEC'],
                                      '2_4_el_i_vtec'   => ['2.4 EL i-VTEC'],
                                      '2_4_el_navi'     => ['2.4 EL NAVI'],
                                      '2_4_jp'          => ['2.4 JP'],
                                      '3_5_v6_i_vtec'   => ['3.5 V6 i-VTEC'],

                                ],
                                'year_13_17'            => [
                                        '2_0_e_i_vtec'          => ['2.0 E i-VTEC'],
                                        '2_0_el'                => ['2.0 EL'],
                                        '2_0_el_i_vtec'         => ['2.0 EL i-VTEC'],
                                        '2_0_el_navi'           => ['2.0 EL NAVI'],
                                        '2_0_hybird_i_vtec'     => ['2.0 Hybrid i-VTEC'],
                                        '2_0_hybird_tech_i_vtec'=> ['2.0 Hybrid TECH i-VTEC'],
                                        '2_4_el'                => ['2.4 EL'],
                                        '2_4_el_i_vtec'         => ['2.4 EL i-VTEC'],
                                        '2_4_el_navi'           => ['2.4 EL NAVI'],
                                        '2_4_tech'              => ['2.4 TECH'],
                                ],
                                'accord_snake'          => [
                                        '2_3_exi'               => ['2.3 EXi'],
                                        '2_3_millennium_vti'    => ['2.3 Millennium VTi'],
                                        '2_3_vti'               => ['2.3 VTi'],
                                        '2_3_vti_prestige'      => ['2.3 VTi Prestige'],
                                        '3_0_v6'                => ['3.0 V6'],
                                        '3_0_vti_prestige'      => ['3.0 VTi Prestige'],
                                ],
                                'accord_eye'            => [
                                        '2_0_ex'                => ['2.0 EX'],
                                        '2_0_exi'               => ['2.0 Exi'],
                                        '2_3_lxi'               => ['2.0 LXi'],
                                ],
                                'accord_abcd'            => [
                                        '2_2_exi'        => ['2.2 EXi'],
                                        '2_2_vti_ex'     => ['2.2 VTi EX'],
                                        '2_2_vti_lx'     => ['2.2 VTi LX'],

                                ],

                    ],

                    'br_v' =>[
                                'br_v_y16_20'   => [
                                    '1_5_sv'        => ['1.5 SV'],
                                    '1_5_v'         => ['1.5 V'],
                                ],
                    ],

                    'brio' =>[
                                'brio_y11_16'           => [
                                        '1_2_amaze_s'   => ['1.2 Amaze S'],
                                        '1_2_amaze_sv'  => ['1.2 Amaze SV'],
                                ],
                    ],

                    'city' =>[
                        'city_y02_05'                   => [
                                '1_5_a_i-dsi'           => ['1.5 A i-DSi'],
                                '1_5_e_i-dsi'           => ['1.5 E i-DSi'],
                                '1_5_e_v_vtec'          => ['1.5 E-V VTEC'],
                                '1_5_si_dsi'            => ['1.5 S i-DSi'],
                         ],

                        'city_y08_14'                        => [
                                '1_5_modulo'                 => ['1.5 MODULO'],
                                '1_5_s'                      => ['1.5 S'],
                                '1_5_s_cng'                  => ['1.5 S CNG'],
                                '1_5_s_i_vtec'               => ['1.5 S i-VTEC'],
                                '1_5_sv'                     => ['1.5 SV'],
                                '1_5_sv_i_vtec'              => ['1.5 SV i-VTEC'],
                                '1_5_v'                      => ['1.5 V'],
                                '1_5_v_cng '                 => ['1.5 V CNG '],
                                '1_5_v_i-vtec'               => ['1.5 V i-VTEC'],
                                '1_5_v-i_vtec_modulo'        => ['1.5 V i-VTEC Modulo'],
                                '1_5_v_i-vtec_society'       => ['1.5 V i-VTEC Society'],
                                '1_5_v_wise_edition_i_vtec'  => ['1.5 V Wise Edition i-VTEC'],
                            ],

                        'city_y14_18'                   => [
                                '1_5_s_cng'             => ['1.5 S CNG'],
                                '1_5_s_i_vtec'          => ['1.5 S i-VTEC'],
                                '1_5_sv_i_vtec'         => ['1.5 SV i-VTEC'],
                                '1_5_sv_plus_i_vtec'    => ['1.5 SV+ i-VTEC'],
                                '1_5_v_cng'             => ['1.5 V CNG'],
                                '1_5_v_i_vtec'          => ['1.5 V i-VTEC'],
                                '1_5_v_plus_i_vtec'     => ['1.5 V+ i-VTEC'],
                            ],


                        'city_y95_99'                   => [
                                '1_3_exi'               => ['1.3 EXi'],
                                '1_5_exi'               => ['1.5 EXi'],
                                '1_5_lxi'               => ['1.5 LXi'],
                            ],

                        'type-z_y99_02'                 => [
                                '1_5_exi'               => ['1.5 EXi'],
                                '1_5_type_z_exi'        => ['1.5 Type-Z EXi'],
                                '1_5_type_z_lxi'        => ['1.5 Type-Z LXi'],
                                '1_5_type_z_vti'        => ['1.5 Type-Z VTi'],
                            ],

                        'zx_y05_07'                     => [
                                '1_5_zx_a_i_dsi'        => ['1.5 ZX A i-DSi'],
                                '1_5_zx_ev_vtec'        => ['1.5 ZX EV VTEC'],
                                '1_5_zx_sv_vtec'        => ['1.5 ZX SV VTEC'],
                                '1_5_zx_v_vtec'         => ['1.5 ZX V VTEC'],
                            ],
                    ],


                    'civic' =>[
                        '3dr_4dr'                       => [
                                '1_5_ex'                => ['1.5 EX'],
                                '1_5_lx'                => ['1.5 LX'],
                                '1_6_exi'               => ['1.6 EXi'],
                                '1_6_lxi'               => ['1.6 LXi'],
                                '1_6_vti_ex'            => ['1.6 VTi EX'],
                                '1_6_vti_lx'            => ['1.6 VTi LX'],
                            ],

                        'coupre'                        => [
                                '1_6_vti'               => ['1.6 VTi'],
                                '1_7_vti'               => ['1.7 VTi'],
                                '1_6_vti_exa'           => ['1.6 VTi EXA'],
                            ],

                        'dimension_y00_04'              => [
                                '1_7_exi'               => ['1.7 EXi'],
                                '1_7_vti'               => ['1.7 VTi'],
                                '2_0_excites_i_vtec'    => ['2.0 Excites i-VTEC'],
                            ],

                        'dimension_y04_06'                    => [
                                '1_7_dimension_rx_sports'     => ['1.7 Dimension RX Sports'],
                                '1_7_exclusive_exi'           => ['1.7 Exclusive EXi'],
                                '1_7_exi'                     => ['1.7 EXi'],
                                '1_7_rx_sports_vtec'          => ['1.7 RX Sports VTEC'],
                                '1_7_vti'                     => ['1.7 VTi'],
                                '2_0_excites'                 => ['2.0 Excites'],
                                '2_0_excites_i_vtec'          => ['2.0 Excites i-VTEC'],
                            ],

                        'fb'                            => [
                                '1_5_hybrid'            => ['1.5 Hybrid (18)'],
                                '1_8_e_i_vtec'          => ['1.8 E i-VTEC'],
                                '1_8_e_modulo'          => ['1.8 E Modulo'],
                                '1_8_es_i_vtec'         => ['1.8 ES i-VTEC'],
                                '1_8_modulo'            => ['1.8 Modulo'],
                                '1_8_s_i_vtec'          => ['1.8 S i-VTEC'],
                                '2_0_el_i-vtec'         => ['2.0 EL i-VTEC'],
                            ],

                        'fc'                            => [
                                '1_5_turbo'             => ['1.5 Turbo'],
                                '1_5_turbo_rs'          => ['1.5 Turbo RS'],
                                '1_8_e_i-vtec'          => ['1.8 E i-VTEC'],
                                '1_8_el_i-vtec'         => ['1.8 EL i-VTEC'],
                            ],

                        'fc_fc'                         => [
                                '1_5_turbo'             => ['1.5 Turbo'],
                                '1_5_turbo_rs'          => ['1.5 Turbo RS'],
                                '1_8_e_i-vtec'          => ['1.8 E i-VTEC'],
                                '1_8_el_i-vtec'         => ['1.8 EL i-VTEC'],
                            ],

                        'fd'                                    => [
                                '1_8_e_i_vtec'                  => ['1.8 E i-VTEC'],
                                '1_8_e_sport_pearl'             => ['1.8 E Sport Pearl'],
                                '1_8_e_wise_edition_i-vtec'     => ['1.8 E Wise Edition i-VTEC'],
                                '1_8_s_i_vtec'                  => ['1.8 S i-VTEC'],
                                '2_0_el_i_vtec'                 => ['2.0 EL i-VTEC'],
                            ],

                        'fk'                            => [
                                '1_5_turbo'             => ['1.5 Turbo'],
                            ],

                        'fk2'                           => [
                                '2_0_type_r'            => ['2.0 Type R'],
                            ],

                        'big_eye'                       => [
                                '1_6_exi'               => ['1.6 EXi'],
                                '1_6_lxi'               => ['1.6 LXi'],
                                '1_6_vti'               => ['1.6 VTi'],
                                '1_6_vti_ex'            => ['1.6 VTi EX'],
                                '1_6_vti_exa'           => ['1.6 VTi EXA'],
                                '1_6_vti_lx'            => ['1.6 VTi LX'],
                                '1_8_vti'               => ['1.8 VTi'],
                            ],

                        'back_light'                    => [
                                '1_5_ex'                => ['1.5 EX'],

                            ],
                    ],
                



                    'cr_v' =>[
                        'cr_V_y02_06'                   => [
                                '2_0_e'                 => ['2.0 E'],
                                '2_0_ef'                => ['2.0 EF'],
                                '2_0_s'                 => ['2.0 S'],
                                '2_0_sf'                => ['2.0 SF'],
                                '2_4_el'                => ['2.4 EL'],
                                '2_4_elf'               => ['2.4 ELF'],
                            ],

                        'cr_v_y06_12'                   => [
                                '2_0_e'                 => ['2.0 E'],
                                '2_0_s'                 => ['2.0 S'],
                                '2_4_el'                => ['2.4 EL'],
                                '2_4_el_prestige'       => ['2.4 EL Prestige'],
                            ],

                        'cr_v_y12_16'                   => [
                                '2_0_e'                 => ['2.0 E'],
                                '2_0_s'                 => ['2.0 S'],
                                '2_4_el'                => ['2.4 EL'],
                            ],

                        'cr_v_y17_21'                   => [
                                '1_6_dt_e'              => ['1.6 DT E'],
                                '1_6_dt_el'             => ['1.6 DT EL'],
                                '2_4_e'                 => ['2.4 E'],
                                '2_4_el'                => ['2.4 EL'],
                            ],

                        'cr_v_y95_02'                   => [
                                '2_0_exi'               => ['2.0 EXi '],
                                '2_0_exi_limited'       => ['2.0 EXi Limited'],

                            ],
                    ],


                    'cr_x_del_sol' =>[
                        'cr_x_del_sol_y02_98'           => [
                                '1_6'                   => ['1.6'],
                            ],
                    ],


                    'cr_z' =>[
                        'cr_z_y10_16'                   => [
                                '1_5_jp'                => ['1.5 JP'],
                            ],
                    ],

                    'crossroad' =>[
                        'crossroad_y08_10'              => [
                                '2_0'                   => ['2.0'],
                            ],
                    ],

                    'elysion' =>[
                        'elysion_y04_13'                => [
                                '2_4_s_prestige'        => ['2.4 S Prestige'],
                            ],
                    ],

                    'elysion' =>[
                        'elysion_y04_13'                => [
                                '2_4_s_prestige'        => ['2.4 S Prestige'],
                            ],
                    ],

                    'fit' =>[
                        'fit_y08_14'                    => [
                                '1_5_rs'                => ['1.5 RS'],
                            ],
                    ],

                    'freed' =>[
                        'freed_y08_16'                  => [
                                '1_5_e'                 => ['1.5 E'],
                                '1_5_e_sport'           => ['1.5 E Sport'],
                                '1_5_el'                => ['1.5 EL'],
                                '1_5_limited'           => ['1.5 Limited'],
                                '1_5_s'                 => ['1.5 S'],
                                '1_5_se'                => ['1.5 SE'],
                            ],
                    ],

                    'hr_v' =>[
                        'hr_v_y14_18'                   => [
                                '1_8_e'                 => ['1.8 E'],
                                '1_8_e_limited'         => ['1.8 E Limited'],
                                '1_8_el'                => ['1.8 EL'],
                                '1_8_limited'           => ['1.8 Limited'],
                                '1_8_el'                => ['1.8 EL'],
                                '1_8_s'                 => ['1.8 S'],
                            ],
                    ],


                    'inspire' =>[
                        'inspire_y95_98'                => [
                                '2_0'                   => ['2.0'],
                            ],
                    ],


                    'integra' =>[
                        'dc2_y94_01'                    => [
                                '1_8_type_r'            => ['1.8 TYPE-R'],
                                '1_8_vti-lx'            => ['1.8 VTi-LX'],
                            ],
                        'dc5_y01_06'                    => [
                                '2_0_type_r'            => ['2.0 TYPE-R'],
                            ],
                    ],

                    'jazz' =>[
                        'jazz_y03_07'                   => [
                                '1_5_cool'              => ['1.5 Cool'],
                                '1_5_e'                 => ['1.5 E'],
                                '1_5_e_i-dsi'           => ['1.5 E i-DSi'],
                                '1_5_e_i_dsi_safety'    => ['1.5 E i-DSi Safety'],
                                '1_5_e_v'               => ['1.5 E-V'],
                                '1_5_e_v_vtec'          => ['1.5 E-V VTEC'],
                                '1_5_e_v_vtec_cool'     => ['1.5 E-V VTEC Cool'],
                                '1_5_e_v_vtec_x_treme'  => ['1.5 E-V VTEC X-Treme'],
                                '1_5_s'                 => ['1.5 S'],
                                '1_5_s_i_dsi'           => ['1.5 S i-DSi'],
                                '1_5_sv_vtec'           => ['1.5 SV VTEC'],
                                '1_5_v_vtec'            => ['1.5 V VTEC'],
                            ],

                        'jazz_y08_14'                   => [
                                '1_3_hybrid'            => ['1.3 Hybrid'],
                                '1_5_active_plus'       => ['1.5 Active Plus'],
                                '1_5_jp'                => ['1.5 JP'],
                                '1_5_s_i_dsi'           => ['1.5 S i-DSi'],
                                '1_5_s_i_vtec'          => ['1.5 S i-VTEC'],
                                '1_5_sv'                => ['1.5 SV'],
                                '1_5_sv_i_vtec'         => ['1.5 SV i-VTEC'],
                                '1_5_sv_vtec'           => ['1.5 SV VTEC'],
                                '1_5_v'                 => ['1.5 V'],
                                '1_5_v_active_plus'     => ['1.5 V Active Plus'],
                                '1_5_v_i_vtec'          => ['1.5 V i-VTEC'],
                                '1_5_v_i_vtec_modulo'   => ['1.5 V i-VTEC Modulo'],
                                '1_5_v_modulo'          => ['1.5 V Modulo'],
 
                            ],

                        'jazz_y14_18'                   => [
                                '1_5_rs_i_vtec'         => ['1.5 RS i-VTEC'],
                                '1_5_rs_plus_i_vtec'    => ['1.5 RS+ i-VTEC'],
                                '1_5_s_i_vtec'          => ['1.5 S i-VTEC'],
                                '1_5_sv_i_vtec'         => ['11.5 SV i-VTEC'],
                                '1_5_sv_plus_i_vtec'    => ['1.5 SV+ i-VTEC'],
                                '1_5_v_i-vtec'          => ['1.5 V i-VTEC'],
                                '1_5_v_plus_i_vtec'     => ['1.5 V+ i-VTEC'],
                               
                            ],
                    ],



                    'legend' =>[
                        'legend_y04_12'                 => [
                                '3_5_v6'                => ['3.5 V6'],
                            ],

                        'legend_y91_98'                 => [
                                '3_2_exi'               => ['3.2 EXi'],
                            ],

                        'legend_y95_04'                 => [
                                '3_5'                   => ['3.5'],        
                            ],
                    ],


                    'mobillio' =>[
                        'mobillio_y14_17'               => [
                                '1_5_rs'                => ['1.5 RS'],
                                '1_5_s'                 => ['1.5 S'],
                                '1_5_v'                 => ['1.5 V'],
                            ],
                    ],


                    'nsx' =>[
                        'nsx_y16_20'                   => [
                                '3_5'                  => ['3.5'],
                            ],
                    ],


                    'odyssey' =>[
                        'odyssey_y03_08'               => [
                                '2_4_el'               => ['2.4 EL'],
                                '2_4_elx'              => ['2.4 ELX'],
                            ],
                        'odyssey_y08_13'               => [
                                '2_4_e'                => ['2.4 E'],
                                '2_4_el'               => ['2.4 EL'],
                                '2_4_jp'               => ['2.4 JP'],
                            ],
                        'odyssey_y13_16'               => [
                                '2_4_el'               => ['2.4 EL'],
                            ],
                        'odyssey_y95_99'               => [
                                '2_2_exclusive'        => ['2.2 Exclusive'],
                                '2_2 EXi'              => ['2.2 EXi'],
                            ],
                        'odyssey_y99_03'               => [
                                '2_3_vti'              => ['2.3 VTi'],
                                '3_0_prestige'         => ['3.0 Prestige'],
                            ],
                    ],



                    'prelude' =>[
                        'prelude_y91_98'               => [
                                '2_2_exi'              => ['2.2 EXi'],
                                '2_2_lxi_cx'           => ['2.2 LXi CX'],
                                '2_2_vti_ex'           => ['2.2 VTi-EX'],
                            ],
                    ],


                    's660' =>[
                        's660_y15_18'                  => [
                                '660_jdm'              => ['660 JDM'],
                            ],
                    ],


                    'stepwgn' =>[
                        'stepwgn_y05_09'                  => [
                                '2_4_i_vtec'              => ['2.4 i-VTEC'],
                                '2_0_i_vtec'              => ['2.0 i-VTEC'],
                            ],
                    ],


                    'stepwgn_spada' =>[
                        'stepwgn_spada_y09_16'          => [
                                '2_0_e'                 => ['2.0 E'],
                                '2_0_el'                => ['2.0 EL'],
                                '2_0_jp'                => ['2.0 JP'],
                            ],
                    ],


                    'stream' =>[
                        'stream_y00_06'                 => [
                                '2_0_e'                 => ['2.0 E'],
                                '2_0_s'                 => ['2.0 S'],
                            ],
                    ],


                    'today' =>[
                        'today_y85_98'                  => [
                                '660'                   => ['660'],
                            ],
                    ],

                ],//ปิด Honda

                'toyota' => [
                    '4runner' => [
                            '4runner_y90_95'            => [
                                    '3_0'               => ['3.0']
                            ]
                    ],
                    '86gt' => [
                        '86gt_y90_95'                   => [
                                '2_0'                   => ['2.0']
                            ]
                    ],
                    'alphard' => [
                        'alphard_y02_07'            => [
                                '2_4_g'             => ['2.4 G'],
                                '2_4_hybrid'        => ['2.4 HYBRID'],
                                '2.4_hybrid_e4'     => ['2.4 Hybrid E-Four'],
                                '2_4_v'             => ['2.4 V'],
                                '3_0_g_v6'          => ['3.0 G V6'],
                                '3_0_v_v6'          => ['3.0 V V6'],
                                '3_0_v6'            => ['3.0 V6']
                        ],

                        'alphard_y08_14'            => [
                                '2_4_g'             => ['2.4 G'], 
                                '2_4_gs'            => ['2.4 GS'],
                                '2_4_hv'            => ['2.4 HV'],
                                '2_4_v'             => ['2.4 V'],
                                '2_4_welcab'        => ['2.4 Welcab'],
                                '3_5_q'             => ['3.5 Q'],
                                '3_5_v'             => ['3.5 V']
                        ],
                        'alphard_y15_18'                        => [
                                '2_4_sa_package'                => ['2.4 SA Package'],
                                '2_5_g'                         => ['2.5 G'],
                                '2_5_hv'                        => ['2.5 HV'],
                                '2_5_hybrid_e4'                 => ['2.5 HYBRID E-Four'],
                                '2_5_hybrid_g_f_package_e4'     => ['2.5 HYBRID G F-Package E-Four'],
                                '2_5_hybrid_sr_c_package_e4'    => ['2.5 HYBRID SR C Package E-Four'],
                                '2_5_hybrid_sr_e4_welcab'       => ['2.5 HYBRID SR E-Four Welcab'],
                                '2_5_s_a_package'               => ['2.5 S A-Package'],
                                '2_5_s_a_package_type_black'    => ['2.5 S A-Package Type Black'],
                                '2_5_sc'                        =>  ['2.5 SC'],
                                '2_5_welcab'                    => ['2.5 Welcab']
                        ],
                    ],
                    'aristo' => [
                        'aristo_y97_04'                         => [
                                '3_0_v300'                      => ['3.0 V300']
                        ],
                    ],
                    'avanza' => [
                        'aristo_y04_11'                         => [
                                '1_3_e'                         => ['1.3 E'],
                                '1_3_e_limited'                 => ['1.3 E Limited'],
                                '1_3_s'                         => ['1.3 S'],
                                '1_5_e'                         => ['1.5 E'],
                                '1_5_e_exclusive'               => ['1.5 E Exclusive'],
                                '1_5_e_limited'                 => ['1.5 E Limited'],
                                '1_5_j'                         => ['1.5 J'],
                                '1_5_s'                         => ['1.5 S']
                        ],
                        'aristo_y12_16'                         => [
                                '1_5_e'                         => ['1.5 E'],
                                '1_5_g'                         => ['1.5 G'],
                                '1_5_s'                         => ['1.5 S'],
                                '1_5_s_touring'                 => ['1.5 S Touring']
                        ],
                    ],
                    'avensis' => [
                        'avensis_y10_16'                        => [
                                '2_0_li'                        => ['2.0 Li']
                        ],
                    ],
                    'aygo'  => [
                        'avensis_y15_20'                        =>[
                                '1_0'                           => ['1.0']
                            ],
                    ],
                    'bb'   => [
                        'db_y06_12'                             =>[
                            '1_3_s'                             => ['1_3_S'],
                            '1_5_z'                             => ['1.5 Z']     
                        ],
                    ], 
                    'c_hr' => [
                        'c_hr_y17_21'                           =>[
                            '1.8 hybrid'                        => ['1.8 Hybrid']
                        ],
                    ],
                    'camry' => [
                        'year_2_6'                              => [
                                '2_0_e'                         => ['2.0 E'],
                                '2_0_g'                         => ['2.0 G'],
                                '2_4_g'                         => ['2.4 G'],
                                '2_4_q'                         => ['2.4 Q']
                            ],
                        'year_6_12'                             => [
                                '2_0_e'                         => ['2.0 E'],
                                '2_0_g'                         => ['2.0 G'],
                                '2_0_g_ex'                      => ['2.0 G Extremo'],
                                '2_4_e'                         => ['2.4 G'],
                                '2_4_hybrid'                    => ['2.4 Hybrid'],
                                '2_4_hybrid_ex'                 => ['2.4 Hybrid Extremo'],
                                '2_4_v'                         => ['2.4 V'],
                                '3_5_q'                         => ['3.5 Q']
                        ],
                        'year_12_16'                            => [
                                '2_0_g'                         => ['2.0 G'],
                                '2_0_g_ex'                      => ['2.0 G Extremo'],
                                '2_5_esport'                    => ['2.5 ESPORT'],
                                '2_5_g'                         => ['2.5 G'],
                                '2_5_hybrid'                    => ['2.5 Hybrid']
                        ],
                        'year_93_97'                            => [
                                '2_2_gxi'                       => ['2.2 GXi']
                        ],
                        'light_long'                            => [
                                '2_2_seg'                       => ['2.2 SEG'],
                                '3_0_glx_v6'                    => ['3.0 GLX V6']
                        ],
                        'light_long_sub'                        => [
                                '2_2_seg'                       => ['2.2 SEG']
                        ],
                    ],
                    'carina' => [
                        'carina_y96_01'                         => [
                            '1_5_ti'                            => ['1.5 Ti']
                        ],
                    ],
                    'celica' => [
                        'celica_y00_05'                         => [
                            '1_8'                               => ['1.8'] 
                        ],
                        '4eye'                                  => [
                            '2_2'                               => ['2.2']
                        ], 
                        'pop_up'                                => [
                            '2_0'                               => ['2.0'],
                            '2_2'                               => ['2.2']
                        ],  
                    ],
                    'celsior' => [
                        'celsior_y00_y06'                       => [
                            '4_0'                               => ['4.0']
                        ]
                    ],
                    'century' => [
                        'century_y97_15'                        => [
                            '5_0'                               => ['5.0']
                        ]
                    ],
                    'corolla' => [
                        'corolla_y66_70'                        => [
                            '1_2_sprinter'                      => ['1.2 Sprinter']
                        ],   
                        'hi_torque'                         => [
                            '1_6_dxis'                      => ['1.6 DXIS'],
                            '1_6_gxi'                       => ['1.6 GXi'],
                            '1_6_gxis'                      => ['1.6 GXIS'],
                            '1_6_seg'                       => ['1.6 SEG'],
                            '1_8_seg'                       => ['1.8 SEG'],
                            '1_8_seg_altis'                 => ['1.8 SEG Altis']
                        ], 
                        'ke'                                => [
                            '1_3_xx'                        => ['1.3 XX']
                        ],
                        'ass_duck'                      => [
                            '1_5_dxi'                   => ['1.5 DXi'],
                            '1_5_gxi'                   => ['1.5 GXi'],
                            '1_5_gxi_saloon'            => ['1.5 GXi Saloon'],
                            '1_6_gxi'                   => ['1.6 GXi'],
                            '1_6_gxi_saloon'            => ['1.6 GXi Saloon'],
                            '1_6_seg'                   => ['1.6 SEG']
                        ],
                        'three_circle'                  => [
                            '1_3_dxi'                   => ['1.3 DXi'],
                            '1_3_gl'                    => ['1.3 GL'],
                            '1_3_gxi'                   => ['1.3 GXi'],
                            '1_6_gli'                   => ['1.6 GLi'],
                            '1_6_gxi'                   => ['1.6 GXi']
                        ],
                        'Doremon_y88_92'                => [
                            '1_6_gli'                   => ['1.6 GLi'],
                            '1_6_sel_limited'           => ['1.6 SEL Limited']
                        ]
                    ],
                    'corolla_altis'=> [
                        'y14_18'                        => [
                            '1_6_g'                     => ['1.6 G'],
                            '1_8_e'                     => ['1.8 E'],
                            '1_8_esport'                => ['1.8 ESPORT'],
                            '1_8_v'                     => ['1.8 V']
                        ],
                        'altis_y80_13'                  => [
                            '1_6_cng'                   => ['1.6 CNG'],
                            '1_6_e'                     => ['1.6 E'],
                            '1_6_g'                     => ['1.6 G'],
                            '1_6_j'                     => ['1.6 J'],
                            '1_6_ss-i'                  => ['1.6 SS-I'],
                            '1_6_trd_sportivo'          => ['1.6 TRD Sportivo'],
                            '1_8_e'                     => ['1.8 E'],
                            '1_8_g'                     => ['1.8 G'],
                            '1_8_trd_sportivo'          => ['1.8 TRD Sportivo'],
                            '2_0_g'                     => ['2.0 G'],
                            '2_0_v'                     => ['2.0 V']
                        ],
                        'altis_y14_18'                  => [
                            '1_6_e'                     => ['1.6 E'],
                            '1_6_g_cng'                 => ['1.6 E CNG'],
                            '1_6_g'                     => ['1.6 G'],
                            '1_6_j'                     => ['1.6 J'],
                            '1_8_e'                     => ['1.8 E'],
                            '1_8_esport'                => ['1.8 ESPORT'],
                            '1_8_g'                     => ['1.8 G'],
                            '1_8_s'                     => ['1.8 S'],
                            '1_8_v'                     => ['1.8 V']
                        ],
                        'face_pig'=> [
                            '1_6_e'                     =>  ['1.6 E'],
                            '1_6_e_limited'             => ['1.6 E Limited'],
                            '1_6_g'                     => ['1.6 G'],
                            '1_6_j'                     => ['1.6 J'],
                            '1_8_e'                     => ['1.8 E'],
                            '1_8_e_limited'             => ['1.8 E Limited'],
                            '1_8_g'                     => ['1.8 G'],
                            '1_8_s'                     => ['1.8 S']
                        ]
                    ],
                    'corona'=> [
                        'exsior'                        =>[
                            '1_6_exsior_exi'            => ['1.6 Exsior GXi'],
                            '2_0_exsior_sed'            => ['2.0 Exsior SEG']
                        ],
                        'tuyen'                         => [
                            '1_6_xl'                    => ['1.6 XL']
                        ],
                        'end_separate'                  => [
                            '1_6_gli'                   => ['1.6 GLi'],
                            '2_0'                       => ['2.0'], 
                            '2_0_gli'                   => ['2.0 GLi']
                            
                        ],
                        'end_high'                      => [
                            '2_0_gli'                   => ['2.0 GLi']
                        ],
                        'facegiant_facesmile'           => [
                            '1_6_gl'                    => ['1.6 GL'],
                            '2_0_gl'                    => ['2.0 GL'],   
                            '2_0_gli'                   => ['2.0 GLi']
                        ],
                        'facesharp'                     => [
                            '1_6_gx'                    => ['1.6 GX']
                        ]
                    ],

                    'cressida'=> [
                        'cressida_y88_92'               => [
                           '2_5'                        => ['2.5']
                        ]
                    ],
                    'crown'=> [
                        'crown_y00_05'                  => [
                            '2_5_royal_saloon'          => ['2.5 Royal Saloon'],
                            '3_0_royal_saloon'          => ['3.0 Royal Saloon']
                        ],
                        'crown_y13_17'                  => [
                            '2_5_hybrid_royal_saloon'   => ['2.5 Hybrid Royal Saloon'],
                            '2_5_royal_saloon'          => ['2.5 Royal Saloon']
                        ],
                        'lower_y2000'                   => [
                            '2_8_royal_saloon'          => ['2.8 Royal Saloon'],
                            '3_0_royal_saloon'          => ['3.0 Royal Saloon']
                        ]
                    ],
                    'dyna'=> [
                        'dyna_y95_02'                   => [
                            '2_7'                       => ['2.7'] 
                        ]
                    ],
                    'esquire'=> [
                        'esquire_y14_17'                => [
                            '1_8_hybrid_Gi'             => ['1.8 HYBRID Gi'],
                            '2_0_gi'                    => ['2.0 Gi']
                        ]
                    ],
                    'estima'=> [
                        'estima_y00_05'                 => [
                            '2_4_aeras'                 => ['2.4 Aeras'],
                            '2_4_g'                     => ['2.4 G'],
                            '2_4_hybrid_E4'             => ['2.4 Hybrid E-Four'],
                            '2_4_l'                     => ['2.4 L']
                        ],
                        'estima_y06_14'                 => [
                            '2_4_aeras'                 => ['2.4 Aeras'],
                            '2_4_g'                     => ['2.4 G'],
                            '2_4_hybrid_e4'             => ['2.4 Hybrid E-Four']
                        ],
                        'estima_y16_19'                 => [
                            '2_4_aeras_premium'         => ['2.4 Aeras Premium']
                        ]
                    ],
                    'fj_cruiser'=> [
                        'fj_cruiser_y07_15'             => [
                            '4_0_4x4'                   => ['4.0 4x4']
                        ]
                    ],
                    'fortuner'=> [
                        'fortuner_y04_08' => [
                            '2_7_v'             => ['2.7 V'],
                            '3_0_exclusive_v'   => ['3.0 Exclusive V'],
                            '3_0_g'             => ['3.0 G'],
                            '3_0_smart_v'       => ['3.0 Smart V'],
                            '3_0_v'             => ['3.0 V'],
                            '3_0_v_exclusive'   => ['3.0 V Exclusive'],
                            '3_0_v_smart'       => ['3.0 V Smart']

                        ],
                        'fortuner_y08_11' => [
                            '2_5_g'                 => ['2.5 G'],
                            '2_7_v'                 => ['2.7 V'],
                            '3_0_g'                 => ['3.0 G'],
                            '3_0_trd_sportivo_I'    => ['3.0 TRD Sportivo I'],
                            '3_0_trd_sportivo_ii'   => ['3.0 TRD Sportivo II'],
                            '3_0_trd_sportivo_iii'  => ['3.0 TRD Sportivo III'],
                            '3_0_V'                 => ['3.0 V'],
                            '3_0_V_aperto'          => ['3.0 V Aperto'],
                            '3_0_V_smart'           => ['3.0 V Smart']
                        ],
                        'fortuner_y12_15'=> [
                            '2_5_g'                 => ['2.5 G'],
                            '2_5_v'                 => ['2.5 V'],
                            '2_7_v'                 => ['2.7 V'],
                            '3_0_trd_sportivo'      => ['3.0 TRD Sportivo'],
                            '3_0_v'                 => ['3.0 V']
                        ],
                        'fortuner_y15_18'=> [
                            '2_4_g'                 => ['2.4 G'],
                            '2_4_v'                 => ['2.4 V'],
                            '2_7_v'                 => ['2.7 V'],
                            '2_8_trd_sportivo'      => ['2.8 TRD Sportivo'],
                            '2_8_v'                 => ['2.8 V'],
                            '2_8_v_4wd'             => ['2.8 V 4WD']
                        ],
                        'fortuner_y1518'            => [
                            '2_8_trd_sportivo'      => ['2.8 TRD Sportivo']
                        ]
                    ],

                    'grand_hiace'=> [
                            'grand_hiace_y92_02'    => [
                                '3_4'               => ['3.4']       
                            ]
                    ],
                    'granvia'=> [
                            'granvia_y95_02'        => [
                                '2_7_g'             => ['2.7 G'],
                                '3_0'               => ['3.0'],
                                '3_4_v6'            => ['3.4 V6']
                            ]
                    ],
                    'harrier'=> [
                            'harrier_y03_13'        => [
                                '2_4_240g'          => ['2.4 240G'],
                                '3_0_300g'          => ['3.0 300G']
                            ],
                            'harrier_y14_17' => [
                                '2_0_elegance_fs'   => ['2.0 Elegance GS'],
                                '2_0_premium'       => ['2.0 PREMIUM'],
                                '2_5_hybrid'        => ['2.5 HYBRID'],
                                '2_5_hybrid_e4'     => ['2.5 Hybrid E- Four'],
                                '2_5_hybrid_premium'=> ['2.5 Hybrid PREMIUM']
                            ],
                            'harrier_y97_03'        => [
                                '2_4_240g'          => ['2.4 240G'],
                                '3_0_300g'          => ['3.0 300G']
                            ]
                    ],

                    'hiace'=> [
                            'commuter_y05_16'       => [
                                '2_5_d4d'           => ['2.5 D4D'],
                                '2_7_vvti'          => ['2.7 VVTi'],
                                '3_0_d4d'           => ['3.0 D4D ']
                            ],
                            'short_y05_16'          => [
                                '2_5_d4d'           => ['2.5 D4D'],
                                '3_0_d4d'           => ['3.0 D4D']
                            ],
                            'rocket_head'           => [
                                '2_0'               => ['2.0'],
                                '2_4'               => ['2.4'],
                                '2_4_economy'       => ['2.4 Economy'],
                                '2_4_gl'            => ['2.4 GL'],
                                '2_5_commuter'      => ['2.5 Commuter'],
                                '2_8_gl'            => ['2.8 GL'],
                                '3_0_commuter'      => ['3.0 Commuter'],
                                '3_0_economy'       => ['3.0 Economy'],
                                '3_0_gl'            => ['3.0 GL']
                            ]
                    ],
                    'hilux_hero' => [
                            'single'                => [
                                '2_5'               => ['2.5']
                            ]
                    ],
                    'hilux_mighty_x' => [
                            'double_cab'            =>  [
                                '2_4_gl'            => ['2.4 GL']
                            ],
                            'extracab'              => [
                                '2_4'               => ['2.4'],
                                '2_4_gl'            => ['2.4 GL'],
                                '2_4_sgl_luxury'    => ['2.4 SGL Luxury'],
                                '2_4_standard'      => ['2.4 Standard'],
                                '2_4_super_gl'      => ['2.4 Super GL'],
                                '2_5_gl'            => ['2.5 GL']
                            ],
                            'single'  => [
                                '2_4_standard'      => ['2.4 Standard']
                            ]
                    ],
                    
                    'hilux_revo' => [
                            'revo_double_cab'                   => [
                                '2_4_e'                         => ['2.4 E'],
                                '2_4_e_plus'                    => ['2.4 E Plus'],
                                '2_4_e_prerunner'               => ['2.4 E Prerunner'],
                                '2_4_g_prerunner'               => ['2.4 G Prerunner'],
                                '2_4_j_plus_prerunner'          => ['2.4 J Plus Prerunner'],
                                '2_4_prerunner_e'               => ['2.4 Prerunner E (30)'],
                                '2_4_prerunner_e_plus'          => ['2.4 Prerunner E Plus'],
                                '2_4_prerunner_g'               => ['2.4 Prerunner G'],
                                '2_4_prerunner_j_plus'          =>  ['2.4 Prerunner J Plus'],
                                '2_4_prerunner_trd_sportivo'    => ['2.4 Prerunner TRD Sportivo '],
                                '2_4_trd_sportivo'              => ['2.4 TRD Sportivo'],
                                '2_8_g'                         => ['2.8 G'],
                                '2_8_g_prerunner'               => ['2.8 G Prerunner'],
                                '2_8_prerunner_g'               => ['2.8 Prerunner G']
                            ],
                            'revo_single'       => [
                                '2_4_j'         => ['2.4 J'],
                                '2_4_j_plus'    => ['2.4 J Plus'],
                                '2_7_j'         => ['2.7 J'],
                                '2_8_j'         => ['2.8 J'],
                                '2_8_4x4'       => ['2.8 J 4x4'],
                                '2_8_j_plus'    => ['2.8 J Plus']
                            ],
                            'revo_smartcab'                     => [
                                '2_4_e'                         => ['2.4 E'],
                                '2_4_e_prerunner'               => ['2.4 E Prerunner'],
                                '2_4_g'                         => ['2.4 G'],
                                '2_4_g_prerunner'               => ['2.4 G Prerunner'],
                                '2_4_j'                         => ['2.4 J'],
                                '2_4_j_plus'                    => ['2.4 J Plus'],
                                '2_4_j_plus_prerunner'          => ['2.4 J Plus Prerunner'],
                                '2_4_prerunner_e_plus'          => ['2.4 Prerunner E Plus'],
                                '2_4_prerunner_g'               => ['2.4 Prerunner G'],
                                '2_4_prerunner_j_plus'          => ['2.4 Prerunner J Plus'],
                                '2_4_prerunner_trd_sportivo'    => ['2.4 Prerunner TRD Sportivo'],
                                '2_4_trd_sportivo'              => ['2.4 TRD Sportivo'],
                                '2_7__e_prerunner'              => ['2.7 E Prerunner'],
                                '2_7_prerunner_e'               => ['2.7 Prerunner E'],
                                '2_8_g'                         => ['2.8 G'],
                                '2_8_g_Prerunner'               => ['2.8 G Prerunner'],
                                '2_8_prerunner_g'               => ['2.8 Prerunner G']
                            ],
                            'revo_smartcab_smartcab'            => [
                                '2_4_prerunner_trd_sportivo'    => ['2.4 Prerunner TRD Sportivo'],
                                '2_4_trd_sportivo'              => ['2.4 TRD Sportivo']
                            ],

                    ],

                    'hilux_surf'=> [
                            'surf_y88_97'       => [
                                '2_8'           => ['2.8'],
                                '3_0_ssr'       => ['3.0 SSR']
                            ]

                    ],
                    'hilux_tiger'=> [
                            'tiger_doublecab'   => [
                                '2_4'           => ['2.4'],
                                '2_4_gl'        => ['2.4 GL'],
                                '2_4_super_gl'  => ['2.4 Super GL'],
                                '2_5_gl'        => ['2.5 GL'],
                                '3_0_gl'        => ['3.0 GL']
                            ],

                    ],
                    'hilux_vigo'=> [

                            'champ_double_cab'                  => [
                                '2_5_e'                         => ['2.5 E'],
                                '2_5_e_prerunner_vn_turbo'      => ['2.5 E Prerunner VN Turbo'],
                                '2_5_e_prerunner_vn_turbo_trd'  => ['2.5 E Prerunner VN Turbo TRD'],
                                '2_5_g'                         => ['2.5 G'],
                                '2_5_g_prerunner_vn_turbo'      => ['2.5 G Prerunner VN Turbo'],
                                '2_5_g_vn_turbo'                => ['2.5 G VN Turbo'],
                                '2_5_j'                         => ['2.5 J'],
                                '2_5_prerunner_e'               => ['2.5 Prerunner E'],
                                '2_5_prerunner_e_trd_sportivo'  => ['2.5 Prerunner E TRD Sportivo'],
                                '2_5_prerunner_g'               => ['2.5 Prerunner G'],
                                '2_7_e'                         => ['2.7 E'],
                                '3_0_g'                         => ['3.0 G'],
                                '3_0_g_4x4_vn_turbo'            => ['3.0 G 4x4 VN Turbo'],
                                '3_0_g_prerunner_vn_turbo'      => ['3.0 G Prerunner VN Turbo'],
                                '3_0_prerunner_g'               => ['3.0 Prerunner G']
                            ],
                            'champ_extracab'                    => [
                                '2_5_j'                         => ['2.5 J'],
                                '2_7_j'                         => ['2.7 J']
                            ],
                            'champ_single'                      =>[
                                '2_5_j'                         => ['2.5 J'],
                                '2_5_j_std'                     => ['2.5 J STD'],
                                '2_7_cng'                       => ['2.7 CNG'],
                                '2_7_j'                         => ['2.7 J'],
                                '3_0_j'                         => ['3.0 J']
                            ],
                            'champ_smartcab'                    => [
                                '2_5_e'                         => ['2.5 E'],
                                '2_5_e_prerunner'               => ['2.5 E Prerunner'],
                                '2_5_e_prerunner_vn_turbo'      => ['2.5 E Prerunner VN Turbo'],
                                '2_5_e_prerunner_vn_turbo_trd'  => ['2.5 E Prerunner VN Turbo TRD'],
                                '2_5_e_trd'                     => ['2.5 E TRD'],
                                '2_5_e_trd_sportivo'            => ['2.5 E TRD Sportivo'],
                                '2_5_e_vn_turbo_trd'            => ['2.5 E VN Turbo TRD'],
                                '2_5_g'                         => ['2.5 G'],
                                '2_5_g_prerunner_vn_turbo'      => ['2.5 G Prerunner VN Turbo'],
                                '2_5_j'                         => ['2.5 J'],
                                '2_5_j_std'                     => ['2.5 J STD'],
                                '2_5_prerunner_e'               => ['2.5 Prerunner E'],
                                '2_5_prerunner_e_trd_sportivo'  => ['2.5 Prerunner E TRD Sportivo'],
                                '2_5_prerunner_g'               => ['2.5 Prerunner G'],
                                '2_7_cng'                       => ['2.7 CNG'],
                                '2_7_j'                         => ['2.7 J'],
                                '2_7_j_cng'                     => ['2.7 J CNG'],
                                '2_7_j_std'                     => ['2.7 J STD'],
                                '3_0_g'                         => ['3.0 G'],
                                '3_0_g_prerunner_vn_turbo'      => ['3.0 G Prerunner VN Turbo'],
                                '3_0_prerunner_g'               => ['3.0 Prerunner G']
                           ],
                           'vigo_bdcab_y05_08'                  => [
                                '2_5_e'                         => ['2.5 E'],
                                '2_5_g'                         => ['2.5 G'],
                                '2_5_j'                         => ['2.5 J'],
                                '2_7_g'                         => ['2.7 G'],
                                '3_0_e'                         => ['3.0 E'],
                                '3_0_e_prerunner'               => ['3.0 E Prerunner'],
                                '3_0_g'                         => ['3.0 G'],
                                '3_0_prerunner_e'               => ['3.0 Prerunner E']
                           ],
                           'vigo_bdcab_y08_11'                  => [
                                '2_5_e'                         => ['2.5 E'],
                                '2_5_e_prerunner_vn_turbo'      => ['2.5 E Prerunner VN Turbo'],
                                '2_5_g'                         => ['2.5 G'],
                                '2_5_j'                         => ['2.5 J'],
                                '2_5_prerunner_e'               => ['2.5 Prerunner E'],
                                '2_7_e'                         => ['2_7_E'],
                                '3_0_e'                         => ['3.0 E'],
                                '3_0_e_prerunner'               => ['3.0 E Prerunner'],
                                '3_0_e_prerunner_vn_turbo'      => ['3.0 E Prerunner VN Turbo'],
                                '3_0_g'                         => ['3.0 G'],
                                '3_0_g_4x4'                     => ['3.0 G 4x4'],
                                '3_0_g_prerunner'               => ['3.0 G Prerunner'],
                                '3_0_g_prerunner_vn_turbo'      => ['3.0 G Prerunner VN Turbo'],
                                '3_0_g_vn_turbo'                => ['3.0 G VN Turbo'],
                                '3_0_prerunner_e'               => ['3.0 Prerunner E'],
                                '3_0_prerunner_g'               => ['3.0 Prerunner G']
                           ],
                           'vigo_extracab_y04_08'               => [
                                '2_5_e'                         => ['2.5 E'],
                                '2_5_g'                         => ['2.5 G'],
                                '2_5_j'                         => ['2.5 J'],
                                '2_5_prerunner'                 => ['2.5 Prerunner'],
                                '2_7_g'                         => ['2.7 G'],
                                '3_0_e'                         => ['3.0 E'],
                                '3_0_e_prerunner'               => ['3.0 E Prerunner'],
                                '3_0_g'                         => ['3.0 G'],
                                '3_0_j'                         => ['3.0 J'],
                                '3.0 Prerunner'                 => ['3.0 Prerunner']
                           ],
                           'vigo_extracab_y08_11'               => [
                                '2_5_e'                         => ['2.5 E'],
                                '2_5_j'                         => ['2.5 J'],
                                '3_0_e Prerunner'               => ['3.0 E Prerunner']
                           ],
                           'vigo_single_y04_08'                 => [
                                '2_5_j'                         => ['2.5 J'],
                                '3_0_j'                         => ['3.0 J']
                           ],
                           'vigo_single_y08_11'                 => [
                                '2_5_j'                         => ['2.5 J'],
                                '2_7_j'                         => ['2.7 J'],
                                '3_0_j'                         => ['3.0 J']
                           ],
                           'vigo_smartcab_y08_11'           => [
                                '2_5_e'                     => ['2.5 E'],
                                '2_5_e_limited'             => ['2.5 E Limited'],
                                '2_5_e_prerunner'           => ['2.5 E Prerunner'],
                                '2_5_e_prerunner_vn_turbo'  => ['2.5 E Prerunner VN Turbo'],
                                '2_5_g'                     => ['2.5 G'],
                                '2_5_g_prerunner'           => ['2.5 G Prerunner'],
                                '2_5_j'                     => ['2.5 J'],
                                '2_5_prerunner_e'           => ['2.5 Prerunner E'],
                                '2_7_g'                     => ['2.7 G'],
                                '2_7_j'                     => ['2.7 J'],
                                '3_0_e'                     => ['3.0 E'],
                                '3_0_e_prerunner'           => ['3.0 E Prerunner'],
                                '3_0_g'                     => ['3.0 G'],
                                '3_0_g_prerunner_vn_turbo'  => ['3.0 G Prerunner VN Turbo'],
                                '3_0_g_vn_turbo'            => ['3.0 G VN Turbo'],
                                '3_0_prerunner_e'           => ['3.0 Prerunner E'],
                                '3_0_prerunner_e'           => ['3.0 Prerunner G']
                           ],
                           'vigo_sportvan_y08_11'           => [
                                '3_0'                       => ['3.0']
                           ],
                    ],

                    'innova'=> [
                        'innova_y04_11'                     => [
                                '2_0_e'                     => ['2.0 E'],
                                '2_0_g'                     => ['2.0 G'],
                                '2_0_g_exclusive'           => ['2_0_g_exclusive'],
                                '2_0_v'                     => ['2.0 V'],
                                '2_5_v'                     => ['2.5 V']
                        ],
                        'innova_y11_15'                     => [
                                '2_0_e'                     => ['2.0 E'],
                                '2_0_g'                     => ['2.0 G'],
                                '2_0_g_option'              => ['2.0 G Option'],
                                '2_0_v'                     => ['2.0 V']
                        ],
                        'innova_y16_20'                     => [
                                '2_8_crysta_v'              => ['2.8 Crysta V']
                        ],
                    ],

                    'iq'=> [
                        'iq_y08_15'                     => [
                                '1.0'                   => ['1.0']
                        ],
                    ],
                    'kluger'=> [
                        'kluger_y01_07'                 =>[
                                '3_0_v6'                => ['3.0 V6']
                        ],
                    ],

                    'land_cruiser'=> [
                        'land_cruiser_43'               => [
                                '3_4'                   => ['3.4']
                        ],
                        'land_cruiser_45'               => [
                                '4_2'                   => ['4.2']
                        ],
                        'land_cruiser_70'               => [
                                '3_4'                   => ['3.4'],
                                '4_0_30th_anniversary'  => ['4.0 30TH ANNIVERSARY']
                        ],
                        'land_cruiser_80'               => [
                                '4_5_vx_limited'        => ['4.5 VX Limited']
                        ],
                        'land_cruiser_100'              => [
                                '4_2_vx'                => ['4.2 VX'],
                                '4_7_cygnus'            => ['4.7 Cygnus'],
                                '4_7_vx_limited'        => ['4.7 VX Limited']
                        ],
                        'land_cruiser_200'              => [
                                '4_5_vx'                => ['4.5 VX'],
                                '4_6_zx'                => ['4.6 ZX'],
                                '4_7_vx'                => ['4.7 VX']
                        ]

                    ],
                    'landcruiser_prado'=> [
                        'land_cruiser_prado_90'         => [
                            '2_7_rx'                    => ['2.7 RX'],
                            '3_0'                       => ['3.0'],
                            '3_4'                       => ['3.4']
                        ],
                        'land_cruiser_prado_120'        => [
                            '2_7_tx'                    => ['2.7 TX'],
                            '3_4_rz'                    => ['3.4 RZ'],
                            '3_4_tx'                    => ['3.4 TX'],
                            '3_4_tz'                    => ['3.4 TZ']
                        ],
                        'land_prado_90'                 => [
                            '2_7_rx'                    => ['2.7 RX'],
                            '2_7_tx'                    => ['2.7 TX'],
                            '2_8_tx'                    => ['2.8 TX'],
                            '2_8_tz-g'                  => ['2.8 TZ-G'],
                            '3_0 d4d'                   => ['3.0 D4D']
                        ],
                    ],
                    'ln'=> [
                        'all_nickname'                  => [
                            '2_8_single'                => ['2.8 Single']
                        ],    
                    ],
                    'lucida'=> [
                        'lucida_y90_99'                 => [
                            '2_2'                       => ['2.2']
                        ],  
                    ],
                    'mark_2'=> [
                        'mark_2_y92-96'                 => [
                            '2_0'                       => ['2.0']
                        ],
                    ],
                    'mark_x'=> [
                        'mark_x_y04_10'                 => [
                            '2_5_250g'                  => ['2.5 250G']
                        ],
                    ],
                    'mega_cruiser'=> [
                        'mega_cruiser_y95_02'           => [
                            '4_1'                       => ['4.1']
                        ],
                    ],
                    'mega_cruiser'=> [
                        'mega_cruiser_y95_02'           => [
                            '4_1'                       => ['4.1']
                        ],
                    ],
                    'mr_s'=> [
                        'mr_s_y00_07'                   => [
                            '1_8'                       => ['1.8'],
                            '1_8_s'                     => ['1.8 S']
                        ],
                    ],
                    'mr_2'=> [
                        'mr_2_y89_99'                   => [
                            '2_0'                       => ['2.0'],
                            '2_0_g'                     => ['2.0 G']
                        ],
                    ],
                    'noah'=> [
                        'noah_y01_13'                   => [
                            '2_0_super_extra_limo'      => ['2.0 Super Extra Limo']
                        ],
                        'noah_y14_17'                   => [
                            '2_0_si'                    => ['2.0 Si']
                        ],
                    ],
                    'opa'=> [
                        'noah_y00_05'                   => [
                            '2_2017'                    => ['2.0']
                        ],
                    ],
                    'porte'=> [
                        'porte_y05_15'                  => [
                            '1_5_vvt_i'                 => ['1.5 VVT-i']
                        ],
                    ],
                    'prius'=> [
                        'porte_y05_15'                          => [
                            '1_8_hybrid'                        => ['1.8 Hybrid'],
                            '1_8_hybrid_e_trd_sportivo'         => ['1.8 Hybrid E TRD Sportivo'],
                            '1_8_hybrid_standard_grade'         => ['1.8 Hybrid Standard grade'],
                            '1_8_hybrid_top_grade'              => ['1.8 Hybrid Top grade'],
                            '1_8_hybrid_top_option_grade'       => ['1.8 Hybrid Top option grade'],
                            '1_8_hybrid_trd_sportivo_ii'        => ['1.8 Hybrid TRD Sportivo II '],
                            '1_8_hybrid_trd_sportivo_top_option' => ['1.8 Hybrid TRD Sportivo Top option'],
                            '1_8_trd_sportivo'                  => ['1.8 TRD Sportivo']
                        ],
                    ],
                    'rav4'=> [
                        'av4_y02_12'                    => [
                            '2_0_sporty'                => ['2.0 Sporty'],
                            '2_4_g'                     => ['2.4 G']
                        ],
                        'rav4_y95_02'                   => [
                            '2_0'                       => ['2.0']
                        ],
                    ],
                    'sienta'=> [
                        'ienta_y16_20'                  => [
                            '1_5_g'                     => ['1.5 G'],
                            '1_5_v'                     => ['1.5 V']
                        ],

                    ],
                    'soarer'=> [
                        'soarer_y01_05'                 => [
                            '4_3'                       => ['4.3']
                        ],
                    ],
                    'soluna'=> [
                        'al50_first_nickname'           => [
                            '1_5_gli'                   => ['1.5 GLi'],
                            '1_5_gli_v_version'         => ['1.5 GLi V-Version'],
                            '1_5_xli'                   => ['1.5 XLi']
                        ],
                        'al50_droplet_y00_03'           => [
                            '1_5_e'                     => ['1.5 E'],
                            '1_5_gli'                   => ['1.5 GLi'],
                            '1_5_sli'                   => ['1.5 SLi'],
                            '1_5_xli'                   => ['1.5 XLi']
                        ],
                        'droplet_al50_y00_03'           => [
                            '1_5_e'                     => ['1.5 E']
                        ],
                    ],
                    'soluna'=> [
                        'al50_first_nickname'           => [
                            '1_5_gli'                   => ['1.5 GLi'],
                            '1_5_gli_v_version'         => ['1.5 GLi V-Version'],
                            '1_5_xli'                   => ['1.5 XLi']
                        ],
                        'al50_droplet_y00_03'           => [
                            '1_5_e'                     => ['1.5 E'],
                            '1_5_gli'                   => ['1.5 GLi'],
                            '1_5_sli'                   => ['1.5 SLi'],
                            '1_5_xli'                   => ['1.5 XLi']
                        ],
                        'droplet_al50_y00_03'           => [
                            '1_5_e'                     => ['1.5 E']
                        ],
                    ],
                    'sport_cruiser'=> [
                        'sport_cruiser_y00_04'          => [
                            '2_5_e'                     => ['2.5 E'],
                            '2_5_prerunner_e'           => ['2.5 Prerunner E'],
                            '2_5_s'                     => ['2.5 S'],
                            '3_0_g'                     => ['3.0 G'],
                            '3_0_g_limited'             => ['3.0 G Limited']
                        ],
                    ],
                    'sport_rider'=> [
                        'sport_rider_y98_02'            => [
                            '3_0_prerunner'             => ['3.0 Prerunner'],
                            '3_0_prerunner_sgl_limited' => ['3.0 Prerunner SGL Limited'],
                            '3_0_sr5'                   => ['3.0 SR5'],
                            '3_0_sr5_limited'           => ['3.0 SR5 Limited'],
                            '3_0_sr5_limited_g_series'  => ['3.0 SR5 Limited G-Series']
                        ],
                        'd4d_y02_04'                    => [
                            '3_0_prerunner'             => ['2.5 Prerunner E'],
                            '2_5_s'                     => ['2.5 S'],
                            '3_0_g'                     => ['3.0 G'],
                            '3_0_sr5_limited'           => ['3.0 SR5 Limited'],
                            '3_0_limited'               => ['3.0 Limited']
                        ],
                    ],
                    'starlet'=> [
                        'starlet_ep71'                  => [
                            '1_3_xl'                    => ['1.3 XL']
                        ],
                        'starlet_ep82'                  => [
                            '1_3_xli'                   => ['1.3 XLi']
                        ],
                    ],
                    'super_custom'=> [
                        'super_custom_y96_04'           => [
                            '2_4'                       => ['2.4']
                        ],
                    ],
                    'super_wagon'=> [
                        'super_wagon_y96_04'            => [
                            '2_4'                       => ['2.4']
                        ],
                    ],
                    'supra'=> [
                        'supra_y02_02'                  => [
                            '3_0'                       => ['3.0'],
                            '3_0_a80'                   => ['3.0 A80']
                        ],
                    ],
                    'vellfire'=> [
                        'vellfire_y08_14'               => [
                            '2_4_gs'                    => ['2.4 GS'],
                            '2_4_hybrid_e_four'         => ['2.4 Hybrid E-Four'],
                            '2_4_v'                     => ['2.4 V'],
                            '2_4_z'                     => ['2.4 Z']
                        ],
                        'vellfire_y15_18'                           => [
                            '2_5'                                   => ['2.5'],
                            '2_5_e_four_hybrid'                     => ['2.5 E-Four Hybrid'],
                            '2_5_hybrid_e_four'                     => ['2.5 Hybrid E-Four'],
                            '2_5_hybrid_executive_lounge_e_four'    => ['2.5 HYBRID Executive Lounge E-Four'],
                            '2_5_hybrid_zr_g_edition_e_four'        => ['2.5 Hybrid ZR G Edition E-Four'],
                            '2_5_v'                                 => ['2.5 V'],
                            '2_5_welcab'                            => ['2.5 Welcab'],
                            '2_5_z'                                 => ['2.5 Z'],
                            '2_5_z_g_edition'                       => ['2.5 Z G EDITION'],
                            '2_5_za_edition'                        => ['2.5 ZA EDITION'],
                            '2_5_za_edition_golden_eyes'            => ['2.5 ZA EDITION GOLDEN EYES']
                        ],
                    ],
                    'ventury'=> [
                        'vellfire_y08_14'       => [
                            '2_7_g'             => ['2.7 G'],
                            '2_7_majesty'       => ['2.7 Majesty'],
                            '2_7_v'             => ['2.7 V'],
                            '2_7_v_majesty'     => ['2.7 V Majesty'],
                            '3_0_g'             => ['3.0 G'],
                            '3_0_v'             => ['3.0 V']
                        ],
                    ],
                    'vios'=> [
                        'vios_y02_07'           => [
                            '1_5_e'             => ['1.5 E'],
                            '1_5_e_ivory'       => ['1.5 E IVORY'],
                            '1_5_j'             => ['1.5 J'],
                            '2_7_v_majesty'     => ['1.5 S'],
                            '1_5_s'             => ['3.0 G'],
                            '1_5_s_sporty'      => ['1.5 S Sporty']
                        ],
                        'vios_y07_13'           => [
                            '1_5_e'             => ['1.5 E'],
                            '1_5_es'            => ['1.5 ES'],
                            '1_5_g'             => ['1.5 G'],
                            '1_5_g_limited'     => ['1.5 G Limited'],
                            '1_5_gt_street'     => ['1.5 GT Street'],
                            '1_5_j'             => ['1.5 J'],
                            '1_5_j_sportivo'    => ['1.5 J Sportivo'],
                            '1_5_s_limited'     => ['1.5 S Limited'],
                            '1_5_trd'           => ['1.5 TRD'],
                            '1_5_trd_sportivo'  => ['1.5 TRD Sportivo']
                        ],
                        'vios_y13_17'           => [
                            '1_5_e'             => ['1.5 E'],
                            '1_5_g'             => ['1.5 G'],
                            '1_5_j'             => ['1.5 J'],
                            '1_5_s'             => ['1.5 S'],
                            '1_5_trd'           => ['1.5 TRD'],
                            '1_5_trd_sportivo'  => ['1.5 TRD Sportivo']
                        ],
                        'vios_y1317'            => [
                            '1_5_e'             => ['1.5 E'],
                            '1_5_g'             => ['1.5 G'],
                            '1_5_j'             => ['1.5 J'],
                            '1_5_s'             => ['1.5 S']
                        ],
                    ],
                    'vista'=> [
                        'vista_y90_91'          => [
                            '2.0 vl'            => ['2.0 VL']
                        ],
                    ],
                    'vitz'=> [
                        'vitz_y00_05'           => [
                            '1.0'               => ['1.0']
                        ],
                    ],
                    'voxy'=> [
                        'voxy_y14_17'           => [
                            '2_0_azr60g'        => ['2.0 AZR60G']
                        ],
                    ],
                    'wish'=> [
                        'wish_y03_10'                   => [
                            '2_0_q'                     => ['2.0 Q'],
                            '2_0_q_limited'             => ['2.0 Q Limited'],
                            '2_0_q_limited_option'      => ['2.0 Q Limited Option'],
                            '2_0_q_sport_touring_ii'    => ['2.0 Q Sport Touring II'],
                            '2_0_q_sport_touring_iii'   => ['2.0 Q Sport Touring III'],
                            '2_0_s'                     => ['2.0 S'],
                            '2_0_st2'                   => ['2.0 ST2'],
                            '2_0_st3'                   => ['2.0 ST3']
                        ],
                    ],
                    'yaris'=> [

                        'yaris_y06_13'                  => [
                            '1_5_ace'                   => ['1.5 ACE'],
                            '1_5_e'                     => ['1.5 E'],
                            '1_5_e_limited'             => ['1.5 E Limited'],
                            '1_5_g'                     => ['1.5 G'],
                            '1_5_g_limited'             => ['1.5 G Limited'],
                            '1_5_j'                     => ['1.5 J'],
                            '1.5 rs'                    => ['1.5 RS'],
                            '1_5_s'                     => ['1.5 S'],
                            '1_5_s_limited'             => ['1.5 S Limited'],
                            '1_5_trd_sportivo'          => ['1.5 TRD Sportivo']
                        ],
                        'yaris_y13_17'                  => [
                            '1_2_e'                     => ['1.2 E'],
                            '1_2_g'                     => ['1.2 G'],
                            '1_2_j'                     => ['1.2 J'],
                            '1_2_j_eco'                 => ['1.2 J ECO'],
                            '1_2_trd_sportivo'          => ['1.2 TRD Sportivo']
                        ],
                    ],

                    'yaris_ativ'=> [
                            'yaris_ativ_y17_21'             => [
                                '1_5_e'                     => ['1.5 E'],
                                '1_5_j'                     => ['1.5 J'],
                                '1_2_s'                     => ['1.2 S']
                            ],
                    ],

                ], //ปิด Toyota

                'isuzu' =>[
                    'adventure' =>[
                                'adventure_y96_00'          => [
                                    '2_5_4x4'               => ['2.5 4x4'],
                                    '2_8_4x2'               => ['2.8 4x2'],
                                    '2_8_4x4'               => ['2.8 4x4']
                            ],

                    ], 

                    'adventure_master' =>[
                                'amaster_y02_06'            => [
                                    '3_0_4x2'               => ['3.0 4x2'],
                                    '3_0_4x4'               => ['3.0 4x4']
                            ],
                    ],           

                    'cameo' =>[
                                'cameo_y91_97'              => [
                                    '2_5'                   => ['2.5'],
                            ],
                    ],

                    'd_max' =>[
                                'cab_4_y02_06'                  => [
                                    '2_5_hi_lander'             => ['2.5 Hi-Lander'],
                                    '2_5_sl'                    => ['2.5 SL'],
                                    '2_5_sl_ddi_i_teq'          => ['2.5 SL Ddi i-TEQ'],
                                    '2_5_slx_ddi_i_teq'         => ['2.5 SLX Ddi i-TEQ'],
                                    '2_5_sx'                    => ['2.5 SX'],
                                    '2_5_sx_ddi_i_teq'          => ['2.5 SX Ddi i-TEQ'],
                                    '3_0_hi_lander'             => ['3.0 Hi-Lander'],
                                    '3_0_hi_lander_ddi_i_teq'   => ['3.0 Hi-Lander Ddi i-TEQ'],
                                    '3_0_ls'                    => ['3.0 LS'],
                                    '3_0_ls_ddi_i_teq'          => ['3.0 LS Ddi i-TEQ'],
                                    '3_0_slx'                   => ['3.0 SLX'],
                                    '3_0_slx_ddi_i_teq'         => ['3.0 SLX Ddi i-TEQ']
                                ],

                                'cab_4_y07_11'                              => [
                                    '2_5_hi_lander'                         => ['2.5 Hi-Lander'],
                                    '2_5_hi_lander_ddi_i_teq'               => ['2.5 Hi-Lander Ddi i-TEQ'],
                                    '2_5_hi_lander_l'                       => ['2.5 Hi-Lander L'],
                                    '2_5_hi_lander_platinuml'               => ['2.5 Hi-Lander PlatinumL'],
                                    '2_5_hi_lander_super_platinuml'         => ['2.5 Hi-Lander Super PlatinumL'],
                                    '2_5_hi_lander_super_titanium'          => ['2.5 Hi-Lander Super Titanium'],
                                    '2_5_hi_lander_x_series'                => ['2.5 Hi-Lander X-Series'],
                                    '2_5_hi_lander_x_series_super_titanium' => ['2.5 Hi-Lander X-Series Super Titanium'],
                                    '2_5_sl_ddi_i_teq'                      => ['2.5 SL Ddi i-TEQ'],
                                    '2_5_slx'                               => ['2.5 SLX'],
                                    '2_5_slx_ddi_i_teq'                     => ['2.5 SLX Ddi i-TEQ'],
                                    '2_5_slx_super_platinum'                => ['2.5 SLX Super Platinum'],
                                    '2_5_slx_super_titanium'                => ['2.5 SLX Super Titanium'],
                                    '2_5_sx_ddi_i_teq'                      => ['2.5 SX Ddi i-TEQ'],
                                    '2_5_sx_platinum'                       => ['2.5 SX Platinum'],
                                    '2_5_sx_super_titanium'                 => ['2.5 SX Super Titanium'],
                                    '3_0_hi_lander'                         => ['3.0 Hi-Lander'],
                                    '3_0_hi_lander_ddi_i_teq'               => ['3.0 Hi-Lander Ddi i-TEQ'],
                                    '3_0_hi_lander_l'                       => ['3.0 Hi-Lander L'],
                                    '3_0_hi_lander_platinuml'               => ['3.0 Hi-Lander Platinum'],
                                    '3_0_hi_lander_super_platinuml'         => ['3.0 Hi-Lander Super Platinum'],
                                    '3_0_hi_lander_super_titanium'          => ['3.0 Hi-Lander Super Titanium'],
                                    '3_0_hi_lander_x_series'                => ['3.0 Hi-Lander X-Series'],
                                    '3_0_hi_lander_x_series_super_titanium' => ['3.0 Hi-Lander X-Series Super Titanium'],
                                    '3_0_ls_ddi_i-teq'                      => ['3.0 LS Ddi i-TEQ'],
                                    '3_0_ls_gt_ddi_i_teq'                   => ['3.0 LS GT Ddi i-TEQ'],
                                    '3_0_ls_super_platinum'                 => ['3.0 LS Super Platinum'],
                                    '3_0_ls_super_titanium'                 => ['3.0 LS Super Titanium'],
                                    '3_0_slx'                               => ['3.0 SLX'],
                                    '3_0_slx_ddi_i_teq'                     => ['3.0 SLX Ddi i-TEQ'],
                                ],


                                'cab_4_y11_17'                              => [
                                    '1_9_hi_lander_l'                       => ['1.9 Hi-Lander L'],
                                    '1_9_hi_lander_z'                       => ['1.9 Hi-Lander Z'],
                                    '1_9_hi_lander_z_prestige'              => ['1.9 Hi-Lander Z-Prestige'],
                                    '1_9_s'                                 => ['1.9 S'],
                                    '1_9_z'                                 => ['1.9 Z'],
                                    '2_5_hi_lander_l'                       => ['2.5 Hi-Lander L'],
                                    '2_5_hi_lander_x_series'                => ['2.5 Hi-Lander X-Series'],
                                    '2_5_hi_lander_x_series_z'              => ['2.5 Hi-Lander X-Series Z'],
                                    '2_5_hi_lander_x_series_z_prestige'     => ['2.5 Hi-Lander X-Series Z Prestige'],
                                    '2_5_hi_lander_z'                       => ['2.5 Hi-Lander Z'],
                                    '2_5_hi_lander_z_prestige_ddi_vgs_turbo'=> ['2.5 Hi-Lander Z Prestige Ddi VGS Turbo'],
                                    '2_5_hi_lander_z_prestige'              => ['2.5 Hi-Lander Z-Prestige'],
                                    '2_5_l'                                 => ['2.5 L'],
                                    '2_5_s'                                 => ['2.5 S'],
                                    '2_5_vcross_l'                          => ['2.5 Vcross L'],
                                    '2_5_vcross_z'                          => ['2.5 Vcross Z'],
                                    '2_5_z'                                 => ['2.5 Z'],
                                    '3_0_hi_lander_z_prestige_ddi_vgs_turbo'=> ['3.0 Hi-Lander Z Prestige Ddi VGS Turbo'],
                                    '3_0_hi_lander_z_prestige'              => ['3.0 Hi-Lander Z-Prestige'],
                                    '3_0_vcross_max_z_prestige'             => ['3.0 Vcross MAX Z-Prestige'],
                                    '3_0_vcross_z_prestige'                 => ['3.0 Vcross Z-Prestige']
                                ],


                                'spacecab_y02_06'                           => [
                                    '2_5_hi_lander_ddi_i_teq'               => ['2.5 Hi-Lander Ddi i-TEQ'],
                                    '2_5_sl'                                => ['2.5 SL'],
                                    '2_5_slx'                               => ['2.5 SLX'],
                                    '2_5_slx_ddi_i_teq'                     => ['2.5 SLX Ddi i-TEQ'],
                                    '2_5_sx'                                => ['2.5 SX'],
                                    '2_5_sx_ddi_i_teq'                      => ['2.5 SX Ddi i-TEQ'],
                                    '3_0_hi_lander'                         => ['3.0 Hi-Lander'],
                                    '3_0_hi_lander_ddi_i_teq'               => ['3.0 Hi-Lander Ddi i-TEQ'],
                                    '3_0_rodeo_ls'                          => ['3.0 Rodeo LS'],
                                    '3_0_rodeo_ls_ddi_i-teq'                => ['3.0 Rodeo LS Ddi i-TEQ'],
                                    '3_0_rodeo_s'                           => ['3.0 Rodeo S'],
                                    '3_0_rodeo_s_ddi_i-teq'                 => ['3.0 Rodeo S Ddi i-TEQ'],
                                    '3_0_slx'                               => ['3.0 SLX'],
                                    '3_0_slx_ddi_i-teq'                     => ['3.0 SLX Ddi i-TEQ'],
                                ],


                                'spacecab_y07_11'                           => [
                                    '2_5_hi_lander'                         => ['2.5 Hi-Lander'],
                                    '2_5_hi_lander_ddi_i_teq'               => ['2.5 Hi-Lander Ddi i-TEQ'],
                                    '2_5_hi_lander_l'                       => ['2.5 Hi-Lander L'],
                                    '2_5_hi_lander_platinum'                => ['2.5 Hi-Lander Platinum'],
                                    '2_5_hi_lander_super_platinum_smart'    => ['2.5 Hi-Lander Platinum Smart'],
                                    '2_5_hi_lander_super_platinum'          => ['2.5 Hi-Lander Super Platinum'],
                                    '2_5_hi_lander_super_titanium'          => ['2.5 Hi-Lander Super Titanium'],
                                    '2_5_hi_lander_super_titanium_smart'    => ['2.5 Hi-Lander Super Titanium Smart'],
                                    '2_5_hi_lander_x_series'                => ['2.5 Hi-Lander X-Series'],
                                    '2_5_hi_lander_x_series_super_titanium' => ['2.5 Hi-Lander X-Series Super Titanium'],
                                    '2_5_rodeo_ls_ddi_i_teq'                => ['2.5 Rodeo LS Ddi i-TEQ'],
                                    '2_5_rodeo_ls_super_titanium'           => ['2.5 Rodeo LS Super Titanium'],
                                    '2_5_sl_ddi_i_teq'                      => ['2.5 SL Ddi i-TEQ'],
                                    '2_5_slx'                               => ['2.5 SLX'],
                                    '2_5_slx_ddi_i_teq'                     => ['2.5 SLX Ddi i-TEQ'],
                                    '2_5_slx_platinum'                      => ['2.5 SLX Platinum'],
                                    '2_5_slx_platinum_smart'                => ['2.5 SLX Platinum Smart'],
                                    '2_5_slx_speed_ddi_i_teq'               => ['2.5 SLX Speed Ddi i-TEQ'],
                                    '2_5_slx_super_platinum'                => ['2.5 SLX Super Platinum'],
                                    '2_5_slx_super_titanium'                => ['2.5 SLX Super Platinum Smart'],
                                    '2_5_slx_super_titanium_smart'          => ['2.5 SLX Super Titanium Smart'],
                                    '2_5_slx_x_series'                      => ['2.5 SLX X-Series'],
                                    '2_5_slx_x_series_super_titanium'       => ['2.5 SLX X-Series Super Titanium'],
                                    '2_5_sx'                                => ['2.5 SX'],
                                    '2_5_sx_ddi_i_teq'                      => ['2.5 SX Ddi i-TEQ'],
                                    '2_5_sx_platinum'                       => ['2.5 SX Platinum'],
                                    '2_5_slx_super_platinum'                => ['2.5 SLX Super Platinum'],
                                    '2_5_sx_super_titanium'                 => ['2.5 SX Super Titanium'],
                                    '2_5_x_series'                          => ['2.5 X-Series'],
                                    '3_0_hi_lander'                         => ['3.0 Hi-Lander'],
                                    '3_0_hi_lander_ddi_i_teq'               => ['3.0 Hi-Lander Ddi i-TEQ'],
                                    '3_0_hi_lander_platinuml'               => ['3.0 Hi-Lander Platinum'],
                                    '3_0_hi_lander_super_platinuml'         => ['3.0 Hi-Lander Super Platinum'],
                                    '3_0_hi_lander_super_titanium'          => ['3.0 Hi-Lander Super Titanium'],
                                    '3_0_hi_lander_x_series'                => ['3.0 Hi-Lander X-Series'],
                                    '3_0_rodeo_ls'                          => ['3.0 Rodeo LS'],
                                    '3_0_rodeo_ls_ddi_i-teq'                => ['3.0 Rodeo LS Ddi i-TEQ'],
                                    '3_0_rodeo_ls_super_platinum'           => ['3.0 Rodeo LS Super Platinum'],
                                    '3_0_rodeo_ls_super_titanium'           => ['3.0 Rodeo LS Super Titanium'],
                                    '3_0_rodeo_ls_x-series_super_titanium'  => ['3.0 Rodeo LS X-Series Super Titanium'],
                                    '3_0_slx'                               => ['3.0 SLX'],
                                    '3_0_slx_ddi_i_teq'                     => ['3.0 SLX Ddi i-TEQ'],
                                    '3_0_slx_platinum'                      => ['3.0 SLX Platinum'],
                                    '3_0_slx_super_platinum'                => ['3.0 SLX Super Platinum'],
                                    '3_0_slx_super_titanium'                => ['3.0 SLX Super Titanium'],
                                ],


                                'spacecab_y11_17'                               => [
                                    '1_9_hi_lander_l'                           => ['1.9 Hi-Lander L'],
                                    '1_9_hi_lander_x_series_z'                  => ['1.9 Hi-Lander X-Series Z'],
                                    '1_9_hi_lander_z'                           => ['1.9 Hi-Lander Z'],
                                    '1_9_l'                                     => ['1.9 L'],
                                    '1_9_s'                                     => ['1.9 S'],
                                    '1_9_x_series_z'                            => ['1.9 X-Series Z'],
                                    '1_9_z'                                     => ['1.9 Z'],
                                    '2_5_hi_lander_99_th_anniversary_edition'   => ['2.5 Hi-Lander 99 th anniversary Edition'],
                                    '2_5_hi_lander l'                           => ['2.5 Hi-Lander L'],
                                    '2_5_hi_lander_x_series'                    => ['2.5 Hi-Lander X-Series'],
                                    '2_5_hi_lander_x_series_z'                  => ['2.5 Hi-Lander X-Series Z'],
                                    '2_5_hi_lander_z'                           => ['2.5 Hi-Lander Z'],
                                    '2_5_hi_lander_z_prestige_ddi_vgs_turbo'    => ['2.5 Hi-Lander Z Prestige Ddi VGS Turbo'],
                                    '2_5_hi_lander_z_prestige'                  => ['2.5 Hi-Lander Z-Prestige'],
                                    '2_5_l'                                     => ['2.5 L'],
                                    '2_5_s'                                     => ['2.5 S'],
                                    '2_5_vcross_l'                              => ['2.5 Vcross L'],
                                    '2_5_vcross_z'                              => ['2.5 Vcross Z'],
                                    '2_5_x_series'                              => ['2.5 X-Series'],
                                    '2_5_z'                                     => ['2.5 Z'],
                                    '2_5_z_prestige_ddi_vgs_turbo'              => ['2.5 Z Prestige Ddi VGS Turbo'],
                                    '3_0_hi_lander_z_prestige_ddi_vgs_turbo'    => ['3.0 Hi-Lander Z Prestige Ddi VGS Turbo'],
                                    '3_0_hi_lander_z_prestige'                  => ['3.0 Hi-Lander Z-Prestige'],
                                    '3_0_vcross_z'                              => ['3.0 Vcross Z'],
                                    '3_0_vcross_z_prestige'                     => ['3.0 Vcross Z-Prestige'],

                                ],


                                'spacecab_y1117'                                => [
                                    '1_9_hi_lander_z'                           => ['1.9 Hi-Lander Z'],
                                ],


                                'spark_y02_96'                                  => [
                                    '2_5_ex'                                    => ['2.5 EX'],
                                    '2_5_ex_ddi_i_teq'                          => ['2.5 EX Ddi i-TEQ'],
                                    '3_0_ex'                                    => ['3.0 EX'],
                                ],

                                'spark_y07_11'                                  => [
                                    '2_5_ex'                                    => ['2.5 EX'],
                                    '2_5_ex_ddi_i_teq'                          => ['2.5 EX Ddi i-TEQ'],
                                    '2_5_ex_platinum'                           => ['2.5 EX Platinum'],
                                    '2_5_ex_super_platinum'                     => ['2.5 EX Super Platinum'],
                                    '2_5_ex_super_titanium'                     => ['2.5 EX Super Titanium'],
                                    '2_5_exl_platinum'                          => ['2.5 EXL Platinum'],
                                ],

                                'spark_y11_17'                                  => [
                                    '1_9_b'                                     => ['1.9 B'],
                                    '1_9_s'                                     => ['1.9 S'],
                                    '2_5_b'                                     => ['2.5 B'],
                                    '2_5_spark_vgs_s'                           => ['2.5 SPARK VGS S'],
                                    '3_0_b'                                     => ['3.0 B'],
                                    '3_0_s'                                     => ['3.0 S'],
                                ],
                    ],



                    'dragon_eyes' =>[
                                'dragon_eyes_spacecab'                      => [
                                    '2_5_slx'                               => ['2.5 SLX'],
                                    '2_8_rodeo'                             => ['2.8 Rodeo'],
                                    '2_8_slx'                               => ['2.8 SLX'],
                                    '2_8_sx'                                => ['2.8 SX'],
                                ],

                                'spacecab_y96_99'                           => [
                                    '2_5_slx'                               => ['2.5 SLX'],
                                    '2_5_sx'                                => ['2.5 SX'],
                                    '2_8_rodeo'                             => ['2.8 Rodeo'],
                                    '2_8_slx'                               => ['2.8 SLX'],
                                ],

                                'spark_y96_99'                              => [
                                    '2_5_exy_std'                           => ['2.5 EXY STD'],
                                ],

                    ],


                    'dragon_power' =>[
                                'dragonpower_cab4'                          => [
                                    '2_5_sx'                                => ['2.5 SX'],
                                ],

                                'cab4_y00_02'                               => [
                                    '3_0_rodeo'                             => ['3.0 Rodeo'],
                                    '3_0_slx'                               => ['3.0 SLX'],
                                    '3_0_slx_ltd'                           => ['3.0 SLX LTD'],
                                ],

                                'dragonpower_spacecab'                      => [
                                    '2_8_rodeo_ls'                          => ['2.8 Rodeo LS'],
                                    '2_8_slx'                               => ['2.8 SLX'],
                                ],

                                'spacecab_y00_02'                           => [
                                    '2_5_sl'                                => ['2.5 SL'],
                                    '2_5_slx'                               => ['2.5 SLX'],
                                    '2_5_sx'                                => ['2.5 SX'],
                                    '3_0_slx'                               => ['3.0 SLX'],
                                ],


                                'spark_y00_02'                              => [
                                    '2_5_exy'                               => ['2.5 EXY'],
                                ],

                    ],



                    'elf' =>[
                                'elf_all'                                   => [
                                    '2_8'                                   => ['2.8'],
                                    '2_8_nkr'                               => ['2.8 NKR'],
                                    '3_0_nlr'                               => ['3.0 NLR'],
                                ],

                    ],


                    'grand_adventure' =>[
                                'g_adventure_y_96_02'                       => [
                                    '2_8_4x4'                               => ['2.8 4x4'],
                                    '3_0_4x2'                               => ['3.0 4x2'],
                                    '3_0_4x4'                               => ['3.0 4x4'],
                                ],

                    ],

                    'mu_7' =>[
                                'mu_7_y04_06'                               => [
                                    '3_0'                                   => ['3.0'],
                                ],

                                'mu_7_y07_13'                               => [
                                    '3_0_activo'                            => ['3.0 Activo'],
                                    '3_0_activo_super_platinum'             => ['3.0 Activo Super Platinum'],
                                    '3_0_activo_super_titanium'             => ['3.0 Activo Super Titanium (8)'],
                                    '3_0_choiz'                             => ['3.0 CHOIZ'],
                                    '3_0_primo'                             => ['3.0 Primo'],
                                    '3_0_primo_platinum'                    => ['3.0 Primo Platinum'],
                                    '3_0 primo_super_platinum'              => ['3.0 Primo Super Platinum (17)'],
                                    '3_0_primo_super_titanium'              => ['3.0 Primo Super Titanium (35)'],
                                ],
                    ],



                    'mu_x' =>[
                                'mu_x_y13_17'                               => [
                                    '1_9'                                   => ['1.9'],
                                    '2_5'                                   => ['2.5'],
                                    '3_0'                                   => ['3.0'],
                                ],

                                'mu_x_1317'                                 => [
                                    '1_9'                                   => ['1.9'],
                                    '3_0'                                   => ['3.0'],
                                ],
                    ],


                    'trf' =>[
                                'dragon_gold'                               => [
                                    '2_5_sl'                                => ['2.5 SL'],
                                    '2_5_space_cab'                         => ['2.5 Space Cab'],
                                    '2_5_station_wagon'                     => ['2.5 Station Wagon'],
                                    '2_5_supreme'                           => ['2.5 Supreme'],
                                ],
                    ],


                    'trooper' =>[
                                'trooper_y91_03'                            => [
                                    '3_2_se'                                => ['3.2 SE'],
                                    '3_2_xs'                                => ['3.2 XS'],
                                ],
                    ],


                    'vega' =>[
                                'vega_y97_03'                               => [
                                    '2_8'                                   => ['2.8'],
                                    '3_0'                                   => ['3.0'],
                                ],
                    ],
                ], //ปิด isuzu

                'nissan' => [
                    '200sx'  =>[
                                '200sx_y92_95'          => [
                                      '1_8'             => ['1.8'],
                                ],
                    ],

                    '300zx'  =>[
                                '300zx_y92_98'          => [
                                      '3_0'             => ['3.0'],
                                ],
                    ],

                    '350z'  =>[
                                '350z_y03_09'           => [
                                      '3_5'             => ['3.5'],
                                      '3_5_v6'          => ['3.5 V6'],
                                      '3_5_z33'         => ['3.5 Z33'],
                                ],
                    ],

                    '370z'  =>[
                                '300zx_y09_15'          => [
                                      '3_7'             => ['3.7'],
                                ],
                    ],

                    'almera'  =>[
                                'almera_y11_16'          => [
                                      '1_2_e'            => ['1.2 E'],
                                      '1_2_e_nismo'      => ['1.2 E Nismo'],
                                      '1.2 e sportech'   => ['1.2 E SPORTECH'],
                                      '1_2_el'           => ['1.2 EL'],
                                      '1_2_es'           => ['1.2 ES'],
                                      '1_2_v'            => ['1.2 V'],
                                      '1_2_v_sportech'   => ['1.2 V SPORTECH'],
                                      '1_2_vl'           => ['1.2 VL'],
                                      '1_2_nismo'        => ['1.2 Nismo'],
                                      '1_2_vl_sportech'  => ['1.2 VL SPORTECH'],
                                ],
                                'almera_y17'          => [
                                      '1_2_e'            => ['1.2 E'],
                                      '1_2_e_nismo'      => ['1.2 E Nismo'],
                                      '1.2 e sportech'   => ['1.2 E SPORTECH'],
                                      '1_2_el'           => ['1.2 EL'],
                                      '1_2_es'           => ['1.2 ES'],
                                      '1_2_v'            => ['1.2 V'],
                                      '1_2_v_sportech'   => ['1.2 V SPORTECH'],
                                      '1_2_vl'           => ['1.2 VL'],
                                      '1_2_nismo'        => ['1.2 Nismo'],
                                      '1_2_vl_sportech'  => ['1.2 VL SPORTECH'],
                                ],
                    ],

                    'bigm'  =>[
                                'bigm_4dr'              => [
                                      '2_0'             => ['2.0'],
                                ],

                                'bigm_kingcab'          => [
                                      '2_5_super_dx'    => ['2.5 Super DX'],
                                ],

                                'bigm_single'           => [
                                      '1_6_super_gl'    => ['1.6 Super GL'],
                                      '2_5_super_dx'    => ['2.5 Super DX'],
                                      '2_7'             => ['2.7'],
                                ],
                    ],

                    'bluebird_atessa'  =>[
                                'bluebird_a_y93_97'     => [
                                      '2_0_sss_g'       => ['2.0 SSS-G'],
                                ],
                    ],

                    'cedric'  =>[
                                'cedric_y92_95'         => [
                                      '2_0_classic'     => ['2.0 Classic'],
                                      '3_0_gran_turismo'=> ['3.0 Gran Turismo'],

                                ],
                    ],

                    'cefiro'  =>[
                                'A31'                   => [
                                      '2_0'             => ['2.0'],
                                      '2_0_12v'         => ['2.0 12V'],
                                ],

                                'A32'          => [
                                      '2_0_20g'         => ['2.0 20G'],
                                      '2_0_vip'         => ['2.0 VIP'],
                                      '2_0_vq20'        => ['2.0 VQ20'],
                                      '3_0_vip'         => ['3.0 VIP'],
                                ],

                                'A33'          => [
                                      '2_0_excimo'      => ['2.0 Excimo'],
                                      '2_0_executive'   => ['2.0 Executive'],
                                      '3_0_brougham'    => ['3.0 Brougham'],
                                ],
                    ],


                    'cube'  =>[
                                'cube_y02_08'          => [
                                      '1_4_z11'        => ['1.4 Z11'],
                                      '1_4_z11_e_4wd'  => ['1.4 Z11 e-4WD'],
                                      '1_5_z11'        => ['1.5 Z11'],
                                ],

                                'cube_y02_15'          => [
                                      '1_5_z12'        => ['1.5 Z12'],
                                ],
                    ],


                    'elgrand'  =>[
                                'elgrand_y02_10'            => [
                                      '2_5_rider'           => ['2.5 Rider'],
                                ],

                                'elgrand_y10_15'            => [
                                      '2.5 high-way star'   => ['2.5 High-Way Star'],
                                ],
                                'elgrand_97_02'             => [
                                      '3_0_di'              => ['3.0 Di'],
                                      '3_3_v'               => ['3.3 V'],
                                      '3_5_v6'              => ['3.5 v6'],
                                ],
                    ],


                    'figaro'  =>[
                                'figaro_y91_95'             => [
                                      '1_0_turbo'           => ['1.0 Turbo '],
                                ],
                    ],


                    'frontier'  =>[
                                'frontier_4dr'              => [
                                      '2_5'                 => ['2.5'],
                                      '2_5_ax_l'            => ['2.5 AX-L'],
                                      '2_7_tl'              => ['2.7 TL'],
                                      '3_0_zdi'             => ['3.0 ZDi'],
                                      '3_0_zdi_t'           => ['3.0 ZDi-T'],
                                ],

                                'frontier_kingcab'          => [
                                      '2_5_al'              => ['2.5 AL'],
                                      '2_5_ax'              => ['2.5 AX'],
                                      '2_5_ax_super'        => ['2.5 AX Super'],
                                      '2_5_ax_l'            => ['2.5 AX-L'],
                                      '2_5_yd'              => ['2.5 YD'],
                                      '2_7_tl'              => ['2.7 TL'],
                                      '2_7_txp'             => ['2.7 TXP'],
                                      '3_0_zdi'             => ['3.0 ZDi '],
                                      '3_0_zdi_t'           => ['3.0 ZDi-T'],
                                ],

                                'frontier_single'           => [
                                      '2_5_ae'              => ['2.5 AE'],
                                      '2_5_aep'             => ['2.5 AEP'],
                                      '2_5_sx'              => ['2.5 SX'],
                                      '2_7_tx'              => ['2.7 TX'],
                                      '2_7_txp'             => ['2.7 TXP'],
                                ],
                    ],

                    'frontier_nacara'  =>[
                                'f_nacara_4dr'                          => [
                                      '2_5_black_star_limited'          => ['2.5 Black Star Limited'],
                                      '2_5_calibre'                     => ['2.5 Calibre'],
                                      '2_5_calibre_le'                  => ['2.5 Calibre LE'],
                                      '2_5_calibre_le_grand_titanium'   => ['2.5 CALIBRE LE Grand Titanium'],
                                      '2_5_calibre_se'                  => ['2.5 Calibre SE'],
                                      '2_5_calibre_sport_version'       => ['2.5 Calibre Sport Version'],
                                      '2_5_gt'                          => ['2.5 GT'],
                                      '2_5_le'                          => ['2.5 LE'],
                                      '2_5_le_calibre'                  => ['2.5 LE Calibre'],
                                      '2_5_se'                          => ['2.5 SE'],
                                      '2_5_sv_calibre_le'               => ['2.5 SV Calibre LE'],
                                      '2_5_sv_calibre_se'               => ['2.5 SV Calibre SE'],
                                      '2_5_sv_le'                       => ['2.5 SV LE'],
                                ],

                                'f_nacara_kingcab'                      => [
                                      '2_5_calibre'                     => ['2.5 Calibre'],
                                      '2_5_calibre_le'                  => ['2.5 Calibre LE'],
                                      '2_5_calibre_le_grand_titanium'   => ['2.5 Calibre LE Grand Titanium'],
                                      '2_5_calibre_se'                  => ['2.5 Calibre SE'],
                                      '2_5_calibre_sport_version'       => ['2.5 Calibre Sport Version'],
                                      '2_5_gt_calibre_le'               => ['2.5 GT Calibre LE'],
                                      '2_5_le'                          => ['2.5 LE'],
                                      '2_5_le_calibre'                  => ['2.5 LE Calibre'],
                                      '2_5_le_calibre_gt'               => ['2.5 LE Calibre GT'],
                                      '2_5_se'                          => ['2.5 SE'],
                                      '2_5_se_cng'                      => ['2.5 SE CNG'],
                                      '2_5_sv'                          => ['2.5 SV'],
                                      '2_5_sv_calibre'                  => ['2.5 SV Calibre'],
                                      '2_5_sv_le'                       => ['2.5 SV LE'],
                                ],


                                'f_nacara_single'                       => [
                                      '2_5_xe'                          => ['2.5 XE'],
                                      '2_5_xe_cng'                      => ['2.5 XE CNG'],
                                ],
                    ],



                    'gt_r'  =>[
                                'gt_r_y08_15'                           => [
                                      '3_8_r35'                         => ['3.8 R35'],
                                ],
                    ],

                    'infiniti_r'  =>[
                                'gt_r_y08_15'                           => [
                                      '4_1'                             => ['4.1'],
                                ],
                    ],

                    'juke'  =>[
                                'juke_y10_16'                           => [
                                      '1_5_jp'                          => ['1.5 JP'],
                                      '1_6_e'                           => ['1.6 E'],
                                      '1_6_invader'                     => ['1.6 Invader'],
                                      '1_6_tokyo_edition'               => ['1.6 Tokyo Edition'],
                                      '1_6_turbo'                       => ['1.6 Turbo'],
                                      '1_6_v'                           => ['1.6 V'],
                                ],
                    ],

                    
                    'leaf'  =>[
                                'leaf_y12_16'                           => [
                                      '1_5_jp'                          => ['1.5 JP'],
                                      '1_6_e'                           => ['1.6 E'],
                                      '1_6_invader'                     => ['1.6 Invader'],
                                      '1_6_tokyo_edition'               => ['1.6 Tokyo Edition'],
                                      '1_6_turbo'                       => ['1.6 Turbo'],
                                      '1_6_v'                           => ['1.6 V'],
                                ],
                    ],

                    'livina'  =>[
                                'livina_y14_17'                         => [
                                      '1_6_e'                           => ['1.6 E'],
                                      '1_6_v'                           => ['1.6 V'],
                                ],
                    ],

                    'march'  =>[
                                'march_y10_16'                          => [
                                      '1_2_e'                           => ['1.2 E'],
                                      '1_2_e_limited_edition'           => ['1.2 E Limited Edition'],
                                      '1_2_e_smart_edition'             => ['1.2 E Smart Edition'],
                                      '1_2_el'                          => ['1.2 EL'],
                                      '1_2_el_limited_edition'          => ['1.2 EL Limited Edition'],
                                      '1_2_s'                           => ['1.2 EL Sport Version'],
                                      '1_2_e_limited_edition'           => ['1.2 S'],
                                      '1_2_sport_version'               => ['1.2 Sport Version'],
                                      '1_2_v'                           => ['1.2 V'],
                                      '1.2 vl'                          => ['1.2 VL'],
                                      '1_2_vl_sport_version'            => ['1.2 VL Sport Version'],

                                ],
                    ],

                    'murano'  =>[
                                'murano_y04_10'                         => [
                                      '2_5'                             => ['2.5'],
                                      '3_5'                             => ['3.5'],
                                ],
                    ],


                    'note'  =>[
                                'note_y17_21'                           => [
                                      '1_2_v'                           => ['1.2 V'],
                                      '1_2_vl'                          => ['1.2 VL'],
                                ],
                    ],

                    'np_300_navara'  =>[
                                'np300_dbcab'                           => [
                                      '2_5_calibre_e'                   => ['2.5 Calibre E'],
                                      '2_5_calibre_el'                  => ['2.5 Calibre EL'],
                                      '2_5_calibre_sportech'            => ['2.5 Calibre Sportech'],
                                      '2_5_calibre_v'                   => ['2.5 Calibre V'],
                                      '2_5_calibre_vl'                  => ['2.5 Calibre VL'],
                                      '2_5_e'                           => ['2.5 E'],
                                      '2_5_s'                           => ['2.5 S'],
                                      '2_5_vl'                          => ['2.5 VL'],
                                ],

                                'np_300_kcab'                           => [
                                      '2_5_calibre_e'                   => ['2.5 Calibre E'],
                                      '2_5_calibre_el'                  => ['2.5 Calibre EL'],
                                      '2_5_calibre_s'                   => ['2.5 Calibre S'],
                                      '2_5_calibre_sportech'            => ['2.5 Calibre Sportech'],
                                      '2_5_calibre_v'                   => ['2.5 Calibre V'],
                                      '2_5_e'                           => ['2.5 E'],
                                      '2_5_el_calibre'                  => ['2.5 EL Calibre'],
                                      '2_5_s'                           => ['2.5 S'],
                                      '2_5_v'                           => ['2.5 V'],
                                ],

                                'np_300_single'                         => [
                                      '2_5_s'                           => ['2.5 S'],
                                      '2_5_sl'                          => ['2.5 Calibre SL'],
                                ],
                    ],

                    'nv'  =>[
                                'nv_queencab'                           => [
                                      '1_6_queen_cab'                   => ['1.6 Queen Cab'],
                                      '1_6_queen_cab_lxt'               => ['1.6 Queen Cab LXT'],
                                      '1_6_queen_cab_slx'               => ['1.6 Queen Cab SLX'],
                                      '1_6_slx'                         => ['1.6 SLX'],
                                ],

                                'nv_single'                             => [
                                      '1_6_slx'                         => ['1.6 SLX'],
                                ],

                                'nv_wingroad'                           => [
                                      '1_6_slx'                         => ['1.6 SLX'],
                                      '1_6_wing_road_slx'               => ['1.6 Wing Road SLX'],
                                ],
                    ],

                    'nx'  =>[
                                'nx_y92_95'                             => [
                                      '1_6'                             => ['1.6'],
                                ],
                    ],

                    'pao'  =>[
                                'pao_y89_92'                            => [
                                      '1_0'                             => ['1.0'],
                                ],
                    ],

                    'presea'  =>[
                                'presea_y92_97'                         => [
                                      '1_8'                             => ['1.8'],
                                ],
                    ],

                    'president'  =>[
                                'president_y93_95'                      => [
                                      '4_5'                             => ['4.5'],
                                ],
                    ],

                    'primera'  =>[
                                'primera_y97_99'                        => [
                                      '2_0'                             => ['2.0'],
                                ],
                    ],

                    'pulsar'  =>[
                                'pulsar_y12_16'                         => [
                                      '1_6_s'                           => ['1.6 S'],
                                      '1_6_sv'                          => ['1.6 SV'],
                                      '1_6_v'                           => ['1.6 V'],
                                      '1_8_v'                           => ['1.8 V'],
                                ],
                    ],

                    'sentra'  =>[
                                'sentra_y92_96'                         => [
                                      '1_6_ex_saloon'                   => ['1.6 EX Saloon'],
                                ],
                    ],

                    'serena'  =>[
                                'serena_y00_05'                         => [
                                      '2_0_high_way_star'               => ['2.0 High-Way Star'],
                                ],
                    ],

                    'silvia'  =>[
                                'silvia_y00_05'                         => [
                                      '2_0_s15'                         => ['2.0 S15'],
                                      '2_0_spec_r'                      => ['2.0 Spec-R'],
                                ],
                    ],

                    'skyline'  =>[
                                'skyline_r33'                           => [
                                      '2_6_gt_r'                        => ['2.6 GT-R'],
                                ],
                                'skyline_v35'                           => [
                                      '3_5_350gt'                       => ['3.5 350GT'],
                                ],
                                'skyline_v36'                           => [
                                      '3_7_370gt'                       => ['3.7 370GT'],
                                ],
                    ],

                    'sunny'  =>[
                                'b14_15'                                => [
                                      '1_5_ex_saloon'                   => ['1.5 EX Saloon'],
                                      '1_6_super_gl_saloon'             => ['1.6 Super GL Saloon'],
                                      '1_6_super_saloon'                => ['1.6 Super Saloon'],
                                ],
                                'sunny_neo'                             => [
                                      '1_6_gl_neo'                      => ['1.6 GL Neo'],
                                      '1_6_super_gl'                    => ['1.6 Super GL'],
                                      '1_6_super_neo'                   => ['1.6 Super Neo'],
                                      '1_8_almera'                      => ['1.8 Almera'],
                                      '1_8_almera_young'                => ['1.8 Almera Young'],
                                      '1_8_vip_neo'                     => ['1.8 VIP Neo'],
                                ],
                                'sunny_neo_y04_06'                      => [
                                      '1_6_gl'                          => ['1.6 GL'],
                                      '1_6_gl_neo'                      => ['1.6 GL Neo'],
                                      '1_6_super_neo'                   => ['1.6 Super Neo'],
                                      '1_8_super_neo'                   => ['1.8 Super Neo'],
                                      '1_8_vip_neo'                     => ['1.8 VIP Neo'],
                                ],
                    ],

                    'sylphy'  =>[
                                'sylphy_y12_16'                         => [
                                      '1_6_e'                           => ['1.6 E'],
                                      '1_6_e_cng'                       => ['1.6 E CNG'],
                                      '1_6_s'                           => ['1.6 S'],
                                      '1_6_sv'                          => ['1.6 SV'],
                                      '1_6_v'                           => ['1.6 V'],
                                      '1_8_V'                           => ['1.8 V'],
                                ],
                    ],

                    'teena'  =>[
                                'teena_y04_08'                         => [
                                      '2_0_200_jk'                     => ['2.0 200 JK'],
                                      '2_0_200jk'                      => ['2.0 200JK'],
                                      '2_3_230_jk'                     => ['2.3 230 JK'],
                                      '2_3_230_jm'                     => ['2.3 230 JM'],
                                      '2_3_230_js'                     => ['2.3 230 JS'],
                                      '2_3_230jm'                      => ['2.3 230JM'],
                                      '2_3_230js'                      => ['2.3 230JS'],
                                ],

                                'teena_y09_13'                         => [
                                      '2_0_200_xl'                     => ['2.0 200 XL'],
                                      '2_0_200_xl_sport'               => ['2.0 200 XL Sport'],
                                      '2_0_200_xl_sports_series_navi'  => ['2.0 200 XL Sports Series Navi'],
                                      '2_5_250_xv'                     => ['2.5 250 XV'],
                                      '2_5_250_xv_sport'               => ['2.5 250 XV Sport'],
                                      '2_5_250_xv_sports_series_navi'  => ['2.5 250 XV Sports Series Navi'],
                                ],

                                'teena_13_16'                         => [
                                      '2_0_xe'                        => ['2.0 XE'],
                                      '2_0_xl'                        => ['2.0 XL'],
                                      '2_5_xv'                        => ['2.5 XV'],
                                ],
                    ],

                    'terrano_ii'  =>[
                                'terrano_ii_y95_00'                   => [
                                      '2_4'                           => ['2.4'],
                                ],
                    ],


                    'tiida'  =>[
                                'tiida_y06_12'                        => [
                                      '2_4'                           => ['2.4'],
                                ],
                    ],

                    'urvan'  =>[
                                'urvan_y01_12'                        => [
                                      '3_0_gx'                        => ['3.0 GX'],
                                ],

                                'urvan_y13_17'                        => [
                                      '2_5_nv350'                     => ['2.5 NV350'],
                                      '2_5_nv350_cng'                 => ['2.5 NV350 CNG'],
                                ],


                                'urvan_y13_17'                        => [
                                      '2_5_nv350'                     => ['2.5 NV350'],
                                      '2_5_nv350_cng'                 => ['2.5 NV350 CNG'],
                                ],
                    ],

                    'x-trail'  =>[
                                'x-trail_y02_08'                      => [
                                      '2_5_comfort'                   => ['2.5 Comfort'],
                                      '2_5_luxury'                    => ['2.5 Luxury'],
                                ],

                                'x-trail_y08_13'                      => [
                                      '2_0'                           => ['2.0'],
                                      '2_0_v'                         => ['2.0 V'],
                                ],


                                'x-trail_y14_17'                      => [
                                      '2_0_e'                         => ['2.0 E'],
                                      '2_0_vs'                        => ['2.0 V'],
                                      '2_5_v'                         => ['2.5 V'],
                                ],
                                'x-trail_15_19'                       => [
                                      '2_0_e'                         => ['2.0 E'],
                                      '2_0_e_hybrid'                  => ['2.0 E Hybrid'],
                                      '2_0_v'                         => ['2.0 V'],
                                      '2_0_v_hybrid'                  => ['2.0 V Hybrid'],
                                ],
                    ],

                    'xciter'  =>[
                                'xciter_y01_06'                       => [
                                      '3_0_super_gl'                  => ['3.0 Super GL'],
                                ],
                    ],
                ],//ปิด nissan






                'mitsubishi' => [
                    '300gt'  =>[
                                '300gt_y92_97'                      => [
                                      '3_0'                         => ['3.0'],
                                ],
                    ],

                    'attrage'  =>[
                                'attrage_y13_16'                    => [
                                      '1_2_gls'                     => ['1.2 GLS'],
                                      '1_2_gls_limited'             => ['1.2 GLS Limited'],
                                      '1_2_gls_ltd'                 => ['1.2 GLS LTD'],
                                      '1_2_glx'                     => ['1.2 GLX'],
                                ],
                    ],

                        'canter'  =>[
                                    'all_canter'                    => [
                                      '2_8'                         => ['2.8'],
                                ],
                    ],

                    'cyblone'  =>[
                                'aero_body_y09_05'                  => [
                                      '2_5_aero_body'               => ['2.5 Aero body'],
                                ],
                                'cyblone_single'                    => [
                                      '2_5'                         => ['2.5'],
                                ],
                    ],

                    'delica_space_wagon'  =>[
                                'aero_body_y15_18'                  => [
                                      '2_0'                         => ['2.0'],
                                ],
                    ],

                        'evolution'  =>[
                                    'iv_face_benz'                  => [
                                      '2_0_iv'                      => ['2.0 IV'],
                                ],

                                'ix_lancer'                         => [
                                      '2_0_v'                       => ['2.0 V'],
                                ],

                                'v_end_benz'                        => [
                                      '2_0_v'                       => ['2.0 V'],
                                ],

                                'vi_end_benz'                       => [
                                      '2_0_vi'                      => ['2.0 VI'],
                                ],


                                'vii_Cedia'                         => [
                                      '2_0_vii'                     => ['2.0 VII'],
                                ],

                                'viii_lancer'                       => [
                                      '2_0_viii'                    => ['2.0 VIII'],
                                ],

                                'x_lancer_ex'                       => [
                                      '2_0_x'                       => ['2.0 X'],
                                ],
                    ],

                    'galant'  =>[
                                'galant_y76_80'                     => [
                                      '2_0_lambda'                  => ['2.0 Lambda'],
                                      '2_6_lambda'                  => ['2.6 Lambda'],
                                ],

                                'galant_y80_87'                     => [
                                      '1_6'                         => ['1.6'],
                                ],


                                'galant_y92_99'                     => [
                                      '2_0_ultima'                  => ['2.0 Ultima'],
                                      '2_0_ultima_glsi'             => ['2.0 Ultima GLSi'],
                                ],
                    ],

                    'l300'  =>[
                                'l300_y92_99'                       => [
                                      '2_5'                         => ['2.5'],
                                ],
                    ],


                    'lancer'  =>[
                                'lancer_y04_12'                     => [
                                      '1_6_glx'                     => ['1.6 GLX'],
                                      '1_6_glx_cng'                 => ['1.6 GLX CNG'],
                                      '1_6_glxi'                    => ['1.6 GLXi'],
                                      '1_6_glxi_ltd'                => ['1.6 GLXi LTD'],
                                      '1_6_glxi_ltd_ralliart'       => ['1.6 GLXi LTD Ralliart'],
                                      '1_6_sei'                     => ['1.6 SEi'],
                                      '2_0_sei_ltd'                 => ['2.0 SEi LTD'],
                                      '2.0 sei ralliart'            => ['2.0 SEi Ralliart'],
                                ],

                                'cedia_y01_04'                      => [
                                      '1_6_cedia_glxi'              => ['1.6 Cedia GLXi'],
                                      '1_6_cedia_glxi_ltd.'         => ['1.6 Cedia GLXi-LTD.'],
                                      '1_8_cedia_sei_ltd'           => ['1.8 Cedia SEi-LTD'],
                                ],

                                'champ_i6_y84_85'                   => [
                                      '1_3_gl'                      => ['1.3 GL'],
                                      '1_5'                         => ['1.5'],
                                ],

                                'ecar_y92_96'                       => [
                                      '1_5_glx'                     => ['1.5 GLX'],
                                      '1_5_glxi'                    => ['1.5 GLXi'],
                                      '1_6_glxi'                    => ['1.6 GLXi'],
                                      '1_8_gli'                     => ['1.8 GLi'],
                                ],


                                'f_style_endbenz'                   => [
                                      '1_5_glxi'                    => ['1.5 GLXi'],
                                      '1_6_glxi'                    => ['1.6 GLXi'],
                                      '1_6_glxi_limited'            => ['1.6 GLXi Limited'],
                                      '1_8_sei'                     => ['1.8 SEi'],
                                      '1_8_sei_ltd'                 => ['1.8 SEi LTD'],
                                ],
                    ],


                    'lancer_ex'  =>[
                                'lancer_ex_y09_15'                  => [
                                      '1_8_gls'                     => ['1.8 GLS'],
                                      '1_8_gls_ltd'                 => ['1.8 GLS LTD'],
                                      '1_8_glx'                     => ['1.8 GLX'],
                                      '2_0_gt'                      => ['2.0 GT'],
                                ],
                    ],


                    'lancer_ex'  =>[
                                'lancer_ex_y09_15'                  => [
                                      '1_8_gls'                     => ['1.8 GLS'],
                                      '1_8_gls_ltd'                 => ['1.8 GLS LTD'],
                                      '1_8_glx'                     => ['1.8 GLX'],
                                      '2_0_gt'                      => ['2.0 GT'],
                                ],
                    ],

                    'mirage'  =>[
                                'mirage_y12_16'                         => [
                                      '1_2_gl'                          => ['1.2 GL'],
                                      '1_2_gls'                         => ['1.2 GLS'],
                                      '1_2_gls_limited'                 => ['1.2 GLS Limited'],
                                      '1_2_gls_limited_bloom_edition'   => ['1.2 GLS Limited Bloom Edition'],
                                      '1_2_gls_ltd'                     => ['1.2 GLS LTD'],
                                ],
                    ],


                    'pajero'  =>[
                                'pajero_y00_06'                     => [
                                      '2_8'                         => ['2.8'],
                                      '3_5_gls'                     => ['3.5 GLS'],
                                ],

                                'pajero_y08_15'                     => [
                                      '3.8 exceed'                  => ['3.8 Exceed'],
                                ],

                                'pajero_y92_02'                     => [
                                      '2_5'                         => ['2.5 '],
                                ],
                    ],

                    'pajero'  =>[
                                'pajero_y00_06'                     => [
                                      '2_8'                         => ['2.8'],
                                      '3_5_gls'                     => ['3.5 GLS'],
                                ],

                                'pajero_y08_15'                     => [
                                      '3.8 exceed'                  => ['3.8 Exceed'],
                                ],

                                'pajero_y92_02'                     => [
                                      '2_5'                         => ['2.5'],
                                      '3_0_glx'                     => ['3.0 GLX'],
                                      '3_5_gls'                     => ['3.5 GLS'],
                                ],
                    ],

                    'pajero_io'  =>[
                                'pajero_io_y98_07'                  => [
                                      '1_8_gdi'                     => ['1.8 GDI'],
                                ],
                    ],

                    'pajero_junior'  =>[
                                'pajero_junior_y95_98'              => [
                                      '1_1_zr_ii'                   => ['1.1 ZR-II'],
                                ],
                    ],

                    'pajero_sport'  =>[
                                'pajero_sport_y08_15'               => [
                                      '2_4_gls'                     => ['2.4 GLS'],
                                      '2_5_gls'                     => ['2.5 GLS'],
                                      '2_5_gt'                      => ['2.5 GT'],
                                      '3_0_gt'                      => ['3.0 GT'],
                                      '3_2_gls'                     => ['3.2 GLS'],
                                      '3_2_gt'                      => ['3.2 GT'],
                                ],

                                'pajero_sport_y15_18'               => [
                                      '2_4_gls_ltd'                 => ['2.4 GLS LTD'],
                                      '2_4_gt'                      => ['2.4 GT'],
                                      '2_4_gt_premium'              => ['2.4 GT Premium'],
                                ],
                    ],


                    'space_runner'  =>[
                                'space_runner_y92_00'               => [
                                      '1_8'                         => ['1.8'],
                                ],
                    ],


                    'space_wagon'  =>[
                                'space_wagon_y04_12'                => [
                                      '2_4_gls'                     => ['2.4 GLS'],
                                      '2_4_gls_limited'             => ['2.4 GLS Limited'],
                                      '2_4_gt'                      => ['2.4 GT'],
                                ],
                    ],


                    'strada'  =>[
                                'strada_grandis4dr'                  => [
                                      '2_5_grandis'                  => ['2.5 Grandis'],
                                      '2_8_grandis_gls'              => ['2.8 Grandis GLS'],
                                      '2_8_grandis_glx'              => ['2.8 Grandis GLX'],
                                ],

                                'strada_megacab'                     => [
                                      '2_5_grandis'                  => ['2.5 Grandis'],
                                      '2_8_grandis_gls'              => ['2.8 Grandis GLS'],
                                      '2_8_grandis_glx'              => ['2.8 Grandis GLX'],
                                ],

                                'strada_sngle'                       => [
                                      '2_5_std'                      => ['2.5 STD'],
                                ],
                    ],

                    'strada_g_wagon'  =>[
                                'strada_g_wagon_y01_06'              => [
                                      '2_5_vg_turbo'                 => ['2.5 VG Turbo'],
                                      '2_8_euro_evolution'           => ['2.8 Euro Evolution'],
                                      '2_8_euro_evolution_ii'        => ['2.8 Euro Evolution II'],
                                      '2_8_gls'                      => ['2.8 GLS'],
                                      '2_8_glx'                      => ['2.8 GLX']
                                ],
                    ],

                    'triton'  =>[
                                'double_cab_y05_15'                  => [
                                      '2_4_cng'                      => ['2.4 CNG'],
                                      '2_4_gls_plus'                 => ['2.4 GLS Plus'],
                                      '2_4_glx'                      => ['2.4 GLX'],
                                      '2_4_plus'                     => ['2.4 PLUS'],
                                      '2_4_plus_cng'                 => ['2.4 PLUS CNG'],
                                      '2_5_gl'                       => ['2.5 GL'],
                                      '2_5_gls'                      => ['2.5 GLS'],
                                      '2_5_gls_plus'                 => ['2.5 GLS Plus'],
                                      '2_5_gls_limited'              => ['2.5 GLS-Limited'],
                                      '2_5_glx'                      => ['2.5 GLX'],
                                      '2_5_plus'                     => ['2.5 PLUS'],
                                      '2_5_plus_vg_turbo'            => ['2.5 PLUS VG TURBO'],
                                      '2_5_plus_gls'                 => ['2.5 PLUS GLS'],
                                      '3_2_gls'                      => ['3.2 GLS'],
                                ],

                                'double_cab_y14_19'                  => [
                                      '2_4_gls'                      => ['2.4 GLS'],
                                      '2_4_gls_plus'                 => ['2.4 GLS Plus'],
                                      '2_4_gls_limited'              => ['2.4 GLS-Limited'],
                                      '2_4_gls_limited_plus'         => ['2.4 GLS-Limited_Plus'],
                                      '2_4_glx'                      => ['2.4 GLX'],
                                      '2_4_glx_plus'                 => ['2.4 GLX Plus'],
                                      '2_4_plus_gls'                 => ['2.4 PLUS GLS'],
                                      '2_4_gls_limited'              => ['2.4 GLS-Limited'],
                                      '2_5_glx'                      => ['2.5 GLX'],
                                ],

                                'mega_cab_y05_15'                    => [
                                      '2_4_gl'                       => ['2.4 GL'],
                                      '2_4_gls_plus'                 => ['2.4 GLS Plus'],
                                      '2_4_glx'                      => ['2.4 GLX'],
                                      '2_4_plus'                     => ['2.4 PLUS'],
                                      '2_5_gl'                       => ['2.5 GL'],
                                      '2_5_gls'                      => ['2.5 GLS'],
                                      '2_5_gls_plus'                 => ['2.5 GLS Plus'],
                                      '2_5_glx'                      => ['2.5 GLX'],
                                      '2_5_glx_plus'                 => ['2.5 GLX Plus'],
                                      '2_5_plus_vg_turbo'            => ['2.5 PLUS VG TURBO'],
                                ],

                                'mega_cab_y14_19'                    => [
                                      '2_4_glx'                      => ['2.4 GLX'],
                                      '2_4_glx_plus'                 => ['2.4 GLX Plus'],
                                      '2_5_gl'                       => ['2.5 GL'],
                                      '2_5_glx'                      => ['2.5 GLX'],
                                ],

                                'megacab_y05_15'                     => [
                                      '2_4_cng'                      => ['2.4 CNG'],
                                      '2_4_plus'                     => ['2.4 PLUS'],
                                      '2_4_plus_cng'                 => ['2.4 PLUS CNG'],
                                      '2_5_gls'                      => ['2.5 GLS'],
                                      '2_5_plus'                     => ['2.5 PLUS'],
                                      '2_5_plus_gls'                 => ['2.5 PLUS GLS'],
                                      '2_5_plus_gls_vg_turbo'        => ['2.5 PLUS GLS VG TURBO'],
                                      '2_5_plus_vg_turbo'            => ['2.5 PLUS VG TURBO'],
                                ],

                                'megacab_y14_19'                     => [
                                      '2_4_gls_plus'                 => ['2.4 GLS Plus'],
                                      '2_4_gls_limited_plus'         => ['2.4 GLS-Limited Plus'],
                                      '2_4_glx'                      => ['2.4 GLX'],
                                      '2_4_glx_plus'                 => ['2.4 GLX Plus'],
                                      '2_4_plus_gls_limited'         => ['2.4 PLUS GLS-Limited'],
                                      '2_4_plus_glx'                 => ['2.4 PLUS GLX'],
                                      '2_5_gl'                       => ['2.5 GL'],
                                      '2_5_gls'                      => ['2.5 GLS'],
                                      '2_5_glx'                      => ['2.5 GLX'],
                                ],

                                'single_y05_15'                      => [
                                      '2_4_cng'                      => ['2.4 CNG'],
                                      '2_4_gl'                       => ['2.4 GL'],
                                      '2_5_gl'                       => ['2.5 GL'],
                                      '2_5_gl_vg_turbo_4wd'          => ['2.5 GL VG Turbo 4WD'],
                                      '3.2 gl 4wd'                   => ['3.2 GL 4WD'],
                                ],

                                'single_y14_19'                      => [
                                      '2_4_gl'                       => ['2.4 GL'],
                                      '2_5_gl'                       => ['2.5 GL'],
                                ],
                    ],

                ], // ปิด mitsubishi

                'mazda' => [
                    '2'     =>[
                                '2_y09_14'                          => [
                                      '1_5_elegance_groove'         => ['1.5 Elegance Groove'],
                                      '1_5_elegance_maxx'           => ['1.5 Elegance Maxx'],
                                      '1_5_elegance_racing_series'  => ['1.5 Elegance Racing Series'],
                                      '1_5_elegance_spirit'         => ['1.5 Elegance Spirit'],
                                      '1_5_groove'                  => ['1.5 Groove'],
                                      '1_5_maxx_sports'             => ['1.5 Maxx Sports'],
                                      '1_5_navi'                    => ['1.5 Navi'],
                                      '1_5_R'                       => ['1.5 R'],
                                      '1_5_S'                       => ['1.5 S'],
                                      '1_5_sports_maxx'             => ['1.5 Sports Maxx'],
                                      '1_5_sports_maxx_sports'      => ['1.5 Sports Maxx Sports'],
                                      '1_5_sports_racing_series'    => ['1.5 Sports Racing Series'],
                                      '1_5_sports_spirit'           => ['15 Sports Spirit'],
                                      '1.5 v'                       => ['1.5 V'],


                                ],

                                '2_y15_18'                          => [
                                      '1_3_high'                    => ['1.3 High'],
                                      '1_3_high_plus'               => ['1.3 High Plus'],
                                      '1_3_sports'                  => ['1.3 Sports'],
                                      '1_3_sports_high'             => ['1.3 Sports High '],
                                      '1_3_sports_high_connect'     => ['1.3 Sports High Connect'],
                                      '1_3_sports_high_plus'        => ['1.3 Sports High Plus'],
                                      '1_3_sports_standard'         => ['1.3 Sports Standard'],
                                      '1_3_standard'                => ['1.3 Standard'],
                                      '1_5_xd'                      => ['1_5_XD'],
                                      '1_5_xd_high'                 => ['1.5 XD High'],
                                      '1_5_xd_high_connect'         => ['1.5 XD High Connect'],
                                      '1_5_xd_high_plus'            => ['1.5 XD High Plus'],
                                      '1_5_xd_high_plus_l'          => ['1.5 XD High Plus L'],
                                      '1_5_xd_sports'               => ['1.5 XD Sports'],
                                      '1_5_xd_sports_high'          => ['1.5 XD Sports High'],
                                      '1_5_xd_sports_high_connect'  => ['1.5 XD Sports High Connect'],
                                      '1.5 xd sports high plus'     => ['1.5 XD Sports High Plus'],


                                ],

                    ],

                    '3'     =>[
                                '3_y05_10'                          => [
                                      '1_6_groove'                  => ['1.6 Groove'],
                                      '1_6_life'                    => ['1.6 Life'],
                                      '1_6_s'                       => ['1.6 S'],
                                      '1_6_spirit'                  => ['1.6 Spirit'],
                                      '1_6_spirit_sports'           => ['1.6 Spirit Sports'],
                                      '1_6_v'                       => ['1.6 V'],
                                      '2.0 maxx'                    => ['2.0 Maxx'],
                                      '2_0_maxx sports'             => ['2.0 Maxx Sports'],
                                      '2_0_play'                    => ['2.0 Play'],
                                      '2_0_r'                       => ['2.0 R'],
                                      '2_0_r_sport'                 => ['2.0 R Sport'],
                                ],

                                '3_y11_14'                          => [
                                      '1_6_groove'                  => ['1.6 Groove'],
                                      '1_6_s_plus'                  => ['1.6 S Plus'],
                                      '1_6_spirit'                  => ['1.6 Spirit'],
                                      '1_6_spirit_sports'           => ['1.6 Spirit Sports'],
                                      '1_6_spirit_sports_plus'      => ['1.6 Spirit Sports Plus'],
                                      '2.0 maxx'                    => ['2.0 Maxx'],
                                      '2_0_maxx sports'             => ['2.0 Maxx Sports'],
                                      '2_0_spirit'                  => ['2.0 Spirit'],
                                ],

                                '3_y14_17'                                  => [
                                      '2_0_c'                               => ['2.0 C'],
                                      '2_0_c_sports'                        => ['2.0 C Sports'],
                                      '2_0_e'                               => ['2.0 E'],
                                      '2_0_e_sports'                        => ['2.0 E Sports'],
                                      '2_0_racing_series_limited_edition'   => ['2.0 Racing Series Limited Edition'],
                                      '2_0_s'                               => ['2.0 S'],
                                      '2_0_s_sports'                        => ['2.0 S Sports'],
                                      '2_0_sp_sports'                       => ['2.0 SP Sports'],
                                ],

                    ],

                    '6'  =>[
                                '6_y02_08'                          => [
                                      '2_3'                         => ['2.3'],
                                ],
                    ],

                    '121'  =>[
                                    '121_y92_96'                    => [
                                      '1_3'                         => ['1.3'],
                                ],
                    ],

                    '323'  =>[
                                '323_y95_98'                        => [
                                      '1_8_astina_gti'              => ['1.8 Astina GTi'],
                                ],
                                'protege_y00_06'                    => [
                                      '1_6_protege_glx'             => ['1.6 Protege GLX'],
                                      '1_6_protege_lx'              => ['1.6 Protege LX'],
                                      '1_8_protege_gt'              => ['1.8 Protege GT'],
                                      '2_0_protege_gt'              => ['2.0 Protege GT'],
                                ],
                    ],

                    '626'  =>[
                                '626_y97_00'                        => [
                                      '2_0_glx'                     => ['2.0 GLX'],
                                ],
                                'cronos_y92_97'                     => [
                                      '2_0_cronos'                  => ['2.0 Cronos'],
                                ],
                    ],

                    'bt_50'  =>[
                                'double_cab'                        => [
                                      '2_5_hi_racer'                => ['2.5 Hi-Racer'],
                                      '2_5_s'                       => ['2.5 S'],
                                      '3_0_r'                       => ['3.0 R'],
                                ],
                                'free_style_cab'                    => [
                                      '2_5_hi_racer'                => ['2.5 Hi-Racer'],
                                      '2_5_s'                       => ['2.5 S'],
                                      '2_5_v'                       => ['2.5 V'],
                                      '2_5_v_high'                  => ['2.5 V High'],
                                      '3_0_r'                       => ['3.0 R'],
                                ],
                    ],

                    'bt_50_pro'  =>[
                                'double_cab'                        => [
                                      '2_2_eclipse'                 => ['2.2 ECLIPSE'],
                                      '2_2_hi_racer'                => ['2.2 Hi-Racer'],
                                      '2_2_hi_racer_eclipse'        => ['2.2 Hi-Racer Eclipse'],
                                      '2_2_hi_racer_proseries'      => ['2.2 Hi-Racer PROSERIES'],
                                      '2_2_s'                       => ['2.2 S'],
                                      '2_2_v'                       => ['2.2 V'],
                                      '2_5_s'                       => ['2.5 S'],
                                      '3_2_r'                       => ['3.2 R'],
                                ],
                                'free_style_cab'                    => [
                                      '2_2_hi_racer'                => ['2.2 Hi-Racer'],
                                      '2_2_hi_racer_eclipse'        => ['2.2 Hi-Racer Eclipse'],
                                      '2_2_hi_racer_proseries'      => ['2.2 Hi-Racer PROSERIES'],
                                      '2_2_s'                       => ['2.2 S'],
                                      '2_2_v'                       => ['2.2 V'],
                                      '2_5_v'                       => ['2.5 V'],
                                ],
                                'single'                            => [
                                      '2_2_s'                       => ['2.2 S'],
                                      '2_5_s'                       => ['2.5 S'],
                                ],
                    ],

                    'cx_3'  =>[
                                'y15_19'                            => [
                                      '1_5_xdl'                     => ['1.5 XDL'],
                                      '2_0_s'                       => ['2.0 S'],
                                      '2_0_sp'                      => ['2.0 SP'],
                                ],
                    ],


                    'cx_5'  =>[
                                'y13_16'                            => [
                                      '2_0_c'                       => ['2.0 C'],
                                      '2_0_s'                       => ['2.0 S'],
                                      '2_2_xdl'                     => ['2.2 XDL'],
                                      '2_5_s'                       => ['2.5 S'],
                                ],

                                'y17_20'                            => [
                                      '2_0_s'                       => ['2.0 S'],
                                      '2_0_sp'                      => ['2.0 SP'],
                                      '2_2_xdl'                     => ['2.2 XDL'],
                                ],
                    ],

                    'famillia'  =>[
                                'single'                            => [
                                      '1_4_str'                     => ['1.4 STR'],
                                ],

                                'super_cab'                         => [
                                      '1_4_std'                     => ['1.4 STD '],
                                ],
                    ],

                    'fighter'  =>[
                                'double_cab'                        => [
                                      '2_5'                         => ['2.5'],
                                      '2_5_lux'                     => ['2.5 Lux'],
                                      '2_5_mid'                     => ['2.5 Mid'],
                                      '2_9'                         => ['2.9'],
                                ],

                                'free_style_cab'                    => [
                                      '2_5_lux'                     => ['2.5 LUX'],
                                      '2_5_mid'                     => ['2.5 Mid'],
                                      '2_5_se1'                     => ['2.5 SE1'],
                                ],

                                'super_saloon'                      => [
                                      '2_5_super_saloon'            => ['2.5 Super Saloon'],
                                      '2_5_super_saloon_str_base'   => ['2.5 Super Saloon STR Base'],
                                      '2_5_super_saloon_str_lux'    => ['2.5 Super Saloon STR Lux'],
                                      '2_5_super_saloon_str_mid'    => ['2.5 Super Saloon STR Mid'],
                                ],
                    ],

                    'lantis'  =>[
                                'y94_00'                            => [
                                      '1_8'                         => ['1.8'],
                                ],
                    ],

                    'magnum_thunders'  =>[
                                'y92_98'                            => [
                                      '2_5_b2500_str'               => ['2.5 B2500 STR'],
                                      '2_5_std'                     => ['2.5 STD'],
                                      '2_5_str'                     => ['2.5 STR'],
                                ],
                    ],

                    'mpv'  =>[
                                'y02_06'                            => [
                                      '3_0'                         => ['3.0'],
                                ],
                    ],

                    'mx_5'  =>[
                                'y01_05'                            => [
                                      '1_8'                         => ['1.8'],
                                ],
                                'y05_15'                            => [
                                      '2_0'                         => ['2.0'],
                                ],
                                'y16_20'                            => [
                                      '2_0'                         => ['2.0'],
                                ],
                                'y92_98'                            => [
                                      '1_6'                         => ['1.6'],
                                      '1_8'                         => ['1.8'],
                                      '1_8_roadster'                => ['1.8 Roadster'],
                                ],
                    ],

                    'rx_7'  =>[
                                'y92_00'                            => [
                                      '1_3'                         => ['1.3'],
                                      '1_3_fd'                      => ['1.3_FD'],
                                ],
                    ],

                    'rx_8'  =>[
                                'y03_08'                            => [
                                      '1_3'                         => ['1.3'],
                                      '1_3_roadster'                => ['1.3 Roadster'],
                                      '1_3_se3p'                    => ['1.3 SE3P'],
                                ],
                    ],

                    'tribute'  =>[
                                'y02_06'                            => [
                                      '2_3_sdx'                     => ['2.3 SDX'],
                                      '3_0_sdx'                     => ['3.0 SDX'],
                                ],
                    ],

                ],// ปิด mazda




                'ford' => [
                    'aspire'     =>[
                                'y94_97'                            => [
                                      '1_3_gl'                      => ['1.3 GL'],
                                ],
                    ],

                    'courier'     =>[
                                'y77_85'                            => [
                                      '2_2'                         => ['2.2'],
                                ],
                    ],

                    'ecosport'     =>[
                                'y13_16'                            => [
                                      '1_5_ambiente'                => ['1.5 Ambiente'],
                                      '1_5_titanium'                => ['1.5 Titanium'],
                                      '1_5_trend'                   => ['1.5 Trend'],
                                ],
                    ],

                    'escape'     =>[
                                'y03_07'                            => [
                                      '2_0'                         => ['2.0'],
                                      '2_3_xls'                     => ['2.3 XLS'],
                                      '2_3_xlt'                     => ['2.3 XLT'],
                                      '3_0_ltd'                     => ['3.0 LTD'],
                                      '3_0_xlt'                     => ['3.0 XLT'],
                                ],

                                'y09_12'                            => [
                                      '2_3_xls'                     => ['2.3 XLS'],
                                      '2_3_xlt'                     => ['2.3 XLT'],
                                ],
                    ],

                    'everest'     =>[
                                'y03_06'                            => [
                                      '2_5_ltd'                     => ['2.5 LTD'],
                                      '2_5_xlt'                     => ['2.5 XLT'],

                                ],

                                'y07_13'                            => [
                                      '2_5_ltd_sport_tdci'          => ['2.5 LTD Sport TDCi'],
                                      '2_5_ltd_tdci'                => ['2.5 LTD TDCi'],
                                      '2_5_xlt_tdci'                => ['2.5 XLT TDCi'],
                                      '3_0_ltd_tdci'                => ['3.0 LTD TDCi'],

                                ],

                                'y15_18'                            => [
                                      '2_2_titanium'                => ['2.2 Titanium'],
                                      '2_2_titanium_plus'           => ['2.2 Titanium+'],
                                      '3_2_titanium'                => ['3.2 Titanium'],
                                      '3_2_titanium_plus'           => ['3.2 Titanium+'],

                                ],

                                'y15_18_2'                          => [
                                      '3_2_titanium_plus'           => ['3.2 Titanium+'],

                                ],
                    ],

                    'festiva'     =>[
                                'y92_96'                            => [
                                      '1_3_gl'                      => ['1.3 GL'],

                                ],

                    ],


                    'fiesta'     =>[
                                'y10_16'                            => [
                                      '1_0_sport'                   => ['1.0 Sport'],
                                      '1_0_titanium'                => ['1.0 Titanium'],
                                      '1_4_style'                   => ['1.4 Style'],
                                      '1_4_trend'                   => ['1.4 Trend'],
                                      '1_5_ambiente'                => ['1.5 Ambiente'],
                                      '1_5_sport'                   => ['1.5 Sport'],
                                      '1_5_titanium'                => ['1.5 Titanium'],
                                      '1_5_trend'                   => ['1.5 Trend'],
                                      '1_6_sport'                   => ['1.6 Sport'],
                                      '1_6_sport_ultimate'          => ['1.6 Sport Ultimate'],
                                      '1_6_sport_plus'              => ['1.6 Sport+'],
                                      '1_6_trend'                   => ['1.6 Trend'],
                                      

                                ],

                    ],

                    'focus'     =>[
                                'y04_08'                            => [
                                      '1_8_finesse'                 => ['1.8 Finesse'],
                                      '1_8_ghia'                    => ['1.8 Ghia'],
                                      '1_8_trend'                   => ['1.8 Trend'],
                                      '2_0_sport'                   => ['2.0 Sport'],
                                ],

                                'y09_12'                            => [
                                      '1_8_ambiente'                => ['1.8 Ambiente'],
                                      '1_8_finesse'                 => ['1.8 Finesse'],
                                      '2_0_ghia'                    => ['2.0 Ghia'],
                                      '2_0_sport'                   => ['2.0 Sport'],
                                ],

                                'y12_16'                            => [
                                      '1_5_sport'                   => ['1.5 Sport'],
                                      '1_6_ambiente'                => ['1.6 Ambiente'],
                                      '1_6_trend'                   => ['1.6 Trend'],
                                      '2_0_sport'                   => ['2.0 Sport'],
                                      '2_0_sport_plus'              => ['2.0 Sport+'],
                                      '2_0_titanium'                => ['2.0 Titanium'],
                                      '2_0_titanium_plus'           => ['2.0 Titanium+'],
                                ],


                    ],

                    'laser'     =>[
                                'y00_05'                            => [
                                      '1_6_glx'                     => ['1.6 GLX'],
                                      '1_6_glxi'                    => ['1.6 GLXi'],
                                      '1_6_tierra_vxi'              => ['1.6 Tierra VXI'],
                                      '1_8_ghia'                    => ['1.8 Ghia'],
                                      '1_8_tierra_ghia'             => ['1.8 Tierra Ghia'],
                                      '1.8 tierra rs'               => ['1.8 Tierra RS'],
                                ],

                                'y91_94'                            => [
                                      '1_6_gl'                      => ['1.6 GL'],
                                ],
                    ],


                    'lincoln_continental'     =>[
                                'y70_79'                            => [
                                      '6_6'                         => ['6.6'],
                                ],
                    ],

                    'mustang'     =>[
                                'y05_15'                            => [
                                      '3_7'                         => ['3.7'],
                                      '4_6'                         => ['4.6'],
                                ],
                                'y15_20'                            => [
                                      '2_3_ecoboost'                => ['2.3 EcoBoost'],
                                      '5_0_gt'                      => ['5.0 GT'],
                                      '5_2_shelby_gt350'            => ['5.2 Shelby GT350'],
                                ],
                    ],

                    'ranger'     =>[
                                'double_cab_y03_05'                 => [
                                      '2_5_xl'                      => ['2.5 XL'],
                                      '2_5_xls'                     => ['2.5 XLS'],
                                      '2_5_xlt'                     => ['2.5 XLT'],
                                      '2_5_xlt_limited'             => ['2.5 XLT Limited'],
                                ],
                                'double_cab_y06_08'                 => [
                                      '2_5_hi_rider_xlt'            => ['2.5 Hi-Rider XLT'],
                                      '2_5_xls'                     => ['2.5 XLS'],
                                      '3_0_xlt_tdci'                => ['3.0 XLT TDCi'],
                                ],
                                'double_cab_y09_12'                 => [
                                      '2_5_hi_rider_xlt'            => ['2.2 Hi-Rider XLT'],
                                      '2_5_hi_rider_wildtrak_ltd'   => ['2.5 Hi-Rider WildTrak LTD'],
                                      '2_5_hi_rider_wildtrak_xlt'   => ['2.5 Hi-Rider WildTrak XLT'],
                                      '2_5_xls_tdci'                => ['2.5 XLS TDCi'],
                                      '2_5_wildtrak'                => ['2.5 WildTrak'],
                                      '3_0_xlt_tdci'                => ['3.0 XLT TDCi'],
                                ],
                                'double_cab_y12_15'                 => [
                                      '2_2_hi_rider_wildtrak'       => ['2.2 Hi-Rider WildTrak'],
                                      '2_2_hi_rider_wildtrak_tdci'  => ['2.2 Hi-Rider WildTrak TDCi'],
                                      '2_2_hi_rider_xls'            => ['2.2 Hi-Rider XLS'],
                                      '2_2_hi_rider_xlt'            => ['2.2 Hi-Rider XLT'],
                                      '2_2_wildtrak'                => ['2.2 WildTrak'],
                                      '2_2_xlt'                     => ['2.2 XLT'],
                                      '3_2_wildtrak'                => ['3.2 WildTrak'],
                                ],
                                'double_cab_y15_18'                 => [
                                      '2_2_hi_rider'                => ['2.2 Hi-Rider'],
                                      '2_2_hi_rider_wildtrak_tdci'  => ['2.2 Hi-Rider WildTrak TDCi'],
                                      '2_2_hi_rider_fx4'            => ['2.2 Hi-Rider FX4'],
                                      '2_2_hi_rider_wildtrak'       => ['2.2 Hi-Rider WildTrak'],
                                      '2_2_hi_rider_xls'            => ['2.2 Hi-Rider XLS'],
                                      '2_2_hi_rider_xlt'            => ['2.2 Hi-Rider XLT'],
                                      '2_2_wildtrak'                => ['2.2 WildTrak'],
                                      '2_5_xlt'                     => ['2.5 XLT'],
                                      '3_2_wildtrak'                => ['3.2 WildTrak'],
                                      '3_2_xlt'                     => ['3.2 XLT'],
                                ],
                                'double_cab_y99_02'                 => [
                                      '2_5_xlt'                     => ['2.5 XLT'],
                                      '2_9_xlt'                     => ['2.9 XLT'],
                                ],
                                'open_cab_y03_05'                   => [
                                      '2_5_hi_rider_xlt'            => ['2.5 Hi-Rider XLT'],
                                      '2_5_xl'                      => ['2.5 XL'],
                                      '2_5_xls'                     => ['2.5 XLS'],
                                      '2_5_xlt'                     => ['2.5 XLT'],
                                ],
                                'open_cab_y06_08'                   => [
                                      '2_5_hd_limited'              => ['2.5 HD Limited'],
                                      '2_5_hi_rider_wildtrak'       => ['2.5 Hi-Rider WildTrak'],
                                      '2_5_hi_rider_xls'            => ['2.5 Hi-Rider XLS'],
                                      '2_5_hi_rider_xlt'            => ['2.5 Hi-Rider XLT'],
                                      '2_5_xl'                      => ['2.5 XL'],
                                      '2_5_xls'                     => ['2.5 XLS'],
                                      '2_5_xlt'                     => ['2.5 XLT'],
                                      '3_0_hi_rider_xlt_tdci'       => ['3.0 Hi-Rider XLT TDCi'],
                                ],
                                'open_cab_y09_12'                   => [
                                      '2_5_hi_rider_wildtrak_ltd'   => ['2.5 Hi-Rider WildTrak LTD'],
                                      '2_5_hi_rider_wildtrak_xlt'   => ['2.5 Hi-Rider WildTrak XLT'],
                                      '2_5_hi_rider_xls_tdci'       => ['2.5 Hi-Rider XLS TDCi'],
                                      '2_5_hi_rider_xlt_tdci'       => ['2.5 Hi-Rider XLT TDCi'],
                                      '2_5_wildtrak'                => ['2.5 WildTrak'],
                                      '2_5_xls'                     => ['2.5 XLS'],
                                      '2_5_xls_tdci'                => ['2.5 XLS TDCi'],
                                      '2_5_xlt_tdci'                => ['2.5 XLT TDCi'],
                                      '3_0_hi_rider_xlt_tdci'       => ['3.0 Hi-Rider XLT TDCi'],
                                ],
                                'open_cab_y12_15'                   => [
                                      '2_2_hi_rider_wildtrak'       => ['2.2 Hi-Rider WildTrak'],
                                      '2_2_hi_rider_xls'            => ['2.2 Hi-Rider XLS'],
                                      '2_2_hi_rider_xls_tdci'       => ['2.2 Hi-Rider XLS TDCi'],
                                      '2_2_hi_rider_xlt'            => ['2.2 Hi-Rider XLT'],
                                      '2_2_wildtrak'                => ['2.2 WildTrak'],
                                      '2_2_xls'                     => ['2.2 XLS'],
                                      '2_2_xls_tdci'                => ['2.2 XLS TDCi'],
                                      '2_2_xlt'                     => ['2.2 XLT'],
                                      '2_5_xl'                      => ['2.5 XL'],
                                ],
                                'open_cab_y15_18'                   => [
                                      '2_2_hi_rider_xl_plus'        => ['2.2 Hi-Rider XL+'],
                                      '2_2_hi_rider_xls'            => ['2.2 Hi-Rider XLS'],
                                      '2_2_hi_rider_xlt'            => ['2.2 Hi-Rider XLT'],
                                      '2_2_xl'                      => ['2.2 XL'],
                                      '2_2_xls'                     => ['2.2 XLS'],
                                ],
                                'single_cab_y03_05'                 => [
                                      '2_5_xl'                      => ['2.5 XL'],
                                ],
                                'single_cab_y12_15'                 => [
                                      '2_2_xl'                      => ['2.2 XL'],
                                      '2_5_xl'                      => ['2.5 XL'],
                                ],
                                'single_cab_y15_18'                 => [
                                      '2_2_standard_xl'             => ['2.2 Standard XL'],
                                ],
                                'single_cab_y99_02'                 => [
                                      '2_5_xl'                      => ['2.5 XL'],
                                ],
                                'super_cab_y03_05'                  => [
                                      '2_5_xl'                      => ['2.5 XL'],
                                      '2_5_xls'                     => ['2.5 XLS'],
                                ],
                                'super_cab_y09_12'                  => [
                                      '2_5_xl'                      => ['2.5 XL'],
                                      '2_5_xlt'                     => ['2.5 XLT'],
                                      '2_9_xlt'                     => ['2.9 XLT'],
                                ],
                    ],


                ], // ปิด ford

            ], //  ปิดรายละเอียดรุ่น


            'type' => [
                'cabriolet'             =>  ['Cabriolet'],
                'mpv'                   =>  ['MPV'],
                'suv'                   =>  ['SUV'],
                'truck'                 =>  ['Truck'],
                'wagon'                 =>  ['Wagon'],
                'pickup'                =>  ['Pickup'],
                'coupe'                 =>  ['Coupe'],
                'sedan'                 =>  ['Sedan'],
                'hatchback'             =>  ['Hatchback'],
                'van'                   =>  ['Van'],
                'convertible'           =>  ['Convertible']
            ], //


            'passengers_quantity' => [
                2   => ['2 คน'],
                3   => ['3 คน'],
                4   => ['4 คน'],
                5   => ['5 คน'],
                6   => ['6 คน'],
                7   => ['7 คน'],
                8   => ['8 คน'],
                9   => ['9 คน'],
                10   => ['10+ คนขึ้นไป'],
            ], // ปิด passengers_quantity

            'baggage_quantity' => [
                0   => ['1 สัมภาระ'],
                1   => ['2 สัมภาระ'],
                2   => ['3 สัมภาระ'],
                3   => ['4 สัมภาระ'],
                4   => ['5 สัมภาระ'],
                5   => ['6 สัมภาระ'],
                6   => ['7 สัมภาระ'],
                7   => ['8 สัมภาระ'],
                8   => ['9 สัมภาระ'],
                9   => ['10+ สัมภาระขึ้นไป'],
            ], // ปิด baggage_quantity

            'insurance'                             =>[
                        'bangkok'                   => ['Bangkok Insurance'],
                        'viriyah'                   => ['Viriyah Insurance'],
                        'synmankong'                => ['Syn Man Kong Insurance'],
                        'thanachart'                => ['Thanachart Insurance'],
                        'directasia'                => ['Direct Asia Insurance'],
                        'axa'                       => ['AXA Insurance'],
                        'gobear'                    => ['Go Bear Insurance'],
                        'aig'                       => ['AIG Insurance'],
            ], //ปิด insurance

            'bangkok'                     =>[
                        'type1'                     => ['Comprehensive'],
                        'type2'                     => ['Third party Fire and Theft + vehicle collision'],
                        'type3+'                    => ['Third Party + vehicle collision'],
                        'type2'                     => ['Third-Party Fire and Theft'],
                        'type3'                     => ['Third-Party Only']
            ], //ปิด bangkok_insurance

            'viriyah'                        =>[
                        'type1'                     => ['Maximum coverage'],
                        'type2'                     => ['Coverage slightly less to Type 1'],
                        'type3'                     => ['Providing coverage for third parties only'],
                        'type4'                     => ['This policy provides coverage for property of third parties'],
                        'type5'                     => ['Insurance coverage for specific perils']
            ], //ปิด viri_insurance

            'synmankong'                  =>[
                        'type1'                     => ['First class insurance premium starting 9,600 baht'],
                        'type2'                     => ['Does not cover damage to the insured vehicle.'],
                        'type2+'                    => ['Coverage for both collision and lost fire'],
                        'type3'                     => ['Will only cover damage to life and property to third parties.'],
                        'type3+'                    => ['Covers both car and car insurance up to a maximum of 150,000 baht.']
            ],//ปิด synmankong_insurance

            'thanachart'                  =>[
                        'type1'                     => ['Comprehensive'],
                        'type2'                     => ['Third party Fire and Theft + vehicle collision'],
                        'type3+'                    => ['Third Party + vehicle collision'],
                        'type2'                     => ['Third-Party Fire and Theft'],
                        'type3'                     => ['Third-Party Only']
            ], //ปิด thanachart_insurance


            'directasia'                 =>[
                        'type1'                     => ['Comprehensive'],
                        'type2'                     => ['Third party Fire and Theft + vehicle collision'],
                        'type2+'                    => ['Third Party + vehicle collision'],
                        'type3'                     => ['Third-Party Fire and Theft'],
                        'type3+'                     => ['Third-Party Only']
            ], //ปิด direct_asia_insurance

            'axa'                         =>[
                        'type1'                     => ['Our most extensive coverage for a worry-free drive.'],
                        'type1+'                    => ['The less you drive, the less you pay.'],
                        'type2'                     => ['Class 2+ motor insurance with maximum coverage'],
                        'type2+'                    => ['Class 2+ motor insurance with a variety of coverage options.'],
                        'type3'                     => ['Third-Party Only'],
                        'type3+'                    => ['Ready to take care ofyou 24/7'],
                        'axa_motor_3_extra'         => ['3rd Class insurance with free Roadside Assistance'],
                        'motor_add_on'
            ], //ปิด axa_insurance

            'aig'                         =>[
                        'type1'                    => ['Comprehensive'],
                        'type2'                   => ['Third-Party Fire and Theft'],
                        'type2+'                    => ['Protection for your car from fire or theft'],
                        'type3'                    => ['protection for your legal liability to any third party'],
            ], //ปิด aig_insurance

            'option_extra'                          =>[
                        'gearbox'                   =>  ['Gearbox'],
                        'baby_seat'                 =>  ['Baby Seat'],
                        'parking_sensors'           =>  ['Parking sensors'],
                        'bluetooth'                 =>  ['Bluetooth'],
                        'manual_gearbox'            =>  ['Manual gearbox '],
                        'big_wheels'                =>  ['Big wheels']
            ], //ปิด option_extra

    ],//ปิดภาษาอังกฤษ

    
];